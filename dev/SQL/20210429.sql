
ALTER TABLE `product_variants` ADD `special_price_start_date` TIMESTAMP NULL DEFAULT NULL AFTER `fabpik_seller_discount_percentage`;
ALTER TABLE `product_variants` ADD `special_price_end_date` TIMESTAMP NULL DEFAULT NULL AFTER `special_price_start_date`;

ALTER TABLE `coupons` ADD INDEX(`coupon_group_id`);
ALTER TABLE `coupon_groups` ADD INDEX(`created_by`);
ALTER TABLE `coupon_groups` CHANGE `status` `status` TINYINT NULL DEFAULT '1';

ALTER TABLE `seller_setting_change_logs` ADD INDEX(`created_by`);
ALTER TABLE `seller_setting_change_logs` CHANGE `created_at` `created_at` TIMESTAMP NULL;

ALTER TABLE `order_details` ADD `cod_charge` INT NULL DEFAULT '0' AFTER `total`;
ALTER TABLE `order_details` ADD `dis_type` ENUM('f','p') NOT NULL AFTER `coupon_discount`, ADD `sub_type` ENUM('m','s') NOT NULL AFTER `dis_type`;

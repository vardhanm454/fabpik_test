ALTER TABLE `order_details` ADD `excepted_delivery_date` DATE NULL DEFAULT NULL AFTER `order_status_id`;

ALTER TABLE `order_details` ADD `gst` INT(11) NULL AFTER `tax`, ADD `commission` INT(11) NULL AFTER `gst`, ADD `final_amount_to_pay_seller` INT(11) NULL AFTER `commission`, ADD `payout_status` INT(11) NULL AFTER `final_amount_to_pay_seller`, ADD `payout_date_from_admin` DATE NULL AFTER `payout_status`;

ALTER TABLE `order_details` ADD `shipping_charge_gst` INT(11) NULL AFTER `shipping_charge`;

ALTER TABLE `orders` ADD `fabpik_invoice_no` VARCHAR(255) NULL DEFAULT NULL AFTER `invoice_no`;

ALTER TABLE `order_details` CHANGE `gst` `sgst` INT(11) NULL DEFAULT NULL;
ALTER TABLE `order_details` ADD `cgst` INT(11) NULL DEFAULT NULL AFTER `sgst`;

ALTER TABLE `order_details` ADD `courier_charges` INT(11) NULL DEFAULT NULL AFTER `cgst`;
ALTER TABLE `order_details` ADD `courier_sgst` INT(11) NULL DEFAULT NULL AFTER `courier_charges`;
ALTER TABLE `order_details` ADD `courier_cgst` INT(11) NULL DEFAULT NULL AFTER `courier_sgst`;
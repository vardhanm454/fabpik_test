ALTER TABLE `order_details` ADD `shipping_model` ENUM('f','s') NULL DEFAULT NULL COMMENT 'f=Fabpik,s=self' AFTER `excepted_delivery_date`;
ALTER TABLE `order_details` ADD `pickup_scheduled_date` TIMESTAMP NULL DEFAULT NULL AFTER `delivered_date`;

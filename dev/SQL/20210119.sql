CREATE TABLE `order_handling_charge` ( `id` int(11) NOT NULL, `min_amount` int(11) NOT NULL DEFAULT 0, `max_amount` int(11) NOT NULL DEFAULT 0, `charge_amount` int(11) NOT NULL DEFAULT 0, `status` tinyint(1) NOT NULL DEFAULT 1, `created_at` timestamp NOT NULL DEFAULT current_timestamp(), `updated_at` timestamp NULL DEFAULT NULL, `deleted_at` timestamp NULL DEFAULT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

ALTER TABLE `order_details` ADD `images` TEXT NULL DEFAULT NULL AFTER `awb_courier_id`;

ALTER TABLE `order_details` ADD `thumbnail` VARCHAR(255) NULL DEFAULT NULL AFTER `images`;

ALTER TABLE `order_handling_charge` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`id`);


INSERT INTO `order_handling_charge` (`id`, `min_amount`, `max_amount`, `charge_amount`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 250, 8, 1, '2020-11-12 21:20:40', '2020-12-09 06:37:05', NULL),
(2, 251, 500, 15, 1, '2020-11-12 21:40:14', '2020-12-09 06:29:03', NULL),
(3, 501, 750, 21, 1, '2020-12-09 06:30:36', '2020-12-09 06:30:41', NULL),
(4, 751, 1000, 26, 1, '2020-12-09 06:30:36', '2020-12-09 06:30:41', NULL),
(5, 1001, 2000, 45, 1, '2020-12-09 06:30:36', '2020-12-09 06:30:41', NULL),
(7, 2001, 0, 60, 1, '2020-12-09 06:30:36', '2020-12-09 06:30:41', NULL);

ALTER TABLE `order_details` ADD `order_handling_charge` DOUBLE NULL DEFAULT NULL AFTER `shipping_charge_cgst`, ADD `order_handling_charge_sgst` DOUBLE NULL DEFAULT NULL AFTER `order_handling_charge`, ADD `order_handling_charge_cgst` DOUBLE NULL DEFAULT NULL AFTER `order_handling_charge_sgst`;
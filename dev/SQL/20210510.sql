CREATE TABLE `commission_models` 
( 
    `id` INT(11) NOT NULL AUTO_INCREMENT , 
    `name` VARCHAR(100) NULL , 
    `commission_type` ENUM('f','v') NULL COMMENT 'f=Fixed, v=variable' ,
    `commission_on` ENUM('m','s') COMMENT 'm=MRP, s=Sell Price' ,
    `vary_with_mrp_discount` ENUM('y','n') NULL , 
    `shipping_charges` ENUM('y','n') NULL , 
    `handling_charges` ENUM('y','n') NULL , 
    `description` VARCHAR(255) NULL,
    `status` TINYINT NULL DEFAULT '1',
    `created_by` INT(11) NULL , 
    `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP , 
    `updated_by` INT(11) NULL , 
    `updated_at` TIMESTAMP NULL , 
    `deleted_by` INT(11) NULL , 
    `deleted_at` TIMESTAMP NULL , 
    PRIMARY KEY (`id`)
)

ALTER TABLE `commission_models` ADD INDEX(`created_by`);
ALTER TABLE `commission_models` ADD INDEX(`updated_by`);
ALTER TABLE `commission_models` ADD INDEX(`deleted_by`);

ALTER TABLE `sellers` ADD `commision_id` INT NULL DEFAULT NULL AFTER `commission`;

CREATE TABLE `stock_notify` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_variant_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
);

ALTER TABLE `stock_notify`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_variant_id` (`product_variant_id`);

ALTER TABLE `stock_notify`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
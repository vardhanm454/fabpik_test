ALTER TABLE `verify_otp` ADD `email` VARCHAR(50) NULL AFTER `mobile`;

ALTER TABLE `verify_otp` CHANGE `otp` `otp` VARCHAR(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `orders` CHANGE `fabpik_invoice_no` `parent_order_id` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `order_details` CHANGE `invoice_no` `child_order_id` INT(11) NOT NULL;

ALTER TABLE `order_details` ADD `invoice_no` INT(11) NULL AFTER `child_order_id`;
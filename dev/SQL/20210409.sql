-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 09, 2021 at 10:22 AM
-- Server version: 5.7.33-0ubuntu0.18.04.1
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fabpik`
--

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE `staffs` (
  `id` int(11) NOT NULL,
  `name` varchar(211) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(211) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(211) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staffs`
--

INSERT INTO `staffs` (`id`, `name`, `email`, `designation`, `mobile_no`, `seller_id`, `updated_at`, `created_at`, `deleted_at`) VALUES
(19, 'john Doe', 'john@gmail.com', 'Tester', '9078675432', NULL, '2021-03-24 06:17:17', '2021-03-24 06:03:44', '2021-03-24 06:17:17'),
(20, 'john Doe', 'john123@gmail.com', 'Tester', '9078654321', NULL, '2021-03-31 11:26:41', '2021-03-24 06:18:04', '2021-03-31 11:26:41'),
(21, 'New Staff', 'staff1@fabpik.in', 'Tech Support', '6290844051', NULL, '2021-03-31 09:52:23', '2021-03-31 09:52:23', NULL),
(22, 'sadasd', 'testco@mail.com', 'asdasddad', '1122334455', 19, '2021-04-02 06:29:01', '2021-04-02 06:29:01', NULL),
(23, 'sadasd', 'testcoo@mail.com', 'asdasddad', '1122334455', 19, '2021-04-02 06:30:00', '2021-04-02 06:30:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `staffs`
--
ALTER TABLE `staffs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `staffs`
--
ALTER TABLE `staffs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `staffs`  ADD `seller_id` INT NULL DEFAULT NULL  AFTER `mobile_no`;

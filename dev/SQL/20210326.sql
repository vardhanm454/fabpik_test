ALTER TABLE `products` ADD `product_tags` LONGTEXT NULL AFTER `country_of_origin`;

ALTER TABLE `product_variants` CHANGE `unique_id` `unique_id` VARCHAR(500) NULL DEFAULT NULL;
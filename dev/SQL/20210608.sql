ALTER TABLE `orders` ADD `grouped_convenience_fee` JSON NULL DEFAULT NULL AFTER `handling_charge`, ADD `total_convenience_fee` INT NOT NULL AFTER `grouped_convenience_fee`;

ALTER TABLE `order_details` ADD `label_url` LONGTEXT NULL AFTER `shipping_model`;
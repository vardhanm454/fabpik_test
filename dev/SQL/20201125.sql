ALTER TABLE `help_desks` ADD `attachment` VARCHAR(255) NULL DEFAULT NULL AFTER `category`;
ALTER TABLE `customers` CHANGE `alternate_mobile` `alternate_mobile` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL; 
ALTER TABLE `customers` CHANGE `dob` `dob` DATE NULL; 
ALTER TABLE `customers` CHANGE `gst_certificate` `gst_certificate` VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;

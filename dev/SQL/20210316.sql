ALTER TABLE `deals` ADD `max_orders` INT NULL DEFAULT NULL AFTER `deal_price`;
ALTER TABLE `carts` ADD `is_deal` ENUM('y','n') NOT NULL DEFAULT 'n' AFTER `coupon_applied`;
ALTER TABLE `products` ADD `hsn_code` VARCHAR(100) NULL AFTER `sku`;

ALTER TABLE `sellers` CHANGE `shipping_model` `shipping_model` ENUM('f','s') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'f' COMMENT 'f=Fabpik,s=Own Seller';

ALTER TABLE `products` ADD `fabpik_seller_price` DOUBLE NULL AFTER `sell_price`, ADD `fabpik_seller_discount` DOUBLE NULL AFTER `fabpik_seller_price`;

ALTER TABLE `product_variants` ADD `fabpik_seller_price` DOUBLE NULL AFTER `price`, ADD `fabpik_seller_discount` DOUBLE NULL AFTER `fabpik_seller_price`
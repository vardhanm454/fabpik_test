ALTER TABLE `sellers` ADD `chat_enable` TINYINT NOT NULL DEFAULT '1' AFTER `shipping_model`;

ALTER TABLE `sellers` ADD `commission` DOUBLE NOT NULL DEFAULT '14' AFTER `phone`;
CREATE TABLE `size_charts` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brands` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Array of brand ids',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `size_charts` ADD PRIMARY KEY (`id`);

ALTER TABLE `size_charts` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `size_charts`  ADD `image` VARCHAR(255) NULL DEFAULT NULL  AFTER `description`;
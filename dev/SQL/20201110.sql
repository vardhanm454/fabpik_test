CREATE TABLE `verify_otp` (
  `id` int(11) NOT NULL,
  `mobile` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` int(6) DEFAULT NULL,
  `otp_expiry` timestamp NULL DEFAULT NULL,
  `tries` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `verify_otp` ADD PRIMARY KEY (`id`);

ALTER TABLE `verify_otp` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
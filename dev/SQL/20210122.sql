ALTER TABLE `sellers` ADD `brand_name` VARCHAR(100) NULL AFTER `shipping_model`;

ALTER TABLE `sellers` ADD `store_name` VARCHAR(150) NULL DEFAULT NULL AFTER `name`;
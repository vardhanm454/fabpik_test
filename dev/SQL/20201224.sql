ALTER TABLE `order_details` ADD `shipping_weight` DOUBLE NULL DEFAULT NULL AFTER `price`, ADD `shipping_length` DOUBLE NULL DEFAULT NULL AFTER `shipping_weight`, ADD `shipping_breadth` DOUBLE NULL DEFAULT NULL AFTER `shipping_length`, ADD `shipping_height` DOUBLE NULL DEFAULT NULL AFTER `shipping_breadth`;
ALTER TABLE `order_details` ADD `mrp` DOUBLE NULL DEFAULT NULL AFTER `quantity`;
ALTER TABLE `order_details` ADD `total_seller_discount` DOUBLE NULL DEFAULT NULL AFTER `coupon_discount`;
ALTER TABLE `order_details` ADD `shipment_id` VARCHAR(100) NULL DEFAULT NULL AFTER `invoice_no`;
ALTER TABLE `order_details` ADD `seller_warehouse_pincode` VARCHAR(10) NULL DEFAULT NULL AFTER `shipping_charge_cgst`;
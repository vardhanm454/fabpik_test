INSERT INTO `order_statuses`(`name`) VALUES ('Not Delivered');

ALTER TABLE `order_details` CHANGE `payout_status` `payout_status` TINYINT(2) NULL DEFAULT '0';

ALTER TABLE `order_details` CHANGE `sgst` `sgst` DOUBLE NULL DEFAULT NULL, CHANGE `cgst` `cgst` DOUBLE NULL DEFAULT NULL, CHANGE `courier_charges` `courier_charges` DOUBLE NULL DEFAULT NULL, CHANGE `courier_sgst` `courier_sgst` DOUBLE NULL DEFAULT NULL, CHANGE `courier_cgst` `courier_cgst` DOUBLE NULL DEFAULT NULL, CHANGE `commission` `commission` DOUBLE NULL DEFAULT NULL, CHANGE `final_amount_to_pay_seller` `final_amount_to_pay_seller` DOUBLE NULL DEFAULT NULL;
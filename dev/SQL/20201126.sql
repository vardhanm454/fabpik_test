ALTER TABLE `customer_addresses` ADD `name` VARCHAR(255) NOT NULL AFTER `customer_id`; 
ALTER TABLE `customer_addresses` ADD `locality` VARCHAR(255) NOT NULL AFTER `landmark`; 
ALTER TABLE `customer_addresses` CHANGE `name` `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL; 
ALTER TABLE `customer_addresses` CHANGE `locality` `locality` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL; 
ALTER TABLE `customer_addresses` CHANGE `name` `name` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL; 
ALTER TABLE `customer_addresses` CHANGE `locality` `locality` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL; 

TRUNCATE iron_types

INSERT INTO `iron_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'iron at any temperature', '2020-11-19 00:06:39', NULL),
(2, 'do not iron', '2020-11-19 00:06:39', NULL),
(3, 'steam', '2021-02-15 11:22:23', NULL),
(4, 'no steam', '2021-02-15 11:22:23', NULL),
(5, 'low heat', '2021-02-15 11:22:41', NULL),
(6, 'medium heat', '2021-02-15 11:22:41', NULL),
(7, 'high heat', '2021-02-15 11:22:52', NULL),
(8, 'N/A', '2021-02-15 11:23:21', NULL);

-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2021 at 12:30 PM
-- Server version: 5.7.33-0ubuntu0.18.04.1
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `lqueue`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE
    `reviews` CHANGE `review_title` `review_title` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
    CHANGE `rating` `rating` TINYINT(1) NULL,
    CHANGE `review_comment` `review_comment` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
    CHANGE `viewed` `viewed` TINYINT(1) NOT NULL DEFAULT '0',
    CHANGE `status` `status` TINYINT(1) NOT NULL DEFAULT '1',
    CHANGE `created_at` `created_at` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CHANGE `updated_at` `updated_at` TIMESTAMP NULL DEFAULT NULL;

ALTER TABLE `reviews` CHANGE `viewed` `published` TINYINT(1) NOT NULL DEFAULT '0';

DROP TABLE attribute_category;

ALTER TABLE `carts` DROP INDEX `customer_id`;

ALTER TABLE `carts` ADD INDEX(`customer_id`);
ALTER TABLE `carts` ADD INDEX(`seller_id`);
ALTER TABLE `carts` ADD INDEX(`product_id`);
ALTER TABLE `carts` ADD INDEX(`product_variant_id`);
ALTER TABLE `carts` ADD INDEX(`coupon_id`);
ALTER TABLE `carts` ADD INDEX(`customer_id`);

ALTER TABLE `categories` DROP INDEX `primary_attribute`;
ALTER TABLE `categories` ADD INDEX(`primary_attribute`);
ALTER TABLE `categories` ADD INDEX(`secondary_attribute`);

ALTER TABLE `coupons` DROP INDEX `code`;
ALTER TABLE `coupons` ADD INDEX(`dis_type`);
ALTER TABLE `coupons` ADD INDEX(`sub_type`);

ALTER TABLE `customers` DROP INDEX `name`;
ALTER TABLE `customers` ADD INDEX(`name`);
ALTER TABLE `customers` ADD INDEX(`email`);
ALTER TABLE `customers` ADD INDEX(`mobile`);

ALTER TABLE `customer_addresses` DROP INDEX `customer_id`;
ALTER TABLE `customer_addresses` ADD INDEX(`customer_id`);
ALTER TABLE `customer_addresses` ADD INDEX(`mobile`);
ALTER TABLE `customer_addresses` ADD INDEX(`addr_type`);
ALTER TABLE `customer_addresses` ADD INDEX(`state_id`);

ALTER TABLE `customer_feedback` ADD INDEX(`customer_id`);

ALTER TABLE `help_desks` DROP INDEX `ticket_no`;
ALTER TABLE `help_desks` ADD INDEX(`ticket_no`);
ALTER TABLE `help_desks` ADD INDEX(`seller_id`);

ALTER TABLE `help_desk_comments` DROP INDEX `help_desk_id`;
ALTER TABLE `help_desk_comments` ADD INDEX(`help_desk_id`);
ALTER TABLE `help_desk_comments` ADD INDEX(`comment_by`);

ALTER TABLE `products` DROP INDEX `name`;
ALTER TABLE `products` ADD INDEX(`name`);
ALTER TABLE `products` ADD INDEX(`seller_id`);
ALTER TABLE `products` ADD INDEX(`brand_id`);
ALTER TABLE `products` ADD INDEX(`unique_id`);

ALTER TABLE `products` DROP INDEX `primary_category`;
ALTER TABLE `products` ADD INDEX(`primary_category`);
ALTER TABLE `products` ADD INDEX(`size_chart_id`);
ALTER TABLE `products` ADD INDEX(`washing_type_id`);
ALTER TABLE `products` ADD INDEX(`iron_type_id`);
ALTER TABLE `products` ADD INDEX(`country_of_origin`);

DROP TABLE product_category;

ALTER TABLE `product_categories` DROP INDEX `product_id`;
ALTER TABLE `product_categories` ADD INDEX(`product_id`);
ALTER TABLE `product_categories` ADD INDEX(`category_id`);
ALTER TABLE `product_categories` ADD INDEX(`subcategory_id`);
ALTER TABLE `product_categories` ADD INDEX(`childcategory_id`);

ALTER TABLE `product_variants` DROP INDEX `product_id`;
ALTER TABLE `product_variants` ADD INDEX(`product_id`);
ALTER TABLE `product_variants` ADD INDEX(`unique_id`);
ALTER TABLE `product_variants` ADD INDEX(`name`);
ALTER TABLE `product_variants` ADD INDEX(`sku`);

ALTER TABLE `product_variant_options` DROP INDEX `product_variant_id`;
ALTER TABLE `product_variant_options` ADD INDEX(`product_variant_id`);
ALTER TABLE `product_variant_options` ADD INDEX(`attribute_id`);
ALTER TABLE `product_variant_options` ADD INDEX(`attribute_option_id`);

ALTER TABLE `reviews` ADD INDEX(`customer_id`);
ALTER TABLE `reviews` ADD INDEX(`product_id`);
ALTER TABLE `reviews` ADD INDEX(`product_variant_id`);

ALTER TABLE `seller_warehouses` DROP INDEX `seller_id`;
ALTER TABLE `seller_warehouses` ADD INDEX(`seller_id`);
ALTER TABLE `seller_warehouses` ADD INDEX(`state_id`);

ALTER TABLE `sms_logs` ADD INDEX(`mobile`);

ALTER TABLE `users` DROP INDEX `customer_id`;
ALTER TABLE `users` ADD INDEX(`customer_id`);
ALTER TABLE `users` ADD INDEX(`seller_id`);
ALTER TABLE `users` ADD INDEX(`mobile`);

ALTER TABLE `variation_images` DROP INDEX `product_id_2`;
ALTER TABLE `variation_images` DROP INDEX `product_id`;
ALTER TABLE `variation_images` ADD INDEX(`product_id`);
ALTER TABLE `variation_images` ADD INDEX(`attribute_option_id`);

ALTER TABLE `wishlists` DROP `deleted_at`;
ALTER TABLE `wishlists` DROP INDEX `customer_id_2`;
ALTER TABLE `wishlists` DROP INDEX `customer_id`;
ALTER TABLE `wishlists` ADD INDEX(`customer_id`);
ALTER TABLE `wishlists` ADD INDEX(`product_id`);
ALTER TABLE `wishlists` ADD INDEX(`product_variant_id`);

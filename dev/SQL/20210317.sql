INSERT INTO `order_statuses` (`id`, `name`, `admin_text`, `seller_text`, `user_text`, `notify_email`, `notify_sms`, `created_at`, `updated_at`) VALUES (NULL, 'Product Returned', 'Product Returned', 'Product Returned', 'Product Returned', NULL, NULL, CURRENT_TIMESTAMP, NULL);
ALTER TABLE `deals` ADD `total_max_orders` INT NULL DEFAULT NULL AFTER `max_orders`;

CREATE TABLE `shipping_statuses`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NULL DEFAULT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY(`id`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_unicode_ci;

INSERT INTO `shipping_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Pending', '2020-11-27 07:37:24', NULL),
(2, 'Confirmed', '2020-11-27 07:37:24', NULL),
(3, 'Packed', '2020-11-27 07:38:17', NULL),
(4, 'Picked up by Shipping Partner', '2020-11-27 07:38:17', NULL),
(5, 'Shipped', '2020-11-27 07:38:41', NULL),
(6, 'Out for Delivery', '2020-11-27 07:38:41', NULL),
(7, 'Delivered', '2020-11-27 07:39:03', NULL);

CREATE TABLE `payment_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for table `payment_statuses`
--
ALTER TABLE `payment_statuses`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `payment_statuses`
--
ALTER TABLE `payment_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `payment_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Not Paid', '2020-11-27 07:36:34', NULL),
(2, 'Paid', '2020-11-27 07:36:34', NULL),
(3, 'Failed', '2020-11-27 07:36:58', NULL),
(4, 'Cancelled', '2020-11-27 07:36:58', NULL);


CREATE TABLE `order_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `order_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Pending', '2020-11-27 07:06:19', NULL),
(2, 'Confirmed', '2020-11-27 07:06:19', NULL),
(3, 'Rejected by Seller', '2020-11-27 07:07:53', NULL),
(4, 'Rejected by Admin', '2020-11-27 07:07:53', NULL),
(5, 'Refund Requested by User', '2020-11-27 07:08:08', NULL),
(6, 'Exchange/Return Requested by User', '2020-11-27 07:08:08', NULL),
(7, 'Cancelled', '2020-11-27 07:08:32', NULL),
(8, 'Refund request Accepted by Seller', '2020-11-27 07:08:32', NULL),
(9, 'Refund Accepted by Admin', '2020-11-27 07:09:06', NULL),
(10, 'Refunded', '2020-11-27 07:09:06', NULL),
(11, 'Under User Exchange Period', '2020-11-27 07:11:01', NULL),
(12, 'Delivered', '2020-11-27 07:11:01', NULL),
(13, 'Completed', '2020-11-27 07:11:17', NULL);


CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `invoice_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_prefix` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `customer_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_mobile` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_pincode` int(6) DEFAULT NULL,
  `payment_status_id` int(11) DEFAULT NULL,
  `shipping_first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_mobile` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state_id` int(11) DEFAULT NULL,
  `shipping_pincode` int(6) DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` double DEFAULT NULL,
  `order_status_id` int(11) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `coupon_discount` double DEFAULT NULL,
  `shipping_charge` double DEFAULT NULL,
  `handling_charge` double DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `orders` ADD INDEX(`invoice_no`);
ALTER TABLE `orders` ADD INDEX(`customer_id`);
ALTER TABLE `orders` ADD INDEX(`email`);
ALTER TABLE `orders` ADD INDEX(`mobile`);
ALTER TABLE `orders` ADD INDEX(`billing_state_id`);
ALTER TABLE `orders` ADD INDEX(`shipping_state_id`);
ALTER TABLE `orders` ADD INDEX(`order_status_id`);
ALTER TABLE `orders` ADD INDEX(`coupon_id`);

CREATE TABLE `order_histories`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `order_id` INT NULL DEFAULT NULL,
    `order_detail_id` INT NULL DEFAULT NULL,
    `order_status_id` INT NULL DEFAULT NULL,
    `notify` TINYINT(1) NOT NULL DEFAULT '0',
    `comment` VARCHAR(255) NULL DEFAULT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY(`id`),
    INDEX(`order_id`),
    INDEX(`order_detail_id`),
    INDEX(`order_status_id`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE `order_details`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `order_id` INT NULL DEFAULT NULL,
  	`tracking_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `seller_id` INT NULL DEFAULT NULL,
    `product_id` INT NULL DEFAULT NULL,
    `product_variant_id` INT NULL DEFAULT NULL,
    `name` VARCHAR(100) NULL DEFAULT NULL,
    `quantity` INT NULL DEFAULT NULL,
    `price` DOUBLE NULL DEFAULT NULL,
    `coupon_id` INT NULL DEFAULT NULL,
    `coupon_discount` DOUBLE NULL DEFAULT NULL,
    `discount` DOUBLE NULL DEFAULT NULL,
    `total` DOUBLE NULL DEFAULT NULL,
    `tax` DOUBLE NULL DEFAULT NULL,
  	`shipping_status_id` int(11) DEFAULT NULL,
  	`shipping_charge` double DEFAULT NULL,
    `order_status_id` INT NULL DEFAULT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY(`id`),
    INDEX(`order_status_id`),
    INDEX(`shipping_status_id`),
    INDEX(`coupon_id`),
    INDEX(`product_variant_id`),
    INDEX(`product_id`),
    INDEX(`seller_id`),
    INDEX(`tracking_no`),
    INDEX(`order_id`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE `sellers`  ADD `unique_id` VARCHAR(50) NULL DEFAULT NULL  AFTER `name`;

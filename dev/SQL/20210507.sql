ALTER TABLE `notifications` ADD `notified_to` TINYTEXT NULL DEFAULT NULL COMMENT 'c=>customers, s=>sellers' AFTER `notify_on`;
ALTER TABLE `notifications` ADD `deleted_by` INT NULL DEFAULT NULL AFTER `updated_at`;
ALTER TABLE `users` ADD `get_notified` ENUM('y','n') NOT NULL DEFAULT 'n' AFTER `password`;

CREATE TABLE `stylelayouts` (
  `id` int NOT NULL,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `columns` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` json DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) 

ALTER TABLE `stylelayouts` ADD PRIMARY KEY(`id`);

ALTER TABLE `stylelayouts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

ALTER TABLE `stylelayouts`
  ADD KEY `category_id` (`category_id`);


CREATE TABLE `layout_columns` 
( 
`id` int NOT NULL, 
`name` varchar(200) DEFAULT NULL, 
`created_by` int DEFAULT NULL, 
`modified_by` int DEFAULT NULL, 
`deleted_by` int DEFAULT NULL, 
`status` tinyint DEFAULT '1', 
`created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP, 
`updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP, 
`deleted_at` timestamp NULL DEFAULT NULL 
)

ALTER TABLE `layout_columns` ADD PRIMARY KEY(`id`);

ALTER TABLE `layout_columns` CHANGE `id` `id` INT NOT NULL AUTO_INCREMENT;


CREATE TABLE `layout_column_logs` 
( 
`id` int NOT NULL, 
`column_id` int NOT NULL, 
`from_name` varchar(200) NOT NULL, 
`to_name` varchar(200) NOT NULL, 
`from_status` tinyint DEFAULT NULL, 
`to_status` tinyint DEFAULT NULL, 
`modified_by` int NOT NULL, 
`created_at` timestamp NOT NULL, 
`updated_at` timestamp NOT NULL 
)

ALTER TABLE `layout_column_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `column_id` (`column_id`),
  ADD KEY `modified_by` (`modified_by`);

ALTER TABLE `layout_column_logs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT

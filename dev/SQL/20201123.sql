CREATE VIEW `view_categories` as SELECT
    `t1`.`id` AS `child_id`,
    `t1`.`subcategory_id` AS `subcategory_id`,
    `t1`.`category_id` AS `category_id`,
    `t1`.`title` AS `child_title`,
    `t2`.`title` AS `subcategory_title`,
    `t3`.`title` AS `category_title`,
    CONCAT(`t3`.`id`, '-', `t2`.`id`, '-', `t1`.`id`) AS path_id,
    CONCAT(`t3`.`title`, ' > ', `t2`.`title`, ' > ', `t1`.`title`) AS path_title
FROM
    childcategories AS t1
JOIN
    subcategories AS t2
ON
    t2.id = t1.subcategory_id
JOIN
    categories AS t3
ON
    t3.id = t1.category_id
    WHERE `t1`.`deleted_at` IS NULL AND `t2`.`deleted_at` IS NULL AND `t3`.`deleted_at` IS NULL AND `t1`.`status` = 1 AND `t2`.`status` = 1 AND `t3`.`status` = 1
ORDER BY
    `t3`.`title`,
    `t2`.`title`,
    `t1`.`title`;

ALTER TABLE `categories` ADD INDEX(`title`);
ALTER TABLE `childcategories` ADD INDEX(`category_id`);
ALTER TABLE `childcategories` ADD INDEX(`subcategory_id`);
ALTER TABLE `childcategories` ADD INDEX(`title`);
ALTER TABLE `subcategories` ADD INDEX(`category_id`);
ALTER TABLE `subcategories` ADD INDEX(`title`);
ALTER TABLE `products` ADD INDEX( `name`, `seller_id`, `brand_id`, `unique_id`);
ALTER TABLE `attributes` ADD INDEX( `name`, `display_name`);
ALTER TABLE `attribute_options` ADD INDEX( `attribute_id`, `option_name`);
ALTER TABLE `brands` ADD INDEX( `name`, `slug`, `show_on_home`);
ALTER TABLE `carts` ADD INDEX( `customer_id`, `seller_id`, `product_id`, `product_variant_id`, `coupon_id`);
ALTER TABLE `categories`  ADD `slug` VARCHAR(255) NULL DEFAULT NULL  AFTER `title`;
ALTER TABLE `categories` ADD INDEX( `primary_attribute`, `secondary_attribute`);
ALTER TABLE `childcategories`  ADD `slug` VARCHAR(191) NULL DEFAULT NULL  AFTER `title`;
ALTER TABLE `coupons` ADD INDEX( `code`, `dis_type`, `sub_type`);
ALTER TABLE `customers` ADD INDEX( `name`, `email`, `mobile`);
ALTER TABLE `customer_addresses` ADD INDEX( `customer_id`, `mobile`, `addr_type`, `state_id`);
ALTER TABLE `help_desks` ADD INDEX( `ticket_no`, `seller_id`);
ALTER TABLE `help_desk_comments` ADD INDEX( `help_desk_id`, `comment_by`);
ALTER TABLE `products` ADD INDEX( `primary_category`, `size_chart_id`, `washing_type_id`, `iron_type_id`, `country_of_origin`);
ALTER TABLE `product_categories` ADD INDEX( `product_id`, `category_id`, `subcategory_id`, `childcategory_id`);
ALTER TABLE `product_variants` ADD INDEX( `product_id`, `unique_id`, `name`, `sku`);
ALTER TABLE `product_variant_options` ADD INDEX( `product_variant_id`, `attribute_id`, `attribute_option_id`);
ALTER TABLE `sellers` ADD INDEX( `name`, `email`, `mobile`, `phone`);
ALTER TABLE `seller_warehouses` ADD INDEX( `seller_id`, `state_id`);
ALTER TABLE `size_charts` ADD INDEX( `seller_id`);
ALTER TABLE `subcategories`  ADD `slug` VARCHAR(255) NULL DEFAULT NULL  AFTER `title`;
ALTER TABLE `users` ADD INDEX( `customer_id`, `seller_id`, `mobile`);
ALTER TABLE `variation_images` ADD INDEX( `product_id`, `attribute_option_id`);
ALTER TABLE `verify_otp` ADD INDEX( `mobile`);
ALTER TABLE `wishlists` ADD INDEX( `customer_id`, `product_id`, `product_variant_id`);
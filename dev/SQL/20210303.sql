ALTER TABLE `categories` ADD `priority` INT(11) NULL AFTER `secondary_attribute`;

ALTER TABLE `customers` ADD `inactive_reason` VARCHAR(191) NULL AFTER `status`;
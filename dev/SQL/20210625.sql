CREATE TABLE `courier_partners` (
  `id` int NOT NULL,
  `name` varchar(155) DEFAULT NULL,
  `tracking_url` longtext,
  `is_append` ENUM('y','n') NULL DEFAULT 'n',
  `created_by` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
);

ALTER TABLE `courier_partners`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `courier_partners`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

INSERT INTO `courier_partners` (`id`, `name`, `tracking_url`, `is_append`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Aramex International', 'https://www.aramex.com/us/en/track/results?mode=0&ShipmentNumber=', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(2, 'Blue Dart', 'https://www.bluedart.com/tracking', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(3, 'Blue Dart Surface', 'https://www.bluedart.com/tracking', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(4, 'Delhivery', 'https://www.bluedart.com/tracking', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(5, 'Delhivery Reverse', 'https://www.bluedart.com/tracking', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(6, 'Delhivery Surface', 'https://www.delhivery.com/track/package/', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(7, 'Delhivery Surface 10 Kgs', 'https://www.delhivery.com/track/package/', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(8, 'Delhivery Surface 2 Kgs', 'https://www.delhivery.com/track/package/', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(9, 'Delhivery Surface 20 Kgs', 'https://www.delhivery.com/track/package/', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(10, 'Delhivery Surface 5 Kgs', 'https://www.delhivery.com/track/package/', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(11, 'Ecom Express', 'https://ecomexpress.in/tracking/?awb_field=', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(12, 'Ecom Express Reverse', 'https://ecomexpress.in/tracking/?awb_field=', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(13, 'Ecom Express ROS', 'https://ecomexpress.in/tracking/?awb_field=', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(14, 'Ecom Express ROS Reverse', 'https://ecomexpress.in/tracking/?awb_field=', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(15, 'Ekart Logistics Surface', 'https://www.ekartlogistics.com/track/', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(16, 'FedEx', 'https://www.fedex.com/fedextrack/?trknbr=', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(17, 'FedEx Flat Rate', 'https://www.fedex.com/fedextrack/?trknbr=', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(18, 'FedEx Surface 1 Kg', 'https://www.fedex.com/fedextrack/?trknbr=', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(19, 'FedEx Surface 10 Kg', 'https://www.fedex.com/fedextrack/?trknbr=', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(20, 'FedEx Surface 5 Kg', 'https://www.fedex.com/fedextrack/?trknbr=', 'y', NULL, '2021-06-25 13:19:07', NULL, NULL),
(21, 'Gati Surface 5 Kg', 'https://www.gati.com/track-by-docket/', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(22, 'Shadowfax Local', 'https://tracker.shadowfax.in/#/', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(23, 'Shadowfax Surface', 'https://tracker.shadowfax.in/#/', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(24, 'Wefast Local', 'https://wefast.in/track', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(25, 'Wefast Local 5 Kg', 'https://wefast.in/track', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(26, 'Xpressbees', 'https://www.xpressbees.com/track', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(27, 'Xpressbees 1kg', 'https://www.xpressbees.com/track', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(28, 'Xpressbees 2kg', 'https://www.xpressbees.com/track', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(29, 'Xpressbees 5kg', 'https://www.xpressbees.com/track', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL),
(30, 'Xpressbees Surface', 'https://www.xpressbees.com/track', 'n', NULL, '2021-06-25 13:19:07', NULL, NULL);

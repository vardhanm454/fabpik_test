CREATE TABLE `sms_logs`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `mobile` VARCHAR(15) NULL DEFAULT NULL,
    `action` VARCHAR(50) NULL DEFAULT NULL,
    `response_data` VARCHAR(255) NULL DEFAULT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY(`id`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE `order_details` ADD `payment_status_id` INT(11) NULL AFTER `order_status_id`;

ALTER TABLE `order_details` ADD FOREIGN KEY (`payment_status_id`) REFERENCES `payment_statuses`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

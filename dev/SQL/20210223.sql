CREATE TABLE `seller_setting_change_logs` ( `id` INT(11) NOT NULL , `settings_column_name` VARCHAR(50) NULL , `old_value` VARCHAR(50) NULL , `new_value` VARCHAR(50) NULL , `created_by` INT(11) NULL , `created_at` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP , `approved_by` INT(11) NULL , `approved_at` TIMESTAMP NULL , `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `seller_setting_change_logs` CHANGE `id` `id` INT NOT NULL AUTO_INCREMENT;

ALTER TABLE `sellers` ADD `cod_enable` TINYINT NOT NULL DEFAULT '0' AFTER `phone`;
ALTER TABLE `sellers` CHANGE `cod_enable` `cod_enable` ENUM('y','n') NOT NULL DEFAULT 'n';

ALTER TABLE `seller_setting_change_logs` ADD `is_deleted` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `updated_at`;
ALTER TABLE `seller_setting_change_logs` CHANGE `is_deleted` `is_deleted` TIMESTAMP NULL;
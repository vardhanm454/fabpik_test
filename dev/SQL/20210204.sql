ALTER TABLE `categories` CHANGE `commission` `commission` DOUBLE NOT NULL DEFAULT '0', CHANGE `tax` `tax` DOUBLE NOT NULL DEFAULT '0';

ALTER TABLE `product_categories` ADD `is_active` TINYINT(2) NOT NULL DEFAULT '1' AFTER `childcategory_id`;
ALTER TABLE `categories` DROP `primary_attr`;
ALTER TABLE `products` DROP `primary_attribute`;

CREATE VIEW `view_childcat_product_count` AS
SELECT
    t1.childcategory_id,
    COUNT(t1.id) AS total_products
FROM
    `product_categories` AS t1
JOIN
    products AS t2
ON
    t2.id = t1.product_id
JOIN
    product_variants AS t3
ON
    t3.product_id = t2.id
JOIN
    view_categories AS t4
ON
    t4.child_id = t1.childcategory_id
WHERE
    t2.status = 1 AND t2.deleted_at IS NULL
GROUP BY
    t1.childcategory_id;


CREATE VIEW `view_subcat_product_count` AS
SELECT
    t1.subcategory_id,
    COUNT(t1.id) AS total_products
FROM
    `product_categories` AS t1
JOIN
    products AS t2
ON
    t2.id = t1.product_id
JOIN
    product_variants AS t3
ON
    t3.product_id = t2.id
JOIN
    view_categories AS t4
ON
    t4.subcategory_id = t1.subcategory_id
WHERE
    t2.status = 1 AND t2.deleted_at IS NULL
GROUP BY
    t1.subcategory_id;

CREATE VIEW `view_cat_product_count` AS
SELECT
    t1.category_id,
    COUNT(t1.id) AS total_products
FROM
    `product_categories` AS t1
JOIN
    products AS t2
ON
    t2.id = t1.product_id
JOIN
    product_variants AS t3
ON
    t3.product_id = t2.id
JOIN
    view_categories AS t4
ON
    t4.category_id = t1.category_id
WHERE
    t2.status = 1 AND t2.deleted_at IS NULL
GROUP BY
    t1.category_id;

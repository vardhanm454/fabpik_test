ALTER TABLE `coupons` ADD `max_usage` ENUM('o','m') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'o' COMMENT 'o=once m=multiple' AFTER `sub_type`;

ALTER TABLE `coupons` ADD `description` VARCHAR(155) NULL DEFAULT NULL AFTER `code`;
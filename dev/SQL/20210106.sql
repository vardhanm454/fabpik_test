ALTER TABLE `wishlists` ADD `deleted_at` TIMESTAMP NULL AFTER `updated_at`;

CREATE TABLE `fabpik`.`customer_feedback` ( `id` INT NOT NULL AUTO_INCREMENT , `customer_id` INT NOT NULL , `feedback` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , `created_at` TIMESTAMP NULL , `update_at` TIMESTAMP NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `customer_feedback` CHANGE `update_at` `updated_at` TIMESTAMP NULL DEFAULT NULL;
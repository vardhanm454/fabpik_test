ALTER TABLE `order_details` ADD `order_handling_charge_igst` DOUBLE NULL DEFAULT NULL AFTER `order_handling_charge_cgst`;
ALTER TABLE `order_details`  ADD `shipping_charge_igst` DOUBLE NULL DEFAULT NULL  AFTER `shipping_charge_cgst`;
ALTER TABLE `order_details`  ADD `commission_igst` DOUBLE NULL DEFAULT NULL  AFTER `commission_cgst`;

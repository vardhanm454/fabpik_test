ALTER TABLE `users`  ADD `system_generated_password` VARCHAR(5) NOT NULL DEFAULT 'y'  AFTER `remember_token`;
ALTER TABLE `order_details` ADD `tracking_url` LONGTEXT NULL AFTER `shiprocket_order_id`;
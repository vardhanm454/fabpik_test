CREATE TABLE `product_tags_logs` (
  `id` int NOT NULL,
  `tag_id` int NOT NULL,
  `from_name` varchar(200) NOT NULL,
  `to_name` varchar(200) NOT NULL,
  `from_status` tinyint DEFAULT NULL,
  `to_status` tinyint DEFAULT NULL,
  `modified_by` int NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) 

ALTER TABLE `product_tags_logs`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `product_tags_logs` ADD INDEX(`tag_id`);

ALTER TABLE `product_tags_logs` ADD INDEX(`modified_by`);

ALTER TABLE `product_tags_logs` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `product_tags` ADD `modified_by` INT NULL AFTER `created_by`;

ALTER TABLE `product_tags_logs` ADD `from_status` TINYINT NULL AFTER `to_name`, ADD `to_status` TINYINT NULL AFTER `from_status`;


CREATE TABLE `assigned_product_tags` (
  `id` int NOT NULL,
  `product_id` int NOT NULL,
  `tag_id` int NOT NULL,
  `created_by` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
)

ALTER TABLE `assigned_product_tags`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `assigned_product_tags` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `assigned_product_tags` ADD INDEX(`tag_id`);

ALTER TABLE `assigned_product_tags` ADD INDEX(`product_id`);
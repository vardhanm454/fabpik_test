ALTER TABLE `commission_models` ADD `description` VARCHAR(255) NULL AFTER `handling_charges`;

ALTER TABLE `sellers` CHANGE `commision_id` `commission_id` INT NULL DEFAULT NULL;

ALTER TABLE `product_variants` ADD `min_stock_quantity` INT(11) NULL DEFAULT '1' AFTER `stock`;

ALTER TABLE `size_charts` ADD `style_layout_id` INT NULL AFTER `seller_id`;

ALTER TABLE `products` ADD `min_stock_quantity` INT(11) NULL DEFAULT '1' AFTER `product_tags`;
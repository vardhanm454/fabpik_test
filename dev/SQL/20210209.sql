ALTER TABLE `products` CHANGE `washing_type_id` `washing_type_id` VARCHAR(100) NULL DEFAULT NULL;
ALTER TABLE `product_variants` ADD `hsn_code` INT(11) NULL AFTER `unique_id`;
ALTER TABLE `product_variants` ADD `isbn_code` VARCHAR(200) NULL AFTER `hsn_code`;

ALTER TABLE `categories` ADD `priority` INT(11) NULL AFTER `secondary_attribute`;
ALTER TABLE `carts` ADD `etd` TIMESTAMP NULL DEFAULT NULL AFTER `seller_pincode`;

ALTER TABLE `order_details` ADD `mrp` DOUBLE NULL DEFAULT NULL AFTER `quantity`;

ALTER TABLE `order_details` ADD `total_seller_discount` DOUBLE NULL DEFAULT NULL AFTER `coupon_discount`;

ALTER TABLE `order_details` ADD `shipment_id` VARCHAR(100) NULL DEFAULT NULL AFTER `invoice_no`;

ALTER TABLE `order_details` ADD `seller_warehouse_pincode` VARCHAR(10) NULL DEFAULT NULL AFTER `shipping_charge_cgst`;
ALTER TABLE `staffs` DROP COLUMN `designation`;
ALTER TABLE `staffs` ADD `status` TINYINT(1) NULL DEFAULT NULL COMMENT '1=>Active,0=>Inactive' AFTER `mobile_no`;
ALTER TABLE `permissions`  ADD `module_name` VARCHAR(50) NULL DEFAULT NULL  AFTER `id`;

ALTER TABLE `users`  ADD `ref_id` INT NULL DEFAULT NULL  AFTER `id`,  ADD `user_type` VARCHAR(20) NULL DEFAULT NULL COMMENT 'admin-staff, seller, seller-staff, customer'  AFTER `ref_id`;
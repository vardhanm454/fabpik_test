ALTER TABLE `products`  ADD `seller_discount` DOUBLE NOT NULL DEFAULT '0.00'  AFTER `mrp`;

ALTER TABLE `carts` CHANGE `cutsomer_pincode` `customer_pincode` VARCHAR(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `carts` CHANGE `quantity` `quantity` INT(11) NOT NULL DEFAULT '1';
ALTER TABLE `products` DROP `attribute_ids`;
ALTER TABLE `products` CHANGE `ready_to_ship_days` `min_ship_hours` INT(11) NULL DEFAULT NULL COMMENT 'minimum hours to ship ready';
ALTER TABLE `products` CHANGE `featured` `seller_featured` TINYINT(1) NULL DEFAULT '0';
ALTER TABLE `products`  ADD `fabpik_featured` TINYINT(1) NOT NULL DEFAULT '0'  AFTER `seller_featured`;
ALTER TABLE `products`  ADD `dress_material` VARCHAR(255) NULL DEFAULT NULL  AFTER `fabpik_featured`,  ADD `washing_type_id` INT NULL DEFAULT NULL  AFTER `dress_material`,  ADD `iron_type_id` INT NULL DEFAULT NULL  AFTER `washing_type_id`,  ADD `no_of_items` INT NULL DEFAULT NULL  AFTER `iron_type_id`,  ADD `items_in_package` VARCHAR(255) NULL DEFAULT NULL  AFTER `no_of_items`,  ADD `country_of_origin` INT NULL DEFAULT NULL  AFTER `items_in_package`;

ALTER TABLE `categories`  ADD `secondary_attribute` INT NULL DEFAULT NULL  AFTER `primary_attr`;
ALTER TABLE `categories` CHANGE `primary_attr` `primary_attribute` INT(11) NULL DEFAULT NULL;
ALTER TABLE `products`  ADD `thumbnail` VARCHAR(255) NULL DEFAULT NULL  AFTER `images`;

CREATE TABLE `fabpik`.`variation_images`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `product_id` INT NULL DEFAULT NULL,
    `attribute_option_id` INT NULL DEFAULT NULL,
    `thumbnail` VARCHAR(255) NULL DEFAULT NULL,
    `images` TEXT NULL DEFAULT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    `deleted_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY(`id`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE `product_variants` DROP `images`;
ALTER TABLE `product_variants`  ADD `unique_id` INT NULL DEFAULT NULL  AFTER `product_id`;
ALTER TABLE `product_variants` CHANGE `ready_to_ship_days` `min_ship_hours` INT(11) NULL DEFAULT NULL COMMENT 'minimum hours to ship ready';

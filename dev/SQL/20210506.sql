CREATE TABLE `notifications` (
  `id` int NOT NULL,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `notify_on` timestamp NULL DEFAULT NULL,
  `notified_to` tinytext COLLATE utf8mb4_unicode_ci COMMENT 'c=>customers, s=>sellers',
  `users` json DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
)


ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `master_sizecharts` (
  `id` int NOT NULL,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `sizes` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `columns` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` json DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) 

ALTER TABLE `master_sizecharts` ADD PRIMARY KEY(`id`);

ALTER TABLE `master_sizecharts`
  ADD KEY `category_id` (`category_id`);

ALTER TABLE `master_sizecharts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

CREATE TABLE `seller_sizecharts`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `master_sizechart_id` INT NULL DEFAULT NULL,
    `category_id` INT NULL DEFAULT NULL,
    `table_data` JSON NULL DEFAULT NULL,
    `note` VARCHAR(255) NULL DEFAULT NULL,
    `created_by` INT NULL DEFAULT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by` INT NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    `deleted_by` INT NULL DEFAULT NULL,
    `deleted_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY(`id`),
    INDEX `master_sizechart_id`(`master_sizechart_id`),
    INDEX `category_id`(`category_id`)
) 

CREATE TABLE `newsletters`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `subject` VARCHAR(100) NULL,
    `content` LONGTEXT NULL,
    `notify_on` TIMESTAMP NULL,
    `created_by` INT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by` INT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY(`id`)
);

ALTER TABLE `newsletters` ADD `deleted_at` TIMESTAMP NULL DEFAULT NULL AFTER `updated_at`;
ALTER TABLE `notification_logs` CHANGE `visited_on` `visited_on` TIMESTAMP NULL DEFAULT NULL;

ALTER TABLE `sliders`  ADD `filter_params` JSON NULL  AFTER `slider_url`;
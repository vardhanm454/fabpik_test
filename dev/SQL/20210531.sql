ALTER TABLE `users` ADD `marketplace_password` VARCHAR(255) NULL AFTER `password`;

UPDATE users set marketplace_password=password where user_type='seller';
    CREATE TABLE `order_refund_log`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `child_order_id` INT NOT NULL,
    `amount` DOUBLE NOT NULL,
    `reason` VARCHAR(255) NOT NULL,
    `created_by` TINYINT NOT NULL,
    `created_at` TIMESTAMP NOT NULL,
    PRIMARY KEY(`id`)
) ENGINE = InnoDB;

ALTER TABLE `roles` ADD `deleted_at` TIMESTAMP NULL AFTER `updated_at`;

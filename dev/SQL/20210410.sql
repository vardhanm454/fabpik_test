

    ALTER TABLE `products` CHANGE `dress_material` `dress_material` VARCHAR(255) NULL DEFAULT NULL, CHANGE `washing_type_id` `washing_type_id` VARCHAR(500) NULL DEFAULT NULL, CHANGE `iron_type_id` `iron_type_id` VARCHAR(500) NULL DEFAULT NULL;

    ALTER TABLE `products` CHANGE `washing_type_id` `washing_type` VARCHAR(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL, CHANGE `iron_type_id` `iron_type` VARCHAR(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;


    ALTER TABLE `sellers` CHANGE `commission_type` `commission_type` ENUM('v','f','vf') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'v' COMMENT 'F=Fixed, V=Variable, VF=VariableFixed';

    ALTER TABLE `products` ADD `fabpik_seller_discount_percentage` DOUBLE NULL DEFAULT '0' AFTER `fabpik_seller_discount`;
    ALTER TABLE `product_variants` ADD `fabpik_seller_discount_percentage` DOUBLE NULL DEFAULT '0' AFTER `fabpik_seller_discount`
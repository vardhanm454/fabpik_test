ALTER TABLE `order_details` ADD `seller_invoice_no` INT(11) NULL AFTER `invoice_no`;
ALTER TABLE `order_details` ADD `seller_invoice_prefix` VARCHAR(100) NULL DEFAULT 'FAB' AFTER `invoice_no`;
ALTER TABLE `order_details` ADD `user_invoice_prefix` VARCHAR(5) NULL DEFAULT 'S' AFTER `child_order_id`;

ALTER TABLE `sellers` CHANGE `approval_status` `approval_status` TINYINT(1) NULL DEFAULT NULL;
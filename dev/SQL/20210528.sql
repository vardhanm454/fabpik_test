ALTER TABLE `sellers` ADD `min_order_amount` INT NULL DEFAULT NULL AFTER `phone`, ADD `convenience_fee` INT NULL DEFAULT NULL AFTER `min_order_amount`;

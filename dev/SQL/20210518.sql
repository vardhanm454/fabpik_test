CREATE TABLE `notification_logs`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `visited_on` TIMESTAMP NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`id`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_general_ci;

ALTER TABLE `notification_logs` ADD `updated_at` TIMESTAMP NOT NULL AFTER `created_at`;

CREATE TABLE `sliders` (
  `id` int NOT NULL,
  `title` varchar(211) DEFAULT NULL,
  `image_path` varchar(211) NOT NULL,
  `slider_url` varchar(211) NOT NULL,
  `display_order` int NOT NULL,
  `status` varchar(211) DEFAULT '1',
  `view` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=>desktop,1=>mobile',
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
);


ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

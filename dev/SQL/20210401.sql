UPDATE `users` SET `ref_id` = `seller_id`, `user_type` = 'seller' WHERE `seller_id` IS NOT NULL;
UPDATE `users` SET `ref_id` = `customer_id`, `user_type` = 'customer' WHERE `customer_id` IS NOT NULL;

ALTER TABLE `users`
  DROP `customer_id`,
  DROP `seller_id`;

ALTER TABLE `customers` ADD UNIQUE(`email`);
ALTER TABLE `customers` ADD UNIQUE(`mobile`);

ALTER TABLE `sellers` ADD UNIQUE(`seller_code`);
ALTER TABLE `sellers` ADD UNIQUE(`unique_id`);
ALTER TABLE `sellers` ADD UNIQUE(`email`);
ALTER TABLE `sellers` ADD UNIQUE(`mobile`);
ALTER TABLE `products` ADD `is_customized` TINYINT NOT NULL DEFAULT '0' AFTER `cancel_avl`;

ALTER TABLE `order_details` ADD `user_customization_text` varchar(500) DEFAULT NULL AFTER `cancelled_at`
ALTER TABLE `order_details`  ADD `courier_name` VARCHAR(255) NULL DEFAULT NULL  AFTER `awb_courier_id`;
ALTER TABLE `order_details` CHANGE `awb_courier_id` `courier_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE `order_details`  ADD `post_shipment_details` JSON NULL DEFAULT NULL  AFTER `manifest_url`;

CREATE TABLE `mobile_app_sliders` (
    `id` int(11) NOT NULL,
    `page` enum('h','c','o') DEFAULT NULL COMMENT 'h=Home,c=Category,o=Offers',
    `panel` enum('info','middle','bottom') DEFAULT NULL,
    `title` varchar(255) DEFAULT NULL,
    `image` varchar(255) DEFAULT NULL,
    `filter_params` json DEFAULT NULL,
    `created_by` int(11) DEFAULT NULL,
    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by` int(11) DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    `deleted_by` int(11) DEFAULT NULL,
    `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `mobile_app_sliders` ADD PRIMARY KEY (`id`);
ALTER TABLE `mobile_app_sliders` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
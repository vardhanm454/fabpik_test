-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 23, 2021 at 05:53 PM
-- Server version: 8.0.23-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fabpikall`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attr_type` enum('c','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'n' COMMENT 'c=Color,n=Normal',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `display_name`, `attr_type`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Color', 'Color', 'c', 1, '2020-12-23 06:22:25', '2021-01-27 21:24:19', NULL),
(2, 'Clothing size', 'Size', 'n', 1, '2020-12-23 06:24:39', '2020-12-24 00:29:17', NULL),
(3, 'Footwear size', 'Size', 'n', 1, '2020-12-23 06:25:04', '2020-12-24 00:52:18', NULL),
(4, 'Variables', 'Test', 'c', 1, '2020-12-24 00:41:31', '2020-12-24 00:49:01', '2020-12-24 00:49:01'),
(5, 'Quantity', 'Quantity', 'n', 1, '2020-12-24 02:46:36', '2021-02-06 04:19:21', NULL),
(6, 'Health Size', 'Size', 'n', 1, '2020-12-24 02:50:21', '2021-01-17 19:01:46', NULL),
(7, 'Furniture Sizes', 'Size', 'n', 1, '2020-12-24 05:08:57', '2020-12-24 05:12:33', '2020-12-24 05:12:33'),
(8, 'Furniture Sizes', 'Sizes', 'n', 1, '2020-12-24 05:13:31', '2020-12-24 05:13:31', NULL),
(9, 'Accessories', 'color', 'n', 1, '2021-01-10 23:20:39', '2021-01-19 22:23:22', '2021-01-19 22:23:22'),
(10, 'Test', 'Test MIraki', 'n', 1, '2021-01-17 19:14:07', '2021-01-17 19:14:23', '2021-01-17 19:14:23'),
(11, 'Languages', 'Books & Art', 'n', 1, '2021-01-21 21:07:08', '2021-01-21 21:08:23', NULL),
(12, 'Indoor & Outdoor Games', 'Sports & Fitness', 'n', 1, '2021-01-21 21:16:49', '2021-01-21 21:16:49', NULL),
(13, 'Pens, Books', 'School Supplies', 'n', 1, '2021-01-21 21:26:29', '2021-01-21 21:26:29', NULL),
(14, 'Multi Color', 'Multi Color', 'c', 1, '2021-01-27 19:03:52', '2021-01-27 19:03:52', NULL),
(15, 'Golden', 'Golden', 'c', 1, '2021-01-27 19:06:04', '2021-01-27 19:06:04', NULL),
(16, 'Silver', 'Silver', 'c', 1, '2021-01-27 19:07:24', '2021-01-27 19:08:59', '2021-01-27 19:08:59'),
(17, 'Age', 'Age', 'n', 1, '2021-02-05 13:45:01', '2021-02-05 13:45:01', NULL),
(18, 'Type', 'Type', 'n', 1, '2021-02-06 08:12:45', '2021-02-06 08:12:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_options`
--

CREATE TABLE `attribute_options` (
  `id` int NOT NULL,
  `attribute_id` int DEFAULT NULL,
  `option_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colour_code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_options`
--

INSERT INTO `attribute_options` (`id`, `attribute_id`, `option_name`, `colour_code`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Blue', '#1c22c9', 1, '2020-12-23 06:37:40', '2021-01-27 21:12:40', '2021-01-27 21:12:40'),
(2, 1, 'Black', '#000000', 1, '2020-12-23 06:37:54', '2021-01-27 21:12:38', '2021-01-27 21:12:38'),
(3, 1, 'Red', '#eb0e0e', 1, '2020-12-23 06:38:04', '2021-01-27 21:12:47', '2021-01-27 21:12:47'),
(4, 2, 'S', NULL, 1, '2020-12-23 06:38:31', '2021-01-27 21:37:25', '2021-01-27 21:37:25'),
(5, 2, 'M', NULL, 1, '2020-12-23 06:38:37', '2021-01-27 21:37:22', '2021-01-27 21:37:22'),
(6, 2, 'L', NULL, 1, '2020-12-23 06:38:41', '2021-01-27 21:37:20', '2021-01-27 21:37:20'),
(7, 2, 'XL', NULL, 1, '2020-12-23 06:38:45', '2021-01-27 21:37:26', '2021-01-27 21:37:26'),
(8, 2, 'XXL', NULL, 1, '2020-12-23 06:38:50', '2021-01-27 21:37:28', '2021-01-27 21:37:28'),
(9, 3, '5', NULL, 1, '2020-12-23 06:39:04', '2020-12-23 06:39:04', NULL),
(10, 3, '6', NULL, 1, '2020-12-23 06:39:11', '2020-12-23 06:39:11', NULL),
(11, 3, '7', NULL, 1, '2020-12-23 06:39:14', '2020-12-23 06:39:14', NULL),
(12, 3, '8', NULL, 1, '2020-12-23 06:39:17', '2020-12-23 06:39:17', NULL),
(13, 3, '9', NULL, 1, '2020-12-23 06:39:21', '2020-12-23 06:39:21', NULL),
(14, 3, '10', NULL, 1, '2020-12-23 06:39:25', '2020-12-23 06:39:25', NULL),
(15, 3, '11', NULL, 1, '2020-12-23 06:39:28', '2020-12-23 06:39:28', NULL),
(16, 3, '12', NULL, 1, '2020-12-23 06:39:31', '2020-12-23 06:39:31', NULL),
(17, 4, 'Red', '#bf2e2e', 1, '2020-12-24 00:42:28', '2020-12-24 00:42:28', NULL),
(18, 5, 'GM', NULL, 1, '2020-12-24 02:48:57', '2021-02-06 13:10:48', '2021-02-06 13:10:48'),
(19, 5, 'ML', NULL, 1, '2020-12-24 02:49:01', '2021-02-06 13:10:45', '2021-02-06 13:10:45'),
(20, 6, '1 PCK', NULL, 1, '2020-12-24 02:53:01', '2020-12-24 02:53:01', NULL),
(21, 6, '2 PCK', NULL, 1, '2020-12-24 02:53:20', '2020-12-24 02:53:20', NULL),
(22, 7, '2Inch', NULL, 1, '2020-12-24 05:09:08', '2020-12-24 05:11:49', NULL),
(23, 7, '3Inch', NULL, 1, '2020-12-24 05:09:11', '2020-12-24 05:12:03', NULL),
(24, 7, '3', NULL, 1, '2020-12-24 05:09:15', '2020-12-24 05:12:06', '2020-12-24 05:12:06'),
(25, 8, '2Inch', NULL, 1, '2020-12-24 05:13:41', '2020-12-24 05:13:41', NULL),
(26, 8, '5Inch', NULL, 1, '2020-12-24 05:13:46', '2020-12-24 05:13:46', NULL),
(27, 5, '5Inch', NULL, 1, '2020-12-24 05:28:57', '2020-12-24 05:28:57', NULL),
(28, 5, '10Inch', NULL, 1, '2020-12-24 05:29:01', '2020-12-24 05:29:01', NULL),
(29, 10, '1', NULL, 1, '2021-01-17 19:14:13', '2021-01-17 19:14:18', '2021-01-17 19:14:18'),
(30, 1, 'Coral', '#f06060', 1, '2021-01-20 22:11:33', '2021-01-27 21:12:42', '2021-01-27 21:12:42'),
(31, 1, 'Yellow', '#eded15', 1, '2021-01-20 22:12:03', '2021-01-27 21:12:51', '2021-01-27 21:12:51'),
(32, 1, 'White', '#fafafa', 1, '2021-01-20 22:12:23', '2021-01-27 21:12:48', '2021-01-27 21:12:48'),
(33, 1, 'Pink', '#e8259a', 1, '2021-01-20 22:12:46', '2021-01-27 21:12:44', '2021-01-27 21:12:44'),
(34, 2, '0-3 M', NULL, 1, '2021-01-20 22:14:00', '2021-01-27 21:37:08', '2021-01-27 21:37:08'),
(35, 2, '3-6 M', NULL, 1, '2021-01-20 22:14:10', '2021-01-27 21:37:15', '2021-01-27 21:37:15'),
(36, 2, '6-12 M', NULL, 1, '2021-01-20 22:14:22', '2021-01-27 21:37:18', '2021-01-27 21:37:18'),
(37, 2, '1-2 Y', NULL, 1, '2021-01-20 22:14:29', '2021-01-27 21:37:10', '2021-01-27 21:37:10'),
(38, 2, '2-3 Y', NULL, 1, '2021-01-20 22:15:28', '2021-01-27 21:37:13', '2021-01-27 21:37:13'),
(39, 11, 'Languages', NULL, 1, '2021-01-21 21:07:22', '2021-01-21 21:07:22', NULL),
(40, 16, 'red', '#9e4f4f', 1, '2021-01-27 19:07:33', '2021-01-27 19:07:33', NULL),
(41, 1, 'Blue', '#5757f0', 1, '2021-01-27 21:14:10', '2021-01-27 21:14:10', NULL),
(42, 1, 'Red', '#e62525', 1, '2021-01-27 21:14:36', '2021-01-27 21:14:36', NULL),
(43, 1, 'Grey', '#e0d5d5', 1, '2021-01-27 21:15:13', '2021-01-27 21:15:13', NULL),
(44, 1, 'Yellow', '#e8d017', 1, '2021-01-27 21:15:27', '2021-01-27 21:15:27', NULL),
(45, 1, 'Green', '#38c433', 1, '2021-01-27 21:15:52', '2021-01-27 21:15:52', NULL),
(46, 1, 'White', '#faf7f7', 1, '2021-01-27 21:16:09', '2021-01-27 21:16:09', NULL),
(47, 1, 'Black', '#0e0f0f', 1, '2021-01-27 21:16:22', '2021-02-01 08:23:05', NULL),
(48, 1, 'Orange', '#e68747', 1, '2021-01-27 21:16:55', '2021-01-27 21:16:55', NULL),
(49, 1, 'Brown', '#994545', 1, '2021-01-27 21:17:13', '2021-01-27 21:17:13', NULL),
(50, 1, 'Pink', '#d615c6', 1, '2021-01-27 21:17:23', '2021-01-27 21:17:23', NULL),
(51, 1, 'Maroon', '#c22727', 1, '2021-01-27 21:18:04', '2021-01-27 21:18:04', NULL),
(52, 1, 'Cream', '#e0db8f', 1, '2021-01-27 21:18:37', '2021-01-27 21:18:37', NULL),
(53, 1, 'MultiColor', '#7d963c', 1, '2021-01-27 21:19:01', '2021-02-10 07:19:28', '2021-02-10 07:19:28'),
(54, 1, 'Beige', '#e3d7d7', 1, '2021-01-27 21:20:25', '2021-01-27 21:20:25', NULL),
(55, 1, 'Peach', '#e3ae8b', 1, '2021-01-27 21:21:08', '2021-02-01 23:41:56', NULL),
(56, 1, 'Pruple', '#ae6fde', 1, '2021-01-27 21:21:47', '2021-02-03 02:15:41', NULL),
(57, 1, 'Violet', '#9010cc', 1, '2021-01-27 21:22:12', '2021-01-27 21:22:12', NULL),
(58, 1, 'Silver', '#bdb9b9', 1, '2021-01-27 21:22:33', '2021-01-27 21:22:33', NULL),
(59, 1, 'Golden', '#dbbc40', 1, '2021-01-27 21:23:00', '2021-01-27 21:23:00', NULL),
(60, 2, '0-3 M', NULL, 1, '2021-01-27 21:37:32', '2021-01-27 21:37:32', NULL),
(61, 2, '0-6 M', NULL, 1, '2021-01-27 21:37:39', '2021-01-27 21:37:39', NULL),
(62, 2, '1M', NULL, 1, '2021-01-27 21:37:47', '2021-01-27 21:37:47', NULL),
(63, 2, '2M', NULL, 1, '2021-01-27 21:37:55', '2021-01-27 21:37:55', NULL),
(64, 2, '3M', NULL, 1, '2021-01-27 21:38:02', '2021-01-27 21:38:02', NULL),
(65, 2, '3-6 M', NULL, 1, '2021-01-27 21:38:09', '2021-01-27 21:38:09', NULL),
(66, 2, '6M', NULL, 1, '2021-01-27 21:38:14', '2021-01-27 21:38:14', NULL),
(67, 2, '6-9 M', NULL, 1, '2021-01-27 21:38:22', '2021-01-27 21:38:22', NULL),
(68, 2, '6-12 M', NULL, 1, '2021-01-27 21:38:32', '2021-01-27 21:38:32', NULL),
(69, 2, '9M', NULL, 1, '2021-01-27 21:38:40', '2021-01-27 21:38:40', NULL),
(70, 2, '9-12 M', NULL, 1, '2021-01-27 21:38:47', '2021-01-27 21:38:47', NULL),
(71, 2, '12M', NULL, 1, '2021-01-27 21:38:54', '2021-01-27 21:38:54', NULL),
(72, 2, '12-15 M', NULL, 1, '2021-01-27 21:39:01', '2021-01-27 21:39:01', NULL),
(73, 2, '12-18 M', NULL, 1, '2021-01-27 21:39:11', '2021-01-27 21:39:11', NULL),
(74, 2, '15-18 M', NULL, 1, '2021-01-27 21:39:41', '2021-01-27 21:39:41', NULL),
(75, 2, '18M', NULL, 1, '2021-01-27 21:39:51', '2021-01-27 21:39:51', NULL),
(76, 2, '18-24 M', NULL, 1, '2021-01-27 21:39:58', '2021-01-27 21:39:58', NULL),
(77, 2, '24M', NULL, 1, '2021-01-27 21:40:04', '2021-01-27 21:40:04', NULL),
(78, 2, '0-3 Y', NULL, 1, '2021-01-27 21:40:09', '2021-01-27 21:40:09', NULL),
(79, 2, '1-2 Y', NULL, 1, '2021-01-27 21:40:19', '2021-01-27 21:40:19', NULL),
(80, 2, '2-3 Y', NULL, 1, '2021-01-27 21:40:24', '2021-01-27 21:40:24', NULL),
(81, 2, '3-4 Y', NULL, 1, '2021-01-27 21:40:29', '2021-01-27 21:40:29', NULL),
(82, 2, '4Y', NULL, 1, '2021-01-27 21:40:38', '2021-01-27 21:40:38', NULL),
(83, 2, '4-5 Y', NULL, 1, '2021-01-27 21:40:44', '2021-01-27 21:40:44', NULL),
(84, 2, '4-6 Y', NULL, 1, '2021-01-27 21:40:49', '2021-01-27 21:40:49', NULL),
(85, 2, '5Y', NULL, 1, '2021-01-27 21:40:55', '2021-01-27 21:40:55', NULL),
(86, 2, '5-6 Y', NULL, 1, '2021-01-27 21:41:00', '2021-01-27 21:41:00', NULL),
(87, 2, '6-7 Y', NULL, 1, '2021-01-27 21:41:06', '2021-01-27 21:41:06', NULL),
(88, 2, '6-8 Y', NULL, 1, '2021-01-27 21:41:11', '2021-01-27 21:41:11', NULL),
(89, 2, '7-8 Y', NULL, 1, '2021-01-27 21:41:17', '2021-01-27 21:41:17', NULL),
(90, 2, '8-9 Y', NULL, 1, '2021-01-27 21:41:22', '2021-01-27 21:41:22', NULL),
(91, 2, '8-10 Y', NULL, 1, '2021-01-27 21:41:27', '2021-01-27 21:41:27', NULL),
(92, 2, '9-10 Y', NULL, 1, '2021-01-27 21:41:32', '2021-01-27 21:41:32', NULL),
(93, 2, '10-11 Y', NULL, 1, '2021-01-27 21:41:40', '2021-01-27 21:41:40', NULL),
(94, 2, '11-12 Y', NULL, 1, '2021-01-27 21:41:48', '2021-01-27 21:41:48', NULL),
(95, 2, '12-13 Y', NULL, 1, '2021-01-27 21:41:54', '2021-01-27 21:41:54', NULL),
(96, 2, '13-14 Y', NULL, 1, '2021-01-27 21:42:00', '2021-01-27 21:42:00', NULL),
(97, 2, '14-15 Y', NULL, 1, '2021-01-27 21:42:05', '2021-01-27 21:42:05', NULL),
(98, 2, '15-16 Y', NULL, 1, '2021-01-27 21:42:11', '2021-01-27 21:42:11', NULL),
(99, 1, 'Offwhite', '#cfc4bf', 1, '2021-02-01 23:38:48', '2021-02-04 02:17:20', NULL),
(100, 1, 'new color miraki', '#752828', 1, '2021-02-01 23:51:10', '2021-02-01 23:51:34', '2021-02-01 23:51:34'),
(101, 1, 'Coral', '#e67e55', 1, '2021-02-02 04:02:03', '2021-02-02 04:02:03', NULL),
(102, 1, 'Multi Color', '#78376b', 1, '2021-02-03 02:15:25', '2021-02-03 02:15:25', NULL),
(103, 1, 'Purple', '#7b2ebf', 1, '2021-02-05 01:23:12', '2021-02-05 01:23:12', NULL),
(104, 2, '80x80', NULL, 1, '2021-02-05 06:21:11', '2021-02-05 06:21:11', NULL),
(105, 2, '100x100', NULL, 1, '2021-02-05 06:21:19', '2021-02-05 06:21:19', NULL),
(106, 2, '120x120', NULL, 1, '2021-02-05 06:21:26', '2021-02-05 06:21:26', NULL),
(107, 2, '120x60', NULL, 1, '2021-02-05 06:21:32', '2021-02-05 06:21:32', NULL),
(108, 17, '0+ Yrs', NULL, 1, '2021-02-05 13:45:19', '2021-02-05 13:45:19', NULL),
(109, 17, '1+ Yrs', NULL, 1, '2021-02-05 13:45:54', '2021-02-05 13:45:54', NULL),
(110, 17, '2+ Yrs', NULL, 1, '2021-02-05 13:46:02', '2021-02-06 07:55:13', NULL),
(111, 17, '3+ Yrs', NULL, 1, '2021-02-05 13:46:08', '2021-02-06 07:55:18', NULL),
(112, 17, '4+ Yrs', NULL, 1, '2021-02-05 13:46:12', '2021-02-06 07:55:22', NULL),
(113, 17, '5+ Yrs', NULL, 1, '2021-02-05 13:46:20', '2021-02-06 07:55:25', NULL),
(114, 17, '6+ Yrs', NULL, 1, '2021-02-05 13:46:24', '2021-02-06 07:55:49', NULL),
(115, 17, '7+ Yrs', NULL, 1, '2021-02-05 13:46:29', '2021-02-06 07:55:34', NULL),
(116, 17, '8+ Yrs', NULL, 1, '2021-02-05 13:46:34', '2021-02-06 07:55:38', NULL),
(117, 17, '9+ Yrs', NULL, 1, '2021-02-05 13:46:45', '2021-02-05 13:46:45', NULL),
(118, 17, '10+ Yrs', NULL, 1, '2021-02-05 13:46:51', '2021-02-05 13:46:51', NULL),
(119, 2, '2-4 Y', NULL, 1, '2021-02-05 19:26:25', '2021-02-05 19:26:25', NULL),
(120, 5, '280 g', NULL, 1, '2021-02-06 04:20:04', '2021-02-06 04:25:26', NULL),
(121, 5, '350 g', NULL, 1, '2021-02-06 04:20:15', '2021-02-06 04:25:32', NULL),
(122, 5, '60 g', NULL, 1, '2021-02-06 04:20:27', '2021-02-06 04:25:44', NULL),
(123, 5, '75 g', NULL, 1, '2021-02-06 04:20:36', '2021-02-06 04:26:00', NULL),
(124, 5, '100 ml', NULL, 1, '2021-02-06 04:20:47', '2021-02-06 04:20:47', NULL),
(125, 5, '1400 g', NULL, 1, '2021-02-06 04:20:58', '2021-02-06 04:24:57', NULL),
(126, 5, '840 g', NULL, 1, '2021-02-06 04:21:04', '2021-02-06 04:25:50', NULL),
(127, 5, '560 g', NULL, 1, '2021-02-06 04:21:09', '2021-02-06 04:25:38', NULL),
(128, 5, '1960 g', NULL, 1, '2021-02-06 04:21:14', '2021-02-06 04:25:07', NULL),
(129, 5, '11100 g', NULL, 1, '2021-02-06 04:21:53', '2021-02-06 04:24:50', '2021-02-06 04:24:50'),
(130, 5, '1120 g', NULL, 1, '2021-02-06 04:26:17', '2021-02-06 04:26:17', NULL),
(131, 18, 'Drawing Books', NULL, 1, '2021-02-06 08:13:07', '2021-02-06 08:32:21', '2021-02-06 08:32:21'),
(132, 18, 'Art & Crafts', NULL, 1, '2021-02-06 08:13:18', '2021-02-06 08:32:14', '2021-02-06 08:32:14'),
(133, 18, 'Reusable', NULL, 1, '2021-02-06 08:32:43', '2021-02-06 08:32:43', NULL),
(134, 18, 'Non Reusable', NULL, 1, '2021-02-06 08:32:58', '2021-02-06 08:32:58', NULL),
(135, 5, '200 g', NULL, 1, '2021-02-06 13:29:33', '2021-02-06 13:44:34', NULL),
(136, 5, '150 ml', NULL, 1, '2021-02-06 13:30:41', '2021-02-06 13:30:41', NULL),
(137, 5, '30 ml', NULL, 1, '2021-02-06 13:31:20', '2021-02-06 13:31:20', NULL),
(138, 5, '250 ml', NULL, 1, '2021-02-06 13:33:23', '2021-02-06 13:33:23', NULL),
(139, 5, '500 ml', NULL, 1, '2021-02-06 13:33:50', '2021-02-06 13:33:50', NULL),
(140, 2, '0-12 M', NULL, 1, '2021-02-06 15:03:35', '2021-02-06 15:03:35', NULL),
(141, 17, '3-9 Yrs', NULL, 1, '2021-02-06 16:26:41', '2021-02-06 16:26:41', NULL),
(142, 17, '6-8 Yrs', NULL, 1, '2021-02-06 16:26:51', '2021-02-06 16:26:51', NULL),
(143, 17, '4-9 Yrs', NULL, 1, '2021-02-06 16:26:56', '2021-02-06 16:26:56', NULL),
(144, 17, '3-5 Yrs', NULL, 1, '2021-02-06 16:27:03', '2021-02-06 16:27:03', NULL),
(145, 2, 'NB', NULL, 1, '2021-02-06 17:09:45', '2021-02-06 17:09:45', NULL),
(146, 2, '0-2 Y', NULL, 1, '2021-02-06 17:32:17', '2021-02-15 10:54:18', NULL),
(147, 5, 'Pack of 7', NULL, 1, '2021-02-08 14:11:47', '2021-02-08 14:11:47', NULL),
(148, 18, 'Pack of 1', NULL, 1, '2021-02-11 01:46:46', '2021-02-11 01:46:46', NULL),
(149, 18, 'Pack of 2', NULL, 1, '2021-02-11 01:46:52', '2021-02-11 01:46:52', NULL),
(150, 18, 'Pack of 3', NULL, 1, '2021-02-11 01:46:56', '2021-02-11 01:46:56', NULL),
(151, 18, 'Pack of 4', NULL, 1, '2021-02-11 01:47:00', '2021-02-11 01:47:00', NULL),
(152, 18, 'Pack of 5', NULL, 1, '2021-02-11 01:47:05', '2021-02-11 01:47:05', NULL),
(153, 18, 'Pack of 6', NULL, 1, '2021-02-11 01:47:09', '2021-02-11 01:47:09', NULL),
(154, 18, 'Pack of 7', NULL, 1, '2021-02-11 01:47:13', '2021-02-11 01:47:13', NULL),
(155, 18, 'Pack of 8', NULL, 1, '2021-02-11 01:47:16', '2021-02-11 01:47:16', NULL),
(156, 18, 'Pack of 9', NULL, 1, '2021-02-11 01:47:20', '2021-02-11 01:47:20', NULL),
(157, 18, 'Pack of 10', NULL, 1, '2021-02-11 01:47:24', '2021-02-11 01:47:24', NULL),
(158, 18, 'Pack of 24', NULL, 1, '2021-02-11 01:47:50', '2021-02-11 01:47:50', NULL),
(159, 18, 'Pack of 12', NULL, 1, '2021-02-11 01:48:03', '2021-02-11 01:48:03', NULL),
(160, 5, 'Pack of 1', NULL, 1, '2021-02-12 06:03:38', '2021-02-12 06:03:47', '2021-02-12 06:03:47'),
(161, 5, 'Pack of 1', NULL, 1, '2021-02-12 06:04:27', '2021-02-12 06:04:27', NULL),
(162, 5, 'Pack of 2', NULL, 1, '2021-02-12 06:04:32', '2021-02-12 06:04:32', NULL),
(163, 5, 'Pack of 3', NULL, 1, '2021-02-12 06:04:36', '2021-02-12 06:04:36', NULL),
(164, 5, 'Pack of 4', NULL, 1, '2021-02-12 06:04:40', '2021-02-12 06:04:40', NULL),
(165, 5, 'Pack of 5', NULL, 1, '2021-02-12 06:04:44', '2021-02-12 06:04:44', NULL),
(166, 5, 'Pack of 6', NULL, 1, '2021-02-12 06:04:48', '2021-02-12 06:04:48', NULL),
(167, 5, 'Pack of 8', NULL, 1, '2021-02-12 06:04:51', '2021-02-12 06:04:51', NULL),
(168, 5, 'Pack of 9', NULL, 1, '2021-02-12 06:04:55', '2021-02-12 06:04:55', NULL),
(169, 5, 'Pack of 10', NULL, 1, '2021-02-12 06:04:59', '2021-02-12 06:04:59', NULL),
(170, 3, '6-12 M', NULL, 1, '2021-02-17 08:20:12', '2021-02-17 08:20:12', NULL),
(171, 3, '3-6 M', NULL, 1, '2021-02-17 08:20:20', '2021-02-17 08:20:20', NULL),
(172, 3, '12-18 M', NULL, 1, '2021-02-17 08:20:31', '2021-02-17 08:20:31', NULL),
(173, 3, '0-6 M', NULL, 1, '2021-02-18 00:53:12', '2021-02-18 00:53:12', NULL),
(174, 3, '6-9 M', NULL, 1, '2021-02-18 00:53:59', '2021-02-18 00:53:59', NULL),
(175, 3, '9-12 M', NULL, 1, '2021-02-18 00:54:24', '2021-02-18 00:54:24', NULL),
(176, 3, '0-3 M', NULL, 1, '2021-02-18 00:56:06', '2021-02-18 00:56:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_on_home` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int DEFAULT NULL,
  `meta_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `slug`, `image`, `show_on_home`, `created_by`, `meta_title`, `meta_keywords`, `meta_description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Freehugs', 'freehugs', 'avatar1ed07a7cc006467ba75b3c83de2a9ff3.jpg', 1, NULL, 'FreeHugs', 'Free Hug, Children Clothing, Kids Wear, Party Wear', 'FreeHugs is premium brand for kids apparel.', 1, '2020-12-23 06:28:19', '2021-03-05 05:49:20', NULL),
(2, 'Babyoye', 'babyoye', 'avatar01e9f2af1af64d1c04b3cb60b1c3d1d6.jpg', 1, NULL, 'Babyoye Meta Title', 'Babyoye Meta Keywords', 'Babyoye Meta Description', 1, '2020-12-23 06:29:15', '2021-02-02 00:30:13', '2021-02-02 00:30:13'),
(3, 'Carter\'s', 'carters', 'avatar0e2393f36c2be3a7109bc9e76c3cc1c3.jpg', 1, NULL, 'Carter\'s Meta Title', 'Carter\'s Meta Keywords', 'Carter\'s Meta Description', 1, '2020-12-23 06:29:59', '2021-02-02 00:30:13', '2021-02-02 00:30:13'),
(4, 'Kookie Kids', 'kookie-kids', 'avatar83238cd46ebccb0ee4dcbad4d411f14e.jpg', 1, NULL, 'Kookie Kids Meta Title', 'Kookie Kids Meta Keywords', 'Kookie Kids Meta Description', 1, '2020-12-23 06:31:17', '2021-02-02 00:30:13', '2021-02-02 00:30:13'),
(5, 'Mark & Mia', 'mark-mia', 'avatarae541f57742c27f456fa0683b457d1c8.jpg', 1, NULL, 'Mark & Mia Meta Title', 'Mark & Mia Meta Keywords', 'Mark & Mia Meta Description', 1, '2020-12-23 06:32:22', '2021-02-02 00:29:56', '2021-02-02 00:29:56'),
(6, 'Aurelia', 'aurelia', 'avatard0b4966c7b5d794e935f3211aa66a234.jpg', 1, NULL, 'Meta Title', NULL, NULL, 1, '2021-01-17 18:51:11', '2021-02-02 00:30:13', '2021-02-02 00:30:13'),
(7, 'NEw Brand', 'new-brand', '1610016825.jpg', 1, NULL, 'Test', 'Test', 'Test', 1, '2021-01-19 20:14:11', '2021-01-19 20:14:17', '2021-01-19 20:14:17'),
(8, 'Keebee Organics', 'keebee-organics', 'avatar0f222924635340fca7401e822d2df416.jpg', 1, NULL, NULL, NULL, 'Keebee Organics', 1, '2021-01-20 21:46:37', '2021-02-02 00:30:13', '2021-02-02 00:30:13'),
(9, 'test', 'test', '1611146634.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-01-27 22:42:12', '2021-02-01 08:05:01', '2021-02-01 08:05:01'),
(10, 'Parrot Crow', 'parrot-crow', 'avatar0faa25e69991694021fffc3daab569b5.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-01 01:52:44', '2021-03-05 05:49:20', NULL),
(11, 'Pikaboo', 'pikaboo', 'avatar0f222924635340fca7401e822d2df416.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-01 04:58:27', '2021-03-05 05:49:20', NULL),
(12, 'Saka', 'saka', 'avatar0f222924635340fca7401e822d2df416.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-02 00:32:17', '2021-03-05 05:49:20', NULL),
(13, 'Baby Aroosa', 'baby-aroosa', 'avatar02753e23ba80e0c3b5932a2b3148fcaa.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-02 00:34:32', '2021-03-05 05:49:20', NULL),
(14, 'Neenee', 'neenee', 'avatar01e41a921550194822eaa8bdcacdbca1.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-03 02:06:48', '2021-03-05 05:49:20', NULL),
(15, 'The Pony & Peony', 'the-pony-peony', 'avatar2f18f2bb5b550666445f9c9291526c36.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-04 02:13:04', '2021-03-05 05:49:20', NULL),
(16, 'Keebee Organics', 'keebee-organics', 'avatar06e9b2cb45589b5089a296735824d751.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-05 06:24:20', '2021-03-05 05:49:20', NULL),
(17, 'Whitehenz', 'whitehenz', 'avatar173fb6428b7ebe35635dca24df4bc16a.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-05 12:43:47', '2021-03-05 05:49:20', NULL),
(18, 'This and That by Vedika', 'this-and-that-by-vedika', 'avatar01e41a921550194822eaa8bdcacdbca1.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-05 13:44:07', '2021-03-05 05:49:20', NULL),
(19, 'Giggleo', 'giggleo', 'avatarc9843cf841874cc3ae3918be42b53b57.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-05 19:25:35', '2021-03-05 05:49:20', NULL),
(20, 'Bygrandma', 'bygrandma', 'avatar22f3781f635ea58bf39915a477a5c655.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-06 04:50:31', '2021-03-05 05:49:20', NULL),
(21, 'Inkmeo', 'inkmeo', 'avatar3732059cf168b1339ab7b9c632fbff9c.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-06 07:09:01', '2021-03-05 05:49:20', NULL),
(22, 'Arirotoys', 'ariro-toys', 'avatar318b295d944154347240b69fd0c5934e.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-06 09:43:49', '2021-03-05 05:49:20', NULL),
(23, 'Aarshaveda Wellness', 'aarshaveda-wellness', 'avatar1daa04ffaa456526c940de9f7901a3c1.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-06 13:13:22', '2021-03-05 05:49:20', NULL),
(24, 'Brandonn Fashion', 'brandonn-fashion', 'avatar2d64ac38ca742584af41d9d8fe06adb9.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-06 14:51:44', '2021-03-05 05:49:20', NULL),
(25, 'Toiing', 'toiing', 'avatar2d0410d63ba67bc17812919ec8bc7fc9.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-06 16:23:03', '2021-03-05 05:49:20', NULL),
(26, 'Whitewaterkids', 'whitewater-kids', 'avatar2ab29042236727bf5b14bb40486bb750.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-06 16:23:24', '2021-03-05 05:49:20', NULL),
(27, 'Desi Toys', 'desi-toys', 'avatar318b295d944154347240b69fd0c5934e.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-06 20:05:39', '2021-03-05 05:49:20', NULL),
(28, 'Halfscale', 'halfscale', 'avatar4b15ec50ed80f3d79b347cb17ddf2471.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-08 10:53:49', '2021-03-05 05:49:20', NULL),
(29, 'DGN Traders', 'dgn-traders', 'avatar22f3781f635ea58bf39915a477a5c655.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-08 13:33:07', '2021-03-05 05:49:20', NULL),
(30, 'Rabitat', 'rabitat', 'avatar2e3b63d44c8f126317da2d3a0dc8ce13.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-08 18:06:30', '2021-03-05 05:49:20', NULL),
(31, 'Elfin House', 'elfin-house', 'avatar2e3b63d44c8f126317da2d3a0dc8ce13.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-09 13:07:23', '2021-03-05 05:49:20', NULL),
(32, 'Budding Bees', 'budding-bees', '100000/products/1611142601.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-09 14:50:28', '2021-03-05 05:49:20', NULL),
(33, 'Smily Kiddos', 'smily-kiddos', 'avatar173fb6428b7ebe35635dca24df4bc16a.jpg', 1, NULL, NULL, NULL, NULL, 1, '2021-02-11 01:35:50', '2021-03-05 05:49:20', NULL),
(34, 'M3zone', 'm3zone', '2.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-12 04:30:34', '2021-03-05 05:49:20', NULL),
(35, 'Fairies Forever', 'fairies-forever', 'avatar01e41a921550194822eaa8bdcacdbca1.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-12 05:10:50', '2021-03-05 05:49:20', NULL),
(36, 'Tiny Toons', 'tiny-toon', 'avatar56303c5de3ea71d3c261065888c0200c.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-15 01:29:31', '2021-03-05 05:49:20', NULL),
(37, 'XYZA', 'xyza', 'avatar02753e23ba80e0c3b5932a2b3148fcaa.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-19 00:21:18', '2021-02-24 00:01:49', '2021-02-24 00:01:49'),
(38, 'Tipy Tipy Tap', 'tipy-tipy-tap', 'avatarf9c3d828455a50b781888c3c096bda2d.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-02-24 00:28:57', '2021-03-05 05:49:20', NULL),
(39, 'Solly & Dolly', 'solly-dolly', 'avatar01e9f2af1af64d1c04b3cb60b1c3d1d6.jpg', 0, NULL, NULL, NULL, NULL, 1, '2021-03-01 04:06:00', '2021-03-05 05:49:20', NULL),
(40, 'Go Bees', 'go-bees', NULL, 0, 47, NULL, NULL, NULL, 1, '2021-03-01 08:46:59', '2021-03-01 08:46:59', NULL),
(41, 'Oscar Home', 'oscar-home', NULL, 0, 48, NULL, NULL, NULL, 1, '2021-03-02 04:04:54', '2021-03-02 04:04:54', NULL),
(42, 'Toothless', 'toothless', NULL, 0, 49, NULL, NULL, NULL, 1, '2021-03-03 07:35:35', '2021-03-03 07:35:35', NULL),
(43, 'Kooka Kids', 'kooka-kids', NULL, 0, 50, NULL, NULL, NULL, 1, '2021-03-03 08:15:11', '2021-03-03 08:15:29', NULL),
(44, 'ahkrishna', 'ahk', NULL, 0, 13, NULL, NULL, NULL, 1, '2021-03-05 12:53:55', '2021-03-17 10:22:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int NOT NULL,
  `customer_id` int DEFAULT NULL,
  `seller_id` int DEFAULT NULL,
  `product_id` int NOT NULL,
  `product_variant_id` int DEFAULT NULL,
  `quantity` int NOT NULL DEFAULT '1',
  `mrp` int DEFAULT NULL,
  `price` double NOT NULL DEFAULT '0',
  `seller_discount` double NOT NULL DEFAULT '0',
  `total_seller_discount` int DEFAULT NULL,
  `coupon_id` int DEFAULT NULL,
  `coupon_discount` double NOT NULL DEFAULT '0',
  `total_price` double NOT NULL DEFAULT '0',
  `seller_pincode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `etd` timestamp NULL DEFAULT NULL,
  `coupon_type` enum('f','s') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'f=Fabpik,s=Seller',
  `customer_pincode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `customer_id`, `seller_id`, `product_id`, `product_variant_id`, `quantity`, `mrp`, `price`, `seller_discount`, `total_seller_discount`, `coupon_id`, `coupon_discount`, `total_price`, `seller_pincode`, `etd`, `coupon_type`, `customer_pincode`, `created_at`, `updated_at`) VALUES
(3, 9, 3, 17, 50, 1, 350, 350, 0, 0, NULL, 0, 350, '515511', '2021-01-26 18:30:00', NULL, '507002', '2021-01-21 07:04:07', '2021-01-22 01:20:17'),
(4, 4, 3, 19, 54, 3, 425, 425, 0, 0, NULL, 0, 1275, '515511', '2021-01-27 18:30:00', NULL, NULL, '2021-01-21 08:59:26', '2021-01-23 05:56:56'),
(51, 7, 3, 26, 64, 1, 1200, 1100, 100, 100, NULL, 0, 1100, '515511', NULL, NULL, '515511', '2021-01-28 06:15:30', '2021-01-28 06:32:33'),
(52, 7, 3, 16, 49, 1, 350, 350, 0, 0, NULL, 0, 350, '515511', NULL, NULL, '515511', '2021-01-28 07:55:45', '2021-01-28 07:55:45'),
(53, 7, 3, 16, 48, 1, 350, 350, 0, 0, NULL, 0, 350, '515511', NULL, NULL, '515511', '2021-01-28 11:33:37', '2021-01-28 11:33:37'),
(60, NULL, 13, 42, 76, 1, 200, 190, 10, 10, NULL, 0, 190, '500085', NULL, NULL, NULL, '2021-01-29 06:33:16', '2021-01-29 08:07:19'),
(69, 7, 13, 42, 76, 1, 200, 190, 10, 10, NULL, 0, 190, '500085', NULL, NULL, '533105', '2021-01-30 05:23:24', '2021-01-30 05:23:42'),
(78, 12, 13, 42, 76, 1, 200, 190, 10, 10, NULL, 0, 190, '500085', NULL, NULL, '500085', '2021-01-31 23:10:13', '2021-01-31 23:10:13'),
(79, 12, 13, 42, 77, 1, 500, 400, 100, 100, NULL, 0, 400, '500085', NULL, NULL, '500085', '2021-01-31 23:10:17', '2021-01-31 23:10:17');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int UNSIGNED NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `commission` double NOT NULL DEFAULT '0',
  `tax` double NOT NULL DEFAULT '0',
  `primary_attr` int DEFAULT NULL,
  `primary_attribute` int DEFAULT NULL,
  `secondary_attribute` int DEFAULT NULL,
  `priority` int DEFAULT NULL,
  `meta_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `featured` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `slug`, `icon`, `image`, `description`, `commission`, `tax`, `primary_attr`, `primary_attribute`, `secondary_attribute`, `priority`, `meta_title`, `meta_keywords`, `meta_description`, `status`, `featured`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Clothing', 'clothing', 'category/1611302161.png', 'category/1611302161.png', 'Test', 18, 10, NULL, 1, 2, 1, NULL, NULL, NULL, 1, 0, '2020-12-23 01:01:05', '2021-03-03 07:42:40', NULL),
(2, 'Footwear', 'footwear', 'category/1611302379.png', 'category/1611302379.png', 'Test', 10, 12, NULL, 1, 3, 2, 'Test', 'Test', 'Test', 1, 0, '2020-12-23 01:04:13', '2021-03-03 07:42:40', NULL),
(3, 'Toys', 'toys', 'category/1611302305.png', 'category/1611302305.png', 'Test', 10, 10, NULL, 1, 17, 3, 'Test', 'Test', 'Test', 1, 0, '2020-12-23 01:06:33', '2021-03-03 07:42:40', NULL),
(4, 'test', 'test', 'avatar48c8d0140699264a5e14838304d038c7.jpg', 'avatar01e9f2af1af64d1c04b3cb60b1c3d1d6.jpg', 'test', 10, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-12-23 19:15:18', '2020-12-23 19:18:17', '2020-12-23 19:18:17'),
(5, 'Health & Safety', 'health-safety', 'category/1611303134.png', 'category/1611303134.png', 'MEDICAL CARE', 10, 12, NULL, 6, NULL, 4, NULL, NULL, NULL, 1, 0, '2020-12-23 22:27:57', '2021-03-03 07:42:40', NULL),
(6, 'Nursery', 'nursery', '1610014070.png', '1610014070.png', 'Personal Care', 11, 10, NULL, 8, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-12-23 23:29:15', '2021-01-21 21:34:48', '2021-01-21 21:34:48'),
(7, 'Personal Care', 'personal-care', 'category/1611303071.png', 'category/1611303071.png', 'Personal Care', 10, 11, NULL, 5, 17, 5, NULL, NULL, NULL, 1, 0, '2020-12-23 23:55:09', '2021-03-03 07:42:40', NULL),
(8, 'Food & Nutrition', 'food-nutrition', 'category/1611302596.png', 'category/1611302596.png', 'Foods', 10, 12, NULL, 5, NULL, 6, NULL, NULL, NULL, 1, 0, '2020-12-25 23:18:00', '2021-03-03 07:42:40', NULL),
(9, 'Clothing1', 'clothing1', 'avatar1ed07a7cc006467ba75b3c83de2a9ff3.jpg', '1610016023.jpg', 'rtest', 10, 10, NULL, 2, 1, NULL, NULL, NULL, NULL, 1, 0, '2021-01-07 22:48:05', '2021-01-07 22:50:38', '2021-01-07 22:50:38'),
(10, 'Accessories', 'accessories', 'category/1611302453.png', 'category/1611302453.png', 'TEST', 10, 5, NULL, 1, 17, 7, 'Accessories Title', 'Accessories Keywords', 'Accessories Description', 1, 0, '2021-01-10 23:12:49', '2021-03-03 07:42:40', NULL),
(11, 'Boutiques', 'boutiques', 'avatara21133b6e99cb12952ef1a137be563cd.jpg', 'avatara21133b6e99cb12952ef1a137be563cd.jpg', 'Description', 10, 10, NULL, 6, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-01-17 19:02:53', '2021-01-19 22:34:47', '2021-01-19 22:34:47'),
(12, 'New & Category', 'new-category', '1610016023.jpg', '1610014964.jpg', 'Test', 10, 8, NULL, 2, 1, NULL, NULL, NULL, NULL, 1, 0, '2021-01-17 21:00:20', '2021-01-17 21:00:27', '2021-01-17 21:00:27'),
(13, 'Books & Art', 'books-art', 'category/1611309564.jpg', 'category/1611309564.jpg', 'Books & Art', 10, 10, NULL, 18, 17, 8, NULL, NULL, NULL, 1, 0, '2021-01-21 21:10:05', '2021-03-03 07:42:40', NULL),
(14, 'Sports', 'sports', 'category/1611303465.png', 'category/1611303465.png', 'Sports & Fitness', 10, 10, NULL, 12, NULL, 9, NULL, NULL, NULL, 1, 0, '2021-01-21 21:18:15', '2021-03-03 07:42:40', NULL),
(15, 'School Supplies', 'school-supplies', 'category/1611302972.png', 'category/1611302972.png', 'School Supplies', 10, 10, NULL, 13, NULL, NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 22:27:44', '2021-01-21 23:32:41', NULL),
(16, 'Organic', 'organic', 'category/1611308163.png', 'category/1611308163.png', 'Organic', 10, 10, NULL, 6, NULL, 10, NULL, NULL, NULL, 1, 0, '2021-01-21 22:36:27', '2021-03-03 07:42:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `childcategories`
--

CREATE TABLE `childcategories` (
  `id` int NOT NULL,
  `category_id` int DEFAULT NULL,
  `subcategory_id` int DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `childcategories`
--

INSERT INTO `childcategories` (`id`, `category_id`, `subcategory_id`, `title`, `slug`, `icon`, `image`, `description`, `meta_title`, `meta_keywords`, `meta_description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 4, 'Soft toys', 'soft-toys', 'childcategory/1611314174.jpg', 'childcategory/1611314174.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-19 23:35:52', '2021-01-22 00:16:39', NULL),
(2, 3, 4, 'Puzzle games', 'puzzle-games', 'childcategory/1611314225.jpg', 'childcategory/1611314225.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-19 23:38:45', '2021-01-22 00:17:26', NULL),
(3, 3, 4, 'Rattlers & Teethers', 'rattlers-teethers', 'childcategory/1611314302.jpg', 'childcategory/1611314302.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-19 23:44:40', '2021-02-06 10:00:44', NULL),
(4, 3, 4, 'Outdoor games', 'outdoor-games', 'childcategory/1611314506.jpg', 'childcategory/1611314506.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-19 23:48:34', '2021-01-22 00:22:05', NULL),
(5, 1, 1, 'Clothing Sets & Suits', 'clothing-sets-suits', 'childcategory/1611294643.jpg', 'childcategory/1611294643.jpg', 'Clothing Sets & Suits', NULL, NULL, NULL, 1, '2021-01-19 23:53:18', '2021-01-21 18:51:07', NULL),
(6, 3, 4, 'Pretend Play sets', 'pretend-play-sets', 'childcategory/1611314555.jpg', 'childcategory/1611314555.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-19 23:55:24', '2021-01-22 00:22:55', NULL),
(7, 3, 4, 'Bathtime fun', 'bathtime-fun', 'childcategory/1611314596.jpg', 'childcategory/1611314596.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 00:27:38', '2021-01-22 00:23:36', NULL),
(8, 3, 4, 'Drawing,Art&Crafts', 'drawingartcrafts', 'childcategory/1611314657.jpg', 'childcategory/1611314657.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 00:33:54', '2021-01-27 19:51:42', '2021-01-27 19:51:42'),
(9, 3, 4, 'Wheels, Rides and Scooters', 'wheels-rides-and-scooters', 'childcategory/1611314696.jpg', 'childcategory/1611314696.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 00:36:16', '2021-02-12 04:56:45', NULL),
(10, 3, 4, 'Fun toys', 'fun-toys', 'childcategory/1611314745.jpg', 'childcategory/1611314745.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 00:37:53', '2021-01-22 00:26:04', NULL),
(11, 3, 4, 'Card and Board Games', 'card-and-board-games', 'childcategory/1611314792.jpg', 'childcategory/1611314792.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 00:40:40', '2021-02-11 02:36:18', NULL),
(12, 3, 4, 'Educational and IQ', 'educational-and-iq', 'childcategory/1611314848.jpg', 'childcategory/1611314848.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 00:44:19', '2021-02-12 05:51:02', NULL),
(13, 3, 4, 'Music & Electronic Fun', 'music-electronic-fun', 'childcategory/1611314900.jpg', 'childcategory/1611314900.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 00:46:16', '2021-01-22 01:05:25', NULL),
(14, 3, 4, 'Blocks Games', 'blocks-games', 'childcategory/1611315045.jpg', 'childcategory/1611315045.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 00:53:38', '2021-01-22 00:31:09', NULL),
(15, 1, 1, 'Onesies & Rompers', 'onesies-rompers', 'subcategory/1611292676.jpg', 'subcategory/1611292759.jpg', 'Onesies & Rompers', NULL, NULL, NULL, 1, '2021-01-20 00:55:41', '2021-01-21 18:19:40', NULL),
(16, 3, 4, 'Combos', 'combos', 'childcategory/1611315090.jpg', 'childcategory/1611315090.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 00:55:49', '2021-01-22 00:32:14', NULL),
(17, 1, 1, 'T-shirts', 't-shirts', 'subcategory/1611292805.jpg', 'subcategory/1611292805.jpg', 'T-shirts', NULL, NULL, NULL, 1, '2021-01-20 00:57:57', '2021-01-21 18:20:32', NULL),
(18, 1, 1, 'Shirts', 'shirts', 'subcategory/1611292867.jpg', 'subcategory/1611292867.jpg', 'Shirts', NULL, NULL, NULL, 1, '2021-01-20 01:00:42', '2021-01-21 18:21:30', NULL),
(19, 3, 7, 'Educational and IQ', 'educational-and-iq', 'childcategory/1611317476.jpg', 'childcategory/1611317476.jpg', 'LEARNING', NULL, NULL, NULL, 1, '2021-01-20 01:02:42', '2021-01-27 21:04:16', '2021-01-27 21:04:16'),
(20, 3, 7, 'Blocks Games', 'blocks-games', 'childcategory/1611317518.jpg', 'childcategory/1611317518.jpg', 'LEARNING', NULL, NULL, NULL, 1, '2021-01-20 01:06:56', '2021-01-27 21:04:16', '2021-01-27 21:04:16'),
(21, 3, 7, 'Cards&Board Games', 'cardsboard-games', 'childcategory/1611317593.jpg', 'childcategory/1611317593.jpg', 'LEARNING', NULL, NULL, NULL, 1, '2021-01-20 01:08:06', '2021-01-27 21:04:16', '2021-01-27 21:04:16'),
(22, 1, 1, 'Jackets, Sweaters & Sweat Shirts', 'jackets-sweaters-sweat-shirts', 'childcategory/1611293083.jpg', 'childcategory/1611293083.jpg', 'Jackets, Sweaters & Sweat Shirts', NULL, NULL, NULL, 1, '2021-01-20 01:08:08', '2021-02-02 03:58:44', NULL),
(23, 1, 1, 'Shorts', 'shorts', 'childcategory/1611293161.jpg', 'childcategory/1611293161.jpg', 'Shorts', NULL, NULL, NULL, 1, '2021-01-20 01:09:48', '2021-01-21 18:26:26', NULL),
(24, 1, 1, 'Jeans & Pants', 'jeans-pants', 'childcategory/1611293212.jpg', 'childcategory/1611293212.jpg', 'Jeans & Pants', NULL, NULL, NULL, 1, '2021-01-20 01:12:58', '2021-01-21 18:27:17', NULL),
(25, 1, 1, 'Pajamas & Joggers', 'pajamas-joggers', 'childcategory/1611293312.jpg', 'childcategory/1611293312.jpg', 'Pajamas & Joggers', NULL, NULL, NULL, 1, '2021-01-20 01:16:34', '2021-01-21 18:28:55', NULL),
(26, 1, 1, 'Inner Wear & Thermals', 'inner-wear-thermals', 'childcategory/1611293363.jpg', 'childcategory/1611293363.jpg', 'Innerwear & Thermals', NULL, NULL, NULL, 1, '2021-01-20 01:18:23', '2021-01-21 18:29:45', NULL),
(27, 3, 9, 'Upto 1 Year', 'upto-1-year', 'childcategory/1611318707.jpg', 'childcategory/1611318707.jpg', 'BY AGE', NULL, NULL, NULL, 1, '2021-01-20 01:20:05', '2021-01-22 01:32:08', NULL),
(28, 1, 1, 'Dresses', 'dresses', 'childcategory/1611293434.jpg', 'childcategory/1611293434.jpg', 'Dresses', NULL, NULL, NULL, 1, '2021-01-20 01:20:58', '2021-01-21 18:31:12', NULL),
(29, 1, 1, 'Twinning Dresses', 'twinning-dresses', 'childcategory/1611293592.jpg', 'childcategory/1611293592.jpg', 'Twinning Dresses', NULL, NULL, NULL, 1, '2021-01-20 01:27:05', '2021-01-21 18:33:33', NULL),
(30, 1, 1, 'Casual Wear', 'casual-wear', 'childcategory/1611293633.jpg', 'childcategory/1611293633.jpg', 'Casual Wear', NULL, NULL, NULL, 1, '2021-01-20 01:29:42', '2021-01-21 18:34:15', NULL),
(31, 1, 1, 'Nightwear', 'nightwear', 'childcategory/1611293909.jpg', 'childcategory/1611293909.jpg', 'Nightwear', NULL, NULL, NULL, 1, '2021-01-20 01:31:10', '2021-01-21 18:40:45', NULL),
(32, 3, 10, 'Upto 99', 'upto-99', 'childcategory/1611319750.jpg', 'childcategory/1611319750.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-20 01:32:26', '2021-01-22 01:49:36', NULL),
(33, 1, 1, 'Ethnic Wear', 'ethnic-wear', 'childcategory/1611294084.jpg', 'childcategory/1611294084.jpg', 'Ethnic Wear', NULL, NULL, NULL, 1, '2021-01-20 01:33:33', '2021-01-21 18:41:53', NULL),
(34, 1, 1, 'Party Wear', 'party-wear', 'childcategory/1611294307.jpg', 'childcategory/1611294307.jpg', 'Party Wear', NULL, NULL, NULL, 1, '2021-01-20 01:35:15', '2021-01-21 18:45:41', NULL),
(35, 1, 1, 'Designer Wear', 'designer-wear', 'childcategory/1611294387.jpg', 'childcategory/1611294387.jpg', 'Designer Wear', NULL, NULL, NULL, 1, '2021-01-20 01:38:11', '2021-01-21 18:47:11', NULL),
(36, 1, 1, 'Indo Western Wear', 'indo-western-wear', 'childcategory/1611294453.jpg', 'childcategory/1611294453.jpg', 'Indo Western Wear', NULL, NULL, NULL, 1, '2021-01-20 01:39:43', '2021-01-21 18:47:50', NULL),
(37, 1, 1, 'Theme Costumes', 'theme-costumes', 'childcategory/1611294489.jpg', 'childcategory/1611294489.jpg', 'Theme Costumes', NULL, NULL, NULL, 1, '2021-01-20 01:44:47', '2021-01-21 18:48:40', NULL),
(38, 3, 11, 'Upto 10% OFF', 'upto-10-off', 'childcategory/1611320952.jpg', 'childcategory/1611320952.jpg', 'On sale', NULL, NULL, NULL, 1, '2021-01-20 01:48:00', '2021-01-22 02:09:31', NULL),
(39, 1, 2, 'Clothing Sets & Suits', 'clothing-sets-suits', 'childcategory/1611296496.jpg', 'childcategory/1611296496.jpg', 'Clothing Sets & Suits', NULL, NULL, NULL, 1, '2021-01-20 02:06:24', '2021-01-21 19:21:58', NULL),
(40, 1, 2, 'Onesies & Rompers', 'onesies-rompers', 'childcategory/1611296570.jpg', 'childcategory/1611296570.jpg', 'Onesies & Rompers', NULL, NULL, NULL, 1, '2021-01-20 02:09:10', '2021-01-21 19:23:08', NULL),
(41, 1, 2, 'Tops & T-shirts', 'tops-t-shirts', 'childcategory/1611296627.jpg', 'childcategory/1611296627.jpg', 'Tops & T-shirts', NULL, NULL, NULL, 1, '2021-01-20 02:11:44', '2021-01-21 19:24:13', NULL),
(42, 1, 2, 'Shirts', 'shirts', 'childcategory/1611296698.jpg', 'childcategory/1611296698.jpg', 'Shirts', NULL, NULL, NULL, 1, '2021-01-20 02:13:22', '2021-01-21 19:25:15', NULL),
(43, 1, 2, 'Jackets, Sweaters & Sweat Shirts', 'jackets-sweaters-sweat-shirts', 'childcategory/1611296741.jpg', 'childcategory/1611296741.jpg', 'Jackets,Sweaters & Sweat Shirts', NULL, NULL, NULL, 1, '2021-01-20 02:16:30', '2021-02-02 03:58:30', NULL),
(44, 1, 2, 'Jackets,Sweaters & Sweat Shirts', 'jacketssweaters-sweat-shirts', 'childcategory/1611148569.jpg', 'childcategory/1611148569.jpg', 'Jackets,Sweaters & Sweat Shirts', NULL, NULL, NULL, 1, '2021-01-20 02:17:33', '2021-01-20 02:19:09', '2021-01-20 02:19:09'),
(45, 1, 2, 'Shorts & Skirts', 'shorts-skirts', 'childcategory/1611296782.jpg', 'childcategory/1611296782.jpg', 'Shorts & Skirts', NULL, NULL, NULL, 1, '2021-01-20 02:37:07', '2021-01-21 19:26:39', NULL),
(46, 1, 2, 'Jeans & Pants', 'jeans-pants', 'childcategory/1611296853.jpg', 'childcategory/1611296853.jpg', 'Jeans & Pants', NULL, NULL, NULL, 1, '2021-01-20 02:39:40', '2021-01-21 19:28:04', NULL),
(47, 3, 12, 'FisherPrice', 'fisherprice', 'childcategory/1611150329.jpg', 'childcategory/1611150329.jpg', 'BY NEED', NULL, NULL, NULL, 1, '2021-01-20 02:45:46', '2021-01-20 19:09:21', '2021-01-20 19:09:21'),
(48, 1, 2, 'Pajamas & Leggings', 'pajamas-leggings', 'childcategory/1611296924.jpg', 'childcategory/1611296924.jpg', 'Pajamas & Leggings', NULL, NULL, NULL, 1, '2021-01-20 02:50:05', '2021-01-21 19:29:04', NULL),
(49, 1, 2, 'Inner Wear & Thermals', 'inner-wear-thermals', 'childcategory/1611296979.jpg', 'childcategory/1611296979.jpg', 'Inner Wear & Thermals', NULL, NULL, NULL, 1, '2021-01-20 02:54:21', '2021-01-21 19:30:41', NULL),
(50, 1, 2, 'Frocks & Dresses', 'frocks-dresses', 'childcategory/1611297128.jpg', 'childcategory/1611297128.jpg', 'Frocks & Dresses', NULL, NULL, NULL, 1, '2021-01-20 02:55:53', '2021-01-21 19:32:32', NULL),
(51, 1, 2, 'Jump Suits & Dungarees', 'jump-suits-dungarees', 'childcategory/1611297241.jpg', 'childcategory/1611297241.jpg', 'Jump Suits & Dungrees', NULL, NULL, NULL, 1, '2021-01-20 02:57:21', '2021-02-06 18:09:29', NULL),
(52, 1, 2, 'Lehangas', 'lehangas', 'childcategory/1611297280.jpg', 'childcategory/1611297280.jpg', 'Lehangas', NULL, NULL, NULL, 1, '2021-01-20 02:58:51', '2021-01-21 19:34:57', NULL),
(53, 1, 2, 'Twinning Dresses', 'twinning-dresses', 'childcategory/1611297349.jpg', 'childcategory/1611297349.jpg', 'Twinning Dresses', NULL, NULL, NULL, 1, '2021-01-20 03:00:39', '2021-01-21 19:36:08', NULL),
(54, 1, 2, 'Casual Wear', 'casual-wear', 'childcategory/1611297426.jpg', 'childcategory/1611297426.jpg', 'Casual Wear', NULL, NULL, NULL, 1, '2021-01-20 03:02:10', '2021-01-21 19:37:25', NULL),
(55, 1, 2, 'Nightwear', 'nightwear', 'childcategory/1611297471.jpg', 'childcategory/1611297471.jpg', 'Nightwear', NULL, NULL, NULL, 1, '2021-01-20 03:04:33', '2021-01-21 19:38:11', NULL),
(56, 1, 2, 'Ethnic Wear', 'ethnic-wear', 'childcategory/1611297536.jpg', 'childcategory/1611297536.jpg', 'Ethnic Wear', NULL, NULL, NULL, 1, '2021-01-20 03:06:05', '2021-01-21 19:39:16', NULL),
(57, 1, 2, 'Designer Wear', 'designer-wear', 'childcategory/1611297582.jpg', 'childcategory/1611297582.jpg', 'Designer Wear', NULL, NULL, NULL, 1, '2021-01-20 03:07:57', '2021-01-21 19:39:59', NULL),
(58, 1, 2, 'Party Wear', 'party-wear', 'childcategory/1611297619.jpg', 'childcategory/1611297619.jpg', 'Party Wear', NULL, NULL, NULL, 1, '2021-01-20 03:09:33', '2021-01-21 19:40:37', NULL),
(59, 1, 2, 'Theme Costumes', 'theme-costumes', 'childcategory/1611297663.jpg', 'childcategory/1611297663.jpg', 'Theme Costumes', NULL, NULL, NULL, 1, '2021-01-20 03:11:15', '2021-01-21 19:41:21', NULL),
(60, 1, 2, 'Indo Western Wear', 'indo-western-wear', 'childcategory/1611297706.jpg', 'childcategory/1611297706.jpg', 'Indo Western Wear', NULL, NULL, NULL, 1, '2021-01-20 03:13:29', '2021-01-21 19:42:06', NULL),
(61, 1, 2, 'Organic', 'organic', 'childcategory/1611297748.jpg', 'childcategory/1611297748.jpg', 'Organic', NULL, NULL, NULL, 1, '2021-01-20 03:17:51', '2021-01-21 19:42:46', NULL),
(62, 1, 5, 'New Born (0-3 M)', 'new-born-0-3-m', 'childcategory/1611298064.jpg', 'childcategory/1611298064.jpg', 'New Born (0-3 M)', NULL, NULL, NULL, 1, '2021-01-20 03:25:28', '2021-01-21 19:48:07', NULL),
(63, 1, 5, '4-6 Months', '4-6-months', 'childcategory/1611298108.jpg', 'childcategory/1611298108.jpg', '4-6 Months', NULL, NULL, NULL, 1, '2021-01-20 03:27:21', '2021-02-06 17:15:37', NULL),
(64, 1, 5, '7-9 Months', '7-9-months', 'childcategory/1611298145.jpg', 'childcategory/1611298145.jpg', '7-9 Months', NULL, NULL, NULL, 1, '2021-01-20 03:28:46', '2021-01-21 19:49:21', NULL),
(65, 1, 5, '10-12 Months', '10-12-months', 'childcategory/1611298181.jpg', 'childcategory/1611298181.jpg', '10-12 Months', NULL, NULL, NULL, 1, '2021-01-20 03:30:58', '2021-01-21 19:49:59', NULL),
(66, 1, 5, '1-4 Years', '1-4-years', 'childcategory/1611298227.jpg', 'childcategory/1611298227.jpg', '1-4 Years', NULL, NULL, NULL, 1, '2021-01-20 03:33:19', '2021-01-21 19:50:44', NULL),
(67, 1, 5, '5-8 Years', '5-8-years', 'childcategory/1611298289.jpg', 'childcategory/1611298289.jpg', '5-8 Years', NULL, NULL, NULL, 1, '2021-01-20 03:35:19', '2021-01-21 19:51:47', NULL),
(68, 1, 5, '9-12 Years', '9-12-years', 'childcategory/1611298327.jpg', 'childcategory/1611298327.jpg', '9-12 Years', NULL, NULL, NULL, 1, '2021-01-20 03:36:28', '2021-01-21 19:52:24', NULL),
(69, 1, 5, '12+ Years', '12-years', 'childcategory/1611298362.jpg', 'childcategory/1611298362.jpg', '12+ Years', NULL, NULL, NULL, 1, '2021-01-20 03:37:30', '2021-01-21 19:52:59', NULL),
(70, 1, 3, 'Upto 99', 'upto-99', 'childcategory/1611301682.jpg', 'childcategory/1611301682.jpg', 'Upto 99', NULL, NULL, NULL, 1, '2021-01-20 03:54:50', '2021-01-21 20:48:24', NULL),
(71, 1, 3, '100-299', '100-299', 'childcategory/1611298815.jpg', 'childcategory/1611298815.jpg', '100-299', NULL, NULL, NULL, 1, '2021-01-20 03:59:54', '2021-01-21 20:00:47', NULL),
(72, 1, 3, '300-599', '300-599', 'childcategory/1611298887.jpg', 'childcategory/1611298887.jpg', '300-599', NULL, NULL, NULL, 1, '2021-01-20 04:01:44', '2021-01-21 20:02:15', NULL),
(73, 1, 3, '600-999', '600-999', 'childcategory/1611298967.jpg', 'childcategory/1611298967.jpg', '600-999', NULL, NULL, NULL, 1, '2021-01-20 04:02:42', '2021-01-21 20:03:06', NULL),
(74, 1, 3, '1000-1999', '1000-1999', 'childcategory/1611299013.jpg', 'childcategory/1611299013.jpg', '1000-1999', NULL, NULL, NULL, 1, '2021-01-20 04:03:48', '2021-02-02 06:41:17', NULL),
(75, 1, 3, '2000-2999', '2000-2999', 'childcategory/1611299070.jpg', 'childcategory/1611299070.jpg', '2000-2999', NULL, NULL, NULL, 1, '2021-01-20 04:05:12', '2021-01-21 20:04:47', NULL),
(76, 1, 3, '3000-4999', '3000-4999', 'childcategory/1611299059.jpg', 'childcategory/1611299059.jpg', '3000-4999', NULL, NULL, NULL, 1, '2021-01-20 04:06:20', '2021-02-02 06:51:53', NULL),
(77, 1, 3, '5000 and above', '5000-and-above', 'childcategory/1611299138.jpg', 'childcategory/1611299138.jpg', '5000 and above', NULL, NULL, NULL, 1, '2021-01-20 04:07:38', '2021-01-21 20:05:56', NULL),
(78, 1, 6, 'Upto 10% OFF', 'upto-10-off', 'childcategory/1611309964.jpg', 'childcategory/1611309964.jpg', 'Upto 10% OFF', NULL, NULL, NULL, 1, '2021-01-20 04:14:51', '2021-01-21 23:06:30', NULL),
(79, 1, 6, '11-20% OFF', '11-20-off', 'childcategory/1611310056.jpg', 'childcategory/1611310056.jpg', '11-20% OFF', NULL, NULL, NULL, 1, '2021-01-20 04:16:11', '2021-01-21 23:08:10', NULL),
(80, 1, 6, '21-30% OFF', '21-30-off', 'childcategory/1611310145.jpg', 'childcategory/1611310145.jpg', '21-30% OFF', NULL, NULL, NULL, 1, '2021-01-20 04:17:39', '2021-01-21 23:10:07', NULL),
(81, 1, 6, '31-40% OFF', '31-40-off', 'childcategory/1611310277.jpg', 'childcategory/1611310277.jpg', '31-40% OFF', NULL, NULL, NULL, 1, '2021-01-20 04:35:53', '2021-01-21 23:11:40', NULL),
(82, 1, 6, '41-50% OFF', '41-50-off', 'childcategory/1611310332.jpg', 'childcategory/1611310332.jpg', '41-50% OFF', NULL, NULL, NULL, 1, '2021-01-20 04:37:19', '2021-01-21 23:12:58', NULL),
(83, 1, 6, 'Above 50% OFF', 'above-50-off', 'childcategory/1611310452.jpg', 'childcategory/1611310452.jpg', 'Above 50% OFF', NULL, NULL, NULL, 1, '2021-01-20 04:38:31', '2021-01-21 23:14:35', NULL),
(84, 1, 8, 'Parties & Josh', 'parties-josh', 'childcategory/1611310649.jpg', 'childcategory/1611310649.jpg', 'Parties & Josh', NULL, NULL, NULL, 1, '2021-01-20 05:59:58', '2021-02-06 08:00:20', NULL),
(85, 1, 8, 'Beach Time', 'beach-time', 'childcategory/1611310718.jpg', 'childcategory/1611310718.jpg', 'Beach Time', NULL, NULL, NULL, 1, '2021-01-20 06:02:52', '2021-01-21 23:19:04', NULL),
(86, 1, 8, 'Traditional Time', 'traditional-time', 'childcategory/1611310770.jpg', 'childcategory/1611310770.jpg', 'Traditional Time', NULL, NULL, NULL, 1, '2021-01-20 06:04:23', '2021-01-21 23:19:54', NULL),
(87, 1, 8, 'Halloween', 'halloween', 'childcategory/1611310867.jpg', 'childcategory/1611310867.jpg', 'Halloween', NULL, NULL, NULL, 1, '2021-01-20 06:05:55', '2021-01-21 23:21:34', NULL),
(88, 1, 8, 'Wedding Specials', 'wedding-specials', 'childcategory/1611310918.jpg', 'childcategory/1611310918.jpg', 'Wedding Specials', NULL, NULL, NULL, 1, '2021-01-20 06:07:24', '2021-02-06 07:55:39', NULL),
(89, 1, 8, 'SuperHeros & Comics', 'superheros-comics', 'childcategory/1611310973.jpg', 'childcategory/1611310973.jpg', 'SuperHeros & Comics', NULL, NULL, NULL, 1, '2021-01-20 06:09:28', '2021-01-21 23:23:13', NULL),
(90, 1, 8, 'Crazy Quotes', 'crazy-quotes', 'childcategory/1611311017.jpg', 'childcategory/1611311017.jpg', 'Crazy Quotes', NULL, NULL, NULL, 1, '2021-01-20 06:10:42', '2021-01-21 23:23:58', NULL),
(91, 1, 8, 'Unleash White', 'unleash-white', 'childcategory/1611311065.jpg', 'childcategory/1611311065.jpg', 'Unleash White', NULL, NULL, NULL, 1, '2021-01-20 06:12:11', '2021-01-21 23:24:45', NULL),
(92, 1, 8, 'Magic Black', 'magic-black', 'childcategory/1611311127.jpg', 'childcategory/1611311127.jpg', 'Magic Black', NULL, NULL, NULL, 1, '2021-01-20 06:13:24', '2021-01-21 23:25:47', NULL),
(93, 3, 4, 'Wooden toys', 'wooden-toys', 'childcategory/1611314085.jpg', 'childcategory/1611314085.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 17:57:55', '2021-02-06 20:14:45', NULL),
(94, 3, 4, 'Electronic & Remote Control Toys', 'electronic-remote-control-toys', 'childcategory/1611315235.jpg', 'childcategory/1611315235.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 18:00:38', '2021-02-12 05:32:11', NULL),
(95, 3, 4, 'Organic Toys', 'organic-toys', 'childcategory/1611315301.jpg', 'childcategory/1611315301.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-20 18:03:40', '2021-01-22 00:35:19', NULL),
(96, 3, 7, 'Drawing,Arts&Crafts', 'drawingartscrafts', 'childcategory/1611317962.jpg', 'childcategory/1611317962.jpg', 'LEARNING', NULL, NULL, NULL, 1, '2021-01-20 18:06:41', '2021-01-27 21:04:16', '2021-01-27 21:04:16'),
(97, 3, 13, 'Bikes & Cars', 'bikes-cars', 'childcategory/1611318420.jpg', 'childcategory/1611318420.jpg', 'RIDES&FUN', NULL, NULL, NULL, 1, '2021-01-20 18:14:36', '2021-01-22 01:27:24', NULL),
(98, 3, 13, 'Cycles & Scooters', 'cycles-scooters', 'childcategory/1611318471.jpg', 'childcategory/1611318471.jpg', 'RIDES&FUN', NULL, NULL, NULL, 1, '2021-01-20 18:17:03', '2021-01-22 01:28:14', NULL),
(99, 3, 9, '1-4 Years', '1-4-years', 'childcategory/1611318801.jpg', 'childcategory/1611318801.jpg', 'BY AGE', NULL, NULL, NULL, 1, '2021-01-20 18:19:49', '2021-01-22 01:33:41', NULL),
(100, 3, 9, '5-8 Years', '5-8-years', 'childcategory/1611318884.jpg', 'childcategory/1611318884.jpg', 'BY AGE', NULL, NULL, NULL, 1, '2021-01-20 18:24:11', '2021-01-22 01:35:05', NULL),
(101, 3, 9, '9-12 Years', '9-12-years', 'childcategory/1611319022.jpg', 'childcategory/1611319022.jpg', 'BY AGE', NULL, NULL, NULL, 1, '2021-01-20 18:25:31', '2021-01-22 01:37:31', NULL),
(102, 3, 9, '12+ Years', '12-years', 'childcategory/1611319091.jpg', 'childcategory/1611319091.jpg', 'BY AGE', NULL, NULL, NULL, 1, '2021-01-20 18:27:18', '2021-01-22 01:38:46', NULL),
(103, 3, 10, '100-299', '100-299', 'childcategory/1611319803.jpg', 'childcategory/1611319803.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-20 18:29:07', '2021-01-22 01:51:40', NULL),
(104, 3, 10, '300-599', '300-599', 'childcategory/1611319925.jpg', 'childcategory/1611319925.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-20 18:31:13', '2021-01-22 01:52:30', NULL),
(105, 3, 10, '600-999', '600-999', 'childcategory/1611319987.jpg', 'childcategory/1611319987.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-20 18:32:42', '2021-01-22 01:53:54', NULL),
(106, 3, 10, '1000-1999', '1000-1999', 'childcategory/1611320077.jpg', 'childcategory/1611320077.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-20 18:33:59', '2021-01-22 01:54:57', NULL),
(107, 3, 10, '2000-2999', '2000-2999', 'childcategory/1611320118.jpg', 'childcategory/1611320118.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-20 18:35:37', '2021-01-22 01:55:44', NULL),
(108, 3, 10, '3000-4999', '3000-4999', 'childcategory/1611320186.jpg', 'childcategory/1611320186.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-20 18:37:10', '2021-01-22 01:56:46', NULL),
(109, 3, 10, '5000 and above', '5000-and-above', 'childcategory/1611320264.jpg', 'childcategory/1611320264.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-20 18:39:09', '2021-01-22 01:58:03', NULL),
(110, 3, 11, '11-20% OFF', '11-20-off', 'childcategory/1611321023.jpg', 'childcategory/1611321023.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-20 18:41:34', '2021-01-22 02:12:45', NULL),
(111, 3, 11, '21-30% OFF', '21-30-off', 'childcategory/1611321291.jpg', 'childcategory/1611321291.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-20 18:43:07', '2021-01-22 02:15:12', NULL),
(112, 3, 11, '31-40% OFF', '31-40-off', 'childcategory/1611321338.jpg', 'childcategory/1611321338.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-20 18:44:30', '2021-02-12 05:30:07', NULL),
(113, 3, 11, '41-50% OFF', '41-50-off', 'childcategory/1611321382.jpg', 'childcategory/1611321382.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-20 18:45:54', '2021-02-06 10:23:19', NULL),
(114, 3, 11, 'Above 50% OFF', 'above-50-off', 'childcategory/1611321429.jpg', 'childcategory/1611321429.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-20 18:47:37', '2021-01-22 02:17:27', NULL),
(115, 3, 14, 'FisherPrice', 'fisherprice', 'childcategory/1611322366.jpg', 'childcategory/1611322366.jpg', 'BY BRAND', NULL, NULL, NULL, 1, '2021-01-20 18:55:34', '2021-01-22 02:33:06', NULL),
(116, 3, 14, 'Barbie', 'barbie', 'childcategory/1611322460.jpg', 'childcategory/1611322460.jpg', 'BY BRAND', NULL, NULL, NULL, 1, '2021-01-20 18:57:29', '2021-01-22 02:34:43', NULL),
(117, 3, 14, 'Disney', 'disney', 'childcategory/1611322410.jpg', 'childcategory/1611322410.jpg', 'BY  BRAND', NULL, NULL, NULL, 1, '2021-01-20 19:00:00', '2021-01-22 02:33:49', NULL),
(118, 3, 14, 'Lego', 'lego', 'childcategory/1611322506.jpg', 'childcategory/1611322506.jpg', 'BY BRAND', NULL, NULL, NULL, 1, '2021-01-20 19:01:42', '2021-01-22 02:35:26', NULL),
(119, 3, 14, 'Chicco', 'chicco', 'childcategory/1611322590.jpg', 'childcategory/1611322590.jpg', 'BY BRAND', NULL, NULL, NULL, 1, '2021-01-20 19:03:16', '2021-01-22 02:36:54', NULL),
(120, 3, 12, 'Indoor Games & Toys', 'indoor-games-toys', 'childcategory/1611321797.jpg', 'childcategory/1611321797.jpg', 'BY NEED', NULL, NULL, NULL, 1, '2021-01-20 19:12:07', '2021-01-22 02:24:12', NULL),
(121, 3, 12, 'Outdoor Games & Toys', 'outdoor-games-toys', 'childcategory/1611321884.jpg', 'childcategory/1611321884.jpg', 'BY NEED', NULL, NULL, NULL, 1, '2021-01-20 19:13:07', '2021-01-22 02:25:06', NULL),
(122, 3, 12, 'IQ & Learning', 'iq-learning', 'childcategory/1611321995.jpg', 'childcategory/1611321995.jpg', 'BY NEED', NULL, NULL, NULL, 1, '2021-01-20 19:14:00', '2021-01-22 02:26:54', NULL),
(123, 3, 12, 'Arts, Crafts & Dyis', 'arts-crafts-dyis', 'childcategory/1611322093.jpg', 'childcategory/1611322093.jpg', 'BY NEED', NULL, NULL, NULL, 1, '2021-01-20 19:15:17', '2021-01-22 02:28:34', NULL),
(124, 3, 12, 'Organic Toys', 'organic-toys', 'childcategory/1611322148.jpg', 'childcategory/1611322148.jpg', 'BY NEED', NULL, NULL, NULL, 1, '2021-01-20 19:16:27', '2021-01-22 02:29:31', NULL),
(125, 10, 25, 'Belts & Caps', 'belts-caps', 'childcategory/1611492511.jpg', 'childcategory/1611492511.jpg', 'Belts & Caps', NULL, NULL, NULL, 1, '2021-01-24 01:50:08', '2021-01-24 01:50:08', NULL),
(126, 10, 25, 'Ear Rings', 'ear-rings', 'childcategory/1611492755.jpg', 'childcategory/1611492755.jpg', 'Ear Rings', NULL, NULL, NULL, 1, '2021-01-24 01:53:01', '2021-01-24 01:53:01', NULL),
(127, 10, 25, 'Hair Accessories & More', 'hair-accessories-more', 'childcategory/1611492999.jpg', 'childcategory/1611492999.jpg', 'Hair Accessories & More', NULL, NULL, NULL, 1, '2021-01-24 01:57:19', '2021-01-27 20:02:33', NULL),
(128, 10, 25, 'Sun Glasses', 'sun-glasses', 'childcategory/1611493154.jpg', 'childcategory/1611493154.jpg', 'Sun Glasses', NULL, NULL, NULL, 1, '2021-01-24 01:59:51', '2021-01-24 01:59:51', NULL),
(129, 10, 25, 'Bags', 'bags', 'childcategory/1611493243.jpg', 'childcategory/1611493243.jpg', 'Bags', NULL, NULL, NULL, 1, '2021-01-24 02:01:37', '2021-01-24 02:01:37', NULL),
(130, 10, 25, 'Socks, Stockings & Gloves', 'socks-stockings-gloves', 'childcategory/1611493406.jpg', 'childcategory/1611493406.jpg', 'Socks, Stockings & Gloves', NULL, NULL, NULL, 1, '2021-01-24 02:03:55', '2021-01-24 02:03:55', NULL),
(131, 10, 25, 'Watches', 'watches', 'childcategory/1611493582.jpg', 'childcategory/1611493582.jpg', 'Watches', NULL, NULL, NULL, 1, '2021-01-24 02:06:48', '2021-01-24 02:06:48', NULL),
(132, 10, 26, 'Belts & Caps', 'belts-caps', 'childcategory/1611494082.jpg', 'childcategory/1611494082.jpg', 'Belts & Caps', NULL, NULL, NULL, 1, '2021-01-24 02:15:09', '2021-01-24 02:15:09', NULL),
(133, 10, 26, 'Sun Glasses', 'sun-glasses', 'childcategory/1611494161.jpg', 'childcategory/1611494161.jpg', 'Sun Glasses', NULL, NULL, NULL, 1, '2021-01-24 02:16:28', '2021-01-24 02:16:28', NULL),
(134, 10, 26, 'Bags', 'bags', 'childcategory/1611494235.jpg', 'childcategory/1611494235.jpg', 'Bags', NULL, NULL, NULL, 1, '2021-01-24 02:17:40', '2021-01-24 02:17:40', NULL),
(135, 10, 26, 'Socks & Gloves', 'socks-gloves', 'childcategory/1611494330.jpg', 'childcategory/1611494330.jpg', 'Socks & Gloves', NULL, NULL, NULL, 1, '2021-01-24 02:19:24', '2021-01-27 20:15:24', NULL),
(136, 10, 26, 'Watches', 'watches', 'childcategory/1611494410.jpg', 'childcategory/1611494410.jpg', 'Watches', NULL, NULL, NULL, 1, '2021-01-24 02:20:29', '2021-01-24 02:20:29', NULL),
(137, 10, 17, 'Bottles, Sippers & Cups', 'bottles-sippers-cups', 'childcategory/1611494752.jpg', 'childcategory/1611494752.jpg', 'Bottles, Sippers & Cups', NULL, NULL, NULL, 1, '2021-01-24 02:26:21', '2021-01-27 20:18:30', NULL),
(138, 10, 17, 'Teethers & Pacifiers', 'teethers-pacifiers', 'childcategory/1611494968.jpg', 'childcategory/1611494968.jpg', 'Teethers & Pacifiers', NULL, NULL, NULL, 1, '2021-01-24 02:30:17', '2021-02-08 18:26:24', NULL),
(139, 10, 17, 'Bibs & Napkins', 'bibs-napkins', 'childcategory/1611495181.jpg', 'childcategory/1611495181.jpg', 'Bibs & Napkins', NULL, NULL, NULL, 1, '2021-01-24 02:35:28', '2021-01-24 02:35:28', NULL),
(140, 10, 17, 'Utensils & Cutlery', 'utensils-cutlery', 'childcategory/1611495421.jpg', 'childcategory/1611495421.jpg', 'Utensils & Cutlery', NULL, NULL, NULL, 1, '2021-01-24 02:37:47', '2021-01-24 02:37:47', NULL),
(141, 10, 17, 'Pillows & Supporters', 'pillows-supporters', 'childcategory/1611495536.jpg', 'childcategory/1611495536.jpg', 'Pillows & Supporters', NULL, NULL, NULL, 1, '2021-01-24 02:39:23', '2021-01-24 02:39:23', NULL),
(142, 10, 17, 'Breast Pumps', 'breast-pumps', 'childcategory/1611495631.jpg', 'childcategory/1611495631.jpg', 'Breast Pumps', NULL, NULL, NULL, 1, '2021-01-24 02:41:05', '2021-01-24 02:41:05', NULL),
(143, 10, 17, 'Sterilizers & Warmers', 'sterilizers-warmers', 'childcategory/1611495858.jpg', 'childcategory/1611495858.jpg', 'Sterilizers & Warmers', NULL, NULL, NULL, 1, '2021-01-24 02:44:41', '2021-01-24 02:44:41', NULL),
(144, 10, 17, 'Cleaning Accessories', 'cleaning-accessories', 'childcategory/1611495963.jpg', 'childcategory/1611495963.jpg', 'Cleaning Accessories', NULL, NULL, NULL, 1, '2021-01-24 02:46:33', '2021-01-27 20:21:06', '2021-01-27 20:21:06'),
(145, 10, 17, 'Feeding Accessories', 'feeding-accessories', 'childcategory/1611496057.jpg', 'childcategory/1611496057.jpg', 'Feeding Accessories', NULL, NULL, NULL, 1, '2021-01-24 02:48:11', '2021-01-27 20:21:22', '2021-01-27 20:21:22'),
(146, 10, 18, 'Bath Tubs & Chairs', 'bath-tubs-chairs', 'childcategory/1611496494.jpg', 'childcategory/1611496494.jpg', 'Bath Tubs & Chairs', NULL, NULL, NULL, 1, '2021-01-24 02:55:24', '2021-01-24 02:55:24', NULL),
(147, 10, 18, 'Towels and Robes', 'towels-and-robes', 'childcategory/1611496599.jpg', 'childcategory/1611496599.jpg', 'Towels & Wrappers', NULL, NULL, NULL, 1, '2021-01-24 02:57:12', '2021-02-08 18:24:30', NULL),
(148, 10, 18, 'Shower Caps & Sponge', 'shower-caps-sponge', 'childcategory/1611496748.jpg', 'childcategory/1611496748.jpg', 'Shower Caps & Sponge', NULL, NULL, NULL, 1, '2021-01-24 02:59:39', '2021-01-24 02:59:39', NULL),
(149, 10, 18, 'Bathing Toys', 'bathing-toys', 'childcategory/1611496874.jpg', 'childcategory/1611496874.jpg', 'Bathing Toys', NULL, NULL, NULL, 1, '2021-01-24 03:01:56', '2021-01-24 03:01:56', NULL),
(150, 10, 18, 'Brush & More', 'brush-more', 'childcategory/1611497008.jpg', 'childcategory/1611497008.jpg', 'Brush & More', NULL, NULL, NULL, 1, '2021-01-24 03:03:53', '2021-01-27 20:30:36', '2021-01-27 20:30:36'),
(151, 10, 18, 'Hair & Nails', 'hair-nails', 'childcategory/1611497084.jpg', 'childcategory/1611497084.jpg', 'Hair & Nails', NULL, NULL, NULL, 1, '2021-01-24 03:05:15', '2021-01-27 20:31:07', '2021-01-27 20:31:07'),
(152, 10, 19, 'Sterilizers & Warmers', 'sterilizers-warmers', 'childcategory/1611497687.jpg', 'childcategory/1611497687.jpg', 'Sterilizers & Warmers', NULL, NULL, NULL, 1, '2021-01-24 03:15:14', '2021-01-27 20:33:47', '2021-01-27 20:33:47'),
(153, 10, 19, 'Food Processors', 'food-processors', 'childcategory/1611497773.jpg', 'childcategory/1611497773.jpg', 'Food Processors', NULL, NULL, NULL, 1, '2021-01-24 03:16:38', '2021-01-27 20:33:47', '2021-01-27 20:33:47'),
(154, 10, 19, 'Cookers & Utensils', 'cookers-utensils', 'childcategory/1611497868.jpg', 'childcategory/1611497868.jpg', 'Cookers & Utensils', NULL, NULL, NULL, 1, '2021-01-24 03:18:22', '2021-01-27 20:33:47', '2021-01-27 20:33:47'),
(155, 10, 19, 'Breast Pumps', 'breast-pumps', 'childcategory/1611497987.jpg', 'childcategory/1611497987.jpg', 'Breast Pumps', NULL, NULL, NULL, 1, '2021-01-24 03:20:36', '2021-01-27 20:33:47', '2021-01-27 20:33:47'),
(156, 10, 21, 'Bed & Mattress', 'bed-mattress', 'childcategory/1611498654.jpg', 'childcategory/1611498654.jpg', 'Bed & Mattress', NULL, NULL, NULL, 1, '2021-01-24 03:31:33', '2021-01-24 03:31:33', NULL),
(157, 10, 21, 'Pillows & Blankets', 'pillows-blankets', 'childcategory/1611498763.jpg', 'childcategory/1611498763.jpg', 'Pillows & Blankets', NULL, NULL, NULL, 1, '2021-01-24 03:33:05', '2021-01-24 03:33:05', NULL),
(158, 10, 21, 'Mats', 'mats', 'childcategory/1611498828.jpg', 'childcategory/1611498828.jpg', 'Mats', NULL, NULL, NULL, 1, '2021-01-24 03:34:11', '2021-01-24 03:34:11', NULL),
(159, 10, 21, 'Mosquito Nets', 'mosquito-nets', 'childcategory/1611499000.jpg', 'childcategory/1611499000.jpg', 'Mosquito Nets', NULL, NULL, NULL, 1, '2021-01-24 03:37:06', '2021-01-24 03:37:06', NULL),
(160, 10, 20, 'Tables & Chairs', 'tables-chairs', 'childcategory/1611499626.jpg', 'childcategory/1611499626.jpg', 'Tables & Chairs', NULL, NULL, NULL, 1, '2021-01-24 03:47:29', '2021-01-24 03:47:29', NULL),
(161, 10, 20, 'Storage Units', 'storage-units', 'childcategory/1611499727.jpg', 'childcategory/1611499727.jpg', 'Storage Units', NULL, NULL, NULL, 1, '2021-01-24 03:49:10', '2021-01-24 03:49:10', NULL),
(162, 10, 20, 'Cots', 'cots', 'childcategory/1611499810.jpg', 'childcategory/1611499810.jpg', 'Cots', NULL, NULL, NULL, 1, '2021-01-24 03:50:34', '2021-01-24 03:50:34', NULL),
(163, 10, 20, 'Decor & Furnishings', 'decor-furnishings', 'childcategory/1611499929.jpg', 'childcategory/1611499929.jpg', 'Decor & Furnishings', NULL, NULL, NULL, 1, '2021-01-24 03:52:30', '2021-01-24 03:52:30', NULL),
(164, 10, 22, 'Upto 99', 'upto-99', 'childcategory/1611506104.jpg', 'childcategory/1611506104.jpg', 'Upto 99', NULL, NULL, NULL, 1, '2021-01-24 05:35:33', '2021-01-24 05:35:33', NULL),
(165, 10, 22, '100-299', '100-299', 'childcategory/1611506212.jpg', 'childcategory/1611506212.jpg', '100-299', NULL, NULL, NULL, 1, '2021-01-24 05:37:17', '2021-01-24 05:37:17', NULL),
(166, 10, 22, '300-599', '300-599', 'childcategory/1611506320.jpg', 'childcategory/1611506320.jpg', '300-599', NULL, NULL, NULL, 1, '2021-01-24 05:39:07', '2021-01-24 05:39:07', NULL),
(167, 10, 22, '600-999', '600-999', 'childcategory/1611506406.jpg', 'childcategory/1611506406.jpg', '600-999', NULL, NULL, NULL, 1, '2021-01-24 05:40:40', '2021-01-24 05:40:40', NULL),
(168, 10, 22, '1000-1999', '1000-1999', 'childcategory/1611506503.jpg', 'childcategory/1611506503.jpg', '1000-1999', NULL, NULL, NULL, 1, '2021-01-24 05:42:12', '2021-01-24 05:42:12', NULL),
(169, 10, 22, '2000-2999', '2000-2999', 'childcategory/1611506612.jpg', 'childcategory/1611506612.jpg', '2000-2999', NULL, NULL, NULL, 1, '2021-01-24 05:44:36', '2021-01-24 05:44:36', NULL),
(170, 10, 22, '5000 and above', '5000-and-above', 'childcategory/1611506807.jpg', 'childcategory/1611506807.jpg', '5000 and above', NULL, NULL, NULL, 1, '2021-01-24 05:47:15', '2021-01-24 05:49:56', '2021-01-24 05:49:56'),
(171, 10, 22, '3000-4999', '3000-4999', 'childcategory/1611506924.jpg', 'childcategory/1611506924.jpg', '3000-4999', NULL, NULL, NULL, 1, '2021-01-24 05:49:07', '2021-01-24 05:49:07', NULL),
(172, 10, 22, '5000 and above', '5000-and-above', 'childcategory/1611506807.jpg', 'childcategory/1611506807.jpg', '5000 and above', NULL, NULL, NULL, 1, '2021-01-24 05:50:47', '2021-01-24 05:50:47', NULL),
(173, 10, 24, 'Upto 10% OFF', 'upto-10-off', 'childcategory/1611507836.jpg', 'childcategory/1611507836.jpg', 'Upto 10% OFF', NULL, NULL, NULL, 1, '2021-01-24 06:04:19', '2021-01-24 06:04:19', NULL),
(174, 10, 24, '11-20% OFF', '11-20-off', 'childcategory/1611508094.jpg', 'childcategory/1611507910.jpg', '11-20% OFF', NULL, NULL, NULL, 1, '2021-01-24 06:08:33', '2021-01-24 06:08:33', NULL),
(175, 10, 24, '21-30% OFF', '21-30-off', 'childcategory/1611508183.jpg', 'childcategory/1611508183.jpg', '21-30% OFF', NULL, NULL, NULL, 1, '2021-01-24 06:10:01', '2021-01-24 06:10:01', NULL),
(176, 10, 24, '31-40% OFF', '31-40-off', 'childcategory/1611508252.jpg', 'childcategory/1611508252.jpg', '31-40% OFF', NULL, NULL, NULL, 1, '2021-01-24 06:11:13', '2021-01-24 06:11:13', NULL),
(177, 10, 24, '41-50% OFF', '41-50-off', 'childcategory/1611508322.jpg', 'childcategory/1611508322.jpg', '41-50% OFF', NULL, NULL, NULL, 1, '2021-01-24 06:12:31', '2021-01-24 06:12:31', NULL),
(178, 10, 24, 'Above 50% OFF', 'above-50-off', 'childcategory/1611508420.jpg', 'childcategory/1611508420.jpg', 'Above 50% OFF', NULL, NULL, NULL, 1, '2021-01-24 06:14:01', '2021-01-24 06:14:01', NULL),
(179, 2, 27, 'Flipflops', 'flipflops', 'childcategory/1611727613.jpg', 'childcategory/1611727613.jpg', 'Flipflops', NULL, NULL, NULL, 1, '2021-01-26 19:07:18', '2021-01-26 19:07:18', NULL),
(180, 2, 27, 'Sandals', 'sandals', 'childcategory/1611727692.jpg', 'childcategory/1611727692.jpg', 'Sandals', NULL, NULL, NULL, 1, '2021-01-26 19:08:31', '2021-01-26 19:08:31', NULL),
(181, 2, 27, 'Loafers', 'loafers', 'childcategory/1611727765.jpg', 'childcategory/1611727765.jpg', 'Loafers', NULL, NULL, NULL, 1, '2021-01-26 19:09:58', '2021-01-26 19:09:58', NULL),
(182, 2, 27, 'Shoes', 'shoes', 'childcategory/1611727858.jpg', 'childcategory/1611727858.jpg', 'Shoes', NULL, NULL, NULL, 1, '2021-01-26 19:11:26', '2021-01-26 19:11:26', NULL),
(183, 2, 27, 'Baby Footwear', 'baby-footwear', 'childcategory/1611727940.jpg', 'childcategory/1611727940.jpg', 'Baby Footwear', NULL, NULL, NULL, 1, '2021-01-26 19:12:46', '2021-01-26 19:12:46', NULL),
(184, 2, 27, 'Casual Wear', 'casual-wear', 'childcategory/1611728016.jpg', 'childcategory/1611728016.jpg', 'Casual Wear', NULL, NULL, NULL, 1, '2021-01-26 19:14:01', '2021-01-26 19:14:01', NULL),
(185, 2, 27, 'Ethnic Wear', 'ethnic-wear', 'childcategory/1611728090.jpg', 'childcategory/1611728090.jpg', 'Ethnic Wear', NULL, NULL, NULL, 1, '2021-01-26 19:15:38', '2021-01-26 19:15:38', NULL),
(186, 2, 27, 'Socks', 'socks', 'childcategory/1611728235.jpg', 'childcategory/1611728235.jpg', 'Socks', NULL, NULL, NULL, 1, '2021-01-26 19:17:38', '2021-01-26 19:17:38', NULL),
(187, 2, 28, 'Flipflops', 'flipflops', 'childcategory/1611728800.jpg', 'childcategory/1611728800.jpg', 'Flipflops', NULL, NULL, NULL, 1, '2021-01-26 19:27:08', '2021-01-26 19:27:08', NULL),
(188, 2, 28, 'Sandals', 'sandals', 'childcategory/1611728876.jpg', 'childcategory/1611728876.jpg', 'Sandals', NULL, NULL, NULL, 1, '2021-01-26 19:28:24', '2021-01-26 19:28:24', NULL),
(189, 2, 28, 'Loafers', 'loafers', 'childcategory/1611732304.jpg', 'childcategory/1611732304.jpg', 'Loafers', NULL, NULL, NULL, 1, '2021-01-26 20:25:46', '2021-01-26 20:25:46', NULL),
(190, 2, 28, 'Shoes', 'shoes', 'childcategory/1611732452.jpg', 'childcategory/1611732452.jpg', 'Shoes', NULL, NULL, NULL, 1, '2021-01-26 20:28:21', '2021-01-26 20:28:21', NULL),
(191, 2, 28, 'Socks', 'socks', 'childcategory/1611732547.jpg', 'childcategory/1611732547.jpg', 'Socks', NULL, NULL, NULL, 1, '2021-01-26 20:29:33', '2021-01-26 20:29:33', NULL),
(192, 2, 28, 'Baby Footwear', 'baby-footwear', 'childcategory/1611732626.jpg', 'childcategory/1611732626.jpg', 'Baby Footwear', NULL, NULL, NULL, 1, '2021-01-26 20:30:49', '2021-01-26 20:30:49', NULL),
(193, 2, 28, 'Casual Wear', 'casual-wear', 'childcategory/1611732860.jpg', 'childcategory/1611732860.jpg', 'Casual Wear', NULL, NULL, NULL, 1, '2021-01-26 20:34:51', '2021-01-26 20:34:51', NULL),
(194, 2, 28, 'Ethnic Wear', 'ethnic-wear', 'childcategory/1611732940.jpg', 'childcategory/1611732940.jpg', 'Ethnic Wear', NULL, NULL, NULL, 1, '2021-01-26 20:35:56', '2021-01-26 20:35:56', NULL),
(195, 2, 29, 'Upto 1 Year', 'upto-1-year', 'childcategory/1611733249.jpg', 'childcategory/1611733249.jpg', 'Upto 1 Year', NULL, NULL, NULL, 1, '2021-01-26 20:41:17', '2021-01-26 20:41:17', NULL),
(196, 2, 29, '1-4 Years', '1-4-years', 'childcategory/1611733324.jpg', 'childcategory/1611733324.jpg', '1-4 Years', NULL, NULL, NULL, 1, '2021-01-26 20:42:34', '2021-01-26 20:42:34', NULL),
(197, 2, 29, '5-8 Years', '5-8-years', 'childcategory/1611733398.jpg', 'childcategory/1611733398.jpg', '5-8 Years', NULL, NULL, NULL, 1, '2021-01-26 20:43:38', '2021-01-26 20:43:38', NULL),
(198, 2, 29, '9-12 Years', '9-12-years', 'childcategory/1611733457.jpg', 'childcategory/1611733457.jpg', '9-12 Years', NULL, NULL, NULL, 1, '2021-01-26 20:44:35', '2021-01-26 20:44:35', NULL),
(199, 2, 29, '12+ Years', '12-years', 'childcategory/1611733521.jpg', 'childcategory/1611733521.jpg', '12+ Years', NULL, NULL, NULL, 1, '2021-01-26 20:45:39', '2021-01-26 20:45:39', NULL),
(200, 2, 30, 'Upto 99', 'upto-99', 'childcategory/1611734000.jpg', 'childcategory/1611734000.jpg', 'Upto 99', NULL, NULL, NULL, 1, '2021-01-26 20:53:52', '2021-01-26 20:53:52', NULL),
(201, 2, 30, '100-299', '100-299', 'childcategory/1611734264.jpg', 'childcategory/1611734264.jpg', '100-299', NULL, NULL, NULL, 1, '2021-01-26 20:58:07', '2021-01-26 20:58:07', NULL),
(202, 2, 30, '300-599', '300-599', 'childcategory/1611734358.jpg', 'childcategory/1611734358.jpg', '300-599', NULL, NULL, NULL, 1, '2021-01-26 20:59:42', '2021-01-26 20:59:42', NULL),
(203, 2, 30, '600-999', '600-999', 'childcategory/1611734509.jpg', 'childcategory/1611734509.jpg', '600-999', NULL, NULL, NULL, 1, '2021-01-26 21:02:17', '2021-01-26 21:02:17', NULL),
(204, 2, 30, '1000-1999', '1000-1999', 'childcategory/1611734605.jpg', 'childcategory/1611734605.jpg', '1000-1999', NULL, NULL, NULL, 1, '2021-01-26 21:03:44', '2021-01-26 21:03:44', NULL),
(205, 2, 30, '2000-2999', '2000-2999', 'childcategory/1611734679.jpg', 'childcategory/1611734679.jpg', '2000-2999', NULL, NULL, NULL, 1, '2021-01-26 21:05:01', '2021-01-26 21:05:01', NULL),
(206, 2, 30, '3000-4999', '3000-4999', 'childcategory/1611734783.jpg', 'childcategory/1611734783.jpg', '3000-4999', NULL, NULL, NULL, 1, '2021-01-26 21:06:55', '2021-01-26 21:06:55', NULL),
(207, 2, 30, '5000 and above', '5000-and-above', 'childcategory/1611734901.jpg', 'childcategory/1611734901.jpg', '5000 and above', NULL, NULL, NULL, 1, '2021-01-26 21:08:46', '2021-01-26 21:08:46', NULL),
(208, 2, 31, 'Upto 10% OFF', 'upto-10-off', 'childcategory/1611735513.jpg', 'childcategory/1611735513.jpg', 'Upto 10% OFF', NULL, NULL, NULL, 1, '2021-01-26 21:18:57', '2021-01-26 21:18:57', NULL),
(209, 2, 31, '11-20% OFFF', '11-20-offf', 'childcategory/1611735648.jpg', 'childcategory/1611735648.jpg', '11-20% OFFF', NULL, NULL, NULL, 1, '2021-01-26 21:21:15', '2021-01-26 21:21:15', NULL),
(210, 2, 31, '21-30% OFF', '21-30-off', 'childcategory/1611735740.jpg', 'childcategory/1611735740.jpg', '21-30% OFF', NULL, NULL, NULL, 1, '2021-01-26 21:22:42', '2021-01-26 21:22:42', NULL),
(211, 2, 31, '31-40% OFF', '31-40-off', 'childcategory/1611735814.jpg', 'childcategory/1611735814.jpg', '31-40% OFF', NULL, NULL, NULL, 1, '2021-01-26 21:23:54', '2021-01-26 21:23:54', NULL),
(212, 2, 31, '41-50% OFF', '41-50-off', 'childcategory/1611735883.jpg', 'childcategory/1611735883.jpg', '41-50% OFF', NULL, NULL, NULL, 1, '2021-01-26 21:25:31', '2021-01-26 21:25:31', NULL),
(213, 2, 31, 'Above 50% OFF', 'above-50-off', 'childcategory/1611735991.jpg', 'childcategory/1611735991.jpg', 'Above 50% OFF', NULL, NULL, NULL, 1, '2021-01-26 21:26:50', '2021-01-26 21:26:50', NULL),
(214, 2, 32, 'School', 'school', 'childcategory/1611736326.jpg', 'childcategory/1611736326.jpg', 'School', NULL, NULL, NULL, 1, '2021-01-26 21:32:24', '2021-01-26 21:32:24', NULL),
(215, 2, 32, 'Sports', 'sports', 'childcategory/1611736410.jpg', 'childcategory/1611736410.jpg', 'Sports', NULL, NULL, NULL, 1, '2021-01-26 21:33:48', '2021-01-26 21:33:48', NULL),
(216, 2, 32, 'Festivals', 'festivals', 'childcategory/1611736995.jpg', 'childcategory/1611736995.jpg', 'Festivals', NULL, NULL, NULL, 1, '2021-01-26 21:43:30', '2021-01-26 21:43:30', NULL),
(217, 1, 1, 'Organic', 'organic', 'childcategory/1611737104.jpg', 'childcategory/1611737104.jpg', 'Organic', NULL, NULL, NULL, 1, '2021-01-26 21:45:22', '2021-01-26 21:45:22', NULL),
(218, 2, 32, 'Daily Wear', 'daily-wear', 'childcategory/1611737345.jpg', 'childcategory/1611737345.jpg', 'Daily Wear', NULL, NULL, NULL, 1, '2021-01-26 21:49:25', '2021-01-26 21:49:25', NULL),
(219, 2, 32, 'Parties', 'parties', 'childcategory/1611737413.jpg', 'childcategory/1611737413.jpg', 'Parties', NULL, NULL, NULL, 1, '2021-01-26 21:50:35', '2021-01-26 21:50:35', NULL),
(220, 8, 33, 'Infant & Baby Food', 'infant-baby-food', 'childcategory/1611738132.jpg', 'childcategory/1611738132.jpg', 'Infant & Baby Food', NULL, NULL, NULL, 1, '2021-01-26 22:02:36', '2021-01-26 22:02:36', NULL),
(221, 8, 33, 'Cereals & Supplements', 'cereals-supplements', 'childcategory/1611738254.jpg', 'childcategory/1611738254.jpg', 'Cereals & Supplements', NULL, NULL, NULL, 1, '2021-01-26 22:04:38', '2021-01-26 22:04:38', NULL),
(222, 8, 33, 'Food & Snacks', 'food-snacks', 'childcategory/1611738411.jpg', 'childcategory/1611738411.jpg', 'Food & Snacks', NULL, NULL, NULL, 1, '2021-01-26 22:07:18', '2021-01-26 22:07:18', NULL),
(223, 8, 33, 'Biscuits, Chocolates & More', 'biscuits-chocolates-more', 'childcategory/1611738519.jpg', 'childcategory/1611738519.jpg', 'Biscuits, Chocolates & More', NULL, NULL, NULL, 1, '2021-01-26 22:08:57', '2021-01-26 22:08:57', NULL),
(224, 8, 33, 'Oils', 'oils', 'childcategory/1611738885.jpg', 'childcategory/1611738885.jpg', 'Oils', NULL, NULL, NULL, 1, '2021-01-26 22:15:10', '2021-01-26 22:15:10', NULL),
(225, 8, 33, 'Immunity Boosters', 'immunity-boosters', 'childcategory/1611739070.jpg', 'childcategory/1611739070.jpg', 'Immunity Boosters', NULL, NULL, NULL, 1, '2021-01-26 22:18:10', '2021-01-26 22:18:10', NULL),
(226, 8, 33, 'Organic Food', 'organic-food', 'childcategory/1611739158.jpg', 'childcategory/1611739158.jpg', 'Organic Food', NULL, NULL, NULL, 1, '2021-01-26 22:19:37', '2021-01-26 22:19:37', NULL),
(227, 8, 34, 'Upto 99', 'upto-99', 'childcategory/1611741347.jpg', 'childcategory/1611741347.jpg', 'Upto 99', NULL, NULL, NULL, 1, '2021-01-26 22:56:19', '2021-01-26 22:56:19', NULL),
(228, 8, 34, '100-299', '100-299', 'childcategory/1611741434.jpg', 'childcategory/1611741434.jpg', '100-299', NULL, NULL, NULL, 1, '2021-01-26 22:58:04', '2021-01-26 22:58:04', NULL),
(229, 8, 34, '300-599', '300-599', 'childcategory/1611741591.jpg', 'childcategory/1611741591.jpg', '300-599', NULL, NULL, NULL, 1, '2021-01-26 23:00:45', '2021-01-26 23:00:45', NULL),
(230, 8, 34, '600-999', '600-999', 'childcategory/1611741707.jpg', 'childcategory/1611741707.jpg', '600-999', NULL, NULL, NULL, 1, '2021-01-26 23:02:08', '2021-01-26 23:02:08', NULL),
(231, 8, 34, '1000-1999', '1000-1999', 'childcategory/1611741787.jpg', 'childcategory/1611741787.jpg', '1000-1999', NULL, NULL, NULL, 1, '2021-01-26 23:03:33', '2021-01-26 23:03:33', NULL),
(232, 8, 34, '2000-2999', '2000-2999', 'childcategory/1611741920.jpg', 'childcategory/1611741920.jpg', '2000-2999', NULL, NULL, NULL, 1, '2021-01-26 23:05:53', '2021-01-26 23:05:53', NULL),
(233, 8, 34, '3000-4999', '3000-4999', 'childcategory/1611742014.jpg', 'childcategory/1611742014.jpg', '3000-4999', NULL, NULL, NULL, 1, '2021-01-26 23:08:23', '2021-01-26 23:08:23', NULL),
(234, 8, 34, '5000 and above', '5000-and-above', 'childcategory/1611742160.jpg', 'childcategory/1611742160.jpg', '5000 and above', NULL, NULL, NULL, 1, '2021-01-26 23:09:44', '2021-01-26 23:09:44', NULL),
(235, 8, 35, 'Upto 10% OFF', 'upto-10-off', 'childcategory/1611742628.jpg', 'childcategory/1611742628.jpg', 'Upto 10% OFF', NULL, NULL, NULL, 1, '2021-01-26 23:19:54', '2021-01-26 23:19:54', NULL),
(236, 8, 35, '11-20% OFF', '11-20-off', 'childcategory/1611742845.jpg', 'childcategory/1611742845.jpg', '11-20% OFF', NULL, NULL, NULL, 1, '2021-01-26 23:21:04', '2021-01-26 23:21:04', NULL),
(237, 8, 35, '21-30% OFF', '21-30-off', 'childcategory/1611742914.jpg', 'childcategory/1611742914.jpg', '21-30% OFF', NULL, NULL, NULL, 1, '2021-01-26 23:22:20', '2021-01-26 23:22:20', NULL),
(238, 8, 35, '31-40% OFF', '31-40-off', 'childcategory/1611743042.jpg', 'childcategory/1611743042.jpg', '31-40% OFF', NULL, NULL, NULL, 1, '2021-01-26 23:24:22', '2021-01-26 23:24:22', NULL),
(239, 8, 35, '41-50% OFF', '41-50-off', 'childcategory/1611743777.jpg', 'childcategory/1611743777.jpg', '41-50% OFF', NULL, NULL, NULL, 1, '2021-01-26 23:36:33', '2021-01-26 23:36:33', NULL),
(240, 8, 35, 'Above 50% OFF', 'above-50-off', 'childcategory/1611743894.jpg', 'childcategory/1611743894.jpg', 'Above 50% OFF', NULL, NULL, NULL, 1, '2021-01-26 23:38:30', '2021-01-26 23:38:30', NULL),
(241, 8, 36, 'New born (Upto 1 Year)', 'new-born-upto-1-year', 'childcategory/1611744195.jpg', 'childcategory/1611744195.jpg', 'New born (Upto 1 Year)', NULL, NULL, NULL, 1, '2021-01-26 23:43:38', '2021-01-26 23:43:38', NULL),
(242, 8, 36, '1-4 Years', '1-4-years', 'childcategory/1611744274.jpg', 'childcategory/1611744274.jpg', '1-4 Years', NULL, NULL, NULL, 1, '2021-01-26 23:44:53', '2021-01-26 23:44:53', NULL),
(243, 8, 36, '5-8 Years', '5-8-years', 'childcategory/1611744410.jpg', 'childcategory/1611744410.jpg', '5-8 Years', NULL, NULL, NULL, 1, '2021-01-26 23:47:14', '2021-01-26 23:47:14', NULL),
(244, 8, 36, '9-12 Years', '9-12-years', 'childcategory/1611744544.jpg', 'childcategory/1611744544.jpg', '9-12 Years', NULL, NULL, NULL, 1, '2021-01-26 23:49:22', '2021-01-26 23:49:22', NULL),
(245, 8, 36, '12+ Years', '12-years', 'childcategory/1611744613.jpg', 'childcategory/1611744613.jpg', '12+ Years', NULL, NULL, NULL, 1, '2021-01-26 23:50:32', '2021-01-26 23:50:32', NULL),
(246, 8, 37, 'Organic Food', 'organic-food', 'childcategory/1611744889.jpg', 'childcategory/1611744889.jpg', 'Organic Food', NULL, NULL, NULL, 1, '2021-01-26 23:55:08', '2021-01-26 23:55:08', NULL),
(247, 8, 37, 'Nutrition Supplements', 'nutrition-supplements', 'childcategory/1611745010.jpg', 'childcategory/1611745010.jpg', 'Nutrition Supplements', NULL, NULL, NULL, 1, '2021-01-26 23:57:09', '2021-01-26 23:57:09', NULL),
(248, 8, 37, 'Immunity Boosters', 'immunity-boosters', 'childcategory/1611745524.jpg', 'childcategory/1611745524.jpg', 'Immunity boosters', NULL, NULL, NULL, 1, '2021-01-27 00:06:15', '2021-01-27 00:06:15', NULL),
(249, 13, 38, 'Fiction & Fantasy', 'fiction-fantasy', 'childcategory/1611748361.jpg', 'childcategory/1611748361.jpg', 'Fiction & Fantasy', NULL, NULL, NULL, 1, '2021-01-27 00:53:09', '2021-01-27 00:53:09', NULL),
(250, 13, 38, 'Science & Technology', 'science-technology', 'childcategory/1611748361.jpg', 'childcategory/1611748361.jpg', 'Science & Technology', NULL, NULL, NULL, 1, '2021-01-27 00:55:54', '2021-01-27 00:55:54', NULL),
(251, 13, 38, 'Biographies & Diaries', 'biographies-diaries', 'childcategory/1611748655.jpg', 'childcategory/1611748655.jpg', 'Biographies & Diaries', NULL, NULL, NULL, 1, '2021-01-27 00:57:53', '2021-01-27 00:57:53', NULL),
(252, 13, 38, 'Action & Adventure', 'action-adventure', 'childcategory/1611749315.jpg', 'childcategory/1611749315.jpg', 'Action & Adventure', NULL, NULL, NULL, 1, '2021-01-27 01:09:00', '2021-01-27 01:09:00', NULL);
INSERT INTO `childcategories` (`id`, `category_id`, `subcategory_id`, `title`, `slug`, `icon`, `image`, `description`, `meta_title`, `meta_keywords`, `meta_description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(253, 13, 38, 'Crime, Thriller & Mystery', 'crime-thriller-mystery', 'childcategory/1611749439.jpg', 'childcategory/1611749439.jpg', 'Crime, Thriller & Mystery', NULL, NULL, NULL, 1, '2021-01-27 01:11:06', '2021-01-27 01:11:06', NULL),
(254, 13, 38, 'Comics', 'comics', 'childcategory/1611749520.jpg', 'childcategory/1611749520.jpg', 'Comics', NULL, NULL, NULL, 1, '2021-01-27 01:12:20', '2021-01-27 01:12:20', NULL),
(255, 13, 38, 'Parenting', 'parenting', 'childcategory/1611749588.jpg', 'childcategory/1611749588.jpg', 'Parenting', NULL, NULL, NULL, 1, '2021-01-27 01:13:31', '2021-01-27 01:13:31', NULL),
(256, 13, 38, 'Academics', 'academics', 'childcategory/1611750156.jpg', 'childcategory/1611750156.jpg', 'Academics', NULL, NULL, NULL, 1, '2021-01-27 01:23:17', '2021-01-27 01:23:17', NULL),
(257, 13, 38, 'History', 'history', 'childcategory/1611750336.jpg', 'childcategory/1611750336.jpg', 'History', NULL, NULL, NULL, 1, '2021-01-27 01:26:01', '2021-01-27 01:26:01', NULL),
(258, 13, 38, 'Bed Time Stories', 'bed-time-stories', 'childcategory/1611750421.jpg', 'childcategory/1611750421.jpg', 'Bed Time Stories', NULL, NULL, NULL, 1, '2021-01-27 01:28:36', '2021-01-27 01:28:36', NULL),
(259, 13, 38, 'Moral Books', 'moral-books', 'childcategory/1611750573.jpg', 'childcategory/1611750573.jpg', 'Moral Books', NULL, NULL, NULL, 1, '2021-01-27 01:29:53', '2021-01-27 01:29:53', NULL),
(260, 13, 38, 'Crafts & Activity', 'crafts-activity', 'childcategory/1611750696.jpg', 'childcategory/1611750696.jpg', 'Crafts & Activity', NULL, NULL, NULL, 1, '2021-01-27 01:32:52', '2021-01-27 01:32:52', NULL),
(261, 13, 39, 'Drawing Books', 'drawing-books', 'childcategory/1611751110.jpg', 'childcategory/1611751110.jpg', 'Drawing Books', NULL, NULL, NULL, 1, '2021-01-27 01:38:57', '2021-01-27 01:38:57', NULL),
(262, 13, 39, 'Alphabets, Pictures & Rhymes', 'alphabets-pictures-rhymes', 'childcategory/1611751223.jpg', 'childcategory/1611751223.jpg', 'Alphabets, Pictures & Rhymes', NULL, NULL, NULL, 1, '2021-01-27 01:40:50', '2021-01-27 01:40:50', NULL),
(263, 13, 39, 'Board Books', 'board-books', 'childcategory/1611751300.jpg', 'childcategory/1611751300.jpg', 'Board Books', NULL, NULL, NULL, 1, '2021-01-27 01:41:59', '2021-01-27 01:41:59', NULL),
(264, 13, 39, 'Crafts & Activity', 'crafts-activity', 'childcategory/1611751415.jpg', 'childcategory/1611751415.jpg', 'Crafts & Activity', NULL, NULL, NULL, 1, '2021-01-27 01:43:58', '2021-01-27 01:43:58', NULL),
(265, 13, 40, 'Art Pencils & Sketches', 'art-pencils-sketches', 'childcategory/1611751819.jpg', 'childcategory/1611751819.jpg', 'Art Pencils & Sketches', NULL, NULL, NULL, 1, '2021-01-27 01:50:44', '2021-02-11 02:50:43', NULL),
(266, 13, 40, 'Easels & Stands', 'easels-stands', 'childcategory/1611751923.jpg', 'childcategory/1611751923.jpg', 'Easels & Stands', NULL, NULL, NULL, 1, '2021-01-27 01:52:21', '2021-01-27 01:52:21', NULL),
(267, 13, 40, 'Paints & Brushes', 'paints-brushes', 'childcategory/1611752002.jpg', 'childcategory/1611752002.jpg', 'Paints & Brushes', NULL, NULL, NULL, 1, '2021-01-27 01:53:46', '2021-01-27 01:53:46', NULL),
(268, 13, 40, 'Clay & More', 'clay-more', 'childcategory/1611752074.jpg', 'childcategory/1611752074.jpg', 'Clay & More', NULL, NULL, NULL, 1, '2021-01-27 01:54:57', '2021-02-11 03:16:17', NULL),
(269, 13, 40, 'Art Papers & More', 'art-papers-more', 'childcategory/1611752193.jpg', 'childcategory/1611752193.jpg', 'Art Papers & More', NULL, NULL, NULL, 1, '2021-01-27 01:56:55', '2021-01-27 01:56:55', NULL),
(270, 13, 41, 'Note Books & Dairies', 'note-books-dairies', 'childcategory/1611752857.jpg', 'childcategory/1611752857.jpg', 'Note Books & Diaries', NULL, NULL, NULL, 1, '2021-01-27 02:08:15', '2021-02-11 02:48:36', NULL),
(271, 13, 41, 'Pens, Pencils & More', 'pens-pencils-more', 'childcategory/1611753105.jpg', 'childcategory/1611753105.jpg', 'Pens, Pencils & More', NULL, NULL, NULL, 1, '2021-01-27 02:13:07', '2021-01-27 02:13:07', NULL),
(272, 13, 41, 'Calculators', 'calculators', 'childcategory/1611753327.jpg', 'childcategory/1611753327.jpg', 'Calculators', NULL, NULL, NULL, 1, '2021-01-27 02:16:05', '2021-01-27 02:16:05', NULL),
(273, 13, 41, 'Desk Organizers', 'desk-organizers', 'childcategory/1611753962.jpg', 'childcategory/1611753962.jpg', 'Desk Organizers', NULL, NULL, NULL, 1, '2021-01-27 02:26:22', '2021-01-27 02:26:22', NULL),
(274, 13, 41, 'Files & Folders', 'files-folders', 'childcategory/1611754037.jpg', 'childcategory/1611754037.jpg', 'Files & Folders', NULL, NULL, NULL, 1, '2021-01-27 02:27:37', '2021-01-27 02:27:37', NULL),
(275, 13, 41, 'Printing Papers', 'printing-papers', 'childcategory/1611754126.jpg', 'childcategory/1611754126.jpg', 'Printing Papers', NULL, NULL, NULL, 1, '2021-01-27 02:29:39', '2021-01-27 02:29:39', NULL),
(276, 13, 42, 'Upto 99', 'upto-99', 'childcategory/1611755320.jpg', 'childcategory/1611755320.jpg', 'Upto 99', NULL, NULL, NULL, 1, '2021-01-27 02:49:15', '2021-01-27 02:49:15', NULL),
(277, 13, 42, '100-299', '100-299', 'childcategory/1611755510.jpg', 'childcategory/1611755510.jpg', '100-299', NULL, NULL, NULL, 1, '2021-01-27 02:52:16', '2021-01-27 02:52:16', NULL),
(278, 13, 42, '300-599', '300-599', 'childcategory/1611755662.jpg', 'childcategory/1611755662.jpg', '300-599', NULL, NULL, NULL, 1, '2021-01-27 02:54:42', '2021-01-27 02:54:42', NULL),
(279, 13, 42, '600-999', '600-999', 'childcategory/1611755750.jpg', 'childcategory/1611755750.jpg', '600-999', NULL, NULL, NULL, 1, '2021-01-27 02:56:09', '2021-01-27 02:56:09', NULL),
(280, 13, 42, '1000-1999', '1000-1999', 'childcategory/1611755955.jpg', 'childcategory/1611755955.jpg', '1000-1999', NULL, NULL, NULL, 1, '2021-01-27 02:59:56', '2021-01-27 02:59:56', NULL),
(281, 13, 42, '2000-2999', '2000-2999', 'childcategory/1611764002.jpg', 'childcategory/1611764002.jpg', '2000-2999', NULL, NULL, NULL, 1, '2021-01-27 05:13:47', '2021-01-27 05:13:47', NULL),
(282, 13, 42, '3000-4999', '3000-4999', 'childcategory/1611764097.jpg', 'childcategory/1611764097.jpg', '3000-4999', NULL, NULL, NULL, 1, '2021-01-27 05:15:16', '2021-01-27 05:15:16', NULL),
(283, 13, 42, '5000 and above', '5000-and-above', 'childcategory/1611764195.jpg', 'childcategory/1611764195.jpg', '5000 and above', NULL, NULL, NULL, 1, '2021-01-27 05:17:02', '2021-01-27 05:17:02', NULL),
(284, 13, 43, 'Upto 10% OFF', 'upto-10-off', 'childcategory/1611764655.jpg', 'childcategory/1611764655.jpg', 'Upto 10% OFF', NULL, NULL, NULL, 1, '2021-01-27 05:24:56', '2021-01-27 05:24:56', NULL),
(285, 13, 43, '11-20% OFF', '11-20-off', 'childcategory/1611764757.jpg', 'childcategory/1611764757.jpg', '11-20% OFF', NULL, NULL, NULL, 1, '2021-01-27 05:26:27', '2021-01-27 05:26:27', NULL),
(286, 13, 43, '21-30% OFF', '21-30-off', 'childcategory/1611764874.jpg', 'childcategory/1611764874.jpg', '21-30% OFF', NULL, NULL, NULL, 1, '2021-01-27 05:28:25', '2021-01-27 05:28:25', NULL),
(287, 13, 43, '31-40% OFF', '31-40-off', 'childcategory/1611764983.jpg', 'childcategory/1611764983.jpg', '31-40% OFF', NULL, NULL, NULL, 1, '2021-01-27 05:30:14', '2021-01-27 05:30:14', NULL),
(288, 13, 43, '41-50% OFF', '41-50-off', 'childcategory/1611765092.jpg', 'childcategory/1611765092.jpg', '41-50% OFF', NULL, NULL, NULL, 1, '2021-01-27 05:32:08', '2021-01-27 05:32:08', NULL),
(289, 13, 43, 'Above 50% OFF', 'above-50-off', 'childcategory/1611765194.jpg', 'childcategory/1611765194.jpg', 'Above 50% OFF', NULL, NULL, NULL, 1, '2021-01-27 05:34:08', '2021-01-27 05:34:08', NULL),
(290, 7, 44, 'Soaps & Shampoos', 'soaps-shampoos', 'childcategory/1611766619.jpg', 'childcategory/1611766619.jpg', 'Soaps & Shampoos', NULL, NULL, NULL, 1, '2021-01-27 05:57:22', '2021-01-27 05:57:22', NULL),
(291, 7, 44, 'Lotions & Powders', 'lotions-powders', 'childcategory/1611766715.jpg', 'childcategory/1611766715.jpg', 'Lotions & Powders', NULL, NULL, NULL, 1, '2021-01-27 05:59:07', '2021-01-27 05:59:07', NULL),
(292, 7, 44, 'Hair & Massage Oils', 'hair-massage-oils', 'childcategory/1611766803.jpg', 'childcategory/1611766803.jpg', 'Hair & Massage Oils', NULL, NULL, NULL, 1, '2021-01-27 06:00:33', '2021-01-27 06:00:33', NULL),
(293, 7, 44, 'Tooth Brushes & Pastes', 'tooth-brushes-pastes', 'childcategory/1611766914.jpg', 'childcategory/1611766914.jpg', 'Tooth Brushes & Pastes', NULL, NULL, NULL, 1, '2021-01-27 06:02:18', '2021-01-27 06:02:18', NULL),
(294, 7, 44, 'Creams & Ointments', 'creams-ointments', 'childcategory/1611767005.jpg', 'childcategory/1611767005.jpg', 'Creams & Ointments', NULL, NULL, NULL, 1, '2021-01-27 06:03:48', '2021-01-27 06:03:48', NULL),
(295, 7, 45, 'Diapers', 'diapers', 'childcategory/1611767748.jpg', 'childcategory/1611767748.jpg', 'Diapers', NULL, NULL, NULL, 1, '2021-01-27 06:17:01', '2021-01-27 06:17:01', NULL),
(296, 7, 45, 'Wet Wipes', 'wet-wipes', 'childcategory/1611767934.jpg', 'childcategory/1611767934.jpg', 'Wet Wipes', NULL, NULL, NULL, 1, '2021-01-27 06:26:59', '2021-01-27 06:26:59', NULL),
(297, 7, 46, 'New born (Upto 1 year)', 'new-born-upto-1-year', 'childcategory/1611733249.jpg', 'childcategory/1611211150.jpg', 'New born (Upto 1 year)', NULL, NULL, NULL, 1, '2021-01-27 07:20:34', '2021-01-27 07:20:34', NULL),
(298, 7, 46, '1-4 Years', '1-4-years', 'childcategory/1611771728.jpg', 'childcategory/1611771728.jpg', '1-4 Years', NULL, NULL, NULL, 1, '2021-01-27 07:22:38', '2021-01-27 07:22:38', NULL),
(299, 7, 46, '5-8 Years', '5-8-years', 'childcategory/1611771815.jpg', 'childcategory/1611771815.jpg', '5-8 Years', NULL, NULL, NULL, 1, '2021-01-27 07:23:53', '2021-01-27 07:23:53', NULL),
(300, 7, 46, '9-12 Years', '9-12-years', 'childcategory/1611771902.jpg', 'childcategory/1611771902.jpg', '9-12 Years', NULL, NULL, NULL, 1, '2021-01-27 07:25:25', '2021-01-27 07:25:25', NULL),
(301, 7, 46, '12+ Years', '12-years', 'childcategory/1611772000.jpg', 'childcategory/1611772000.jpg', '12+ Years', NULL, NULL, NULL, 1, '2021-01-27 07:27:04', '2021-01-27 07:27:04', NULL),
(302, 7, 47, 'Upto 99', 'upto-99', 'childcategory/1611787512.jpg', 'childcategory/1611787512.jpg', 'Upto 99', NULL, NULL, NULL, 1, '2021-01-27 11:45:44', '2021-01-27 11:45:44', NULL),
(303, 7, 47, '100-299', '100-299', 'childcategory/1611787591.jpg', 'childcategory/1611787591.jpg', '100-299', NULL, NULL, NULL, 1, '2021-01-27 11:46:53', '2021-01-27 11:46:53', NULL),
(304, 7, 47, '300-599', '300-599', 'childcategory/1611787661.jpg', 'childcategory/1611787661.jpg', '300-599', NULL, NULL, NULL, 1, '2021-01-27 11:48:00', '2021-01-27 11:48:00', NULL),
(305, 7, 47, '600-999', '600-999', 'childcategory/1611787730.jpg', 'childcategory/1611787730.jpg', '600-999', NULL, NULL, NULL, 1, '2021-01-27 11:49:09', '2021-01-27 11:49:09', NULL),
(306, 7, 47, '1000-1999', '1000-1999', 'childcategory/1611755955.jpg', 'childcategory/1611755955.jpg', '1000-1999', NULL, NULL, NULL, 1, '2021-01-27 11:51:35', '2021-01-27 11:51:35', NULL),
(307, 7, 47, '2000-2999', '2000-2999', 'childcategory/1611787944.jpg', 'childcategory/1611787944.jpg', '2000-2999', NULL, NULL, NULL, 1, '2021-01-27 11:52:43', '2021-01-27 11:52:43', NULL),
(308, 7, 48, 'Upto 10% OFF', 'upto-10-off', 'childcategory/1611788258.jpg', 'childcategory/1611788258.jpg', 'Upto 10% OFF', NULL, NULL, NULL, 1, '2021-01-27 11:57:55', '2021-01-27 11:57:55', NULL),
(309, 7, 47, '3000-4999', '3000-4999', 'childcategory/1611764097.jpg', 'childcategory/1611742014.jpg', '3000-4999', NULL, NULL, NULL, 1, '2021-01-27 11:58:41', '2021-01-27 11:58:41', NULL),
(310, 7, 47, '5000 and above', '5000-and-above', 'childcategory/1611764195.jpg', 'childcategory/1611764195.jpg', '5000 and above', NULL, NULL, NULL, 1, '2021-01-27 11:59:37', '2021-01-27 11:59:37', NULL),
(311, 7, 48, '11-20% OFF', '11-20-off', 'childcategory/1611764757.jpg', 'childcategory/1611764757.jpg', '11-20% OFF', NULL, NULL, NULL, 1, '2021-01-27 12:00:55', '2021-01-27 12:00:55', NULL),
(312, 7, 48, '21-30% OFF', '21-30-off', 'childcategory/1611508183.jpg', 'childcategory/1611764874.jpg', '21-30% OFF', NULL, NULL, NULL, 1, '2021-01-27 12:01:49', '2021-01-27 12:01:49', NULL),
(313, 7, 48, '31-40% OFF', '31-40-off', 'childcategory/1611764983.jpg', 'childcategory/1611764983.jpg', '31-40% OFF', NULL, NULL, NULL, 1, '2021-01-27 12:02:51', '2021-01-27 12:02:51', NULL),
(314, 7, 48, '41-50% OFF', '41-50-off', 'childcategory/1611765092.jpg', 'childcategory/1611765092.jpg', '41-50% OFF', NULL, NULL, NULL, 1, '2021-01-27 12:03:29', '2021-01-27 12:03:29', NULL),
(315, 7, 48, 'Above 50% OFF', 'above-50-off', 'childcategory/1611765194.jpg', 'childcategory/1611765194.jpg', 'Above 50% OFF', NULL, NULL, NULL, 1, '2021-01-27 12:04:08', '2021-01-27 12:04:08', NULL),
(316, 5, 49, 'Cleansers & Detergents', 'cleansers-detergents', 'childcategory/1611789026.jpg', 'childcategory/1611789026.jpg', 'Cleansers & Detergents', NULL, NULL, NULL, 1, '2021-01-27 12:10:44', '2021-01-27 12:10:44', NULL),
(317, 5, 49, 'First Aid', 'first-aid', 'childcategory/1611789099.jpg', 'childcategory/1611789099.jpg', 'First Aid', NULL, NULL, NULL, 1, '2021-01-27 12:11:58', '2021-01-27 12:11:58', NULL),
(318, 5, 49, 'Masks & Shields', 'masks-shields', 'childcategory/1611789178.jpg', 'childcategory/1611789178.jpg', 'Masks & Shields', NULL, NULL, NULL, 1, '2021-01-27 12:13:17', '2021-01-27 12:13:17', NULL),
(319, 5, 49, 'Creams & Ointments', 'creams-ointments', 'childcategory/1611789259.jpg', 'childcategory/1611789259.jpg', 'Creams & Ointments', NULL, NULL, NULL, 1, '2021-01-27 12:14:47', '2021-01-27 12:14:47', NULL),
(320, 5, 49, 'Repellants', 'repellants', 'childcategory/1611789347.jpg', 'childcategory/1611789347.jpg', 'Repellants', NULL, NULL, NULL, 1, '2021-01-27 12:16:43', '2021-01-27 12:16:43', NULL),
(321, 5, 49, 'Thermometers', 'thermometers', 'childcategory/1611789473.jpg', 'childcategory/1611789473.jpg', 'Thermometers', NULL, NULL, NULL, 1, '2021-01-27 12:18:09', '2021-01-27 12:18:09', NULL),
(322, 5, 49, 'Nebulizers', 'nebulizers', 'childcategory/1611789545.jpg', 'childcategory/1611789545.jpg', 'Nebulizers', NULL, NULL, NULL, 1, '2021-01-27 12:19:23', '2021-01-27 12:19:23', NULL),
(323, 5, 49, 'Handwash & Sanitizers', 'handwash-sanitizers', 'childcategory/1611789643.jpg', 'childcategory/1611789643.jpg', 'Handwash & Sanitizers', NULL, NULL, NULL, 1, '2021-01-27 12:21:08', '2021-01-27 12:21:08', NULL),
(324, 5, 49, 'Child Guards & Safety', 'child-guards-safety', 'childcategory/1611789729.jpg', 'childcategory/1611789729.jpg', 'Child Guards & Safety', NULL, NULL, NULL, 1, '2021-01-27 12:22:24', '2021-01-27 12:22:24', NULL),
(325, 5, 49, 'Gas Relievers', 'gas-relievers', 'childcategory/1611789967.jpg', 'childcategory/1611789967.jpg', 'Gas Relievers', NULL, NULL, NULL, 1, '2021-01-27 12:26:29', '2021-01-27 12:26:29', NULL),
(326, 5, 49, 'Upto 1 year', 'upto-1-year', 'childcategory/1611318707.jpg', 'childcategory/1611733249.jpg', 'Upto 1 year', NULL, NULL, NULL, 1, '2021-01-27 12:44:56', '2021-01-27 12:44:56', NULL),
(327, 5, 50, 'Up to 1 year', 'up-to-1-year', 'childcategory/1611733249.jpg', 'childcategory/1611733249.jpg', 'Up to 1 year', NULL, NULL, NULL, 1, '2021-01-27 12:46:11', '2021-01-27 12:51:43', NULL),
(328, 5, 50, '1-4 Years', '1-4-years', 'childcategory/1611771815.jpg', 'childcategory/1611771815.jpg', '1-4 Years', NULL, NULL, NULL, 1, '2021-01-27 12:47:04', '2021-01-27 12:52:48', NULL),
(329, 5, 50, '5-8 Years', '5-8-years', 'childcategory/1611771902.jpg', 'childcategory/1611771902.jpg', '5-8 Years', NULL, NULL, NULL, 1, '2021-01-27 12:47:36', '2021-01-27 12:53:45', NULL),
(330, 5, 50, '9-12 Years', '9-12-years', 'childcategory/1611771902.jpg', 'childcategory/1611771902.jpg', '9-12 Years', NULL, NULL, NULL, 1, '2021-01-27 12:48:09', '2021-01-27 12:54:35', NULL),
(331, 5, 50, '12+ Years', '12-years', 'childcategory/1611772000.jpg', 'childcategory/1611772000.jpg', '12+ Years', NULL, NULL, NULL, 1, '2021-01-27 12:55:11', '2021-01-27 12:55:11', NULL),
(332, 5, 51, 'Upto 99', 'upto-99', 'childcategory/1611787512.jpg', 'childcategory/1611787512.jpg', 'Upto 99', NULL, NULL, NULL, 1, '2021-01-27 13:00:19', '2021-01-27 13:00:19', NULL),
(333, 5, 51, '100-299', '100-299', 'childcategory/1611787591.jpg', 'childcategory/1611787591.jpg', '100-299', NULL, NULL, NULL, 1, '2021-01-27 13:01:35', '2021-01-27 13:01:35', NULL),
(334, 5, 51, '300-599', '300-599', 'childcategory/1611787661.jpg', 'childcategory/1611787661.jpg', '300-599', NULL, NULL, NULL, 1, '2021-01-27 13:02:12', '2021-01-27 13:02:12', NULL),
(335, 5, 51, '600-999', '600-999', 'childcategory/1611787730.jpg', 'childcategory/1611787730.jpg', '600-999', NULL, NULL, NULL, 1, '2021-01-27 13:02:51', '2021-01-27 13:02:51', NULL),
(336, 5, 51, '1000-1999', '1000-1999', 'childcategory/1611755955.jpg', 'childcategory/1611755955.jpg', '1000-1999', NULL, NULL, NULL, 1, '2021-01-27 13:03:28', '2021-01-27 13:03:28', NULL),
(337, 5, 51, '2000-2999', '2000-2999', 'childcategory/1611787944.jpg', 'childcategory/1611787944.jpg', '2000-2999', NULL, NULL, NULL, 1, '2021-01-27 13:03:57', '2021-01-27 13:03:57', NULL),
(338, 5, 51, '3000-4999', '3000-4999', 'childcategory/1611764097.jpg', 'childcategory/1611764097.jpg', '3000-4999', NULL, NULL, NULL, 1, '2021-01-27 13:04:30', '2021-01-27 13:04:30', NULL),
(339, 5, 51, '5000 and above', '5000-and-above', 'childcategory/1611742160.jpg', 'childcategory/1611764195.jpg', '5000 and above', NULL, NULL, NULL, 1, '2021-01-27 13:05:31', '2021-01-27 13:05:31', NULL),
(340, 5, 52, 'Upto 10% OFF', 'upto-10-off', 'childcategory/1611788258.jpg', 'childcategory/1611764655.jpg', 'Upto 10% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:11:40', '2021-01-27 13:11:40', NULL),
(341, 5, 52, '11-20% OFF', '11-20-off', 'childcategory/1611742845.jpg', 'childcategory/1611764757.jpg', '11-20% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:12:30', '2021-01-27 13:12:30', NULL),
(342, 5, 52, '21-30% OFF', '21-30-off', 'childcategory/1611764874.jpg', 'childcategory/1611764874.jpg', '21-30% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:13:05', '2021-01-27 13:13:05', NULL),
(343, 5, 52, '31-40% OFF', '31-40-off', 'childcategory/1611764983.jpg', 'childcategory/1611764983.jpg', '31-40% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:13:45', '2021-01-27 13:13:45', NULL),
(344, 5, 52, '41-50% OFF', '41-50-off', 'childcategory/1611765092.jpg', 'childcategory/1611765092.jpg', '41-50% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:14:15', '2021-01-27 13:14:15', NULL),
(345, 5, 52, 'Above 50% OFF', 'above-50-off', 'childcategory/1611765194.jpg', 'childcategory/1611765194.jpg', 'Above 50% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:14:54', '2021-01-27 13:14:54', NULL),
(346, 14, 53, 'Cricket', 'cricket', 'childcategory/1611793390.jpg', 'childcategory/1611793390.jpg', 'Cricket', NULL, NULL, NULL, 1, '2021-01-27 13:23:28', '2021-01-27 13:23:28', NULL),
(347, 14, 53, 'VolleyBall', 'volleyball', 'childcategory/1611793475.jpg', 'childcategory/1611793475.jpg', 'VolleyBall', NULL, NULL, NULL, 1, '2021-01-27 13:24:53', '2021-01-27 13:24:53', NULL),
(348, 14, 53, 'BasketBall', 'basketball', 'childcategory/1611793540.jpg', 'childcategory/1611793540.jpg', 'BasketBall', NULL, NULL, NULL, 1, '2021-01-27 13:25:55', '2021-01-27 13:25:55', NULL),
(349, 14, 53, 'Tennis', 'tennis', 'childcategory/1611793683.jpg', 'childcategory/1611793683.jpg', 'Tennis', NULL, NULL, NULL, 1, '2021-01-27 13:28:24', '2021-01-27 13:28:24', NULL),
(350, 14, 53, 'Badminton', 'badminton', 'childcategory/1611793757.jpg', 'childcategory/1611793757.jpg', 'Badminton', NULL, NULL, NULL, 1, '2021-01-27 13:29:38', '2021-01-27 13:29:38', NULL),
(351, 14, 53, 'Table Tennis', 'table-tennis', 'childcategory/1611793856.jpg', 'childcategory/1611793856.jpg', 'Table Tennis', NULL, NULL, NULL, 1, '2021-01-27 13:31:12', '2021-01-27 13:31:12', NULL),
(352, 14, 53, 'Swimming', 'swimming', 'childcategory/1611793925.jpg', 'childcategory/1611793925.jpg', 'Swimming', NULL, NULL, NULL, 1, '2021-01-27 13:32:20', '2021-01-27 13:32:20', NULL),
(353, 14, 53, 'Skates & Scooters', 'skates-scooters', 'childcategory/1611794002.jpg', 'childcategory/1611794002.jpg', 'Skates & Scooters', NULL, NULL, NULL, 1, '2021-01-27 13:33:38', '2021-01-27 13:33:38', NULL),
(354, 14, 53, 'Carroms and Chess', 'carroms-and-chess', 'childcategory/1611794119.jpg', 'childcategory/1611794119.jpg', 'Carroms and Chess', NULL, NULL, NULL, 1, '2021-01-27 13:35:35', '2021-01-27 13:35:35', NULL),
(355, 14, 53, 'Others', 'others', 'childcategory/1611794180.jpg', 'childcategory/1611794180.jpg', 'Others', NULL, NULL, NULL, 1, '2021-01-27 13:36:41', '2021-01-27 13:36:41', NULL),
(356, 14, 54, 'Upto 1 year', 'upto-1-year', 'childcategory/1611211150.jpg', 'childcategory/1611733249.jpg', 'Upto 1 year', NULL, NULL, NULL, 1, '2021-01-27 13:41:12', '2021-01-27 13:41:12', NULL),
(357, 14, 54, '1-4 Years', '1-4-years', 'childcategory/1611771728.jpg', 'childcategory/1611771728.jpg', '1-4 Years', NULL, NULL, NULL, 1, '2021-01-27 13:41:50', '2021-01-27 13:41:50', NULL),
(358, 14, 54, '5-8 Years', '5-8-years', 'childcategory/1611771815.jpg', 'childcategory/1611771815.jpg', '5-8 Years', NULL, NULL, NULL, 1, '2021-01-27 13:42:27', '2021-01-27 13:42:27', NULL),
(359, 14, 54, '9-12 Years', '9-12-years', 'childcategory/1611771902.jpg', 'childcategory/1611771902.jpg', '9-12 Years', NULL, NULL, NULL, 1, '2021-01-27 13:42:59', '2021-01-27 13:42:59', NULL),
(360, 14, 54, '12+ Years', '12-years', 'childcategory/1611772000.jpg', 'childcategory/1611772000.jpg', '12+ Years', NULL, NULL, NULL, 1, '2021-01-27 13:43:35', '2021-01-27 13:43:35', NULL),
(361, 14, 55, 'Upto 99', 'upto-99', 'childcategory/1611787512.jpg', 'childcategory/1611787512.jpg', 'Upto 99', NULL, NULL, NULL, 1, '2021-01-27 13:47:11', '2021-01-27 13:47:11', NULL),
(362, 14, 55, '100-299', '100-299', 'childcategory/1611787591.jpg', 'childcategory/1611787591.jpg', '100-299', NULL, NULL, NULL, 1, '2021-01-27 13:47:42', '2021-01-27 13:47:42', NULL),
(363, 14, 55, '300-599', '300-599', 'childcategory/1611787661.jpg', 'childcategory/1611787661.jpg', '300-599', NULL, NULL, NULL, 1, '2021-01-27 13:48:09', '2021-01-27 13:48:09', NULL),
(364, 14, 55, '600-999', '600-999', 'childcategory/1611787730.jpg', 'childcategory/1611787730.jpg', '600-999', NULL, NULL, NULL, 1, '2021-01-27 13:48:36', '2021-01-27 13:48:36', NULL),
(365, 14, 55, '1000-1999', '1000-1999', 'childcategory/1611755955.jpg', 'childcategory/1611755955.jpg', '1000-1999', NULL, NULL, NULL, 1, '2021-01-27 13:49:15', '2021-01-27 13:49:15', NULL),
(366, 14, 55, '2000-2999', '2000-2999', 'childcategory/1611787944.jpg', 'childcategory/1611787944.jpg', '2000-2999', NULL, NULL, NULL, 1, '2021-01-27 13:49:45', '2021-01-27 13:49:45', NULL),
(367, 14, 55, '3000-4999', '3000-4999', 'childcategory/1611764097.jpg', 'childcategory/1611764097.jpg', '3000-4999', NULL, NULL, NULL, 1, '2021-01-27 13:50:26', '2021-01-27 13:50:26', NULL),
(368, 14, 55, '5000 and above', '5000-and-above', 'childcategory/1611764195.jpg', 'childcategory/1611764195.jpg', '5000 and above', NULL, NULL, NULL, 1, '2021-01-27 13:51:08', '2021-01-27 13:51:08', NULL),
(369, 14, 56, 'Upto 10% OFF', 'upto-10-off', 'childcategory/1611764655.jpg', 'childcategory/1611764655.jpg', 'Upto 10% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:54:28', '2021-01-27 13:54:28', NULL),
(370, 14, 56, '11-20% OFF', '11-20-off', 'childcategory/1611764757.jpg', 'childcategory/1611764757.jpg', '11-20% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:55:04', '2021-01-27 13:55:04', NULL),
(371, 14, 56, '21-30% OFF', '21-30-off', 'childcategory/1611764874.jpg', 'childcategory/1611764874.jpg', '21-30% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:55:53', '2021-01-27 13:55:53', NULL),
(372, 14, 56, '31-40% OFF', '31-40-off', 'childcategory/1611764983.jpg', 'childcategory/1611764983.jpg', '31-40% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:56:28', '2021-01-27 13:56:28', NULL),
(373, 14, 56, '41-50% OFF', '41-50-off', 'childcategory/1611765092.jpg', 'childcategory/1611765092.jpg', '41-50% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:56:59', '2021-01-27 13:56:59', NULL),
(374, 14, 56, 'Above 50% OFF', 'above-50-off', 'childcategory/1611765194.jpg', 'childcategory/1611765194.jpg', 'Above 50% OFF', NULL, NULL, NULL, 1, '2021-01-27 13:57:41', '2021-01-27 13:57:41', NULL),
(375, 16, 57, 'Boy Clothing', 'boy-clothing', 'childcategory/1611810586.jpg', 'childcategory/1611810586.jpg', 'Boy Clothing', NULL, NULL, NULL, 1, '2021-01-27 18:10:05', '2021-01-27 18:10:05', NULL),
(376, 16, 57, 'Girl Clothing', 'girl-clothing', 'childcategory/1611810648.jpg', 'childcategory/1611810648.jpg', 'Girl Clothing', NULL, NULL, NULL, 1, '2021-01-27 18:11:07', '2021-01-27 18:11:07', NULL),
(377, 16, 57, 'Food & Nutrition', 'food-nutrition', 'childcategory/1611810728.jpg', 'childcategory/1611810728.jpg', 'Food & Nutrition', NULL, NULL, NULL, 1, '2021-01-27 18:12:23', '2021-01-27 18:12:23', NULL),
(378, 16, 57, 'Personal Care', 'personal-care', 'childcategory/1611810843.jpg', 'childcategory/1611810843.jpg', 'Personal Care', NULL, NULL, NULL, 1, '2021-01-27 18:14:24', '2021-01-27 18:14:24', NULL),
(379, 16, 57, 'Toys', 'toys', 'childcategory/1611810938.jpg', 'childcategory/1611810938.jpg', 'Toys', NULL, NULL, NULL, 1, '2021-01-27 18:16:05', '2021-01-27 18:16:05', NULL),
(380, 16, 58, 'Upto 1 year', 'upto-1-year', 'childcategory/1611318707.jpg', 'childcategory/1611744195.jpg', 'Upto 1 year', NULL, NULL, NULL, 1, '2021-01-27 18:24:05', '2021-01-27 18:24:05', NULL),
(381, 16, 58, '1-4 Years', '1-4-years', 'childcategory/1611771728.jpg', 'childcategory/1611771728.jpg', '1-4 Years', NULL, NULL, NULL, 1, '2021-01-27 18:24:54', '2021-01-27 18:24:54', NULL),
(382, 16, 58, '5-8 Years', '5-8-years', 'childcategory/1611771815.jpg', 'childcategory/1611771815.jpg', '5-8 Years', NULL, NULL, NULL, 1, '2021-01-27 18:25:29', '2021-01-27 18:25:29', NULL),
(383, 16, 58, '9-12 Years', '9-12-years', 'childcategory/1611771902.jpg', 'childcategory/1611771902.jpg', '9-12 Years', NULL, NULL, NULL, 1, '2021-01-27 18:26:04', '2021-01-27 18:26:04', NULL),
(384, 16, 58, '12+ Years', '12-years', 'childcategory/1611772000.jpg', 'childcategory/1611772000.jpg', '12+ Years', NULL, NULL, NULL, 1, '2021-01-27 18:27:54', '2021-01-27 18:27:54', NULL),
(385, 16, 59, 'Upto 99', 'upto-99', 'childcategory/1611787512.jpg', 'childcategory/1611787512.jpg', 'Upto 99', NULL, NULL, NULL, 1, '2021-01-27 18:31:40', '2021-01-27 18:31:40', NULL),
(386, 16, 59, '100-299', '100-299', 'childcategory/1611787591.jpg', 'childcategory/1611787591.jpg', '100-299', NULL, NULL, NULL, 1, '2021-01-27 18:32:21', '2021-01-27 18:32:21', NULL),
(387, 16, 59, '300-599', '300-599', 'childcategory/1611506320.jpg', 'childcategory/1611787661.jpg', '300-599', NULL, NULL, NULL, 1, '2021-01-27 18:33:02', '2021-01-27 18:33:02', NULL),
(388, 16, 59, '600-999', '600-999', 'childcategory/1611298967.jpg', 'childcategory/1611755750.jpg', '600-999', NULL, NULL, NULL, 1, '2021-01-27 18:45:18', '2021-01-27 18:45:18', NULL),
(389, 16, 59, '1000-1999', '1000-1999', 'childcategory/1611755955.jpg', 'childcategory/1611755955.jpg', '1000-1999', NULL, NULL, NULL, 1, '2021-01-27 18:46:36', '2021-01-27 18:46:36', NULL),
(390, 16, 59, '2000-2999', '2000-2999', 'childcategory/1611787944.jpg', 'childcategory/1611787944.jpg', '2000-2999', NULL, NULL, NULL, 1, '2021-01-27 18:48:48', '2021-01-27 18:48:48', NULL),
(391, 16, 59, '3000-4999', '3000-4999', 'childcategory/1611764097.jpg', 'childcategory/1611764097.jpg', '3000-4999', NULL, NULL, NULL, 1, '2021-01-27 18:49:31', '2021-01-27 18:49:31', NULL),
(392, 16, 59, '5000 and above', '5000-and-above', 'childcategory/1611299138.jpg', 'childcategory/1611299138.jpg', '5000 and above', NULL, NULL, NULL, 1, '2021-01-27 18:52:42', '2021-01-27 18:52:42', NULL),
(393, 16, 60, 'Upto 10% OFF', 'upto-10-off', 'childcategory/1611764655.jpg', 'childcategory/1611764655.jpg', 'Upto 10% OFF', NULL, NULL, NULL, 1, '2021-01-27 19:00:37', '2021-01-27 19:00:37', NULL),
(394, 16, 60, '11-20% OFF', '11-20-off', 'childcategory/1611764757.jpg', 'childcategory/1611764757.jpg', '11-20% OFF', NULL, NULL, NULL, 1, '2021-01-27 19:01:11', '2021-01-27 19:01:11', NULL),
(395, 16, 60, '21-30% OFF', '21-30-off', 'childcategory/1611764874.jpg', 'childcategory/1611764874.jpg', '21-30% OFF', NULL, NULL, NULL, 1, '2021-01-27 19:01:56', '2021-01-27 19:01:56', NULL),
(396, 16, 60, '31-40% OFF', '31-40-off', 'childcategory/1611764983.jpg', 'childcategory/1611764983.jpg', '31-40% OFF', NULL, NULL, NULL, 1, '2021-01-27 19:03:17', '2021-01-27 19:03:17', NULL),
(397, 16, 60, '41-50% OFF', '41-50-off', 'childcategory/1611765092.jpg', 'childcategory/1611765092.jpg', '41-50% OFF', NULL, NULL, NULL, 1, '2021-01-27 19:04:27', '2021-01-27 19:04:27', NULL),
(398, 16, 60, 'Above 50% OFF', 'above-50-off', 'childcategory/1611765194.jpg', 'childcategory/1611765194.jpg', 'Above 50% OFF', NULL, NULL, NULL, 1, '2021-01-27 19:05:06', '2021-01-27 19:05:06', NULL),
(399, 7, 44, 'Grooming Accessories', 'grooming-accessories', 'childcategory/1611814349.jpg', 'childcategory/1611814349.jpg', 'Grooming Accessories', NULL, NULL, NULL, 1, '2021-01-27 19:12:52', '2021-01-27 19:12:52', NULL),
(400, 7, 44, 'Beauty & More', 'beauty-more', 'childcategory/1611814239.jpg', 'childcategory/1611814239.jpg', 'Beauty & More', NULL, NULL, NULL, 1, '2021-01-27 19:19:54', '2021-01-27 19:19:54', NULL),
(401, 14, 53, 'FootBall', 'football', 'childcategory/1611814903.jpg', 'childcategory/1611814903.jpg', 'FootBall', NULL, NULL, NULL, 1, '2021-01-27 19:21:57', '2021-01-27 19:21:57', NULL),
(402, 5, 49, 'Cold Relievers', 'cold-relievers', 'childcategory/1611814978.jpg', 'childcategory/1611814978.jpg', 'Cold Relievers', NULL, NULL, NULL, 1, '2021-01-27 19:23:33', '2021-01-27 19:23:33', NULL),
(403, 1, 1, 'Swaddles & Wraps', 'swaddles-wraps', 'childcategory/1611815960.jpg', 'childcategory/1611815960.jpg', 'Swaddles & Wraps', NULL, NULL, NULL, 1, '2021-01-27 19:39:38', '2021-01-27 19:39:38', NULL),
(404, 1, 2, 'Swaddles & Wraps', 'swaddles-wraps', 'childcategory/1611815960.jpg', 'childcategory/1611815960.jpg', 'Swaddles & Wraps', NULL, NULL, NULL, 1, '2021-01-27 19:44:15', '2021-02-06 14:54:41', NULL),
(405, 10, 25, 'Key Chains', 'key-chains', 'childcategory/1611818012.jpg', 'childcategory/1611818012.jpg', 'Key Chains', NULL, NULL, NULL, 1, '2021-01-27 20:13:43', '2021-01-27 20:13:43', NULL),
(406, 10, 26, 'Key Chains', 'key-chains', 'childcategory/1611818216.jpg', 'childcategory/1611818216.jpg', 'Key Chains', NULL, NULL, NULL, 1, '2021-01-27 20:17:14', '2021-01-27 20:17:14', NULL),
(407, 10, 17, 'Food Processors', 'food-processors', 'childcategory/1611818730.jpg', 'childcategory/1611818730.jpg', 'Food Processors', NULL, NULL, NULL, 1, '2021-01-27 20:26:20', '2021-01-27 20:26:20', NULL),
(408, 10, 21, 'Dry Sheets', 'dry-sheets', 'childcategory/1611819579.jpg', 'childcategory/1611819579.jpg', 'Dry Sheets', NULL, NULL, NULL, 1, '2021-01-27 20:39:56', '2021-01-27 20:39:56', NULL),
(409, 10, 61, 'Bags & More', 'bags-more', 'childcategory/1611821228.jpg', 'childcategory/1611821228.jpg', 'Bags & More', NULL, NULL, NULL, 1, '2021-01-27 21:07:41', '2021-02-08 18:36:48', NULL),
(410, 10, 61, 'Water Bottles', 'water-bottles', 'childcategory/1611821286.jpg', 'childcategory/1611821286.jpg', 'Water Bottles', NULL, NULL, NULL, 1, '2021-01-27 21:08:24', '2021-01-27 21:08:24', NULL),
(411, 10, 61, 'Lunch Boxes', 'lunch-boxes', 'childcategory/1611821338.jpg', 'childcategory/1611821338.jpg', 'Lunch Boxes', NULL, NULL, NULL, 1, '2021-01-27 21:09:14', '2021-01-27 21:09:14', NULL),
(412, 1, 1, 'Combos', 'combos', 'childcategory/1612648454.jpg', 'childcategory/1612648454.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-28 23:26:00', '2021-02-06 21:54:35', NULL),
(413, 1, 2, 'Combos', 'combos', 'childcategory/1612648537.jpg', 'childcategory/1612648537.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-28 23:28:13', '2021-02-06 21:56:04', NULL),
(414, 2, 27, 'Combos', 'combos', 'childcategory/1612648634.jpg', 'childcategory/1612648634.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:06:14', '2021-02-06 21:57:36', NULL),
(415, 2, 28, 'Combos', 'combos', 'childcategory/1612648728.jpg', 'childcategory/1612648728.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:08:43', '2021-02-06 21:59:13', NULL),
(416, 10, 25, 'Combos', 'combos', 'childcategory/1612648815.jpg', 'childcategory/1612648815.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:10:50', '2021-02-06 22:00:53', NULL),
(417, 10, 26, 'Combos', 'combos', 'childcategory/1612648923.jpg', 'childcategory/1612648923.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:12:54', '2021-02-06 22:02:21', NULL),
(418, 10, 17, 'Combos', 'combos', 'childcategory/1612649019.jpg', 'childcategory/1612649019.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:17:39', '2021-02-06 22:03:58', NULL),
(419, 10, 18, 'Combos', 'combos', 'childcategory/1612649142.jpg', 'childcategory/1612649142.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:20:18', '2021-02-06 22:06:01', NULL),
(420, 10, 21, 'Combos', 'combos', 'childcategory/1612674187.jpg', 'childcategory/1612674187.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:32:21', '2021-02-07 05:03:32', NULL),
(421, 10, 20, 'Combos', 'combos', 'childcategory/1612649649.jpg', 'childcategory/1612649649.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:34:04', '2021-02-06 22:14:30', NULL),
(422, 10, 61, 'Combos', 'combos', 'childcategory/1612649770.jpg', 'childcategory/1612649770.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:36:02', '2021-02-06 22:16:34', NULL),
(423, 8, 33, 'Combos', 'combos', 'childcategory/1612649878.jpg', 'childcategory/1612649878.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:37:59', '2021-02-06 22:18:36', NULL),
(424, 8, 37, 'Combos', 'combos', 'childcategory/1612649992.jpg', 'childcategory/1612649992.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:39:42', '2021-02-06 22:20:12', NULL),
(425, 13, 38, 'Combos', 'combos', 'childcategory/1612650094.jpg', 'childcategory/1612650094.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:42:40', '2021-02-06 22:21:53', NULL),
(426, 13, 39, 'Combos', 'combos', 'childcategory/1612650195.jpg', 'childcategory/1612650195.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:44:08', '2021-02-06 22:23:35', NULL),
(427, 13, 40, 'Combos', 'combos', 'childcategory/1612650280.jpg', 'childcategory/1612650280.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:45:31', '2021-02-06 22:25:00', NULL),
(428, 7, 44, 'Combos', 'combos', 'childcategory/1612650358.jpg', 'childcategory/1612650358.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:48:10', '2021-02-06 22:26:17', NULL),
(429, 5, 49, 'Combos', 'combos', 'childcategory/1612650440.jpg', 'childcategory/1612650440.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:49:53', '2021-02-06 22:27:40', NULL),
(430, 14, 53, 'Combos', 'combos', 'childcategory/1612650517.jpg', 'childcategory/1612650517.jpg', 'Combos', NULL, NULL, NULL, 1, '2021-01-29 00:51:33', '2021-02-06 22:28:55', NULL),
(431, 10, 17, 'Feeders & Utensils', 'feeders-utensils', 'childcategory/1612427579.jpg', 'childcategory/1612427579.jpg', 'Feeders & Utensils', NULL, NULL, NULL, 1, '2021-02-04 03:03:18', '2021-02-04 03:03:18', NULL),
(432, 1, 1, 'Jump Suits & Dungarees', 'jump-suits-dungarees', 'childcategory/1612877743.jpg', 'childcategory/1612877743.jpg', 'Jump Suits & Dungarees', NULL, NULL, NULL, 1, '2021-02-09 13:29:27', '2021-02-09 13:36:07', NULL),
(433, 10, 61, 'Back Packs', 'back-packs', 'avatar01e41a921550194822eaa8bdcacdbca1.jpg', 'avatar06e9b2cb45589b5089a296735824d751.jpg', 'Back Packs', NULL, NULL, NULL, 1, '2021-02-11 02:43:48', '2021-02-11 02:43:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int NOT NULL,
  `seller_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `from_user` int DEFAULT NULL,
  `to_user` int DEFAULT NULL,
  `message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `conversations`
--

INSERT INTO `conversations` (`id`, `seller_id`, `user_id`, `from_user`, `to_user`, `message`, `created_at`, `updated_at`) VALUES
(1, 15, 84, 84, 15, 'hello', '2021-01-07 06:43:54', '2021-01-07 06:43:54'),
(2, 15, 77, 77, 15, 'fsga', '2021-01-07 07:21:27', '2021-01-07 07:21:27'),
(3, 17, 77, 77, 17, 'hii', '2021-01-07 08:56:37', '2021-01-07 08:56:37');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int NOT NULL,
  `code` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`, `created_at`, `updated_at`) VALUES
(1, 'AF', 'Afghanistan', '2020-11-17 02:07:26', NULL),
(2, 'AL', 'Albania', '2020-11-17 02:07:26', NULL),
(3, 'DZ', 'Algeria', '2020-11-17 02:07:26', NULL),
(4, 'DS', 'American Samoa', '2020-11-17 02:07:26', NULL),
(5, 'AD', 'Andorra', '2020-11-17 02:07:26', NULL),
(6, 'AO', 'Angola', '2020-11-17 02:07:26', NULL),
(7, 'AI', 'Anguilla', '2020-11-17 02:07:26', NULL),
(8, 'AQ', 'Antarctica', '2020-11-17 02:07:26', NULL),
(9, 'AG', 'Antigua and Barbuda', '2020-11-17 02:07:26', NULL),
(10, 'AR', 'Argentina', '2020-11-17 02:07:26', NULL),
(11, 'AM', 'Armenia', '2020-11-17 02:07:26', NULL),
(12, 'AW', 'Aruba', '2020-11-17 02:07:26', NULL),
(13, 'AU', 'Australia', '2020-11-17 02:07:26', NULL),
(14, 'AT', 'Austria', '2020-11-17 02:07:26', NULL),
(15, 'AZ', 'Azerbaijan', '2020-11-17 02:07:26', NULL),
(16, 'BS', 'Bahamas', '2020-11-17 02:07:26', NULL),
(17, 'BH', 'Bahrain', '2020-11-17 02:07:26', NULL),
(18, 'BD', 'Bangladesh', '2020-11-17 02:07:26', NULL),
(19, 'BB', 'Barbados', '2020-11-17 02:07:26', NULL),
(20, 'BY', 'Belarus', '2020-11-17 02:07:26', NULL),
(21, 'BE', 'Belgium', '2020-11-17 02:07:26', NULL),
(22, 'BZ', 'Belize', '2020-11-17 02:07:26', NULL),
(23, 'BJ', 'Benin', '2020-11-17 02:07:26', NULL),
(24, 'BM', 'Bermuda', '2020-11-17 02:07:26', NULL),
(25, 'BT', 'Bhutan', '2020-11-17 02:07:26', NULL),
(26, 'BO', 'Bolivia', '2020-11-17 02:07:26', NULL),
(27, 'BA', 'Bosnia and Herzegovina', '2020-11-17 02:07:26', NULL),
(28, 'BW', 'Botswana', '2020-11-17 02:07:26', NULL),
(29, 'BV', 'Bouvet Island', '2020-11-17 02:07:26', NULL),
(30, 'BR', 'Brazil', '2020-11-17 02:07:26', NULL),
(31, 'IO', 'British Indian Ocean Territory', '2020-11-17 02:07:26', NULL),
(32, 'BN', 'Brunei Darussalam', '2020-11-17 02:07:26', NULL),
(33, 'BG', 'Bulgaria', '2020-11-17 02:07:26', NULL),
(34, 'BF', 'Burkina Faso', '2020-11-17 02:07:26', NULL),
(35, 'BI', 'Burundi', '2020-11-17 02:07:26', NULL),
(36, 'KH', 'Cambodia', '2020-11-17 02:07:26', NULL),
(37, 'CM', 'Cameroon', '2020-11-17 02:07:26', NULL),
(38, 'CA', 'Canada', '2020-11-17 02:07:26', NULL),
(39, 'CV', 'Cape Verde', '2020-11-17 02:07:26', NULL),
(40, 'KY', 'Cayman Islands', '2020-11-17 02:07:26', NULL),
(41, 'CF', 'Central African Republic', '2020-11-17 02:07:27', NULL),
(42, 'TD', 'Chad', '2020-11-17 02:07:27', NULL),
(43, 'CL', 'Chile', '2020-11-17 02:07:27', NULL),
(44, 'CN', 'China', '2020-11-17 02:07:27', NULL),
(45, 'CX', 'Christmas Island', '2020-11-17 02:07:27', NULL),
(46, 'CC', 'Cocos (Keeling) Islands', '2020-11-17 02:07:27', NULL),
(47, 'CO', 'Colombia', '2020-11-17 02:07:27', NULL),
(48, 'KM', 'Comoros', '2020-11-17 02:07:27', NULL),
(49, 'CD', 'Democratic Republic of the Congo', '2020-11-17 02:07:27', NULL),
(50, 'CG', 'Republic of Congo', '2020-11-17 02:07:27', NULL),
(51, 'CK', 'Cook Islands', '2020-11-17 02:07:27', NULL),
(52, 'CR', 'Costa Rica', '2020-11-17 02:07:27', NULL),
(53, 'HR', 'Croatia (Hrvatska)', '2020-11-17 02:07:27', NULL),
(54, 'CU', 'Cuba', '2020-11-17 02:07:27', NULL),
(55, 'CY', 'Cyprus', '2020-11-17 02:07:27', NULL),
(56, 'CZ', 'Czech Republic', '2020-11-17 02:07:27', NULL),
(57, 'DK', 'Denmark', '2020-11-17 02:07:27', NULL),
(58, 'DJ', 'Djibouti', '2020-11-17 02:07:27', NULL),
(59, 'DM', 'Dominica', '2020-11-17 02:07:27', NULL),
(60, 'DO', 'Dominican Republic', '2020-11-17 02:07:27', NULL),
(61, 'TP', 'East Timor', '2020-11-17 02:07:27', NULL),
(62, 'EC', 'Ecuador', '2020-11-17 02:07:27', NULL),
(63, 'EG', 'Egypt', '2020-11-17 02:07:27', NULL),
(64, 'SV', 'El Salvador', '2020-11-17 02:07:27', NULL),
(65, 'GQ', 'Equatorial Guinea', '2020-11-17 02:07:27', NULL),
(66, 'ER', 'Eritrea', '2020-11-17 02:07:27', NULL),
(67, 'EE', 'Estonia', '2020-11-17 02:07:27', NULL),
(68, 'ET', 'Ethiopia', '2020-11-17 02:07:27', NULL),
(69, 'FK', 'Falkland Islands (Malvinas)', '2020-11-17 02:07:27', NULL),
(70, 'FO', 'Faroe Islands', '2020-11-17 02:07:27', NULL),
(71, 'FJ', 'Fiji', '2020-11-17 02:07:27', NULL),
(72, 'FI', 'Finland', '2020-11-17 02:07:27', NULL),
(73, 'FR', 'France', '2020-11-17 02:07:27', NULL),
(74, 'FX', 'France, Metropolitan', '2020-11-17 02:07:27', NULL),
(75, 'GF', 'French Guiana', '2020-11-17 02:07:27', NULL),
(76, 'PF', 'French Polynesia', '2020-11-17 02:07:27', NULL),
(77, 'TF', 'French Southern Territories', '2020-11-17 02:07:27', NULL),
(78, 'GA', 'Gabon', '2020-11-17 02:07:27', NULL),
(79, 'GM', 'Gambia', '2020-11-17 02:07:27', NULL),
(80, 'GE', 'Georgia', '2020-11-17 02:07:27', NULL),
(81, 'DE', 'Germany', '2020-11-17 02:07:27', NULL),
(82, 'GH', 'Ghana', '2020-11-17 02:07:27', NULL),
(83, 'GI', 'Gibraltar', '2020-11-17 02:07:27', NULL),
(84, 'GK', 'Guernsey', '2020-11-17 02:07:27', NULL),
(85, 'GR', 'Greece', '2020-11-17 02:07:27', NULL),
(86, 'GL', 'Greenland', '2020-11-17 02:07:27', NULL),
(87, 'GD', 'Grenada', '2020-11-17 02:07:27', NULL),
(88, 'GP', 'Guadeloupe', '2020-11-17 02:07:27', NULL),
(89, 'GU', 'Guam', '2020-11-17 02:07:27', NULL),
(90, 'GT', 'Guatemala', '2020-11-17 02:07:27', NULL),
(91, 'GN', 'Guinea', '2020-11-17 02:07:27', NULL),
(92, 'GW', 'Guinea-Bissau', '2020-11-17 02:07:27', NULL),
(93, 'GY', 'Guyana', '2020-11-17 02:07:27', NULL),
(94, 'HT', 'Haiti', '2020-11-17 02:07:27', NULL),
(95, 'HM', 'Heard and Mc Donald Islands', '2020-11-17 02:07:27', NULL),
(96, 'HN', 'Honduras', '2020-11-17 02:07:27', NULL),
(97, 'HK', 'Hong Kong', '2020-11-17 02:07:27', NULL),
(98, 'HU', 'Hungary', '2020-11-17 02:07:27', NULL),
(99, 'IS', 'Iceland', '2020-11-17 02:07:27', NULL),
(100, 'IN', 'India', '2020-11-17 02:07:27', NULL),
(101, 'IM', 'Isle of Man', '2020-11-17 02:07:27', NULL),
(102, 'ID', 'Indonesia', '2020-11-17 02:07:27', NULL),
(103, 'IR', 'Iran (Islamic Republic of)', '2020-11-17 02:07:27', NULL),
(104, 'IQ', 'Iraq', '2020-11-17 02:07:27', NULL),
(105, 'IE', 'Ireland', '2020-11-17 02:07:27', NULL),
(106, 'IL', 'Israel', '2020-11-17 02:07:27', NULL),
(107, 'IT', 'Italy', '2020-11-17 02:07:27', NULL),
(108, 'CI', 'Ivory Coast', '2020-11-17 02:07:27', NULL),
(109, 'JE', 'Jersey', '2020-11-17 02:07:27', NULL),
(110, 'JM', 'Jamaica', '2020-11-17 02:07:27', NULL),
(111, 'JP', 'Japan', '2020-11-17 02:07:27', NULL),
(112, 'JO', 'Jordan', '2020-11-17 02:07:27', NULL),
(113, 'KZ', 'Kazakhstan', '2020-11-17 02:07:27', NULL),
(114, 'KE', 'Kenya', '2020-11-17 02:07:27', NULL),
(115, 'KI', 'Kiribati', '2020-11-17 02:07:27', NULL),
(116, 'KP', 'Korea, Democratic People\'s Republic of', '2020-11-17 02:07:27', NULL),
(117, 'KR', 'Korea, Republic of', '2020-11-17 02:07:27', NULL),
(118, 'XK', 'Kosovo', '2020-11-17 02:07:27', NULL),
(119, 'KW', 'Kuwait', '2020-11-17 02:07:27', NULL),
(120, 'KG', 'Kyrgyzstan', '2020-11-17 02:07:27', NULL),
(121, 'LA', 'Lao People\'s Democratic Republic', '2020-11-17 02:07:27', NULL),
(122, 'LV', 'Latvia', '2020-11-17 02:07:27', NULL),
(123, 'LB', 'Lebanon', '2020-11-17 02:07:27', NULL),
(124, 'LS', 'Lesotho', '2020-11-17 02:07:27', NULL),
(125, 'LR', 'Liberia', '2020-11-17 02:07:27', NULL),
(126, 'LY', 'Libyan Arab Jamahiriya', '2020-11-17 02:07:27', NULL),
(127, 'LI', 'Liechtenstein', '2020-11-17 02:07:27', NULL),
(128, 'LT', 'Lithuania', '2020-11-17 02:07:27', NULL),
(129, 'LU', 'Luxembourg', '2020-11-17 02:07:27', NULL),
(130, 'MO', 'Macau', '2020-11-17 02:07:27', NULL),
(131, 'MK', 'North Macedonia', '2020-11-17 02:07:27', NULL),
(132, 'MG', 'Madagascar', '2020-11-17 02:07:27', NULL),
(133, 'MW', 'Malawi', '2020-11-17 02:07:27', NULL),
(134, 'MY', 'Malaysia', '2020-11-17 02:07:27', NULL),
(135, 'MV', 'Maldives', '2020-11-17 02:07:27', NULL),
(136, 'ML', 'Mali', '2020-11-17 02:07:27', NULL),
(137, 'MT', 'Malta', '2020-11-17 02:07:27', NULL),
(138, 'MH', 'Marshall Islands', '2020-11-17 02:07:27', NULL),
(139, 'MQ', 'Martinique', '2020-11-17 02:07:27', NULL),
(140, 'MR', 'Mauritania', '2020-11-17 02:07:27', NULL),
(141, 'MU', 'Mauritius', '2020-11-17 02:07:27', NULL),
(142, 'TY', 'Mayotte', '2020-11-17 02:07:27', NULL),
(143, 'MX', 'Mexico', '2020-11-17 02:07:27', NULL),
(144, 'FM', 'Micronesia, Federated States of', '2020-11-17 02:07:27', NULL),
(145, 'MD', 'Moldova, Republic of', '2020-11-17 02:07:27', NULL),
(146, 'MC', 'Monaco', '2020-11-17 02:07:27', NULL),
(147, 'MN', 'Mongolia', '2020-11-17 02:07:27', NULL),
(148, 'ME', 'Montenegro', '2020-11-17 02:07:27', NULL),
(149, 'MS', 'Montserrat', '2020-11-17 02:07:27', NULL),
(150, 'MA', 'Morocco', '2020-11-17 02:07:27', NULL),
(151, 'MZ', 'Mozambique', '2020-11-17 02:07:27', NULL),
(152, 'MM', 'Myanmar', '2020-11-17 02:07:27', NULL),
(153, 'NA', 'Namibia', '2020-11-17 02:07:27', NULL),
(154, 'NR', 'Nauru', '2020-11-17 02:07:27', NULL),
(155, 'NP', 'Nepal', '2020-11-17 02:07:27', NULL),
(156, 'NL', 'Netherlands', '2020-11-17 02:07:27', NULL),
(157, 'AN', 'Netherlands Antilles', '2020-11-17 02:07:27', NULL),
(158, 'NC', 'New Caledonia', '2020-11-17 02:07:27', NULL),
(159, 'NZ', 'New Zealand', '2020-11-17 02:07:27', NULL),
(160, 'NI', 'Nicaragua', '2020-11-17 02:07:27', NULL),
(161, 'NE', 'Niger', '2020-11-17 02:07:27', NULL),
(162, 'NG', 'Nigeria', '2020-11-17 02:07:27', NULL),
(163, 'NU', 'Niue', '2020-11-17 02:07:27', NULL),
(164, 'NF', 'Norfolk Island', '2020-11-17 02:07:27', NULL),
(165, 'MP', 'Northern Mariana Islands', '2020-11-17 02:07:27', NULL),
(166, 'NO', 'Norway', '2020-11-17 02:07:27', NULL),
(167, 'OM', 'Oman', '2020-11-17 02:07:27', NULL),
(168, 'PK', 'Pakistan', '2020-11-17 02:07:27', NULL),
(169, 'PW', 'Palau', '2020-11-17 02:07:27', NULL),
(170, 'PS', 'Palestine', '2020-11-17 02:07:27', NULL),
(171, 'PA', 'Panama', '2020-11-17 02:07:27', NULL),
(172, 'PG', 'Papua New Guinea', '2020-11-17 02:07:27', NULL),
(173, 'PY', 'Paraguay', '2020-11-17 02:07:27', NULL),
(174, 'PE', 'Peru', '2020-11-17 02:07:27', NULL),
(175, 'PH', 'Philippines', '2020-11-17 02:07:27', NULL),
(176, 'PN', 'Pitcairn', '2020-11-17 02:07:27', NULL),
(177, 'PL', 'Poland', '2020-11-17 02:07:28', NULL),
(178, 'PT', 'Portugal', '2020-11-17 02:07:28', NULL),
(179, 'PR', 'Puerto Rico', '2020-11-17 02:07:28', NULL),
(180, 'QA', 'Qatar', '2020-11-17 02:07:28', NULL),
(181, 'RE', 'Reunion', '2020-11-17 02:07:28', NULL),
(182, 'RO', 'Romania', '2020-11-17 02:07:28', NULL),
(183, 'RU', 'Russian Federation', '2020-11-17 02:07:28', NULL),
(184, 'RW', 'Rwanda', '2020-11-17 02:07:28', NULL),
(185, 'KN', 'Saint Kitts and Nevis', '2020-11-17 02:07:28', NULL),
(186, 'LC', 'Saint Lucia', '2020-11-17 02:07:28', NULL),
(187, 'VC', 'Saint Vincent and the Grenadines', '2020-11-17 02:07:28', NULL),
(188, 'WS', 'Samoa', '2020-11-17 02:07:28', NULL),
(189, 'SM', 'San Marino', '2020-11-17 02:07:28', NULL),
(190, 'ST', 'Sao Tome and Principe', '2020-11-17 02:07:28', NULL),
(191, 'SA', 'Saudi Arabia', '2020-11-17 02:07:28', NULL),
(192, 'SN', 'Senegal', '2020-11-17 02:07:28', NULL),
(193, 'RS', 'Serbia', '2020-11-17 02:07:28', NULL),
(194, 'SC', 'Seychelles', '2020-11-17 02:07:28', NULL),
(195, 'SL', 'Sierra Leone', '2020-11-17 02:07:28', NULL),
(196, 'SG', 'Singapore', '2020-11-17 02:07:28', NULL),
(197, 'SK', 'Slovakia', '2020-11-17 02:07:28', NULL),
(198, 'SI', 'Slovenia', '2020-11-17 02:07:28', NULL),
(199, 'SB', 'Solomon Islands', '2020-11-17 02:07:28', NULL),
(200, 'SO', 'Somalia', '2020-11-17 02:07:28', NULL),
(201, 'ZA', 'South Africa', '2020-11-17 02:07:28', NULL),
(202, 'GS', 'South Georgia South Sandwich Islands', '2020-11-17 02:07:28', NULL),
(203, 'SS', 'South Sudan', '2020-11-17 02:07:28', NULL),
(204, 'ES', 'Spain', '2020-11-17 02:07:28', NULL),
(205, 'LK', 'Sri Lanka', '2020-11-17 02:07:28', NULL),
(206, 'SH', 'St. Helena', '2020-11-17 02:07:28', NULL),
(207, 'PM', 'St. Pierre and Miquelon', '2020-11-17 02:07:28', NULL),
(208, 'SD', 'Sudan', '2020-11-17 02:07:28', NULL),
(209, 'SR', 'Suriname', '2020-11-17 02:07:28', NULL),
(210, 'SJ', 'Svalbard and Jan Mayen Islands', '2020-11-17 02:07:28', NULL),
(211, 'SZ', 'Swaziland', '2020-11-17 02:07:28', NULL),
(212, 'SE', 'Sweden', '2020-11-17 02:07:28', NULL),
(213, 'CH', 'Switzerland', '2020-11-17 02:07:28', NULL),
(214, 'SY', 'Syrian Arab Republic', '2020-11-17 02:07:28', NULL),
(215, 'TW', 'Taiwan', '2020-11-17 02:07:28', NULL),
(216, 'TJ', 'Tajikistan', '2020-11-17 02:07:28', NULL),
(217, 'TZ', 'Tanzania, United Republic of', '2020-11-17 02:07:28', NULL),
(218, 'TH', 'Thailand', '2020-11-17 02:07:28', NULL),
(219, 'TG', 'Togo', '2020-11-17 02:07:28', NULL),
(220, 'TK', 'Tokelau', '2020-11-17 02:07:28', NULL),
(221, 'TO', 'Tonga', '2020-11-17 02:07:28', NULL),
(222, 'TT', 'Trinidad and Tobago', '2020-11-17 02:07:28', NULL),
(223, 'TN', 'Tunisia', '2020-11-17 02:07:28', NULL),
(224, 'TR', 'Turkey', '2020-11-17 02:07:28', NULL),
(225, 'TM', 'Turkmenistan', '2020-11-17 02:07:28', NULL),
(226, 'TC', 'Turks and Caicos Islands', '2020-11-17 02:07:28', NULL),
(227, 'TV', 'Tuvalu', '2020-11-17 02:07:28', NULL),
(228, 'UG', 'Uganda', '2020-11-17 02:07:28', NULL),
(229, 'UA', 'Ukraine', '2020-11-17 02:07:28', NULL),
(230, 'AE', 'United Arab Emirates', '2020-11-17 02:07:28', NULL),
(231, 'GB', 'United Kingdom', '2020-11-17 02:07:28', NULL),
(232, 'US', 'United States', '2020-11-17 02:07:28', NULL),
(233, 'UM', 'United States minor outlying islands', '2020-11-17 02:07:28', NULL),
(234, 'UY', 'Uruguay', '2020-11-17 02:07:28', NULL),
(235, 'UZ', 'Uzbekistan', '2020-11-17 02:07:28', NULL),
(236, 'VU', 'Vanuatu', '2020-11-17 02:07:28', NULL),
(237, 'VA', 'Vatican City State', '2020-11-17 02:07:28', NULL),
(238, 'VE', 'Venezuela', '2020-11-17 02:07:28', NULL),
(239, 'VN', 'Vietnam', '2020-11-17 02:07:28', NULL),
(240, 'VG', 'Virgin Islands (British)', '2020-11-17 02:07:28', NULL),
(241, 'VI', 'Virgin Islands (U.S.)', '2020-11-17 02:07:28', NULL),
(242, 'WF', 'Wallis and Futuna Islands', '2020-11-17 02:07:28', NULL),
(243, 'EH', 'Western Sahara', '2020-11-17 02:07:28', NULL),
(244, 'YE', 'Yemen', '2020-11-17 02:07:28', NULL),
(245, 'ZM', 'Zambia', '2020-11-17 02:07:28', NULL),
(246, 'ZW', 'Zimbabwe', '2020-11-17 02:07:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int NOT NULL,
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dis_type` enum('f','p') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'p',
  `amount` int NOT NULL DEFAULT '0',
  `min_cart_amount` int NOT NULL DEFAULT '0',
  `max_usage` enum('o','m') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_ids` json DEFAULT NULL,
  `category_ids` json DEFAULT NULL,
  `product_varient_ids` json DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `expiry_date` timestamp NULL DEFAULT NULL,
  `sub_type` enum('m','s') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'm' COMMENT 'm=MRPs=Selling Price',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `description`, `dis_type`, `amount`, `min_cart_amount`, `max_usage`, `seller_ids`, `category_ids`, `product_varient_ids`, `start_date`, `expiry_date`, `sub_type`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CP01', 'LIMITED PERIOD ONLY Get Extra FLAT 100 OFF on ', 'f', 100, 500, NULL, NULL, NULL, NULL, '2020-06-08 21:38:06', '2021-01-14 12:42:39', 's', 1, '2020-12-15 11:22:35', NULL, NULL),
(2, 'CP02', 'LIMITED PERIOD ONLY Get Extra 10% OFF on', 'p', 10, 500, NULL, NULL, NULL, NULL, '2020-12-15 11:21:25', '2021-01-21 12:42:46', 's', 1, '2020-12-15 11:22:38', NULL, NULL),
(4, 'CP03', 'LIMITED PERIOD ONLY Get Extra 10% OFF on', 'p', 10, 100, 'o', NULL, NULL, NULL, '2020-12-31 18:30:00', '2021-02-28 18:30:00', 'm', 1, '2020-12-15 11:22:38', '2021-01-29 08:23:13', NULL),
(13, 'FINAL', 'Sample description', 'f', 1, 1000, 'o', NULL, '[\"1\"]', NULL, '2021-03-03 12:20:00', '2021-09-30 23:50:00', 's', 1, '2021-01-22 07:40:31', '2021-03-20 11:06:55', NULL),
(14, 'code500', 'Cart Amount', 'p', 9, 10, 'o', '[\"6\", \"15\"]', '[\"1\", \"13\"]', '[\"25\", \"29\"]', '2021-02-24 18:30:00', '2021-02-25 18:30:00', 'm', 1, '2021-02-25 05:55:35', '2021-02-25 10:55:37', NULL),
(15, 'code5000', 'Desc', 'f', 100, 500, 'o', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 's', 1, '2021-03-20 10:38:27', '2021-03-20 10:38:27', NULL),
(16, 'code50000', 'DESC', 'p', 10, 2000, 'm', NULL, NULL, NULL, '2021-03-20 11:25:59', '2021-03-22 12:05:59', 'm', 1, '2021-03-20 11:06:33', '2021-03-20 11:06:33', NULL),
(17, 'code5000000', 'Description', 'f', 100, 1000, 'o', NULL, NULL, NULL, '2021-03-23 10:25:12', '2021-03-24 10:25:12', 's', 1, '2021-03-22 10:25:45', '2021-03-22 10:25:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternate_mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('m','f','t') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst` enum('y','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_no` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_certificate` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `inactive_reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `mobile`, `alternate_mobile`, `image`, `gender`, `dob`, `gst`, `gst_no`, `gst_certificate`, `status`, `inactive_reason`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Avez', 'mohammedavez85@gmail.com', '9550261360', NULL, '71728c5900e9ad68502cd52ddde55107.jpg', 'm', '2021-01-21', 'n', NULL, NULL, 1, NULL, '2021-01-20 03:37:51', '2021-01-21 04:34:56', NULL),
(3, NULL, NULL, '9549588439', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2021-01-20 08:32:06', '2021-01-20 08:32:06', NULL),
(4, 'Abhi', 'abhi.adulapuram71@gmail.com', '7396043338', NULL, 'f918e970e43fb36b585e3c2cf0380591.jpg', 'm', '1996-04-05', 'n', NULL, NULL, 1, NULL, '2021-01-21 01:18:55', '2021-01-23 00:21:15', NULL),
(6, 'Abhi', 'abhishekmiraki@gmail.com', '9948839362', NULL, '6f59733172d22076e68ee755f772efaf.jpg', 'm', '1996-04-05', 'n', NULL, NULL, 1, NULL, '2021-01-21 03:22:01', '2021-01-22 02:12:39', NULL),
(7, 'Anitha', 'anithamiraki@gmail.com', '9949587496', '9866336421', '0698a274a1c9a927a6155476ca0ed9f2.jpg', 'f', '1996-02-21', 'n', NULL, NULL, 1, NULL, '2021-01-21 04:08:02', '2021-01-21 04:14:39', NULL),
(8, 'Hari', 'avezmiraki@gmail.com', '9290573910', NULL, '81ada494f5bb1a721ad2c6ec8dbe2fda.jpg', 'm', '2021-01-01', 'n', NULL, NULL, 1, NULL, '2021-01-21 04:52:06', '2021-01-21 04:53:33', NULL),
(9, 'Avez', 'harik@mirakitech.com', '6290844051', NULL, '377fda5c14c2cc84e450e40275657bb4.jpg', 'm', '2021-01-08', 'n', NULL, NULL, 1, NULL, '2021-01-21 05:40:43', '2021-03-03 14:00:24', NULL),
(10, 'Deepak', 'deepakn@mirakitech.com', '8520021995', NULL, 'b6bbd76dc27aed18d4fca4b5f029bd9c.jpg', 'm', '1990-01-09', 'n', NULL, NULL, 1, NULL, '2021-01-21 10:01:43', '2021-01-21 10:21:11', NULL),
(11, 'SRIHARSHA SRIRAMOJU', 'mirakitech@gmail.com', '9948037153', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2021-01-28 05:20:59', '2021-03-03 13:02:29', NULL),
(12, 'lavanya', 'lavanyamiraki1@yopmail.com', '7780361239', NULL, 'bd4590b5f8320df9977f4d2ccf06decc.jpg', 'f', '2021-01-29', 'n', NULL, NULL, 1, NULL, '2021-01-29 01:46:08', '2021-01-29 05:50:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_addresses`
--

CREATE TABLE `customer_addresses` (
  `id` int NOT NULL,
  `customer_id` int DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `addr_type` enum('h','w','o') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'h' COMMENT 'h=Home,w=Work,o=Others',
  `addr_other` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locality` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_id` int DEFAULT NULL,
  `pincode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_addresses`
--

INSERT INTO `customer_addresses` (`id`, `customer_id`, `name`, `mobile`, `is_default`, `addr_type`, `addr_other`, `address`, `landmark`, `locality`, `city`, `state_id`, `pincode`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, 'Anitha', '9949587496', 1, 'w', NULL, 'Ammerpet', NULL, 'Hyderabad', 'Anantapur', 2, '515511', 1, '2021-01-21 04:09:25', '2021-01-29 00:41:18', '2021-01-29 00:41:18'),
(2, 1, 'Avez', '9550261360', 0, 'h', NULL, 'hno:4-1-76', NULL, 'Hyderabad', 'Hyderabad', 2, '507003', 1, '2021-01-21 04:22:25', '2021-01-25 08:14:22', NULL),
(3, 5, 'lavanya', '7780361239', 1, 'h', NULL, 'gokul[plots', NULL, 'gokulplots', 'hyderabad', 36, '500085', 1, '2021-01-21 04:43:25', '2021-01-25 04:47:05', NULL),
(4, 5, 'lavanya', '7780361239', 0, 'h', NULL, 'gokulplots', NULL, 'gokulplots', 'hyderabad', 36, '500085', 1, '2021-01-21 04:44:56', '2021-01-25 04:47:05', NULL),
(5, 8, 'hari', '9550261360', 0, 'h', NULL, 'tet', NULL, 'test', 'test', 1, '507003', 1, '2021-01-21 04:53:06', '2021-01-21 05:25:40', NULL),
(6, 8, 'test', '9550261360', NULL, 'h', NULL, 'testt', NULL, 'est', 'test', 1, '507003', 1, '2021-01-21 05:25:40', '2021-01-21 05:25:40', NULL),
(7, 9, 'Avez', '9550261360', 1, 'h', NULL, '10 2 557, Kalha Road, Humayun Nagar, Mehdipatnam', NULL, 'Mehdipatnam', 'Hyderabad', 4, '507002', 1, '2021-01-21 06:08:10', '2021-01-21 06:08:10', NULL),
(8, 10, 'Deepak', '8520021995', 1, 'h', NULL, 'Gachibowli', NULL, 'Hyderabad', 'Hyderabad', 36, '500032', 1, '2021-01-21 10:08:09', '2021-01-21 10:19:16', NULL),
(9, 10, 'Deepak office', '9988989898', 0, 'w', NULL, 'Hitech city, Hitech city', NULL, 'Hyderabad', 'Hyderabad', 36, '500042', 1, '2021-01-21 10:11:16', '2021-01-21 10:19:16', NULL),
(10, 6, 'abhi', '9948839362', 1, 'h', NULL, '6-4-297/1, IB Colony', NULL, 'Godavarikhani', 'Peddapalli', 36, '505209', 1, '2021-01-22 02:06:12', '2021-01-22 02:06:12', NULL),
(11, 4, 'Abhishek', '9948839362', 1, 'h', NULL, '6-4-297/1,IBCOLONY, GODAVARIKHANI,OPPT2-562', NULL, 'GDK', 'Peddapalli', 36, '500072', 1, '2021-01-23 00:18:56', '2021-01-23 00:52:09', '2021-01-23 00:52:09'),
(12, 1, 'Avez', '9550261360', 1, 'h', NULL, 'Vishwapriya Nagar, Begur, Bengaluru, Karnataka 530068, India', NULL, 'Bengaluru', 'Bangalore', 17, '560013', 1, '2021-01-25 08:13:53', '2021-01-25 08:14:21', NULL),
(13, 7, 'anita', '9949587496', 1, 'h', NULL, '123333', NULL, 'rju', 'rqjah', 17, '533101', 1, '2021-01-29 00:42:00', '2021-01-29 01:28:28', '2021-01-29 01:28:28'),
(14, 12, 'Lavanya', '7780361239', 1, 'h', NULL, 'Hyderabad', NULL, 'Hyderabad', 'Hyderabad', 36, '500085', 1, '2021-01-29 03:44:00', '2021-01-29 03:44:00', NULL),
(15, 7, 'anitha', '9949587496', 1, 'h', NULL, 'hyderabad', NULL, 'hyderabad', 'hyderabad', 15, '533105', 1, '2021-01-29 05:52:19', '2021-01-29 05:52:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_feedback`
--

CREATE TABLE `customer_feedback` (
  `id` int NOT NULL,
  `customer_id` int NOT NULL,
  `feedback` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_feedback`
--

INSERT INTO `customer_feedback` (`id`, `customer_id`, `feedback`, `created_at`, `updated_at`) VALUES
(1, 19, 'test', '2021-01-15 00:39:33', '2021-01-15 00:39:33'),
(2, 19, 'testing', '2021-01-15 00:39:58', '2021-01-15 00:39:58'),
(3, 12, 'Test', '2021-01-29 02:00:06', '2021-01-29 02:00:06'),
(4, 12, 'Test', '2021-01-29 02:04:10', '2021-01-29 02:04:10'),
(5, 12, 'Test', '2021-01-29 02:04:22', '2021-01-29 02:04:22');

-- --------------------------------------------------------

--
-- Table structure for table `deals`
--

CREATE TABLE `deals` (
  `id` int NOT NULL,
  `product_id` int DEFAULT NULL,
  `product_variant_id` int DEFAULT NULL,
  `deal_price` int DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `handeling_charges`
--

CREATE TABLE `handeling_charges` (
  `id` int NOT NULL,
  `min_amount` int NOT NULL DEFAULT '0',
  `max_amount` int DEFAULT NULL,
  `charge_amount` int NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `handeling_charges`
--

INSERT INTO `handeling_charges` (`id`, `min_amount`, `max_amount`, `charge_amount`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 999, 100, 1, '2020-12-17 09:36:12', NULL, NULL),
(4, 1000, 1999, 50, 1, '2020-12-17 09:36:16', NULL, NULL),
(5, 2000, NULL, 0, 1, '2020-12-17 09:38:04', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `help_desks`
--

CREATE TABLE `help_desks` (
  `id` int NOT NULL,
  `ticket_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_id` int DEFAULT NULL,
  `issue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_status` tinyint(1) DEFAULT '0' COMMENT '0=Pending , 1=Processing , 2=Completed',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `help_desks`
--

INSERT INTO `help_desks` (`id`, `ticket_no`, `issue_title`, `seller_id`, `issue`, `category`, `attachment`, `issue_status`, `status`, `created_at`, `updated_at`) VALUES
(1, '262062', 'Support Ticket24', 19, 'Payment Issue', 'Orders & Shipping', '1608792164.jpeg', 2, 1, '2020-12-24 06:42:44', '2020-12-24 06:45:16'),
(2, '515941', 'Payment', 19, 'Amount Debited', 'Payments & Commissions', '1608793542.png', 2, 1, '2020-12-24 07:05:42', '2020-12-24 07:54:52'),
(3, '571797', 'Issues with Product Returned', 19, 'I have an issue with product returned. its not returned in good shape', 'Others', '1608925198.jpg', 0, 1, '2020-12-25 19:39:58', '2020-12-25 19:39:58'),
(4, '955000', 'Payment Bounces', 19, 'payment Not working', 'Payments & Commissions', '1611048347.jpg', 2, 1, '2021-01-19 03:55:47', '2021-01-19 03:56:33'),
(5, '696645', 'Payment Fabpik Seller', 19, 'Payment Issue Fabpik', 'Techincal Issue', '1611048448.jpg', 2, 1, '2021-01-19 03:57:28', '2021-01-19 05:02:04'),
(6, '368384', 'Issue with Product', 3, 'Issue with Product', 'Refund & Exchange Requests', NULL, 0, 1, '2021-01-21 08:50:02', '2021-01-21 08:50:02'),
(7, '478937', 'Issue with order', 13, 'Issue with order', 'Orders & Shipping', NULL, 0, 1, '2021-02-26 11:42:55', '2021-02-26 11:42:55'),
(8, '836147', 'subjecy', 13, 'Issue', 'Refund & Exchange Requests', '1616141337.jpg', 0, 1, '2021-03-19 08:08:57', '2021-03-19 08:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `help_desk_comments`
--

CREATE TABLE `help_desk_comments` (
  `id` int NOT NULL,
  `help_desk_id` int DEFAULT NULL,
  `comments` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_by` int DEFAULT NULL COMMENT 'user_id',
  `attachment` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `help_desk_comments`
--

INSERT INTO `help_desk_comments` (`id`, `help_desk_id`, `comments`, `comment_by`, `attachment`, `created_at`, `updated_at`) VALUES
(1, 1, 'New Fabpik', 1, NULL, '2020-12-24 06:44:16', '2020-12-24 06:44:16'),
(2, 1, 'Closed', 55, NULL, '2020-12-24 06:44:37', '2020-12-24 06:44:37'),
(3, 2, 'Payment', 55, '1608794843.jpeg', '2020-12-24 07:27:23', '2020-12-24 07:27:23'),
(4, 2, 'Payment Issue', 55, NULL, '2020-12-24 07:27:58', '2020-12-24 07:27:58'),
(5, 2, 'Note Fabpik', 55, '1608794927.jpeg', '2020-12-24 07:28:47', '2020-12-24 07:28:47'),
(6, 2, 'We Will work on it', 1, NULL, '2020-12-24 07:32:32', '2020-12-24 07:32:32'),
(7, 2, 'New  Test', 1, NULL, '2020-12-24 07:41:29', '2020-12-24 07:41:29'),
(8, 2, 'Test Comment', 1, '1608796832.jpeg', '2020-12-24 08:00:32', '2020-12-24 08:00:32'),
(9, 3, 'Adding a test comment to see how it looks on the Ticket History.', 55, NULL, '2020-12-25 19:42:16', '2020-12-25 19:42:16'),
(10, 3, 'What is the exact issue.. can you elaborate more', 1, NULL, '2020-12-25 19:43:20', '2020-12-25 19:43:20'),
(11, 3, 'Sure. Let me upload a picture for you', 55, '1608925461.jpg', '2020-12-25 19:44:21', '2020-12-25 19:44:21'),
(12, 5, 'New Under Development', 1, '1611050223.jpg', '2021-01-19 04:27:03', '2021-01-19 04:27:03'),
(13, 5, 'Process Right', 55, NULL, '2021-01-19 04:27:33', '2021-01-19 04:27:33'),
(14, 5, 'hello', 1, NULL, '2021-01-19 04:28:28', '2021-01-19 04:28:28'),
(15, 5, 'test', 1, '1611050704.jpg', '2021-01-19 04:35:04', '2021-01-19 04:35:04'),
(16, 5, 'Ok', 1, NULL, '2021-01-19 04:41:02', '2021-01-19 04:41:02'),
(17, 5, 'Process', 55, NULL, '2021-01-19 04:41:26', '2021-01-19 04:41:26'),
(18, 5, 'Righrt', 1, NULL, '2021-01-19 04:41:42', '2021-01-19 04:41:42'),
(19, 5, 'ok', 55, NULL, '2021-01-19 04:41:55', '2021-01-19 04:41:55'),
(20, 5, 'Fabpik', 1, NULL, '2021-01-19 04:42:10', '2021-01-19 04:42:10'),
(21, 5, 'Right', 55, NULL, '2021-01-19 04:42:23', '2021-01-19 04:42:23'),
(22, 6, 'What is the Problem', 1, NULL, '2021-01-21 08:50:57', '2021-01-21 08:50:57'),
(23, 6, 'Product returned as damanged', 116, NULL, '2021-01-21 08:51:33', '2021-01-21 08:51:33'),
(24, 6, 'Product returned as damanged', 116, NULL, '2021-01-21 08:52:43', '2021-01-21 08:52:43'),
(25, 6, 'okay', 1, NULL, '2021-01-21 08:52:57', '2021-01-21 08:52:57'),
(26, 7, 'okay', 1, NULL, '2021-02-26 11:43:19', '2021-02-26 11:43:19'),
(27, 7, 'hi', 134, NULL, '2021-02-26 11:43:28', '2021-02-26 11:43:28'),
(28, 7, 'hello', 1, NULL, '2021-02-26 11:43:37', '2021-02-26 11:43:37');

-- --------------------------------------------------------

--
-- Table structure for table `iron_types`
--

CREATE TABLE `iron_types` (
  `id` int NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iron_types`
--

INSERT INTO `iron_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'iron at any temperature', '2020-11-18 18:36:39', NULL),
(2, 'do not iron', '2020-11-18 18:36:39', NULL),
(3, 'steam', '2021-02-15 05:52:23', NULL),
(4, 'no steam', '2021-02-15 05:52:23', NULL),
(5, 'low heat', '2021-02-15 05:52:41', NULL),
(6, 'medium heat', '2021-02-15 05:52:41', NULL),
(7, 'high heat', '2021-02-15 05:52:52', NULL),
(8, 'N/A', '2021-02-15 05:53:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint UNSIGNED NOT NULL,
  `reserved_at` int UNSIGNED DEFAULT NULL,
  `available_at` int UNSIGNED NOT NULL,
  `created_at` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(1, 'default', '{\"uuid\":\"c74e63e8-9ce9-481e-a244-10b086b5de13\",\"displayName\":\"App\\\\Mail\\\\OrderReturnApprovedByAdminEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Mail\\\\SendQueuedMailable\",\"command\":\"O:34:\\\"Illuminate\\\\Mail\\\\SendQueuedMailable\\\":12:{s:8:\\\"mailable\\\";O:40:\\\"App\\\\Mail\\\\OrderReturnApprovedByAdminEmail\\\":29:{s:4:\\\"data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:22:\\\"App\\\\Models\\\\OrderDetail\\\";s:2:\\\"id\\\";i:13;s:9:\\\"relations\\\";a:3:{i:0;s:5:\\\"order\\\";i:1;s:6:\\\"seller\\\";i:2;s:14:\\\"productVarient\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"locale\\\";N;s:4:\\\"from\\\";a:0:{}s:2:\\\"to\\\";a:1:{i:0;a:2:{s:4:\\\"name\\\";N;s:7:\\\"address\\\";s:26:\\\"a.harikrishna108@gmail.com\\\";}}s:2:\\\"cc\\\";a:0:{}s:3:\\\"bcc\\\";a:0:{}s:7:\\\"replyTo\\\";a:0:{}s:7:\\\"subject\\\";N;s:11:\\\"\\u0000*\\u0000markdown\\\";N;s:7:\\\"\\u0000*\\u0000html\\\";N;s:4:\\\"view\\\";N;s:8:\\\"textView\\\";N;s:8:\\\"viewData\\\";a:0:{}s:11:\\\"attachments\\\";a:0:{}s:14:\\\"rawAttachments\\\";a:0:{}s:15:\\\"diskAttachments\\\";a:0:{}s:9:\\\"callbacks\\\";a:0:{}s:5:\\\"theme\\\";N;s:6:\\\"mailer\\\";s:4:\\\"smtp\\\";s:29:\\\"\\u0000*\\u0000assertionableRenderStrings\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1613456339, 1613456339),
(2, 'default', '{\"uuid\":\"674ce97b-7183-488f-888b-d6f5c6c47344\",\"displayName\":\"App\\\\Mail\\\\OrderReturnApprovedByAdminUserEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Mail\\\\SendQueuedMailable\",\"command\":\"O:34:\\\"Illuminate\\\\Mail\\\\SendQueuedMailable\\\":12:{s:8:\\\"mailable\\\";O:44:\\\"App\\\\Mail\\\\OrderReturnApprovedByAdminUserEmail\\\":29:{s:4:\\\"data\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:22:\\\"App\\\\Models\\\\OrderDetail\\\";s:2:\\\"id\\\";i:13;s:9:\\\"relations\\\";a:3:{i:0;s:5:\\\"order\\\";i:1;s:6:\\\"seller\\\";i:2;s:14:\\\"productVarient\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"locale\\\";N;s:4:\\\"from\\\";a:0:{}s:2:\\\"to\\\";a:1:{i:0;a:2:{s:4:\\\"name\\\";N;s:7:\\\"address\\\";s:28:\\\"harikrishna.miraki@gmail.com\\\";}}s:2:\\\"cc\\\";a:0:{}s:3:\\\"bcc\\\";a:0:{}s:7:\\\"replyTo\\\";a:0:{}s:7:\\\"subject\\\";N;s:11:\\\"\\u0000*\\u0000markdown\\\";N;s:7:\\\"\\u0000*\\u0000html\\\";N;s:4:\\\"view\\\";N;s:8:\\\"textView\\\";N;s:8:\\\"viewData\\\";a:0:{}s:11:\\\"attachments\\\";a:0:{}s:14:\\\"rawAttachments\\\";a:0:{}s:15:\\\"diskAttachments\\\";a:0:{}s:9:\\\"callbacks\\\";a:0:{}s:5:\\\"theme\\\";N;s:6:\\\"mailer\\\";s:4:\\\"smtp\\\";s:29:\\\"\\u0000*\\u0000assertionableRenderStrings\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1613456339, 1613456339);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_11_07_045240_create_permission_tables', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(4, 'App\\Models\\User', 2),
(4, 'App\\Models\\User', 3),
(4, 'App\\Models\\User', 4),
(3, 'App\\Models\\User', 36),
(3, 'App\\Models\\User', 37),
(3, 'App\\Models\\User', 38),
(3, 'App\\Models\\User', 39),
(3, 'App\\Models\\User', 40),
(3, 'App\\Models\\User', 41),
(3, 'App\\Models\\User', 42),
(3, 'App\\Models\\User', 43),
(3, 'App\\Models\\User', 47),
(3, 'App\\Models\\User', 48),
(3, 'App\\Models\\User', 49),
(3, 'App\\Models\\User', 50),
(3, 'App\\Models\\User', 51),
(3, 'App\\Models\\User', 52),
(4, 'App\\Models\\User', 53),
(4, 'App\\Models\\User', 54),
(3, 'App\\Models\\User', 55),
(4, 'App\\Models\\User', 56),
(3, 'App\\Models\\User', 57),
(3, 'App\\Models\\User', 58),
(3, 'App\\Models\\User', 59),
(3, 'App\\Models\\User', 60),
(3, 'App\\Models\\User', 61),
(3, 'App\\Models\\User', 62),
(3, 'App\\Models\\User', 63),
(3, 'App\\Models\\User', 64),
(3, 'App\\Models\\User', 65),
(3, 'App\\Models\\User', 66),
(3, 'App\\Models\\User', 67),
(3, 'App\\Models\\User', 68),
(3, 'App\\Models\\User', 69),
(3, 'App\\Models\\User', 70),
(3, 'App\\Models\\User', 71),
(3, 'App\\Models\\User', 72),
(3, 'App\\Models\\User', 73),
(3, 'App\\Models\\User', 74),
(4, 'App\\Models\\User', 75),
(4, 'App\\Models\\User', 76),
(4, 'App\\Models\\User', 77),
(3, 'App\\Models\\User', 78),
(4, 'App\\Models\\User', 79),
(3, 'App\\Models\\User', 80),
(3, 'App\\Models\\User', 81),
(3, 'App\\Models\\User', 82),
(4, 'App\\Models\\User', 83),
(4, 'App\\Models\\User', 84),
(4, 'App\\Models\\User', 85),
(3, 'App\\Models\\User', 86),
(3, 'App\\Models\\User', 87),
(4, 'App\\Models\\User', 88),
(3, 'App\\Models\\User', 89),
(4, 'App\\Models\\User', 90),
(3, 'App\\Models\\User', 91),
(3, 'App\\Models\\User', 92),
(3, 'App\\Models\\User', 93),
(3, 'App\\Models\\User', 94),
(3, 'App\\Models\\User', 95),
(3, 'App\\Models\\User', 96),
(3, 'App\\Models\\User', 97),
(3, 'App\\Models\\User', 98),
(4, 'App\\Models\\User', 99),
(4, 'App\\Models\\User', 100),
(3, 'App\\Models\\User', 101),
(4, 'App\\Models\\User', 102),
(3, 'App\\Models\\User', 103),
(4, 'App\\Models\\User', 104),
(3, 'App\\Models\\User', 105),
(3, 'App\\Models\\User', 106),
(3, 'App\\Models\\User', 107),
(3, 'App\\Models\\User', 108),
(4, 'App\\Models\\User', 109),
(3, 'App\\Models\\User', 110),
(4, 'App\\Models\\User', 111),
(3, 'App\\Models\\User', 112),
(4, 'App\\Models\\User', 113),
(4, 'App\\Models\\User', 114),
(3, 'App\\Models\\User', 115),
(3, 'App\\Models\\User', 116),
(3, 'App\\Models\\User', 117),
(4, 'App\\Models\\User', 118),
(4, 'App\\Models\\User', 119),
(4, 'App\\Models\\User', 120),
(4, 'App\\Models\\User', 121),
(4, 'App\\Models\\User', 122),
(4, 'App\\Models\\User', 123),
(3, 'App\\Models\\User', 124),
(3, 'App\\Models\\User', 125),
(4, 'App\\Models\\User', 126),
(3, 'App\\Models\\User', 127),
(3, 'App\\Models\\User', 128),
(3, 'App\\Models\\User', 129),
(3, 'App\\Models\\User', 130),
(3, 'App\\Models\\User', 131),
(4, 'App\\Models\\User', 132),
(3, 'App\\Models\\User', 133),
(3, 'App\\Models\\User', 134),
(4, 'App\\Models\\User', 135),
(3, 'App\\Models\\User', 136),
(3, 'App\\Models\\User', 137),
(3, 'App\\Models\\User', 138),
(3, 'App\\Models\\User', 139),
(3, 'App\\Models\\User', 140),
(1, 'App\\Models\\User', 142),
(3, 'App\\Models\\User', 145),
(3, 'App\\Models\\User', 146),
(3, 'App\\Models\\User', 147),
(1, 'App\\Models\\User', 149),
(3, 'App\\Models\\User', 150);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int NOT NULL,
  `parent_order_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_prefix` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `customer_id` int DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_first_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_last_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_mobile` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address1` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address2` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state_id` int DEFAULT NULL,
  `billing_pincode` int DEFAULT NULL,
  `payment_status_id` int DEFAULT NULL,
  `shipping_first_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_last_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_mobile` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address1` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address2` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state_id` int DEFAULT NULL,
  `shipping_pincode` int DEFAULT NULL,
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` double DEFAULT NULL,
  `order_status_id` int DEFAULT NULL,
  `coupon_id` int DEFAULT NULL,
  `coupon_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_discount` double DEFAULT NULL,
  `shipping_charge` double DEFAULT NULL,
  `handling_charge` double DEFAULT NULL,
  `payment_type` enum('o','c') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `parent_order_id`, `invoice_prefix`, `customer_id`, `first_name`, `last_name`, `email`, `mobile`, `billing_first_name`, `billing_last_name`, `billing_email`, `billing_mobile`, `billing_address1`, `billing_address2`, `billing_city`, `billing_state`, `billing_state_id`, `billing_pincode`, `payment_status_id`, `shipping_first_name`, `shipping_last_name`, `shipping_email`, `shipping_mobile`, `shipping_address1`, `shipping_address2`, `shipping_city`, `shipping_state`, `shipping_state_id`, `shipping_pincode`, `comment`, `total`, `order_status_id`, `coupon_id`, `coupon_code`, `coupon_discount`, `shipping_charge`, `handling_charge`, `payment_type`, `created_at`, `updated_at`) VALUES
(1, '10000000', 'FAB', 12, 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 36, 500085, 2, 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 1, 500085, NULL, 1560, 1, NULL, NULL, 0, NULL, 40, 'c', '2021-01-28 22:48:57', '2021-01-28 22:48:57'),
(2, '10000001', 'FAB', 12, 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 36, 500085, 2, 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 1, 500085, NULL, 990, 1, NULL, NULL, 0, NULL, 40, 'c', '2021-01-29 00:52:35', '2021-01-29 00:52:35'),
(3, '10000002', 'FAB', 12, 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 36, 500085, 2, 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 1, 500085, NULL, 1180, 1, NULL, NULL, 0, NULL, 40, 'c', '2021-01-29 00:54:11', '2021-01-29 00:54:11'),
(4, '10000003', 'FAB', 12, 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 36, 500085, 2, 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 1, 500085, NULL, 1180, 1, NULL, NULL, 0, NULL, 40, 'c', '2021-01-29 01:05:52', '2021-01-29 01:05:52'),
(5, '10000004', 'FAB', 12, 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 36, 500085, 2, 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 1, 500085, NULL, 760, 1, 4, NULL, 80, NULL, 40, 'c', '2021-01-29 03:04:47', '2021-01-29 03:04:47'),
(6, '10000005', 'FAB', 12, 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 36, 500085, 4, 'Lavanya', 'Lavanya', 'harikrishna.miraki@gmail.com', '7780361239', 'Hyderabad', 'Hyderabad', 'Hyderabad', 'Telangana', 1, 500085, NULL, 800, 1, NULL, NULL, 0, NULL, 40, 'c', '2021-01-29 03:10:51', '2021-01-29 03:10:51'),
(7, '10000006', 'FAB', 8, 'lavanyatest', 'lavanyatest', 'harikrishna.miraki@gmail.com', '7780361239', 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', '142/A abhi grand,hafezpet', '142/A abhi grand,hafezpet', 'hyderabad', 'Telangana', 36, 500085, 2, 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', '142/A abhi grand,hafezpet', '142/A abhi grand,hafezpet', 'hyderabad', 'Telangana', 1, 500085, NULL, 1227, 1, NULL, NULL, 0, NULL, 120, 'c', '2021-02-02 23:36:48', '2021-02-02 23:36:48'),
(8, '10000007', 'FAB', 8, 'lavanyatest', 'lavanyatest', 'harikrishna.miraki@gmail.com', '7780361239', 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', '142/A,abhigrand,hafezpet.', '142/A,abhigrand,hafezpet.', 'hyderabad', 'Telangana', 36, 500085, 2, 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', '142/A,abhigrand,hafezpet.', '142/A,abhigrand,hafezpet.', 'hyderabad', 'Telangana', 1, 500085, NULL, 1147, 1, NULL, NULL, 0, NULL, 40, 'c', '2021-02-03 00:52:57', '2021-02-03 00:52:57'),
(9, '10000008', 'FAB', 8, 'lavanyatest', 'lavanyatest', 'harikrishna.miraki@gmail.com', '7780361239', 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', '142/A,abhigrand,hafezpet.', '142/A,abhigrand,hafezpet.', 'hyderabad', 'Telangana', 36, 500085, 2, 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', '142/A,abhigrand,hafezpet.', '142/A,abhigrand,hafezpet.', 'hyderabad', 'Telangana', 1, 500085, NULL, 1885, 8, NULL, NULL, 0, NULL, 40, 'c', '2021-02-03 00:57:56', '2021-02-03 00:57:56'),
(10, '10000009', 'FAB', 8, 'lavanyatest', 'lavanyatest', 'harikrishna.miraki@gmail.com', '7780361239', 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', '142/A,abhigrand,hafezpet.', '142/A,abhigrand,hafezpet.', 'hyderabad', 'Telangana', 36, 500085, 2, 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', '142/A,abhigrand,hafezpet.', '142/A,abhigrand,hafezpet.', 'hyderabad', 'Telangana', 1, 500085, NULL, 1885, 8, NULL, NULL, 0, NULL, 40, 'c', '2021-02-03 01:00:06', '2021-02-03 01:00:06'),
(11, '10000010', 'FAB', 8, 'lavanyatest', 'lavanyatest', 'harikrishna.miraki@gmail.com', '7780361239', 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', '142/A,abhigrand,hafezpet.', '142/A,abhigrand,hafezpet.', 'hyderabad', 'Telangana', 36, 500085, 4, 'lavanya', 'lavanya', 'harikrishna.miraki@gmail.com', '7780361239', '142/A,abhigrand,hafezpet.', '142/A,abhigrand,hafezpet.', 'hyderabad', 'Telangana', 1, 500085, NULL, 1639, 1, NULL, NULL, 0, NULL, 80, 'c', '2021-02-03 18:35:13', '2021-02-03 18:35:13');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int NOT NULL,
  `order_id` int DEFAULT NULL,
  `child_order_id` int NOT NULL,
  `invoice_no` int DEFAULT NULL,
  `tracking_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipment_id` int DEFAULT NULL,
  `awb_courier_id` int NOT NULL DEFAULT '0',
  `images` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `thumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_id` int DEFAULT NULL,
  `seller_warehouse_pincode` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `product_variant_id` int DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `mrp` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `shipping_weight` double DEFAULT NULL,
  `shipping_length` double DEFAULT NULL,
  `shipping_breadth` double DEFAULT NULL,
  `shipping_height` double DEFAULT NULL,
  `coupon_id` int DEFAULT NULL,
  `coupon_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_discount` double DEFAULT NULL,
  `dis_type` enum('f','p') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_type` enum('m','s') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_seller_discount` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `cod_charge` int DEFAULT '0',
  `tax` double DEFAULT NULL,
  `sgst` double DEFAULT NULL,
  `cgst` double DEFAULT NULL,
  `courier_charges` double DEFAULT NULL,
  `courier_sgst` double DEFAULT NULL,
  `courier_cgst` double DEFAULT NULL,
  `commission` double DEFAULT NULL,
  `commission_sgst` double DEFAULT NULL,
  `commission_cgst` double DEFAULT NULL,
  `final_amount_to_pay_seller` double DEFAULT NULL,
  `payout_status` tinyint DEFAULT '0',
  `payout_date_from_admin` date DEFAULT NULL,
  `shipping_status_id` int DEFAULT NULL,
  `shipping_charge` double DEFAULT NULL,
  `shipping_charge_gst` int DEFAULT NULL,
  `shipping_charge_sgst` double DEFAULT NULL,
  `shipping_charge_cgst` double DEFAULT NULL,
  `order_handling_charge` double DEFAULT NULL,
  `order_handling_charge_sgst` double DEFAULT NULL,
  `order_handling_charge_cgst` double DEFAULT NULL,
  `order_status_id` int DEFAULT NULL,
  `payment_status_id` int DEFAULT NULL,
  `delivered_date` date DEFAULT NULL,
  `excepted_delivery_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `child_order_id`, `invoice_no`, `tracking_no`, `shipment_id`, `awb_courier_id`, `images`, `thumbnail`, `seller_id`, `seller_warehouse_pincode`, `product_id`, `product_variant_id`, `name`, `quantity`, `mrp`, `price`, `shipping_weight`, `shipping_length`, `shipping_breadth`, `shipping_height`, `coupon_id`, `coupon_code`, `coupon_discount`, `dis_type`, `sub_type`, `total_seller_discount`, `discount`, `total`, `cod_charge`, `tax`, `sgst`, `cgst`, `courier_charges`, `courier_sgst`, `courier_cgst`, `commission`, `commission_sgst`, `commission_cgst`, `final_amount_to_pay_seller`, `payout_status`, `payout_date_from_admin`, `shipping_status_id`, `shipping_charge`, `shipping_charge_gst`, `shipping_charge_sgst`, `shipping_charge_cgst`, `order_handling_charge`, `order_handling_charge_sgst`, `order_handling_charge_cgst`, `order_status_id`, `payment_status_id`, `delivered_date`, `excepted_delivery_date`, `created_at`, `updated_at`) VALUES
(1, 1, 10000000, NULL, NULL, NULL, 0, '[\"100007\\/products\\/29012021\\/DSC01092-1.jpg\",\"100007\\/products\\/29012021\\/DSC01127-600x750.jpg\",\"100007\\/products\\/29012021\\/DSC05567 (1).jpg\",null,null,null]', '1', 18, 500085, 3, 7, 'Hyderaba', 8, 1600, 1520, 70, 40, 30, 30, NULL, NULL, 0, 'f', 'm', 80, 10, 1560, 0, 152, 76, 76, NULL, NULL, NULL, 212.8, 106.4, 106.4, 983.48, 0, NULL, 1, 49, NULL, 4.41, 4.41, 45, 4.05, 4.05, 1, 1, NULL, '2021-02-02', '2021-01-28 22:48:57', '2021-03-09 12:57:11'),
(2, 2, 10001234, NULL, NULL, NULL, 0, '[\"100007\\/products\\/29012021\\/DSC01092-1.jpg\",\"100007\\/products\\/29012021\\/DSC01127-600x750.jpg\",\"100007\\/products\\/29012021\\/DSC05567 (1).jpg\",null,null,null]', '1', 13, 500085, 4, 8, 'Hyderaba', 5, 1000, 950, 70, 40, 30, 30, NULL, NULL, 0, 'f', 'm', 50, 10, 990, 0, 95, 47.5, 47.5, NULL, NULL, NULL, 133, 66.5, 66.5, 595.5, 0, NULL, 1, 49, NULL, 4.41, 4.41, 26, 2.34, 2.34, 1, 1, NULL, '2021-02-02', '2021-01-29 00:52:35', '2021-03-09 12:57:12'),
(3, 3, 10000002, NULL, NULL, NULL, 0, '[\"100007\\/products\\/29012021\\/DSC01092-1.jpg\",\"100007\\/products\\/29012021\\/DSC01127-600x750.jpg\",\"100007\\/products\\/29012021\\/DSC05567 (1).jpg\",null,null,null]', '1', 18, 500085, 3, 7, 'Hyderaba', 6, 1200, 1140, 70, 40, 30, 30, NULL, NULL, 0, 'f', 'm', 60, 10, 1180, 0, 114, 57, 57, NULL, NULL, NULL, 159.6, 79.8, 79.8, 709.88, 0, NULL, 1, 49, NULL, 4.41, 4.41, 45, 4.05, 4.05, 1, 1, NULL, '2021-02-02', '2021-01-29 00:54:11', '2021-03-09 12:13:04'),
(4, 4, 10000003, NULL, NULL, NULL, 0, '[\"100007\\/products\\/29012021\\/DSC01092-1.jpg\",\"100007\\/products\\/29012021\\/DSC01127-600x750.jpg\",\"100007\\/products\\/29012021\\/DSC05567 (1).jpg\",null,null,null]', '1', 13, 500085, 4, 8, 'Hyderaba', 6, 1200, 1140, 70, 40, 30, 30, NULL, NULL, 0, 'f', 'm', 60, 10, 1180, 0, 114, 57, 57, NULL, NULL, NULL, 159.6, 79.8, 79.8, 709.88, 0, NULL, 1, 49, NULL, 4.41, 4.41, 45, 4.05, 4.05, 1, 1, NULL, '2021-02-02', '2021-01-29 01:05:52', '2021-03-09 12:14:33'),
(5, 5, 10000004, NULL, NULL, NULL, 0, '[\"100007\\/products\\/29012021\\/DSC01092-1.jpg\",\"100007\\/products\\/29012021\\/DSC01127-600x750.jpg\",\"100007\\/products\\/29012021\\/DSC05567 (1).jpg\",null,null,null]', '1', 13, 500085, 3, 7, 'Hyderaba', 4, 800, 720, 70, 40, 30, 30, 4, NULL, 80, 'f', 'm', 40, 10, 760, 0, 72, 36, 36, NULL, NULL, NULL, 100.8, 50.4, 50.4, 435.8, 0, NULL, 1, 49, NULL, 4.41, 4.41, 21, 1.89, 1.89, 1, 1, NULL, '2021-02-02', '2021-01-29 03:04:47', '2021-03-09 12:14:35'),
(6, 6, 10000005, NULL, NULL, NULL, 0, '[\"100007\\/products\\/29012021\\/DSC01092-1.jpg\",\"100007\\/products\\/29012021\\/DSC01127-600x750.jpg\",\"100007\\/products\\/29012021\\/DSC05567 (1).jpg\",null,null,null]', '1', 18, 500085, 4, 8, 'Hyderaba', 4, 800, 760, 70, 40, 30, 30, NULL, NULL, 0, 'f', 'm', 40, 10, 800, 0, 76, 38, 38, NULL, NULL, NULL, 106.4, 53.2, 53.2, 458.7, 0, NULL, 1, 49, NULL, 4.41, 4.41, 26, 2.34, 2.34, 1, 1, NULL, '2021-02-02', '2021-01-29 03:10:51', '2021-03-09 12:14:37'),
(7, 7, 10000006, NULL, NULL, NULL, 0, '[\"100007\\/products\\/01022021\\/901104423 FRONT.jpg\",\"100007\\/products\\/01022021\\/901104423 BACK.jpg\",\"100007\\/products\\/01022021\\/901104423 PRINT.jpg\",null,null,null]', '1', 13, 500085, 3, 7, 'Hyderaba', 1, 369, 1107, 0.25, 50, 30, 15, NULL, NULL, 0, 'f', 'm', 0, 0, 369, 40, 0, 0, 0, NULL, NULL, NULL, 51.66, 25.83, 25.83, 190.16, 0, NULL, 1, 49, NULL, 4.41, 4.41, 15, 1.35, 1.35, 1, 1, NULL, '2021-02-07', '2021-02-02 23:36:48', '2021-03-09 12:14:39'),
(8, 7, 10000007, NULL, NULL, NULL, 0, '[\"100007\\/products\\/01022021\\/901104423 FRONT.jpg\",\"100007\\/products\\/01022021\\/901104423 BACK.jpg\",\"100007\\/products\\/01022021\\/901104423 PRINT.jpg\",null,null,null]', '1', 13, 500085, 4, 8, 'Hyderaba', 1, 369, 1107, 0.25, 50, 30, 15, NULL, NULL, 0, 'f', 'm', 0, 0, 369, 40, 0, 0, 0, NULL, NULL, NULL, 51.66, 25.83, 25.83, 190.16, 0, NULL, 1, 49, NULL, 4.41, 4.41, 15, 1.35, 1.35, 1, 1, NULL, '2021-02-07', '2021-02-02 23:36:48', '2021-03-09 12:14:40'),
(9, 7, 10000008, NULL, NULL, NULL, 0, '[\"100007\\/products\\/01022021\\/401084423 FRONT.jpg\",\"100007\\/products\\/01022021\\/401084423 BACK.jpg\",\"100007\\/products\\/01022021\\/401084423 PRINT.jpg\",null,null,null]', '1', 18, 500085, 3, 7, 'Hyderaba', 1, 369, 1107, 0.25, 50, 30, 15, NULL, NULL, 0, 'f', 'm', 0, 0, 369, 40, 0, 0, 0, NULL, NULL, NULL, 51.66, 25.83, 25.83, 190.16, 0, NULL, 1, 49, NULL, 4.41, 4.41, 15, 1.35, 1.35, 1, 1, NULL, '2021-02-07', '2021-02-02 23:36:48', '2021-03-09 12:17:37'),
(10, 8, 10000009, NULL, NULL, NULL, 0, '[\"100007\\/products\\/01022021\\/401019023 FRONT.jpg\",\"100007\\/products\\/01022021\\/401019023 BACK.jpg\",\"100007\\/products\\/01022021\\/401019023 PRINT.jpg\",null,null,null]', '1', 13, 500085, 4, 8, 'Hyderaba', 3, 369, 369, 0.25, 50, 30, 15, NULL, NULL, 0, 'f', 'm', 0, 0, 1107, 40, 0, 0, 0, NULL, NULL, NULL, 154.98, 77.49, 77.49, 686.12, 0, NULL, 1, 49, NULL, 4.41, 4.41, 45, 4.05, 4.05, 3, 1, NULL, '2021-02-07', '2021-02-03 00:52:57', '2021-03-16 07:33:49'),
(11, 9, 10000010, 10000007, NULL, NULL, 0, '[\"100007\\/products\\/01022021\\/401019023 FRONT.jpg\",\"100007\\/products\\/01022021\\/401019023 BACK.jpg\",\"100007\\/products\\/01022021\\/401019023 PRINT.jpg\",null,null,null]', '1', 18, 500085, 3, 7, 'Hyderaba', 5, 369, 369, 0.25, 50, 30, 15, NULL, NULL, 0, 'f', 'm', 0, 0, 1845, 40, 0, 0, 0, NULL, NULL, NULL, 258.3, 23.52, 23.52, 1217.48, 0, NULL, 6, 49, NULL, 4.41, 4.41, 45, 4.05, 4.05, 8, 1, '2021-03-16', '2021-02-07', '2021-02-03 00:57:56', '2021-03-16 11:07:26'),
(12, 10, 10000011, 10000009, NULL, 84153991, 0, '[\"100007\\/products\\/01022021\\/901104423 FRONT.jpg\",\"100007\\/products\\/01022021\\/901104423 BACK.jpg\",\"100007\\/products\\/01022021\\/901104423 PRINT.jpg\",null,null,null]', '1', 13, 500085, 4, 8, 'Hyderaba', 5, 369, 369, 0.25, 50, 30, 15, NULL, NULL, 0, 'f', 'm', 0, 0, 1845, 40, 0, 0, 0, NULL, NULL, NULL, 258.3, 23.52, 23.52, 1217.48, 0, NULL, 3, 49, NULL, 4.41, 4.41, 45, 4.05, 4.05, 8, 1, '2021-03-17', '2021-02-07', '2021-03-09 01:00:06', '2021-03-23 08:31:26'),
(13, 11, 10000012, 10000004, NULL, 84157034, 0, '[\"100007\\/products\\/01022021\\/901059023 FRONT.jpg\",\"100007\\/products\\/01022021\\/901059023 BACK.jpg\",\"100007\\/products\\/01022021\\/901059023 PRINT.jpg\",null,null,null]', '1', 13, 500085, 3, 7, 'Hyderaba', 1, 369, 369, 0.25, 50, 30, 15, NULL, NULL, 0, 'f', 'm', 0, 0, 369, 40, 0, 0, 0, NULL, NULL, NULL, 51.66, 25.83, 25.83, 190.16, 0, NULL, 2, 49, NULL, 4.41, 4.41, 15, 1.35, 1.35, 4, 1, '2021-02-16', '2021-02-08', '2021-03-08 18:35:13', '2021-03-23 08:38:14'),
(14, 11, 10000013, 10000005, NULL, NULL, 0, '[\"100001\\/products\\/02022021\\/DSC_9203.jpg\",\"100001\\/products\\/02022021\\/DSC_9201.jpg\",\"100001\\/products\\/02022021\\/DSC_9204.jpg\",\"100001\\/products\\/02022021\\/DSC_9198.jpg\",\"100001\\/products\\/02022021\\/DSC_9206.jpg\",null]', '1', 18, 500085, 4, 8, 'gokulplots', 1, 1200, 1190, 12, 12, 12, 12, NULL, NULL, 0, 'f', 'm', 10, 10, 1190, 40, 119, 59.5, 59.5, NULL, NULL, NULL, 166.6, 83.3, 83.3, 745.88, 0, NULL, 10, 49, NULL, 4.41, 4.41, 45, 4.05, 4.05, 4, 1, '2021-02-16', '2021-02-08', '2021-03-08 18:35:13', '2021-03-16 11:30:42');

-- --------------------------------------------------------

--
-- Table structure for table `order_handling_charge`
--

CREATE TABLE `order_handling_charge` (
  `id` int NOT NULL,
  `min_amount` int NOT NULL DEFAULT '0',
  `max_amount` int NOT NULL DEFAULT '0',
  `charge_amount` int NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_handling_charge`
--

INSERT INTO `order_handling_charge` (`id`, `min_amount`, `max_amount`, `charge_amount`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 250, 8, 1, '2020-11-12 15:50:40', '2020-12-09 01:07:05', NULL),
(2, 251, 500, 15, 1, '2020-11-12 16:10:14', '2020-12-09 00:59:03', NULL),
(3, 501, 750, 21, 1, '2020-12-09 01:00:36', '2020-12-09 01:00:41', NULL),
(4, 751, 1000, 26, 1, '2020-12-09 01:00:36', '2020-12-09 01:00:41', NULL),
(5, 1001, 2000, 45, 1, '2020-12-09 01:00:36', '2020-12-09 01:00:41', NULL),
(7, 2001, 0, 60, 1, '2020-12-09 01:00:36', '2020-12-09 01:00:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_histories`
--

CREATE TABLE `order_histories` (
  `id` int NOT NULL,
  `order_id` int DEFAULT NULL,
  `order_detail_id` int DEFAULT NULL,
  `order_status_id` int DEFAULT NULL,
  `shipping_status_id` int DEFAULT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_histories`
--

INSERT INTO `order_histories` (`id`, `order_id`, `order_detail_id`, `order_status_id`, `shipping_status_id`, `notify`, `comment`, `created_at`, `updated_at`) VALUES
(20, 20, 20, 1, 1, 0, NULL, '2021-01-21 05:22:20', '2021-01-21 05:22:20'),
(21, 21, 21, 1, 1, 0, NULL, '2021-01-21 05:27:30', '2021-01-21 05:27:30'),
(22, 21, 22, 1, 1, 0, NULL, '2021-01-21 05:27:30', '2021-01-21 05:27:30'),
(23, 21, 22, 4, 1, 0, NULL, '2021-01-21 11:01:07', NULL),
(24, 22, 23, 1, 1, 0, NULL, '2021-01-21 07:01:07', '2021-01-21 07:01:07'),
(25, 22, 24, 1, 1, 0, NULL, '2021-01-21 07:01:08', '2021-01-21 07:01:08'),
(26, 22, 24, 4, 1, 0, NULL, '2021-01-21 13:43:09', NULL),
(27, 22, 24, 3, NULL, 0, NULL, '2021-01-21 13:45:08', NULL),
(28, 22, 24, 4, 1, 0, NULL, '2021-01-21 13:47:15', NULL),
(29, 22, 24, 10, 3, 0, NULL, '2021-01-21 13:48:31', NULL),
(30, 23, 25, 1, 1, 0, NULL, '2021-01-22 02:31:26', '2021-01-22 02:31:26'),
(31, 24, 26, 1, 1, 0, NULL, '2021-01-22 03:00:42', '2021-01-22 03:00:42'),
(32, 24, 27, 1, 1, 0, NULL, '2021-01-22 03:00:43', '2021-01-22 03:00:43'),
(33, 25, 28, 1, 1, 0, NULL, '2021-01-22 03:02:43', '2021-01-22 03:02:43'),
(34, 26, 29, 1, 1, 0, NULL, '2021-01-22 03:05:57', '2021-01-22 03:05:57'),
(35, 27, 30, 1, 1, 0, NULL, '2021-01-22 03:08:17', '2021-01-22 03:08:17'),
(36, 28, 31, 1, 1, 0, NULL, '2021-01-22 03:08:42', '2021-01-22 03:08:42'),
(37, 29, 32, 1, 1, 0, NULL, '2021-01-22 03:19:59', '2021-01-22 03:19:59'),
(38, 30, 33, 1, 1, 0, NULL, '2021-01-22 04:50:45', '2021-01-22 04:50:45'),
(39, 31, 34, 1, 1, 0, NULL, '2021-01-22 05:43:12', '2021-01-22 05:43:12'),
(40, 32, 35, 1, 1, 0, NULL, '2021-01-22 05:44:37', '2021-01-22 05:44:37'),
(41, 33, 36, 1, 1, 0, NULL, '2021-01-22 05:53:44', '2021-01-22 05:53:44'),
(42, 20, 20, 4, 1, 0, NULL, '2021-01-22 12:02:31', NULL),
(43, 24, 26, 3, NULL, 0, NULL, '2021-01-22 14:11:45', NULL),
(44, 34, 37, 1, 1, 0, NULL, '2021-01-25 00:02:22', '2021-01-25 00:02:22'),
(45, 34, 38, 1, 1, 0, NULL, '2021-01-25 00:02:23', '2021-01-25 00:02:23'),
(46, 34, 38, 4, 1, 0, NULL, '2021-01-25 05:35:55', NULL),
(47, 34, 37, 4, 1, 0, NULL, '2021-01-25 05:36:03', NULL),
(48, 35, 39, 1, 1, 0, NULL, '2021-01-25 00:34:19', '2021-01-25 00:34:19'),
(49, 35, 39, 4, 1, 0, NULL, '2021-01-25 06:37:37', NULL),
(50, 35, 39, 10, 3, 0, NULL, '2021-01-25 06:43:36', NULL),
(51, 34, 38, 3, NULL, 0, NULL, '2021-01-25 07:01:35', NULL),
(52, 34, 38, 7, NULL, 0, NULL, '2021-01-25 07:02:04', NULL),
(53, 34, 38, 7, NULL, 0, NULL, '2021-01-25 07:02:29', NULL),
(54, 34, 38, 10, 3, 0, NULL, '2021-01-25 07:02:35', NULL),
(55, 34, 38, 7, NULL, 0, NULL, '2021-01-25 07:02:43', NULL),
(56, 34, 38, 3, NULL, 0, NULL, '2021-01-25 07:06:33', NULL),
(57, 35, 39, 2, NULL, 0, NULL, '2021-01-25 07:13:27', NULL),
(58, 35, 39, 2, NULL, 0, NULL, '2021-01-25 07:13:34', NULL),
(59, 36, 40, 1, 1, 0, NULL, '2021-01-25 01:44:04', '2021-01-25 01:44:04'),
(60, 35, 39, 2, NULL, 0, NULL, '2021-01-25 07:15:11', NULL),
(61, 37, 41, 1, 1, 0, NULL, '2021-01-25 01:50:17', '2021-01-25 01:50:17'),
(62, 37, 41, 4, 1, 0, NULL, '2021-01-25 07:22:59', NULL),
(63, 37, 41, NULL, 2, 0, NULL, '2021-01-25 07:25:42', NULL),
(64, 37, 41, 10, 3, 0, NULL, '2021-01-25 07:26:37', NULL),
(65, 38, 42, 1, 1, 0, NULL, '2021-01-25 02:17:56', '2021-01-25 02:17:56'),
(66, 37, 41, 4, 1, 0, NULL, '2021-01-25 07:54:36', NULL),
(67, 37, 41, 10, 3, 0, NULL, '2021-01-25 07:55:30', NULL),
(68, 38, 42, 4, 1, 0, NULL, '2021-01-25 07:56:58', NULL),
(69, 38, 42, NULL, 2, 0, NULL, '2021-01-25 07:57:43', NULL),
(70, 38, 42, 10, 3, 0, NULL, '2021-01-25 07:59:54', NULL),
(71, 39, 43, 1, 1, 0, NULL, '2021-01-25 03:43:55', '2021-01-25 03:43:55'),
(72, 39, 43, 4, 1, 0, NULL, '2021-01-25 09:15:09', NULL),
(73, 39, 43, NULL, 2, 0, NULL, '2021-01-25 09:21:55', NULL),
(74, 39, 43, 10, 3, 0, NULL, '2021-01-25 09:22:34', NULL),
(75, 40, 44, 1, 1, 0, NULL, '2021-01-25 04:02:03', '2021-01-25 04:02:03'),
(76, 40, 44, 4, 1, 0, NULL, '2021-01-25 09:32:47', NULL),
(77, 40, 44, NULL, 2, 0, NULL, '2021-01-25 09:57:50', NULL),
(78, 40, 44, 10, 3, 0, NULL, '2021-01-25 10:04:26', NULL),
(79, 21, 22, 2, NULL, 0, NULL, '2021-01-25 10:13:22', NULL),
(80, 41, 45, 1, 1, 0, NULL, '2021-01-25 04:50:59', '2021-01-25 04:50:59'),
(81, 41, 45, 2, NULL, 0, NULL, '2021-01-25 10:22:15', NULL),
(82, 42, 46, 1, 1, 0, NULL, '2021-01-25 05:01:50', '2021-01-25 05:01:50'),
(83, 43, 47, 1, 1, 0, NULL, '2021-01-25 05:15:17', '2021-01-25 05:15:17'),
(84, 43, 47, 3, NULL, 0, NULL, '2021-01-25 11:25:44', NULL),
(91, 43, 47, 3, NULL, 0, NULL, '2021-01-25 11:36:21', NULL),
(93, 43, 47, 10, 3, 0, NULL, '2021-01-25 11:38:31', NULL),
(95, 43, 47, 3, NULL, 0, NULL, '2021-01-25 11:39:51', NULL),
(99, 44, 48, 1, 1, 0, NULL, '2021-01-25 06:35:46', '2021-01-25 06:35:46'),
(100, 44, 48, 3, NULL, 0, NULL, '2021-01-25 12:37:29', NULL),
(101, 45, 49, 1, 1, 0, NULL, '2021-01-25 07:11:25', '2021-01-25 07:11:25'),
(103, 45, 49, 4, 1, 0, NULL, '2021-01-25 12:47:40', NULL),
(104, 44, 48, 3, NULL, 0, NULL, '2021-01-25 12:47:42', NULL),
(105, 45, 49, NULL, 2, 0, NULL, '2021-01-25 12:48:21', NULL),
(106, 45, 49, 3, NULL, 0, NULL, '2021-01-25 12:48:24', NULL),
(107, 45, 49, NULL, 2, 0, NULL, '2021-01-25 12:48:34', NULL),
(108, 45, 49, NULL, 2, 0, NULL, '2021-01-25 12:50:19', NULL),
(109, 45, 49, 4, 1, 0, NULL, '2021-01-25 12:51:35', NULL),
(110, 45, 49, NULL, 2, 0, NULL, '2021-01-25 12:51:49', NULL),
(111, 46, 50, 1, 1, 0, NULL, '2021-01-25 08:08:19', '2021-01-25 08:08:19'),
(112, 46, 50, 4, 1, 0, NULL, '2021-01-25 13:39:22', NULL),
(113, 46, 50, NULL, 2, 0, NULL, '2021-01-25 13:39:32', NULL),
(114, 47, 51, 1, 1, 0, NULL, '2021-01-25 08:14:43', '2021-01-25 08:14:43'),
(115, 47, 51, 4, 1, 0, NULL, '2021-01-25 13:45:44', NULL),
(116, 47, 51, NULL, 2, 0, NULL, '2021-01-25 13:45:52', NULL),
(117, 47, 51, 3, NULL, 0, NULL, '2021-01-25 14:12:21', NULL),
(118, 46, 50, 3, NULL, 0, NULL, '2021-01-25 14:20:44', NULL),
(119, 48, 52, 1, 1, 0, NULL, '2021-01-27 23:42:00', '2021-01-27 23:42:00'),
(120, 48, 52, 3, NULL, 0, NULL, '2021-01-28 05:13:04', NULL),
(123, 49, 53, 1, 1, 0, NULL, '2021-01-28 00:01:22', '2021-01-28 00:01:22'),
(124, 29, 32, 4, 1, 0, NULL, '2021-01-28 06:41:14', NULL),
(125, 28, 31, 4, 1, 0, NULL, '2021-01-28 06:41:15', NULL),
(126, 30, 33, 4, 1, 0, NULL, '2021-01-28 06:41:24', NULL),
(127, 29, 32, NULL, 2, 0, NULL, '2021-01-28 06:41:31', NULL),
(128, 28, 31, NULL, 2, 0, NULL, '2021-01-28 06:41:32', NULL),
(129, 50, 54, 1, 1, 0, NULL, '2021-01-28 01:15:35', '2021-01-28 01:15:35'),
(130, 50, 54, 4, 1, 0, NULL, '2021-01-28 06:51:55', NULL),
(131, 50, 54, NULL, 2, 0, NULL, '2021-01-28 06:53:11', NULL),
(132, 50, 54, 10, 3, 0, NULL, '2021-01-28 06:53:31', NULL),
(134, 48, 52, 7, NULL, 0, NULL, '2021-01-28 07:04:35', NULL),
(135, 45, 49, 10, 3, 0, NULL, '2021-01-28 07:19:12', NULL),
(136, 30, 33, NULL, 2, 0, NULL, '2021-01-28 07:43:27', NULL),
(137, 30, 33, 10, 3, 0, NULL, '2021-01-28 07:47:30', NULL),
(138, 51, 55, 1, 1, 0, NULL, '2021-01-29 00:11:51', '2021-01-29 00:11:51'),
(139, 1, 1, 1, 1, 0, NULL, '2021-01-29 04:18:57', '2021-01-29 04:18:57'),
(140, 1, 1, 2, NULL, 0, NULL, '2021-01-29 10:09:21', NULL),
(141, 1, 1, 2, NULL, 0, NULL, '2021-01-29 10:09:30', NULL),
(142, 2, 2, 1, 1, 0, NULL, '2021-01-29 06:22:35', '2021-01-29 06:22:35'),
(143, 3, 3, 1, 1, 0, NULL, '2021-01-29 06:24:11', '2021-01-29 06:24:11'),
(144, 4, 4, 1, 1, 0, NULL, '2021-01-29 06:35:52', '2021-01-29 06:35:52'),
(145, 4, 4, 4, 1, 0, NULL, '2021-01-29 12:07:40', NULL),
(146, 4, 4, NULL, 2, 0, NULL, '2021-01-29 12:07:55', NULL),
(147, 3, 3, 3, NULL, 0, NULL, '2021-01-29 12:10:40', NULL),
(148, 5, 5, 1, 1, 0, NULL, '2021-01-29 08:34:47', '2021-01-29 08:34:47'),
(149, 6, 6, 1, 1, 0, NULL, '2021-01-29 08:40:51', '2021-01-29 08:40:51'),
(162, 11, 13, 6, NULL, 0, NULL, '2021-02-16 06:18:59', NULL),
(168, 11, 13, 5, NULL, 0, NULL, '2021-02-16 06:41:47', NULL),
(170, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:01:19', NULL),
(171, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:03:09', NULL),
(176, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:24:48', NULL),
(177, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:25:44', NULL),
(185, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:42:40', NULL),
(186, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:43:22', NULL),
(187, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:46:29', NULL),
(188, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:47:11', NULL),
(189, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:47:45', NULL),
(190, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:48:19', NULL),
(191, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:52:59', NULL),
(192, 11, 13, 6, NULL, 0, NULL, '2021-02-16 07:58:34', NULL),
(193, 11, 13, 7, NULL, 0, NULL, '2021-02-16 11:26:24', NULL),
(196, 11, 13, 10, 3, 0, NULL, '2021-02-16 12:58:49', NULL),
(199, 11, 13, 10, 3, 0, NULL, '2021-02-16 13:00:24', NULL),
(200, 10, 12, 10, 3, 0, NULL, '2021-02-16 13:01:37', NULL),
(201, 11, 13, 10, 3, 0, NULL, '2021-02-16 13:01:44', NULL),
(202, 11, 14, 10, 3, 0, NULL, '2021-02-16 13:01:51', NULL),
(203, 11, 14, 10, 3, 0, NULL, '2021-02-16 13:06:18', NULL),
(204, 11, 13, 8, 5, 0, NULL, '2021-02-16 14:00:19', NULL),
(205, 11, 14, 8, 5, 0, NULL, '2021-02-16 14:00:19', NULL),
(206, 10, 12, 8, 5, 0, NULL, '2021-02-16 14:00:19', NULL),
(207, 11, 13, 8, 5, 0, NULL, '2021-02-16 14:02:27', NULL),
(208, 11, 14, 8, 5, 0, NULL, '2021-02-16 14:02:27', NULL),
(209, 10, 12, 8, 5, 0, NULL, '2021-02-16 14:02:27', NULL),
(210, 11, 13, 8, 5, 0, NULL, '2021-02-16 14:13:04', NULL),
(211, 11, 14, 8, 5, 0, NULL, '2021-02-16 14:13:04', NULL),
(212, 10, 12, 8, 5, 0, NULL, '2021-02-16 14:13:04', NULL),
(213, 11, 13, NULL, 6, 0, NULL, '2021-02-16 14:43:21', NULL),
(214, 11, 14, NULL, 6, 0, NULL, '2021-02-16 14:43:21', NULL),
(215, 8, 10, 6, NULL, 0, NULL, '2021-02-22 13:28:52', NULL),
(216, 7, 7, 3, NULL, 0, NULL, '2021-02-23 08:13:00', NULL),
(217, 6, 6, 3, NULL, 0, NULL, '2021-02-23 08:13:30', NULL),
(219, 7, 7, 4, 1, 0, NULL, '2021-02-23 10:54:53', NULL),
(220, 6, 6, 4, 1, 0, NULL, '2021-02-23 11:00:32', NULL),
(221, 7, 8, 4, 1, 0, NULL, '2021-02-23 11:01:58', NULL),
(222, 8, 10, 4, 1, 0, NULL, '2021-02-23 11:09:34', NULL),
(223, 2, 2, 4, 1, 0, NULL, '2021-03-04 07:02:11', NULL),
(224, 2, 2, 4, 1, 0, NULL, '2021-03-04 07:04:32', NULL),
(225, 11, 13, 2, NULL, 0, NULL, '2021-03-04 08:17:27', NULL),
(226, 11, 14, 4, 1, 0, NULL, '2021-03-05 07:26:04', NULL),
(227, 1, 1, 4, 1, 0, NULL, '2021-03-09 10:22:14', NULL),
(228, 3, 3, 4, 1, 0, NULL, '2021-03-09 10:22:15', NULL),
(229, 4, 4, 4, 1, 0, NULL, '2021-03-09 10:22:16', NULL),
(230, 5, 5, 4, 1, 0, NULL, '2021-03-09 10:22:17', NULL),
(231, 7, 9, 4, 1, 0, NULL, '2021-03-09 10:22:18', NULL),
(232, 9, 11, 4, 1, 0, NULL, '2021-03-09 10:22:19', NULL),
(233, 10, 12, 4, 1, 0, NULL, '2021-03-09 10:22:20', NULL),
(234, 11, 13, 4, 1, 0, NULL, '2021-03-09 10:22:21', NULL),
(235, 1, 1, NULL, 2, 0, NULL, '2021-03-09 11:40:38', NULL),
(236, 2, 2, NULL, 2, 0, NULL, '2021-03-09 11:43:21', NULL),
(237, 1, 1, NULL, 2, 0, NULL, '2021-03-09 11:55:48', NULL),
(238, 2, 2, NULL, 2, 0, NULL, '2021-03-09 12:02:37', NULL),
(239, 1, 1, NULL, 2, 0, NULL, '2021-03-09 12:09:10', NULL),
(240, 1, 1, NULL, 2, 0, NULL, '2021-03-09 12:11:19', NULL),
(241, 2, 2, NULL, 2, 0, NULL, '2021-03-09 12:12:01', NULL),
(242, 3, 3, NULL, 2, 0, NULL, '2021-03-09 12:13:04', NULL),
(243, 4, 4, NULL, 2, 0, NULL, '2021-03-09 12:14:31', NULL),
(244, 4, 4, 10, 3, 0, NULL, '2021-03-09 12:14:33', NULL),
(245, 5, 5, NULL, 2, 0, NULL, '2021-03-09 12:14:34', NULL),
(246, 5, 5, 10, 3, 0, NULL, '2021-03-09 12:14:35', NULL),
(247, 6, 6, NULL, 2, 0, NULL, '2021-03-09 12:14:35', NULL),
(248, 6, 6, 10, 3, 0, NULL, '2021-03-09 12:14:37', NULL),
(249, 7, 7, NULL, 2, 0, NULL, '2021-03-09 12:14:38', NULL),
(250, 7, 7, 10, 3, 0, NULL, '2021-03-09 12:14:39', NULL),
(251, 7, 8, NULL, 2, 0, NULL, '2021-03-09 12:14:40', NULL),
(252, 7, 9, NULL, 2, 0, NULL, '2021-03-09 12:17:37', NULL),
(253, 8, 10, NULL, 2, 0, NULL, '2021-03-09 12:18:21', NULL),
(254, 1, 1, NULL, 2, 0, NULL, '2021-03-09 12:25:04', NULL),
(255, 1, 1, NULL, 2, 0, NULL, '2021-03-09 12:57:10', NULL),
(256, 1, 1, 10, 3, 0, NULL, '2021-03-09 12:57:12', NULL),
(257, 2, 2, NULL, 2, 0, NULL, '2021-03-09 12:57:12', NULL),
(264, 11, 13, 4, 1, 0, NULL, '2021-03-16 07:27:09', NULL),
(265, 11, 13, 4, 1, 0, NULL, '2021-03-16 07:27:09', NULL),
(266, 10, 12, 4, 1, 0, NULL, '2021-03-16 07:27:09', NULL),
(267, 11, 13, 4, 1, 0, NULL, '2021-03-16 07:33:39', NULL),
(268, 11, 13, 4, 1, 0, NULL, '2021-03-16 07:33:40', NULL),
(269, 10, 12, 4, 1, 0, NULL, '2021-03-16 07:33:40', NULL),
(270, 8, 10, 3, NULL, 0, NULL, '2021-03-16 07:33:49', NULL),
(271, 11, 14, 4, 1, 0, NULL, '2021-03-16 09:51:51', NULL),
(272, 11, 14, 4, 1, 0, NULL, '2021-03-16 09:51:51', NULL),
(273, 9, 11, 4, 1, 0, NULL, '2021-03-16 09:51:51', NULL),
(274, 11, 14, 4, 1, 0, NULL, '2021-03-16 09:55:44', NULL),
(275, 11, 14, 4, 1, 0, NULL, '2021-03-16 09:55:44', NULL),
(276, 9, 11, 4, 1, 0, NULL, '2021-03-16 09:55:44', NULL),
(277, 11, 14, NULL, 4, 0, NULL, '2021-03-16 10:21:22', NULL),
(278, 9, 11, NULL, 4, 0, NULL, '2021-03-16 10:35:13', NULL),
(279, 9, 11, 8, 5, 0, NULL, '2021-03-16 10:35:47', NULL),
(280, 9, 11, NULL, 6, 0, NULL, '2021-03-16 10:58:39', NULL),
(281, 11, 14, NULL, 6, 0, NULL, '2021-03-16 11:02:57', NULL),
(282, 9, 11, NULL, 6, 0, NULL, '2021-03-16 11:07:26', NULL),
(283, 11, 14, NULL, 10, 0, NULL, '2021-03-16 11:30:42', NULL),
(284, 10, 12, 8, 5, 0, NULL, '2021-03-17 11:13:15', NULL),
(285, 10, 12, NULL, 11, 0, NULL, '2021-03-17 11:14:04', NULL),
(286, 10, 12, 13, 11, 0, NULL, '2021-03-17 11:38:01', NULL),
(287, 10, 12, 13, 12, 0, NULL, '2021-03-17 11:38:17', NULL),
(288, 10, 12, NULL, 6, 0, NULL, '2021-03-17 11:39:26', NULL),
(289, 10, 12, 11, 6, 0, NULL, '2021-03-17 11:44:24', NULL),
(290, 10, 12, 11, 6, 0, NULL, '2021-03-17 11:46:53', NULL),
(291, 10, 12, 11, 7, 0, NULL, '2021-03-17 11:47:18', NULL),
(292, 10, 12, 11, 8, 0, NULL, '2021-03-17 11:47:29', NULL),
(293, 10, 12, 8, 5, 0, NULL, '2021-03-17 12:56:42', NULL),
(299, 11, 13, NULL, 4, 0, NULL, '2021-03-19 11:46:45', NULL),
(300, 11, 13, NULL, 4, 0, NULL, '2021-03-19 12:01:15', NULL),
(301, 11, 13, NULL, 4, 0, NULL, '2021-03-19 12:08:05', NULL),
(302, 11, 13, NULL, 4, 0, NULL, '2021-03-19 12:12:35', NULL),
(305, 11, 13, NULL, 4, 0, NULL, '2021-03-19 12:27:42', NULL),
(306, 11, 13, NULL, 4, 0, NULL, '2021-03-19 12:31:53', NULL),
(307, 10, 12, NULL, 2, 0, NULL, '2021-03-23 08:31:28', NULL),
(312, 11, 13, NULL, 2, 0, NULL, '2021-03-23 08:38:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_text` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_text` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_text` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify_email` enum('y','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify_sms` enum('y','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `admin_text`, `seller_text`, `user_text`, `notify_email`, `notify_sms`, `created_at`, `updated_at`) VALUES
(1, 'Pending/Active Orders', 'Pending From Seller', 'Pending From Seller', 'Order Placed', 'y', 'y', '2020-12-24 08:44:16', NULL),
(2, 'Cancelled', 'Cancelled', 'Cancelled', 'Cancelled', 'y', 'y', '2020-12-24 08:44:54', NULL),
(3, 'Rejected by Seller', 'Rejected by Seller', 'Rejected by Seller', 'Order Placed', 'y', 'n', '2020-12-24 09:17:59', NULL),
(4, 'Order Confirmed', 'Confirmed by Seller', 'Confirmed by Seller', 'Order Placed', 'n', 'n', '2020-12-24 08:44:54', NULL),
(5, 'Return Requested by User', 'Return Requested', 'Return Requested', 'Return Requested', 'n', 'n', '2020-12-24 08:44:54', NULL),
(6, 'Return Approved by Admin', 'Return Approved by Admin', 'Return Approved by Admin', 'Return Requested', 'n', 'n', '2020-12-24 08:44:54', NULL),
(7, 'Seller Rejection Approved by Admin', 'Rejection Approved by Admin', 'Rejection Approved by Admin', 'Problem with the Order', 'y', 'y', '2020-12-24 08:44:54', NULL),
(8, 'Delivered', 'Delivered', 'Delivered', 'Delivered', 'y', 'y', '2020-12-24 08:44:54', NULL),
(9, 'Completed', 'Completed', 'Completed', 'Delivered', 'n', 'n', '2020-12-24 08:44:54', NULL),
(10, 'Generate AWB', 'AWB Created', 'Order Confirmed', 'Order Placed', 'n', 'n', '2020-12-24 08:44:54', NULL),
(11, 'Not Delivered', 'Delivery Unsuccessful', 'Delivery Unsuccessful', 'Delivery Unsuccessful', NULL, NULL, '2020-12-24 09:17:59', NULL),
(12, 'Product Returned', 'Product Returned', 'Product Returned', 'Product Returned', NULL, NULL, '2021-03-17 11:24:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint NOT NULL,
  `customer_id` int DEFAULT NULL,
  `order_id` int DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `receipt_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `razorpay_payment_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `razorpay_signature` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `customer_id`, `order_id`, `email`, `mobile`, `name`, `amount`, `receipt_id`, `currency`, `razorpay_payment_id`, `razorpay_signature`, `payment_status_id`, `created_at`, `updated_at`) VALUES
(1, 7, NULL, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 450, 'order_GRubRbJ5LAV7xe', 'INR', NULL, NULL, 2, '2021-01-21 04:15:04', '2021-01-21 04:15:04'),
(2, 1, NULL, 'mohammedavez85@gmail.com', '9550261360', 'Avez', 450, 'order_GRuxAYFgcKS93t', 'INR', NULL, NULL, 2, '2021-01-21 04:35:38', '2021-01-21 04:35:38'),
(3, 1, NULL, 'mohammedavez85@gmail.com', '9550261360', 'Avez', 450, 'order_GRv3UPXx0LjiMb', 'INR', NULL, NULL, 2, '2021-01-21 04:41:37', '2021-01-21 04:41:37'),
(4, 5, NULL, 'lavanya@miraki.com', '7780361239', 'lavanya', 2564, 'order_GRvASuBwsaLfAH', 'INR', NULL, NULL, 2, '2021-01-21 04:48:14', '2021-01-21 04:48:14'),
(5, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvGH8aD3oG178', 'INR', NULL, NULL, 2, '2021-01-21 04:53:44', '2021-01-21 04:53:44'),
(6, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvM6COFBJjt8M', 'INR', NULL, NULL, 2, '2021-01-21 04:59:14', '2021-01-21 04:59:14'),
(7, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvN8pOAfu8ANQ', 'INR', NULL, NULL, 2, '2021-01-21 05:00:14', '2021-01-21 05:00:14'),
(8, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvOR7NWOJd8gF', 'INR', NULL, NULL, 2, '2021-01-21 05:01:27', '2021-01-21 05:01:27'),
(9, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvQigtt7qJBnY', 'INR', NULL, NULL, 2, '2021-01-21 05:03:37', '2021-01-21 05:03:37'),
(10, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvUuoS3jkVyLV', 'INR', NULL, NULL, 2, '2021-01-21 05:07:35', '2021-01-21 05:07:35'),
(11, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvY9fd2NtiXP5', 'INR', NULL, NULL, 2, '2021-01-21 05:10:39', '2021-01-21 05:10:39'),
(12, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvZs1PPJBXTyY', 'INR', NULL, NULL, 2, '2021-01-21 05:12:17', '2021-01-21 05:12:17'),
(13, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvbVoZ1n5OMLT', 'INR', NULL, NULL, 2, '2021-01-21 05:13:50', '2021-01-21 05:13:50'),
(14, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRve9OSyleyDAh', 'INR', NULL, NULL, 2, '2021-01-21 05:16:20', '2021-01-21 05:16:20'),
(15, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvg4jbNuVz3ha', 'INR', NULL, NULL, 2, '2021-01-21 05:18:09', '2021-01-21 05:18:09'),
(16, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvgh1BI6eQ3HF', 'INR', NULL, NULL, 2, '2021-01-21 05:18:44', '2021-01-21 05:18:44'),
(17, 8, NULL, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvhKeZYO1dW89', 'INR', NULL, NULL, 2, '2021-01-21 05:19:21', '2021-01-21 05:19:21'),
(18, 8, 10000000, 'avezmiraki@gmail.com', '9290573910', 'Hari', 450, 'order_GRvkDwLC9wvonx', 'INR', 'pay_GRvkS5T2Orh4nk', 'add43534bb2a3810c426210602206c460241841bd3e1d7e5d368794815c5b3b1', 1, '2021-01-21 05:22:05', '2021-01-21 05:22:30'),
(19, 9, 10000002, 'harik@mirakitech.com', '6290844051', 'Avez', 3264, 'order_GRxQ9f06qlE9cW', 'INR', 'pay_GRxQkS5CADVtgP', '2315682d8d5e0702fdaad7d732198ae7712a56bfab834052ae1ef7baa1bc6c7d', 1, '2021-01-21 07:00:28', '2021-01-21 07:01:22'),
(20, 9, NULL, 'harik@mirakitech.com', '6290844051', 'Avez', 450, 'order_GRxUMPoXg4jzek', 'INR', NULL, NULL, 2, '2021-01-21 07:04:27', '2021-01-21 07:04:27'),
(21, 6, NULL, 'abhishekmiraki@gmail.com', '9948839362', 'Abhi', 955, 'order_GSH8AH49yc5SqU', 'INR', NULL, NULL, 2, '2021-01-22 02:17:19', '2021-01-22 02:17:19'),
(22, 6, NULL, 'abhishekmiraki@gmail.com', '9948839362', 'Abhi', 955, 'order_GSH8h1ScOD1HgA', 'INR', NULL, NULL, 2, '2021-01-22 02:17:49', '2021-01-22 02:17:49'),
(23, 6, NULL, 'abhishekmiraki@gmail.com', '9948839362', 'Abhi', 955, 'order_GSH9oD9K8FDyMS', 'INR', NULL, NULL, 2, '2021-01-22 02:18:52', '2021-01-22 02:18:52'),
(24, 5, 10000007, 'lavanya@miraki.com', '7780361239', 'lavanya', 450, 'order_GSHzpJtrfQYb1h', 'INR', 'pay_GSHzy5F8GGzbVl', '3ca8a5888b3ca2343af6cc3af2f469166afed2a38a8afc99613fcb29db557a72', 1, '2021-01-22 03:08:07', '2021-01-22 03:08:27'),
(25, 5, 10000010, 'lavanya@miraki.com', '7780361239', 'lavanya', 1775, 'order_GSJk1zzzuEjY6N', 'INR', 'pay_GSJkCcFDdmtiWE', '10627a954c79da7c5ae007c6d86a5e70c26a7801ba5c4051cb8c7d05c35c3e71', 1, '2021-01-22 04:50:33', '2021-01-22 04:50:56'),
(26, 7, NULL, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 925, 'order_GTQPRGOHKCr29n', 'INR', NULL, NULL, 2, '2021-01-25 00:00:54', '2021-01-25 00:00:54'),
(27, 7, 10000014, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 925, 'order_GTQQAmAXa6FG7g', 'INR', 'pay_GTQQvKRhEJObNv', 'aaa2d6f67132d4701cec00fb7334a835cf6d1d9430212c4aff08973e3634744b', 1, '2021-01-25 00:01:36', '2021-01-25 00:02:37'),
(28, 7, 10000016, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 450, 'order_GTSA99xMl5meQ9', 'INR', 'pay_GTSAMjRrptP4MU', 'a5d35587084f8da32c0a406617aa3a1349c1a2eaaf35fce593105b6777f75abe', 1, '2021-01-25 01:43:49', '2021-01-25 01:44:13'),
(29, 7, 10000017, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 450, 'order_GTSGkmDA6rfx29', 'INR', 'pay_GTSGwZiehJHqUe', 'aaf0693cfcc51a91053993e2d78b8e3c87551081fad8fec4fa8863fc69c819c2', 1, '2021-01-25 01:50:04', '2021-01-25 01:50:27'),
(30, 7, NULL, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 740, 'order_GTSTyWYLj8cIUb', 'INR', NULL, NULL, 2, '2021-01-25 02:02:35', '2021-01-25 02:02:35'),
(31, 7, 10000019, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 1865, 'order_GTUCj5YNI7oSxT', 'INR', 'pay_GTUCwqey3xVbcr', '0050a891ed507876bbe555dd825853019d3ba0e2fbdef4599af5e0744d234c6f', 1, '2021-01-25 03:43:39', '2021-01-25 03:44:05'),
(32, 7, 10000020, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 2025, 'order_GTUVuKQSLuWGjK', 'INR', 'pay_GTUW7a5Soh4Hza', '53e6a002956412652b2d9a60b6d657f1c442249a1c4e7705dd267489e413c422', 1, '2021-01-25 04:01:48', '2021-01-25 04:02:12'),
(33, 7, 10000021, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 2189, 'order_GTVLd4Po9BqsXW', 'INR', 'pay_GTVLoAwZL7ekyh', '649698a5e8a34c5b72ceeeccea316e117e456bbe2df2f8bdcccd0db1186ad082', 1, '2021-01-25 04:50:46', '2021-01-25 04:51:08'),
(34, 7, 10000022, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 1150, 'order_GTVX5F8IoAW4AE', 'INR', 'pay_GTVXFqzyLTsJ5S', '1e4698cf60df4a7a9bcb239c0ed7730f181fa462d8d822598b2d3dba1d0614e5', 1, '2021-01-25 05:01:37', '2021-01-25 05:02:00'),
(35, 7, 10000023, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 1150, 'order_GTVlJPEHboMXln', 'INR', 'pay_GTVlU6w8DY5qq9', '7ca1a633701f9d17c233972578ce113a4e7ca9de965ce848165d91858a5dfbf2', 1, '2021-01-25 05:15:05', '2021-01-25 05:15:27'),
(36, 7, 10000024, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 1150, 'order_GTX8L5j1y32pU5', 'INR', 'pay_GTX8VSpQyXAI4g', 'b60391dab0c0dac6d333554b531d1943581741cf4863a86f15477ecb7294bb86', 1, '2021-01-25 06:35:34', '2021-01-25 06:35:57'),
(37, 7, 10000025, 'anithamiraki@gmail.com', '9949587496', 'Anitha', 2200, 'order_GTXjyWFip1k7TC', 'INR', 'pay_GTXkASwfbYm7N6', 'a22e03d794a8524d59803b26c6960e6d2ea6f8321e960c6aa5f650a08ac36d57', 1, '2021-01-25 07:11:12', '2021-01-25 07:11:35'),
(38, 1, 10000026, 'mohammedavez85@gmail.com', '9550261360', 'Avez', 1150, 'order_GTYi54bxIWeLyZ', 'INR', 'pay_GTYiGOaNNFz9GC', '31eaa1b709c2dae7198573ec7069233a1f3cf5edd72a0f440b76a6571d64e27c', 1, '2021-01-25 08:08:06', '2021-01-25 08:08:29'),
(39, 1, 10000027, 'mohammedavez85@gmail.com', '9550261360', 'Avez', 1150, 'order_GTYopDxTCec575', 'INR', 'pay_GTYp1UxBfjHoiq', '1f65928e53cf7ff59b9071b9491286d9ca92f6222832b9a9d70b197829a833ba', 1, '2021-01-25 08:14:29', '2021-01-25 08:14:53'),
(40, 5, 10000029, 'lavanya@miraki.com', '7780361239', 'lavanya', 1330, 'order_GUc1777dEX6gL9', 'INR', 'pay_GUc1FTOyrRdhxK', '92bbffbbc98b25fbfd0b9e874ca280ab70018cca701c890035dd0511a15f5e00', 1, '2021-01-28 00:01:12', '2021-01-28 00:01:31'),
(41, 5, 10000030, 'lavanya@miraki.com', '7780361239', 'lavanya', 1150, 'order_GUdHUdgqTboRVf', 'INR', 'pay_GUdHcZRTsl3K2P', '203700cc8eb41c977fe8577925550444ff0d2c36a6492e25f2384d3c4e153b85', 1, '2021-01-28 01:15:24', '2021-01-28 01:15:45'),
(42, 12, NULL, 'lavanyamiraki1@yop.com', '7780361239', 'lavanya', 290, 'order_GV4QzKyR1tBgQy', 'INR', NULL, NULL, 2, '2021-01-29 03:49:07', '2021-01-29 03:49:07'),
(43, 12, NULL, 'lavanyamiraki1@yop.com', '7780361239', 'lavanya', 290, 'order_GV4RLUPOmPSrhP', 'INR', NULL, NULL, 2, '2021-01-29 03:49:27', '2021-01-29 03:49:27'),
(44, 12, NULL, 'lavanyamiraki1@yop.com', '7780361239', 'lavanya', 290, 'order_GV4XhE6k7EswhJ', 'INR', NULL, NULL, 2, '2021-01-29 03:55:28', '2021-01-29 03:55:28'),
(45, 12, NULL, 'lavanyamiraki1@yop.com', '7780361239', 'lavanya', 290, 'order_GV4XtaHr6ZMD7Z', 'INR', NULL, NULL, 2, '2021-01-29 03:55:39', '2021-01-29 03:55:39'),
(46, 12, NULL, 'lavanyamiraki1@yop.com', '7780361239', 'lavanya', 290, 'order_GV56U8kyYG8whL', 'INR', NULL, NULL, 2, '2021-01-29 04:28:24', '2021-01-29 04:28:24'),
(47, 12, NULL, 'lavanyamiraki1@yopmail.com', '7780361239', 'lavanya', 290, 'order_GV72CpW4wrqGt1', 'INR', NULL, NULL, 2, '2021-01-29 06:21:44', '2021-01-29 06:21:44');

-- --------------------------------------------------------

--
-- Table structure for table `payment_statuses`
--

CREATE TABLE `payment_statuses` (
  `id` int NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_text` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_text` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_text` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify_email` enum('y','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify_sms` enum('y','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_statuses`
--

INSERT INTO `payment_statuses` (`id`, `name`, `admin_text`, `seller_text`, `user_text`, `notify_email`, `notify_sms`, `created_at`, `updated_at`) VALUES
(1, 'Paid', 'Paid', 'Paid', 'Paid', 'n', 'n', '2020-12-24 08:45:21', NULL),
(2, 'Not Paid', 'Not Paid', 'Not Paid', 'Not Paid', 'n', 'n', '2020-12-24 08:45:21', NULL),
(3, 'Refund Request In Review', 'Refund Request in review', 'N/A', 'Refund request in Review', 'n', 'n', '2020-12-24 08:45:21', NULL),
(4, 'Refund Done', 'Refund Done', 'N/A', 'Refund Done', 'n', 'n', '2020-12-24 08:45:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_id` int DEFAULT NULL,
  `brand_id` int DEFAULT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hsn_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unique_id` int DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `images` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `thumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mrp` double DEFAULT NULL,
  `seller_discount` double NOT NULL DEFAULT '0',
  `sell_price` double DEFAULT NULL,
  `fabpik_seller_price` double DEFAULT NULL,
  `fabpik_seller_discount` double DEFAULT NULL,
  `min_ship_hours` int DEFAULT NULL COMMENT 'minimum hours to ship ready',
  `tax` double DEFAULT NULL,
  `size_chart_id` int DEFAULT NULL,
  `unit` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `length` double DEFAULT NULL,
  `breadth` double DEFAULT NULL,
  `height` double DEFAULT NULL,
  `return_avbl` tinyint(1) DEFAULT NULL,
  `refund_avl` tinyint(1) DEFAULT NULL,
  `cancel_avl` tinyint(1) DEFAULT NULL,
  `seller_featured` tinyint(1) DEFAULT '0',
  `fabpik_featured` tinyint(1) NOT NULL DEFAULT '0',
  `dress_material` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `washing_type_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iron_type_id` int DEFAULT NULL,
  `no_of_items` int DEFAULT NULL,
  `items_in_package` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_of_origin` int DEFAULT NULL,
  `meta_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `primary_category` int DEFAULT NULL,
  `primary_attribute` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `seller_id`, `brand_id`, `sku`, `hsn_code`, `unique_id`, `description`, `images`, `thumbnail`, `mrp`, `seller_discount`, `sell_price`, `fabpik_seller_price`, `fabpik_seller_discount`, `min_ship_hours`, `tax`, `size_chart_id`, `unit`, `weight`, `length`, `breadth`, `height`, `return_avbl`, `refund_avl`, `cancel_avl`, `seller_featured`, `fabpik_featured`, `dress_material`, `washing_type_id`, `iron_type_id`, `no_of_items`, `items_in_package`, `country_of_origin`, `meta_title`, `meta_keywords`, `meta_description`, `status`, `created_at`, `updated_at`, `deleted_at`, `primary_category`, `primary_attribute`) VALUES
(1, 'CARTOON FOREHEAD INFANT BAND', 'cartoon-forehead-infant-bandtpcb001-02-03', 13, 25, 'TPCB001_02_03', NULL, 10000000, 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', '[\"100007\\/products\\/25022021\\/TPCB001.jpg\",\"100007\\/products\\/25022021\\/TPCB001-(2).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(3).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(4).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(5).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(6).jpg\"]', '1', 399, 100, 299, 399, 0, 1, 5, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 1, 0, NULL, NULL, NULL, 1, 'hair band', 100, NULL, NULL, NULL, 1, '2021-02-26 06:03:14', '2021-02-26 06:03:27', NULL, 10, NULL),
(2, 'CARTOON FOREHEAD INFANT BAND', 'cartoon-forehead-infant-bandtpcb001-02-03', 13, 25, 'TPCB001_02_03', NULL, 10000001, 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', '[\"100007\\/products\\/25022021\\/TPCB001.jpg\",\"100007\\/products\\/25022021\\/TPCB001-(2).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(3).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(4).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(5).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(6).jpg\"]', '1', 399, 0, 399, 399, 0, 1, 5, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 1, NULL, NULL, NULL, 1, 'hair band', 100, NULL, NULL, NULL, 1, '2021-02-26 06:03:14', '2021-02-26 06:03:27', NULL, 10, NULL),
(3, 'CARTOON FOREHEAD INFANT BAND', 'cartoon-forehead-infant-bandtpcb001-02-03', 13, 25, 'TPCB001_02_03', NULL, 10000002, 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', '[\"100007\\/products\\/25022021\\/TPCB001.jpg\",\"100007\\/products\\/25022021\\/TPCB001-(2).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(3).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(4).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(5).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(6).jpg\"]', '1', 399, 0, 399, 299, 100, 1, 5, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 1, NULL, NULL, NULL, 1, 'hair band', 100, NULL, NULL, NULL, 1, '2021-02-26 06:19:18', '2021-03-16 06:47:17', NULL, 10, NULL),
(4, 'CARTOON FOREHEAD INFANT BAND', 'cartoon-forehead-infant-band', 13, 25, 'TPCB001_02_03', NULL, 10000003, 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', '[\"100007\\/products\\/25022021\\/TPCB001.jpg\",\"100007\\/products\\/25022021\\/TPCB001-(2).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(3).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(4).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(5).jpg\",\"100007\\/products\\/25022021\\/TPCB001-(6).jpg\"]', '1', 399, 0, 399, 399, 0, 1, 5, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 1, NULL, NULL, NULL, 1, 'hair band', 100, NULL, NULL, NULL, 0, '2021-02-26 06:19:18', '2021-03-16 05:49:37', NULL, 10, NULL),
(5, 'Anarkali Dress', 'anarkali-dressmishthi0011122323344', 13, 19, 'Mishthi0011122323344', NULL, 10000004, 'Twirl Love in our georgette anarkali \nin rani pink with beautiful floral embriodery in the front and halter neck at the back pepped up with orange pants .', '[\"100007\\/products\\/06022021\\/IMG_9796.jpg\"]', '1', 3000, 0, 3000, 3000, 0, 1, 0, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 0, 'N/A', '5', 8, 1, 'N/A', 100, NULL, NULL, NULL, 1, '2021-03-01 09:46:51', '2021-03-01 09:48:44', NULL, 1, NULL),
(6, 'Layered Gown', 'layered-gownmishthi0000000000005', 13, 19, 'Mishthi0000000000005', NULL, 10000005, 'Rich floral Embroidery on silk pepped with\n layers of net in orange and pink to make a perfect princess gown.', '[\"100007\\/products\\/06022021\\/IMG_9603.jpg\",\"100007\\/products\\/06022021\\/IMG_9597.jpg\"]', '1', 3000, 500, 2500, 3000, 0, 1, 0, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 0, 'N/A', '5', 8, 1, 'N/A', 100, NULL, NULL, NULL, 1, '2021-03-01 09:46:51', '2021-03-16 06:40:19', NULL, 1, NULL),
(7, 'Test Name', 'test-name1122edw', 18, 25, '1122edw', NULL, 10000006, 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish \nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,\n the balls move about inside of the maze track. To win the game, kids must get all three balls \ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills \nand problem solving skills and will aid in children\'s growth and development in a fun way!', '[\"100007\\/products\\/07022021\\/DT350-Main.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-1.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-2.jpg\"]', '1', 399, 0, 399, 399, 0, 1, 0, 7, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 0, 'N/A', '5', 8, 1, '80', 100, NULL, NULL, NULL, 1, '2021-03-01 10:49:26', '2021-03-01 10:49:26', NULL, 1, NULL),
(8, 'Test Name', 'test-name1122edwa', 18, 25, '1122edwa', NULL, 10000007, 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish \nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,\n the balls move about inside of the maze track. To win the game, kids must get all three balls \ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills \nand problem solving skills and will aid in children\'s growth and development in a fun way!', '[\"100007\\/products\\/07022021\\/DT350-Main.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-1.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-2.jpg\"]', '1', 399, 0, 399, 399, 0, 1, 0, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 0, NULL, NULL, NULL, 1, '80', 100, NULL, NULL, NULL, 1, '2021-03-01 10:56:35', '2021-03-01 10:56:35', NULL, 7, NULL),
(9, 'Test Name', 'test-nametest13', 21, 36, 'test13', NULL, 10000008, 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish \nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,\n the balls move about inside of the maze track. To win the game, kids must get all three balls \ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills \nand problem solving skills and will aid in children\'s growth and development in a fun way!', '[\"100007\\/products\\/07022021\\/DT350-Main.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-1.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-2.jpg\"]', '1', 399, 0, 399, 399, 0, 1, 0, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 0, NULL, NULL, NULL, 1, '80', 100, NULL, NULL, NULL, 1, '2021-03-01 11:01:07', '2021-03-01 11:01:07', NULL, 16, NULL),
(10, 'Desi Toys Labyrinth , Maze , Swirl , Bada Bhool Bhulaiya', 'desi-toys-labyrinth-maze-swirl-bada-bhool-bhulaiyadts1-dl-350-bhulaiyaa-1', 21, 30, 'DTs1-DL-350-Bhulaiyaa_1', NULL, 10000009, 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish \nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,\n the balls move about inside of the maze track. To win the game, kids must get all three balls \ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills \nand problem solving skills and will aid in children\'s growth and development in a fun way!', '[\"100007\\/products\\/07022021\\/DT350-Main.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-1.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-2.jpg\"]', '1', 399, 0, 399, 399, 0, 1, 0, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 0, NULL, NULL, NULL, 1, '8', 100, NULL, NULL, NULL, 1, '2021-03-01 11:09:13', '2021-03-01 11:09:13', NULL, 3, NULL),
(11, 'shoe', 'shoe5656', 13, 30, '5656', NULL, 10000010, 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish \nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,\n the balls move about inside of the maze track. To win the game, kids must get all three balls \ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills \nand problem solving skills and will aid in children\'s growth and development in a fun way!', '[\"100007\\/products\\/07022021\\/DT350-Main.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-1.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-2.jpg\"]', '1', 399, 0, 399, 299, 100, 1, 0, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 0, NULL, NULL, NULL, 1, '80', 100, NULL, NULL, NULL, 1, '2021-03-01 11:25:15', '2021-03-16 06:47:52', NULL, 2, NULL),
(14, 'Test Name', 'test-namepl-aq-350-bhulaiyaa-1', 17, 16, 'PL_AQ-350-Bhulaiyaa_1', NULL, 10000011, 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish \nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,\n the balls move about inside of the maze track. To win the game, kids must get all three balls \ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills \nand problem solving skills and will aid in children\'s growth and development in a fun way!', '[\"100007\\/products\\/07022021\\/DT350-Main.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-1.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-2.jpg\"]', '1', 399, 0, 399, 299, 100, 1, 0, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 0, NULL, NULL, NULL, 1, '80', 100, NULL, NULL, NULL, 1, '2021-03-01 11:42:55', '2021-03-16 06:47:52', NULL, 8, NULL),
(15, 'Test Name', 'test-nametest11', 17, 11, 'test11', NULL, 10000012, 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish \nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,\n the balls move about inside of the maze track. To win the game, kids must get all three balls \ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills \nand problem solving skills and will aid in children\'s growth and development in a fun way!', '[\"100007\\/products\\/07022021\\/DT350-Main.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-1.jpg\",\"100007\\/products\\/07022021\\/DT350-Other-2.jpg\"]', '1', 399, 200, 199, 299, 100, 1, 0, NULL, 'KG', 0.45, 15, 15, 10, 1, 1, 1, 0, 0, NULL, NULL, NULL, 1, '80', 100, NULL, NULL, NULL, 1, '2021-03-01 11:46:38', '2021-03-16 06:47:52', NULL, 5, NULL),
(16, 'jeans', 'jeans', 13, 1, 'Sample123', NULL, 10000013, '<p>Sample Descriprion</p>', NULL, NULL, 2000, 460, 1540, 1540, 460, 3, 18, 7, 'Grms', 0.12, 0.1, 0.1, 0.1, 1, 1, 1, 0, 0, NULL, NULL, NULL, 2, '1', 100, NULL, NULL, NULL, 0, '2021-03-05 06:11:47', '2021-03-19 09:49:46', NULL, 2, NULL),
(17, 'jeans123', 'jeans123', 18, 27, 'Sample12323', '123456789', 10000014, '<p><span style=\"color: rgb(51, 51, 51); font-size: 14px;\">Description of the product</span><br></p>', NULL, NULL, 100, 30, 70, 70, 30, 3, 18, NULL, 'Grms', 0.12, 0.1, 0.1, 0.1, 1, 1, 1, 0, 0, NULL, NULL, NULL, 2, '1', 100, NULL, NULL, NULL, 1, '2021-03-05 07:09:07', '2021-03-15 10:18:48', NULL, 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int NOT NULL,
  `product_id` int DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `subcategory_id` int DEFAULT NULL,
  `childcategory_id` int DEFAULT NULL,
  `is_active` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `product_id`, `category_id`, `subcategory_id`, `childcategory_id`, `is_active`) VALUES
(1, 1, 10, 25, 125, 1),
(2, 1, 10, 25, 127, 1),
(3, 1, 10, 26, 132, 1),
(4, 1, 10, 22, 166, 1),
(5, 2, 10, 25, 125, 1),
(6, 2, 10, 25, 127, 1),
(7, 2, 10, 26, 132, 1),
(8, 2, 10, 22, 166, 1),
(9, 3, 10, 25, 125, 1),
(10, 3, 10, 25, 127, 1),
(11, 3, 10, 26, 132, 1),
(12, 3, 10, 22, 166, 1),
(37, 5, 1, 2, 57, 1),
(38, 5, 1, 2, 56, 1),
(39, 5, 1, 2, 58, 1),
(40, 5, 1, 5, 66, 1),
(41, 5, 1, 5, 67, 1),
(42, 5, 1, 5, 68, 1),
(43, 5, 1, 3, 76, 1),
(44, 5, 1, 8, 86, 1),
(45, 5, 1, 8, 84, 1),
(46, 5, 1, 8, 88, 1),
(47, 6, 1, 2, 50, 1),
(48, 6, 1, 2, 56, 1),
(49, 6, 1, 2, 58, 1),
(50, 6, 1, 5, 66, 1),
(51, 6, 1, 5, 67, 1),
(52, 6, 1, 5, 68, 1),
(53, 6, 1, 3, 76, 1),
(54, 6, 1, 8, 86, 1),
(55, 6, 1, 8, 84, 1),
(56, 6, 1, 8, 88, 1),
(57, 7, 7, 44, 291, 1),
(58, 7, 7, 46, 299, 1),
(59, 7, 7, 47, 302, 1),
(60, 8, 7, 44, 291, 1),
(61, 8, 7, 46, 299, 1),
(62, 8, 7, 47, 302, 1),
(63, 9, 16, 57, 376, 1),
(64, 9, 16, 58, 382, 1),
(65, 9, 16, 59, 385, 1),
(66, 10, 3, 4, 16, 1),
(67, 10, 3, 10, 104, 1),
(68, 11, 2, 28, 192, 1),
(69, 11, 2, 27, 183, 1),
(76, 14, 8, 33, 226, 1),
(77, 14, 8, 35, 235, 1),
(78, 14, 8, 37, 246, 1),
(79, 15, 5, 49, 318, 1),
(80, 15, 5, 50, 329, 1),
(81, 15, 5, 51, 332, 1),
(91, 17, 3, 9, 100, 1),
(100, 4, 10, 25, 125, 1),
(101, 4, 10, 25, 127, 1),
(102, 4, 10, 26, 132, 1),
(103, 4, 10, 22, 166, 1),
(104, 16, 2, 27, 184, 1),
(105, 16, 2, 27, 185, 1),
(106, 16, 2, 32, 216, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

CREATE TABLE `product_variants` (
  `id` int NOT NULL,
  `product_id` int DEFAULT NULL,
  `unique_id` int DEFAULT NULL,
  `hsn_code` int DEFAULT NULL,
  `isbn_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `stock` int NOT NULL DEFAULT '0',
  `mrp` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `fabpik_seller_price` double DEFAULT NULL,
  `fabpik_seller_discount` double DEFAULT NULL,
  `min_order_qty` int NOT NULL DEFAULT '1',
  `min_ship_hours` int DEFAULT NULL COMMENT 'minimum hours to ship ready',
  `shipping_weight` double DEFAULT NULL,
  `shipping_length` double DEFAULT NULL,
  `shipping_breadth` double DEFAULT NULL,
  `shipping_height` double DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_variants`
--

INSERT INTO `product_variants` (`id`, `product_id`, `unique_id`, `hsn_code`, `isbn_code`, `name`, `slug`, `sku`, `description`, `stock`, `mrp`, `discount`, `price`, `fabpik_seller_price`, `fabpik_seller_discount`, `min_order_qty`, `min_ship_hours`, `shipping_weight`, `shipping_length`, `shipping_breadth`, `shipping_height`, `is_default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 10000000, 6505, NULL, 'CARTOON FOREHEAD INFANT BAND', 'cartoon-forehead-infant-bandtpcb001', 'TPCB001', 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', 1, 399, 0, 399, 399, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-02-26 06:03:14', '2021-02-26 06:03:27', NULL),
(2, 1, 10000001, 6505, NULL, 'CARTOON FOREHEAD INFANT BAND 1', 'cartoon-forehead-infant-bandtpcb002', 'TPCB002', 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', 1, 399, 0, 399, 399, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-02-26 06:03:14', '2021-02-26 06:03:27', NULL),
(3, 1, 10000002, 6505, NULL, 'CARTOON FOREHEAD INFANT BAND 3', 'cartoon-forehead-infant-bandtpcb003', 'TPCB003', 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', 1, 399, 0, 399, 399, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-02-26 06:03:14', '2021-02-26 06:03:27', NULL),
(4, 2, 10000003, 6505, NULL, 'CARTOON FOREHEAD INFANT BAND', 'cartoon-forehead-infant-bandtpcb001', 'TPCB001', 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', 1, 399, 0, 399, 399, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-02-26 06:03:14', '2021-02-26 06:03:27', '2021-02-26 06:03:27'),
(5, 3, 10000004, 6505, NULL, 'CARTOON FOREHEAD INFANT BAND', 'cartoon-forehead-infant-bandtpcb001', 'TPCB001', 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', 1, 399, 0, 399, 299, 100, 1, 1, 0.45, 30, 20, 4, 0, '2021-02-26 06:19:18', '2021-03-16 06:47:17', NULL),
(6, 3, 10000005, 6505, NULL, 'CARTOON FOREHEAD INFANT BAND', 'cartoon-forehead-infant-bandtpcb002', 'TPCB002', 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', 1, 399, 0, 399, 299, 100, 1, 1, 0.45, 30, 20, 4, 0, '2021-02-26 06:19:18', '2021-03-16 06:47:17', NULL),
(7, 3, 10000006, 6505, NULL, 'CARTOON FOREHEAD INFANT BAND', 'cartoon-forehead-infant-bandtpcb003', 'TPCB003', 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', 1, 399, 0, 399, 299, 100, 1, 1, 0.45, 30, 20, 4, 0, '2021-02-26 06:19:18', '2021-03-16 06:47:17', NULL),
(8, 4, 10000007, 6505, NULL, 'CARTOON FOREHEAD INFANT BAND', 'cartoon-forehead-infant-bandtpcb001', 'TPCB001', 'Baby band style cap empty top baby summer newborn forehead eye protecting hair band. Washable.  Material- cotton', 1, 399, 100, 299, 299, 100, 1, 1, 0.45, 30, 20, 4, 0, '2021-02-26 06:19:18', '2021-03-17 11:08:07', NULL),
(9, 5, 10000008, 979089, NULL, 'Anarkali Dress', 'anarkali-dressmishthi0000000000001', 'Mishthi0000000000001', 'Twirl Love in our georgette anarkali \nin rani pink with beautiful floral embriodery in the front and halter neck at the back pepped up with orange pants .', 1, 3000, 0, 3000, 3000, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-03-01 09:46:51', '2021-03-01 09:48:44', NULL),
(10, 5, 10000009, 979090, NULL, 'Anarkali Dress', 'anarkali-dressmishthi0000000000002', 'Mishthi0000000000002', 'Twirl Love in our georgette anarkali \nin rani pink with beautiful floral embriodery in the front and halter neck at the back pepped up with orange pants .', 1, 3200, 0, 3200, 3200, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-03-01 09:46:51', '2021-03-01 09:48:44', NULL),
(11, 5, 10000010, 979091, NULL, 'Anarkali Dress', 'anarkali-dressmishthi0000000000003', 'Mishthi0000000000003', 'Twirl Love in our georgette anarkali \nin rani pink with beautiful floral embriodery in the front and halter neck at the back pepped up with orange pants .', 1, 3400, 0, 3400, 3400, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-03-01 09:46:51', '2021-03-01 09:48:44', NULL),
(12, 5, 10000011, 979092, NULL, 'Anarkali Dress', 'anarkali-dressmishthi0000000000004', 'Mishthi0000000000004', 'Twirl Love in our georgette anarkali \nin rani pink with beautiful floral embriodery in the front and halter neck at the back pepped up with orange pants .', 1, 3500, 0, 3500, 3500, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-03-01 09:46:51', '2021-03-01 09:48:44', NULL),
(13, 6, 10000012, 979094, NULL, 'Layered Gown', 'layered-gownmishthi0000000000006', 'Mishthi0000000000006', 'Rich floral Embroidery on silk pepped with\n layers of net in orange and pink to make a perfect princess gown.', 1, 3000, 500, 2500, 3000, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-03-01 09:46:51', '2021-03-16 06:40:19', NULL),
(14, 6, 10000013, 979095, NULL, 'Layered Gown', 'layered-gownmishthi0000000000007', 'Mishthi0000000000007', 'Rich floral Embroidery on silk pepped with\n layers of net in orange and pink to make a perfect princess gown.', 1, 3200, 500, 2700, 3200, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-03-01 09:46:51', '2021-03-16 06:40:19', NULL),
(15, 6, 10000014, 979096, NULL, 'Layered Gown', 'layered-gownmishthi0000000000008', 'Mishthi0000000000008', 'Rich floral Embroidery on silk pepped with\n layers of net in orange and pink to make a perfect princess gown.', 1, 3400, 500, 2900, 3400, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-03-01 09:46:51', '2021-03-16 06:40:19', NULL),
(16, 6, 10000015, 979097, NULL, 'Layered Gown', 'layered-gownmishthi0000000000009', 'Mishthi0000000000009', 'Rich floral Embroidery on silk pepped with\n layers of net in orange and pink to make a perfect princess gown.', 1, 3500, 500, 3000, 3500, 0, 1, 1, 0.45, 30, 20, 4, 0, '2021-03-01 09:46:51', '2021-03-16 06:40:19', NULL),
(17, 7, 10000016, 3211, NULL, 'Test Name', 'test-nameer33', 'er33', 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves<br />\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish <br />\nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not<br />\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,<br />\n the balls move about inside of the maze track. To win the game, kids must get all three balls <br />\ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills <br />\nand problem solving skills and will aid in children\'s growth and development in a fun way!', 15, 399, 0, 399, 399, 0, 1, 1, 0.45, 15, 15, 10, 0, '2021-03-01 10:49:27', '2021-03-01 10:49:27', NULL),
(18, 8, 10000017, 13211, NULL, 'Test Name', 'test-nameer33e', 'er33e', 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves<br />\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish <br />\nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not<br />\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,<br />\n the balls move about inside of the maze track. To win the game, kids must get all three balls <br />\ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills <br />\nand problem solving skills and will aid in children\'s growth and development in a fun way!', 15, 399, 0, 399, 399, 0, 1, 1, 0.45, 15, 15, 10, 0, '2021-03-01 10:56:35', '2021-03-01 10:56:35', NULL),
(19, 9, 10000018, 3114, NULL, 'Test Name', 'test-nametest14', 'test14', 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves<br />\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish <br />\nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not<br />\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,<br />\n the balls move about inside of the maze track. To win the game, kids must get all three balls <br />\ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills <br />\nand problem solving skills and will aid in children\'s growth and development in a fun way!', 15, 399, 0, 399, 399, 0, 1, 1, 0.45, 15, 15, 10, 0, '2021-03-01 11:01:07', '2021-03-01 11:01:07', NULL),
(20, 10, 10000019, 1919, NULL, 'Desi Toys Labyrinth , Maze , Swirl , Bada Bhool Bhulaiya', 'desi-toys-labyrinth-maze-swirl-bada-bhool-bhulaiyadts1-dl-350-bhulaiyaa', 'DTs1-DL-350-Bhulaiyaa', 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves<br />\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish <br />\nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not<br />\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,<br />\n the balls move about inside of the maze track. To win the game, kids must get all three balls <br />\ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills <br />\nand problem solving skills and will aid in children\'s growth and development in a fun way!', 15, 399, 0, 399, 399, 0, 1, 1, 0.45, 15, 15, 10, 0, '2021-03-01 11:09:13', '2021-03-01 11:09:13', NULL),
(21, 11, 10000020, 123456, NULL, 'shoe', 'shoe56766', '56766', 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves<br />\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish <br />\nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not<br />\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,<br />\n the balls move about inside of the maze track. To win the game, kids must get all three balls <br />\ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills <br />\nand problem solving skills and will aid in children\'s growth and development in a fun way!', 15, 399, 0, 399, 299, 100, 1, 1, 0.45, 30, 20, 4, 0, '2021-03-01 11:25:15', '2021-03-16 06:47:52', NULL),
(22, 14, 10000021, 154, NULL, 'Test Name', 'test-namepl-aq-350-bhulaiyaa', 'PL_AQ-350-Bhulaiyaa', 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves<br />\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish <br />\nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not<br />\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,<br />\n the balls move about inside of the maze track. To win the game, kids must get all three balls <br />\ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills <br />\nand problem solving skills and will aid in children\'s growth and development in a fun way!', 15, 399, 0, 399, 299, 100, 1, 1, 0.45, 15, 15, 10, 0, '2021-03-01 11:42:55', '2021-03-16 06:47:52', NULL),
(23, 15, 10000022, 6543, NULL, 'Test Name', 'test-nametest21', 'test21', 'Handcrafted in Shesham wood, this is a fun, challenging game enjoyed by everyone.Deep grooves<br />\n have been carved into the surface of the 6\"diameter disc and the entire game has a smooth finish <br />\nwith no sharp edges. Three small steel balls have been placed inside of the grooves and will not<br />\n come out even when tipped upsided down. When the toy is rattled, jostled, tipped and shaken,<br />\n the balls move about inside of the maze track. To win the game, kids must get all three balls <br />\ninto the middle of the puzzle. Game play requires hand eye coordination, fine motor skills <br />\nand problem solving skills and will aid in children\'s growth and development in a fun way!', 15, 399, 200, 199, 299, 100, 1, 1, 0.45, 15, 15, 10, 0, '2021-03-01 11:46:38', '2021-03-16 06:47:52', NULL),
(24, 16, 10000023, NULL, NULL, 'jeans', 'jeans', 'Sample1234', NULL, 2, 200, 100, 100, 200, 0, 2, 3, 0.12, 30, 20, 4, 0, '2021-03-05 06:44:20', '2021-03-05 06:44:38', '2021-03-05 06:44:38'),
(25, 16, 10000024, NULL, NULL, 'jeans', 'jeans', 'Sample123', NULL, 2, 200, 46, 154, 154, 46, 1, 3, 0.12, 30, 20, 4, 0, '2021-03-05 06:54:14', '2021-03-16 05:29:14', '2021-03-16 05:29:14'),
(26, 17, 10000025, NULL, NULL, 'jeans123', 'jeans123', 'Sample12323', NULL, 2, 100, 40, 60, 60, 40, 1, 3, 0.12, 0.1, 0.1, 0.1, 0, '2021-03-05 07:13:27', '2021-03-15 10:24:30', NULL),
(27, 16, 10000026, NULL, NULL, 'jeans', 'jeans', 'Sample1232323', NULL, 2, 2000, 460, 1540, 1540, 460, 1, 3, 0.12, 30, 20, 4, 0, '2021-03-08 06:01:52', '2021-03-16 05:29:14', '2021-03-16 05:29:14'),
(28, 11, 10000027, NULL, NULL, 'shoe', 'shoe', 'Sample123WEE', NULL, 1, 399, 39.900000000000006, 359.1, 299, 100, 1, 1, 0.45, 30, 20, 4, 0, '2021-03-15 10:25:29', '2021-03-17 07:29:49', NULL),
(29, 4, 10000028, NULL, NULL, 'CARTOON FOREHEAD INFANT BAND', 'cartoon-forehead-infant-band', 'Sample123213', NULL, 1, 399, 10, 389, 199, 200, 1, 1, 0.45, 30, 20, 4, 0, '2021-03-16 05:51:01', '2021-03-17 11:08:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_variant_options`
--

CREATE TABLE `product_variant_options` (
  `id` int NOT NULL,
  `product_variant_id` int DEFAULT NULL,
  `attribute_id` int DEFAULT NULL,
  `attribute_option_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_variant_options`
--

INSERT INTO `product_variant_options` (`id`, `product_variant_id`, `attribute_id`, `attribute_option_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 50, '2021-02-26 06:03:14', NULL, NULL),
(2, 2, 1, 41, '2021-02-26 06:03:14', NULL, NULL),
(3, 3, 1, 44, '2021-02-26 06:03:14', NULL, NULL),
(4, 4, 1, 50, '2021-02-26 06:03:14', NULL, NULL),
(5, 5, 1, 50, '2021-02-26 06:19:18', NULL, NULL),
(6, 6, 1, 41, '2021-02-26 06:19:18', NULL, NULL),
(7, 7, 1, 44, '2021-02-26 06:19:18', NULL, NULL),
(8, 8, 1, 50, '2021-02-26 06:19:18', NULL, NULL),
(9, 9, 1, 50, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(10, 9, 2, 119, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(11, 10, 1, 50, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(12, 10, 2, 84, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(13, 11, 1, 50, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(14, 11, 2, 88, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(15, 12, 1, 50, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(16, 12, 2, 91, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(17, 13, 1, 50, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(18, 13, 2, 119, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(19, 14, 1, 50, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(20, 14, 2, 84, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(21, 15, 1, 50, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(22, 15, 2, 88, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(23, 16, 1, 50, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(24, 16, 2, 91, '2021-03-01 09:46:51', '2021-03-01 09:48:44', '2021-03-01 09:48:44'),
(25, 9, 1, 50, '2021-03-01 09:48:44', NULL, NULL),
(26, 9, 2, 119, '2021-03-01 09:48:44', NULL, NULL),
(27, 10, 1, 50, '2021-03-01 09:48:44', NULL, NULL),
(28, 10, 2, 84, '2021-03-01 09:48:44', NULL, NULL),
(29, 11, 1, 50, '2021-03-01 09:48:44', NULL, NULL),
(30, 11, 2, 88, '2021-03-01 09:48:44', NULL, NULL),
(31, 12, 1, 50, '2021-03-01 09:48:44', NULL, NULL),
(32, 12, 2, 91, '2021-03-01 09:48:44', NULL, NULL),
(33, 13, 1, 50, '2021-03-01 09:48:44', NULL, NULL),
(34, 13, 2, 119, '2021-03-01 09:48:44', NULL, NULL),
(35, 14, 1, 50, '2021-03-01 09:48:44', NULL, NULL),
(36, 14, 2, 84, '2021-03-01 09:48:44', NULL, NULL),
(37, 15, 1, 50, '2021-03-01 09:48:44', NULL, NULL),
(38, 15, 2, 88, '2021-03-01 09:48:44', NULL, NULL),
(39, 16, 1, 50, '2021-03-01 09:48:44', NULL, NULL),
(40, 16, 2, 91, '2021-03-01 09:48:44', NULL, NULL),
(41, 17, 1, 42, '2021-03-01 10:49:27', NULL, NULL),
(42, 17, 2, 60, '2021-03-01 10:49:27', NULL, NULL),
(43, 18, 5, 27, '2021-03-01 10:56:35', NULL, NULL),
(44, 19, 6, 20, '2021-03-01 11:01:07', NULL, NULL),
(45, 20, 1, 41, '2021-03-01 11:09:13', NULL, NULL),
(46, 21, 1, 0, '2021-03-01 11:25:15', NULL, NULL),
(47, 22, 5, 27, '2021-03-01 11:42:55', NULL, NULL),
(48, 23, 6, 0, '2021-03-01 11:46:38', NULL, NULL),
(49, 24, 1, 47, '2021-03-05 06:44:20', '2021-03-05 06:44:20', NULL),
(50, 24, 3, NULL, '2021-03-05 06:44:20', '2021-03-05 06:44:20', NULL),
(51, 25, 1, 47, '2021-03-05 06:54:14', '2021-03-08 06:01:19', '2021-03-08 06:01:19'),
(52, 25, 3, 176, '2021-03-05 06:54:14', '2021-03-08 06:01:19', '2021-03-08 06:01:19'),
(53, 26, 1, 47, '2021-03-05 07:13:27', '2021-03-05 07:20:53', '2021-03-05 07:20:53'),
(54, 26, 17, 108, '2021-03-05 07:13:27', '2021-03-05 07:20:53', '2021-03-05 07:20:53'),
(55, 26, 1, 47, '2021-03-05 07:20:53', '2021-03-05 07:21:02', '2021-03-05 07:21:02'),
(56, 26, 17, 108, '2021-03-05 07:20:53', '2021-03-05 07:21:02', '2021-03-05 07:21:02'),
(57, 26, 1, 47, '2021-03-05 07:21:02', '2021-03-05 07:21:09', '2021-03-05 07:21:09'),
(58, 26, 17, 108, '2021-03-05 07:21:02', '2021-03-05 07:21:09', '2021-03-05 07:21:09'),
(59, 26, 1, 47, '2021-03-05 07:21:09', '2021-03-15 10:24:30', '2021-03-15 10:24:30'),
(60, 26, 17, 108, '2021-03-05 07:21:09', '2021-03-15 10:24:30', '2021-03-15 10:24:30'),
(61, 25, 1, 47, '2021-03-08 06:01:19', '2021-03-08 06:01:19', NULL),
(62, 25, 3, 176, '2021-03-08 06:01:19', '2021-03-08 06:01:19', NULL),
(63, 27, 1, 41, '2021-03-08 06:01:52', '2021-03-08 06:02:00', '2021-03-08 06:02:00'),
(64, 27, 3, 14, '2021-03-08 06:01:52', '2021-03-08 06:02:00', '2021-03-08 06:02:00'),
(65, 27, 1, 41, '2021-03-08 06:02:00', '2021-03-08 06:02:00', NULL),
(66, 27, 3, 14, '2021-03-08 06:02:00', '2021-03-08 06:02:00', NULL),
(67, 26, 1, 47, '2021-03-15 10:24:30', '2021-03-15 10:24:30', NULL),
(68, 26, 17, 108, '2021-03-15 10:24:30', '2021-03-15 10:24:30', NULL),
(69, 28, 1, 50, '2021-03-15 10:25:29', '2021-03-15 10:25:29', NULL),
(70, 28, 3, 15, '2021-03-15 10:25:29', '2021-03-15 10:25:29', NULL),
(71, 29, 1, 42, '2021-03-16 05:51:01', '2021-03-16 05:51:01', NULL),
(72, 29, 17, 108, '2021-03-16 05:51:01', '2021-03-16 05:51:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int NOT NULL,
  `customer_id` int NOT NULL,
  `child_order_id` int DEFAULT NULL,
  `product_id` int NOT NULL,
  `product_variant_id` int NOT NULL,
  `review_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` tinyint(1) DEFAULT NULL,
  `review_comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'web', '2020-11-06 23:24:01', '2020-11-06 23:24:01'),
(2, 'Admin', 'web', '2020-11-06 23:24:09', '2020-11-06 23:24:09'),
(3, 'Seller', 'web', '2020-11-06 23:24:14', '2020-11-06 23:24:14'),
(4, 'Customer', 'web', '2020-11-06 23:24:21', '2020-11-06 23:24:21');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint UNSIGNED NOT NULL,
  `role_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE `sellers` (
  `id` int NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unique_id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cod_enable` enum('y','n') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'n',
  `commission` double NOT NULL DEFAULT '14',
  `commission_type` enum('v','f') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'F=Fixed, V=Variable',
  `commission_accept` tinyint NOT NULL DEFAULT '0' COMMENT '1=accept, 0=NotAccept',
  `shipping_model` enum('f','s') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'f=Fabpik,s=Own Seller',
  `brand_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` int DEFAULT NULL,
  `chat_enable` tinyint NOT NULL DEFAULT '1',
  `categories` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_reg_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `legal_entity` enum('r','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'r' COMMENT 'r=Registered , n=NOT Registered',
  `company_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incorporation_certificate` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_registered` tinyint(1) DEFAULT NULL,
  `gst_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_certificate` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_phone_no` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_state` int DEFAULT NULL,
  `company_pincode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `swift_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelled_cheque_file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poc_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poc_designation` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poc_email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poc_mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poc_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poc_doc` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approval_status` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `inactive_reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `approved_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`id`, `name`, `store_name`, `url_slug`, `seller_code`, `unique_id`, `email`, `mobile`, `phone`, `cod_enable`, `commission`, `commission_type`, `commission_accept`, `shipping_model`, `brand_name`, `brand_id`, `chat_enable`, `categories`, `company_name`, `company_reg_no`, `legal_entity`, `company_type`, `incorporation_certificate`, `gst_registered`, `gst_no`, `gst_certificate`, `office_phone_no`, `company_address`, `company_state`, `company_pincode`, `bank_account_name`, `bank_account_no`, `ifsc_code`, `bank_name`, `branch_name`, `swift_code`, `cancelled_cheque_file`, `poc_name`, `poc_designation`, `poc_email`, `poc_mobile`, `poc_phone`, `poc_doc`, `logo`, `approval_status`, `status`, `inactive_reason`, `created_at`, `updated_at`, `deleted_at`, `approved_on`) VALUES
(4, 'lavanya', 'Sweet Spot.\r\n', NULL, '100001', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, NULL, 0, 'f', NULL, NULL, 1, '3', 'miraki', NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, '2021-01-21 01:18:53', '2021-01-21 02:36:43', NULL, NULL),
(5, 'Praveen', 'One of a Kind Studio.', 'MirakiFabpik', '100002', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, NULL, 0, 's', NULL, NULL, 1, '1', 'Mirakitechnologies', 'A55555BB4444CC66666', 'r', 'Pvt Ltd', '1611234561.pdf', 1, '22AAAAA4444A', '1611234561.pdf', '9866336421', 'HYD', 2, '515511', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2021-01-21 07:36:42', '2021-01-21 07:39:51', NULL, '2021-01-20 19:39:51'),
(6, 'Aakash', 'Decorama Boutique.', NULL, '100003', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, '2021-01-21 07:48:54', '2021-01-21 07:48:54', NULL, NULL),
(7, 'DeepakRegFullName', NULL, NULL, '10012', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, NULL, 0, NULL, NULL, NULL, 1, NULL, 'Deepak', NULL, 'n', NULL, NULL, 0, NULL, NULL, '9439430493', 'Deepak Address company', 2, '533200', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Deepak POC Name', 'Deepak POC Designation', 'DeepakPOCEmail@gmail.com', NULL, '7428731210', '600bf3e6f120c.jpeg', NULL, 0, 1, NULL, '2021-01-22 08:48:00', '2021-01-23 05:51:00', NULL, NULL),
(8, 'mirakitest', NULL, NULL, '100004', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2021-01-24 23:31:27', '2021-01-24 23:42:55', NULL, '2021-01-24 23:42:55'),
(9, 'Test', 'store Name', 'shopurl', '10013', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, NULL, 0, 'f', 'Brand Name', NULL, 0, '2', 'Test Company', NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2021-01-27 00:51:32', '2021-02-16 11:09:06', NULL, '2021-02-15 23:09:06'),
(10, 'Verified User', NULL, NULL, '10014', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, 'v', 0, NULL, NULL, NULL, 1, NULL, 'Verified company', NULL, 'n', NULL, NULL, 0, NULL, NULL, '8989090909', 'Hitech city', 36, '500035', 'Verified account name', '9898989898989898', 'INDB0000007', 'INDUSIND BANK', 'Hitech city', NULL, '60111c2e6401c.jpeg', 'Verified POC', 'Verified POC Designation', 'verifiedpocemail@email.com', NULL, '8989989989', '60111cbea8171.jpeg', NULL, 1, 1, 'sample', '2021-01-27 01:56:56', '2021-03-03 13:57:46', NULL, '2021-01-28 01:14:27'),
(11, 'charan', NULL, NULL, '100005', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, '2021-01-27 08:28:48', '2021-01-27 08:28:48', NULL, NULL),
(13, 'hari Krishna', 'naveenn', 'naveen12url', '100007', NULL, 'harik@mirakitech.com', '8328566312', NULL, 'y', 16, 'v', 1, 'f', 'ahkrishna', 44, 0, '10', 'Coding it my way', '', 'r', 'Limited Liability Partnership', NULL, NULL, NULL, '1613027585.pdf', NULL, 'D.No 19, panjagutta', 35, '500031', 'naveenmiraki', '50200046435282', 'HDFC0000093', 'SBI', 'SBI', 'HDFC0000093', '1612793584.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2021-01-29 01:32:04', '2021-03-19 13:37:39', NULL, '2021-03-02 23:51:51'),
(14, 'harikrishna', NULL, NULL, '100008', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, '2021-01-29 01:54:46', '2021-01-29 01:54:46', NULL, NULL),
(15, 'Arina Sethi', NULL, NULL, '100009', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, '2021-01-29 01:58:49', '2021-01-29 01:58:49', NULL, NULL),
(16, 'Meenaakshi overseas', NULL, NULL, '100010', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, 'v', 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2021-01-29 02:37:20', '2021-02-08 12:58:55', NULL, '2021-01-29 03:21:10'),
(17, 'Harsh Mohta', NULL, NULL, '100011', NULL, 'a.harikrishna108@gmail.com', '9290573910', NULL, 'n', 14, NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2021-01-29 03:18:03', '2021-01-29 03:21:30', NULL, '2021-01-29 03:21:30'),
(18, 'hari', 'Store Name', 'YOURSHOPURL', '10015', NULL, 'ahk9290@gmail.com', '9290574912', NULL, 'n', 14, 'v', 1, 's', 'ahkrishna', 50, 0, '2', 'Company Name', NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2021-02-09 10:43:28', '2021-03-05 07:07:16', NULL, '2021-02-25 20:49:29'),
(21, 'name', NULL, NULL, '10016', NULL, 'temp123456@yopmail.com', '9290573910', NULL, 'n', 14, NULL, 0, NULL, NULL, NULL, 1, NULL, 'shopname', NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, 'shopaddress', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, '2021-02-17 07:37:43', '2021-02-17 07:37:43', NULL, NULL),
(22, 'Krishna', NULL, NULL, '10017', NULL, 'ljiasiqs@maxresistance.com', '9293275832', NULL, 'n', 14, NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, '2021-03-23 10:36:23', '2021-03-23 10:36:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `seller_setting_change_logs`
--

CREATE TABLE `seller_setting_change_logs` (
  `id` int NOT NULL,
  `settings_column_name` varchar(50) DEFAULT NULL,
  `old_value` varchar(50) DEFAULT NULL,
  `new_value` varchar(50) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `approved_by` int DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `seller_setting_change_logs`
--

INSERT INTO `seller_setting_change_logs` (`id`, `settings_column_name`, `old_value`, `new_value`, `created_by`, `approved_by`, `approved_at`, `is_deleted`) VALUES
(1, 'cod_enable', 'n', 'y', 13, 1, '2021-02-24 11:30:00', '2021-02-24 11:30:00'),
(2, 'cod_enable', 'y', 'n', 13, 1, '2021-03-01 08:30:00', NULL),
(3, 'cod_enable', 'n', 'y', 13, 1, '2021-03-02 11:30:00', '2021-03-02 11:30:00'),
(5, 'shipping_model', 'f', 's', 13, NULL, NULL, '2021-03-02 10:58:01'),
(6, 'shipping_model', 'f', 's', 13, 1, '2021-03-02 11:30:00', NULL),
(7, 'shipping_model', 'f', 's', 13, 1, '2021-03-02 11:30:00', '2021-03-02 11:30:00'),
(8, 'shipping_model', 's', 'f', 13, NULL, NULL, '2021-03-18 10:52:32'),
(9, 'shipping_model', 's', 'f', 13, NULL, NULL, NULL),
(10, 'cod_enable', 'y', 'n', 13, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `seller_warehouses`
--

CREATE TABLE `seller_warehouses` (
  `id` int NOT NULL,
  `seller_id` int DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `shiprocket_pickup_nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_id` int DEFAULT NULL,
  `pincode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seller_warehouses`
--

INSERT INTO `seller_warehouses` (`id`, `seller_id`, `is_default`, `shiprocket_pickup_nickname`, `name`, `address`, `landmark`, `city`, `state_id`, `pincode`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 1, 'hyd', 'gokulplots', 'hyderabad', 'gokulplots', 'HYD', 36, '500085', 1, '2021-01-21 02:37:40', '2021-01-21 05:24:03', NULL),
(2, 3, 1, 'hyd', 'English Man', 'Ammerpet', 'Sathyam Theatre', 'HYD', 2, '515511', 1, '2021-01-21 03:09:12', '2021-01-21 05:20:27', NULL),
(3, 18, 1, 'hyd', 'Twills', 'HYD', 'Ammerpet', 'HYD', 2, '515511', 1, '2021-01-21 07:39:41', '2021-01-21 07:39:41', NULL),
(4, 8, 1, 'hyd', 'Hyderaba', 'Hyderabad', 'hyderabad', 'HYD', 36, '500085', 1, '2021-01-24 23:38:50', '2021-01-24 23:38:50', NULL),
(5, 13, 0, 'hyd', 'Hyderabad', 'hyderabad', 'hyderabad', 'HYD', 36, '500085', 1, '2021-01-29 03:42:05', '2021-03-22 05:58:14', NULL),
(6, 13, 0, NULL, 'hyderabad123', 'hyderabad', 'Hitech City, etc.', 'HYD', 36, '500085', 1, '2021-03-22 05:58:14', '2021-03-22 05:59:01', NULL),
(7, 13, 1, NULL, 'Bombay-Andheri Warehouse', 'Bombay', 'Hitech City', 'hyd', 36, '500085', 1, '2021-03-22 05:59:01', '2021-03-22 05:59:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shiping_charges`
--

CREATE TABLE `shiping_charges` (
  `id` int NOT NULL,
  `min_amount` int NOT NULL DEFAULT '0',
  `max_amount` int NOT NULL DEFAULT '0',
  `charge_amount` int NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shiping_charges`
--

INSERT INTO `shiping_charges` (`id`, `min_amount`, `max_amount`, `charge_amount`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 10, 55, 1000, 1, '2020-11-13 02:50:40', '2020-12-09 12:07:05', NULL),
(2, 50, 100, 49, 1, '2020-11-13 03:10:14', '2020-12-09 11:59:03', NULL),
(3, 0, 50, 1000, 1, '2020-12-09 12:00:36', '2020-12-09 12:00:41', '2020-12-09 12:00:41');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_statuses`
--

CREATE TABLE `shipping_statuses` (
  `id` int NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_text` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_text` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_text` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify_email` enum('y','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify_sms` enum('y','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_statuses`
--

INSERT INTO `shipping_statuses` (`id`, `name`, `admin_text`, `seller_text`, `user_text`, `notify_email`, `notify_sms`, `created_at`, `updated_at`) VALUES
(1, 'Pending', 'Pending', 'Pending', 'Under Process', 'n', 'n', '2020-12-24 08:45:43', NULL),
(2, 'Ready For Pickup', 'Ready for Pickup', 'Ready for Pickup', 'Package Ready', 'n', 'n', '2020-12-24 08:45:43', NULL),
(3, 'Pickup Scheduled', 'Pickup Scheduled', 'Pickup Scheduled', 'Package Ready', 'n', 'n', '2020-12-24 08:45:43', NULL),
(4, 'Shipped', 'Shipped', 'Shipped', 'Shipped', 'y', 'y', '2020-12-24 08:45:43', NULL),
(5, 'Delivered', 'Delivered', 'Delivered', 'Delivered', 'y', 'y', '2020-12-24 08:45:43', NULL),
(6, 'Delivery Unsuccessful', 'Delivery Unsuccessful', 'Delivery Unsuccessful', 'Delivery Unsuccessful', 'y', 'y', '2020-12-24 08:45:43', NULL),
(7, 'Undelivered Product Returned', 'Undelivered Product Returned', 'Undelivered Product Returned', 'Delivery Unsuccessful', 'n', 'n', '2020-12-24 08:45:43', NULL),
(8, 'Undelivered Product Return Confirmed', 'Undelivered Product Return Confirmed', 'Undelivered Product Return Confirmed', 'Delivery Unsuccessful', 'n', 'n', '2020-12-24 08:45:43', NULL),
(9, 'Return - Schedule Return Pickup', 'Schedule Return Pickup', 'Schedule Return Pickup', 'Delivered', 'n', 'n', '2020-12-24 08:45:43', NULL),
(10, 'Return Pickup Initiated', 'Return Pickup Initiated', 'Return Pickup Initiated', 'Return Pickup Initiated', 'y', 'y', '2020-12-24 08:45:43', NULL),
(11, 'Product Returned', 'Product Returned to Seller', 'Product Returned to Seller', 'Product Returned', 'n', 'n', '2020-12-24 08:45:43', NULL),
(12, 'Product Return confirmed', 'Product Return Confirmed', 'Product Return Confirmed', 'Product Returned', 'n', 'n', '2020-12-24 08:45:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `size_charts`
--

CREATE TABLE `size_charts` (
  `id` int NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brands` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Array of brand ids',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seller_id` int DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `size_charts`
--

INSERT INTO `size_charts` (`id`, `name`, `brands`, `description`, `image`, `seller_id`, `is_default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Fabpik Test Team', NULL, 'Test', '100000/sizechart/21012021/msi-general-clothing-size-chart.jpg', 2, 0, '2021-01-20 18:38:29', '2021-01-20 18:38:29', NULL),
(3, 'Fabpik Test Team', NULL, 'Test', '100000/sizechart/21012021/msi-general-clothing-size-chart.jpg', 3, 0, '2021-01-20 19:12:12', '2021-01-20 19:12:12', NULL),
(4, 'mirakitest', NULL, 'mirakitest', '100004/sizechart/1611551901.jpg', 8, 1, '2021-01-24 18:18:42', '2021-01-24 18:18:42', NULL),
(5, 'Fabpik', NULL, 'Fabpik', '100007/sizechart/1611904175.png', 13, 0, '2021-01-28 20:09:58', '2021-02-02 20:16:52', '2021-02-02 20:16:52'),
(6, 'PARROT CROW SIZE CHART', NULL, 'PARROT CROW SIZE CHART', '100010/sizechart/29012021/PARROT CROW SIZE CHART.jpg', 16, 0, '2021-01-28 23:07:08', '2021-01-28 23:07:08', NULL),
(7, 'PARROT CROW SIZE CHART', NULL, 'PARROT CROW SIZE CHART', '100007/sizechart/03022021/Parrotcrow size chart.jpg', 13, 0, '2021-02-02 20:22:57', '2021-02-02 20:22:57', NULL),
(8, 'lavanyatest', NULL, 'lavanyatest', '100001/sizechart/02022021/189.jpg', 4, 0, '2021-02-03 18:24:31', '2021-02-03 18:24:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sms_logs`
--

CREATE TABLE `sms_logs` (
  `id` int NOT NULL,
  `mobile` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_data` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sms_logs`
--

INSERT INTO `sms_logs` (`id`, `mobile`, `action`, `response_data`, `created_at`, `updated_at`) VALUES
(1, '8520021995', 'verify_mobile', '{\"message\":\"306c7a715350303534373734\",\"type\":\"success\"}', '2020-12-26 12:15:42', NULL),
(2, '8520021995', 'verify_mobile', '{\"message\":\"306c7a715643383036343937\",\"type\":\"success\"}', '2020-12-26 12:18:29', NULL),
(3, '9676468686', 'verify_mobile', '{\"message\":\"306c426f6970353431383333\",\"type\":\"success\"}', '2020-12-28 09:39:16', NULL),
(4, '9949587496', 'login_otp', '{\"message\":\"306c456f4e41363932373534\",\"type\":\"success\"}', '2020-12-31 10:10:27', NULL),
(5, '9948839362', 'login_otp', '{\"message\":\"3161656f4775303438343935\",\"type\":\"success\"}', '2021-01-05 10:03:21', NULL),
(6, '9676313918', 'login_otp', '{\"message\":\"316165733044393639313132\",\"type\":\"success\"}', '2021-01-05 13:30:30', NULL),
(7, '9550261360', 'login_otp', '{\"message\":\"3161666b6176363337373936\",\"type\":\"success\"}', '2021-01-06 05:31:22', NULL),
(8, '9676313918', 'login_otp', '{\"message\":\"3161666b6137353535313137\",\"type\":\"success\"}', '2021-01-06 05:31:59', NULL),
(9, '9550261360', 'login_otp', '{\"message\":\"3161666b626a343235393631\",\"type\":\"success\"}', '2021-01-06 05:32:10', NULL),
(10, '9948839362', 'login_otp', '{\"message\":\"3161666b5866313034323733\",\"type\":\"success\"}', '2021-01-06 06:20:06', NULL),
(11, '9949587496', 'login_otp', '{\"message\":\"3161666c7558353738313137\",\"type\":\"success\"}', '2021-01-06 06:51:50', NULL),
(12, '9550261360', 'login_otp', '{\"message\":\"3161666e4634313331373736\",\"type\":\"success\"}', '2021-01-06 09:02:56', NULL),
(13, '9550261360', 'login_otp', '{\"message\":\"3161666e4744313138313034\",\"type\":\"success\"}', '2021-01-06 09:03:30', NULL),
(14, '9550261360', 'login_otp', '{\"message\":\"31616671624e393133383935\",\"type\":\"success\"}', '2021-01-06 11:32:40', NULL),
(15, '9550261360', 'login_otp', '{\"message\":\"316166736957323238383630\",\"type\":\"success\"}', '2021-01-06 13:39:49', NULL),
(16, '9948037153', 'verify_mobile', '{\"message\":\"316166744b41363634383436\",\"type\":\"success\"}', '2021-01-06 15:07:27', NULL),
(17, '9949587496', 'login_otp', '{\"message\":\"31616676566e313438313230\",\"type\":\"success\"}', '2021-01-06 17:18:14', NULL),
(18, '9676313918', 'verify_mobile', '{\"message\":\"3161676a576e383335373339\",\"type\":\"success\"}', '2021-01-07 05:19:14', NULL),
(19, '7780361239', 'verify_mobile', '{\"message\":\"3161676a5978333133313733\",\"type\":\"success\"}', '2021-01-07 05:21:24', NULL),
(20, '7780361239', 'verify_mobile', '{\"message\":\"3161676a5a63353233313135\",\"type\":\"success\"}', '2021-01-07 05:22:03', NULL),
(21, '7780361239', 'verify_mobile', '{\"message\":\"3161676a5a6c333130313634\",\"type\":\"success\"}', '2021-01-07 05:22:12', NULL),
(22, '9550261360', 'login_otp', '{\"message\":\"3161676c6130353532333237\",\"type\":\"success\"}', '2021-01-07 06:31:00', NULL),
(23, '9550261360', 'login_otp', '{\"message\":\"3161676c4c73363032363030\",\"type\":\"success\"}', '2021-01-07 07:08:19', NULL),
(24, '9550261360', 'login_otp', '{\"message\":\"3161676c4c55333430363237\",\"type\":\"success\"}', '2021-01-07 07:08:47', NULL),
(25, '9550261360', 'login_otp', '{\"message\":\"3161676c4c35393937393637\",\"type\":\"success\"}', '2021-01-07 07:08:58', NULL),
(26, '9847474855', 'verify_mobile', '{\"message\":\"3161676d6e34303339353238\",\"type\":\"success\"}', '2021-01-07 07:44:56', NULL),
(27, '9177669893', 'login_otp', '{\"message\":\"3161676d7152393130363237\",\"type\":\"success\"}', '2021-01-07 07:47:44', NULL),
(28, '8884273650', 'verify_mobile', '{\"message\":\"3161676d7a58343436393435\",\"type\":\"success\"}', '2021-01-07 07:56:50', NULL),
(29, '3456789098', 'login_otp', '{\"message\":\"3161676d4475353636313132\",\"type\":\"success\"}', '2021-01-07 08:00:21', NULL),
(30, '5678909876', 'login_otp', '{\"message\":\"3161676d455a333137373932\",\"type\":\"success\"}', '2021-01-07 08:01:52', NULL),
(31, '5678909876', 'login_otp', '{\"message\":\"3161676d4848333533363330\",\"type\":\"success\"}', '2021-01-07 08:04:34', NULL),
(32, '2345678909', 'login_otp', '{\"message\":\"3161676d4d48383836323537\",\"type\":\"success\"}', '2021-01-07 08:09:34', NULL),
(33, '9550261360', 'login_otp', '{\"message\":\"3161676e4d70383530303630\",\"type\":\"success\"}', '2021-01-07 09:09:16', NULL),
(34, '9550261360', 'login_otp', '{\"message\":\"3161676e4d31343832323937\",\"type\":\"success\"}', '2021-01-07 09:09:53', NULL),
(35, '9550261360', 'login_otp', '{\"message\":\"3161676e5771373335323437\",\"type\":\"success\"}', '2021-01-07 09:19:17', NULL),
(36, '9550261360', 'login_otp', '{\"message\":\"3161676e5755323437373132\",\"type\":\"success\"}', '2021-01-07 09:19:47', NULL),
(37, '9550261360', 'login_otp', '{\"message\":\"3161676e5861393539343234\",\"type\":\"success\"}', '2021-01-07 09:20:01', NULL),
(38, '7780361239', 'verify_mobile', '{\"message\":\"3161676e5a5a373330353635\",\"type\":\"success\"}', '2021-01-07 09:22:52', NULL),
(39, '8328566312', 'verify_mobile', '{\"message\":\"3161676f4b35383835313536\",\"type\":\"success\"}', '2021-01-07 10:07:57', NULL),
(40, '3456789876', 'login_otp', '{\"message\":\"3161676f5337363736303038\",\"type\":\"success\"}', '2021-01-07 10:15:59', NULL),
(41, '6789876545', 'login_otp', '{\"message\":\"3161676f5453333734363835\",\"type\":\"success\"}', '2021-01-07 10:16:46', NULL),
(42, '2345678987', 'login_otp', '{\"message\":\"3161676f5542373132313532\",\"type\":\"success\"}', '2021-01-07 10:17:28', NULL),
(43, '6789876545', 'login_otp', '{\"message\":\"3161676f5637323331363034\",\"type\":\"success\"}', '2021-01-07 10:18:59', NULL),
(44, '6789876545', 'login_otp', '{\"message\":\"3161676f5974373739373138\",\"type\":\"success\"}', '2021-01-07 10:21:20', NULL),
(45, '8328566312', 'verify_mobile', '{\"message\":\"3161676f5941303331333833\",\"type\":\"success\"}', '2021-01-07 10:21:27', NULL),
(46, '6789876545', 'login_otp', '{\"message\":\"3161676f5a67343438343834\",\"type\":\"success\"}', '2021-01-07 10:22:07', NULL),
(47, '2345678987', 'login_otp', '{\"message\":\"3161676f3262373539313330\",\"type\":\"success\"}', '2021-01-07 10:24:02', NULL),
(48, '2345678987', 'login_otp', '{\"message\":\"3161676f334f343830363334\",\"type\":\"success\"}', '2021-01-07 10:25:41', NULL),
(49, '2345678987', 'login_otp', '{\"message\":\"316167706546393135363035\",\"type\":\"success\"}', '2021-01-07 10:35:32', NULL),
(50, '2345678987', 'login_otp', '{\"message\":\"316167706668333234313733\",\"type\":\"success\"}', '2021-01-07 10:36:08', NULL),
(51, '2345678987', 'login_otp', '{\"message\":\"316167706765373238373736\",\"type\":\"success\"}', '2021-01-07 10:37:05', NULL),
(52, '2345678987', 'login_otp', '{\"message\":\"316167706733363232333130\",\"type\":\"success\"}', '2021-01-07 10:37:56', NULL),
(53, '4567898765', 'login_otp', '{\"message\":\"316167706842333930323032\",\"type\":\"success\"}', '2021-01-07 10:38:28', NULL),
(54, '2345678987', 'login_otp', '{\"message\":\"316167706a6f333331343138\",\"type\":\"success\"}', '2021-01-07 10:40:15', NULL),
(55, '2345678909', 'login_otp', '{\"message\":\"316167706c73343932343232\",\"type\":\"success\"}', '2021-01-07 10:42:20', NULL),
(56, '4567898765', 'login_otp', '{\"message\":\"316167706d52343238303731\",\"type\":\"success\"}', '2021-01-07 10:43:44', NULL),
(57, '3456789098', 'login_otp', '{\"message\":\"316167706f30303933353233\",\"type\":\"success\"}', '2021-01-07 10:45:00', NULL),
(58, '8121572957', 'verify_mobile', '{\"message\":\"316167707233323437393430\",\"type\":\"success\"}', '2021-01-07 10:48:55', NULL),
(59, '6789876545', 'login_otp', '{\"message\":\"316167707343383434313837\",\"type\":\"success\"}', '2021-01-07 10:49:29', NULL),
(60, '5678909876', 'login_otp', '{\"message\":\"316167707457383432353532\",\"type\":\"success\"}', '2021-01-07 10:50:49', NULL),
(61, '2345678909', 'login_otp', '{\"message\":\"31616770754a343639333438\",\"type\":\"success\"}', '2021-01-07 10:51:36', NULL),
(62, '9550261360', 'login_otp', '{\"message\":\"316167705977323832303837\",\"type\":\"success\"}', '2021-01-07 11:21:23', NULL),
(63, '9618629334', 'login_otp', '{\"message\":\"316167703643393638343233\",\"type\":\"success\"}', '2021-01-07 11:28:29', NULL),
(64, '9618629334', 'login_otp', '{\"message\":\"316167703633323035383730\",\"type\":\"success\"}', '2021-01-07 11:28:55', NULL),
(65, '9618629334', 'login_otp', '{\"message\":\"316167703748333139353435\",\"type\":\"success\"}', '2021-01-07 11:29:34', NULL),
(66, '9676313918', 'login_otp', '{\"message\":\"316167716266383439393138\",\"type\":\"success\"}', '2021-01-07 11:32:06', NULL),
(67, '9676313918', 'login_otp', '{\"message\":\"316167716237393130313233\",\"type\":\"success\"}', '2021-01-07 11:32:59', NULL),
(68, '9550261360', 'order_place_admin', '{\"message\":\"31616771746c373935393739\",\"type\":\"success\"}', '2021-01-07 11:50:12', NULL),
(69, '9550261360', 'order_place_user', '{\"message\":\"31616771746d353933333130\",\"type\":\"success\"}', '2021-01-07 11:50:13', NULL),
(70, '8121572957', 'order_place_seller', '{\"message\":\"31616771746d323933363935\",\"type\":\"success\"}', '2021-01-07 11:50:13', NULL),
(71, '8121572957', 'order_place_seller', '{\"message\":\"316167717470333139343238\",\"type\":\"success\"}', '2021-01-07 11:50:16', NULL),
(72, '7083775982', 'login_otp', '{\"message\":\"316167715066343536353638\",\"type\":\"success\"}', '2021-01-07 12:12:06', NULL),
(73, '9550261360', 'order_place_admin', '{\"message\":\"31616771524f323832353339\",\"type\":\"success\"}', '2021-01-07 12:14:41', NULL),
(74, '9676313918', 'order_place_user', '{\"message\":\"31616771524f393537333830\",\"type\":\"success\"}', '2021-01-07 12:14:41', NULL),
(75, '8121572957', 'order_place_seller', '{\"message\":\"316167715250313237333636\",\"type\":\"success\"}', '2021-01-07 12:14:42', NULL),
(76, '9676313918', 'login_otp', '{\"message\":\"316167715661363038393433\",\"type\":\"success\"}', '2021-01-07 12:18:01', NULL),
(77, '9676313918', 'login_otp', '{\"message\":\"316167715658333136383233\",\"type\":\"success\"}', '2021-01-07 12:18:50', NULL),
(78, '9949587496', 'login_otp', '{\"message\":\"31616771574f323331393036\",\"type\":\"success\"}', '2021-01-07 12:19:41', NULL),
(79, '9550261360', 'order_place_admin', '{\"message\":\"316167715845363132343330\",\"type\":\"success\"}', '2021-01-07 12:20:31', NULL),
(80, '9676313918', 'order_place_user', '{\"message\":\"316167715846313033343735\",\"type\":\"success\"}', '2021-01-07 12:20:32', NULL),
(81, '8121572957', 'order_place_seller', '{\"message\":\"316167715846333435383434\",\"type\":\"success\"}', '2021-01-07 12:20:32', NULL),
(82, '9550261360', 'order_place_admin', '{\"message\":\"31616771316f353132373535\",\"type\":\"success\"}', '2021-01-07 12:23:15', NULL),
(83, '9676313918', 'order_place_user', '{\"message\":\"31616771316f383434383733\",\"type\":\"success\"}', '2021-01-07 12:23:15', NULL),
(84, '8121572957', 'order_place_seller', '{\"message\":\"31616771316f393135323131\",\"type\":\"success\"}', '2021-01-07 12:23:15', NULL),
(85, '9177669893', 'login_otp', '{\"message\":\"31616771354c363436373639\",\"type\":\"success\"}', '2021-01-07 12:27:38', NULL),
(86, '9550261360', 'order_place_admin', '{\"message\":\"316167713663313335353435\",\"type\":\"success\"}', '2021-01-07 12:28:03', NULL),
(87, '9550261360', 'order_place_user', '{\"message\":\"316167713664373937333836\",\"type\":\"success\"}', '2021-01-07 12:28:04', NULL),
(88, '8121572957', 'order_place_seller', '{\"message\":\"316167713664383838313030\",\"type\":\"success\"}', '2021-01-07 12:28:04', NULL),
(89, '8121572957', 'order_place_seller', '{\"message\":\"316167713667363338393134\",\"type\":\"success\"}', '2021-01-07 12:28:07', NULL),
(90, '9177669893', 'login_otp', '{\"message\":\"316167713676383232313436\",\"type\":\"success\"}', '2021-01-07 12:28:22', NULL),
(91, '9676313918', 'login_otp', '{\"message\":\"316167723068333134383633\",\"type\":\"success\"}', '2021-01-07 12:30:08', NULL),
(92, '9550261360', 'order_place_admin', '{\"message\":\"316167726243363239383730\",\"type\":\"success\"}', '2021-01-07 12:32:29', NULL),
(93, '9676313918', 'order_place_user', '{\"message\":\"316167726243333132303231\",\"type\":\"success\"}', '2021-01-07 12:32:29', NULL),
(94, '8121572957', 'order_place_seller', '{\"message\":\"316167726243333738383932\",\"type\":\"success\"}', '2021-01-07 12:32:29', NULL),
(95, '9550261360', 'order_place_admin', '{\"message\":\"316167726259373033333834\",\"type\":\"success\"}', '2021-01-07 12:32:51', NULL),
(96, '9676313918', 'order_place_user', '{\"message\":\"316167726259383730333130\",\"type\":\"success\"}', '2021-01-07 12:32:51', NULL),
(97, '8121572957', 'order_place_seller', '{\"message\":\"316167726259343735383134\",\"type\":\"success\"}', '2021-01-07 12:32:51', NULL),
(98, '9550261360', 'order_place_admin', '{\"message\":\"316167726b6c393437373733\",\"type\":\"success\"}', '2021-01-07 12:41:12', NULL),
(99, '9550261360', 'order_place_user', '{\"message\":\"316167726b6c313132353138\",\"type\":\"success\"}', '2021-01-07 12:41:12', NULL),
(100, '8121572957', 'order_place_seller', '{\"message\":\"316167726b6c373436393934\",\"type\":\"success\"}', '2021-01-07 12:41:12', NULL),
(101, '8121572957', 'order_place_seller', '{\"message\":\"316167726b70333335393837\",\"type\":\"success\"}', '2021-01-07 12:41:16', NULL),
(102, '9550261360', 'order_place_admin', '{\"message\":\"316167727152333731323830\",\"type\":\"success\"}', '2021-01-07 12:47:44', NULL),
(103, '9676313918', 'order_place_user', '{\"message\":\"316167727152333032333730\",\"type\":\"success\"}', '2021-01-07 12:47:44', NULL),
(104, '8121572957', 'order_place_seller', '{\"message\":\"316167727152323930313131\",\"type\":\"success\"}', '2021-01-07 12:47:45', NULL),
(105, '9550261360', 'order_place_admin', '{\"message\":\"31616772764e373835383631\",\"type\":\"success\"}', '2021-01-07 12:52:40', NULL),
(106, '9550261360', 'order_place_user', '{\"message\":\"31616772764e303533383435\",\"type\":\"success\"}', '2021-01-07 12:52:40', NULL),
(107, '8121572957', 'order_place_seller', '{\"message\":\"31616772764e363836303139\",\"type\":\"success\"}', '2021-01-07 12:52:40', NULL),
(108, '8121572957', 'order_place_seller', '{\"message\":\"316167727652353934323736\",\"type\":\"success\"}', '2021-01-07 12:52:44', NULL),
(109, '9550261360', 'order_place_admin', '{\"message\":\"31616772786b323636383731\",\"type\":\"success\"}', '2021-01-07 12:54:11', NULL),
(110, '9550261360', 'order_place_user', '{\"message\":\"31616772786b373636313435\",\"type\":\"success\"}', '2021-01-07 12:54:11', NULL),
(111, '8121572957', 'order_place_seller', '{\"message\":\"31616772786c353137323332\",\"type\":\"success\"}', '2021-01-07 12:54:12', NULL),
(112, '8121572957', 'order_place_seller', '{\"message\":\"31616772786f303830363032\",\"type\":\"success\"}', '2021-01-07 12:54:15', NULL),
(113, '9550261360', 'login_otp', '{\"message\":\"316167724a49393430343030\",\"type\":\"success\"}', '2021-01-07 13:06:35', NULL),
(114, '9676468686', 'login_otp', '{\"message\":\"316167724d6b323439393338\",\"type\":\"success\"}', '2021-01-07 13:09:11', NULL),
(115, '9550261360', 'login_otp', '{\"message\":\"316167724d35323837323134\",\"type\":\"success\"}', '2021-01-07 13:09:57', NULL),
(116, '9948839362', 'login_otp', '{\"message\":\"316167725a46383234353737\",\"type\":\"success\"}', '2021-01-07 13:22:32', NULL),
(117, '9550261360', 'login_otp', '{\"message\":\"316167723154313135313738\",\"type\":\"success\"}', '2021-01-07 13:23:46', NULL),
(118, '9550261360', 'login_otp', '{\"message\":\"316167736d61333637313238\",\"type\":\"success\"}', '2021-01-07 13:43:01', NULL),
(119, '9949587496', 'login_otp', '{\"message\":\"316167737730383837343039\",\"type\":\"success\"}', '2021-01-07 13:53:00', NULL),
(120, '9550261360', 'order_place_admin', '{\"message\":\"31616773414e343731393039\",\"type\":\"success\"}', '2021-01-07 13:57:40', NULL),
(121, '9550261360', 'order_place_user', '{\"message\":\"31616773414e393432343838\",\"type\":\"success\"}', '2021-01-07 13:57:40', NULL),
(122, '8121572957', 'order_place_seller', '{\"message\":\"31616773414e303539353636\",\"type\":\"success\"}', '2021-01-07 13:57:40', NULL),
(123, '9550261360', 'order_place_admin', '{\"message\":\"316167734973333833393137\",\"type\":\"success\"}', '2021-01-07 14:05:19', NULL),
(124, '9550261360', 'order_place_user', '{\"message\":\"316167734973313434383138\",\"type\":\"success\"}', '2021-01-07 14:05:19', NULL),
(125, '8121572957', 'order_place_seller', '{\"message\":\"316167734973313930343739\",\"type\":\"success\"}', '2021-01-07 14:05:19', NULL),
(126, '9550261360', 'order_place_admin', '{\"message\":\"316167734a30353431323831\",\"type\":\"success\"}', '2021-01-07 14:06:00', NULL),
(127, '9550261360', 'order_place_user', '{\"message\":\"316167734a61343735383237\",\"type\":\"success\"}', '2021-01-07 14:06:01', NULL),
(128, '8121572957', 'order_place_seller', '{\"message\":\"316167734a61393937383232\",\"type\":\"success\"}', '2021-01-07 14:06:01', NULL),
(129, '9676313918', 'login_otp', '{\"message\":\"3161686a4643393838373734\",\"type\":\"success\"}', '2021-01-08 05:02:29', NULL),
(130, '9550261360', 'order_place_admin', '{\"message\":\"3161686a4932363637383831\",\"type\":\"success\"}', '2021-01-08 05:05:55', NULL),
(131, '9676313918', 'order_place_user', '{\"message\":\"3161686a4933373935383038\",\"type\":\"success\"}', '2021-01-08 05:05:55', NULL),
(132, '8328566312', 'order_place_seller', '{\"message\":\"3161686a4933313938363637\",\"type\":\"success\"}', '2021-01-08 05:05:55', NULL),
(133, '8121572957', 'order_place_seller', '{\"message\":\"3161686a4937393030323932\",\"type\":\"success\"}', '2021-01-08 05:05:59', NULL),
(134, '7780361239', 'verify_mobile', '{\"message\":\"3161686a4d4a353239303536\",\"type\":\"success\"}', '2021-01-08 05:09:36', NULL),
(135, '9676313918', 'verify_mobile', '{\"message\":\"3161686a524a353333353635\",\"type\":\"success\"}', '2021-01-08 05:14:36', NULL),
(136, '9676313918', 'verify_mobile', '{\"message\":\"3161686a524b393132383431\",\"type\":\"success\"}', '2021-01-08 05:14:37', NULL),
(137, '9676313918', 'verify_mobile', '{\"message\":\"3161686a524d343534373134\",\"type\":\"success\"}', '2021-01-08 05:14:39', NULL),
(138, '9618050570', 'verify_mobile', '{\"message\":\"3161686a5571333338353233\",\"type\":\"success\"}', '2021-01-08 05:17:17', NULL),
(139, '9676313918', 'verify_mobile', '{\"message\":\"3161686a5931383832373337\",\"type\":\"success\"}', '2021-01-08 05:21:53', NULL),
(140, '9676313918', 'verify_mobile', '{\"message\":\"3161686a326b363530343739\",\"type\":\"success\"}', '2021-01-08 05:24:11', NULL),
(141, '9676313918', 'verify_mobile', '{\"message\":\"3161686a3443343032363630\",\"type\":\"success\"}', '2021-01-08 05:26:29', NULL),
(142, '9676313918', 'login_otp', '{\"message\":\"3161686b7731313138393438\",\"type\":\"success\"}', '2021-01-08 05:53:53', NULL),
(143, '9676313918', 'login_otp', '{\"message\":\"3161686b7875363232373435\",\"type\":\"success\"}', '2021-01-08 05:54:21', NULL),
(144, '9676313918', 'login_otp', '{\"message\":\"3161686b7971343236373839\",\"type\":\"success\"}', '2021-01-08 05:55:17', NULL),
(145, '7780361239', 'login_otp', '{\"message\":\"3161686b4163313338393635\",\"type\":\"success\"}', '2021-01-08 05:57:03', NULL),
(146, '9676313918', 'login_otp', '{\"message\":\"3161686b414c323136383639\",\"type\":\"success\"}', '2021-01-08 05:57:38', NULL),
(147, '9618050570', 'login_otp', '{\"message\":\"3161686b4278393732333039\",\"type\":\"success\"}', '2021-01-08 05:58:24', NULL),
(148, '9618050570', 'login_otp', '{\"message\":\"3161686b4478333333333735\",\"type\":\"success\"}', '2021-01-08 06:00:24', NULL),
(149, '9618050570', 'login_otp', '{\"message\":\"3161686b5530393538303632\",\"type\":\"success\"}', '2021-01-08 06:17:00', NULL),
(150, '9550261360', 'login_otp', '{\"message\":\"3161686b5a45383136393931\",\"type\":\"success\"}', '2021-01-08 06:22:31', NULL),
(151, '9550261360', 'order_place_admin', '{\"message\":\"3161686b334c313031383331\",\"type\":\"success\"}', '2021-01-08 06:25:38', NULL),
(152, '9550261360', 'order_place_user', '{\"message\":\"3161686b334c393937313733\",\"type\":\"success\"}', '2021-01-08 06:25:39', NULL),
(153, '8328566312', 'order_place_seller', '{\"message\":\"3161686b334d383634323931\",\"type\":\"success\"}', '2021-01-08 06:25:39', NULL),
(154, '9550261360', 'order_place_admin', '{\"message\":\"3161686b3354313632373833\",\"type\":\"success\"}', '2021-01-08 06:25:47', NULL),
(155, '9550261360', 'order_place_user', '{\"message\":\"3161686b3355353039303430\",\"type\":\"success\"}', '2021-01-08 06:25:47', NULL),
(156, '8328566312', 'order_place_seller', '{\"message\":\"3161686b3355373339303434\",\"type\":\"success\"}', '2021-01-08 06:25:47', NULL),
(157, '9550261360', 'order_place_admin', '{\"message\":\"3161686b3646353030343436\",\"type\":\"success\"}', '2021-01-08 06:28:32', NULL),
(158, '9550261360', 'order_place_user', '{\"message\":\"3161686b3646323537333331\",\"type\":\"success\"}', '2021-01-08 06:28:32', NULL),
(159, '8328566312', 'order_place_seller', '{\"message\":\"3161686b3646393630303338\",\"type\":\"success\"}', '2021-01-08 06:28:32', NULL),
(160, '9550261360', 'login_otp', '{\"message\":\"3161686b3776353635333536\",\"type\":\"success\"}', '2021-01-08 06:29:22', NULL),
(161, '9550261360', 'order_place_admin', '{\"message\":\"3161686c7543383032383730\",\"type\":\"success\"}', '2021-01-08 06:51:29', NULL),
(162, '9550261360', 'order_place_user', '{\"message\":\"3161686c7544323435363233\",\"type\":\"success\"}', '2021-01-08 06:51:30', NULL),
(163, '8328566312', 'order_place_seller', '{\"message\":\"3161686c7544323039373638\",\"type\":\"success\"}', '2021-01-08 06:51:30', NULL),
(164, '8328566312', 'order_place_seller', '{\"message\":\"3161686c7548333630393238\",\"type\":\"success\"}', '2021-01-08 06:51:34', NULL),
(165, '9550261360', 'order_place_admin', '{\"message\":\"3161686d6463313532323531\",\"type\":\"success\"}', '2021-01-08 07:34:03', NULL),
(166, '9550261360', 'order_place_user', '{\"message\":\"3161686d6464313132353931\",\"type\":\"success\"}', '2021-01-08 07:34:04', NULL),
(167, '8328566312', 'order_place_seller', '{\"message\":\"3161686d6464343037333232\",\"type\":\"success\"}', '2021-01-08 07:34:04', NULL),
(168, '9550261360', 'order_place_admin', '{\"message\":\"3161686d654d353532373938\",\"type\":\"success\"}', '2021-01-08 07:35:39', NULL),
(169, '9550261360', 'order_place_user', '{\"message\":\"3161686d654d303534313633\",\"type\":\"success\"}', '2021-01-08 07:35:39', NULL),
(170, '8328566312', 'order_place_seller', '{\"message\":\"3161686d654d363736373835\",\"type\":\"success\"}', '2021-01-08 07:35:39', NULL),
(171, '9948839362', 'login_otp', '{\"message\":\"3161686d7054353432323131\",\"type\":\"success\"}', '2021-01-08 07:46:46', NULL),
(172, '9618050570', 'login_otp', '{\"message\":\"3161686e6155323337333031\",\"type\":\"success\"}', '2021-01-08 08:31:47', NULL),
(173, '9550261360', 'login_otp', '{\"message\":\"3161686e6776393638343135\",\"type\":\"success\"}', '2021-01-08 08:37:22', NULL),
(174, '9550261360', 'order_place_admin', '{\"message\":\"3161686e684b373134313135\",\"type\":\"success\"}', '2021-01-08 08:38:37', NULL),
(175, '9550261360', 'order_place_user', '{\"message\":\"3161686e684c393533363336\",\"type\":\"success\"}', '2021-01-08 08:38:38', NULL),
(176, '8328566312', 'order_place_seller', '{\"message\":\"3161686e684c333937333033\",\"type\":\"success\"}', '2021-01-08 08:38:38', NULL),
(177, '9550261360', 'order_place_admin', '{\"message\":\"3161686e6950313238373232\",\"type\":\"success\"}', '2021-01-08 08:39:42', NULL),
(178, '9550261360', 'order_place_user', '{\"message\":\"3161686e6950353534353036\",\"type\":\"success\"}', '2021-01-08 08:39:42', NULL),
(179, '8328566312', 'order_place_seller', '{\"message\":\"3161686e6951363636323535\",\"type\":\"success\"}', '2021-01-08 08:39:43', NULL),
(180, '7780361239', 'verify_mobile', '{\"message\":\"3161686e7750393935343639\",\"type\":\"success\"}', '2021-01-08 08:53:42', NULL),
(181, '9948839362', 'login_otp', '{\"message\":\"3161686f4a37343035393535\",\"type\":\"success\"}', '2021-01-08 10:06:59', NULL),
(182, '9948839362', 'login_otp', '{\"message\":\"3161686f4b78363537383332\",\"type\":\"success\"}', '2021-01-08 10:07:24', NULL),
(183, '9618050570', 'login_otp', '{\"message\":\"316168713663383032303333\",\"type\":\"success\"}', '2021-01-08 12:28:03', NULL),
(184, '9948839362', 'login_otp', '{\"message\":\"3161696a7a57363038363539\",\"type\":\"success\"}', '2021-01-09 04:56:49', NULL),
(185, '9948839362', 'login_otp', '{\"message\":\"3161696c424c333533353639\",\"type\":\"success\"}', '2021-01-09 06:58:38', NULL),
(186, '9948839362', 'login_otp', '{\"message\":\"3161696d6b6e383434383632\",\"type\":\"success\"}', '2021-01-09 07:41:14', NULL),
(187, '9948839362', 'login_otp', '{\"message\":\"3161696e7836343236323733\",\"type\":\"success\"}', '2021-01-09 08:54:58', NULL),
(188, '9948839362', 'login_otp', '{\"message\":\"3161696e434f333337373634\",\"type\":\"success\"}', '2021-01-09 08:59:41', NULL),
(189, '8500456815', 'login_otp', '{\"message\":\"316169725a66313939313831\",\"type\":\"success\"}', '2021-01-09 13:22:06', NULL),
(190, '9948839362', 'login_otp', '{\"message\":\"31616b6a6566383637323535\",\"type\":\"success\"}', '2021-01-11 04:35:06', NULL),
(191, '9550261360', 'login_otp', '{\"message\":\"31616b6c4b6f323437363832\",\"type\":\"success\"}', '2021-01-11 07:07:15', NULL),
(192, '9550261360', 'login_otp', '{\"message\":\"31616b6c4b5a343032333236\",\"type\":\"success\"}', '2021-01-11 07:07:52', NULL),
(193, '9550261360', 'order_place_admin', '{\"message\":\"31616b6c5333323332363136\",\"type\":\"success\"}', '2021-01-11 07:15:55', NULL),
(194, '9550261360', 'order_place_user', '{\"message\":\"31616b6c5333383032393133\",\"type\":\"success\"}', '2021-01-11 07:15:55', NULL),
(195, '8328566312', 'order_place_seller', '{\"message\":\"31616b6c5333363936353239\",\"type\":\"success\"}', '2021-01-11 07:15:55', NULL),
(196, '8121572957', 'order_place_seller', '{\"message\":\"31616b6c5337393037303133\",\"type\":\"success\"}', '2021-01-11 07:15:59', NULL),
(197, '9948839362', 'login_otp', '{\"message\":\"31616b6c5750383231303731\",\"type\":\"success\"}', '2021-01-11 07:19:42', NULL),
(198, '9676313918', 'login_otp', '{\"message\":\"31616b6d614e393936323930\",\"type\":\"success\"}', '2021-01-11 07:31:40', NULL),
(199, '9949587496', 'login_otp', '{\"message\":\"31616b6d6356343132393930\",\"type\":\"success\"}', '2021-01-11 07:33:48', NULL),
(200, '9949587496', 'login_otp', '{\"message\":\"31616b6d6433363039313534\",\"type\":\"success\"}', '2021-01-11 07:34:55', NULL),
(201, '9949587496', 'login_otp', '{\"message\":\"31616b6d6650393134393131\",\"type\":\"success\"}', '2021-01-11 07:36:42', NULL),
(202, '9949587496', 'login_otp', '{\"message\":\"31616b6f6848323032383036\",\"type\":\"success\"}', '2021-01-11 09:38:34', NULL),
(203, '9949587496', 'login_otp', '{\"message\":\"31616b6f694c323333373837\",\"type\":\"success\"}', '2021-01-11 09:39:38', NULL),
(204, '9949587496', 'login_otp', '{\"message\":\"31616b6f735a383630383930\",\"type\":\"success\"}', '2021-01-11 09:49:52', NULL),
(205, '9949587496', 'login_otp', '{\"message\":\"31616b6f7a44383033313632\",\"type\":\"success\"}', '2021-01-11 09:56:30', NULL),
(206, '9949587496', 'login_otp', '{\"message\":\"31616b6f4267303537343638\",\"type\":\"success\"}', '2021-01-11 09:58:07', NULL),
(207, '9550261360', 'login_otp', '{\"message\":\"31616b716570383537373132\",\"type\":\"success\"}', '2021-01-11 11:35:16', NULL),
(208, '9949587496', 'login_otp', '{\"message\":\"31616b71565a363937363538\",\"type\":\"success\"}', '2021-01-11 12:18:53', NULL),
(209, '9948839362', 'login_otp', '{\"message\":\"31616b745633353537313936\",\"type\":\"success\"}', '2021-01-11 15:18:55', NULL),
(210, '9949587496', 'login_otp', '{\"message\":\"31616c6a7161363736393130\",\"type\":\"success\"}', '2021-01-12 04:47:01', NULL),
(211, '9948839362', 'login_otp', '{\"message\":\"31616c6a5630333437383932\",\"type\":\"success\"}', '2021-01-12 05:18:00', NULL),
(212, '8008314842', 'verify_mobile', '{\"message\":\"31616c6b6833353738363530\",\"type\":\"success\"}', '2021-01-12 05:38:55', NULL),
(213, '9550261360', 'order_place_admin', '{\"message\":\"31616c6b3574303031383632\",\"type\":\"success\"}', '2021-01-12 06:27:21', NULL),
(214, '9949587496', 'order_place_user', '{\"message\":\"31616c6b3575393437393133\",\"type\":\"success\"}', '2021-01-12 06:27:21', NULL),
(215, '8121572957', 'order_place_seller', '{\"message\":\"31616c6b3575393139333835\",\"type\":\"success\"}', '2021-01-12 06:27:21', NULL),
(216, '9949587496', 'login_otp', '{\"message\":\"31616c6c726a373739333030\",\"type\":\"success\"}', '2021-01-12 06:48:10', NULL),
(217, '8787878787', 'verify_mobile', '{\"message\":\"31616c6c584a343133333630\",\"type\":\"success\"}', '2021-01-12 07:20:36', NULL),
(218, '9550261360', 'login_otp', '{\"message\":\"31616c6c5930373836383137\",\"type\":\"success\"}', '2021-01-12 07:21:00', NULL),
(219, '9949587496', 'login_otp', '{\"message\":\"31616c6e7471393437363238\",\"type\":\"success\"}', '2021-01-12 08:50:18', NULL),
(220, '9949587496', 'login_otp', '{\"message\":\"31616c6e7564333638373738\",\"type\":\"success\"}', '2021-01-12 08:51:04', NULL),
(221, '9550261360', 'order_place_admin', '{\"message\":\"31616c6e3457303533313739\",\"type\":\"success\"}', '2021-01-12 09:26:49', NULL),
(222, '9949587496', 'order_place_user', '{\"message\":\"31616c6e3457303439393539\",\"type\":\"success\"}', '2021-01-12 09:26:49', NULL),
(223, '8121572957', 'order_place_seller', '{\"message\":\"31616c6e3457333832333538\",\"type\":\"success\"}', '2021-01-12 09:26:49', NULL),
(224, '8328566312', 'order_place_seller', '{\"message\":\"31616c6e3431313530313238\",\"type\":\"success\"}', '2021-01-12 09:26:53', NULL),
(225, '8328566312', 'order_place_seller', '{\"message\":\"31616c6e3435393239363036\",\"type\":\"success\"}', '2021-01-12 09:26:57', NULL),
(226, '9550261360', 'order_place_admin', '{\"message\":\"31616c6f614b383435373032\",\"type\":\"success\"}', '2021-01-12 09:31:37', NULL),
(227, '9949587496', 'order_place_user', '{\"message\":\"31616c6f614b343632343930\",\"type\":\"success\"}', '2021-01-12 09:31:38', NULL),
(228, '8328566312', 'order_place_seller', '{\"message\":\"31616c6f614c343033373635\",\"type\":\"success\"}', '2021-01-12 09:31:38', NULL),
(229, '9550261360', 'order_place_admin', '{\"message\":\"31616c6f746b373036343232\",\"type\":\"success\"}', '2021-01-12 09:50:11', NULL),
(230, '9949587496', 'order_place_user', '{\"message\":\"31616c6f746b323233353231\",\"type\":\"success\"}', '2021-01-12 09:50:11', NULL),
(231, '0328566312', 'order_place_seller', '{\"message\":\"31616c6f746b343437313836\",\"type\":\"success\"}', '2021-01-12 09:50:12', NULL),
(232, '9948883936', 'login_otp', '{\"message\":\"31616c6f5431393231373935\",\"type\":\"success\"}', '2021-01-12 10:16:53', NULL),
(233, '9948839362', 'login_otp', '{\"message\":\"31616c6f5752353436353932\",\"type\":\"success\"}', '2021-01-12 10:19:44', NULL),
(234, '9949587496', 'login_otp', '{\"message\":\"31616c717979383235373039\",\"type\":\"success\"}', '2021-01-12 11:55:25', NULL),
(235, '9949587496', 'login_otp', '{\"message\":\"31616c714951353632313931\",\"type\":\"success\"}', '2021-01-12 12:05:43', NULL),
(236, '9550261360', 'order_place_admin', '{\"message\":\"31616c714c30383632333230\",\"type\":\"success\"}', '2021-01-12 12:08:00', NULL),
(237, '9949587496', 'order_place_user', '{\"message\":\"31616c714c61323831353338\",\"type\":\"success\"}', '2021-01-12 12:08:01', NULL),
(238, '0328566312', 'order_place_seller', '{\"message\":\"31616c714c61323331333137\",\"type\":\"success\"}', '2021-01-12 12:08:01', NULL),
(239, '8121572957', 'order_place_seller', '{\"message\":\"31616c714c65373539383233\",\"type\":\"success\"}', '2021-01-12 12:08:05', NULL),
(240, '9550261360', 'order_place_admin', '{\"message\":\"31616c714e68323635363535\",\"type\":\"success\"}', '2021-01-12 12:10:08', NULL),
(241, '9949587496', 'order_place_user', '{\"message\":\"31616c714e68353336363337\",\"type\":\"success\"}', '2021-01-12 12:10:08', NULL),
(242, '0328566312', 'order_place_seller', '{\"message\":\"31616c714e69363131303430\",\"type\":\"success\"}', '2021-01-12 12:10:09', NULL),
(243, '8121572957', 'order_place_seller', '{\"message\":\"31616c714e6c383032333936\",\"type\":\"success\"}', '2021-01-12 12:10:12', NULL),
(244, '9550261360', 'order_place_admin', '{\"message\":\"31616c715357353136313035\",\"type\":\"success\"}', '2021-01-12 12:15:49', NULL),
(245, '9949587496', 'order_place_user', '{\"message\":\"31616c715357323235383237\",\"type\":\"success\"}', '2021-01-12 12:15:49', NULL),
(246, '0328566312', 'order_place_seller', '{\"message\":\"31616c715357323330303132\",\"type\":\"success\"}', '2021-01-12 12:15:49', NULL),
(247, '8121572957', 'order_place_seller', '{\"message\":\"31616c715331323136373131\",\"type\":\"success\"}', '2021-01-12 12:15:53', NULL),
(248, '9948839362', 'login_otp', '{\"message\":\"31616c715835393137383233\",\"type\":\"success\"}', '2021-01-12 12:20:57', NULL),
(249, '9550261360', 'order_place_admin', '{\"message\":\"31616c715837313536363433\",\"type\":\"success\"}', '2021-01-12 12:20:59', NULL),
(250, '9949587496', 'order_place_user', '{\"message\":\"31616c715837313136393235\",\"type\":\"success\"}', '2021-01-12 12:20:59', NULL),
(251, '8121572957', 'order_place_seller', '{\"message\":\"31616c715930323538393932\",\"type\":\"success\"}', '2021-01-12 12:21:00', NULL),
(252, '8121572957', 'order_place_seller', '{\"message\":\"31616c715963313738373534\",\"type\":\"success\"}', '2021-01-12 12:21:03', NULL),
(253, '0328566312', 'order_place_seller', '{\"message\":\"31616c715967353531343633\",\"type\":\"success\"}', '2021-01-12 12:21:07', NULL),
(254, '9550261360', 'order_place_admin', '{\"message\":\"31616c715a44313633393933\",\"type\":\"success\"}', '2021-01-12 12:22:31', NULL),
(255, '9949587496', 'order_place_user', '{\"message\":\"31616c715a45323236333533\",\"type\":\"success\"}', '2021-01-12 12:22:31', NULL),
(256, '8121572957', 'order_place_seller', '{\"message\":\"31616c715a45353034343531\",\"type\":\"success\"}', '2021-01-12 12:22:31', NULL),
(257, '8121572957', 'order_place_seller', '{\"message\":\"31616c715a49303438373437\",\"type\":\"success\"}', '2021-01-12 12:22:35', NULL),
(258, '0328566312', 'order_place_seller', '{\"message\":\"31616c715a4c343033303934\",\"type\":\"success\"}', '2021-01-12 12:22:38', NULL),
(259, '9550261360', 'order_place_admin', '{\"message\":\"31616c713549313338303537\",\"type\":\"success\"}', '2021-01-12 12:27:35', NULL),
(260, '9949587496', 'order_place_user', '{\"message\":\"31616c71354a333135343738\",\"type\":\"success\"}', '2021-01-12 12:27:36', NULL),
(261, '8121572957', 'order_place_seller', '{\"message\":\"31616c71354a303636313134\",\"type\":\"success\"}', '2021-01-12 12:27:36', NULL),
(262, '8121572957', 'order_place_seller', '{\"message\":\"31616c71354e363531393033\",\"type\":\"success\"}', '2021-01-12 12:27:40', NULL),
(263, '0328566312', 'order_place_seller', '{\"message\":\"31616c713551393638303637\",\"type\":\"success\"}', '2021-01-12 12:27:43', NULL),
(264, '9550261360', 'order_place_admin', '{\"message\":\"31616c72306b353133343036\",\"type\":\"success\"}', '2021-01-12 12:30:12', NULL),
(265, '9949587496', 'order_place_user', '{\"message\":\"31616c72306c383132333535\",\"type\":\"success\"}', '2021-01-12 12:30:12', NULL),
(266, '8121572957', 'order_place_seller', '{\"message\":\"31616c72306c353236323038\",\"type\":\"success\"}', '2021-01-12 12:30:12', NULL),
(267, '8121572957', 'order_place_seller', '{\"message\":\"31616c723070363535353031\",\"type\":\"success\"}', '2021-01-12 12:30:16', NULL),
(268, '0328566312', 'order_place_seller', '{\"message\":\"31616c723073383039393437\",\"type\":\"success\"}', '2021-01-12 12:30:19', NULL),
(269, '9550261360', 'order_place_admin', '{\"message\":\"31616c726171313932303935\",\"type\":\"success\"}', '2021-01-12 12:31:17', NULL),
(270, '9949587496', 'order_place_user', '{\"message\":\"31616c726171363134323537\",\"type\":\"success\"}', '2021-01-12 12:31:17', NULL),
(271, '8121572957', 'order_place_seller', '{\"message\":\"31616c726172393039363634\",\"type\":\"success\"}', '2021-01-12 12:31:18', NULL),
(272, '8121572957', 'order_place_seller', '{\"message\":\"31616c726175343630333132\",\"type\":\"success\"}', '2021-01-12 12:31:21', NULL),
(273, '0328566312', 'order_place_seller', '{\"message\":\"31616c726179343530303432\",\"type\":\"success\"}', '2021-01-12 12:31:25', NULL),
(274, '9550261360', 'order_place_admin', '{\"message\":\"31616c72634a363037323432\",\"type\":\"success\"}', '2021-01-12 12:33:36', NULL),
(275, '9949587496', 'order_place_user', '{\"message\":\"31616c72634a313037353030\",\"type\":\"success\"}', '2021-01-12 12:33:36', NULL),
(276, '8121572957', 'order_place_seller', '{\"message\":\"31616c72634b303737333632\",\"type\":\"success\"}', '2021-01-12 12:33:37', NULL),
(277, '8121572957', 'order_place_seller', '{\"message\":\"31616c72634e363639313437\",\"type\":\"success\"}', '2021-01-12 12:33:40', NULL),
(278, '0328566312', 'order_place_seller', '{\"message\":\"31616c726352363936383431\",\"type\":\"success\"}', '2021-01-12 12:33:44', NULL),
(279, '9550261360', 'order_place_admin', '{\"message\":\"31616c726453313739363539\",\"type\":\"success\"}', '2021-01-12 12:34:45', NULL),
(280, '9949587496', 'order_place_user', '{\"message\":\"31616c726454393231353235\",\"type\":\"success\"}', '2021-01-12 12:34:46', NULL),
(281, '8121572957', 'order_place_seller', '{\"message\":\"31616c726454303438383435\",\"type\":\"success\"}', '2021-01-12 12:34:46', NULL),
(282, '8121572957', 'order_place_seller', '{\"message\":\"31616c726457313038323936\",\"type\":\"success\"}', '2021-01-12 12:34:49', NULL),
(283, '0328566312', 'order_place_seller', '{\"message\":\"31616c726431333936313638\",\"type\":\"success\"}', '2021-01-12 12:34:53', NULL),
(284, '9550261360', 'order_place_admin', '{\"message\":\"31616c726645363433353735\",\"type\":\"success\"}', '2021-01-12 12:36:31', NULL),
(285, '9949587496', 'order_place_user', '{\"message\":\"31616c726645323432363234\",\"type\":\"success\"}', '2021-01-12 12:36:31', NULL),
(286, '8121572957', 'order_place_seller', '{\"message\":\"31616c726645373937343937\",\"type\":\"success\"}', '2021-01-12 12:36:31', NULL),
(287, '8121572957', 'order_place_seller', '{\"message\":\"31616c726649353839303134\",\"type\":\"success\"}', '2021-01-12 12:36:35', NULL),
(288, '0328566312', 'order_place_seller', '{\"message\":\"31616c72664c323834363133\",\"type\":\"success\"}', '2021-01-12 12:36:39', NULL),
(289, '9550261360', 'order_place_admin', '{\"message\":\"31616c724a46393139323332\",\"type\":\"success\"}', '2021-01-12 13:06:32', NULL),
(290, '9949587496', 'order_place_user', '{\"message\":\"31616c724a46393234393930\",\"type\":\"success\"}', '2021-01-12 13:06:32', NULL),
(291, '8121572957', 'order_place_seller', '{\"message\":\"31616c724a46323030393035\",\"type\":\"success\"}', '2021-01-12 13:06:32', NULL),
(292, '8121572957', 'order_place_seller', '{\"message\":\"31616c724a4a363835333930\",\"type\":\"success\"}', '2021-01-12 13:06:36', NULL),
(293, '0328566312', 'order_place_seller', '{\"message\":\"31616c724a4e323330303634\",\"type\":\"success\"}', '2021-01-12 13:06:40', NULL),
(294, '9550261360', 'order_place_admin', '{\"message\":\"31616c724c41373832303534\",\"type\":\"success\"}', '2021-01-12 13:08:27', NULL),
(295, '9949587496', 'order_place_user', '{\"message\":\"31616c724c41383435373537\",\"type\":\"success\"}', '2021-01-12 13:08:27', NULL),
(296, '8121572957', 'order_place_seller', '{\"message\":\"31616c724c42363639333336\",\"type\":\"success\"}', '2021-01-12 13:08:28', NULL),
(297, '8121572957', 'order_place_seller', '{\"message\":\"31616c724c45383538333333\",\"type\":\"success\"}', '2021-01-12 13:08:31', NULL),
(298, '0328566312', 'order_place_seller', '{\"message\":\"31616c724c49393138313133\",\"type\":\"success\"}', '2021-01-12 13:08:35', NULL),
(299, '9948839362', 'login_otp', '{\"message\":\"31616c733264373135373033\",\"type\":\"success\"}', '2021-01-12 14:24:04', NULL),
(300, '9949587496', 'login_otp', '{\"message\":\"31616c764b65373532343238\",\"type\":\"success\"}', '2021-01-12 17:07:05', NULL),
(301, '9550261360', 'order_place_admin', '{\"message\":\"31616c764b57303933353931\",\"type\":\"success\"}', '2021-01-12 17:07:49', NULL),
(302, '9949587496', 'order_place_user', '{\"message\":\"31616c764b57373138363434\",\"type\":\"success\"}', '2021-01-12 17:07:49', NULL),
(303, '8121572957', 'order_place_seller', '{\"message\":\"31616c764b57353038353837\",\"type\":\"success\"}', '2021-01-12 17:07:49', NULL),
(304, '8121572957', 'order_place_seller', '{\"message\":\"31616c764b32353032353832\",\"type\":\"success\"}', '2021-01-12 17:07:54', NULL),
(305, '0328566312', 'order_place_seller', '{\"message\":\"31616c764b35343237303932\",\"type\":\"success\"}', '2021-01-12 17:07:57', NULL),
(306, '9550261360', 'order_place_admin', '{\"message\":\"31616c764c34313433323039\",\"type\":\"success\"}', '2021-01-12 17:08:57', NULL),
(307, '9949587496', 'order_place_user', '{\"message\":\"31616c764c35353030303433\",\"type\":\"success\"}', '2021-01-12 17:08:57', NULL),
(308, '8121572957', 'order_place_seller', '{\"message\":\"31616c764c35323531353237\",\"type\":\"success\"}', '2021-01-12 17:08:57', NULL),
(309, '8121572957', 'order_place_seller', '{\"message\":\"31616c764d61323234383331\",\"type\":\"success\"}', '2021-01-12 17:09:01', NULL),
(310, '0328566312', 'order_place_seller', '{\"message\":\"31616c764d64333430373537\",\"type\":\"success\"}', '2021-01-12 17:09:04', NULL),
(311, '9550261360', 'order_place_admin', '{\"message\":\"31616c765443373038363934\",\"type\":\"success\"}', '2021-01-12 17:16:29', NULL),
(312, '9949587496', 'order_place_user', '{\"message\":\"31616c765443383439373939\",\"type\":\"success\"}', '2021-01-12 17:16:29', NULL),
(313, '8121572957', 'order_place_seller', '{\"message\":\"31616c765443383030393935\",\"type\":\"success\"}', '2021-01-12 17:16:29', NULL),
(314, '8121572957', 'order_place_seller', '{\"message\":\"31616c765448303031313939\",\"type\":\"success\"}', '2021-01-12 17:16:34', NULL),
(315, '0328566312', 'order_place_seller', '{\"message\":\"31616c76544c383537353438\",\"type\":\"success\"}', '2021-01-12 17:16:38', NULL),
(316, '9550261360', 'order_place_admin', '{\"message\":\"31616c765573343236313831\",\"type\":\"success\"}', '2021-01-12 17:17:19', NULL),
(317, '9949587496', 'order_place_user', '{\"message\":\"31616c765574353736303332\",\"type\":\"success\"}', '2021-01-12 17:17:20', NULL),
(318, '8121572957', 'order_place_seller', '{\"message\":\"31616c765574393438343833\",\"type\":\"success\"}', '2021-01-12 17:17:20', NULL),
(319, '8121572957', 'order_place_seller', '{\"message\":\"31616c765578333935313132\",\"type\":\"success\"}', '2021-01-12 17:17:24', NULL),
(320, '0328566312', 'order_place_seller', '{\"message\":\"31616c765541353331383336\",\"type\":\"success\"}', '2021-01-12 17:17:27', NULL),
(321, '9550261360', 'order_place_admin', '{\"message\":\"31616c76586d313530393136\",\"type\":\"success\"}', '2021-01-12 17:20:13', NULL),
(322, '9949587496', 'order_place_user', '{\"message\":\"31616c76586e343338303135\",\"type\":\"success\"}', '2021-01-12 17:20:14', NULL),
(323, '8121572957', 'order_place_seller', '{\"message\":\"31616c76586e313938383239\",\"type\":\"success\"}', '2021-01-12 17:20:14', NULL),
(324, '8121572957', 'order_place_seller', '{\"message\":\"31616c765872383035343138\",\"type\":\"success\"}', '2021-01-12 17:20:18', NULL),
(325, '0328566312', 'order_place_seller', '{\"message\":\"31616c765875383338343339\",\"type\":\"success\"}', '2021-01-12 17:20:21', NULL),
(326, '9550261360', 'order_place_admin', '{\"message\":\"31616c765a70363930313938\",\"type\":\"success\"}', '2021-01-12 17:22:16', NULL),
(327, '9949587496', 'order_place_user', '{\"message\":\"31616c765a71303533383235\",\"type\":\"success\"}', '2021-01-12 17:22:17', NULL),
(328, '8121572957', 'order_place_seller', '{\"message\":\"31616c765a71373733373435\",\"type\":\"success\"}', '2021-01-12 17:22:17', NULL),
(329, '8121572957', 'order_place_seller', '{\"message\":\"31616c765a75383331353437\",\"type\":\"success\"}', '2021-01-12 17:22:21', NULL),
(330, '0328566312', 'order_place_seller', '{\"message\":\"31616c765a78373034323539\",\"type\":\"success\"}', '2021-01-12 17:22:24', NULL),
(331, '9550261360', 'order_place_admin', '{\"message\":\"31616c763361303638313933\",\"type\":\"success\"}', '2021-01-12 17:25:01', NULL),
(332, '9949587496', 'order_place_user', '{\"message\":\"31616c763361363638383730\",\"type\":\"success\"}', '2021-01-12 17:25:01', NULL),
(333, '8121572957', 'order_place_seller', '{\"message\":\"31616c763362393333353536\",\"type\":\"success\"}', '2021-01-12 17:25:02', NULL),
(334, '8121572957', 'order_place_seller', '{\"message\":\"31616c763365333431313633\",\"type\":\"success\"}', '2021-01-12 17:25:05', NULL),
(335, '0328566312', 'order_place_seller', '{\"message\":\"31616c763369333338313336\",\"type\":\"success\"}', '2021-01-12 17:25:09', NULL),
(336, '9949587496', 'login_otp', '{\"message\":\"31616c77774b393534333130\",\"type\":\"success\"}', '2021-01-12 17:53:37', NULL),
(337, '9949587496', 'login_otp', '{\"message\":\"31616c777861353135373234\",\"type\":\"success\"}', '2021-01-12 17:54:01', NULL),
(338, '9949587496', 'login_otp', '{\"message\":\"31616d6a6d67393032333433\",\"type\":\"success\"}', '2021-01-13 04:43:07', NULL),
(339, '9959489674', 'login_otp', '{\"message\":\"31616d6c6e52333732303432\",\"type\":\"success\"}', '2021-01-13 06:44:44', NULL),
(340, '9949587496', 'login_otp', '{\"message\":\"31616d6c706e363039343034\",\"type\":\"success\"}', '2021-01-13 06:46:14', NULL),
(341, '9550261360', 'login_otp', '{\"message\":\"31616d6d4b61383433313632\",\"type\":\"success\"}', '2021-01-13 08:07:01', NULL),
(342, '9550261360', 'login_otp', '{\"message\":\"31616d6d4b4a393731323532\",\"type\":\"success\"}', '2021-01-13 08:07:36', NULL),
(343, '9949587496', 'login_otp', '{\"message\":\"31616d6e4477373139333438\",\"type\":\"success\"}', '2021-01-13 09:00:23', NULL),
(344, '8989878798', 'verify_mobile', '{\"message\":\"31616d6e4e31323637353037\",\"type\":\"success\"}', '2021-01-13 09:10:53', NULL),
(345, '9949587496', 'login_otp', '{\"message\":\"31616d6e5151343831353737\",\"type\":\"success\"}', '2021-01-13 09:13:43', NULL),
(346, '9949587496', 'login_otp', '{\"message\":\"31616d6f6554363230393838\",\"type\":\"success\"}', '2021-01-13 09:35:46', NULL),
(347, '8008314842', 'login_otp', '{\"message\":\"31616d704a7a333930383535\",\"type\":\"success\"}', '2021-01-13 11:06:26', NULL),
(348, '9949587496', 'login_otp', '{\"message\":\"31616d704b52383236373733\",\"type\":\"success\"}', '2021-01-13 11:07:44', NULL),
(349, '8520021901', 'verify_mobile', '{\"message\":\"31616d703644393033393936\",\"type\":\"success\"}', '2021-01-13 11:28:30', NULL),
(350, '8987878789', 'verify_mobile', '{\"message\":\"31616d71306b323739363732\",\"type\":\"success\"}', '2021-01-13 11:30:11', NULL),
(351, '9875678999', 'verify_mobile', '{\"message\":\"31616d716554333234393631\",\"type\":\"success\"}', '2021-01-13 11:35:46', NULL),
(352, '9849656569', 'verify_mobile', '{\"message\":\"31616d71666e363130393933\",\"type\":\"success\"}', '2021-01-13 11:36:14', NULL),
(353, '9849656569', 'verify_mobile', '{\"message\":\"31616d71676d353336313134\",\"type\":\"success\"}', '2021-01-13 11:37:13', NULL),
(354, '9849656569', 'verify_mobile', '{\"message\":\"31616d716754323330353637\",\"type\":\"success\"}', '2021-01-13 11:37:46', NULL),
(355, '9849656569', 'verify_mobile', '{\"message\":\"31616d716932393236383931\",\"type\":\"success\"}', '2021-01-13 11:39:54', NULL),
(356, '9849656569', 'verify_mobile', '{\"message\":\"31616d716a77343935363033\",\"type\":\"success\"}', '2021-01-13 11:40:23', NULL),
(357, '8008831484', 'login_otp', '{\"message\":\"31616d725168373231353135\",\"type\":\"success\"}', '2021-01-13 13:13:08', NULL),
(358, '8008831484', 'login_otp', '{\"message\":\"31616d725261373739343339\",\"type\":\"success\"}', '2021-01-13 13:14:01', NULL),
(359, '8008831484', 'login_otp', '{\"message\":\"31616d72526b313030343738\",\"type\":\"success\"}', '2021-01-13 13:14:11', NULL),
(360, '8008314842', 'login_otp', '{\"message\":\"31616d72524a333431313737\",\"type\":\"success\"}', '2021-01-13 13:14:36', NULL),
(361, '8008314842', 'login_otp', '{\"message\":\"31616d72554a353838343639\",\"type\":\"success\"}', '2021-01-13 13:17:36', NULL),
(362, '9550261360', 'login_otp', '{\"message\":\"31616d723462383832353233\",\"type\":\"success\"}', '2021-01-13 13:26:02', NULL),
(363, '9550261360', 'login_otp', '{\"message\":\"31616d723672343339333331\",\"type\":\"success\"}', '2021-01-13 13:28:18', NULL),
(364, '8008314842', 'login_otp', '{\"message\":\"31616e6a4a42353131323637\",\"type\":\"success\"}', '2021-01-14 05:06:28', NULL),
(365, '7036221419', 'login_otp', '{\"message\":\"31616e6a4b4d393130323636\",\"type\":\"success\"}', '2021-01-14 05:07:39', NULL),
(366, '7036221419', 'login_otp', '{\"message\":\"31616e6c4c34323733343731\",\"type\":\"success\"}', '2021-01-14 07:08:56', NULL),
(367, '7036221419', 'login_otp', '{\"message\":\"31616e6e5157323839323637\",\"type\":\"success\"}', '2021-01-14 09:13:49', NULL),
(368, '7036221419', 'login_otp', '{\"message\":\"31616e705242343532383331\",\"type\":\"success\"}', '2021-01-14 11:14:28', NULL),
(369, '9550261360', 'order_place_admin', '{\"message\":\"31616e703679393837313338\",\"type\":\"success\"}', '2021-01-14 11:28:25', NULL),
(370, '7036221419', 'order_place_user', '{\"message\":\"31616e70367a323630313439\",\"type\":\"success\"}', '2021-01-14 11:28:26', NULL),
(371, '0328566312', 'order_place_seller', '{\"message\":\"31616e70367a363033373435\",\"type\":\"success\"}', '2021-01-14 11:28:26', NULL),
(372, '0328566312', 'order_place_seller', '{\"message\":\"31616e703645313337333032\",\"type\":\"success\"}', '2021-01-14 11:28:31', NULL),
(373, '0328566312', 'order_place_seller', '{\"message\":\"31616e703649363932383135\",\"type\":\"success\"}', '2021-01-14 11:28:35', NULL),
(374, '7036221419', 'login_otp', '{\"message\":\"31616e703735313732353638\",\"type\":\"success\"}', '2021-01-14 11:29:57', NULL),
(375, '9948839362', 'login_otp', '{\"message\":\"31616f6b6335353434313530\",\"type\":\"success\"}', '2021-01-15 05:33:57', NULL),
(376, '7036221419', 'login_otp', '{\"message\":\"31616f6b6a57383734333530\",\"type\":\"success\"}', '2021-01-15 05:40:49', NULL);
INSERT INTO `sms_logs` (`id`, `mobile`, `action`, `response_data`, `created_at`, `updated_at`) VALUES
(377, '9550261360', 'order_place_admin', '{\"message\":\"31616f6c6541383031373939\",\"type\":\"success\"}', '2021-01-15 06:35:27', NULL),
(378, '7036221419', 'order_place_user', '{\"message\":\"31616f6c6541323538393039\",\"type\":\"success\"}', '2021-01-15 06:35:27', NULL),
(379, '0328566312', 'order_place_seller', '{\"message\":\"31616f6c6542353833313731\",\"type\":\"success\"}', '2021-01-15 06:35:28', NULL),
(380, '9550261360', 'login_otp', '{\"message\":\"31616f6c6648343130383038\",\"type\":\"success\"}', '2021-01-15 06:36:34', NULL),
(381, '9948839362', 'login_otp', '{\"message\":\"31616f6d6457333237303238\",\"type\":\"success\"}', '2021-01-15 07:34:49', NULL),
(382, '7036221419', 'login_otp', '{\"message\":\"31616f6d6f69323432363034\",\"type\":\"success\"}', '2021-01-15 07:45:09', NULL),
(383, '9550261360', 'order_place_admin', '{\"message\":\"31616f6d7379313835353930\",\"type\":\"success\"}', '2021-01-15 07:49:25', NULL),
(384, '7036221419', 'order_place_user', '{\"message\":\"31616f6d7379373233383933\",\"type\":\"success\"}', '2021-01-15 07:49:25', NULL),
(385, '0328566312', 'order_place_seller', '{\"message\":\"31616f6d737a363530353130\",\"type\":\"success\"}', '2021-01-15 07:49:26', NULL),
(386, '9550261360', 'order_place_admin', '{\"message\":\"31616f6d484c393336353932\",\"type\":\"success\"}', '2021-01-15 08:04:38', NULL),
(387, '7036221419', 'order_place_user', '{\"message\":\"31616f6d484c353532393536\",\"type\":\"success\"}', '2021-01-15 08:04:38', NULL),
(388, '0328566312', 'order_place_seller', '{\"message\":\"31616f6d484c333238343035\",\"type\":\"success\"}', '2021-01-15 08:04:38', NULL),
(389, '9948839362', 'login_otp', '{\"message\":\"31616f6f7262353330333632\",\"type\":\"success\"}', '2021-01-15 09:48:02', NULL),
(390, '7036221419', 'login_otp', '{\"message\":\"31616f707549313431333436\",\"type\":\"success\"}', '2021-01-15 10:51:35', NULL),
(391, '9550261360', 'order_place_admin', '{\"message\":\"31616f707a70333336333031\",\"type\":\"success\"}', '2021-01-15 10:56:16', NULL),
(392, '7036221419', 'order_place_user', '{\"message\":\"31616f707a71323234363332\",\"type\":\"success\"}', '2021-01-15 10:56:17', NULL),
(393, '0328566312', 'order_place_seller', '{\"message\":\"31616f707a71313133313232\",\"type\":\"success\"}', '2021-01-15 10:56:17', NULL),
(394, '7036221419', 'login_otp', '{\"message\":\"31616f704766373833303834\",\"type\":\"success\"}', '2021-01-15 11:03:06', NULL),
(395, '9550261360', 'order_place_admin', '{\"message\":\"31616f70486f353134303532\",\"type\":\"success\"}', '2021-01-15 11:04:15', NULL),
(396, '7036221419', 'order_place_user', '{\"message\":\"31616f70486f303536343839\",\"type\":\"success\"}', '2021-01-15 11:04:15', NULL),
(397, '0328566312', 'order_place_seller', '{\"message\":\"31616f704870383934343636\",\"type\":\"success\"}', '2021-01-15 11:04:16', NULL),
(398, '0328566312', 'order_place_seller', '{\"message\":\"31616f704874353533373537\",\"type\":\"success\"}', '2021-01-15 11:04:20', NULL),
(399, '9550261360', 'order_place_admin', '{\"message\":\"31616f704c75393230373039\",\"type\":\"success\"}', '2021-01-15 11:08:21', NULL),
(400, '7036221419', 'order_place_user', '{\"message\":\"31616f704c75343339333232\",\"type\":\"success\"}', '2021-01-15 11:08:21', NULL),
(401, '0328566312', 'order_place_seller', '{\"message\":\"31616f704c76393633343033\",\"type\":\"success\"}', '2021-01-15 11:08:22', NULL),
(402, '0328566312', 'order_place_seller', '{\"message\":\"31616f704c7a343539383731\",\"type\":\"success\"}', '2021-01-15 11:08:26', NULL),
(403, '9550261360', 'order_place_admin', '{\"message\":\"31616f705143363537353339\",\"type\":\"success\"}', '2021-01-15 11:13:29', NULL),
(404, '7036221419', 'order_place_user', '{\"message\":\"31616f705143393031343430\",\"type\":\"success\"}', '2021-01-15 11:13:29', NULL),
(405, '0328566312', 'order_place_seller', '{\"message\":\"31616f705144343339383837\",\"type\":\"success\"}', '2021-01-15 11:13:30', NULL),
(406, '0328566312', 'order_place_seller', '{\"message\":\"31616f705148353033333433\",\"type\":\"success\"}', '2021-01-15 11:13:34', NULL),
(407, '9550261360', 'order_place_admin', '{\"message\":\"31616f71714b333032303434\",\"type\":\"success\"}', '2021-01-15 11:47:37', NULL),
(408, '7036221419', 'order_place_user', '{\"message\":\"31616f71714b353035313434\",\"type\":\"success\"}', '2021-01-15 11:47:37', NULL),
(409, '0328566312', 'order_place_seller', '{\"message\":\"31616f71714b383437383734\",\"type\":\"success\"}', '2021-01-15 11:47:37', NULL),
(410, '9550261360', 'order_place_admin', '{\"message\":\"31616f717276373938383330\",\"type\":\"success\"}', '2021-01-15 11:48:22', NULL),
(411, '7036221419', 'order_place_user', '{\"message\":\"31616f717277333731313131\",\"type\":\"success\"}', '2021-01-15 11:48:23', NULL),
(412, '0328566312', 'order_place_seller', '{\"message\":\"31616f717277353033333538\",\"type\":\"success\"}', '2021-01-15 11:48:23', NULL),
(413, '9550261360', 'login_otp', '{\"message\":\"31616f726775383834313631\",\"type\":\"success\"}', '2021-01-15 12:37:21', NULL),
(414, '7036221419', 'login_otp', '{\"message\":\"31616f727242303331343939\",\"type\":\"success\"}', '2021-01-15 12:48:28', NULL),
(415, '9550261360', 'login_otp', '{\"message\":\"31616f727a66353331333039\",\"type\":\"success\"}', '2021-01-15 12:56:06', NULL),
(416, '9550261360', 'login_otp', '{\"message\":\"31616f727a44383137393032\",\"type\":\"success\"}', '2021-01-15 12:56:30', NULL),
(417, '9550261360', 'login_otp', '{\"message\":\"31616f727a56363332323138\",\"type\":\"success\"}', '2021-01-15 12:56:48', NULL),
(418, '9948839362', 'login_otp', '{\"message\":\"31616f734837323836323230\",\"type\":\"success\"}', '2021-01-15 14:04:59', NULL),
(419, '7396043338', 'login_otp', '{\"message\":\"31616f734943353537353031\",\"type\":\"success\"}', '2021-01-15 14:05:29', NULL),
(420, '9948839362', 'login_otp', '{\"message\":\"3161706a4557323734383732\",\"type\":\"success\"}', '2021-01-16 05:01:49', NULL),
(421, '9948839362', 'login_otp', '{\"message\":\"3161706b5250373833373635\",\"type\":\"success\"}', '2021-01-16 06:14:42', NULL),
(422, '9948839362', 'login_otp', '{\"message\":\"3161706e4b46333739393130\",\"type\":\"success\"}', '2021-01-16 09:07:32', NULL),
(423, '7396043338', 'login_otp', '{\"message\":\"316171737357343232313430\",\"type\":\"success\"}', '2021-01-17 13:49:49', NULL),
(424, '9948839362', 'login_otp', '{\"message\":\"316171757450393037363839\",\"type\":\"success\"}', '2021-01-17 15:50:43', NULL),
(425, '7396043338', 'login_otp', '{\"message\":\"31617177756f393937373035\",\"type\":\"success\"}', '2021-01-17 17:51:15', NULL),
(426, '9948839362', 'login_otp', '{\"message\":\"3161726a5a59373630313539\",\"type\":\"success\"}', '2021-01-18 05:22:52', NULL),
(427, '9949587496', 'login_otp', '{\"message\":\"3161726b6b36333932343530\",\"type\":\"success\"}', '2021-01-18 05:41:58', NULL),
(428, '9550261360', 'order_place_admin', '{\"message\":\"3161726b6e69313230303637\",\"type\":\"success\"}', '2021-01-18 05:44:09', NULL),
(429, '9949587496', 'order_place_user', '{\"message\":\"3161726b6e6a393532393037\",\"type\":\"success\"}', '2021-01-18 05:44:10', NULL),
(430, '0328566312', 'order_place_seller', '{\"message\":\"3161726b6e6a353032333431\",\"type\":\"success\"}', '2021-01-18 05:44:10', NULL),
(431, '9550261360', 'order_place_admin', '{\"message\":\"3161726b7579323438373635\",\"type\":\"success\"}', '2021-01-18 05:51:25', NULL),
(432, '9949587496', 'order_place_user', '{\"message\":\"3161726b7579383335373130\",\"type\":\"success\"}', '2021-01-18 05:51:25', NULL),
(433, '0328566312', 'order_place_seller', '{\"message\":\"3161726b7579323533303736\",\"type\":\"success\"}', '2021-01-18 05:51:25', NULL),
(434, '1234567890', 'verify_mobile', '{\"message\":\"3161726b434c383935313136\",\"type\":\"success\"}', '2021-01-18 05:59:38', NULL),
(435, '9948839362', 'login_otp', '{\"message\":\"3161726b4a78353930373935\",\"type\":\"success\"}', '2021-01-18 06:06:24', NULL),
(436, '9948839362', 'login_otp', '{\"message\":\"3161726b4a54333238303934\",\"type\":\"success\"}', '2021-01-18 06:06:46', NULL),
(437, '9948839362', 'login_otp', '{\"message\":\"3161726b4b46303831313434\",\"type\":\"success\"}', '2021-01-18 06:07:32', NULL),
(438, '8328566312', 'verify_mobile', '{\"message\":\"3161726b5843353330333635\",\"type\":\"success\"}', '2021-01-18 06:20:29', NULL),
(439, '9949587466', 'login_otp', '{\"message\":\"3161726c6f7a323234373833\",\"type\":\"success\"}', '2021-01-18 06:45:26', NULL),
(440, '9949587496', 'login_otp', '{\"message\":\"3161726c6f4e313738323631\",\"type\":\"success\"}', '2021-01-18 06:45:40', NULL),
(441, '9948839362', 'login_otp', '{\"message\":\"3161726c334f323335373234\",\"type\":\"success\"}', '2021-01-18 07:25:41', NULL),
(442, '9550261360', 'order_place_admin', '{\"message\":\"3161726c3654393639393630\",\"type\":\"success\"}', '2021-01-18 07:28:46', NULL),
(443, '9949587496', 'order_place_user', '{\"message\":\"3161726c3654373131333335\",\"type\":\"success\"}', '2021-01-18 07:28:46', NULL),
(444, '0328566312', 'order_place_seller', '{\"message\":\"3161726c3654323832333836\",\"type\":\"success\"}', '2021-01-18 07:28:47', NULL),
(445, '8849587496', 'login_otp', '{\"message\":\"3161726e6a51343932313835\",\"type\":\"success\"}', '2021-01-18 08:40:43', NULL),
(446, '8849587496', 'login_otp', '{\"message\":\"3161726e6b50313134373939\",\"type\":\"success\"}', '2021-01-18 08:41:42', NULL),
(447, '9949587496', 'login_otp', '{\"message\":\"3161726e6e6b353436333437\",\"type\":\"success\"}', '2021-01-18 08:44:11', NULL),
(448, '9949587496', 'login_otp', '{\"message\":\"3161726f6935333434303935\",\"type\":\"success\"}', '2021-01-18 09:39:57', NULL),
(449, '9676468686', 'login_otp', '{\"message\":\"316172717348303037323730\",\"type\":\"success\"}', '2021-01-18 11:49:34', NULL),
(450, '9676468686', 'login_otp', '{\"message\":\"316172717462323734373837\",\"type\":\"success\"}', '2021-01-18 11:50:02', NULL),
(451, '9949587496', 'login_otp', '{\"message\":\"316172717866303031383334\",\"type\":\"success\"}', '2021-01-18 11:54:07', NULL),
(452, '9949587496', 'login_otp', '{\"message\":\"316172714d70323830303538\",\"type\":\"success\"}', '2021-01-18 12:09:16', NULL),
(453, '9949587496', 'login_otp', '{\"message\":\"316172715577393735383131\",\"type\":\"success\"}', '2021-01-18 12:17:23', NULL),
(454, '9550261360', 'login_otp', '{\"message\":\"316172725a67383831333135\",\"type\":\"success\"}', '2021-01-18 13:22:07', NULL),
(455, '9949587496', 'login_otp', '{\"message\":\"31617274307a383331343534\",\"type\":\"success\"}', '2021-01-18 14:30:26', NULL),
(456, '9676468686', 'login_otp', '{\"message\":\"316172746235363333353030\",\"type\":\"success\"}', '2021-01-18 14:32:58', NULL),
(457, '9885428628', 'login_otp', '{\"message\":\"316172746334383436303432\",\"type\":\"success\"}', '2021-01-18 14:33:56', NULL),
(458, '9550261360', 'login_otp', '{\"message\":\"3161736a6d58333733313732\",\"type\":\"success\"}', '2021-01-19 04:43:50', NULL),
(459, '9550261360', 'login_otp', '{\"message\":\"3161736a6e5a373935353033\",\"type\":\"success\"}', '2021-01-19 04:44:52', NULL),
(460, '7396043338', 'login_otp', '{\"message\":\"3161736a415a363136383131\",\"type\":\"success\"}', '2021-01-19 04:57:52', NULL),
(461, '1234567899', 'verify_mobile', '{\"message\":\"3161736c4247303832323933\",\"type\":\"success\"}', '2021-01-19 06:58:33', NULL),
(462, '9290573910', 'login_otp', '{\"message\":\"3161736f3342383438313036\",\"type\":\"success\"}', '2021-01-19 10:25:28', NULL),
(463, '9618629334', 'login_otp', '{\"message\":\"3161736f3453323731343339\",\"type\":\"success\"}', '2021-01-19 10:26:45', NULL),
(464, '9550261360', 'login_otp', '{\"message\":\"316173704950363839393030\",\"type\":\"success\"}', '2021-01-19 11:05:42', NULL),
(465, '9550261360', 'login_otp', '{\"message\":\"316173704a64343033333133\",\"type\":\"success\"}', '2021-01-19 11:06:04', NULL),
(466, '9949587496', 'login_otp', '{\"message\":\"316173704b67383531373331\",\"type\":\"success\"}', '2021-01-19 11:07:07', NULL),
(467, '7382207022', 'verify_mobile', '{\"message\":\"31617370536f353737363431\",\"type\":\"success\"}', '2021-01-19 11:15:15', NULL),
(468, '9949587496', 'login_otp', '{\"message\":\"316173736572363630393835\",\"type\":\"success\"}', '2021-01-19 13:35:18', NULL),
(469, '9949587496', 'login_otp', '{\"message\":\"316173736664363837313932\",\"type\":\"success\"}', '2021-01-19 13:36:04', NULL),
(470, '9949587496', 'login_otp', '{\"message\":\"316173736643383332363538\",\"type\":\"success\"}', '2021-01-19 13:36:29', NULL),
(471, '9949587496', 'login_otp', '{\"message\":\"316173736746383936383739\",\"type\":\"success\"}', '2021-01-19 13:37:32', NULL),
(472, '9949587496', 'login_otp', '{\"message\":\"316173736848343539343332\",\"type\":\"success\"}', '2021-01-19 13:38:34', NULL),
(473, '9948839362', 'login_otp', '{\"message\":\"316173766846323032343639\",\"type\":\"success\"}', '2021-01-19 16:38:32', NULL),
(474, '9949587496', 'login_otp', '{\"message\":\"3161746a3554343234333938\",\"type\":\"success\"}', '2021-01-20 05:27:46', NULL),
(475, '9949587496', 'login_otp', '{\"message\":\"3161746a364c303430303437\",\"type\":\"success\"}', '2021-01-20 05:28:38', NULL),
(476, '9949587496', 'login_otp', '{\"message\":\"3161746a3768363237353730\",\"type\":\"success\"}', '2021-01-20 05:29:08', NULL),
(477, '9550261360', 'order_place_admin', '{\"message\":\"3161746b616e393839383337\",\"type\":\"success\"}', '2021-01-20 05:31:14', NULL),
(478, '9949587496', 'order_place_user', '{\"message\":\"3161746b616e343431303131\",\"type\":\"success\"}', '2021-01-20 05:31:14', NULL),
(479, '0328566312', 'order_place_seller', '{\"message\":\"3161746b616e313538333835\",\"type\":\"success\"}', '2021-01-20 05:31:14', NULL),
(480, '9949587496', 'login_otp', '{\"message\":\"3161746b7058393534363939\",\"type\":\"success\"}', '2021-01-20 05:46:50', NULL),
(481, '9550261360', 'login_otp', '{\"message\":\"3161746d6849323833353736\",\"type\":\"success\"}', '2021-01-20 07:38:35', NULL),
(482, '9550261360', 'login_otp', '{\"message\":\"3161746d6958303737313736\",\"type\":\"success\"}', '2021-01-20 07:39:50', NULL),
(483, '9550261360', 'login_otp', '{\"message\":\"3161746d6b67323839343738\",\"type\":\"success\"}', '2021-01-20 07:41:08', NULL),
(484, '9550261360', 'login_otp', '{\"message\":\"3161746d6b72313138323132\",\"type\":\"success\"}', '2021-01-20 07:41:18', NULL),
(485, '9550261360', 'login_otp', '{\"message\":\"3161746d6c6b323430333036\",\"type\":\"success\"}', '2021-01-20 07:42:11', NULL),
(486, '9550261360', 'order_place_admin', '{\"message\":\"3161746d6c7a353033363931\",\"type\":\"success\"}', '2021-01-20 07:42:26', NULL),
(487, '9550261360', 'order_place_user', '{\"message\":\"3161746d6c41323235333237\",\"type\":\"success\"}', '2021-01-20 07:42:27', NULL),
(488, '0328566312', 'order_place_seller', '{\"message\":\"3161746d6c41353132303931\",\"type\":\"success\"}', '2021-01-20 07:42:27', NULL),
(489, '9949587496', 'login_otp', '{\"message\":\"3161746d7131303439383834\",\"type\":\"success\"}', '2021-01-20 07:47:53', NULL),
(490, '9949587496', 'login_otp', '{\"message\":\"3161746d7247303431303032\",\"type\":\"success\"}', '2021-01-20 07:48:33', NULL),
(491, '9550261360', 'login_otp', '{\"message\":\"3161746d7350323537373932\",\"type\":\"success\"}', '2021-01-20 07:49:42', NULL),
(492, '9550261360', 'login_otp', '{\"message\":\"3161746d764c383339383934\",\"type\":\"success\"}', '2021-01-20 07:52:38', NULL),
(493, '9550261360', 'login_otp', '{\"message\":\"3161746e4b4e393034343536\",\"type\":\"success\"}', '2021-01-20 09:07:40', NULL),
(494, '9618629334', 'verify_mobile', '{\"message\":\"316174703547333335333633\",\"type\":\"success\"}', '2021-01-20 11:27:34', NULL),
(495, '9676468686', 'login_otp', '{\"message\":\"316174736842363734393437\",\"type\":\"success\"}', '2021-01-20 13:38:28', NULL),
(496, '9676468686', 'login_otp', '{\"message\":\"316174736834343037363535\",\"type\":\"success\"}', '2021-01-20 13:38:56', NULL),
(497, '9676468686', 'login_otp', '{\"message\":\"316174736979353535353033\",\"type\":\"success\"}', '2021-01-20 13:39:25', NULL),
(498, '9550261360', 'login_otp', '{\"message\":\"316174736a6c313239313731\",\"type\":\"success\"}', '2021-01-20 13:40:12', NULL),
(499, '9550261360', 'login_otp', '{\"message\":\"316174736b62363730313434\",\"type\":\"success\"}', '2021-01-20 13:41:02', NULL),
(500, '9549588439', 'login_otp', '{\"message\":\"31617473455a323733323337\",\"type\":\"success\"}', '2021-01-20 14:01:52', NULL),
(501, '9618629334', 'verify_mobile', '{\"message\":\"3161756a536b373631313432\",\"type\":\"success\"}', '2021-01-21 05:15:11', NULL),
(502, '9618629334', 'verify_mobile', '{\"message\":\"3161756b4b67393031313636\",\"type\":\"success\"}', '2021-01-21 06:07:07', NULL),
(503, '9550261360', 'login_otp', '{\"message\":\"3161756c6454323332323833\",\"type\":\"success\"}', '2021-01-21 06:34:46', NULL),
(504, '9676313918', 'verify_mobile', '{\"message\":\"3161756c7169313735333838\",\"type\":\"success\"}', '2021-01-21 06:47:09', NULL),
(505, '7396043338', 'login_otp', '{\"message\":\"3161756c7246393736373030\",\"type\":\"success\"}', '2021-01-21 06:48:32', NULL),
(506, '7780361239', 'login_otp', '{\"message\":\"3161756d736a363538363337\",\"type\":\"success\"}', '2021-01-21 07:49:10', NULL),
(507, '7780361239', 'login_otp', '{\"message\":\"3161756d755a313439363534\",\"type\":\"success\"}', '2021-01-21 07:51:52', NULL),
(508, '9676468686', 'login_otp', '{\"message\":\"3161756e3059363133323337\",\"type\":\"success\"}', '2021-01-21 08:30:51', NULL),
(509, '9948839362', 'login_otp', '{\"message\":\"3161756e7564353234363537\",\"type\":\"success\"}', '2021-01-21 08:51:04', NULL),
(510, '9948839362', 'login_otp', '{\"message\":\"3161756e754e383031343930\",\"type\":\"success\"}', '2021-01-21 08:51:40', NULL),
(511, '9949587496', 'login_otp', '{\"message\":\"3161756f6746373336323631\",\"type\":\"success\"}', '2021-01-21 09:37:32', NULL),
(512, '9550261360', 'login_otp', '{\"message\":\"3161756f7333323330313836\",\"type\":\"success\"}', '2021-01-21 09:49:55', NULL),
(513, '7780361239', 'login_otp', '{\"message\":\"3161756f4f50363532373338\",\"type\":\"success\"}', '2021-01-21 10:11:42', NULL),
(514, '9550261360', 'login_otp', '{\"message\":\"3161756f5272363530323334\",\"type\":\"success\"}', '2021-01-21 10:14:18', NULL),
(515, '7780361239', 'login_otp', '{\"message\":\"3161756f5479383637303936\",\"type\":\"success\"}', '2021-01-21 10:16:25', NULL),
(516, '9550261360', 'login_otp', '{\"message\":\"3161756f5562313930343539\",\"type\":\"success\"}', '2021-01-21 10:17:02', NULL),
(517, '9949587496', 'login_otp', '{\"message\":\"3161756f5667323431343531\",\"type\":\"success\"}', '2021-01-21 10:18:07', NULL),
(518, '9290573910', 'login_otp', '{\"message\":\"3161756f597a373539323332\",\"type\":\"success\"}', '2021-01-21 10:21:26', NULL),
(519, '9949587496', 'login_otp', '{\"message\":\"3161756f336e363335323535\",\"type\":\"success\"}', '2021-01-21 10:25:14', NULL),
(520, '7780361239', 'login_otp', '{\"message\":\"316175706169373933323934\",\"type\":\"success\"}', '2021-01-21 10:31:09', NULL),
(521, '9550261360', 'order_place_admin', '{\"message\":\"316175707678353732363030\",\"type\":\"success\"}', '2021-01-21 10:52:24', NULL),
(522, '9290573910', 'order_place_user', '{\"message\":\"316175707679343436353733\",\"type\":\"success\"}', '2021-01-21 10:52:25', NULL),
(523, '9618629334', 'order_place_seller', '{\"message\":\"316175707679343332373339\",\"type\":\"success\"}', '2021-01-21 10:52:25', NULL),
(524, '9290573910', 'login_otp', '{\"message\":\"316175707763343931363934\",\"type\":\"success\"}', '2021-01-21 10:53:03', NULL),
(525, '9290573910', 'login_otp', '{\"message\":\"316175707742313138383333\",\"type\":\"success\"}', '2021-01-21 10:53:28', NULL),
(526, '9290573910', 'login_otp', '{\"message\":\"31617570786e373431333437\",\"type\":\"success\"}', '2021-01-21 10:54:14', NULL),
(527, '8328566312', 'login_otp', '{\"message\":\"316175707a44393833353837\",\"type\":\"success\"}', '2021-01-21 10:56:30', NULL),
(528, '7780361239', 'login_otp', '{\"message\":\"316175704163383631353736\",\"type\":\"success\"}', '2021-01-21 10:57:03', NULL),
(529, '9550261360', 'order_place_admin', '{\"message\":\"316175704149353633373834\",\"type\":\"success\"}', '2021-01-21 10:57:35', NULL),
(530, '7780361239', 'order_place_user', '{\"message\":\"316175704149363731313734\",\"type\":\"success\"}', '2021-01-21 10:57:35', NULL),
(531, '9618629334', 'order_place_seller', '{\"message\":\"316175704149363630333730\",\"type\":\"success\"}', '2021-01-21 10:57:35', NULL),
(532, '9618629334', 'order_place_seller', '{\"message\":\"31617570414e313537313536\",\"type\":\"success\"}', '2021-01-21 10:57:40', NULL),
(533, '6290844051', 'login_otp', '{\"message\":\"316175704e72373830393533\",\"type\":\"success\"}', '2021-01-21 11:10:18', NULL),
(534, '6290844051', 'login_otp', '{\"message\":\"316175716643373736353333\",\"type\":\"success\"}', '2021-01-21 11:36:29', NULL),
(535, '9550261360', 'order_place_admin', '{\"message\":\"31617572616d333639363838\",\"type\":\"success\"}', '2021-01-21 12:31:13', NULL),
(536, '6290844051', 'order_place_user', '{\"message\":\"31617572616d353636383136\",\"type\":\"success\"}', '2021-01-21 12:31:13', NULL),
(537, '9618629334', 'order_place_seller', '{\"message\":\"31617572616d373137383237\",\"type\":\"success\"}', '2021-01-21 12:31:13', NULL),
(538, '9618629334', 'order_place_seller', '{\"message\":\"316175726172323633313739\",\"type\":\"success\"}', '2021-01-21 12:31:18', NULL),
(539, '9550261360', 'login_otp', '{\"message\":\"31617572714e333834363237\",\"type\":\"success\"}', '2021-01-21 12:47:40', NULL),
(540, '7382207022', 'verify_mobile', '{\"message\":\"316175724968353338343032\",\"type\":\"success\"}', '2021-01-21 13:05:08', NULL),
(541, '7780361239', 'login_otp', '{\"message\":\"316175724d74373139383738\",\"type\":\"success\"}', '2021-01-21 13:09:21', NULL),
(542, '8884273650', 'verify_mobile', '{\"message\":\"316175725666313438383434\",\"type\":\"success\"}', '2021-01-21 13:18:06', NULL),
(543, '7396043338', 'login_otp', '{\"message\":\"31617573335a373836383038\",\"type\":\"success\"}', '2021-01-21 14:25:52', NULL),
(544, '7780361239', 'login_otp', '{\"message\":\"316175746c65393136343832\",\"type\":\"success\"}', '2021-01-21 14:42:05', NULL),
(545, '8520021995', 'login_otp', '{\"message\":\"316175756167373533353239\",\"type\":\"success\"}', '2021-01-21 15:31:07', NULL),
(546, '8520021995', 'login_otp', '{\"message\":\"316175757643353332323339\",\"type\":\"success\"}', '2021-01-21 15:52:29', NULL),
(547, '7780361239', 'login_otp', '{\"message\":\"3161766a4e68393034313038\",\"type\":\"success\"}', '2021-01-22 05:10:08', NULL),
(548, '7780361239', 'login_otp', '{\"message\":\"3161766a5378373236303239\",\"type\":\"success\"}', '2021-01-22 05:15:24', NULL),
(549, '7396043338', 'login_otp', '{\"message\":\"3161766a5731373531353138\",\"type\":\"success\"}', '2021-01-22 05:19:53', NULL),
(550, '9550261360', 'login_otp', '{\"message\":\"3161766b4142393635333039\",\"type\":\"success\"}', '2021-01-22 05:57:28', NULL),
(551, '6290844051', 'login_otp', '{\"message\":\"3161766b4256383333383836\",\"type\":\"success\"}', '2021-01-22 05:58:48', NULL),
(552, '9948839362', 'login_otp', '{\"message\":\"3161766c3345343737323137\",\"type\":\"success\"}', '2021-01-22 07:25:31', NULL),
(553, '9948839362', 'login_otp', '{\"message\":\"3161766d6854383939343139\",\"type\":\"success\"}', '2021-01-22 07:38:46', NULL),
(554, '9948839362', 'login_otp', '{\"message\":\"3161766d6962393138353139\",\"type\":\"success\"}', '2021-01-22 07:39:02', NULL),
(555, '9948839362', 'login_otp', '{\"message\":\"3161766d6958373932393738\",\"type\":\"success\"}', '2021-01-22 07:39:50', NULL),
(556, '9948839362', 'login_otp', '{\"message\":\"3161766d4570373839323030\",\"type\":\"success\"}', '2021-01-22 08:01:16', NULL),
(557, '9550261360', 'order_place_admin', '{\"message\":\"3161766d4545393539303737\",\"type\":\"success\"}', '2021-01-22 08:01:31', NULL),
(558, '9948839362', 'order_place_user', '{\"message\":\"3161766d4545373939353439\",\"type\":\"success\"}', '2021-01-22 08:01:31', NULL),
(559, '9618629334', 'order_place_seller', '{\"message\":\"3161766d4546333432343536\",\"type\":\"success\"}', '2021-01-22 08:01:32', NULL),
(560, '7780361239', 'login_otp', '{\"message\":\"3161766d5078303136353931\",\"type\":\"success\"}', '2021-01-22 08:12:24', NULL),
(561, '7780361239', 'login_otp', '{\"message\":\"3161766d5433383937373833\",\"type\":\"success\"}', '2021-01-22 08:16:55', NULL),
(562, '7780361239', 'login_otp', '{\"message\":\"3161766d3452383437343332\",\"type\":\"success\"}', '2021-01-22 08:26:44', NULL),
(563, '7780361239', 'login_otp', '{\"message\":\"3161766d356c323334333232\",\"type\":\"success\"}', '2021-01-22 08:27:12', NULL),
(564, '7780361239', 'login_otp', '{\"message\":\"3161766d354d373139383534\",\"type\":\"success\"}', '2021-01-22 08:27:39', NULL),
(565, '7780361239', 'login_otp', '{\"message\":\"3161766d3775393331303132\",\"type\":\"success\"}', '2021-01-22 08:29:21', NULL),
(566, '7780361239', 'login_otp', '{\"message\":\"3161766e3045383538393039\",\"type\":\"success\"}', '2021-01-22 08:30:31', NULL),
(567, '9550261360', 'order_place_admin', '{\"message\":\"3161766e3056303436363337\",\"type\":\"success\"}', '2021-01-22 08:30:48', NULL),
(568, '7780361239', 'order_place_user', '{\"message\":\"3161766e3057393534343539\",\"type\":\"success\"}', '2021-01-22 08:30:49', NULL),
(569, '9618629334', 'order_place_seller', '{\"message\":\"3161766e3057353237373631\",\"type\":\"success\"}', '2021-01-22 08:30:49', NULL),
(570, '9618629334', 'order_place_seller', '{\"message\":\"3161766e3031363536363636\",\"type\":\"success\"}', '2021-01-22 08:30:53', NULL),
(571, '9550261360', 'login_otp', '{\"message\":\"3161766e6154343430323138\",\"type\":\"success\"}', '2021-01-22 08:31:46', NULL),
(572, '9550261360', 'login_otp', '{\"message\":\"3161766e6246333937383230\",\"type\":\"success\"}', '2021-01-22 08:32:32', NULL),
(573, '9550261360', 'order_place_admin', '{\"message\":\"3161766e6255363735353630\",\"type\":\"success\"}', '2021-01-22 08:32:47', NULL),
(574, '9550261360', 'order_place_user', '{\"message\":\"3161766e6255353331373336\",\"type\":\"success\"}', '2021-01-22 08:32:47', NULL),
(575, '9618629334', 'order_place_seller', '{\"message\":\"3161766e6255313431303939\",\"type\":\"success\"}', '2021-01-22 08:32:47', NULL),
(576, '9550261360', 'login_otp', '{\"message\":\"3161766e6554373938343439\",\"type\":\"success\"}', '2021-01-22 08:35:46', NULL),
(577, '9550261360', 'order_place_admin', '{\"message\":\"3161766e6661323737303932\",\"type\":\"success\"}', '2021-01-22 08:36:02', NULL),
(578, '9550261360', 'order_place_user', '{\"message\":\"3161766e6662363239383739\",\"type\":\"success\"}', '2021-01-22 08:36:02', NULL),
(579, '9618629334', 'order_place_seller', '{\"message\":\"3161766e6662303335353230\",\"type\":\"success\"}', '2021-01-22 08:36:02', NULL),
(580, '9550261360', 'order_place_admin', '{\"message\":\"3161766e6876343836393236\",\"type\":\"success\"}', '2021-01-22 08:38:22', NULL),
(581, '7780361239', 'order_place_user', '{\"message\":\"3161766e6876333834373339\",\"type\":\"success\"}', '2021-01-22 08:38:22', NULL),
(582, '9618629334', 'order_place_seller', '{\"message\":\"3161766e6876353539363133\",\"type\":\"success\"}', '2021-01-22 08:38:22', NULL),
(583, '9550261360', 'login_otp', '{\"message\":\"3161766e6846303931333038\",\"type\":\"success\"}', '2021-01-22 08:38:32', NULL),
(584, '9550261360', 'order_place_admin', '{\"message\":\"3161766e6854383533323635\",\"type\":\"success\"}', '2021-01-22 08:38:46', NULL),
(585, '9550261360', 'order_place_user', '{\"message\":\"3161766e6854353032323838\",\"type\":\"success\"}', '2021-01-22 08:38:46', NULL),
(586, '9618629334', 'order_place_seller', '{\"message\":\"3161766e6854363735383934\",\"type\":\"success\"}', '2021-01-22 08:38:46', NULL),
(587, '7780361239', 'login_otp', '{\"message\":\"3161766e6b78363036363539\",\"type\":\"success\"}', '2021-01-22 08:41:24', NULL),
(588, '7780361239', 'login_otp', '{\"message\":\"3161766e6b56373231313933\",\"type\":\"success\"}', '2021-01-22 08:41:48', NULL),
(589, '7780361239', 'login_otp', '{\"message\":\"3161766e6c6e383937353436\",\"type\":\"success\"}', '2021-01-22 08:42:14', NULL),
(590, '7780361239', 'login_otp', '{\"message\":\"3161766e7237353937333837\",\"type\":\"success\"}', '2021-01-22 08:48:59', NULL),
(591, '9550261360', 'order_place_admin', '{\"message\":\"3161766e7464363632373035\",\"type\":\"success\"}', '2021-01-22 08:50:04', NULL),
(592, '7780361239', 'order_place_user', '{\"message\":\"3161766e7464323930373331\",\"type\":\"success\"}', '2021-01-22 08:50:04', NULL),
(593, '9618629334', 'order_place_seller', '{\"message\":\"3161766e7464333736343335\",\"type\":\"success\"}', '2021-01-22 08:50:04', NULL),
(594, '7780361239', 'login_otp', '{\"message\":\"3161766f4c4d363138383034\",\"type\":\"success\"}', '2021-01-22 10:08:39', NULL),
(595, '7780361239', 'login_otp', '{\"message\":\"3161766f4d71383030393836\",\"type\":\"success\"}', '2021-01-22 10:09:17', NULL),
(596, '7780361239', 'login_otp', '{\"message\":\"3161766f5077363039333431\",\"type\":\"success\"}', '2021-01-22 10:12:23', NULL),
(597, '7780361239', 'login_otp', '{\"message\":\"3161766f5547393035333037\",\"type\":\"success\"}', '2021-01-22 10:17:33', NULL),
(598, '7780361239', 'login_otp', '{\"message\":\"3161766f5842343939343138\",\"type\":\"success\"}', '2021-01-22 10:20:28', NULL),
(599, '9550261360', 'order_place_admin', '{\"message\":\"3161766f5859363338303437\",\"type\":\"success\"}', '2021-01-22 10:20:51', NULL),
(600, '7780361239', 'order_place_user', '{\"message\":\"3161766f5859363133363735\",\"type\":\"success\"}', '2021-01-22 10:20:51', NULL),
(601, '9618629334', 'order_place_seller', '{\"message\":\"3161766f5859373734313139\",\"type\":\"success\"}', '2021-01-22 10:20:51', NULL),
(602, '7780361239', 'login_otp', '{\"message\":\"3161766f3758353937393231\",\"type\":\"success\"}', '2021-01-22 10:29:50', NULL),
(603, '7780361239', 'login_otp', '{\"message\":\"31617670306b383230343031\",\"type\":\"success\"}', '2021-01-22 10:30:11', NULL),
(604, '7780361239', 'login_otp', '{\"message\":\"316176706952333435363530\",\"type\":\"success\"}', '2021-01-22 10:39:44', NULL),
(605, '7780361239', 'login_otp', '{\"message\":\"316176706a69333034303036\",\"type\":\"success\"}', '2021-01-22 10:40:09', NULL),
(606, '7780361239', 'login_otp', '{\"message\":\"316176706a56353935393131\",\"type\":\"success\"}', '2021-01-22 10:40:48', NULL),
(607, '9948839362', 'login_otp', '{\"message\":\"316176706d69333431353036\",\"type\":\"success\"}', '2021-01-22 10:43:09', NULL),
(608, '9948839362', 'login_otp', '{\"message\":\"316176706d6c393432323138\",\"type\":\"success\"}', '2021-01-22 10:43:12', NULL),
(609, '7780361239', 'login_otp', '{\"message\":\"316176706e30333939393830\",\"type\":\"success\"}', '2021-01-22 10:44:00', NULL),
(610, '9948839362', 'login_otp', '{\"message\":\"316176704843383431363838\",\"type\":\"success\"}', '2021-01-22 11:04:29', NULL),
(611, '7780361239', 'login_otp', '{\"message\":\"316176704e32303231393630\",\"type\":\"success\"}', '2021-01-22 11:10:54', NULL),
(612, '7780361239', 'login_otp', '{\"message\":\"316176705051343230303634\",\"type\":\"success\"}', '2021-01-22 11:12:43', NULL),
(613, '9550261360', 'order_place_admin', '{\"message\":\"316176705171353033373332\",\"type\":\"success\"}', '2021-01-22 11:13:17', NULL),
(614, '7780361239', 'order_place_user', '{\"message\":\"316176705172363132363737\",\"type\":\"success\"}', '2021-01-22 11:13:18', NULL),
(615, '9618629334', 'order_place_seller', '{\"message\":\"316176705172343937343838\",\"type\":\"success\"}', '2021-01-22 11:13:18', NULL),
(616, '9948839362', 'login_otp', '{\"message\":\"316176705276383732363438\",\"type\":\"success\"}', '2021-01-22 11:14:22', NULL),
(617, '9550261360', 'order_place_admin', '{\"message\":\"31617670524f313635363236\",\"type\":\"success\"}', '2021-01-22 11:14:42', NULL),
(618, '9948839362', 'order_place_user', '{\"message\":\"316176705250383935383337\",\"type\":\"success\"}', '2021-01-22 11:14:42', NULL),
(619, '9618629334', 'order_place_seller', '{\"message\":\"316176705250353436363936\",\"type\":\"success\"}', '2021-01-22 11:14:42', NULL),
(620, '7780361239', 'login_otp', '{\"message\":\"316176705a36333933383232\",\"type\":\"success\"}', '2021-01-22 11:22:58', NULL),
(621, '7780361239', 'login_otp', '{\"message\":\"316176703147343836313931\",\"type\":\"success\"}', '2021-01-22 11:23:33', NULL),
(622, '9550261360', 'order_place_admin', '{\"message\":\"316176703157383139353730\",\"type\":\"success\"}', '2021-01-22 11:23:49', NULL),
(623, '7780361239', 'order_place_user', '{\"message\":\"316176703157393938333831\",\"type\":\"success\"}', '2021-01-22 11:23:49', NULL),
(624, '9618629334', 'order_place_seller', '{\"message\":\"316176703157393330343537\",\"type\":\"success\"}', '2021-01-22 11:23:49', NULL),
(625, '9949587496', 'login_otp', '{\"message\":\"316176717070353935323631\",\"type\":\"success\"}', '2021-01-22 11:46:16', NULL),
(626, '9550261360', 'login_otp', '{\"message\":\"316176726355343435323337\",\"type\":\"success\"}', '2021-01-22 12:33:47', NULL),
(627, '7780361239', 'login_otp', '{\"message\":\"316176727941343134343734\",\"type\":\"success\"}', '2021-01-22 12:55:27', NULL),
(628, '9948839362', 'login_otp', '{\"message\":\"316176724b4c383437333635\",\"type\":\"success\"}', '2021-01-22 13:07:38', NULL),
(629, '9948839362', 'login_otp', '{\"message\":\"316176725041343930383332\",\"type\":\"success\"}', '2021-01-22 13:12:27', NULL),
(630, '8520021995', 'verify_mobile', '{\"message\":\"31617672514d353330383638\",\"type\":\"success\"}', '2021-01-22 13:13:39', NULL),
(631, '8520021995', 'verify_mobile', '{\"message\":\"316176725363353031303936\",\"type\":\"success\"}', '2021-01-22 13:15:03', NULL),
(632, '8520021995', 'verify_mobile', '{\"message\":\"316176725568343130333837\",\"type\":\"success\"}', '2021-01-22 13:17:08', NULL),
(633, '8520021995', 'verify_mobile', '{\"message\":\"316176725853353832323232\",\"type\":\"success\"}', '2021-01-22 13:20:45', NULL),
(634, '8520021995', 'verify_mobile', '{\"message\":\"31617672316d363735303437\",\"type\":\"success\"}', '2021-01-22 13:23:13', NULL),
(635, '8520021995', 'verify_mobile', '{\"message\":\"316176723254303336393833\",\"type\":\"success\"}', '2021-01-22 13:24:46', NULL),
(636, '8520021995', 'verify_mobile', '{\"message\":\"31617673686f393138323031\",\"type\":\"success\"}', '2021-01-22 13:38:15', NULL),
(637, '7780361239', 'login_otp', '{\"message\":\"316176734479353439333632\",\"type\":\"success\"}', '2021-01-22 14:00:25', NULL),
(638, '9949587496', 'login_otp', '{\"message\":\"31617673464f343236313336\",\"type\":\"success\"}', '2021-01-22 14:02:41', NULL),
(639, '7396043338', 'login_otp', '{\"message\":\"3161776b6f75393639393439\",\"type\":\"success\"}', '2021-01-23 05:45:21', NULL),
(640, '9948839362', 'login_otp', '{\"message\":\"3161776b347a313737333435\",\"type\":\"success\"}', '2021-01-23 06:26:26', NULL),
(641, '9948839362', 'login_otp', '{\"message\":\"3161776d344d343031333837\",\"type\":\"success\"}', '2021-01-23 08:26:39', NULL),
(642, '9948839362', 'login_otp', '{\"message\":\"3161776e696f333232353534\",\"type\":\"success\"}', '2021-01-23 08:39:15', NULL),
(643, '9948839362', 'login_otp', '{\"message\":\"3161776e6d6d313033333231\",\"type\":\"success\"}', '2021-01-23 08:43:13', NULL),
(644, '7428731210', 'verify_mobile', '{\"message\":\"3161776f7956383133393934\",\"type\":\"success\"}', '2021-01-23 09:55:48', NULL),
(645, '7428731210', 'verify_mobile', '{\"message\":\"3161776f7a6d323436393337\",\"type\":\"success\"}', '2021-01-23 09:56:13', NULL),
(646, '7428731210', 'verify_mobile', '{\"message\":\"3161776f4230313131313131\",\"type\":\"success\"}', '2021-01-23 09:58:00', NULL),
(647, '7428731210', 'verify_mobile', '{\"message\":\"3161776f4474343336363638\",\"type\":\"success\"}', '2021-01-23 10:00:20', NULL),
(648, '9948839362', 'login_otp', '{\"message\":\"3161776f5747363733313937\",\"type\":\"success\"}', '2021-01-23 10:19:33', NULL),
(649, '9618050570', 'verify_mobile', '{\"message\":\"3161796a4562313637393535\",\"type\":\"success\"}', '2021-01-25 05:01:02', NULL),
(650, '9949587496', 'login_otp', '{\"message\":\"3161796a3372363031323137\",\"type\":\"success\"}', '2021-01-25 05:25:18', NULL),
(651, '9948597496', 'login_otp', '{\"message\":\"3161796b3034393130323535\",\"type\":\"success\"}', '2021-01-25 05:30:56', NULL),
(652, '9949587496', 'login_otp', '{\"message\":\"3161796b6170333331313838\",\"type\":\"success\"}', '2021-01-25 05:31:16', NULL),
(653, '9550261360', 'order_place_admin', '{\"message\":\"3161796b6242373635363036\",\"type\":\"success\"}', '2021-01-25 05:32:28', NULL),
(654, '9949587496', 'order_place_user', '{\"message\":\"3161796b6242323635333739\",\"type\":\"success\"}', '2021-01-25 05:32:28', NULL),
(655, '9618629334', 'order_place_seller', '{\"message\":\"3161796b6242313838363730\",\"type\":\"success\"}', '2021-01-25 05:32:28', NULL),
(656, '9949587496', 'login_otp', '{\"message\":\"3161796b6243393139313336\",\"type\":\"success\"}', '2021-01-25 05:32:29', NULL),
(657, '9618629334', 'order_place_seller', '{\"message\":\"3161796b6247303039303039\",\"type\":\"success\"}', '2021-01-25 05:32:33', NULL),
(658, '9949587496', 'login_otp', '{\"message\":\"3161796b4753313533393834\",\"type\":\"success\"}', '2021-01-25 06:03:45', NULL),
(659, '9550261360', 'order_place_admin', '{\"message\":\"3161796b4877373136303238\",\"type\":\"success\"}', '2021-01-25 06:04:23', NULL),
(660, '9949587496', 'order_place_user', '{\"message\":\"3161796b4878323330353537\",\"type\":\"success\"}', '2021-01-25 06:04:24', NULL),
(661, '9618050570', 'order_place_seller', '{\"message\":\"3161796b4878323831363538\",\"type\":\"success\"}', '2021-01-25 06:04:24', NULL),
(662, '9550261360', 'order_place_admin', '{\"message\":\"3161796c5269373531383039\",\"type\":\"success\"}', '2021-01-25 07:14:09', NULL),
(663, '9949587496', 'order_place_user', '{\"message\":\"3161796c5269313432373730\",\"type\":\"success\"}', '2021-01-25 07:14:09', NULL),
(664, '9618629334', 'order_place_seller', '{\"message\":\"3161796c5269383132363430\",\"type\":\"success\"}', '2021-01-25 07:14:09', NULL),
(665, '9550261360', 'order_place_admin', '{\"message\":\"3161796c5876323731343535\",\"type\":\"success\"}', '2021-01-25 07:20:22', NULL),
(666, '9949587496', 'order_place_user', '{\"message\":\"3161796c5876333032393837\",\"type\":\"success\"}', '2021-01-25 07:20:22', NULL),
(667, '9618629334', 'order_place_seller', '{\"message\":\"3161796c5876373939343737\",\"type\":\"success\"}', '2021-01-25 07:20:22', NULL),
(668, '9949587496', 'login_otp', '{\"message\":\"3161796c3545363238383534\",\"type\":\"success\"}', '2021-01-25 07:27:31', NULL),
(669, '7780361239', 'login_otp', '{\"message\":\"3161796d634c333236333736\",\"type\":\"success\"}', '2021-01-25 07:33:38', NULL),
(670, '7780361239', 'login_otp', '{\"message\":\"3161796d6479343434303337\",\"type\":\"success\"}', '2021-01-25 07:34:25', NULL),
(671, '7780361239', 'login_otp', '{\"message\":\"3161796d6435323636373836\",\"type\":\"success\"}', '2021-01-25 07:34:57', NULL),
(672, '7780361239', 'login_otp', '{\"message\":\"3161796d6665363539363839\",\"type\":\"success\"}', '2021-01-25 07:36:05', NULL),
(673, '7780361239', 'login_otp', '{\"message\":\"3161796d6658383933353632\",\"type\":\"success\"}', '2021-01-25 07:36:50', NULL),
(674, '7780361239', 'login_otp', '{\"message\":\"3161796d6766313132363933\",\"type\":\"success\"}', '2021-01-25 07:37:06', NULL),
(675, '7780361239', 'login_otp', '{\"message\":\"3161796d6845363234323232\",\"type\":\"success\"}', '2021-01-25 07:38:31', NULL),
(676, '7780361239', 'login_otp', '{\"message\":\"3161796d7142383539363237\",\"type\":\"success\"}', '2021-01-25 07:47:28', NULL),
(677, '9550261360', 'order_place_admin', '{\"message\":\"3161796d7230333234303836\",\"type\":\"success\"}', '2021-01-25 07:48:00', NULL),
(678, '7780361239', 'order_place_user', '{\"message\":\"3161796d7230333635333334\",\"type\":\"success\"}', '2021-01-25 07:48:01', NULL),
(679, '9618050570', 'order_place_seller', '{\"message\":\"3161796d7261323737333730\",\"type\":\"success\"}', '2021-01-25 07:48:01', NULL),
(680, '9550261360', 'order_place_admin', '{\"message\":\"3161796e5230323036363139\",\"type\":\"success\"}', '2021-01-25 09:14:00', NULL),
(681, '9949587496', 'order_place_user', '{\"message\":\"3161796e5230303034363931\",\"type\":\"success\"}', '2021-01-25 09:14:00', NULL),
(682, '9618629334', 'order_place_seller', '{\"message\":\"3161796e5230393735353134\",\"type\":\"success\"}', '2021-01-25 09:14:00', NULL),
(683, '9949587496', 'login_otp', '{\"message\":\"3161796f306b323037393232\",\"type\":\"success\"}', '2021-01-25 09:30:11', NULL),
(684, '9550261360', 'order_place_admin', '{\"message\":\"3161796f6267383539383031\",\"type\":\"success\"}', '2021-01-25 09:32:07', NULL),
(685, '9949587496', 'order_place_user', '{\"message\":\"3161796f6267303239303134\",\"type\":\"success\"}', '2021-01-25 09:32:08', NULL),
(686, '9618629334', 'order_place_seller', '{\"message\":\"3161796f6268343333383232\",\"type\":\"success\"}', '2021-01-25 09:32:08', NULL),
(687, '9550261360', 'login_otp', '{\"message\":\"3161796f6631303637383839\",\"type\":\"success\"}', '2021-01-25 09:36:53', NULL),
(688, '7780361239', 'login_otp', '{\"message\":\"3161796f6b46303630343231\",\"type\":\"success\"}', '2021-01-25 09:41:32', NULL),
(689, '9550261360', 'order_place_admin', '{\"message\":\"3161796f5963333937323931\",\"type\":\"success\"}', '2021-01-25 10:21:03', NULL),
(690, '9949587496', 'order_place_user', '{\"message\":\"3161796f5963323839313237\",\"type\":\"success\"}', '2021-01-25 10:21:04', NULL),
(691, '9618629334', 'order_place_seller', '{\"message\":\"3161796f5964373235303430\",\"type\":\"success\"}', '2021-01-25 10:21:04', NULL),
(692, '9948839362', 'login_otp', '{\"message\":\"3161796f5969303336393736\",\"type\":\"success\"}', '2021-01-25 10:21:09', NULL),
(693, '9550261360', 'order_place_admin', '{\"message\":\"316179706133393439333634\",\"type\":\"success\"}', '2021-01-25 10:31:55', NULL),
(694, '9949587496', 'order_place_user', '{\"message\":\"316179706133393131323339\",\"type\":\"success\"}', '2021-01-25 10:31:55', NULL),
(695, '9618629334', 'order_place_seller', '{\"message\":\"316179706133363632303337\",\"type\":\"success\"}', '2021-01-25 10:31:55', NULL),
(696, '9550261360', 'order_place_admin', '{\"message\":\"316179706f76323830383831\",\"type\":\"success\"}', '2021-01-25 10:45:22', NULL),
(697, '9949587496', 'order_place_user', '{\"message\":\"316179706f76393235313038\",\"type\":\"success\"}', '2021-01-25 10:45:22', NULL),
(698, '9618629334', 'order_place_seller', '{\"message\":\"316179706f76363837363234\",\"type\":\"success\"}', '2021-01-25 10:45:23', NULL),
(699, '9949587496', 'login_otp', '{\"message\":\"31617971624f363735333637\",\"type\":\"success\"}', '2021-01-25 11:32:41', NULL),
(700, '9550261360', 'order_place_admin', '{\"message\":\"31617971495a353437303631\",\"type\":\"success\"}', '2021-01-25 12:05:52', NULL),
(701, '9949587496', 'order_place_user', '{\"message\":\"31617971495a323937313637\",\"type\":\"success\"}', '2021-01-25 12:05:52', NULL),
(702, '9618629334', 'order_place_seller', '{\"message\":\"31617971495a373034313836\",\"type\":\"success\"}', '2021-01-25 12:05:52', NULL),
(703, '9550261360', 'order_place_admin', '{\"message\":\"316179726b44383536323033\",\"type\":\"success\"}', '2021-01-25 12:41:30', NULL),
(704, '9949587496', 'order_place_user', '{\"message\":\"316179726b44353437373639\",\"type\":\"success\"}', '2021-01-25 12:41:30', NULL),
(705, '9618629334', 'order_place_seller', '{\"message\":\"316179726b45323839313934\",\"type\":\"success\"}', '2021-01-25 12:41:31', NULL),
(706, '9550261360', 'login_otp', '{\"message\":\"316179736635323130373931\",\"type\":\"success\"}', '2021-01-25 13:36:57', NULL),
(707, '9550261360', 'order_place_admin', '{\"message\":\"316179736878303038373339\",\"type\":\"success\"}', '2021-01-25 13:38:24', NULL),
(708, '9550261360', 'order_place_user', '{\"message\":\"316179736878303535303437\",\"type\":\"success\"}', '2021-01-25 13:38:24', NULL),
(709, '9618629334', 'order_place_seller', '{\"message\":\"316179736878333337373130\",\"type\":\"success\"}', '2021-01-25 13:38:24', NULL),
(710, '9550261360', 'login_otp', '{\"message\":\"316179736c30393935383330\",\"type\":\"success\"}', '2021-01-25 13:42:00', NULL),
(711, '9550261360', 'order_place_admin', '{\"message\":\"316179736e55323738333535\",\"type\":\"success\"}', '2021-01-25 13:44:47', NULL),
(712, '9550261360', 'order_place_user', '{\"message\":\"316179736e55353830303636\",\"type\":\"success\"}', '2021-01-25 13:44:47', NULL),
(713, '9618629334', 'order_place_seller', '{\"message\":\"316179736e56383033333830\",\"type\":\"success\"}', '2021-01-25 13:44:48', NULL),
(714, '9882929200', 'verify_mobile', '{\"message\":\"3161416b494f323138373536\",\"type\":\"success\"}', '2021-01-27 06:05:41', NULL),
(715, '9889667676', 'verify_mobile', '{\"message\":\"3161416b4a63353732333734\",\"type\":\"success\"}', '2021-01-27 06:06:03', NULL),
(716, '9889667676', 'verify_mobile', '{\"message\":\"3161416b4b56323835363837\",\"type\":\"success\"}', '2021-01-27 06:07:48', NULL),
(717, '8520021995', 'verify_mobile', '{\"message\":\"3161416b4d57333835333130\",\"type\":\"success\"}', '2021-01-27 06:09:49', NULL),
(718, '8520021995', 'verify_mobile', '{\"message\":\"3161416b4e50303235313535\",\"type\":\"success\"}', '2021-01-27 06:10:42', NULL),
(719, '8520021995', 'verify_mobile', '{\"message\":\"3161416b5249323836373532\",\"type\":\"success\"}', '2021-01-27 06:14:35', NULL),
(720, '8520021995', 'verify_mobile', '{\"message\":\"3161416b546b313234383837\",\"type\":\"success\"}', '2021-01-27 06:16:11', NULL),
(721, '8520021995', 'verify_mobile', '{\"message\":\"3161416b5654353639383839\",\"type\":\"success\"}', '2021-01-27 06:18:46', NULL),
(722, '8520021995', 'verify_mobile', '{\"message\":\"3161416b5846343438333430\",\"type\":\"success\"}', '2021-01-27 06:20:32', NULL),
(723, '7036221419', 'verify_mobile', '{\"message\":\"3161416c3335343530373038\",\"type\":\"success\"}', '2021-01-27 07:25:58', NULL),
(724, '8989989989', 'verify_mobile', '{\"message\":\"3161416d7935363534323736\",\"type\":\"success\"}', '2021-01-27 07:55:57', NULL),
(725, '9550261360', 'login_otp', '{\"message\":\"3161416d4953393137373132\",\"type\":\"success\"}', '2021-01-27 08:05:45', NULL),
(726, '9550261360', 'login_otp', '{\"message\":\"3161416e5957353035333634\",\"type\":\"success\"}', '2021-01-27 09:21:49', NULL),
(727, '9899589050', 'verify_mobile', '{\"message\":\"31614173736d343739353534\",\"type\":\"success\"}', '2021-01-27 13:49:13', NULL),
(728, '8500456815', 'verify_mobile', '{\"message\":\"316141737856373037353133\",\"type\":\"success\"}', '2021-01-27 13:54:48', NULL),
(729, '8500456815', 'verify_mobile', '{\"message\":\"316141737a44353336383930\",\"type\":\"success\"}', '2021-01-27 13:56:30', NULL),
(730, '8500456815', 'verify_mobile', '{\"message\":\"316141734261343530323432\",\"type\":\"success\"}', '2021-01-27 13:58:01', NULL),
(731, '9550261360', 'login_otp', '{\"message\":\"316141747843303433303934\",\"type\":\"success\"}', '2021-01-27 14:54:29', NULL),
(732, '9293275832', 'verify_mobile', '{\"message\":\"316141754d48363839393635\",\"type\":\"success\"}', '2021-01-27 16:09:34', NULL),
(733, '9293275832', 'verify_mobile', '{\"message\":\"316141755848343136303239\",\"type\":\"success\"}', '2021-01-27 16:20:34', NULL),
(734, '9293275832', 'verify_mobile', '{\"message\":\"31614175584f353636363136\",\"type\":\"success\"}', '2021-01-27 16:20:41', NULL),
(735, '9293573910', 'verify_mobile', '{\"message\":\"316141755a78373332363931\",\"type\":\"success\"}', '2021-01-27 16:22:24', NULL),
(736, '9293573910', 'verify_mobile', '{\"message\":\"316141755a46383837323030\",\"type\":\"success\"}', '2021-01-27 16:22:32', NULL),
(737, '7780361239', 'login_otp', '{\"message\":\"3161426a4c33323932313234\",\"type\":\"success\"}', '2021-01-28 05:08:55', NULL),
(738, '7780361239', 'login_otp', '{\"message\":\"3161426a4f51383835343436\",\"type\":\"success\"}', '2021-01-28 05:11:43', NULL),
(739, '9550261360', 'order_place_admin', '{\"message\":\"3161426a5064333732383236\",\"type\":\"success\"}', '2021-01-28 05:12:04', NULL),
(740, '7780361239', 'order_place_user', '{\"message\":\"3161426a5064353834333733\",\"type\":\"success\"}', '2021-01-28 05:12:04', NULL),
(741, '9618050570', 'order_place_seller', '{\"message\":\"3161426a5064393535383539\",\"type\":\"success\"}', '2021-01-28 05:12:04', NULL),
(742, '9550261360', 'order_place_admin', '{\"message\":\"3161426b617a323237353138\",\"type\":\"success\"}', '2021-01-28 05:31:26', NULL),
(743, '7780361239', 'order_place_user', '{\"message\":\"3161426b6141353232313431\",\"type\":\"success\"}', '2021-01-28 05:31:27', NULL),
(744, '9618050570', 'order_place_seller', '{\"message\":\"3161426b6141333639363732\",\"type\":\"success\"}', '2021-01-28 05:31:27', NULL),
(745, '9550261360', 'login_otp', '{\"message\":\"3161426b3169303234353631\",\"type\":\"success\"}', '2021-01-28 06:23:09', NULL),
(746, '9550261360', 'order_place_admin', '{\"message\":\"3161426c6f4d343131313530\",\"type\":\"success\"}', '2021-01-28 06:45:40', NULL),
(747, '7780361239', 'order_place_user', '{\"message\":\"3161426c6f4e373433383433\",\"type\":\"success\"}', '2021-01-28 06:45:40', NULL),
(748, '9618629334', 'order_place_seller', '{\"message\":\"3161426c6f4e313431383536\",\"type\":\"success\"}', '2021-01-28 06:45:40', NULL),
(749, '7780361239', 'login_otp', '{\"message\":\"3161426c7566373939393137\",\"type\":\"success\"}', '2021-01-28 06:51:06', NULL),
(750, '9550261360', 'login_otp', '{\"message\":\"316142706f45353139393138\",\"type\":\"success\"}', '2021-01-28 10:45:31', NULL),
(751, '9550261360', 'login_otp', '{\"message\":\"316142707153333037373137\",\"type\":\"success\"}', '2021-01-28 10:47:45', NULL),
(752, '9948037153', 'verify_mobile', '{\"message\":\"316142707335303831383438\",\"type\":\"success\"}', '2021-01-28 10:49:57', NULL),
(753, '9948037153', 'verify_mobile', '{\"message\":\"316142707449353031303737\",\"type\":\"success\"}', '2021-01-28 10:50:35', NULL),
(754, '9948037153', 'login_otp', '{\"message\":\"316142707573313030333635\",\"type\":\"success\"}', '2021-01-28 10:51:20', NULL),
(755, '9948037153', 'login_otp', '{\"message\":\"31614270776f393430313639\",\"type\":\"success\"}', '2021-01-28 10:53:15', NULL);
INSERT INTO `sms_logs` (`id`, `mobile`, `action`, `response_data`, `created_at`, `updated_at`) VALUES
(756, '9948037153', 'login_otp', '{\"message\":\"316142704274323831363931\",\"type\":\"success\"}', '2021-01-28 10:58:20', NULL),
(757, '9550261360', 'login_otp', '{\"message\":\"316142704c4b333635303934\",\"type\":\"success\"}', '2021-01-28 11:08:37', NULL),
(758, '9949587496', 'login_otp', '{\"message\":\"316142716d56393139343331\",\"type\":\"success\"}', '2021-01-28 11:43:48', NULL),
(759, '9550261360', 'login_otp', '{\"message\":\"316142727651333030363330\",\"type\":\"success\"}', '2021-01-28 12:52:43', NULL),
(760, '9948839362', 'login_otp', '{\"message\":\"316142724365363134323032\",\"type\":\"success\"}', '2021-01-28 12:59:05', NULL),
(761, '9949587496', 'login_otp', '{\"message\":\"316142743569363439353230\",\"type\":\"success\"}', '2021-01-28 15:27:09', NULL),
(762, '9550261360', 'login_otp', '{\"message\":\"31614275736b383839363536\",\"type\":\"success\"}', '2021-01-28 15:49:11', NULL),
(763, '9550261360', 'login_otp', '{\"message\":\"316142754b63333330373439\",\"type\":\"success\"}', '2021-01-28 16:07:03', NULL),
(764, '9949587496', 'login_otp', '{\"message\":\"31614275596c383435313038\",\"type\":\"success\"}', '2021-01-28 16:21:12', NULL),
(765, '9949587496', 'login_otp', '{\"message\":\"3161436a5366373935373130\",\"type\":\"success\"}', '2021-01-29 05:15:06', NULL),
(766, '9550261360', 'login_otp', '{\"message\":\"3161436b6754393830363034\",\"type\":\"success\"}', '2021-01-29 05:37:46', NULL),
(767, '9550261360', 'login_otp', '{\"message\":\"3161436b6b4e373037333439\",\"type\":\"success\"}', '2021-01-29 05:41:40', NULL),
(768, '9550261360', 'order_place_admin', '{\"message\":\"3161436b6b34393636393732\",\"type\":\"success\"}', '2021-01-29 05:41:56', NULL),
(769, '9550261360', 'order_place_user', '{\"message\":\"3161436b6b34353539323234\",\"type\":\"success\"}', '2021-01-29 05:41:56', NULL),
(770, '9550261360', 'login_otp', '{\"message\":\"3161436b736c303435333236\",\"type\":\"success\"}', '2021-01-29 05:49:12', NULL),
(771, '9550261360', 'login_otp', '{\"message\":\"3161436b7441343230383736\",\"type\":\"success\"}', '2021-01-29 05:50:27', NULL),
(772, '9814200011', 'verify_mobile', '{\"message\":\"3161436b776d343039313035\",\"type\":\"success\"}', '2021-01-29 05:53:13', NULL),
(773, '9814200011', 'verify_mobile', '{\"message\":\"3161436b796e333138303739\",\"type\":\"success\"}', '2021-01-29 05:55:14', NULL),
(774, '9949587496', 'login_otp', '{\"message\":\"3161436b5948383432343330\",\"type\":\"success\"}', '2021-01-29 06:21:34', NULL),
(775, '9949587496', 'login_otp', '{\"message\":\"3161436b3147303130393834\",\"type\":\"success\"}', '2021-01-29 06:23:33', NULL),
(776, '9949587496', 'login_otp', '{\"message\":\"3161436b3775363239333336\",\"type\":\"success\"}', '2021-01-29 06:29:21', NULL),
(777, '9949587496', 'login_otp', '{\"message\":\"3161436c626f323430313136\",\"type\":\"success\"}', '2021-01-29 06:32:15', NULL),
(778, '9949587496', 'login_otp', '{\"message\":\"3161436c6579333831313032\",\"type\":\"success\"}', '2021-01-29 06:35:25', NULL),
(779, '9676468686', 'login_otp', '{\"message\":\"3161436c666d323433363530\",\"type\":\"success\"}', '2021-01-29 06:36:13', NULL),
(780, '9949587496', 'login_otp', '{\"message\":\"3161436c684a353837383735\",\"type\":\"success\"}', '2021-01-29 06:38:36', NULL),
(781, '7780361239', 'login_otp', '{\"message\":\"3161436c6f56393631343633\",\"type\":\"success\"}', '2021-01-29 06:45:48', NULL),
(782, '9814200011', 'verify_mobile', '{\"message\":\"3161436c7677323533323134\",\"type\":\"success\"}', '2021-01-29 06:52:23', NULL),
(783, '9814200011', 'verify_mobile', '{\"message\":\"3161436c776f393531353634\",\"type\":\"success\"}', '2021-01-29 06:53:15', NULL),
(784, '8328566312', 'verify_mobile', '{\"message\":\"3161436c7a67363436393536\",\"type\":\"success\"}', '2021-01-29 06:56:07', NULL),
(785, '8328566312', 'verify_mobile', '{\"message\":\"3161436c7a51393734333831\",\"type\":\"success\"}', '2021-01-29 06:56:43', NULL),
(786, '8328566312', 'verify_mobile', '{\"message\":\"3161436c7a37373639363430\",\"type\":\"success\"}', '2021-01-29 06:56:59', NULL),
(787, '9618629334', 'verify_mobile', '{\"message\":\"3161436c4573313834313534\",\"type\":\"success\"}', '2021-01-29 07:01:19', NULL),
(788, '9618629334', 'verify_mobile', '{\"message\":\"3161436c4545303035353033\",\"type\":\"success\"}', '2021-01-29 07:01:32', NULL),
(789, '9866336421', 'verify_mobile', '{\"message\":\"3161436c4933343634363136\",\"type\":\"success\"}', '2021-01-29 07:05:55', NULL),
(790, '9814200011', 'verify_mobile', '{\"message\":\"3161436c4f34343732373338\",\"type\":\"success\"}', '2021-01-29 07:11:56', NULL),
(791, '9814200011', 'verify_mobile', '{\"message\":\"3161436c5247363138353033\",\"type\":\"success\"}', '2021-01-29 07:14:33', NULL),
(792, '7780361239', 'verify_mobile', '{\"message\":\"3161436c5356343639343537\",\"type\":\"success\"}', '2021-01-29 07:15:48', NULL),
(793, '9949587496', 'login_otp', '{\"message\":\"3161436c5a75373737343533\",\"type\":\"success\"}', '2021-01-29 07:22:21', NULL),
(794, '7780361239', 'login_otp', '{\"message\":\"3161436c3151343536363736\",\"type\":\"success\"}', '2021-01-29 07:23:43', NULL),
(795, '9345393943', 'verify_mobile', '{\"message\":\"3161436d494c313730323330\",\"type\":\"success\"}', '2021-01-29 08:05:38', NULL),
(796, '9038475000', 'verify_mobile', '{\"message\":\"3161436e6f33313838303539\",\"type\":\"success\"}', '2021-01-29 08:45:55', NULL),
(797, '9550261360', 'login_otp', '{\"message\":\"3161436e7243393733383637\",\"type\":\"success\"}', '2021-01-29 08:48:29', NULL),
(798, '7780361239', 'login_otp', '{\"message\":\"3161436e4952323439343434\",\"type\":\"success\"}', '2021-01-29 09:05:44', NULL),
(799, '7780361239', 'login_otp', '{\"message\":\"3161436e5732363837393134\",\"type\":\"success\"}', '2021-01-29 09:19:54', NULL),
(800, '7780361239', 'login_otp', '{\"message\":\"3161436e5874373434343838\",\"type\":\"success\"}', '2021-01-29 09:20:20', NULL),
(801, '7780361239', 'login_otp', '{\"message\":\"3161436e365a333836363531\",\"type\":\"success\"}', '2021-01-29 09:28:52', NULL),
(802, '7780361239', 'login_otp', '{\"message\":\"3161436f6237373138393936\",\"type\":\"success\"}', '2021-01-29 09:32:59', NULL),
(803, '7780361239', 'login_otp', '{\"message\":\"3161436f6f37313435323435\",\"type\":\"success\"}', '2021-01-29 09:45:59', NULL),
(804, '7780361239', 'login_otp', '{\"message\":\"3161436f7045353835383732\",\"type\":\"success\"}', '2021-01-29 09:46:31', NULL),
(805, '7780361239', 'login_otp', '{\"message\":\"3161436f7245363633343135\",\"type\":\"success\"}', '2021-01-29 09:48:31', NULL),
(806, '9550261360', 'order_place_admin', '{\"message\":\"3161436f7361313535313239\",\"type\":\"success\"}', '2021-01-29 09:49:01', NULL),
(807, '7780361239', 'order_place_user', '{\"message\":\"3161436f7361383030303739\",\"type\":\"success\"}', '2021-01-29 09:49:01', NULL),
(808, '9949587496', 'login_otp', '{\"message\":\"3161436f7449373130313239\",\"type\":\"success\"}', '2021-01-29 09:50:35', NULL),
(809, '9949587496', 'login_otp', '{\"message\":\"3161436f7576323537383735\",\"type\":\"success\"}', '2021-01-29 09:51:22', NULL),
(810, '7780361239', 'login_otp', '{\"message\":\"3161436f5946323836303333\",\"type\":\"success\"}', '2021-01-29 10:21:32', NULL),
(811, '9676313918', 'login_otp', '{\"message\":\"316143704e44393437363333\",\"type\":\"success\"}', '2021-01-29 11:10:30', NULL),
(812, '9676313918', 'login_otp', '{\"message\":\"31614370516c323430303838\",\"type\":\"success\"}', '2021-01-29 11:13:12', NULL),
(813, '7780361239', 'login_otp', '{\"message\":\"316143705643343834313334\",\"type\":\"success\"}', '2021-01-29 11:18:29', NULL),
(814, '9949587496', 'login_otp', '{\"message\":\"316143716363333935303837\",\"type\":\"success\"}', '2021-01-29 11:33:03', NULL),
(815, '9676313918', 'login_otp', '{\"message\":\"31614371674a303436373333\",\"type\":\"success\"}', '2021-01-29 11:37:36', NULL),
(816, '9290573910', 'login_otp', '{\"message\":\"316143716751303630303232\",\"type\":\"success\"}', '2021-01-29 11:37:43', NULL),
(817, '7780361239', 'login_otp', '{\"message\":\"31614371697a333836353031\",\"type\":\"success\"}', '2021-01-29 11:39:26', NULL),
(818, '7780361239', 'login_otp', '{\"message\":\"316143716955343338383532\",\"type\":\"success\"}', '2021-01-29 11:39:47', NULL),
(819, '7780361239', 'login_otp', '{\"message\":\"316143716b32303537303433\",\"type\":\"success\"}', '2021-01-29 11:41:54', NULL),
(820, '7780361239', 'login_otp', '{\"message\":\"316143716d78353331393234\",\"type\":\"success\"}', '2021-01-29 11:43:24', NULL),
(821, '7780361239', 'login_otp', '{\"message\":\"316143716e42333632353339\",\"type\":\"success\"}', '2021-01-29 11:44:28', NULL),
(822, '7780361239', 'login_otp', '{\"message\":\"316143717676343036333039\",\"type\":\"success\"}', '2021-01-29 11:52:22', NULL),
(823, '9550261360', 'order_place_admin', '{\"message\":\"31614371764d343638393238\",\"type\":\"success\"}', '2021-01-29 11:52:39', NULL),
(824, '7780361239', 'order_place_user', '{\"message\":\"31614371764d363239323431\",\"type\":\"success\"}', '2021-01-29 11:52:40', NULL),
(825, '7780361239', 'login_otp', '{\"message\":\"316143717830313135373635\",\"type\":\"success\"}', '2021-01-29 11:54:00', NULL),
(826, '9550261360', 'order_place_admin', '{\"message\":\"31614371786f393339303530\",\"type\":\"success\"}', '2021-01-29 11:54:15', NULL),
(827, '7780361239', 'order_place_user', '{\"message\":\"31614371786f333033313136\",\"type\":\"success\"}', '2021-01-29 11:54:15', NULL),
(828, '9676313918', 'login_otp', '{\"message\":\"316143714330313530363231\",\"type\":\"success\"}', '2021-01-29 11:59:00', NULL),
(829, '7780361239', 'login_otp', '{\"message\":\"31614371494f383235343334\",\"type\":\"success\"}', '2021-01-29 12:05:41', NULL),
(830, '9550261360', 'order_place_admin', '{\"message\":\"316143714934383631393133\",\"type\":\"success\"}', '2021-01-29 12:05:56', NULL),
(831, '7780361239', 'order_place_user', '{\"message\":\"316143714935313039363333\",\"type\":\"success\"}', '2021-01-29 12:05:57', NULL),
(832, '9177669893', 'verify_mobile', '{\"message\":\"316143715462393733373738\",\"type\":\"success\"}', '2021-01-29 12:16:02', NULL),
(833, '9948839362', 'login_otp', '{\"message\":\"316143724479333731333239\",\"type\":\"success\"}', '2021-01-29 13:00:25', NULL),
(834, '9948839362', 'login_otp', '{\"message\":\"316143724856373232333135\",\"type\":\"success\"}', '2021-01-29 13:04:48', NULL),
(835, '9948839362', 'login_otp', '{\"message\":\"316143724835303134333533\",\"type\":\"success\"}', '2021-01-29 13:04:57', NULL),
(836, '9948839362', 'login_otp', '{\"message\":\"316143724836303534323533\",\"type\":\"success\"}', '2021-01-29 13:04:58', NULL),
(837, '9948839362', 'login_otp', '{\"message\":\"316143724930333332363137\",\"type\":\"success\"}', '2021-01-29 13:05:00', NULL),
(838, '9948839362', 'login_otp', '{\"message\":\"316143724969383536393439\",\"type\":\"success\"}', '2021-01-29 13:05:09', NULL),
(839, '9948839362', 'login_otp', '{\"message\":\"31614372496c363733343133\",\"type\":\"success\"}', '2021-01-29 13:05:12', NULL),
(840, '7780361239', 'login_otp', '{\"message\":\"316143724b43353434323932\",\"type\":\"success\"}', '2021-01-29 13:07:29', NULL),
(841, '9948839362', 'login_otp', '{\"message\":\"316143725050383338333633\",\"type\":\"success\"}', '2021-01-29 13:12:42', NULL),
(842, '9948839362', 'login_otp', '{\"message\":\"31614372536b363936353534\",\"type\":\"success\"}', '2021-01-29 13:15:11', NULL),
(843, '9948839362', 'login_otp', '{\"message\":\"316143725a71313838373031\",\"type\":\"success\"}', '2021-01-29 13:22:17', NULL),
(844, '9948839362', 'login_otp', '{\"message\":\"316143725a79313530333432\",\"type\":\"success\"}', '2021-01-29 13:22:25', NULL),
(845, '9948839362', 'login_otp', '{\"message\":\"316143725a5a333138313231\",\"type\":\"success\"}', '2021-01-29 13:22:52', NULL),
(846, '9948839362', 'login_otp', '{\"message\":\"316143723164393532393831\",\"type\":\"success\"}', '2021-01-29 13:23:04', NULL),
(847, '9948839362', 'login_otp', '{\"message\":\"316143723173313939363531\",\"type\":\"success\"}', '2021-01-29 13:23:19', NULL),
(848, '9948839362', 'login_otp', '{\"message\":\"316143723142373036353634\",\"type\":\"success\"}', '2021-01-29 13:23:28', NULL),
(849, '9948839362', 'login_otp', '{\"message\":\"316143736168373139373932\",\"type\":\"success\"}', '2021-01-29 13:31:08', NULL),
(850, '9948839362', 'login_otp', '{\"message\":\"316143736855333632363635\",\"type\":\"success\"}', '2021-01-29 13:38:47', NULL),
(851, '9948839362', 'login_otp', '{\"message\":\"316143736974363634303738\",\"type\":\"success\"}', '2021-01-29 13:39:20', NULL),
(852, '9948839362', 'login_otp', '{\"message\":\"316143736a43353638333330\",\"type\":\"success\"}', '2021-01-29 13:40:29', NULL),
(853, '9948839362', 'login_otp', '{\"message\":\"316143736d37353935313832\",\"type\":\"success\"}', '2021-01-29 13:43:59', NULL),
(854, '9948839362', 'login_otp', '{\"message\":\"316143736f77353333333834\",\"type\":\"success\"}', '2021-01-29 13:45:23', NULL),
(855, '9948839362', 'login_otp', '{\"message\":\"316143736f37373930373139\",\"type\":\"success\"}', '2021-01-29 13:45:59', NULL),
(856, '9948839362', 'login_otp', '{\"message\":\"31614373704a303732353738\",\"type\":\"success\"}', '2021-01-29 13:46:36', NULL),
(857, '9948839362', 'login_otp', '{\"message\":\"316143737164373230333633\",\"type\":\"success\"}', '2021-01-29 13:47:04', NULL),
(858, '9948839362', 'login_otp', '{\"message\":\"316143737147343439393532\",\"type\":\"success\"}', '2021-01-29 13:47:34', NULL),
(859, '9948839362', 'login_otp', '{\"message\":\"316143737246333039323038\",\"type\":\"success\"}', '2021-01-29 13:48:32', NULL),
(860, '9948839362', 'login_otp', '{\"message\":\"31614373735a333738323939\",\"type\":\"success\"}', '2021-01-29 13:49:52', NULL),
(861, '7780361239', 'login_otp', '{\"message\":\"316143737557393335333431\",\"type\":\"success\"}', '2021-01-29 13:51:49', NULL),
(862, '9948839362', 'login_otp', '{\"message\":\"316143737a79303236373639\",\"type\":\"success\"}', '2021-01-29 13:56:25', NULL),
(863, '9948839362', 'login_otp', '{\"message\":\"316143737a37313134313539\",\"type\":\"success\"}', '2021-01-29 13:56:59', NULL),
(864, '7780361239', 'login_otp', '{\"message\":\"316143734847333538353934\",\"type\":\"success\"}', '2021-01-29 14:04:33', NULL),
(865, '9550261360', 'order_place_admin', '{\"message\":\"31614373496f323431333637\",\"type\":\"success\"}', '2021-01-29 14:05:15', NULL),
(866, '7780361239', 'order_place_user', '{\"message\":\"31614373496f353532383137\",\"type\":\"success\"}', '2021-01-29 14:05:15', NULL),
(867, '9550261360', 'order_place_admin', '{\"message\":\"316143734947303030383730\",\"type\":\"success\"}', '2021-01-29 14:05:33', NULL),
(868, '7780361239', 'order_place_user', '{\"message\":\"316143734947373536393335\",\"type\":\"success\"}', '2021-01-29 14:05:33', NULL),
(869, '7780361239', 'login_otp', '{\"message\":\"316143734a65353732303530\",\"type\":\"success\"}', '2021-01-29 14:06:05', NULL),
(870, '7780361239', 'login_otp', '{\"message\":\"316143734c34353832313935\",\"type\":\"success\"}', '2021-01-29 14:08:56', NULL),
(871, '7780361239', 'login_otp', '{\"message\":\"316143734e4e363332373839\",\"type\":\"success\"}', '2021-01-29 14:10:40', NULL),
(872, '9550261360', 'order_place_admin', '{\"message\":\"316143734e33333537303636\",\"type\":\"success\"}', '2021-01-29 14:10:55', NULL),
(873, '7780361239', 'order_place_user', '{\"message\":\"316143734e33393339343135\",\"type\":\"success\"}', '2021-01-29 14:10:55', NULL),
(874, '9949587496', 'login_otp', '{\"message\":\"316143744b32373330303833\",\"type\":\"success\"}', '2021-01-29 15:07:54', NULL),
(875, '9949587496', 'login_otp', '{\"message\":\"316143753175333832343635\",\"type\":\"success\"}', '2021-01-29 16:23:22', NULL),
(876, '9676468686', 'login_otp', '{\"message\":\"31614376614d353836363034\",\"type\":\"success\"}', '2021-01-29 16:31:39', NULL),
(877, '9676468686', 'login_otp', '{\"message\":\"316143766230353536333633\",\"type\":\"success\"}', '2021-01-29 16:32:00', NULL),
(878, '9550261360', 'login_otp', '{\"message\":\"316143766248333737393835\",\"type\":\"success\"}', '2021-01-29 16:32:34', NULL),
(879, '9676468686', 'login_otp', '{\"message\":\"316143766369313638363238\",\"type\":\"success\"}', '2021-01-29 16:33:09', NULL),
(880, '9676468686', 'login_otp', '{\"message\":\"316143766347373033373632\",\"type\":\"success\"}', '2021-01-29 16:33:33', NULL),
(881, '9948037153', 'login_otp', '{\"message\":\"31614376646b353335303432\",\"type\":\"success\"}', '2021-01-29 16:34:11', NULL),
(882, '9949587496', 'login_otp', '{\"message\":\"316144706e4d303736373432\",\"type\":\"success\"}', '2021-01-30 10:44:39', NULL),
(883, '7780361239', 'login_otp', '{\"message\":\"316144707857373236373433\",\"type\":\"success\"}', '2021-01-30 10:54:49', NULL),
(884, '9549588439', 'login_otp', '{\"message\":\"316144703674363331383439\",\"type\":\"success\"}', '2021-01-30 11:28:20', NULL),
(885, '9549588439', 'login_otp', '{\"message\":\"316144703648333839343031\",\"type\":\"success\"}', '2021-01-30 11:28:34', NULL),
(886, '7780361239', 'login_otp', '{\"message\":\"316144763669303131393439\",\"type\":\"success\"}', '2021-01-30 17:28:10', NULL),
(887, '7780361239', 'login_otp', '{\"message\":\"3162616a6979363230313034\",\"type\":\"success\"}', '2021-02-01 04:39:25', NULL),
(888, '9949587496', 'login_otp', '{\"message\":\"3162616a7a33323236313931\",\"type\":\"success\"}', '2021-02-01 04:56:55', NULL),
(889, '9949587496', 'login_otp', '{\"message\":\"3162616a426c313937323037\",\"type\":\"success\"}', '2021-02-01 04:58:12', NULL),
(890, '7780361239', 'login_otp', '{\"message\":\"3162616a436d363133363832\",\"type\":\"success\"}', '2021-02-01 04:59:13', NULL),
(891, '9949587496', 'login_otp', '{\"message\":\"3162616a4335313832363837\",\"type\":\"success\"}', '2021-02-01 04:59:57', NULL),
(892, '7780361239', 'login_otp', '{\"message\":\"3162616a4456333537373433\",\"type\":\"success\"}', '2021-02-01 05:00:48', NULL),
(893, '7780361239', 'login_otp', '{\"message\":\"3162616a4847313035363832\",\"type\":\"success\"}', '2021-02-01 05:04:33', NULL),
(894, '7780361239', 'login_otp', '{\"message\":\"3162616a4971353837323738\",\"type\":\"success\"}', '2021-02-01 05:05:17', NULL),
(895, '9290573910', 'login_otp', '{\"message\":\"3162616c7735303036363938\",\"type\":\"success\"}', '2021-02-01 06:53:57', NULL),
(896, '9549588439', 'login_otp', '{\"message\":\"3162626e6470343534323236\",\"type\":\"success\"}', '2021-02-02 08:34:16', NULL),
(897, '9290573910', 'verify_mobile', '{\"message\":\"31626970656e333137353530\",\"type\":\"success\"}', '2021-02-09 10:35:14', NULL),
(898, '8328566312', 'verify_mobile', '{\"message\":\"3162706f4232323635363730\",\"type\":\"success\"}', '2021-02-16 09:58:54', NULL),
(899, '9290573910', 'verify_mobile', '{\"message\":\"3162716c506e313736343634\",\"type\":\"success\"}', '2021-02-17 07:12:14', NULL),
(900, '9290573910', 'verify_mobile', '{\"message\":\"3162716d6175353238353333\",\"type\":\"success\"}', '2021-02-17 07:31:21', NULL),
(901, '9290573910', 'verify_mobile', '{\"message\":\"3162716d6445393533363331\",\"type\":\"success\"}', '2021-02-17 07:34:31', NULL),
(902, '9290573910', 'verify_mobile', '{\"message\":\"3162716d666e363534363339\",\"type\":\"success\"}', '2021-02-17 07:36:14', NULL),
(903, '9290573910', 'verify_mobile', '{\"message\":\"3162716d6764363239373239\",\"type\":\"success\"}', '2021-02-17 07:37:04', NULL),
(904, '9290573910', 'verify_mobile', '{\"message\":\"3162716d6747323933353734\",\"type\":\"success\"}', '2021-02-17 07:37:33', NULL),
(905, '9293275832', 'verify_mobile', '{\"message\":\"316363714552383432393939\",\"type\":\"success\"}', '2021-03-03 12:01:44', NULL),
(906, '9293275832', 'verify_mobile', '{\"message\":\"316363714953323437323037\",\"type\":\"success\"}', '2021-03-03 12:05:45', NULL),
(907, '9293275832', 'verify_mobile', '{\"message\":\"316377706565323739343731\",\"type\":\"success\"}', '2021-03-23 10:35:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Andaman and Nicobar Islands', '2020-11-05 08:58:32', NULL),
(2, 'Andhra Pradesh', '2020-11-05 08:58:32', NULL),
(3, 'Arunachal Pradesh', '2020-11-05 08:58:32', NULL),
(4, 'Assam', '2020-11-05 08:58:32', NULL),
(5, 'Bihar', '2020-11-05 08:58:32', NULL),
(6, 'Chandigarh', '2020-11-05 08:58:32', NULL),
(7, 'Chhattisgarh', '2020-11-05 08:58:32', NULL),
(8, 'Dadra and Nagar Haveli', '2020-11-05 08:58:32', NULL),
(9, 'Daman and Diu', '2020-11-05 08:58:32', NULL),
(10, 'Delhi', '2020-11-05 08:58:32', NULL),
(11, 'Goa', '2020-11-05 08:58:32', NULL),
(12, 'Gujarat', '2020-11-05 08:58:32', NULL),
(13, 'Haryana', '2020-11-05 08:58:32', NULL),
(14, 'Himachal Pradesh', '2020-11-05 08:58:32', NULL),
(15, 'Jammu and Kashmir', '2020-11-05 08:58:32', NULL),
(16, 'Jharkhand', '2020-11-05 08:58:32', NULL),
(17, 'Karnataka', '2020-11-05 08:58:32', NULL),
(18, 'Kenmore', '2020-11-05 08:58:32', NULL),
(19, 'Kerala', '2020-11-05 08:58:32', NULL),
(20, 'Lakshadweep', '2020-11-05 08:58:32', NULL),
(21, 'Madhya Pradesh', '2020-11-05 08:58:32', NULL),
(22, 'Maharashtra', '2020-11-05 08:58:32', NULL),
(23, 'Manipur', '2020-11-05 08:58:32', NULL),
(24, 'Meghalaya', '2020-11-05 08:58:32', NULL),
(25, 'Mizoram', '2020-11-05 08:58:32', NULL),
(26, 'Nagaland', '2020-11-05 08:58:32', NULL),
(27, 'Narora', '2020-11-05 08:58:32', NULL),
(28, 'Natwar', '2020-11-05 08:58:32', NULL),
(29, 'Odisha', '2020-11-05 08:58:32', NULL),
(30, 'Paschim Medinipur', '2020-11-05 08:58:32', NULL),
(31, 'Pondicherry', '2020-11-05 08:58:32', NULL),
(32, 'Punjab', '2020-11-05 08:58:32', NULL),
(33, 'Rajasthan', '2020-11-05 08:58:32', NULL),
(34, 'Sikkim', '2020-11-05 08:58:32', NULL),
(35, 'Tamil Nadu', '2020-11-05 08:58:32', NULL),
(36, 'Telangana', '2020-11-05 08:58:32', NULL),
(37, 'Tripura', '2020-11-05 08:58:32', NULL),
(38, 'Uttar Pradesh', '2020-11-05 08:58:32', NULL),
(39, 'Uttarakhand', '2020-11-05 08:58:32', NULL),
(40, 'Vaishali', '2020-11-05 08:58:32', NULL),
(41, 'West Bengal', '2020-11-05 08:58:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int UNSIGNED NOT NULL,
  `category_id` int UNSIGNED DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `title`, `slug`, `icon`, `image`, `description`, `meta_title`, `meta_keywords`, `meta_description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'BOY', 'boy', 'subcategory/1611292573.jpg', 'subcategory/1611292573.jpg', 'BOY', NULL, NULL, NULL, 1, '2021-01-19 22:46:34', '2021-01-21 18:17:03', NULL),
(2, 1, 'GIRL', 'girl', 'subcategory/1611296337.jpg', 'subcategory/1611296337.jpg', 'Girl', NULL, NULL, NULL, 1, '2021-01-19 23:01:54', '2021-01-21 19:19:21', NULL),
(3, 1, 'BY PRICE', 'by-price', 'subcategory/1611301532.jpg', 'subcategory/1611301532.jpg', 'By price', NULL, NULL, NULL, 1, '2021-01-19 23:07:19', '2021-01-21 20:45:54', NULL),
(4, 3, 'BY CATEGORY', 'by-category', 'subcategory/1611313977.jpg', 'subcategory/1611313977.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-19 23:10:54', '2021-01-22 00:13:16', NULL),
(5, 1, 'BY AGE', 'by-age', 'subcategory/1611297948.jpg', 'subcategory/1611297948.jpg', 'By Age', NULL, NULL, NULL, 1, '2021-01-19 23:11:59', '2021-01-21 19:46:10', NULL),
(6, 1, 'ON SALE', 'on-sale', 'subcategory/1611299296.jpg', 'subcategory/1611299296.jpg', 'On sale', NULL, NULL, NULL, 1, '2021-01-19 23:16:11', '2021-01-21 20:08:35', NULL),
(7, 3, 'LEARNING', 'learning', 'subcategory/1611316807.jpg', 'subcategory/1611316807.jpg', 'LEARNING', NULL, NULL, NULL, 1, '2021-01-19 23:18:00', '2021-01-27 21:04:16', '2021-01-27 21:04:16'),
(8, 1, 'BY THEME', 'by-theme', 'subcategory/1611310545.jpg', 'subcategory/1611310545.jpg', 'By Theme', NULL, NULL, NULL, 1, '2021-01-19 23:29:35', '2021-01-21 23:16:05', NULL),
(9, 3, 'BY AGE', 'by-age', 'subcategory/1611318583.jpg', 'subcategory/1611318583.jpg', 'BY AGE', NULL, NULL, NULL, 1, '2021-01-20 01:17:07', '2021-01-22 01:30:01', NULL),
(10, 3, 'BY PRICE', 'by-price', 'subcategory/1611319393.jpg', 'subcategory/1611319393.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-20 01:29:56', '2021-01-22 01:43:34', NULL),
(11, 3, 'ON SALE', 'on-sale', 'subcategory/1611320866.jpg', 'subcategory/1611320866.jpg', 'On Sale', NULL, NULL, NULL, 1, '2021-01-20 01:37:02', '2021-01-22 02:08:02', NULL),
(12, 3, 'BY NEED', 'by-need', 'subcategory/1611321516.jpg', 'subcategory/1611321516.jpg', 'BY NEED', NULL, NULL, NULL, 1, '2021-01-20 01:39:22', '2021-01-22 02:19:57', NULL),
(13, 3, 'RIDES & FUN', 'rides-fun', 'subcategory/1611318335.jpg', 'subcategory/1611318335.jpg', 'RIDES&FUN', NULL, NULL, NULL, 1, '2021-01-20 18:09:31', '2021-01-22 01:25:56', NULL),
(14, 3, 'BY BRANDS', 'by-brands', 'subcategory/1611208320.jpg', 'subcategory/1611322319.jpg', 'BY BRAND', NULL, NULL, NULL, 1, '2021-01-20 18:53:13', '2021-01-22 02:32:11', NULL),
(15, 10, 'GIRL FASHION', 'girl-fashion', 'subcategory/1611212703.jpg', 'subcategory/1611212703.jpg', 'GIRL FASHION', NULL, NULL, NULL, 1, '2021-01-20 20:05:44', '2021-01-24 01:44:13', '2021-01-24 01:44:13'),
(16, 10, 'BOY FASHION', 'boy-fashion', 'subcategory/1611213331.jpg', 'subcategory/1611213331.jpg', 'BOY FASHION', NULL, NULL, NULL, 1, '2021-01-20 20:15:48', '2021-01-24 02:08:13', '2021-01-24 02:08:13'),
(17, 10, 'FEEDING', 'feeding', 'subcategory/1611494587.jpg', 'subcategory/1611494587.jpg', 'FEEDING', NULL, NULL, NULL, 1, '2021-01-20 20:26:23', '2021-01-24 02:23:26', NULL),
(18, 10, 'BATH TIME', 'bath-time', 'subcategory/1611496259.jpg', 'subcategory/1611496259.jpg', 'BATHING & GROOMING', NULL, NULL, NULL, 1, '2021-01-20 20:34:20', '2021-01-24 03:56:10', NULL),
(19, 10, 'ELECTRONICS', 'electronics', 'subcategory/1611497551.jpg', 'subcategory/1611497551.jpg', 'ELECTRONICS', NULL, NULL, NULL, 1, '2021-01-20 20:36:34', '2021-01-27 20:33:47', '2021-01-27 20:33:47'),
(20, 10, 'FURNITURE & MORE', 'furniture-more', 'subcategory/1611499476.jpg', 'subcategory/1611499476.jpg', 'FURNITURE & MORE', NULL, NULL, NULL, 1, '2021-01-20 20:38:36', '2021-01-24 03:45:03', NULL),
(21, 10, 'SLEEP TIME', 'sleep-time', 'subcategory/1611498470.jpg', 'subcategory/1611498470.jpg', 'SLEEP TIME', NULL, NULL, NULL, 1, '2021-01-20 20:44:30', '2021-01-24 03:28:10', NULL),
(22, 10, 'BY PRICE', 'by-price', 'subcategory/1611505902.jpg', 'subcategory/1611505902.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-20 20:46:07', '2021-01-24 05:32:07', NULL),
(23, 10, 'BY NEED', 'by-need', 'subcategory/1611215225.jpg', 'subcategory/1611215225.jpg', 'BY NEED', NULL, NULL, NULL, 1, '2021-01-20 20:47:51', '2021-01-24 05:29:49', '2021-01-24 05:29:49'),
(24, 10, 'ON SALE', 'on-sale', 'subcategory/1611215375.jpg', 'subcategory/1611507721.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-20 20:50:02', '2021-01-24 06:02:24', NULL),
(25, 10, 'GIRL FASHION', 'girl-fashion', 'subcategory/1611492159.jpg', 'subcategory/1611492159.jpg', 'GIRL FASHION', NULL, NULL, NULL, 1, '2021-01-24 01:43:02', '2021-01-24 01:43:02', NULL),
(26, 10, 'BOY FASHION', 'boy-fashion', 'subcategory/1611493949.jpg', 'subcategory/1611493949.jpg', 'BOY FASHION', NULL, NULL, NULL, 1, '2021-01-24 02:13:01', '2021-01-24 02:13:01', NULL),
(27, 2, 'BOY', 'boy', 'subcategory/1611726505.jpg', 'subcategory/1611726505.jpg', 'BOY', NULL, NULL, NULL, 1, '2021-01-26 18:50:29', '2021-01-26 18:50:29', NULL),
(28, 2, 'GIRL', 'girl', 'subcategory/1611728663.jpg', 'subcategory/1611728663.jpg', 'GIRL', NULL, NULL, NULL, 1, '2021-01-26 19:25:01', '2021-01-26 19:25:01', NULL),
(29, 2, 'BY AGE', 'by-age', 'subcategory/1611733119.jpg', 'subcategory/1611733119.jpg', 'BY AGE', NULL, NULL, NULL, 1, '2021-01-26 20:39:04', '2021-01-26 20:39:04', NULL),
(30, 2, 'BY PRICE', 'by-price', 'subcategory/1611733715.jpg', 'subcategory/1611733715.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-26 20:49:04', '2021-01-26 20:49:04', NULL),
(31, 2, 'ON SALE', 'on-sale', 'subcategory/1611735089.jpg', 'subcategory/1611735089.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-26 21:12:08', '2021-01-26 21:12:08', NULL),
(32, 2, 'BY NEED', 'by-need', 'subcategory/1611736192.jpg', 'subcategory/1611736192.jpg', 'BY NEED', NULL, NULL, NULL, 1, '2021-01-26 21:30:11', '2021-01-26 21:30:11', NULL),
(33, 8, 'BY CATEGORY', 'by-category', 'subcategory/1611737988.jpg', 'subcategory/1611737988.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-26 22:00:19', '2021-01-26 22:00:19', NULL),
(34, 8, 'BY PRICE', 'by-price', 'subcategory/1611741100.jpg', 'subcategory/1611741100.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-26 22:52:46', '2021-01-26 22:52:46', NULL),
(35, 8, 'ON SALE', 'on-sale', 'subcategory/1611742506.jpg', 'subcategory/1611742506.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-26 23:15:38', '2021-01-26 23:15:38', NULL),
(36, 8, 'BY AGE', 'by-age', 'subcategory/1611744079.jpg', 'subcategory/1611744079.jpg', 'BY AGE', NULL, NULL, 'BY AGE', 1, '2021-01-26 23:41:46', '2021-01-26 23:41:46', NULL),
(37, 8, 'MOM SPECIAL', 'mom-special', 'subcategory/1611744765.jpg', 'subcategory/1611744765.jpg', 'MOM SPECIAL', NULL, NULL, NULL, 1, '2021-01-26 23:53:03', '2021-01-26 23:53:03', NULL),
(38, 13, 'BY CATEGORY', 'by-category', 'subcategory/1611748171.jpg', 'subcategory/1611748171.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-27 00:50:11', '2021-01-27 00:50:11', NULL),
(39, 13, 'FOR BABIES', 'for-babies', 'subcategory/1611750992.jpg', 'subcategory/1611750992.jpg', 'FOR BABIES', NULL, NULL, NULL, 1, '2021-01-27 01:36:50', '2021-01-27 01:36:50', NULL),
(40, 13, 'ART & CRAFT', 'art-craft', 'subcategory/1611751691.jpg', 'subcategory/1611751691.jpg', 'ART & CRAFT', NULL, NULL, NULL, 1, '2021-01-27 01:48:39', '2021-01-27 01:48:39', NULL),
(41, 13, 'STATIONARY', 'stationary', 'subcategory/1611752374.jpg', 'subcategory/1611752374.jpg', 'STATIONARY', NULL, NULL, NULL, 1, '2021-01-27 01:59:56', '2021-01-27 02:01:47', NULL),
(42, 13, 'BY PRICE', 'by-price', 'subcategory/1611755184.jpg', 'subcategory/1611755184.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-27 02:46:51', '2021-01-27 02:46:51', NULL),
(43, 13, 'ON SALE', 'on-sale', 'subcategory/1611764390.jpg', 'subcategory/1611764390.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-27 05:20:11', '2021-01-27 05:20:11', NULL),
(44, 7, 'BY CATEGORY', 'by-category', 'subcategory/1611766026.jpg', 'subcategory/1611766026.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-27 05:47:25', '2021-01-27 05:47:25', NULL),
(45, 7, 'DIAPERS & MORE', 'diapers-more', 'subcategory/1611767538.jpg', 'subcategory/1611767538.jpg', 'DIAPERS & MORE', NULL, NULL, NULL, 1, '2021-01-27 06:12:41', '2021-01-27 06:12:41', NULL),
(46, 7, 'BY AGE', 'by-age', 'subcategory/1611768866.jpg', 'subcategory/1611768866.jpg', 'BY AGE', NULL, NULL, NULL, 1, '2021-01-27 06:35:16', '2021-01-27 06:35:16', NULL),
(47, 7, 'BY PRICE', 'by-price', 'subcategory/1611787382.jpg', 'subcategory/1611787382.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-27 11:43:32', '2021-01-27 11:43:32', NULL),
(48, 7, 'ON SALE', 'on-sale', 'subcategory/1611788133.jpg', 'subcategory/1611788133.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-27 11:55:53', '2021-01-27 11:55:53', NULL),
(49, 5, 'BY CATEGORY', 'by-category', 'subcategory/1611788896.jpg', 'subcategory/1611788896.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-27 12:08:38', '2021-01-27 12:08:38', NULL),
(50, 5, 'BY AGE', 'by-age', 'subcategory/1611790241.jpg', 'subcategory/1611790241.jpg', 'BY AGE', NULL, NULL, NULL, 1, '2021-01-27 12:30:56', '2021-01-27 12:30:56', NULL),
(51, 5, 'BY PRICE', 'by-price', 'subcategory/1611791865.jpg', 'subcategory/1611791865.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-27 12:58:03', '2021-01-27 12:58:03', NULL),
(52, 5, 'ON SALE', 'on-sale', 'subcategory/1611792501.jpg', 'subcategory/1611792501.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-27 13:08:53', '2021-01-27 13:08:53', NULL),
(53, 14, 'BY CATEGORY', 'by-category', 'subcategory/1611793281.jpg', 'subcategory/1611793281.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-27 13:21:41', '2021-01-27 13:21:41', NULL),
(54, 14, 'BY AGE', 'by-age', 'subcategory/1611794372.jpg', 'subcategory/1611794372.jpg', 'BY AGE', NULL, NULL, NULL, 1, '2021-01-27 13:39:48', '2021-01-27 13:39:48', NULL),
(55, 14, 'BY PRICE', 'by-price', 'subcategory/1611794750.jpg', 'subcategory/1611794750.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-27 13:46:08', '2021-01-27 13:46:08', NULL),
(56, 14, 'ON SALE', 'on-sale', 'subcategory/1611795192.jpg', 'subcategory/1611795192.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-27 13:53:35', '2021-01-27 13:53:35', NULL),
(57, 16, 'BY CATEGORY', 'by-category', 'subcategory/1611810499.jpg', 'subcategory/1611810499.jpg', 'BY CATEGORY', NULL, NULL, NULL, 1, '2021-01-27 18:08:38', '2021-01-27 18:08:38', NULL),
(58, 16, 'BY AGE', 'by-age', 'subcategory/1611811156.jpg', 'subcategory/1611811156.jpg', 'BY AGE', NULL, NULL, NULL, 1, '2021-01-27 18:19:43', '2021-01-27 18:19:43', NULL),
(59, 16, 'BY PRICE', 'by-price', 'subcategory/1611811830.jpg', 'subcategory/1611811830.jpg', 'BY PRICE', NULL, NULL, NULL, 1, '2021-01-27 18:30:48', '2021-01-27 18:30:48', NULL),
(60, 16, 'ON SALE', 'on-sale', 'subcategory/1611813493.jpg', 'subcategory/1611813493.jpg', 'ON SALE', NULL, NULL, NULL, 1, '2021-01-27 18:58:35', '2021-01-27 18:58:35', NULL),
(61, 10, 'SCHOOL SUPPLIES', 'school-supplies', 'subcategory/1611821163.jpg', 'subcategory/1611821163.jpg', 'SCHOOL SUPPLIES', NULL, NULL, NULL, 1, '2021-01-27 21:06:28', '2021-01-27 21:06:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `terms_conditions`
--

CREATE TABLE `terms_conditions` (
  `id` int NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terms_conditions`
--

INSERT INTO `terms_conditions` (`id`, `name`, `display_name`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'privacypolicy', 'Privacy Policy', '<h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Introduction</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">The website [</span><a href=\"https://www.fabpik.in/\" style=\"color: rgb(63, 63, 63); margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; touch-action: manipulation; transition: all 0.25s ease 0s;\">www.fabpik.in</a><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">] is a service provided by Fabpik Ecomm LLP..This support Notice describes the types of personal information we collect, how we store, handle and use the information, with whom we share it, and the choices you can make about our collection, use and disclosure of the information. We also describe the measures we take to protect the security of the information and how you can contact us about our support practices. This support Notice is incorporated in the Terms of Use and therefore governs your use of the FABPIK websites and/or any services offered by FABPIK. By visiting the FABPIK websites and/or utilizing any services offered by FABPIK you agree to and accept the then current practices and policies governing your use of the FABPIK websites and/or utilizing any services offered by FABPIK. Further, by accepting this support Notice, you consent to the manner of collection, handling, usage, disclosure and transfer of your personal information as set out in this support Notice.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">The phrase “personal information” refers to any information by which you or the device you are using to connect to the Internet can be identified.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Information we collect</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We may obtain personal information about you from various sources, including this website and other FABPIK websites (this includes any information that you may provide (i) while registering as a member, (ii) engaging in transactions, (iii) searching products and/or services, etc.), mobile applications, when you call or email us or communicate with us through social media, or when you participate in chats, forums, opinion polls, surveys, bulletin boards, discussion boards or in events or other promotions. We also may obtain information about you from our parent, affiliate or subsidiary companies, business partners and other third parties.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">The types of personal information we may obtain include:</span></p><ul style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Your contact information (such as name, Twitter handle, postal and email address, or phone number)</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Contact information of friends or other people you would like us to contact</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Username and password for the account you may establish on our sites</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Your billing address</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Your shipping information (including the shipping address and phone number) or shipping information of other people to whom purchases have been shipped at your request</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Demographic information (such as age, date of birth and gender)</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Information you provide by interacting with us through social media, including photographs</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Shopping behaviour and preferences, and a record of the purchases you make on our websites and product searches conducted by you</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Other details that you may submit to us or that may be included in the information provided to us by third parties (for e.g. updated delivery addresses provided by our carriers, etc.)</span></li></ul><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">In addition, when you visit our websites, we may collect certain information by automated means, such as cookies and web beacons. A “cookie” is a text file that websites send to a visitor‘s computer or other Internet-connected device to uniquely identify the visitor’s browser or to store information or settings in the browser. A “web beacon,” also known as an Internet tag, pixel tag or clear GIF, is used to transmit information back to a web server. We also may use third-party website analytics tools (such as Google), that collect information about visitor traffic on our sites and mobile applications. The information we may collect by automated means includes:</span></p><ul style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Information about the devices our visitors use to access the Internet (such as the IP address and the device, browser and operating system type)</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">URLs that refer visitors to our sites</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Dates and times of visits to our sites</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Information on actions taken on our sites (such as page views and site navigation patterns)</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">A general geographic location (such as country and city) from which a visitor accesses our websites</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Search terms that visitors use to reach our sites</span></li></ul><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">How we use the information we collect</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We may use the information we obtain about you to:</span></p><ul style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Register you for membership at our websites, and manage and maintain your account on the sites</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Provide products or services you request</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Process, validate, confirm, verify, deliver and track your purchases (including by processing payment card transactions,[arranging shipping and handling returns and refunds, and contacting you about your orders, including by telephone)</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Maintain a record of the purchases you make on our sites</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Respond to your questions and comments and provide customer support</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Communicate with you about our products, services, offers, events and promotions, and offer you products and services we believe may be of interest to you</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Enable you to communicate with us through our blogs, social networks and other interactive media</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Publish your testimonials about FABPIK, including on our websites and blogs, and on social networks (if we choose to publish your testimonial, we will include only your first name, last initial, city and state)</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Manage your participation in our events and other promotions</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Tailor our products and services to suit your personal interests and the manner in which visitors use our sites, applications and social media assets</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Operate, evaluate and improve our business and the products and services we offer</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Analyse and enhance our marketing communications and strategies (including by identifying when emails sent to you have been received and read)</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Analyse trends and statistics regarding visitors’ use of our sites, mobile applications and social media assets, and the purchases visitors make on our sites</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Protect against and prevent fraud, unauthorized transactions, claims and other liabilities, and manage risk exposure, including by identifying potential hackers and other unauthorized users</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Enforce our Website Terms of Use and Product Policies</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Comply with applicable legal requirements and industry standards and our policies</span></li></ul><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">In addition to the data uses described above, we may use the information collected through cookies and other automated means to uniquely identify the electronic shopping basket you may create on our sites and enable you to retrieve shopping baskets you previously created. We also may use cookies to identify and authenticate visitors.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We may combine the information we collect with publicly available information and information we receive from our parent, affiliate or subsidiary companies, business partners and other third parties. We may use that combined information to enhance and personalize your shopping experience with us, to communicate with you about products, services and events that may be of interest to you, for other promotional purposes, and for other purposes described in this section.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We also may use the information we obtain about you in other ways for which we provide specific notice at the time of collection.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Online advertising</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">On our websites, we may collect information about your online activities to provide advertising about products and services tailored to your individual interests. You may see certain ads on this and other FABPIK websites because we participate in advertising networks. Ad networks allow us to target our advertising to users through demographic, behavioural and contextual means. These networks track your online activities over time by collecting information through automated means, including through the use of cookies, web server logs, web beacons and other methods. The networks use this information to show you advertisements for FABPIK and our business partners that are tailored to your individual interests. The information our ad network vendors collect includes information about your visits to websites that participate in the vendors‘ advertising networks, such as the pages or advertisements you have viewed, and the actions you take on the sites. This data collection and ad targeting take place both on our websites and on third-party websites that participate in the ad networks. This process also helps us track the effectiveness of our marketing efforts.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Information we share</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We do not sell or otherwise disclose personal information about you, except as described in this support Notice.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We may share the personal information we collect with our parent, affiliate and subsidiary companies, business partners, franchisees, marketing agents, ad network vendors and their participants, and other third parties for the purposes described in this support Notice, including to communicate with you about products and services, offers, events and promotions that we believe may be of interest to you. We may also share personal information with our service providers who perform services on our behalf. On your behalf FABPIK holds the right to use your particulars for forwarding/returning your order.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">These service providers are not authorized by us to use or disclose your personal information except as necessary to perform services on our behalf or comply with legal requirements. We also may disclose information about you (i) if we are required to do so by law or legal process (such as a court order), (ii) in response to a request by law enforcement authorities for the purposes of verification of identity, prevention, detection, investigation including cyber incidents, prosecution and punishment of offences or (iv) when we believe disclosure is necessary or appropriate to prevent physical harm or financial loss or in connection with an investigation of suspected or actual illegal activity.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We also reserve the right to transfer personal information we have about you in the event we sell, merge or transfer all or a portion of our business or assets. We would not be obliged to provide you with a prior notice of such sale, merger or transfer and you hereby consent to the consequent transfer of your personal information to the transferee entity. Following such a sale, merger or transfer, you may contact the entity to which we transferred your personal information with any inquiries concerning the processing of that information. In the event of our bankruptcy, insolvency, administration or enforcement of any of our creditors’ rights generally which lead to any such similar action, we would not have control over the manner in which your personal information is used. In such an event your personal information may be treated as any other asset of ours and maybe transferred to or shared with any third party and may be used in a manner not contemplated in this support Notice without obtaining your consent or having provided a notice to you.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Your choices</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We offer you certain choices about what information we collect from you, how we use and disclose the information, and how we communicate with you.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You may withdraw any consent you previously provided to us or object at any time on legitimate grounds to the processing of your personal information. We will apply your preferences going forward. In some circumstances, withdrawing your consent to FABPIK’s use or disclosure of your personal information will mean that you cannot take advantage of certain FABPIK offerings or site features. Here are the choices we offer:</span></p><h3 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 22px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Sharing Information with Business Partners</span></h3><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You may direct FABPIK not to share your personal information with our business partners for those partners’ own purposes. To do so, please email us at support@FABPIK.in. If you opt out, we will continue sharing your information as otherwise described in the “Information We Share” section of this support Notice, including with our franchisees and service providers, to fulfil your requests and to comply with legal requirements.</span></p><h3 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 22px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Marketing Emails</span></h3><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You may choose not to receive marketing email and promotional communications from us by clicking on the unsubscribe link in the marketing emails or by adjusting your email preferences using the online account you may establish on our sites.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\">&nbsp;</p><h3 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 22px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Cookies</span></h3><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Most browsers will tell you how to stop accepting new cookies, how to be notified when you receive a new cookie, and how to disable existing cookies. Please note, however, that without cookies you may not be able to take full advantage of all of our sites’ features. In addition, disabling cookies may cancel opt-outs that rely on cookies, such as web analytics or targeted advertising opt-outs.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You may not choose to opt-out of non-promotional communications. If you wish not to receive any non-promotional communication from us, you may choose to terminate your account with FABPIK.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Access and correction</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You may obtain a copy of certain personal information we maintain about you or update or correct inaccuracies in that information using the online account you may establish on our websites. In addition, if you believe other personal information we maintain about you is inaccurate, you may request that we correct or amend the information by contacting us as indicated in the “How to Contact Us” section of this support Notice. If we deny an access request, we will notify you of the reasons for the denial.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Links to Other Websites</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Our websites may contain links to other sites for your convenience and information. These sites may be operated by companies not affiliated with FABPIK. Linked sites may have their own support notices, which you should review if you visit those websites. We are not responsible for the content of any websites not affiliated with FABPIK, any use of those sites, or those sites’ support practices.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Data transfers</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">When we obtain personal information about you, we may process the information outside of the country in which you are located. The countries in which we process the information may not have the same data protection laws as the country in which you are located. We will protect your information as described in this support Notice.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">How We Protect Personal Information</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We maintain administrative, technical and physical safeguards designed to assist us in protecting the personal information we collect against accidental, unlawful or unauthorized destruction, loss, alteration, access, disclosure or use. For example, we use Secure Sockets Layer encryption to protect certain of your purchase information while in transit.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Please note that no electronic transmission of information can be entirely secure. We cannot guarantee that the security measures we have in place to safeguard personal information will never be defeated or fail, or that those measures will always be sufficient or effective.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">To further protect yourself, you should safeguard your FABPIK account user name and password and not share that information with anyone. You should also sign off your account and close your browser window when you have finished your visit to our sites. Please note that we will never ask for your FABPIK account user name or password via email.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Children’s support</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">FABPIK does not direct its websites to children under the age of eighteen. We require registered users of the site to be at least eighteen years old. If we learn that a user is under eighteen years of age, we will promptly delete any personal information that the individual has provided to us.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Updates to this support notice</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">This support Notice may be updated periodically and without prior notice to you to reflect changes in our personal information practices.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">How to contact us</span></h2><p style=\"margin-bottom: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">If you have any questions or comments about this support Notice, or if you would like us to update information we have about you or your preferences, please contact us by email at&nbsp;</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">support@FABPIK.in</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">.</span></p>', 1, '2021-01-29 06:05:55', '2021-01-29 06:05:55', NULL);
INSERT INTO `terms_conditions` (`id`, `name`, `display_name`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'cancellation-refund-policy', 'Cancellation Refund Policy', '<p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Post successful cancellation, refunds would be provided as per applicability. For detailed information, please refer to the&nbsp;</span><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">REFUNDS</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;section under this policy.</span></p><ul style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You can cancel your order online,&nbsp;</span><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">before the product(s) inside the order has been shipped.</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;Applicable refund against the cancelled order will be credited to your respective bank / card / wallet by logging into your FabPik Account (further Refund related details are provided below).</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">In case, even a single product of the order has been shipped, you will not be able to cancel the order (in such cases, user would have to cancel an individual product or complete shipment).</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">In case, the order contains product(s) such as Guaranteed Savings Offer or Gift Certificate, order level cancellation will not be applicable.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You can cancel the product online,</span><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">before the product(s) inside the order has been shipped.</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;Applicable refund against the cancelled product will be credited to your respective bank / card / wallet by logging into your FabPik Account (further Refund related details are provided below).</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">In case, the product has been shipped, you will not be able to cancel individual product (complete shipment would get cancelled in this case).</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">For product(s) such as Guaranteed Savings Offer, Gift Certificate and Gift Wrap, cancellation will not be allowed.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">As a part of cart offer or any other discount offer, if a user has purchased one product along with another product, and during cancellation of same, all the products purchased through same cart offer would get cancelled.</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">For product(s) purchased through specific cart offer or any other discount offer inside the order, if any one of the product gets delivered to the user, then cancellation will not be applicable for all the other products tagged under same cart offer.</span></li></ul>', 1, '2021-01-29 06:08:12', '2021-01-29 06:08:12', NULL);
INSERT INTO `terms_conditions` (`id`, `name`, `display_name`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'FixedCommission', 'Fixed Commission', '<div class=\"main-page-wrapper\" style=\"margin: -40px 0px 0px; padding: 40px 0px 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><div class=\"container\" style=\"margin-top: 0px; margin-bottom: 0px; padding-top: 0px; padding-bottom: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; width: 1222px; max-width: 1222px;\"><div class=\"row content-layout-wrapper align-items-start\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; display: flex; flex-wrap: wrap; align-items: flex-start !important;\"><div class=\"site-content col-lg-12 col-12 col-md-12\" role=\"main\" style=\"margin: 0px 0px 40px; padding-top: 0px; padding-bottom: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; width: 1222px; flex: 0 0 100%; max-width: 100%;\"><article id=\"post-15603\" class=\"post-15603 page type-page status-publish hentry\" style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><div class=\"entry-content\" style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><h3 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 22px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; text-align: center;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik merchant services agreement</span></h3><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">This document is an electronic record in terms of Information Technology Act, 2000 including all its amendments and rules made thereunder as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures.</p><h6 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; text-align: center;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">E-COMMERCE SERVICES AGREEMENT</span></h6><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">This E-Commerce Services Agreement (hereinafter referred to as “<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Agreement</span>”) is made on the day of your acceptance of this Agreement from your designated electronic mail address or in any other form of electronic record including, if applicable or provided, clicking on the check box or “I Agree” / “Accept” button or by any other means which construe your acceptance of this Agreement (“<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Execution Date</span>”) by and between</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You, the details of which are given by you on the website on which this Agreement appears, a natural or juristic person competent to enter into valid and legally binding contract under applicable Indian laws inter alia, a person of legally sound mind, not adjudicated bankrupt and equal to or more than 18 years of age on the Execution Date. If You are a juristic person then the person accepting this Agreement represents that such person is duly authorized by You to bind You to this Agreement and the designated electronic mail address is valid and subsisting and allotted by You to such person (hereinafter referred to as “<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Merchant</span>” which expression shall unless repugnant to the context and meaning thereof, include its heirs, legal representatives, successors, liquidators, receivers, administrators and permitted assigns), of One Part;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">And</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik Ecomm LLP</span>, a Company incorporated under the provisions of the Indian Companies Act, 1956 and having its registered office at&nbsp;<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">2nd Floor, VSSR Square, Plot No. 64 &amp; 64A, Vittal Rao Nagar, Madhapur, Hyderabad, Telangana, 500081, India</span>. (hereinafter referred to as “<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Service Provider</span>” which expression shall unless repugnant to the context and meaning thereof, include its successors, liquidators and assigns), of Other Part.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Merchant and Service Provider shall hereinafter be individually referred to as “<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Party</span>” and collectively as “<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Parties</span>”.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Whereas</span>,</p><ol style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 1.4; font-family: inherit; list-style: none;\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\">&nbsp;Merchant is inter alia engaged in the business of developing and/or manufacturing and/or selling various goods&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;and/or items and related services in the Territory (“Business”);</li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\">&nbsp;Service Provider is inter alia in the business of developing and operating e-commerce businesses for independent&nbsp; &nbsp; &nbsp; &nbsp;third party retailers and manufacturers and providing for those entities / persons Service Provider’s proprietary&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;technology, website design and development capabilities, order processing capabilities, customer service&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;capabilities, fulfillment capabilities and centralized inventory, invoicing and payment management to enable&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;those&nbsp; entities / persons to offer e-commerce to their customers and such services include Platform Services (as&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;defined hereunder) and Transaction Support Services (as defined hereunder) (“Service Provider Business”);</li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\">&nbsp;Merchant has approached Service Provider to avail Service Provider Business for the purpose of Merchant’s&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Business and Service Provider has agreed to make available Service Provider Business to Merchant;</li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\">Service Provider has made and is in the process of making substantial investment both monetary, knowhow and otherwise to establish its trade name among consumers and distributors so as to create a retail image connoting a specific manner in which goods, items and services can be presented on and sold through the Platform;</li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\">Both Service Provider and Merchant recognize that overall success of the Platform and trade names of the Service Provider and its Affiliates depends on the users of the Platform and public in general perceives Platform as a trusted online and electronic marketplace to buy and sell goods and services;</li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\">The Parties wish to enter into this Agreement to document and record their mutual understandings and agreements in relation to the terms and conditions on which Service Provider shall make available Service Provider Business to Merchant and Merchant shall avail Service Provider Business;</li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\">These recitals shall form part of the Agreement.</li></ol><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Now therefore</span>, in consideration of the mutual promises and other consideration, the sufficiency of which is acknowledged, the Parties, intending to be legally bound, agree as follows:</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">1.DEFINITIONS</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“<em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Affiliate</span></em>” shall mean, with respect to each Party, any person or entity directly or indirectly through one or more intermediary Controlling, Controlled by, or under direct or indirect common Control with a Party. “Control”, “Controlled” or “Controlling” shall mean, with respect to any person or entity, any circumstance in which such person or entity is controlled by another person or entity by virtue of the latter person or entity controlling the composition of the board of directors or managers or owning the largest or controlling percentage of the voting securities of such person/entity or otherwise controlling the other.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“<em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Brand</span></em>” or “<em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Brand Name</span></em>” shall mean “Fabpik” or such other successor or replacement brand name / trademark / service mark as may be decided by the Service Provider upon a prior intimation to the Merchant.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“<em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Confidential Information</span></em>” means and includes any and all information which is confidential to a Party including any (i) business information and business processes, (ii) any samples, formulations, specifications, data relating to manufacturing and quality control processes and procedures, (iii) advertising and marketing plans, (iv) any past, current or proposed development projects or plans for future development work, (v) technical, marketing, financial and commercial information whether relating to past or current or future,<br>(vi) the commercial and business affairs of a Party, (vi) all customer related information including any rates and discounts and (vii) and with respect to the Service Provider shall include the End Customer Database.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“<em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Deliverable(s)</em>”</span>&nbsp;shall mean the specific materials, devices, products, services or other deliverables that are provided by Merchant to Service Provider during the course of performing Service Provider Business as per this Agreement and any related document thereto.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“<em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">End Customer</em>”</span>&nbsp;shall mean the retail customers to whom Merchant offers to sell or sells or from whom Merchant receives offers to purchase the Products through the Platform.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“End Customer Database”</span></em>&nbsp;shall mean all data / information (as may be updated from time to time) about the persons/ entities including their names, addresses, contact details, queries, orders and other requests made available by such persons / entities on the Platform or otherwise captured by the Platform that shall further include the usage, behavior, trends and other statistical information / data relating to such persons<br>/ entities, who (i) access the Platform or otherwise get invitation to the Platform or correspond with the Platform, (ii) place any order for Products on the Platform, or (iii) send any enquiry/ request with respect to the Platform, and shall include all analysis and records based on such aforementioned information, including the spending and other patterns of such persons/entitles and Products. For the avoidance of doubt, any list, description or other grouping of consumers or customers or any derivative work from End Customer Database shall be deemed to be End Customer Database.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Intellectual Property”</span></em>&nbsp;includes ideas, concepts, creations, discoveries, inventions, improvements, know how, trade or business secrets; trademarks, service marks, domain names, designs, utility models, tools, devices, models, methods, patents, copyright (including all copyright in any designs and any moral rights), masks rights, design right, procedures, processes, systems, principles, algorithms, works of authorship, flowcharts, drawings, books, papers, models, sketches, formulas, teaching techniques, electronic codes, proprietary techniques, research projects, and other confidential and proprietary information, computer programming code, databases, software programs, data, documents, instruction manuals, records, memoranda, notes, user guides; in either printed or machine-readable form, whether or not copyrightable or patentable, or any written or verbal instructions or comments. The End Customer Database shall be considered to be the Intellectual Property of the Service Provider.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Intellectual Property Rights”</span></em>&nbsp;means and includes (i) all rights, title or interest under any statute or under common law or under customary usage including in any Intellectual Property or any similar right, anywhere in the world, whether negotiable or not and whether registrable or not, (ii) any licenses, permissions and grants in Intellectual Property (iii) applications for any of the foregoing and the right to apply for them in any part of the world and (iv) all extensions and renewals thereto.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Payment Facilitation Services”</span></em>&nbsp;shall mean facilitating the receipt of Sale Price on the Platform either along with Platform Services or otherwise (for example cash on delivery services).</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Platform”</span></em>&nbsp;shall mean the website with a second level domain name / uniform resource locator (URL) bearing the Brand Name with any top level domain name whether presently available for registration or made available for registration at any future date.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Platform Services”</span></em>&nbsp;internet based electronic platform in the form of an intermediary to facilitate sale and purchase of goods and services through Platform.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Product(s)”</span></em>&nbsp;shall mean any and all&nbsp;<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">goods</span>&nbsp;and related services of the Merchant for which Service Provider makes available Service Provider Business to the Merchant.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Sale Price”</span></em>&nbsp;shall be the price at which the Product is offered for sale by the Merchant on the Platform by using Platform Services to the End Customer. Parties agree that Sale Price is dynamic and volatile and may vary at different times and points of sale and therefore can be periodically and from time to time changed or revised by the Merchant in accordance with the terms of this Agreement.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Service Fees”</span></em>&nbsp;shall mean the fees for availing either whole or part of the Service Provider Business in accordance with the terms of this Agreement and/or Commercial Terms (<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">the term as defined in the Agreement</span>).</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Service Provider Business”</span></em>&nbsp;shall have the meaning as set out Recital 2 herein above and shall include Platform, Platform Services, Payment Facilitation Services and Transaction Support Services.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Service Provider Content”</span></em>&nbsp;shall mean the Platform, all the pages of the Platform, all the content contained in the Platform (excluding any third party content and advertisements), look and feel of the Platform, any and all information or content owned or controlled (e.g. by license or otherwise) by Service<br>Provider or its Affiliates, including text, images, graphics, photographs, video and audio, and furnished by Service Provider or its Affiliates in connection with Platform Services, Transaction Support Services, Payment Facilitation Services and for the purpose of offering for sale of Products by the Merchant.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Term”</span></em>&nbsp;shall have the meaning as set out in Section 13.1 hereto.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Territory”</span></em>&nbsp;shall mean the entire world.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><em style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">“Transaction Support Services”</span></em>&nbsp;shall include services in relation to support the sale of the goods and services by the Merchant to End Customer which shall include product listings, warehousing services, logistics management services, Payment Facilitation Services, customer support services and any other additional services that may be agreed between the parties.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">2.INTERPRETATION</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">In this Agreement, unless the context otherwise requires:</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(i) Words importing persons or parties shall include natural person, entity, partnership firm, organization, operation, Company, HUF, voluntary association, LLP, joint venture, trust, limited organization, unlimited organization or any other organization having legal capacity;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(ii) Words importing the singular shall include the plural and vice versa, where the context so requires;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(iii) References to any law shall include such law as from time to time enacted, amended, supplemented or re-enacted;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(iv) Reference to one gender shall include a reference to the other genders;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(v) References to the words “include” or “including” shall be construed without limitation;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(vi) References to this Agreement or any other agreement, deed, instrument or document shall be construed as a reference to this Agreement , such other agreement, deed, instrument or document as the same may from time to time be amended, varied, supplemented or novated in accordance with the terms of this Agreement;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(vii) The headings and titles in this Agreement are indicative and shall not be deemed part thereof or be taken into consideration in the interpretation or construction of this Agreement;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(viii) The word ‘written’ shall include writing in electronic form and ‘signed’ shall include electronic signature or any other electronic communication which signifies the sender’s or originator’s intention to be bound by such electronic communication.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">3.SERVICES</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">3.1 The Merchant appoints the Service Provider and Service Provider accepts such appointment to make available the Service Provider Business to the Merchant for the Products and in accordance with the terms of this Agreement and as further agreed in commercial understanding electronic document or any other similar or analogous electronic or other document (“Commercial Terms”) and in accordance with various Platform rules and policies including privacy policy (“Platform Policies”). The Commercial Terms and Platform Policies are deemed to have been incorporated in this Agreement by way of reference.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">3.2 Merchant agrees and acknowledges that Service Provider is free to provide Service Provider Business and in the Territory in any manner and for any consideration as may be decided by Service Provider in its sole and absolute discretion.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">3.3 Service Provider in its sole and absolute discretion may refuse to provide any one or more of the Service Provider Business including Platform, Platform Services, Payment Facilitation Services and/or Transaction Support Services for any reason whatsoever and especially if providing such Service Provider Business to the Merchant can be detrimental to the reputation, goodwill and competitiveness of Service Provider or could cause any breach of any contractual commitments of the Service Provider and cause Service Provider to breach any applicable laws.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">3.4 In the event of any conflict or inconsistency between the terms of this Agreement and the terms of any Commercial Terms thereto, the terms of this Agreement shall prevail to the extent of such conflict or inconsistency.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">4.ADVERTISING, MARKETING AND SALES PROMOTION</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">4.1 Service Provider as the proprietor and owner of the Platform and Platform Services and rights holder of the Brand Name may at its sole discretion carry out advertising and marketing activities in relation to<br>promotion of the Platform, Platform Services and Brand Name in any manner and to any extent as may be deemed fit by the Service Provider and for such purposes may engage in certain sales promotion activities to increase the sales of Products on the Platform. Service Provider and Merchant may agree on certain terms on which Merchant shall support such sales and marketing activities of Service Provider including providing discounts on the Products or other free of cost goods and services to the End Customers.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">4.2 Service Provider may at its sole and absolute discretion on reasonable commercial efforts basis market, promote or advertise the Products made available for sale by Merchant on the Platform in compliance with this Agreement.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">4.3 Where Merchant believes or is notified by other entity including any third party manufacturers of the Products that any promotion plan/ activity undertaken by Service Provider is against any applicable law or in breach of any contractual obligation of Merchant or such third party manufacturer (in both cases supported by a written legal opinion from a reputed advocate), Merchant shall intimate the same to Service Provider and upon such intimation, Service Provider shall within reasonable time cease such plan/ activities.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">4.4 Merchant agrees and acknowledges that Service Provider shall have the sole right (as to between Service Provider and Merchant) for the design, look and feel, architecture, layout, positioning and all aspects of the Platform including listing, positioning, indexing, placement and tiering the Products offered for sale on the Platform by the Merchant and the Merchant shall not question or dispute such exercise of right or discharge of responsibility by the Service Provider.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">4.5 Service Provider shall be solely responsible at Service Provider’s sole discretion to sell or license any and all advertising and promotional time and space with respect to Platform including web-pages or such portions of the Platform that contains the details of the Products. The advertisement and promotions on any part of the Platform may include video advertising, display/banner/text advertisements, including but not limited to medium rectangle, leader-board, roadblock, hyperlink, page branding, framing, widgets, pop- ups, pop-under, network advertisements (for the sake of example, Google AdSense) available on the Platform. Service Provider shall have the sole right and discretion to decide the style, placement and format of the advertisement and promotion and the price and/or any other consideration, if any, for the sale and license of such advertisement and promotion. Except for the facilitation of payment of sale consideration of the Product through Payment Facilitation Services, Service Provider and/or its Affiliates shall be entitled to retain any and all revenues generated from any sales or licenses of all such advertisements and promotions.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">4.6 Service Provider shall reasonably ensure that all advertisement/ promotion activities undertaken by the Service Provider:</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(i)do not contain any material that, in its knowledge, would infringe or violate any intellectual property rights or any other personal or proprietary right of any person; and<br>(ii)are not obscene or libelous; and<br>(iii)comply with all applicable laws including standards and rules set forth by the Advertising Standards Council of India or any other relevant government authority.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">5. END CUSTOMER DATABASE</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">5.1&nbsp; The End Customer Database shall be proprietary to the Service Provider. Service Provider shall alone retain all rights including all Intellectual Property Rights in the End Customer Database and unless specifically agreed to in writing by the Service Provider, no rights in or to the End Customer Database are deemed to have been granted to the Merchant. To the extent Merchant derives any rights in the End Customer Database by virtue of it undertaking the Merchant Business the Merchant shall hold such rights in trust for Service Provider and the Merchant shall do and undertake all such acts to exclusively assign such rights in the End Customer Database to the Service Provider. The Merchant further agrees that (a) all the End Customer Database shall be treated as Confidential Information of the Service Provider for the purposes of this Agreement; (b) Service Provider being the owner and proprietor of the End Customer Database shall be entitled to use, store and exploit the same in any manner as may be deem fit by the Service Provider and in accordance with Service Provider’s privacy policy as provided on the Platform from time to time; and (c) Merchant shall not use the End Customer Database other than selling the Products by availing Service Provider Business or required for law enforcement purposes and shall in no way sell, transfer or otherwise exploit the End Customer Database without the express written consent of the Service Provider.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">6. CONSIDERATION AND PAYMENT TERMS</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">6.1 Payments to be made by Merchant.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">6.1.1 In consideration of the provision of Service Provider Business by the Service Provider, the Merchant shall pay to the Service Provider Service Fees which shall be calculated in the manner as specified in the Commercial Terms.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">6.1.2 Service Fees for any additional services shall be as set out in the Commercial Terms.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">6.1.3 Parties agree that the details of terms memorialized by the Commercial Terms are dynamic in nature and will evolve or vary as the operating, promotional, marketing and business environment of the Service Provider or user behavior on the Platform changes and evolves and therefore the Commercial Terms will be adjusted or revised from time to time or sometime occasionally or frequently by the Parties as necessary or appropriate during the Term of the Agreement to accurately reflect the evolution of the aforesaid environment and conditions. Such revisions would be with the mutual consent of the Parties which consent can be oral, written or implied. For any oral consent, Service Provider may on reasonable basis confirm such oral consent within reasonable time from such consent and through written records including through electronic communications.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">6..2 Payment Terms:</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">6.2.1 Service Provider shall have the right to receive the Services Fees from the amounts due to the Merchant under Payments Facilitation Services. To the extent the Service Provider is unable to receive the Service Fees from the Payment Facilitation Services as aforesaid; the Merchant shall make all payments within ten (10) business days of receipt of the relevant invoice from the Service Provider.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">6.2.2 Merchant shall be entitled to make any deduction or withholding in accordance with applicable law and shall provide the necessary tax deduction certificates to the Service Provider.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">6.3&nbsp;<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Taxes</span>:</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Each Party shall be responsible for any and all taxes on its business, and taxes based on its net income or gross receipts. However, Service Provider shall be entitled to additionally charge GST i.e Goods and Service Tax or any other indirect or transaction taxes as applicable on one or more of the Service Provider Business and Service Fees.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">7. ADDITIONAL OBLIGATIONS OF SERVICE PROVIDER</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">7.1&nbsp; Service Provider shall reasonably maintain the Platform and Platform Services and shall on reasonable efforts basis provide Transaction Support Services and other services comprising Service Provider Business.<br>7.2&nbsp; Service Provider shall reasonably maintain the registration of domain name in relation to the Platform during the Term at its own costs free from any and all encumbrances, including encumbrances which may lead to any adverse effect on Service Provider’s registration of the domain name or its use of the Platform.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">7.3&nbsp; In order to process payments made by End Customers and to generally provide Payment Facilitation Services, Service Provider shall reasonably maintain appropriate contracts with payment gateways and shall comply with the applicable laws.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">7.4&nbsp; Service Provider shall ensure that it has or procures adequate technology as necessary to maintain the Platform and perform the Service Provider Business under this Agreement.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">7.5&nbsp; Service Provider, as a part of Transaction Support Services, shall list the Products on the Platform for the Merchant at the Sale Price provided or informed by the Merchant. Service Provider acknowledges that the Sale Price is dynamic and volatile and may vary at different times and points of sale and that the Merchant reserves the right to change or modify the Sale Price of the Product at any time before the same is bought by the End Customer on the Platform. The intimation of such revisions of the Sale Price could be oral or in writing. For any oral intimation, Service Provider may on reasonable basis confirm such oral intimation within reasonable time from such intimation and through written records including through electronic communications.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8. OBLIGATIONS, COVENANTS AND WARRANTIES OF MERCHANT</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.1&nbsp; Merchant shall not use the Service Provider Business for any purpose other than Merchant’s Business and in relation to the Products.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.2 Merchant shall manage and maintain sufficient inventory of the Products which the Merchant offers to sell to End Customer on the Platform through Platform Services and shall mandatorily deliver the Products as purchased by the End Customer to the Service Provider within such time as may be prescribed in the Commercial Terms.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.3&nbsp; Merchant shall deliver exactly the same Product to the Service Provider for availing Transaction Support Services from Service Provider in relation to the sale of Products to End Customer.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.4 Merchant shall offer the Products for sale on the Platform on the Sale Price which shall be inclusive of all taxes, duties, levies, warehousing, packaging, shipping and logistics charges and all other charges other than any entry taxes / octroi as applicable in the city or municipal limits of the End Customer. The Sale Price shall be in compliance with all applicable laws and shall not be more than the maximum retail price printed on the Products.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.5 Merchant shall undertake all the necessary after sales services to the End Customer including providing warranty/ guarantee / replacement services to the Products.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.6 Merchant shall provide necessary access to the Service Provider to inspect the warehouse, manufacturing facilities or other facilities and offices of the Merchant in order to ensure Merchant is able to comply with its sales obligations to the End Customer. Merchant acknowledges and agrees that this ingress, regress and inspection rights of the Service Provider is to ensure the goodwill of the Platform, Platform Services and Brand Name and to provide good user experience to the End Customer.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.7 Merchant shall ensure that Merchant employs sufficient staff to meet and fulfill the requirements of this Agreement and to sell, deliver and service the Products sold to the End Customers through Platform Services. Merchant shall further ensure that the Merchant’s staff shall participate in the relevant training programs as organized or approved by the Service Provider from time to time.Merchant shall not print, emboss or otherwise display any brand name, trade name, and trademark, service mark on the Product, on the packing material and on the invoice other than those displayed while making the sale offer on the Platform while packing the products for delivery to Service Provider to avail Transactional Support Services.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.8<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;Merchant shall provide series of invoice numbers in relation to the Products sold to the End Customer(s) through Platform Services and such invoice number shall correspond to the books of accounts of the Merchant as maintained by the Merchant under applicable law. As a part of availing Transaction Support Services, the Merchant hereby authorizes Service Provider and Service Provider Affiliates to issue the invoices containing invoice number from the aforesaid series to the End Customer on behalf of the Merchant for the sale of Products.</span>&nbsp;The Merchant further authorizes Service Provider and Service Provider Affiliates to include the Brand Name on the invoice and for the avoidance of doubt, the inclusion of the Brand Name shall not create any relationship of agency, representative, partnership, joint-venture or otherwise between the Merchant and Service Provider and the relationship shall always remain as that of an independent contractor. Merchant acknowledges and agrees that Service Provider shall provide in the invoice all the necessary details of the taxes, duties and other statutory levies applicable on the sale and<br>delivery of the Product(s) to the End Customer and it shall be the duty and obligation of the Merchant to correctly and timely pay or deposit such taxes etc. to the appropriate government and shall indemnify, defend and hold harmless Service Provider, Service Provider Affiliates and their respective shareholders, directors, officers, employees, contractors and agents in the event Merchant defaults in making the payment of such taxes etc.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.9 The Products offered to be sold by the Merchant on the Platform and subsequent delivery of the same to the Service Provider and Deliverables shall (a) exactly conform to the specifications and representations made by the Merchant on the Platform; (b) shall comply with all the applicable laws including that of the territory of the Merchant, the place from where Merchant dispatches the Products to the Service Provider and the place of final delivery to the End Customer; (c) not infringe any third party’s Intellectual Property Rights whether in India or anywhere in the world; and (d) not violate any international trade, import and export related laws including parallel imports.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.10 Merchant agrees and acknowledges that the title in the Products shall only be transferred from Merchant to the End Customer upon delivery of the Products to the End Customer. For the avoidance of doubt, the title and risk on the Products for any delivery of Products to Service Provider for providing any Transaction Support Services before the purchase of Products by the End Customer on the Platform shall always remain with the Merchant. The Merchant may in its sole discretion take appropriate insurances to safeguard itself from any loss, breakage, theft or damage of the Products till such time the Products are actually delivered to the Service Provider and Service Provider has acknowledged the receipt of the delivery of such Products. The Merchant shall be the lawful owner or the lawful right holder in the Products offered and/or sold on the Platform to the End Customer and the Products at time of listing of the same on the Platform and for all times thereafter shall be free from any encumbrance, charge, lien or any security or third party interests. Merchant hereby irrevocably and unconditionally waives all its liens whether contractual, statutory, equitable or otherwise including those related to unpaid seller on the Products once the Products have been delivered by Merchant to either Service Provider or to the End Customer or to any carrier or bailee.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.11 Merchant shall pass on the full warranty or guarantee received on the Products from the third party manufacturers or third party suppliers to the End Customer and shall fully support the End Customer to enforce such warranty or guarantee.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.12 Merchant shall at no point represent or hold itself out as an agent or representative or an Affiliate of the Service Provider. The sale and purchase transaction between Merchant and the End Customer shall be a bi partite contract between them and Service Provider is merely facilitating the transaction between Merchant and End Customer as an intermediary and a conduit by making available Service Provider Business and Payment Facilitation Services to Merchant and End Customer.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.13 Merchant shall provide such necessary assistance (at no additional direct cost to the Service Provider) as may be required to facilitate the Service Provider to carry out its obligations under this Agreement.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.14 Merchant shall not make any representation or do any act which may be taken to indicate that it has any right, title or interest in or to the Brand Name.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.15 Merchant shall not do, cause or authorize to be done anything which will or may:<br>(i)&nbsp; impair, damage or be detrimental to the rights, reputation and goodwill associated with the Service Provider, its Affiliates, shareholders or directors and/or the Brand Name;<br>(ii) bring the Brand Name or the Platform into disrepute or any claim by third parties; or<br>(iii) may jeopardize or invalidate the Brand Name, Platform registration or any rights associated thereto;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.16&nbsp; Merchant shall not use or register anywhere in the world, the Brand Name or any other trade mark, trade name or domain name, except as authorized under this Agreement, which, in Service Provider’s reasonable opinion, is identical, improvement over, dilution of, combination involving or confusingly similar to, the Brand Name or, that constitutes any translation thereof into any language.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">8.17&nbsp; Merchant understands and acknowledges that the Brand Name and reputation of Service Provider is of utmost importance for its business and that the conduct of Merchant in the performance of this Agreement and otherwise would have material impact and bearing on such Brand Name and reputation of Service Provider. Further Merchant understands and acknowledges that the obligations and covenants placed on Merchant in this Section or elsewhere in the Agreement are essential for the maintenance of quality control and protection of Brand Name, and to ensure timely payments to Merchant. Accordingly Merchant acknowledges that no hardship or onerous obligation is being placed on Merchant under this Agreement.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">9.INTELLECTUAL PROPERTY</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">9.1 Intellectual Property Rights In Relation To Brand Name</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">9.1.1 Merchant acknowledges Service Provider’s absolute ownership of, interest in and rights to the Brand Name and the Platform.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">9.1.2 Without limitation to the foregoing, Merchant acknowledges and agrees that all goodwill in or associated with the Brand Name, including any goodwill generated or arising by or through Service Provider’s or Merchant’s activities pursuant to this Agreement shall accrue for the benefit of and shall belong exclusively to the Service Provider.<br>9.1.3 No right or interest in the Brand Name is granted or deemed to be granted by the Service Provider to the Merchant.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">9.2 Intellectual Property Rights In Relation To Service Provider Content and Service Provider Business</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Service Provider shall retain sole ownership of all the intellectual properties, know-how or other proprietary rights in the Service Provider Content and Service Provider Business and no right or interest is granted or shall be deemed to be granted by Service Provider to the Merchant. To the extent Service Provider Content contains any proprietary content or information of the Merchant, the Merchant hereby grants a royalty- free and world-wide license to such content or information including a right to creative derivative product of such content or information.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">10. CONFIDENTIALITY</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">10.1 Each Party may disclose to the other such Confidential Information as may be necessary to further the performance of this Agreement. The receiving Party undertakes to the disclosing Party:</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(i) to keep confidential the disclosing Party’s Confidential Information;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(ii)not to disclose the Confidential Information in whole or in part to any other person without the disclosing Party’s prior written consent, except to the receiving Party’s employees, agents and sub- contractors involved in the performance of this Agreement on a confidential and need to know basis and provided that employees, agents and subcontractors are bound by written agreements of confidentiality which are at least as stringent as the provisions of this Agreement; and<br>(iii)to use the Confidential Information solely in connection with the performance of this Agreement.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">10.2 The aforementioned confidentiality obligations shall not extend to Confidential Information which:</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(i) has ceased to be confidential without default on the part of the receiving Party;<br>(ii)has been received from a third party who did not receive it in confidence;<br>(iii)the receiving Party is required by any court, government or other regulatory body to disclose, but only to the extent required by law, provided that the receiving Party gives the disclosing Party written notice as soon as practicable of such requirement and consult in good faith the disclosing party on the content and manner of any disclosure.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">10.3 Upon request by the disclosing Party, the receiving Party must deliver to the disclosing Party all documents and other materials in any medium in its possession or control which contain or refer to the disclosing Party’s Confidential Information. If the documents or other materials are not capable of being returned, the receiving Party must destroy and certify the destruction of such documents and materials to the reasonable satisfaction of the Disclosing Party.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">10.4 Your and Merchant personal / sensitive personal data / information shall be governed by the Privacy Policy of the Platform, which terms (including all amendments, modifications, rein statements and substitutions) shall be deemed to be incorporated herein by way of reference.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">11.INDEMNIFICATION AND LIMITATION OF LIABILITY</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">11.1&nbsp; Merchant shall promptly on demand indemnify, defend and hold harmless the Service Provider, its Affiliates and End Customer and their respective officers, directors, proprietors, partners, managers, members, trustees, shareholders, employees and agents (“Indemnified Parties”) for and against all claims, liabilities, costs and expenses (including reasonable attorney’s fees) incurred or to be incurred by the Indemnified Parties that arise out of, in any way relate to, or result from any breach by the Merchant of the provisions of this Agreement, or breach of any laws by the Merchant, or negligence, fraud or willful misconduct of the Merchant or its Affiliates and their respective officers, directors, shareholders, employees, contractors, sub-contractors, agents and personnel. For the avoidance of doubt, it is further clarified that the right to indemnification in connection with any of the aforesaid claims of course of action is independent and in addition to other rights and remedies of the Indemnified Person that may be available at law or in equity. Service Provider shall have a lien on the Products and on the consideration received from the End Customer for the sale of Products on the Platform until Merchant has fully discharged its obligations and liabilities to Indemnified Parties in accordance with this Agreement. In the event Merchant is unable to indemnify the Indemnified Parties within a reasonable period of time, Service Provider shall be entitled to sell or otherwise dispose of the Products and set off the proceeds out of such sale and disposing off against Indemnified Parties’ indemnification claims and/or if permitted under law or by virtue of any order of any court of law Service Provider shall be entitled to receive the sale consideration from the payment gateway which otherwise would have remitted by such payment gateway to the Merchant and/or set off the amounts received by Service Provider from the End Customer who has availed cash on delivery services.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">11.2 Service Provider’s Limitation of Liability:</span><br>NOTWITHSTANDING ANYTHING CONTRARY CONTAINED IN THIS AGREEMENT, IN ANY EVENT SERVICE PROVIDER AND ITS AFFILIATES SHALL NOT BE LIABLE (WHETHER IN CONTRACT, WARRANTY, TORT (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), PRODUCT LIABILITY OR OTHER THEORY), TO THE MERCHANT OR ANY OTHER PERSON OR ENTITY FOR COST OF COVER OR FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES (INCLUDING DAMAGES FOR LOSS OF REVENUES, LOSS PROFIT OR ANTICIPATED PROFITS, LOSS OF GOODWILL, LOSS OF BUSINESS OR DATA) ARISING OUT OF OR IN RELATION THIS AGREEMENT. Service Provider’s entire<br>Liability to Merchant under this Agreement or under any applicable law or equity shall be limited solely to actual and proven direct damages sustained by the Merchant as a result of the gross negligence or willful misconduct of the Service Provider and its Affiliate and their respective directors, officers, employees and agents in the performance of their respective services and other obligations under this Agreement. In no event shall the Service Provider be liable, vicariously or otherwise, to the Merchant and its Affiliates or any third party for any losses, damages, liabilities, costs (including reasonable legal costs) and expenses (including taxation) which are in the aggregate in excess of the (i) amounts paid by the Merchant to the Service Provider in the immediately preceding twelve-month period under this<br>Agreement – if such losses et al. are due to Platform Services, or (ii) the cost of the Products (excluding Services Fees) sold by the Merchant to End Customer – if such losses et al. are due to Transaction Support Services.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">12. FURTHER REPRESENTATIONS AND WARRANTIES</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">12.1 In addition to other representations and warranties in this Agreement, each Party represents and warrants as follows:</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(i)&nbsp; it is a corporation duly organized, validly existing, and in good standing under the laws of its incorporation;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(ii) execution and performance of this Agreement by such Party and the consummation of the transactions contemplated hereby do not and will not contravene the certificate of incorporation or by-laws of such Party and do not and will not conflict with or result in (a) a breach of or default under any indenture, agreement, judgment, decree, order or ruling to which such Party is a party that would materially adversely affect such Party’s ability to perform its obligations under this Agreement; or (b) a breach of any applicable law; and</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(iii) it shall comply with all applicable laws in the performance of its obligations and the exercise of its rights under this Agreement.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">12.2 EXCEPT AS SPECIFIED IN THIS AGREEMENT, NEITHER PARTY MAKES ANY WARRANTY IN CONNECTION WITH THE SUBJECT OF THIS AGREEMENT. EACH PARTY HEREBY DISCLAIMS ANY AND ALL IMPLIED WARRANTIES, INCLUDING WITHOUT LIMITATION ALL IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">12.3 Service Provider specifically disclaims any and all express or implied warranties with respect to the Platform, Platform Services and Payment Facilitation Services and these are provided on ‘as is’ basis.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">13.TERM OF AGREEMENT</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">13.1 This Agreement shall commence on the Effective Date and shall be valid until termination. (“Term”).</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">13.2 Either party shall have the right to terminate this Agreement and all then existing Commercial Terms by issuing a 30-day prior notice of termination in writing without any additional obligations or liabilities to each other.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">13.3 Either party shall have a right to terminate this Agreement or any Commercial Terms thereto upon any material breach of this Agreement by the other Party provided that where in the reasonable opinion of the non- breaching Party, such breach is capable of cure, the non-breaching Party shall not terminate this Agreement / any Commercial Terms thereto without providing the breaching Party a cure period of [thirty<br>(30) days] to cure such breach and provide the non-breaching Party with necessary documents satisfactorily evidencing cure of such breach.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">13.4 Service Provider shall have the right to terminate this Agreement upon occurrence of any insolvency event in relation to Merchant. It is clarified that an insolvency event in relation to Merchant shall be deemed to have occurred upon occurrence of the following:</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(i)Merchant has ceased to carry on or threatens to seize the Business; or</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(ii)Merchant has passed an effective resolution or a binding order has been made for its winding up except under a scheme of amalgamation; or</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(iii)Merchant has become insolvent or has entered into liquidation (unless such liquidation is for the purposes of a fully solvent reorganization); or</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">(iv)Merchant has entered into, or taken steps to enter into, administration, administrative receivership, receivership, a voluntary arrangement, a scheme of arrangement with creditors, any analogous or similar procedure in any jurisdiction or any other form of procedure relating to insolvency, reorganization or dissolution in any jurisdiction, or a petition is presented or other step is taken by any person with a view to any of those things.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">14.CONSEQUENCES OF TERMINATION</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">14.1 Upon expiry or termination of this Agreement all Confidential Information and any other materials which may have been provided by one Party to the other shall be forthwith returned and the returning Party shall certify such return and all copies thereof or any other material or information which cannot be returned, shall be destroyed completely;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">14.2 Termination of this Agreement shall not relieve any Party of its obligations or liabilities and affect the rights and remedies of a Party, which have accrued prior to the date of termination.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">14.3 The provisions of this Agreement contained in Sections 9 (Intellectual Property), 10 (Confidentiality), 11 (Indemnification), 12 (Representations and Warranties), 14 (Consequences of Termination), 15 (Governing Law) and 16 (Dispute Resolution), 17.2 (Notices) shall survive the expiry or early termination of this Agreement.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">14.4 Termination of this Agreement shall not affect any obligations or duties of the Merchant and Service Provider towards the End Customer which obligations or duties accrued before the termination of this Agreement.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">15.GOVERNING LAW</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">15.1 This Agreement shall be governed by the laws of India without giving effect to its principles of conflict of laws. Subject to the provisions of Section 16 (Dispute Resolution); the courts at Hyderabad shall have the exclusive jurisdiction in respect of any matter or dispute under or connected with this Agreement, each of the parties hereby irrevocably consents to the jurisdiction of such courts (and of the appropriate appellate courts therefrom) in any such suit, action or proceeding and irrevocably waives, to the fullest extent permitted by law, any objection that it may now or hereafter have to the laying of the venue of any such suit, action or proceeding in any such court or that any such suit, action or proceeding brought in any such court has been brought in an inconvenient forum. Process in any such suit, action or proceeding may be served on the Merchant anywhere in the world, whether within or without the jurisdiction of any such court including on the designated electronic mail address.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">15.2 You and Company shall not accept this Agreement and use the Platform if You and Company does not wish to submit to the aforesaid applicable laws and jurisdiction.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">16. DISPUTE RESOLUTION</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">16.1 Any dispute which arises between the Parties shall be attempted to be resolved by good faith discussions between the Parties.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">16.2 Where the Parties are unable to resolve such disputes by good faith discussions within a period of thirty (30) business days from the date of a written notice by either Party notifying the existence of such dispute, either Party shall be free to refer the dispute to arbitration in accordance with this section. This Agreement and the rights and obligations of the Parties shall remain in full force and effect pending the award in such arbitration proceedings.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">16.3 The arbitration shall be governed by the Arbitration and Conciliation Act, 1996 (as applicable in India) for the time being in force, and/or any statutory modification or re-enactment thereof.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">16.4 The place and seat of arbitration shall be Hyderabad and the language of the arbitration shall be English.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">16.5 The arbitration shall be conducted by three (3) arbitrators. Each Party shall appoint one arbitrator each and the two appointed arbitrators shall appoint a presiding arbitrator. In case the Parties fail to appoint<br>Their respective arbitrators within thirty (30) days from the submission of dispute for settlement through arbitration in accordance with Section 16.2 above, or the two appointed arbitrators fail to appoint the presiding arbitrator with thirty (30) days from the date of appointment of the latter of the first two arbitrators, a sole arbitrator shall be appointed in accordance with the Indian Arbitration and Conciliation Act, 1996 by the appropriate court of law.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">16.6 The award rendered shall be in writing and shall set out the facts of the dispute and the reasons for the arbitrator’s decision. The award shall apportion the costs of the arbitration as the arbitrator deems fair.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">16.7 Notwithstanding anything contained in this Agreement, both Parties agree and acknowledges that the covenants and obligations with respect to the matters covered by this Agreement and set forth herein relate to special, unique and extraordinary matters, and that a violation of the terms of such covenants and obligations will cause irreparable loss and injury to the aggrieved Party. Therefore, notwithstanding the provisions of this Agreement, either Party shall be entitled to approach any appropriate forums for obtaining an injunction, restraining order or such other equitable relief as a court of competent jurisdiction may deem necessary or appropriate.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.GENERAL CLAUSES</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.1 Independent contractors</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">The relationship between parties is on principal to principal basis. Nothing in this Agreement shall be deemed to constitute either Party a partner, joint venture agent or legal representative of the other Party, or to create any fiduciary, employer-employee relationship between the Parties.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.2 Notices and Correspondences</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Notices</span>: Any notice, consent or waiver (including notice for Arbitration) required or permitted hereunder shall be effective only if it is in writing and shall be deemed received by the Party to which it is sent (i) upon delivery when delivered by hand, (ii) three (3) days after being sent, if sent with all sending expenses prepaid, by an express courier with a reliable system for tracking delivery, (iii) when transmitted, if sent by confirmed facsimile, or (iv) five (5) days (if Merchant is in India) or fourteen (14) days (if Merchant is outside of India) after the date sent, if sent by certified or registered mail, postage prepaid, return receipt requested, addressed as follows:</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">If to the Merchant:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; If to the Service Provider:</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">[At the address provided by You]&nbsp; &nbsp; &nbsp;</span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Address:&nbsp; 2nd Floor, VSSR SQUARE,</span><br><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; PLOT NO: 64 &amp; 64A,</span><br><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; VITTALRAO NAGAR,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; MADHAPURHYDERABAD,</span><br><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; TELANGANA,500081,INDIA.</span><br><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Tel: 08500009633</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; [Service Provider may change the aforesaid address by&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; posting the same on the Platform]</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">General communications through electronic mode:</span><br>When the Merchant uses the Platform or send emails or other data, information or communication to Service Provider, Merchant agrees and understands that Merchant is communicating with Service Provider through electronic records and Merchant consents to receive communications via electronic records from Service Provider periodically and as and when required. Service Provider will communicate with the Merchant by email at the designated electronic mail address provided by the Merchant at the time of registration.<br><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.3 Assignment and Sub-Contracting</span><br>Merchant shall not assign any of its rights, obligations or responsibilities under this Agreement without the prior written consent of the Service Provider and in absence of such consent any such assignment shall be null and void. All terms and conditions of this Agreement shall be binding upon and shall insure to the benefit of the parties hereto and their successors and authorized assignees. Merchant understands, acknowledges and agrees that Service Provider may sub-contract one or more of the Service Provider Business to any third party including affiliates.<br><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.4 Press Releases / Public Statement:</span><br>Unless required by law, the Merchant will not make any public announcement or issue any press release concerning the transactions contemplated by this Agreement without the prior consent of the Service Provider.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.5 Amendment and evolution of Commercial Terms on periodic basis</span><br>Service Providers may amend this Agreement, Commercial Terms and Platform Policies at any time by posting a revised version on the Platform. All updates and amendments may be notified to the Merchant on designated electronic mail addresses. Merchant is advised to regularly check for any amendments or updates to the terms and conditions contained in this Agreement, Commercial Terms and Platform Policies. It is strongly advised that Commercial Terms be checked on a daily basis as these evolve on a regular basis based on certain criteria. Merchant’s using Platform, Platform Services or Service Provider Business after Service Provider’s amendment to this Agreement, Commercial Terms and Platform Policies shall be deemed to be Merchant’s unconditional and absolute acceptance of such amendments (effective from the date such amendments were made by the Service Provider). If Merchant does not agree to the change or amendments, Merchant can cease using the Service Provider Business (except for those Products which have been bought by the End Customers) and may terminate this Agreement as provided in Section 13.2.<br><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.6 Severability</span><br>It is the intent of the Parties that in case anyone or more of the provisions contained in this Agreement shall be held to be invalid or unenforceable in any respect, such provision shall be modified to the extent necessary to render it, as modified, valid and enforceable under applicable laws and such invalidity or unenforceability shall not affect the other provisions of this Agreement.<br><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.7 Waiver</span><br>Except as expressly provided in this Agreement, no waiver of any provision of this Agreement shall be effective unless set forth in a written instrument signed by the Party waiving such provision. No failure or delay by a Party in exercising any right, power or remedy under this Agreement shall operate as a waiver thereof, nor shall any single or partial exercise of the same preclude any further exercise thereof or the exercise of any other right, power or remedy. Without limiting the foregoing, no waiver by a Party of any breach by any other Party of any provision hereof shall be deemed to be a waiver of any preceding or subsequent breach of that or any other provision hereof.<br><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.8 Further Assurance</span><br>Each Party shall cooperate with the other Party and execute and deliver to the other party such instruments and documents and take such other actions as may be reasonably requested from time to time in order to carry out, evidence and confirm their rights hereunder and the intended purpose of this Agreement and to ensure the complete and prompt fulfillment, observance and performance of the provisions of this Agreement and generally that full effect is given to the provisions of this Agreement.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.9 Covenants Reasonable</span><br>The Parties agree that, having regard to all the circumstances, the covenants contained herein are reasonable and necessary for the protection of the Parties. If any such covenant is held to be void as going beyond what is reasonable in all the circumstances, but would be valid if amended as to scope or duration or both, the covenant will apply with such minimum modifications regarding its scope and duration as may be necessary to make it valid and effective.<br><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.10 Independent Rights</span><br>Each of the rights of the Parties hereto under this Agreement are independent, cumulative and without prejudice to all other rights available to them, and the exercise or non-exercise of any such right shall not prejudice or constitute a waiver of any other right of the Party, whether under this Agreement or otherwise.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">17.11 Counsel and management participation</span><br>You and Merchant acknowledge and confirm that you attorneys and management representatives have read, reviewed and approved this Agreement and that You and Merchant have had the benefit of its independent legal counsel’s advice with respect to the terms and provisions hereof and its rights and obligations hereunder.</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">18.GRIEVANCE OFFICER</span><br>In accordance with Information Technology Act, 2000 and the rules made thereunder, the name and contact details of the Grievance Officer currently is Mr.Sriharsha with address at Fabpik Ecomm LLP, 2nd Floor, VSSR Square, Plot no: 64&amp;64A, VittalRao Nagar, Madhapur, Hyderabad, Telangana 5000081, India. Email ID:&nbsp;<a href=\"mailto:sellersupport@fabpik.in\" style=\"color: rgb(63, 63, 63); margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; touch-action: manipulation; transition: all 0.25s ease 0s;\">sellersupport@fabpik.in</a>. Any change shall be communicated on the Platform. Service of notice for Dispute Resolution and for purposes other than those which are required under Information Technology Act, 2000 to be given only to the Grievance Officer shall not be valid.</p></div></article></div></div></div></div>', 1, '2021-01-29 06:40:00', '2021-01-29 06:40:00', NULL);
INSERT INTO `terms_conditions` (`id`, `name`, `display_name`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 'terms-and-conditions', 'Terms & Conditions', '<p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); text-align: center;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">TERMS OF SERVICE</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">This document is an electronic record in terms of Information Technology Act, 2000 and rules there under as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">This website www.Fabpik.in (hereinafter referred to as “Fabpik” or “Site” or “we” or “us”) is a service. Your use of this Site and related services and tools are governed by the following terms and conditions. If you transact on Fabpik, you shall be subject to the policies that are applicable to the Site for such transaction. You agree that you have read and understood these Terms of Use and that you would be bound by these Terms of Use till such time as you may access the Site. If you do not wish to be bound by these Terms of Use, please do not use this Site.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">For the purpose of these Terms of Use, wherever the context so require ‘you’ shall mean any natural or legal person who has agreed to become a member of the Website by providing registration data while registering on the Site as registered user using the computer systems of Fabpik.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">When you use any of the services provided on the Site, including but not limited to customer reviews, etc., you will be subject to the rules, guidelines, policies, terms, and conditions applicable to such service, and they shall be deemed to be incorporated into this Terms of Use and shall be considered as part and parcel of this Terms of Use. However please note that your purchase of products from the Site shall be governed by the Product Policy (as detailed below). Fabpik reserves the right, at its sole discretion, to change, modify, add or remove portions of these Terms of Use, at any time. It is your responsibility to check these Terms of Use periodically for changes. Your continued use of the Site following the posting of changes will mean that you accept and agree to such changes to these Terms of Use. As long as you comply with these Terms of Use and the Product Policy, Fabpik grants you a personal, non-exclusive, non-transferable, limited privilege to enter and use the Site.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Accessing, browsing or otherwise using the Site indicates your agreement to all the terms and conditions in these Terms of Use, so please read this agreement carefully before proceeding.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">General</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">These Terms of Use have been executed and delivered by you and constitute a valid and binding agreement between you and Fabpik, enforceable against you in accordance with their terms. You represent that (1) you are at least 18 years of age, (2) you are not incapacitated to form a binding contract, (3) in case you are registering as a member on behalf of a corporate body, you represent that you have the requisite authority to bind such corporate body and (3) that all of the information, data and other materials you provide on this Site or to Fabpik through any other means are true, accurate, current and complete. You are responsible for updating and correcting the information you have provided on this Site, as appropriate.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You shall establish and use your membership on the Site, and purchase and use the products and services available through the Site in strict compliance with these Terms of Use and all applicable policies, laws, rules and regulations. All calls, emails and other communications between you and Fabpik may be recorded.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Privacy notice</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">A copy of the Privacy Notice that applies to the collection, use, handling, disclosure, transfer and other processing of personal information by Fabpik is located at&nbsp;</span><a href=\"https://fabpik.in/privacypolicy/\" style=\"color: rgb(63, 63, 63); margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; touch-action: manipulation; transition: all 0.25s ease 0s;\">https://fabpik.in/privacypolicy/</a><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">. You consent to any personal information we may obtain about you (either via the Site, by email, telephone or any other means) being collected, stored, disclosed, transferred and otherwise processed in accordance with the terms of the Privacy Notice. Fabpik may update its Privacy Notice from time to time, in its sole discretion, and post an updated version of the notice at the website address provided above.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Product policy</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">The Product Policy governs all aspects of your purchase of products through the Site.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik membership</span></h2><ul style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Establishing a Membership</span><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><br style=\"margin-bottom: 0px;\"></span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You may establish membership on the Site. At a minimum, enrolment requires you to (1) indicate agreement to these Terms of Use and the Product Policy, (2) provide contact information and identification details, and (3) submit any other form of authentication required as part of the enrolment process, in Fabpik’s sole discretion. You agree to accept responsibility for all activities that occur under your account or password. You are responsible for maintaining the confidentiality of your account and password, and for restricting access to your account. We shall not be responsible for any unauthorized use of your account with Fabpik which may result from any failure by you to protect the confidentiality of your account details.</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><br style=\"margin-bottom: 0px;\"></span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You agree that having multiple accounts on the Website is a violation of these Terms of Use and that sending invites to alternate email addresses or accounts or otherwise attempting to circumvent the Referral Credit Program available on the Site (details available in the Product Policy), without limiting any other Fabpik’s rights or remedies, shall result in forfeiture of your membership and all referral credits in your account.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Use of Membership</span><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><br style=\"margin-bottom: 0px;\"></span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You shall not use your Fabpik membership for any purpose competitive to Fabpik. You will notify Fabpik immediately by emailing support@Fabpik.in of any actual or suspected unauthorized use of your Fabpik membership or the Site known to you, whether by you or a third party.</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Membership Termination</span><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><br style=\"margin-bottom: 0px;\"></span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You may cancel your Fabpik membership at any time by contacting Fabpik customer support as specified on the “Contact Us” page on the Site. Fabpik may terminate your membership and refuse any and all current or future use of the Site or other services, or any portion thereof, (1) in order to comply with applicable laws, (2) if you provide any information that Fabpik determines, in its sole discretion, to be untrue, inaccurate, not current or incomplete (or if the information becomes untrue, inaccurate, not current or incomplete), (3) if Fabpik determines, in its sole discretion, that you are using your Fabpik membership in a manner not permitted by these Terms of Use, or (4) in other circumstances, as Fabpik deems appropriate in its sole discretion.</span></li></ul><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\">&nbsp;</p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Vendor Commission</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">On every order received by the vendor on Fabpik website, Fabpik shall reserve the right to take/debit 8% of Sale price of the product irrespective of discounts,offer or any other promotional offers by Vendor. Fabpik may update/change this policy from time to time without prior information/notice. Vendors are responsible to keep checking the policy from time to time.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">License and site access</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">All content available through this Site (including, without limitation, text, design, graphics, logos, icons, images, audio clips, downloads, interfaces, code and software, as well as the selection and arrangement and look and feel of the Site) is the exclusive property of and owned by Fabpik, its franchisees, licensors or content providers, and is protected by copyright, trademark and other applicable U.S. and foreign laws. Access to this Website does not entitle you to any right to the intellectual property rights of Fabpik, its franchisees, licensors or content providers other than as expressly provided herein. Fabpik grants you a limited license to access and make personal use of this Site. Unless indicated to the contrary, you may access, copy, download and print the content available on this Site for your personal, non-commercial use, provided you do not modify or delete any copyright, trademark or other proprietary notices that appear in the content. You are also strictly prohibited from transmitting, posting, linking, deep linking or otherwise modifying the Site without the prior express permission of Fabpik. Fabpik or its licensors or content providers retain full and complete title to the content available on the Site, including all associated intellectual property rights, and provide this content to you under a limited license (only for personal and non-commercial use) that is revocable at any time in Fabpik’s sole discretion. Such license is granted to you on a non-transferable, royalty free and worldwide basis.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik strictly prohibits any other use of any content available through the Site, including but not limited to:</span></p><ol style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">any downloading, copying or other use of the content or the Site for purposes competitive to Fabpik or for the benefit of another vendor or any third party;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">any caching, unauthorized linking to the Site or the framing of any content available on the Site;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">any modification, distribution, transmission, performance, broadcast, publication, uploading, licensing, reverse engineering, transfer or sale of, or the creation of derivative works from, any content, products or services obtained from the Site that you do not have a right to make available (such as the intellectual property rights of another party);</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">any uploading, posting or transmitting of any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">using any hardware or software intended to surreptitiously intercept or otherwise obtain any information (such as system data or personal information) from the Site (including, but not limited to the use of any “scraping” or other data mining techniques, robots or similar data gathering and extraction tools); or</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">any action that imposes or may impose (in Fabpik’s sole discretion) an unreasonable or disproportionately large load on Fabpik’s infrastructure, or damage or interfere with the proper working of our infrastructure.</span></li></ol><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You are responsible for obtaining access to the Site, and that access may involve third-party fees (such as Internet service provider or airtime charges). In addition, you must provide and are responsible for all equipment necessary to access the Site. You may not bypass any measures that have been implemented to prevent or restrict access to this Site. Any unauthorized access to the Site by you (including any such access or use that involves in any way an account you may establish on the Site or any device you may use to access the Site) shall terminate the permission or license granted to you by Fabpik.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik reserves the right to refuse or cancel any person’s registration for this Site, remove any person from this Site and prohibit any person from using this Site for any reason whatsoever, and to limit or terminate your access to or use of the Site at any time without notice. Fabpik neither warrants nor represents that your use of the content available on this Site will not infringe rights of third parties not affiliated with Fabpik. Termination of your access or use will not waive or affect any other right or relief to which Fabpik may be entitled, at law or in equity.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Moments</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik -FabMoments is a platform that allows you to upload the pictures and videos showcasing your kids with products purchased on www.Fabpik.in and other affiliated platforms (hereinafter referred to as “Fabpik” or “Site” or “we” or “us”). Accessing and using this platform is subject to the following terms and conditions. The following terms and conditions are in addition to the Terms of Use and Privacy Policy of Fabpik.</span></p><ul style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">By uploading photos, videos and/or content (“Content”) on Fabpik Moments (Platform) you consent Fabpik to list and display the Content on the Fabpik Moments platform, Fabpik website (www.Fabpik.in), affiliate websites of Fabpik, social media platforms along with any advertising and promotional material issued by Fabpik.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You agree and understand that Fabpik is merely a technology platform for displaying the Content and all&nbsp; liability for any uploaded photo, video or other content lies on the person uploading the content. Fabpik is not liable for any upload of photos or misuse or abuse of this Platform.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You agree and consent that Fabpik shall either itself or through third parties, collate and/or source content uploaded by you showcasing Fabpik products on social media platform or other available online and offline media.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You agree that the Content uploaded by you shall include the products purchased only from Fabpik along with children and adults showcasing the Fabpik products.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You agree that the Content uploaded by you is original and does not involve any imitation or plagiarism of the Content.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You shall upload the Content only of the products that you have purchased from Fabpik.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You shall not display any Content which is offensive, abusive, pornographic or hurting any religious sentiments.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">All the Content may be uploaded and displayed instantly on the Platform at the discretion of management.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">In event Fabpik finds or any individual reports Content to be offensive, abusive, pornographic, hurting any religious sentiments, Fabpik reserves the right to remove such photo, video or other such Content from the platform and if necessary report to the appropriate authorities.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Upon uploading the Content you have granted Fabpik a perpetual, worldwide, unlimited license to use, license, sub-license and display the Content with or without any modification.</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Any and all disputes shall be subject to Telangana jurisdiction.</span></li></ul><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Content you submit</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You acknowledge that you are responsible for any content you may submit through the Site, including the legality, reliability, appropriateness, originality and copyright of any such content. You may not upload to, host, display, modify, transmit, update, share, distribute or otherwise publish through this Site any content that:</span></p><ol style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">is confidential, proprietary, invasive of privacy or publicity rights, infringing on patents, trademarks, copyrights, or other intellectual property rights, unlawful, harmful, threatening, false, fraudulent, libellous, defamatory, obscene, vulgar, pornographic, paedophilic, profane, abusive, harassing, hateful, racially, ethnically or otherwise objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever, including, but not limited to any content that encourages conduct that would constitute a criminal offence, violates the rights of any party or otherwise gives rise to civil liability or otherwise violates any applicable laws for the time being in force;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">belongs to another person and to which you do not have any right to;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">harms minors in any way;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognisable offence of prevents investigation of any offence or is insulting any other nation;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">infringes upon or violates any third party’s privacy rights (including without limitation unauthorized disclosure of a person’s name, physical address, phone number or email address);</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">provides material that exploits people in a sexual, violent or otherwise inappropriate manner or solicits personal information from anyone;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">interferes with another user’s use and enjoyment of the Site or any other individual’s user and enjoyment of similar services; or</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">directly or indirectly, offer for trade, the dealing of any item which is prohibited or restricted in any manner under the provisions of any applicable law, rule, regulation or guideline for the time being in force;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">may contain software viruses or malware or files or programs designed to interrupt, destroy or limit the functionality of any computer;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">contains advertisements or solicitations of any kind, or other commercial content;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">is designed to impersonate others;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">contains messages by non-spokesperson employees of Fabpik purporting to speak on behalf of Fabpik or containing confidential information or expressing opinions concerning Fabpik;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">contains messages that offer unauthorized downloads of any copyrighted, confidential or private information;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">contains multiple messages placed within individual folders by the same user restating the same point;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">contains chain letters of any kind; or</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">contains identical (or substantially similar) messages to multiple recipients advertising any product or service, expressing a political or other similar message, or any other type of unsolicited commercial message; this prohibition includes but is not limited to (a) using the invitation functionality that may be available on the Site to send messages to people who do not know you or who are unlikely to recognize you as a known contact; (b) using the Site to connect to people who do not know you and then sending unsolicited promotional messages to those direct connections without their permission; or (c) sending messages to distribution lists, newsgroup aliases or group aliases.</span></li></ol><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You may not use a false email address or other identifying information, impersonate any person or entity or otherwise misled as to the origin of any content. Some features that may be available on this Site require registration. By registering, you agree to provide true, accurate, current and complete information about yourself.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">With respect to any content you submit or make available through the Site (other than personal information, which is handled in accordance with the Privacy Notice, you grant Fabpik a perpetual, irrevocable, non-terminable, worldwide, royalty-free and non-exclusive license to use, copy, distribute, publicly display, modify, create derivative works, and sublicense such content or any part of such content, in any media. You hereby represent, warrant and covenant that any content you provide does not include anything (including, but not limited to, text, images, music or video) to which you do not have the full right to grant such a license to Fabpik.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You agree that your use of the Site may involuntarily expose you to content that is offensive or obscene which have been posted or transmitted by other users (including unauthorised users such as hackers. etc). It is also possible for others to obtain your personal information which you may have shared during your usage of the Site and use such information to harass or injure you. Fabpik does not authorise or approve such unauthorised usage of your information but by using the Site, you acknowledge and agree that Fabpik will not be responsible for any third party’s use of any of your information which you may have publicly disclosed on the Site. We advise that you exercise caution in disclosing any information on the Site which may become publicly available.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Links</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">This Site may contain links to other websites or resources that are operated by third parties not affiliated with Fabpik. These links are provided as a convenience to you and as an additional avenue of access to the information contained therein. We are not responsible or liable for any content, advertising, products or other materials on or available from such sites or resources. Inclusion of links to other sites or resources should not be viewed as an endorsement of the content of linked sites or resources. Different terms and conditions and privacy policies may apply to your use of any linked sites or resources. Fabpik is not responsible or liable, directly or indirectly, for any damage, loss or liability caused or alleged to be caused by or in connection with any use of or reliance on any such content, products or services available on or through any such linked site or resource.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Disclaimers</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">EXCEPT AS OTHERWISE EXPRESSLY PROVIDED IN THESE TERMS OF USE, OR REQUIRED BY APPLICABLE LAW, Fabpik MAKES NO REPRESENTATIONS, COVENANTS OR WARRANTIES AND OFFERS NO OTHER CONDITIONS, EXPRESS OR IMPLIED, REGARDING ANY MATTER, INCLUDING, WITHOUT LIMITATION, THE MERCHANTABILITY, SUITABILITY, FITNESS FOR A PARTICULAR USE OR PURPOSE, OR NON-INFRINGEMENT OF FABPIK MEMBERSHIP, ANY CONTENT ON THE SITE, OR ANY PRODUCTS OR SERVICES PURCHASED THROUGH THE SITE, AS WELL AS WARRANTIES IMPLIED FROM A COURSE OF PERFORMANCE OR COURSE OF DEALING.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">YOUR USE OF THIS SITE IS AT YOUR SOLE RISK. THE SITE IS PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. WE RESERVE THE RIGHT TO RESTRICT OR TERMINATE YOUR ACCESS TO THE SITE OR ANY FEATURE OR PART THEREOF AT ANY TIME. Fabpik DISCLAIMS ANY WARRANTIES THAT ACCESS TO THE SITE WILL BE UNINTERRUPTED OR ERROR-FREE; THAT THE SITE WILL BE SECURE; THAT THE SITE OR THE SERVER THAT MAKES THE SITE AVAILABLE WILL BE VIRUS-FREE; OR THAT INFORMATION ON THE SITE WILL BE CORRECT, ACCURATE, ADEQUATE, USEFUL, TIMELY, RELIABLE OR OTHERWISE COMPLETE. IF YOU DOWNLOAD ANY CONTENT FROM THIS SITE, YOU DO SO AT YOUR OWN DISCRETION AND RISK. YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH CONTENT. NO ADVICE OR INFORMATION OBTAINED BY YOU FROM THE SITE SHALL CREATE ANY WARRANTY OF ANY KIND.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Limitation of liability</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">YOU ACKNOLWEDGE AND AGREE THAT Fabpik MERELY PROVIDES THE SITE AND THE PROVIDERS WHOSE PRODUCTS AND SERVICES ARE AVAILABLE ON THE FABPIK SITES ARE INDEPENDENT CONTRACTORS AND NOT AGENTS OR EMPLOYEES OF FABPIK. FABPIK IS NOT LIABLE FOR THE ACTS, ERRORS, OMISSIONS, REPRESENTATIONS, WARRANTIES, BREACHES OR NEGLIGENCE OF ANY SUCH THIRD PARTIES OR FOR ANY PERSONAL INJURIES, DEATH, PROPERTY DAMAGE, OR OTHER DAMAGES OR EXPENSES RESULTING THEREFROM.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">YOU ACKNOWLEDGE AND AGREE THAT YOU ASSUME FULL RESPONSIBILITY FOR YOUR USE OF THE SITE AND/OR FOR USE OF SITE’S MEMBERSHIP, COMMUNICATIONS WITH THIRD PARTIES, AND PURCHASE AND USE OF THE PRODUCTS AND SERVICES AVAILABLE THROUGH THE SITE. WITH RESPECT TO THIRD PARTY USER GENERATED CONTENT, Fabpik NEITHER ORIGINATES NOR INITIATES ANY TRANSMISSION ON THE SITE NOR SELECTS THE SENDER AND RECEIVER OF A TRANSMISSION NOR SELECTS NOR MODIFIES THE INFORMATION CONTAINED IN A TRANSMISSION. FABPIK HAS NO CONTROL OVER THE THIRD PARTY USER GENERATED CONTENT IN THE SITE AND ACTS AS AN “INTERMEDIARY” AS UNDERSTOOD IN TERMS OF INFORMATION TECHNOLOGY ACT, 2000 WITH RESPECT TO ALL THIRD PARTY USER GENERATED CONTENT.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">YOU ACKNOWLEDGE AND AGREE THAT ANY INFORMATION YOU SEND OR RECEIVE DURING YOUR MEMBERSHIP AND/OR USE OF THE SITE MAY NOT BE SECURE AND MAY BE INTERCEPTED BY UNAUTHORIZED PARTIES. YOU ACKNOWLEDGE AND AGREE THAT YOUR USE OF THE SITE IS AT YOUR OWN RISK AND THAT THE SITE IS MADE AVAILABLE TO YOU AT NO CHARGE. RECOGNIZING SUCH, YOU ACKNOWLEDGE AND AGREE THAT, TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, NEITHER Fabpik NOR ITS LICENSORS, SUPPLIERS OR THIRD PARTY CONTENT PROVIDERS WILL BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, EXEMPLARY, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR OTHER DAMAGES ARISING OUT OF OR IN ANY WAY RELATED TO (1) THIS SITE, OR ANY OTHER SITE OR RESOURCE YOU ACCESS THROUGH A LINK FROM THIS SITE; (2) ANY ACTION WE TAKE OR FAIL TO TAKE AS A RESULT OF COMMUNICATIONS YOU SEND TO US; (3) YOUR FABPIK MEMBERSHIP, ANY TERMINATION OR CANCELATION OF YOUR MEMBERSHIP, ANY REFERRAL CREDIT PROGRAM (OR ASSOCIATED CREDITS); (4) ANY PRODUCTS OR SERVICES MADE AVAILABLE OR PURCHASED THROUGH THE SITE; (5) ANY FRAUDULENT ACT COMMITTED BY ANY PERSON USING THE SITE WHICH MAY OR MAY NOT INVOLVE FINANCIAL TRANSACTIONS, INCLUDING ANY DAMAGES OR INJURY ARISING FROM ANY USE OF SUCH PRODUCTS OR SERVICES; (5) ANY DELAY OR INABILITY TO USE THE SITE OR ANY INFORMATION, PRODUCTS OR SERVICES ADVERTISED IN OR OBTAINED THROUGH THE SITE; (6) THE MODIFICATION, REMOVAL OR DELETION OF ANY CONTENT SUBMITTED OR POSTED ON THE SITE; OR (7) ANY USE OF THE SITE, WHETHER BASED ON CONTRACT, TORT, STRICT LIABILITY OR OTHERWISE, EVEN IF FABPIK HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. IT IS THE RESPONSIBILITY OF THE USER TO EVALUATE THE ACCURACY, COMPLETENESS OR USEFULNESS OF ANY OPINION, ADVICE OR OTHER CONTENT AVAILABLE THROUGH THE SITE, OR OBTAINED FROM A LINKED SITE OR RESOURCE. THIS DISCLAIMER APPLIES, WITHOUT LIMITATION, TO ANY DAMAGES OR INJURY ARISING FROM ANY FAILURE OF PERFORMANCE, ERROR, OMISSION, INTERRUPTION, DELETION, DEFECT, DELAY IN OPERATION OR TRANSMISSION, COMPUTER VIRUS, FILE CORRUPTION, COMMUNICATION-LINE FAILURE, NETWORK OR SYSTEM OUTAGE, LOSS OF PROFITS BY YOU, OR THEFT, DESTRUCTION, UNAUTHORIZED ACCESS TO, ALTERATION OF, LOSS OR USE OF ANY RECORD OR DATA, AND ANY OTHER TANGIBLE OR INTANGIBLE LOSS. YOU SPECIFICALLY ACKNOWLEDGE AND AGREE THAT NEITHER Fabpik NOR ITS LICENSORS, SUPPLIERS, FRANCHISEES OR THIRD PARTY CONTENT PROVIDERS SHALL BE LIABLE FOR ANY DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT OF ANY USER OF THE SITE. YOUR REMEDY FOR ANY OF THE ABOVE CLAIMS OR ANY DISPUTE WITH FABPIK IS TO DISCONTINUE YOUR USE OF THE SITE. YOU AND FABPIK AGREE THAT ANY CAUSE OF ACTION ARISING OUT OF OR RELATED TO THE SITE MUST COMMENCE WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES OR THE CAUSE OF ACTION IS PERMANENTLY BARRED.NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED ELSEWHERE, FABPIK’S TOTAL LIABILITY TOWARDS YOU FOR ALL LIABILITIES ARISING FROM YOUR USE OF THE SITE OR PURCHASING PRODUCTS USING THE SITE SHALL NOT, BE IT IN TORT OR CONTRACT, EXCEED THE AMOUNT PAID BY YOU TO FABPIK’s FRANCHISEES AGAINST THE VALUE OF THE PRODUCTS ORDERED BY YOU USING THE SITE.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Indemnities</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You will indemnify and hold harmless Fabpik, its licensees, subsidiaries, affiliates, and their employees, directors, officers, agents and representatives (“Indemnified Parties”) from and against any and all fines, penalties, liabilities, losses and other damages of any kind whatsoever (including attorneys’ and experts’ fees), incurred by such Indemnified Parties, and shall defend such Indemnified Parties against any and all claims arising out of (1) your breach of these Terms of Use, Privacy Notice, Product Policy and all other policies applicable to you by virtue of using this Site or any other Fabpik websites; (2) fraud you commit, or your intentional misconduct or gross negligence; or (3) your violation of any applicable laws or the rights of a third party. The Indemnified Parties will control the defence of any claim to which this indemnity may apply, and in any event, you shall not settle any claim without the prior written approval of the Indemnified Parties.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Electronic communications</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">When you use the Site or send emails to Fabpik, you are communicating with Fabpik electronically. You consent to receive electronically any communications related to your use of this Site. Fabpik will communicate with you by email or by posting notices on this Site. Please refer to the Privacy Notice to opt-out of communication that you may not desire to receive from Fabpik. You agree that all agreements, notices, disclosures and other communications that are provided to you electronically satisfy any legal requirement that such communications be in writing. All notices from Fabpik intended for receipt by a customer shall be deemed delivered and effective when sent to the email address you provide on the Site.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Access to password-protected areas of the site</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Access to and use of password-protected areas of the Site is restricted to authorized users only. You are responsible for protecting your login credentials, including any password. You agree that you will be responsible for any and all statements made, and acts or omissions that occur, through the use of your login credentials. If you have any reason to believe or become aware of any loss, theft or unauthorized use of your login credentials, notify Fabpik immediately. Fabpik may assume that any communications we receive from your email or other address, or communications that are associated with your login credentials or your account on this Site, have been made by you unless we receive notice indicating otherwise.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Assignment</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You may not assign these Terms of Use (or any rights, benefits or obligations hereunder) by operation of law or otherwise without the prior written consent of Fabpik, which may be withheld at Fabpik’s sole discretion. Any attempted assignment that does not comply with these Terms of Use shall be null and void. Fabpik may assign these Terms of Use, in whole or in part, to any third party in its sole discretion.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Entire agreement</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">These Terms of Use constitute the entire agreement between you and Fabpik regarding the specific matters herein, and all prior agreements, letters, proposals, discussions and other documents regarding the matters herein are superseded and merged into these Terms of Use.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\">&nbsp;</p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Governing law and dispute resolution</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">These Terms of Use shall be governed by laws of India without reference to conflict of laws principle. The Courts in [Mumbai] would have the exclusive jurisdiction in any proceedings that arise out of these Terms of Use.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Severability</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Each of the provisions of these Terms of Use is severable. If any provision of these Terms of Use (or part of a provision) is found by any court of competent jurisdiction to be invalid, unenforceable or illegal, the other provisions shall remain in force. If any invalid, unenforceable or illegal provision would be valid, enforceable or legal if some part of it were deleted or modified, the provision shall apply with whatever modification is necessary to give effect to these Terms of Use.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Modification of terms of use</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You acknowledge and agree that Fabpik may, in its sole discretion, modify, add or remove any portion of these Terms of Use at any time and in any manner, including the terms of Fabpik membership, by posting revised Terms of Use on the Site. You may not amend or modify these Terms of Use under any circumstances. The current version of these Terms of Use is available at&nbsp;</span><a href=\"http://www.hopscotch.in/terms\" style=\"color: rgb(63, 63, 63); margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; touch-action: manipulation; transition: all 0.25s ease 0s;\">http://www.Fabpik.in</a><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;It is your responsibility to check periodically for any changes we make to the Terms of Use. Your continued use of this Site after any changes to the Terms of Use means you accept the changes.</span></p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Effect of termination/survival of selected provisions</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Notwithstanding the expiration or earlier termination of your Fabpik membership, or any general legal principles to the contrary, any provision of these Terms of Use that impose or contemplate continuing obligations or rights of a party will survive expiration or termination of these Terms of Use.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\">&nbsp;</p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Claims of intellectual property infringement</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik respects the intellectual property rights of others, and we ask our users to do the same. You are hereby informed that Fabpik has adopted and reasonably implemented a policy that provides for the termination in appropriate circumstances of website users or Fabpik members who are repeat copyright infringers. Fabpik may, in appropriate circumstances and at its discretion, disable and/or terminate the accounts and/or memberships of users who may be infringing the intellectual property of a third party. If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please contact us at:&nbsp;</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Support@Fabpik.in</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\">&nbsp;</p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Branded packaging materials</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Use only plain Courier Bag/Envelopes/Pouches/Cover Temper Proof Plastic Poly-bags with or without invoice pod. Do not use packaging material which contains branding of any other online marketplace or e-commerce company. Do not re-use printed packaging material.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik Rules:</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Like any community, Fabpik Ecommerce platform has rules to help ensure a safe and enjoyable selling experience.</span></p><ul style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Honour your commitment to sell.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Maintain current account information.</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Do not misrepresent yourself.</span></li></ul><h1 style=\"font-size: 28px; margin-top: 0px; margin-bottom: 20px; font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Drop Shipping Policy</span></h1><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Drop shipping, or allowing a third party to fulfil orders to customers on your behalf, is generally acceptable. If you intend to fulfil orders using a drop shipper, you must always:</span></p><ul style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Be the seller of record of your products;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Identify yourself as the seller of your products on all packing slips and other information included or provided in connection with them;</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Be responsible for accepting and processing customer returns of your products; and</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Comply with all other terms of your seller agreement and applicable Amazon policies.</span></li></ul><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\">&nbsp;</p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Failure to comply with these requirements may result in the suspension or removal of your selling privileges.</span></p><h1 style=\"font-size: 28px; margin-top: 0px; margin-bottom: 20px; font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Product detail page rules</span></h1><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">The following rules and restrictions apply to sellers who use the Amazon Add a Product feature</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">:</span></p><ul style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Detail/Description Space may not feature or contain&nbsp;</span><a href=\"https://sellercentral.amazon.in/gp/help/200386260?language=en_IN&amp;ref=ag_200386260_cont_200390640\" style=\"color: rgb(63, 63, 63); margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; touch-action: manipulation; transition: all 0.25s ease 0s;\">Prohibited Content</a><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;or&nbsp;</span><a href=\"https://sellercentral.amazon.in/gp/help/200164330?language=en_IN&amp;ref=ag_200164330_cont_200390640\" style=\"color: rgb(63, 63, 63); margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; touch-action: manipulation; transition: all 0.25s ease 0s;\">Restricted Products</a><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">.</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">The inclusion of any of the following information in detail page titles, descriptions, bullet points or images is prohibited:</span></li></ul><ul style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><ul style=\"margin-top: 10px; margin-right: 0px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 1.4; font-family: inherit; list-style: none;\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Information that is grossly harmful, harassing, blasphemous, defamatory, paedophilic, libellous, invasive of another’s privacy, hateful or racially/ethnically objectionable, disparaging, relating to or encouraging money laundering or gambling, pornographic, obscene or offensive content or otherwise unlawful in any manner whatever.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Phone numbers, physical mail addresses, email addresses or website URLs.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Availability, price, condition, alternative ordering information (such as links to other websites for placing orders) or alternative delivery offers (such as free delivery).</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Advertisements, promotional material or watermarks on images, photos or videos.</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Do not include HTML, DHTML, JavaScript or other types of executables in your description space.</span></li></ul></li></ul><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\">&nbsp;</p><h2 style=\"font-family: Cabin, Arial, Helvetica, sans-serif; font-weight: 600; line-height: 1.4; color: rgb(45, 42, 42); margin: 0px 0px 20px; font-size: 24px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Claims of intellectual property infringement</span></h2><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik respects the intellectual property rights of others, and we ask our users to do the same. You are hereby informed that Fabpik has adopted and reasonably implemented a policy that provides for the termination in appropriate circumstances of website users or Fabpik members who are repeat copyright infringers. Fabpik may, in appropriate circumstances and at its discretion, disable and/or terminate the accounts and/or memberships of users who may be infringing the intellectual property of a third party. If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please contact us at:&nbsp;</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Support@Fabpik.in</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">.</span></p><p style=\"margin-bottom: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik may update this contact information from time to time without notice to you. We will post the current contact information on this Site.</span></p>', 1, '2021-01-29 07:55:05', '2021-01-29 07:57:38', NULL);
INSERT INTO `terms_conditions` (`id`, `name`, `display_name`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 'return-policy', 'Return Policy', '<p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We offer a hassle free 7 day return policy on our returnable** products. If you do not like a product or it does not fit well, then&nbsp;we will issue you store credit in form of a Gift Card that can be used to purchase anything you like. Please follow the these steps to initiate a return:</span></p><ol style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">You can simply login to your FabPik account and schedule a reverse pick up yourself, alternately you can email us at support@fabpik.in or call us at 9948037153 with 10 days of receiving the product. Please mention your order number and the product you want to return.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Once we receive your request, we will initiate the pickup from the shipping address mentioned in your original order.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We do not offer reverse pickup service for International orders where the address&nbsp;is outside of India. In case you want to return or exchange a product, please send it to the following address:</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><br style=\"margin-bottom: 0px;\"></span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik Ecomm LLP, 2</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">nd</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;Floor, VSSR Square, Madhapur, Hitech City, Hyderabad – 500081</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Please hand over the product to our logistics partner when they come to your doorstep. Please note the product&nbsp;must be unused and unwashed for hygiene reasons. Also the product tags should be intact with its original packing,&nbsp;else we will not be able to process your return. Reverse Pickup usually takes 1-3 days from the day the request is initiated.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">After we receive&nbsp;the return, the product will go through a quality check. Please note that it takes 7-10 days for us to receive the returned product.</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; list-style: decimal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Finally, after the product passes the quality check, we will issue you a store credit in form of a Gift Card that will be emailed to your original order email address. The Gift Card will have no expiry and can be used to purchase anything from our store.</span></li></ol><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">**Pre-Orders, Socks, Stockings &amp; Accessories can not be returned or exchanged. Used, washed, damaged or laundered products can not be returned/exchanged. Products tags and original packing must be intact to avail return/exchange.</span></p><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;</span><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Good To Know:</span></p><ul style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px 0px 0px 20px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: 1.4; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; list-style: none; color: rgb(119, 119, 119);\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We offer reverse pick up for majority of pincodes in India. But if&nbsp;reverse pick up is not available&nbsp;for your pincode, then we would request you to send the product back to us at the following address.</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><br style=\"margin-bottom: 0px;\"></span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik Ecomm LLP, 2</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">nd</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;Floor, VSSR Square, Madhapur, Hitech City, Hyderabad – 500081</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We can only offer exchanges based on availability of size upon receiving the return product and not before that.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Please allow up to 3 working days to get your Gift Card Credit&nbsp;after we have received the return product.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Images on the website are professionally clicked due to which sometimes the colour of actual product may vary from the image taken.</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Shipping charges are not refundable</span></li><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">In case a product is missing from your shipment or a product is damaged in transit, please call or email us within 48 hours of the receipt of the shipment for us to&nbsp;investigate further</span></li><li style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; position: relative; list-style: none;\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">We do not offer reverse pickup service for International orders where the address&nbsp;is outside of India. In case you want to return or exchange a product, please send it to the following address:</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><br style=\"margin-bottom: 0px;\"></span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Fabpik Ecomm LLP, 2</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">nd</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">&nbsp;Floor, VSSR Square, Madhapur, Hitech City, Hyderabad – 500081</span><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\"><br style=\"margin-bottom: 0px;\"></span></li></ul><p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"font-weight: 600; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">Cancel the order?</span></p><p style=\"margin-bottom: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119);\"><span style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit;\">An order can be cancelled until the order is dispatched, applicable only to non pre-order products. Orders with pre-order products can be cancelled only within&nbsp;24 hours of placing the order. Please call us on 9948037153 or email us&nbsp;at support@fabpik.in to cancel your order. If the order is shipped, we can not cancel the order. In case&nbsp;you reject a COD order,&nbsp;we will not be able to offer you COD service for any subsequent order.</span></p>', 1, '2021-01-29 07:56:32', '2021-01-29 07:58:14', NULL),
(6, 'shipping-delivery-policy', 'Shipping & Delivery Policy', '<p style=\"padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 17px; color: rgb(119, 119, 119);\">At FabPik.in,&nbsp;we make sure that your orders reach you in time. For this we are associated with trusted shipping channels&nbsp;who work 24/7 to help us send packages.<br>There are some facts related to our shipping policy that you must be aware of before placing an order with us.<br>Shipping Charges:<br>For orders with Delivery Address within India:<br>1. Free on orders of INR&nbsp;999 and above<br>2. INR&nbsp;100 on orders&nbsp;below&nbsp;INR 999<br>For orders with Delivery Address within USA, Canada, UK, Australia, New Zealand,<br>Singapore, Hong Kong, Malaysia, UAE &amp;amp;amp; Europe:<br>1. Free on orders of&nbsp;INR 14,999 and above<br>2.&nbsp;INR 1,499 on orders of&nbsp;INR 9,999 –&nbsp;INR 14,998<br>3. INR 999 on orders below INR 9,999<br>For orders with Delivery Address to any other Country:<br>1. INR 1,999 on orders of INR 9,999 and above<br>2. INR 1,499 on orders of INR 4,999 – INR 9,998<br>3. INR 999 on orders below INR 4,999<br>Time taken to deliver your order:<br>We get on our toes as soon as you place an order with us. Delivery times are usually as follows:<br>1. Within India – your order should reach you in 2 – 6 business days from the date of dispatch.<br>2. For USA, Canada, Australia, New Zealand, Singapore, Hong Kong, Malaysia, UAE &amp;amp;amp; Europe – your order should each you in 5 – 10 business days from the date of dispatch.<br>3. For UK – your order should reach you in 10 – 20 business days from the date of dispatch<br>4. Rest of the Countries – your order should reach you in 10 – 25 business days from the date of dispatch<br>Please note that the date of dispatch depends on the products you order. The product page displays exactly how many days will it take to dispatch that product.<br>Packaging:<br>All the orders that we ship are carefully packed so that the product retains its quality and reaches you in good shape. We make sure your order is sealed and discreet.<br>International Shipping:<br>International shipping is possible. The checkout page shows you the exact&nbsp;shipping rates for your country. The number of days for delivery can increase&nbsp;due to delay in customs processing at the destination country.<br>International Customs&nbsp;Charges:<br style=\"margin-bottom: 0px;\">Some destination countries&nbsp;charge customs on the shipments. All customs charges, local taxes&nbsp;and delivery company&amp;amp;#39;s processing&nbsp;fees for clearing customs is to be&nbsp;borne by the customer.</p><p style=\"margin-bottom: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Cabin, Arial, Helvetica, sans-serif; font-size: 17px; color: rgb(119, 119, 119);\">Tampered, Damaged or Opened Packages:<br>If you receive a shipment that seems tampered, damaged or opened – please do not accept that shipment. Take a photo of the shipment and get in touch with our customer care team at 9948037153 or email us at support@FabPik.in<br>Part Payment:<br style=\"margin-bottom: 0px;\">For certain&nbsp;Cash-on-Delivery orders,&nbsp;you might be required to make an&nbsp;advance part payment of&nbsp;INR 500 –&nbsp;INR 2000.&nbsp;We usually&nbsp;ask for a part payment in cases where the product is custom made especially for that order.</p>', 1, '2021-01-29 07:59:57', '2021-01-29 07:59:57', NULL),
(7, 'VariableCommission', 'Variable Commission', 'Terms', 1, '2021-03-19 13:55:06', '2021-03-19 13:56:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `customer_id` int DEFAULT NULL,
  `seller_id` int DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `customer_id`, `seller_id`, `name`, `avatar`, `mobile`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Super Admin', '01f9f55d88f12ce47fc749c03a4b27c3.jpg', '9000000000', 'admin@admin.com', NULL, '$2y$10$bGmkRTlOeC/.sfVn9x1G6upAgjjXcKL1rD3xhdcv0gBbIi9MGrGVG', NULL, '2020-11-06 23:52:32', '2020-11-06 23:52:32'),
(111, 1, NULL, 'Avez', '71728c5900e9ad68502cd52ddde55107.jpg', '9000000000', 'mohammedavez85@gmail.com', '2021-01-21 10:16:48', '$2y$10$w8fcx/fvA5RMxuKU7NNpJ.aJZd7mFXr24sk.gQwnd0f2YRqbT9w5q', NULL, '2021-01-20 03:37:51', '2021-01-21 04:34:56'),
(114, 3, NULL, NULL, NULL, '9000000000', NULL, NULL, '$2y$10$HQ43hKQJnl1HfC8mEcmsMOTsoyVareZkP4DfmjIBD6Rr9orXGWao2', NULL, '2021-01-20 08:32:06', '2021-01-20 08:32:06'),
(117, NULL, 4, 'lavanya', 'a43764a1e81f245dbb36aec311fc1678.jpg', '9000000000', 'lavanyamiraki@gmail.com', NULL, '$2y$10$2DWX5xAs/jgBVxhB/zREZexZv8TQJWw9IecFvlS5I1YnH5/Q2JtzK', 'uqtPA5PGoNmCHBHkj5CwKzBBdUc4JLxF', '2021-01-21 01:18:53', '2021-01-29 06:01:23'),
(118, 4, NULL, 'Abhi', 'f918e970e43fb36b585e3c2cf0380591.jpg', '9000000000', 'abhi.adulapuram71@gmail.com', NULL, '$2y$10$1Lkco5phhL2dC1qGgNolq.3neWLeclDi0CRUuGy3a5qMqA8x07Fv2', NULL, '2021-01-21 01:18:55', '2021-01-23 00:21:15'),
(120, 6, NULL, 'Abhi', '6f59733172d22076e68ee755f772efaf.jpg', '9000000000', 'abhishekmiraki@gmail.com', NULL, '$2y$10$hOfBpeKDsJImFwXDEce1dedujOZuiqtbX.b2EvIu4apPxlc2ckoiu', NULL, '2021-01-21 03:22:01', '2021-01-22 02:12:39'),
(121, 7, NULL, 'Anitha', '0698a274a1c9a927a6155476ca0ed9f2.jpg', '9000000000', 'anithamiraki@gmail.com', NULL, '$2y$10$S4JKKOBoo/aYLMmtFB4JieASHm6yvQR3btj2h0kdvMihst/2eLgqG', NULL, '2021-01-21 04:08:02', '2021-01-21 04:14:39'),
(122, 8, NULL, 'Hari', '81ada494f5bb1a721ad2c6ec8dbe2fda.jpg', '9000000000', 'avezmiraki@gmail.com', NULL, '$2y$10$bGmkRTlOeC/.sfVn9x1G6upAgjjXcKL1rD3xhdcv0gBbIi9MGrGVG', NULL, '2021-01-21 04:52:06', '2021-01-21 04:53:33'),
(123, 9, NULL, 'Avez', '377fda5c14c2cc84e450e40275657bb4.jpg', '9000000000', 'harikk@mirakitech.com', NULL, '$2y$10$OkPVq2cxc3gFuG1C4fQScOdOKrND489C8nQMObO40Dj4unxAuCQ9G', NULL, '2021-01-21 05:40:43', '2021-01-21 06:58:45'),
(124, NULL, 5, 'Praveen', 'f12fac3d6e2f18fc690de8da99923989.jpg', '9000000000', 'praveenmiraki@gmail.com', NULL, '$2y$10$lUfRKl7IvNnEePjdSggRjObKIQSIKIm0HiRUc1yMpxhf/MIKOnr1O', 'WElld1Se0e3o1Mt7VoiJbYN4DFE6NhW2IW7eXD3n485rD8siPyATNBRGoeGv', '2021-01-21 07:36:42', '2021-01-21 07:37:52'),
(125, NULL, 6, 'Aakash', '2907d3229cfe76170533ca55b8416e32.jpg', '9000000000', 'aakashranjan@fabpik.in', NULL, '$2y$10$sRNm2uCoRq4qe3AlnzIa9ejpCSu.BddKlG5NjjvvuSi62XYYRtZyi', 'irEGiDMPIXnaLryqPn2VIn0mtFz7v6c3hdXFFgwYhgkd2EvQdLrR7tqRGv1T', '2021-01-21 07:48:54', '2021-01-21 07:48:54'),
(126, 10, NULL, 'Deepak', 'b6bbd76dc27aed18d4fca4b5f029bd9c.jpg', '9000000000', 'deepakn@mirakitech.com', NULL, '$2y$10$e09/HaFB/5k77z1l1.G86OowOwEKr9tsFpD7/rwgWyQpNGo6oSv6a', NULL, '2021-01-21 10:01:43', '2021-01-21 10:21:11'),
(127, NULL, 7, 'DeepakRegFullName', NULL, '9000000000', 'hitedo9258@febula.com', NULL, '$2y$10$Sf3CxyRI.sO0Ite5ZkN4d.FqVtwq2.6m3xuOM8cQY1l5yj7E8Rpma', 'rc5OPbmCQMywyMOEcT1X2uwVwJfC3S6y', '2021-01-22 08:48:00', '2021-01-22 08:48:00'),
(128, NULL, 8, 'mirakitest', '20805fd63d802ef147c63c5cc42a7958.jpg', '9000000000', 'mirakitest@yopmail.com', NULL, '$2y$10$/V1XVTaiHP1/ZdlmCP8lFeZaCkgRmwwNJwnL6sEq2naW5JcbaMFRi', 'aCxojAIwhB9VU4bTjO2sIWWeyWkh7h0k', '2021-01-24 23:31:27', '2021-01-24 23:31:27'),
(129, NULL, 9, 'Test', NULL, '9000000000', 'a.harikrishna108@gmail.com', NULL, '$2y$10$0mC3dBUXrkgnaMBr2KCHQOpFE6FX0sKyhgTyPWrXdwlAn0931eCfK', 'mq6lSq4RsrGKRe8sMrC2c3o0ZGoEWtUp', '2021-01-27 00:51:32', '2021-02-17 07:05:17'),
(130, NULL, 10, 'Verified User', NULL, '9000000000', 'verified@verified.com', NULL, '$2y$10$jc/jIhACcobYHC4ZS23W6ObAYCtxVq3.JkblCImEncwwYFgUymoJm', 'ruLkdlAaGEqLs6g4vIzsTUAOQA6Vi4s1', '2021-01-27 01:56:56', '2021-01-27 01:56:56'),
(131, NULL, 11, 'charan', '723422e4e2333abbdb3f1c56c64be755.jpg', '9000000000', 'charanmiraki@gmail.com', NULL, '$2y$10$PRSBqZ3oUbCbqlEeSijGbOXjfzrr7hvjxsUkA.1ZDdUarJ8AVX8hq', 'BzvtKdrdV6EXSnl3noRSnnrvFw5MocFM', '2021-01-27 08:28:48', '2021-01-27 08:28:48'),
(132, 11, NULL, 'SRIHARSHA SRIRAMOJU', NULL, '9000000000', 'mirakitech@gmail.com', NULL, '$2y$10$i4ueLQV3tpibZPVVvOzP.u/3Kdyd2LmmBhJ0HUwxaBvet7qrsy0Eu', NULL, '2021-01-28 05:20:59', '2021-01-28 05:20:59'),
(134, NULL, 13, 'hari Krishna', '557bb4663a7ed5afc3fa1da32378b488.jpg', '8328566312', 'harik@mirakitech.com', NULL, '$2y$10$ZTjDlQdjKoOV5uNq4Cwr0emOf7M90EX/gmm693aJ3TgRyeIiGZcsK', 'LUphR7euSI6kPAwHghapySnaCbYGXWyFauwDeKVxFoL7dmBpc1eEiMRF8ElV', '2021-01-29 01:32:04', '2021-03-18 06:47:17'),
(135, 12, NULL, 'lavanya', 'bd4590b5f8320df9977f4d2ccf06decc.jpg', '9000000000', 'lavanyamiraki1@yopmail.com', NULL, '$2y$10$bCFe0LTmq4INsZso0lVw.ePGi5M.Zgm0kZJxK7eMaBYbTBUXkiJ4C', NULL, '2021-01-29 01:46:08', '2021-01-29 05:50:30'),
(136, NULL, 14, 'harikrishna', 'dc82e6b89094d5fddd7318a6a579d3f3.jpg', '9000000000', 'tempemailseven@yopmail.com', NULL, '$2y$10$9bTIhdBQC8ET71KJLkp7pOA6.L5zgWyEUENdmdT1oQhmwwPx9eXZq', 'HxqFpUSFBfyx1k0wpoHmaZvcyUFJKnZi', '2021-01-29 01:54:47', '2021-01-29 01:54:47'),
(137, NULL, 15, 'Arina Sethi', '0c07d975cba27e8f101c4bbd392e84da.jpg', '9000000000', 'babyaroosa04@gmail.com', NULL, '$2y$10$fWB.apGtNpipRbwJJCIdZ.naT7XazgViQu/TkuVfP2hYwF4Ige1FC', '6hfS36pWrJgBYLZYtfCxa8igeBaVj1iv9QDR8rbKWJbA4Nyj0JJm1LLlcngB', '2021-01-29 01:58:49', '2021-01-29 01:58:49'),
(138, NULL, 16, 'Meenaakshi overseas', '17197b1adcddfc510b427879ca15c0b8.jpg', '9000000000', 'sendilkumaar@parrotcrow.in', NULL, '$2y$10$fL9ngj76KWHjEfKxd4R.bebd/gzffNYNRNpubEOkVWzSuOIB/fTvu', 'nGIJzQu5LjyloYzag09u130nIM25BjgiLQ7rxIFY4YP3GA7LoYVxa2sjpmpu', '2021-01-29 02:37:20', '2021-01-29 02:37:20'),
(139, NULL, 17, 'Harsh Mohta', '0b1b5a5a8e6a2a68977b0910f5a37bfb.jpg', '9000000000', 'hello@neenee.in', NULL, '$2y$10$8mta7KFUfAZEzlX6ZltX2.5cxeSDUF3u9.d0oNXUaGH1O1CQ43Dga', 'xgSSf2kmrdp4A19FAkOMn1IWO0R0hGDB', '2021-01-29 03:18:03', '2021-01-29 03:18:03'),
(140, NULL, 18, 'hari', '568843d3c4a7cabb254585076b1c1705.jpg', '9290574912', 'ahk9290@gmail.com', NULL, '$2y$10$pIpKwee5.9GKyutd6NXXWuHR0zVXEFacTehoYU9cBvUFLiE/tbQai', 'riUkC2XJamdv4nWGv2I7bGvGUNckNBaA3XM4W6EKVe0KRhuTPeu4XyXFtMHG', '2021-02-09 10:43:28', '2021-03-05 07:16:13'),
(142, NULL, NULL, 'Super Admin', NULL, '9000000000', 'vendorsupport@fabpik.in', NULL, '$2y$10$5iGBqR7gIQTZLFkqBHPWt.qr6TrRrLfr2X1ngd96RIi5YOZsNKS8e', NULL, '2021-02-11 05:38:49', '2021-02-11 05:38:49'),
(147, NULL, 21, 'name', '85c92940a96acf74e27a4ffea8b08b06.jpg', '9290573910', 'temp123456@yopmail.com', '2021-02-16 19:37:43', '$2y$10$306Ftg2dVD.xXnqN1IVgGecUjr2mj1lCxKLgxSYzd2bQup2Jss6Be', 'BpCgyXn7irdBkxCssAqRygHaZzpJPWLRzgR24EO2OUQO4jcKQdX38pTuU4Sp', '2021-02-17 07:37:43', '2021-02-17 07:37:43'),
(149, NULL, NULL, 'Super Admin', NULL, '1234567890', 'aakashr@fabpik.in', NULL, '$2y$10$LcXtV6M28t0rey5rCalvWe3hsLu.VEnEOKxOgZ3RJFpHo4LqaIGBy', NULL, '2021-03-23 05:41:01', '2021-03-23 05:41:01'),
(150, NULL, 22, 'Krishna', '348e10fcdc9b7d6a7f155aaae30de957.jpg', '9293275832', 'ljiasiqs@maxresistance.com', NULL, '$2y$10$03lyYdESsfmcyEPfHTBMMOWhk9Z7MaFncgZBNELfJTpKqUjNEAUu2', 'PyhjBCs5ulPMOdGb2YJDaiaEnhHyYllm', '2021-03-23 10:36:29', '2021-03-23 10:36:29');

-- --------------------------------------------------------

--
-- Table structure for table `variation_images`
--

CREATE TABLE `variation_images` (
  `id` int NOT NULL,
  `product_id` int DEFAULT NULL,
  `attribute_option_id` int DEFAULT NULL,
  `thumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variation_images`
--

INSERT INTO `variation_images` (`id`, `product_id`, `attribute_option_id`, `thumbnail`, `images`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 50, '1', '[\"03022021/NEE1-FOX.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-02-26 06:03:14', '2021-02-26 06:03:14', NULL),
(2, 1, 41, '1', '[\"100007/products\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-02-26 06:03:14', '2021-02-26 06:03:14', NULL),
(3, 1, 44, '1', '[\"100007/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-02-26 06:03:14', '2021-02-26 06:03:14', NULL),
(4, 2, 50, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-02-26 06:03:14', '2021-02-26 06:03:14', NULL),
(5, 3, 50, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-02-26 06:19:18', '2021-02-26 06:19:18', NULL),
(6, 3, 41, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-02-26 06:19:18', '2021-02-26 06:19:18', NULL),
(7, 3, 44, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-02-26 06:19:18', '2021-02-26 06:19:18', NULL),
(8, 4, 50, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-02-26 06:19:18', '2021-02-26 06:19:18', NULL),
(9, 5, 50, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-01 09:46:51', '2021-03-01 09:48:44', NULL),
(10, 6, 50, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-01 09:46:51', '2021-03-01 09:48:44', NULL),
(11, 7, 42, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-01 10:49:27', '2021-03-01 10:49:27', NULL),
(12, 8, 27, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-01 10:56:35', '2021-03-01 10:56:35', NULL),
(13, 9, 20, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-01 11:01:07', '2021-03-01 11:01:07', NULL),
(14, 10, 41, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-01 11:09:13', '2021-03-01 11:09:13', NULL),
(15, 11, 0, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-01 11:25:15', '2021-03-01 11:25:15', NULL),
(16, 14, 27, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-01 11:42:55', '2021-03-01 11:42:55', NULL),
(17, 15, 0, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-01 11:46:38', '2021-03-01 11:46:38', NULL),
(18, 16, 47, '1', '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-05 06:14:12', '2021-03-05 06:14:12', NULL),
(19, 17, 47, NULL, '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-05 07:11:50', '2021-03-05 07:11:50', NULL),
(20, 11, 54, NULL, '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-17 12:14:32', '2021-03-17 12:14:32', NULL),
(21, 11, 47, NULL, '[\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\",\"100007/products/03022021/NEE1-FOX-2.jpg\"]', '2021-03-17 12:14:32', '2021-03-17 12:14:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `verify_otp`
--

CREATE TABLE `verify_otp` (
  `id` int NOT NULL,
  `mobile` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_expiry` timestamp NULL DEFAULT NULL,
  `tries` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `verify_otp`
--

INSERT INTO `verify_otp` (`id`, `mobile`, `email`, `otp`, `otp_expiry`, `tries`, `created_at`, `updated_at`) VALUES
(2, '9618629334', NULL, '5601', '2021-01-29 01:34:31', NULL, '2021-01-20 11:27:28', NULL),
(3, NULL, 'naveenmiraki@gmail.com', '823076', '2021-01-29 01:34:15', NULL, '2021-01-20 11:27:28', NULL),
(9, NULL, 'lavanyamiraki@gmail.com', '783201', '2021-01-21 01:20:04', NULL, '2021-01-21 06:47:04', NULL),
(22, '8328566312', NULL, '3408', '2021-02-16 10:01:50', NULL, '2021-01-21 10:56:30', NULL),
(25, '7382207022', NULL, '7253', '2021-01-21 07:38:04', NULL, '2021-01-21 13:05:04', NULL),
(26, NULL, 'praveenmiraki@gmail.com', '134965', '2021-01-21 07:38:04', NULL, '2021-01-21 13:05:04', NULL),
(27, '8884273650', NULL, '0257', '2021-01-21 07:51:01', NULL, '2021-01-21 13:18:01', NULL),
(28, NULL, 'aakashr@fabpik.in', '681423', '2021-01-21 07:51:01', NULL, '2021-01-21 13:18:01', NULL),
(32, '8520021995', NULL, '8705', '2021-01-21 10:25:29', NULL, '2021-01-21 15:52:29', NULL),
(48, NULL, 'sefih30326@loopsnow.com', '173802', '2021-01-22 07:54:17', NULL, '2021-01-22 13:18:06', NULL),
(49, NULL, 'hitedo9258@febula.com', '051498', '2021-01-22 08:50:04', NULL, '2021-01-22 13:21:47', NULL),
(56, '9618050570', NULL, '4095', '2021-01-24 23:33:57', NULL, '2021-01-25 05:00:57', NULL),
(57, NULL, 'mirakitest@yopmail.com', '789321', '2021-01-24 23:33:57', NULL, '2021-01-25 05:00:57', NULL),
(59, '9948597496', NULL, '8537', '2021-01-25 00:03:56', NULL, '2021-01-25 05:30:56', NULL),
(70, NULL, 'sopap93232@febula.com', '217439', '2021-01-27 00:51:56', NULL, '2021-01-27 06:06:17', NULL),
(71, NULL, 'kabeha1319@febula.com', '351860', '2021-01-27 01:59:27', NULL, '2021-01-27 07:06:55', NULL),
(74, '9899589050', NULL, '3087', '2021-01-27 08:22:13', NULL, '2021-01-27 13:49:13', NULL),
(75, '8500456815', NULL, '8392', '2021-01-27 08:30:57', NULL, '2021-01-27 13:54:48', NULL),
(76, NULL, 'charanmiraki@gmail.com', '893014', '2021-01-27 08:30:57', NULL, '2021-01-27 13:57:57', NULL),
(78, '9293275832', NULL, '6413', '2021-03-23 10:37:47', NULL, '2021-01-27 16:09:34', NULL),
(79, '9293573910', NULL, '5203', '2021-01-27 10:55:31', NULL, '2021-01-27 16:22:24', NULL),
(90, '9948839362', NULL, '8297', '2021-01-29 08:29:59', NULL, '2021-01-28 12:59:05', NULL),
(96, '9814200011', NULL, '6809', '2021-01-29 01:59:50', NULL, '2021-01-29 05:53:10', NULL),
(97, NULL, 'babyaroosa04@gmail.com', '079468', '2021-01-29 01:59:50', NULL, '2021-01-29 05:53:10', NULL),
(102, '9676468686', NULL, '3879', '2021-01-29 11:06:33', NULL, '2021-01-29 06:36:13', NULL),
(105, NULL, 'tempemailseven@yopmail.com', '534901', '2021-01-29 01:56:42', NULL, '2021-01-29 06:56:03', NULL),
(106, '9866336421', NULL, '8439', '2021-01-29 01:38:51', NULL, '2021-01-29 07:05:51', NULL),
(107, NULL, 'naveenkli@yopmail.com', '351847', '2021-01-29 01:38:51', NULL, '2021-01-29 07:05:51', NULL),
(109, '9345393943', NULL, '7621', '2021-01-29 02:38:33', NULL, '2021-01-29 07:18:53', NULL),
(110, NULL, 'sendilkumaar@parrotcrow.in', '297031', '2021-01-29 02:38:33', NULL, '2021-01-29 07:18:53', NULL),
(111, '9498883068', NULL, '1564', '2021-01-29 01:52:31', NULL, '2021-01-29 07:19:31', NULL),
(112, NULL, 'a.harikrishna108@gmail.com', '354067', '2021-02-09 10:38:08', NULL, '2021-01-29 07:19:31', NULL),
(116, '9038475000', NULL, '6819', '2021-01-29 03:18:51', NULL, '2021-01-29 08:45:51', NULL),
(117, NULL, 'hello@neenee.in', '097136', '2021-01-29 03:18:51', NULL, '2021-01-29 08:45:51', NULL),
(131, NULL, 'anitha@yopmail.com', '518479', '2021-01-29 06:49:59', NULL, '2021-01-29 12:16:59', NULL),
(138, '9550261360', NULL, '1784', '2021-01-29 11:05:34', NULL, '2021-01-29 16:32:34', NULL),
(141, '9549588439', NULL, '8602', '2021-02-02 03:07:16', NULL, '2021-01-30 11:28:20', NULL),
(151, '9290573910', NULL, '7453', '2021-02-17 07:40:33', NULL, '2021-02-09 10:35:08', NULL),
(152, NULL, 'sidik49510@hrandod.com', '159043', '2021-02-16 10:01:50', NULL, '2021-02-16 09:58:50', NULL),
(153, NULL, 'voyire6514@timothyjsilverman.com', '934867', '2021-03-03 12:08:41', NULL, '2021-03-03 12:01:39', NULL),
(154, NULL, 'ljiasiqs@maxresistance.com', '329761', '2021-03-23 10:37:48', NULL, '2021-03-23 10:34:48', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_categories`
-- (See below for the actual view)
--
CREATE TABLE `view_categories` (
`child_id` int
,`child_slug` varchar(191)
,`subcategory_id` int
,`category_id` int
,`child_title` varchar(191)
,`subcategory_title` varchar(191)
,`subcat_slug` varchar(255)
,`category_title` varchar(191)
,`cat_slug` varchar(255)
,`path_id` varchar(33)
,`path_title` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_cat_product_count`
-- (See below for the actual view)
--
CREATE TABLE `view_cat_product_count` (
`category_id` int
,`total_products` bigint
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_childcat_product_count`
-- (See below for the actual view)
--
CREATE TABLE `view_childcat_product_count` (
`childcategory_id` int
,`total_products` bigint
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_product_images`
-- (See below for the actual view)
--
CREATE TABLE `view_product_images` (
`images` text
,`thumbnail` varchar(255)
,`product_variant_id` int
,`product_id` int
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_subcat_product_count`
-- (See below for the actual view)
--
CREATE TABLE `view_subcat_product_count` (
`subcategory_id` int
,`total_products` bigint
);

-- --------------------------------------------------------

--
-- Table structure for table `washing_types`
--

CREATE TABLE `washing_types` (
  `id` int NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `washing_types`
--

INSERT INTO `washing_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Dry Clean Only', '2020-11-18 18:35:11', NULL),
(2, 'Regular Wash / Hand Wash', '2020-11-18 18:35:38', NULL),
(3, 'Machine Wash', '2021-01-28 12:45:08', NULL),
(4, 'Wash Dark Colors Separately', '2021-01-28 12:45:08', NULL),
(5, 'N/A', '2021-02-09 13:13:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int NOT NULL,
  `customer_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `product_variant_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `customer_id`, `product_id`, `product_variant_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 11, NULL, 19, '2021-01-07 11:24:00', '2021-01-07 11:24:16', '2021-01-07 11:24:16'),
(2, 11, NULL, 17, '2021-01-07 11:24:05', '2021-01-07 11:24:18', '2021-01-07 11:24:18'),
(5, 11, NULL, 19, '2021-01-07 11:26:08', '2021-01-07 11:26:31', '2021-01-07 11:26:31'),
(7, 11, NULL, 17, '2021-01-07 11:27:32', '2021-01-07 07:31:41', '2021-01-07 07:31:41'),
(8, 11, NULL, 10, '2021-01-07 11:29:49', '2021-01-07 09:00:45', '2021-01-07 09:00:45'),
(9, 11, NULL, 19, '2021-01-07 11:30:47', '2021-01-07 09:02:19', '2021-01-07 09:02:19'),
(10, 9, NULL, 19, '2021-01-07 06:50:23', '2021-01-07 08:24:05', '2021-01-07 08:24:05'),
(11, 10, NULL, 19, '2021-01-09 01:26:36', '2021-01-09 01:27:40', '2021-01-09 01:27:40'),
(12, 10, NULL, 17, '2021-01-09 01:26:39', '2021-01-09 01:27:50', '2021-01-09 01:27:50'),
(13, 11, NULL, 19, '2021-01-12 00:57:07', '2021-01-12 00:57:10', '2021-01-12 00:57:10'),
(45, 19, 70, 13, '2021-01-14 04:16:28', '2021-01-14 04:16:28', NULL),
(49, 19, 74, 33, '2021-01-14 04:19:11', '2021-01-14 04:19:11', NULL),
(51, 19, 81, 68, '2021-01-15 05:57:40', '2021-01-15 05:57:40', NULL),
(52, 11, 100, 106, '2021-01-20 02:15:38', '2021-01-20 02:15:38', NULL),
(54, 9, 100, 106, '2021-01-20 03:03:12', '2021-01-20 03:03:12', NULL),
(55, 5, 3, 4, '2021-01-21 02:23:47', '2021-01-21 02:23:47', NULL),
(56, 4, 3, 4, '2021-01-21 03:05:16', '2021-01-21 03:05:16', NULL),
(62, 4, 14, 43, '2021-01-21 09:48:36', '2021-01-21 09:48:36', NULL),
(63, 4, 19, 54, '2021-01-21 09:48:39', '2021-01-21 09:48:39', NULL);

-- --------------------------------------------------------

--
-- Structure for view `view_categories`
--
DROP TABLE IF EXISTS `view_categories`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_categories`  AS  select `t1`.`id` AS `child_id`,`t1`.`slug` AS `child_slug`,`t1`.`subcategory_id` AS `subcategory_id`,`t1`.`category_id` AS `category_id`,`t1`.`title` AS `child_title`,`t2`.`title` AS `subcategory_title`,`t2`.`slug` AS `subcat_slug`,`t3`.`title` AS `category_title`,`t3`.`slug` AS `cat_slug`,concat(`t3`.`id`,'-',`t2`.`id`,'-',`t1`.`id`) AS `path_id`,concat(`t3`.`title`,' > ',`t2`.`title`,' > ',`t1`.`title`) AS `path_title` from ((`childcategories` `t1` join `subcategories` `t2` on((`t2`.`id` = `t1`.`subcategory_id`))) join `categories` `t3` on((`t3`.`id` = `t1`.`category_id`))) where ((`t1`.`deleted_at` is null) and (`t2`.`deleted_at` is null) and (`t3`.`deleted_at` is null) and (`t1`.`status` = 1) and (`t2`.`status` = 1) and (`t3`.`status` = 1)) order by `t3`.`title`,`t2`.`title`,`t1`.`title` ;

-- --------------------------------------------------------

--
-- Structure for view `view_cat_product_count`
--
DROP TABLE IF EXISTS `view_cat_product_count`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_cat_product_count`  AS  select `t1`.`category_id` AS `category_id`,count(`t1`.`id`) AS `total_products` from (((`product_categories` `t1` join `products` `t2` on((`t2`.`id` = `t1`.`product_id`))) join `product_variants` `t3` on((`t3`.`product_id` = `t2`.`id`))) join `view_categories` `t4` on((`t4`.`category_id` = `t1`.`category_id`))) where ((`t2`.`status` = 1) and (`t2`.`deleted_at` is null)) group by `t1`.`category_id` ;

-- --------------------------------------------------------

--
-- Structure for view `view_childcat_product_count`
--
DROP TABLE IF EXISTS `view_childcat_product_count`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_childcat_product_count`  AS  select `t1`.`childcategory_id` AS `childcategory_id`,count(`t1`.`id`) AS `total_products` from (((`product_categories` `t1` join `products` `t2` on((`t2`.`id` = `t1`.`product_id`))) join `product_variants` `t3` on((`t3`.`product_id` = `t2`.`id`))) join `view_categories` `t4` on((`t4`.`child_id` = `t1`.`childcategory_id`))) where ((`t2`.`status` = 1) and (`t2`.`deleted_at` is null)) group by `t1`.`childcategory_id` ;

-- --------------------------------------------------------

--
-- Structure for view `view_product_images`
--
DROP TABLE IF EXISTS `view_product_images`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_product_images`  AS  select `vm`.`images` AS `images`,`vm`.`thumbnail` AS `thumbnail`,`product_variants`.`id` AS `product_variant_id`,`product_variants`.`product_id` AS `product_id` from (((((`product_variants` join `products` on((`products`.`id` = `product_variants`.`product_id`))) join `sellers` on((`sellers`.`id` = `products`.`seller_id`))) join `categories` `c` on((`c`.`id` = `products`.`primary_category`))) join `product_variant_options` `pvo` on((`pvo`.`product_variant_id` = `product_variants`.`id`))) left join `variation_images` `vm` on(((`vm`.`attribute_option_id` = `pvo`.`attribute_option_id`) and (`vm`.`product_id` = `products`.`id`)))) where ((`pvo`.`attribute_id` = `c`.`primary_attribute`) and (`products`.`status` = 1) and (`products`.`deleted_at` is null) and (`product_variants`.`deleted_at` is null) and (`c`.`status` = 1) and (`c`.`deleted_at` is null) and (`sellers`.`status` = 1) and (`sellers`.`deleted_at` is null) and (`sellers`.`approval_status` = 1)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_subcat_product_count`
--
DROP TABLE IF EXISTS `view_subcat_product_count`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_subcat_product_count`  AS  select `t1`.`subcategory_id` AS `subcategory_id`,count(`t1`.`id`) AS `total_products` from (((`product_categories` `t1` join `products` `t2` on((`t2`.`id` = `t1`.`product_id`))) join `product_variants` `t3` on((`t3`.`product_id` = `t2`.`id`))) join `view_categories` `t4` on((`t4`.`subcategory_id` = `t1`.`subcategory_id`))) where ((`t2`.`status` = 1) and (`t2`.`deleted_at` is null)) group by `t1`.`subcategory_id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`,`display_name`);

--
-- Indexes for table `attribute_options`
--
ALTER TABLE `attribute_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_id` (`attribute_id`,`option_name`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`,`slug`,`show_on_home`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `seller_id` (`seller_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_variant_id` (`product_variant_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `customer_id_2` (`customer_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`),
  ADD KEY `primary_attribute` (`primary_attribute`),
  ADD KEY `secondary_attribute` (`secondary_attribute`);

--
-- Indexes for table `childcategories`
--
ALTER TABLE `childcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `subcategory_id` (`subcategory_id`),
  ADD KEY `title` (`title`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seller_id` (`seller_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `from_user` (`from_user`),
  ADD KEY `to_user` (`to_user`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dis_type` (`dis_type`),
  ADD KEY `sub_type` (`sub_type`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `email` (`email`),
  ADD KEY `mobile` (`mobile`);

--
-- Indexes for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `mobile` (`mobile`),
  ADD KEY `addr_type` (`addr_type`),
  ADD KEY `state_id` (`state_id`);

--
-- Indexes for table `customer_feedback`
--
ALTER TABLE `customer_feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `handeling_charges`
--
ALTER TABLE `handeling_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help_desks`
--
ALTER TABLE `help_desks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticket_no` (`ticket_no`),
  ADD KEY `seller_id` (`seller_id`);

--
-- Indexes for table `help_desk_comments`
--
ALTER TABLE `help_desk_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `help_desk_id` (`help_desk_id`),
  ADD KEY `comment_by` (`comment_by`);

--
-- Indexes for table `iron_types`
--
ALTER TABLE `iron_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `email` (`email`),
  ADD KEY `mobile` (`mobile`),
  ADD KEY `billing_state_id` (`billing_state_id`),
  ADD KEY `shipping_state_id` (`shipping_state_id`),
  ADD KEY `order_status_id` (`order_status_id`),
  ADD KEY `coupon_id` (`coupon_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_status_id` (`order_status_id`),
  ADD KEY `shipping_status_id` (`shipping_status_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `product_variant_id` (`product_variant_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `seller_id` (`seller_id`),
  ADD KEY `tracking_no` (`tracking_no`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `invoice_no` (`child_order_id`),
  ADD KEY `payment_status_id` (`payment_status_id`);

--
-- Indexes for table `order_handling_charge`
--
ALTER TABLE `order_handling_charge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_histories`
--
ALTER TABLE `order_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `order_detail_id` (`order_detail_id`),
  ADD KEY `order_status_id` (`order_status_id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_statuses`
--
ALTER TABLE `payment_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `seller_id` (`seller_id`),
  ADD KEY `brand_id` (`brand_id`),
  ADD KEY `unique_id` (`unique_id`),
  ADD KEY `primary_category` (`primary_category`),
  ADD KEY `size_chart_id` (`size_chart_id`),
  ADD KEY `washing_type_id` (`washing_type_id`),
  ADD KEY `iron_type_id` (`iron_type_id`),
  ADD KEY `country_of_origin` (`country_of_origin`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `subcategory_id` (`subcategory_id`),
  ADD KEY `childcategory_id` (`childcategory_id`);

--
-- Indexes for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_variant_options`
--
ALTER TABLE `product_variant_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_variant_id` (`product_variant_id`,`attribute_id`,`attribute_option_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `unique_id` (`unique_id`),
  ADD KEY `email` (`email`),
  ADD KEY `mobile` (`mobile`);

--
-- Indexes for table `seller_setting_change_logs`
--
ALTER TABLE `seller_setting_change_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_warehouses`
--
ALTER TABLE `seller_warehouses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seller_id` (`seller_id`,`state_id`);

--
-- Indexes for table `shiping_charges`
--
ALTER TABLE `shiping_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_statuses`
--
ALTER TABLE `shipping_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `size_charts`
--
ALTER TABLE `size_charts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seller_id` (`seller_id`);

--
-- Indexes for table `sms_logs`
--
ALTER TABLE `sms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `title` (`title`);

--
-- Indexes for table `terms_conditions`
--
ALTER TABLE `terms_conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `customer_id` (`customer_id`,`seller_id`,`mobile`);

--
-- Indexes for table `variation_images`
--
ALTER TABLE `variation_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`,`attribute_option_id`),
  ADD KEY `product_id_2` (`product_id`,`attribute_option_id`);

--
-- Indexes for table `verify_otp`
--
ALTER TABLE `verify_otp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mobile` (`mobile`);

--
-- Indexes for table `washing_types`
--
ALTER TABLE `washing_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`,`product_id`,`product_variant_id`),
  ADD KEY `customer_id_2` (`customer_id`,`product_id`,`product_variant_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `attribute_options`
--
ALTER TABLE `attribute_options`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `childcategories`
--
ALTER TABLE `childcategories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=434;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `customer_feedback`
--
ALTER TABLE `customer_feedback`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `handeling_charges`
--
ALTER TABLE `handeling_charges`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `help_desks`
--
ALTER TABLE `help_desks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `help_desk_comments`
--
ALTER TABLE `help_desk_comments`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `iron_types`
--
ALTER TABLE `iron_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `order_handling_charge`
--
ALTER TABLE `order_handling_charge`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `order_histories`
--
ALTER TABLE `order_histories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `payment_statuses`
--
ALTER TABLE `payment_statuses`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `product_variants`
--
ALTER TABLE `product_variants`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `product_variant_options`
--
ALTER TABLE `product_variant_options`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sellers`
--
ALTER TABLE `sellers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `seller_setting_change_logs`
--
ALTER TABLE `seller_setting_change_logs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `seller_warehouses`
--
ALTER TABLE `seller_warehouses`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `shiping_charges`
--
ALTER TABLE `shiping_charges`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shipping_statuses`
--
ALTER TABLE `shipping_statuses`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `size_charts`
--
ALTER TABLE `size_charts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sms_logs`
--
ALTER TABLE `sms_logs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=908;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `terms_conditions`
--
ALTER TABLE `terms_conditions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `variation_images`
--
ALTER TABLE `variation_images`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `verify_otp`
--
ALTER TABLE `verify_otp`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `washing_types`
--
ALTER TABLE `washing_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`payment_status_id`) REFERENCES `payment_statuses` (`id`);

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

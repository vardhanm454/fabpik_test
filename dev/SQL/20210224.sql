ALTER TABLE `coupons` ADD `seller_ids` JSON NULL AFTER `max_usage`, ADD `category_ids` JSON NULL AFTER `seller_ids`, ADD `product_varient_ids` JSON NULL AFTER `category_ids`;

ALTER TABLE `sellers` ADD `brand_id` INT(11) NULL AFTER `brand_name`;

ALTER TABLE `brands` ADD `created_by` INT(11) NULL AFTER `show_on_home`;
CREATE TABLE `conversations`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `seller_id` INT NULL DEFAULT NULL,
    `user_id` INT NULL DEFAULT NULL,
    `from_user` INT NULL DEFAULT NULL,
    `to_user` INT NULL DEFAULT NULL,
    `message` VARCHAR(255) NULL DEFAULT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY(`id`),
    INDEX(`seller_id`),
    INDEX(`user_id`),
    INDEX(`from_user`),
    INDEX(`to_user`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE `brands`  ADD `meta_title` VARCHAR(255) NULL DEFAULT NULL  AFTER `show_on_home`;
ALTER TABLE `categories`  ADD `meta_title` VARCHAR(255) NULL DEFAULT NULL  AFTER `tax`;
ALTER TABLE `childcategories`  ADD `meta_title` VARCHAR(255) NULL DEFAULT NULL  AFTER `description`;
ALTER TABLE `subcategories`  ADD `meta_title` VARCHAR(255) NULL DEFAULT NULL  AFTER `description`;
ALTER TABLE `products`  ADD `meta_title` VARCHAR(255) NULL DEFAULT NULL  AFTER `featured`,  ADD `meta_keywords` VARCHAR(255) NULL DEFAULT NULL  AFTER `meta_title`,  ADD `meta_description` VARCHAR(255) NULL DEFAULT NULL  AFTER `meta_keywords`;
ALTER TABLE `products`  ADD `unique_id` INT NULL DEFAULT NULL  AFTER `sku`;
ALTER TABLE `products`  ADD `primary_category` INT NULL  AFTER `deleted_at`,  ADD `primary_attribute` INT NULL  AFTER `primary_category`;
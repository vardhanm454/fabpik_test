ALTER TABLE `order_statuses` ADD `admin_text` VARCHAR(100) NULL AFTER `name`, ADD `seller_text` VARCHAR(100) NULL AFTER `admin_text`, ADD `user_text` VARCHAR(100) NULL AFTER `seller_text`, ADD `notify_email` ENUM('y','n') NULL AFTER `user_text`, ADD `notify_sms` ENUM('y','n') NULL AFTER `notify_email`;

ALTER TABLE `payment_statuses` ADD `admin_text` VARCHAR(100) NULL AFTER `name`, ADD `seller_text` VARCHAR(100) NULL AFTER `admin_text`, ADD `user_text` VARCHAR(100) NULL AFTER `seller_text`, ADD `notify_email` ENUM('y','n') NULL AFTER `user_text`, ADD `notify_sms` ENUM('y','n') NULL AFTER `notify_email`;

ALTER TABLE `shipping_statuses` ADD `admin_text` VARCHAR(100) NULL AFTER `name`, ADD `seller_text` VARCHAR(100) NULL AFTER `admin_text`, ADD `user_text` VARCHAR(100) NULL AFTER `seller_text`, ADD `notify_email` ENUM('y','n') NULL AFTER `user_text`, ADD `notify_sms` ENUM('y','n') NULL AFTER `notify_email`;

INSERT INTO `order_statuses` VALUES ('1', 'Pending/Active Orders', 'Pending From Seller', 'Pending From Seller', 'Order Placed', 'y', 'y', CURRENT_TIMESTAMP, NULL);
INSERT INTO `order_statuses` VALUES ('2', 'Cancelled', 'Cancelled', 'Cancelled', 'Cancelled', 'y', 'y', CURRENT_TIMESTAMP, NULL);
INSERT INTO `order_statuses` VALUES ('4', 'Order Confirmed', 'Confirmed by Seller', 'Confirmed by Seller', 'Order Placed', 'n', 'n', CURRENT_TIMESTAMP, NULL);
INSERT INTO `order_statuses` VALUES ('5', 'Return Requested by User', 'Return Requested', 'Return Requested', 'Return Requested', 'n', 'n', CURRENT_TIMESTAMP, NULL);
INSERT INTO `order_statuses` VALUES ('6', 'Return Approved by Admin', 'Return Approved by Admin', 'Return Approved by Admin', 'Return Requested', 'n', 'n', CURRENT_TIMESTAMP, NULL);
INSERT INTO `order_statuses` VALUES ('7', 'Seller Rejection Approved by Admin', 'Rejection Approved by Admin', 'Rejection Approved by Admin', 'Problem with the Order', 'y', 'y', CURRENT_TIMESTAMP, NULL);
INSERT INTO `order_statuses` VALUES ('8', 'Delivered', 'Delivered', 'Delivered', 'Delivered', 'y', 'y', CURRENT_TIMESTAMP, NULL);
INSERT INTO `order_statuses` VALUES ('9', 'Completed', 'Completed', 'Completed', 'Delivered', 'n', 'n', CURRENT_TIMESTAMP, NULL);
INSERT INTO `order_statuses` VALUES ('10', 'Generate AWB', 'AWB Created', 'Order Confirmed', 'Order Placed', 'n', 'n', CURRENT_TIMESTAMP, NULL);

INSERT INTO `payment_statuses` (`id`, `name`, `admin_text`, `seller_text`, `user_text`, `notify_email`, `notify_sms`, `created_at`, `updated_at`) VALUES 
(1, 'Paid', 'Paid', 'Paid', 'Paid', 'n', 'n', CURRENT_TIMESTAMP, NULL), 
(2, 'Not Paid', 'Not Paid', 'Not Paid', 'Not Paid', 'n', 'n', CURRENT_TIMESTAMP, NULL), 
(3, 'Refund Request In Review', 'Refund Request in review', 'N/A', 'Refund request in Review', 'n', 'n', CURRENT_TIMESTAMP, NULL), 
(4, 'Refund Done', 'Refund Done', 'N/A', 'Refund Done', 'n', 'n', CURRENT_TIMESTAMP, NULL);

INSERT INTO `shipping_statuses` (`id`, `name`, `admin_text`, `seller_text`, `user_text`, `notify_email`, `notify_sms`, `created_at`, `updated_at`) VALUES
(1, 'Pending', 'Pending', 'Pending', 'Under Process', 'n', 'n', CURRENT_TIMESTAMP, NULL),
(2, 'Ready For Pickup', 'Ready for Pickup', 'Ready for Pickup', 'Package Ready', 'n', 'n', CURRENT_TIMESTAMP, NULL),
(3, 'Pickup Scheduled', 'Pickup Scheduled', 'Pickup Scheduled', 'Package Ready', 'n', 'n', CURRENT_TIMESTAMP, NULL),
(4, 'Shipped', 'Shipped', 'Shipped', 'Shipped', 'y', 'y', CURRENT_TIMESTAMP, NULL),
(5, 'Delivered', 'Delivered', 'Delivered', 'Delivered', 'y', 'y', CURRENT_TIMESTAMP, NULL),
(6, 'Delivery Unsuccessful', 'Delivery Unsuccessful', 'Delivery Unsuccessful', 'Delivery Unsuccessful', 'y', 'y', CURRENT_TIMESTAMP, NULL),
(7, 'Undelivered Product Returned', 'Undelivered Product Returned', 'Undelivered Product Returned', 'Delivery Unsuccessful', 'n', 'n', CURRENT_TIMESTAMP, NULL),
(8, 'Undelivered Product Return Confirmed', 'Undelivered Product Return Confirmed', 'Undelivered Product Return Confirmed', 'Delivery Unsuccessful', 'n', 'n', CURRENT_TIMESTAMP, NULL),
(9, 'Return - Schedule Return Pickup', 'Schedule Return Pickup', 'Schedule Return Pickup', 'Delivered', 'n', 'n', CURRENT_TIMESTAMP, NULL),
(10, 'Return Pickup Initiated', 'Return Pickup Initiated', 'Return Pickup Initiated', 'Return Pickup Initiated', 'y', 'y', CURRENT_TIMESTAMP, NULL),
(11, 'Product Returned', 'Product Returned to Seller', 'Product Returned to Seller', 'Product Returned', 'n', 'n', CURRENT_TIMESTAMP, NULL),
(12, 'Product Return confirmed', 'Product Return Confirmed', 'Product Return Confirmed', 'Product Returned', 'n', 'n', CURRENT_TIMESTAMP, NULL);

ALTER TABLE `order_details` ADD `shipment_id` INT(11) NULL DEFAULT NULL AFTER `tracking_no`, ADD `awb_courier_id` INT(11) NOT NULL DEFAULT '0' AFTER `shipment_id`;

ALTER TABLE `mobile_app_sliders` CHANGE `filter_params` `filter_params` TEXT NULL DEFAULT NULL;

TRUNCATE `mobile_app_sliders`;

INSERT INTO `mobile_app_sliders` (`id`, `page`, `panel`, `title`, `image`, `filter_params`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(1, 'h', 'info', NULL, 'mobile_covid_banner.webp', NULL, 1, '2021-06-29 06:22:22', NULL, NULL, NULL, NULL),
(2, 'h', 'middle', NULL, 'slider1.webp', '{\"category_id\":[1],\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":false,\"max_discount\":false,\"coupon\":false}\r\n', 1, '2021-06-29 06:22:22', NULL, NULL, NULL, NULL),
(3, 'h', 'middle', NULL, 'slider2-mobile.webp', '{\"category_id\":false,\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":99,\"max_price\":999,\"min_discount\":false,\"max_discount\":false,\"coupon\":false}\r\n', 1, '2021-06-29 06:23:19', NULL, NULL, NULL, NULL),
(4, 'h', 'middle', NULL, 'slider3-mobile.webp', '{\"category_id\":false,\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":0,\"max_discount\":70,\"coupon\":false}\r\n', 1, '2021-06-29 06:23:19', NULL, NULL, NULL, NULL),
(5, 'h', 'middle', NULL, 'slider4-mobile.webp', '{\"category_id\":false,\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":false,\"max_discount\":false,\"coupon\":false}\r\n', 1, '2021-06-29 06:23:49', NULL, NULL, NULL, NULL),
(6, 'h', 'middle', NULL, 'slider5-mobile.webp', '{\"category_id\":false,\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":false,\"max_discount\":false,\"coupon\":false}\r\n', 1, '2021-06-29 06:23:49', NULL, NULL, NULL, NULL),
(7, 'h', 'bottom', 'Girls Clothing', 'girls.webp', '{\"category_id\":false,\"sub_category_id\":[2],\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":false,\"max_discount\":false,\"coupon\":false}\r\n', 1, '2021-06-29 06:25:14', NULL, NULL, NULL, NULL),
(8, 'h', 'bottom', 'Boys Clothing', 'boys.webp', '{\"category_id\":false,\"sub_category_id\":[1],\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":false,\"max_discount\":false,\"coupon\":false}\r\n', 1, '2021-06-29 06:25:14', NULL, NULL, NULL, NULL),
(9, 'h', 'bottom', 'Toys Clothing', 'toys.webp', '{\"category_id\":[3],\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":false,\"max_discount\":false,\"coupon\":false}\r\n', 1, '2021-06-29 06:25:31', NULL, NULL, NULL, NULL),
(10, 'c', NULL, NULL, 'clothing-1.webp', '{\"category_id\":[1],\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":0,\"max_discount\":50,\"coupon\":false}\r\n', 1, '2021-06-29 07:22:17', NULL, NULL, NULL, NULL),
(11, 'c', NULL, NULL, 'clothing-2.webp', '{\"category_id\":[1],\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":0,\"max_discount\":30,\"coupon\":false}\r\n', 1, '2021-06-29 07:22:25', NULL, NULL, NULL, NULL),
(12, 'o', NULL, NULL, 'clothing-offer.webp', '{\"category_id\":[1],\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":99,\"max_price\":false,\"min_discount\":false,\"max_discount\":false,\"coupon\":false}\r\n', 1, '2021-06-29 07:30:42', NULL, NULL, NULL, NULL),
(13, 'o', NULL, NULL, 'priceguaranted.webp', '{\"category_id\":false,\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":false,\"max_discount\":false,\"coupon\":false}\r\n', 1, '2021-06-29 07:30:42', NULL, NULL, NULL, NULL),
(14, 'o', NULL, NULL, 'personalcare-offer.webp', '{\"category_id\":[7],\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":false,\"max_discount\":false,\"coupon\":false}\r\n', 1, '2021-06-29 07:31:34', NULL, NULL, NULL, NULL),
(15, 'o', NULL, NULL, 'accessories-offer.webp', '{\"category_id\":[10],\"sub_category_id\":false,\"child_category_id\":false,\"brand\":false,\"min_price\":false,\"max_price\":false,\"min_discount\":false,\"max_discount\":false,\"coupon\":false}\r\n', 1, '2021-06-29 07:31:34', NULL, NULL, NULL, NULL);

CREATE TABLE `commission_update_logs`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `order_detail_id` INT(11) NOT NULL,
    `commission` INT(11) NULL,
    `updated_commission` INT(11) NULL,
    `commission_sgst` INT(11) NULL,
    `updated_commission_sgst` INT(11) NULL,
    `commission_cgst` INT(11) NULL,
    `updated_commission_cgst` INT(11) NULL,
    `commission_igst` INT(11) NULL,
    `updated_commission_igst` INT(11) NULL,
    `shipping_charge` INT(11) NULL,
    `updated_shipping_charge` INT(11) NULL,
    `shipping_charge_sgst` INT(11) NULL,
    `updated_shipping_charge_sgst` INT(11) NULL,
    `shipping_charge_cgst` INT(11) NULL,
    `updated_shipping_charge_cgst` INT(11) NULL,
    `shipping_charge_igst` INT(11) NULL,
    `updated_shipping_charge_igst` INT(11) NULL,
    `order_handling_charge` INT(11) NULL,
    `updated_order_handling_charge` INT(11) NULL,
    `order_handling_charge_sgst` INT(11) NULL,
    `updated_order_handling_charge_sgst` INT(11) NULL,
    `order_handling_charge_cgst` INT(11) NULL,
    `updated_order_handling_charge_cgst` INT(11) NULL,
    `order_handling_charge_igst` INT(11) NULL,
    `updated_order_handling_charge_igst` INT(11) NULL,
    `created_by` INT(11) NULL,
    `comments` VARCHAR(550) NULL,
    `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL,
    `deleted_at` TIMESTAMP NULL,
    PRIMARY KEY(`id`)
) ENGINE = InnoDB;

ALTER TABLE `commission_update_logs` ADD INDEX(`order_detail_id`);
ALTER TABLE `commission_update_logs` ADD INDEX(`created_by`);

ALTER TABLE `categories` ADD `brands` JSON NULL AFTER `secondary_attribute`;
ALTER TABLE `order_handling_charge` CHANGE `min_amount` `min_amount` FLOAT(11) NOT NULL DEFAULT '0';
ALTER TABLE `order_handling_charge` CHANGE `max_amount` `max_amount` FLOAT(11) NOT NULL DEFAULT '0';

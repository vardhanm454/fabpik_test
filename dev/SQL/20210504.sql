CREATE VIEW view_stock_report AS SELECT
    `t1`.`unique_id` AS `pv_unique_id`,
    `t2`.`unique_id` AS `p_unique_id`,
    `t1`.`name` AS `name`,
    `t1`.`sku` AS `sku`,
    `t1`.`stock` AS `stock`,
    `t1`.`min_stock_quantity` AS `min_stock`,
    `t4`.`title` AS `c_name`,
    `t5`.`name` AS `s_name`,
    `t5`.`seller_code` AS `seller_code`,
    `t4`.`id` AS `category_id`,
    `t5`.`id` AS `seller_id`,
    `t6`.`id` AS `brand_id`,
    `t6`.`name` AS `brand_name`,
    `t1`.`updated_at` AS `updated_at`
FROM
    `product_variants` AS `t1`
JOIN `products` AS `t2`
ON
    `t1`.`product_id` = `t2`.`id`
JOIN `product_categories` AS `t3`
ON
    `t3`.`product_id` = `t2`.`id`
JOIN `categories` AS `t4`
ON
    `t3`.`category_id` = `t4`.`id`
JOIN `sellers` AS `t5`
ON
    `t2`.`seller_id` = `t5`.`id`
    JOIN `brands` AS `t6`
ON
    `t2`.`brand_id` = `t6`.`id`
WHERE
    `t3`.`is_active` = 1 AND `t2`.`deleted_at` IS NULL
ORDER BY
    `t1`.`unique_id`;
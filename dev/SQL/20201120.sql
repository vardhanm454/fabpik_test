ALTER TABLE `customers`  ADD `status` TINYINT(1) NOT NULL DEFAULT '1'  AFTER `gender`;
ALTER TABLE `customers` ADD `alternative_mobile` VARCHAR(20) NOT NULL AFTER `mobile`;
ALTER TABLE `customers` ADD `dob` VARCHAR(30) NOT NULL AFTER `gender`, ADD `gst` VARCHAR(4) NOT NULL AFTER `dob`, ADD `gst_no` VARCHAR(250) NULL AFTER `gst`;
ALTER TABLE `customers` CHANGE `gst` `gst` ENUM('y','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `customers` ADD `gst_certificate` VARCHAR(200) NOT NULL AFTER `gst_no`; 

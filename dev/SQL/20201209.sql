ALTER TABLE `orders` DROP INDEX `invoice_no`;
ALTER TABLE `orders` DROP `invoice_no`;
ALTER TABLE `order_details`  ADD `invoice_no` INT NOT NULL  AFTER `order_id`,  ADD   INDEX  (`invoice_no`);

ALTER TABLE `customers` CHANGE `alternative_mobile` `alternative_mobile` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `customers` CHANGE `dob` `dob` VARCHAR(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL, CHANGE `gst_certificate` `gst_certificate` VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `order_details` ADD `commission_sgst` DOUBLE NULL AFTER `commission`, ADD `commission_cgst` DOUBLE NULL AFTER `commission_sgst`;
ALTER TABLE `order_details` CHANGE `shipping_charge_gst` `shipping_charge_sgst` DOUBLE NULL DEFAULT NULL;
ALTER TABLE `order_details` ADD `shipping_charge_cgst` DOUBLE NULL AFTER `shipping_charge_sgst`;
CREATE TABLE `coupon_groups` (
  `id` int NOT NULL,
  `group_name` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `childcategory_id` int DEFAULT NULL,
  `product_varient_ids` json DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
);

ALTER TABLE `coupon_groups`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `coupon_groups` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `product_tags` (
  `id` int NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `status` tinyint DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
);

ALTER TABLE `product_tags`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `product_tags` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `coupon_groups` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `variation_images` ADD `thumbnail_img_name` VARCHAR(256) NULL DEFAULT NULL AFTER `thumbnail`;

CREATE VIEW view_brands AS (SELECT name,id FROM `brands` WHERE deleted_at IS NULL AND status = 1)

CREATE VIEW view_product_variants AS (SELECT
    product_variants.id,
    product_variants.name,
    product_variants.slug,
    product_variants.sku,
    product_variants.mrp,
    product_variants.price,
    product_variants.fabpik_seller_price,
    product_variants.fabpik_seller_discount,
    brands.name AS brand_name,
    product_variants.unique_id,
    'product' AS 'type',
    (
        product_variants.mrp - product_variants.fabpik_seller_price
    ) / product_variants.mrp * 100 AS discount_percent,
    imgs.images,
    imgs.thumbnail,
    imgs.thumbnail_img_name,

    deals.start_time,
    deals.end_time,
    deals.deal_price,
    product_variants.stock,
    product_variants.product_id,
    products.product_tags,
    IF(
        deals.deal_price,
        (
            product_variants.mrp - deals.deal_price
        ) / product_variants.mrp * 100,
        0
    ) AS deal_discount,
    CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_price ELSE product_variants.price END AS final_price,
    CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1)  ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) END AS final_discount
FROM
    `product_variants`
INNER JOIN `products` ON `products`.`id` = `product_variants`.`product_id`
INNER JOIN `brands` ON `brands`.`id` = `products`.`brand_id`
INNER JOIN `sellers` ON `sellers`.`id` = `products`.`seller_id`
INNER JOIN `view_product_images` AS `imgs`
ON
    `imgs`.`product_variant_id` = `product_variants`.`id`
LEFT JOIN `deals` ON `deals`.`product_variant_id` = `product_variants`.`id` AND NOW() BETWEEN deals.start_time AND deals.end_time AND deals.max_orders > 0
WHERE
     sellers.status = 1 AND deals.deleted_at IS NULL AND product_variants.deleted_at IS NULL AND products.status = 1 AND products.deleted_at IS NULL AND sellers.approval_status = 1 AND sellers.deleted_at IS NULL AND brands.deleted_at IS NULL AND brands.status = 1 AND `product_variants`.`deleted_at` IS NULL
GROUP BY
    `product_variants`.`product_id`
ORDER BY
    `stock`
DESC)
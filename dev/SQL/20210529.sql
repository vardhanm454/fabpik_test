CREATE TABLE `order_address_modify_logs` (
  `id` int NOT NULL,
  `order_id` int DEFAULT NULL,
  `order_detail_id` int DEFAULT NULL,
  `old_shipping_address1` varchar(255) DEFAULT NULL,
  `old_shipping_address2` varchar(255) DEFAULT NULL,
  `old_shipping_city` varchar(50) DEFAULT NULL,
  `old_shipping_state` varchar(50) DEFAULT NULL,
  `old_shipping_state_id` int DEFAULT NULL,
  `old_shipping_pincode` varchar(50) DEFAULT NULL,
  `current_shipping_address1` varchar(255) DEFAULT NULL,
  `current_shipping_address2` varchar(255) DEFAULT NULL,
  `current_shipping_city` varchar(50) DEFAULT NULL,
  `current_shipping_state` varchar(50) DEFAULT NULL,
  `current_shipping_state_id` int DEFAULT NULL,
  `current_shipping_pincode` varchar(50) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
);

ALTER TABLE `order_address_modify_logs`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `order_address_modify_logs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_details` ADD `cancellation_reason` VARCHAR(255) NULL AFTER `manifest_url`, ADD `cancelled_by` INT NULL AFTER `cancellation_reason`, ADD `cancelled_at` TIMESTAMP NULL AFTER `cancelled_by`;

ALTER TABLE `products` ADD `fabpik_addon_discount` DOUBLE NULL DEFAULT '0' AFTER `fabpik_seller_discount_percentage`;

ALTER TABLE `product_variants` ADD `fabpik_addon_discount` DOUBLE NULL DEFAULT '0' AFTER `fabpik_seller_discount_percentage`;

CREATE TABLE `order_refund_log` (
  `id` int(11) NOT NULL,
  `child_order_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `reason` varchar(255) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `order_cancel_log` (
  `id` int(11) NOT NULL,
  `child_order_id` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
);

ALTER TABLE `order_cancel_log`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `order_cancel_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
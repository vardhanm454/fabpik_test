CREATE TABLE `fcm_tokens` (
  `id` int(11) NOT NULL,
  `fcm_token` text DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ;

ALTER TABLE `fcm_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `fcm_tokens` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
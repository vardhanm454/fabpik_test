ALTER TABLE `sellers` ADD `commission_accept` TINYINT NOT NULL DEFAULT '0' COMMENT '1=accept, 0=NotAccept' AFTER `commission_type`;

UPDATE `washing_types` SET `name` = 'Dry Clean Only' WHERE `washing_types`.`id` = 1;
UPDATE `washing_types` SET `name` = 'Regular Wash / Hand Wash' WHERE `washing_types`.`id` = 2;
INSERT INTO `washing_types` (`id`, `name`, `created_at`, `updated_at`) VALUES (NULL, 'Machine Wash', current_timestamp(), NULL), (NULL, 'Wash Dark Colors Separately', current_timestamp(), NULL);

--
-- Table structure for table `terms_conditions`
--

CREATE TABLE `terms_conditions` (
  `id` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `display_name` varchar(100) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `terms_conditions`
--
ALTER TABLE `terms_conditions`
  ADD PRIMARY KEY (`id`);

  ALTER TABLE `terms_conditions` CHANGE `id` `id` INT(10) NOT NULL AUTO_INCREMENT;
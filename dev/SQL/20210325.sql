CREATE TABLE `product_size_charts`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `product_id` INT NULL DEFAULT NULL,
    `title` VARCHAR(100) NULL DEFAULT NULL,
    `sizes` JSON NULL DEFAULT NULL,
    `columns` JSON NULL DEFAULT NULL,
    `size_chart_data` JSON NULL DEFAULT NULL,
    `notes` VARCHAR(255) NULL DEFAULT NULL,
    `images` JSON NULL DEFAULT NULL,
    `created_by` INT NULL DEFAULT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by` INT NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    `deleted_by` INT NULL DEFAULT NULL,
    `deleted_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY(`id`),
    INDEX(`product_id`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE `sizechart_image_master`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(100) NULL DEFAULT NULL,
    `images` JSON NULL DEFAULT NULL,
    `created_by` INT NULL DEFAULT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by` INT NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    `deleted_by` INT NULL DEFAULT NULL,
    `deleted_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY(`id`)
) ENGINE = InnoDB CHARSET = utf8mb4 COLLATE utf8mb4_unicode_ci;

INSERT INTO `sizechart_image_master` (`id`, `title`, `images`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES (NULL, 'T-Shirt', '[\"image1.jpeg\",\"image2.jpeg\",\"image3.jpeg\"]', NULL, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL), (NULL, 'Shirt', '[\"image1.jpeg\",\"image2.jpeg\",\"image3.jpeg\"]', NULL, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL);

ALTER TABLE `deals` ADD `publish_date` TIMESTAMP NULL DEFAULT NULL AFTER `end_time`;
ALTER TABLE `order_details` ADD `deal_price` INT NULL DEFAULT NULL AFTER `price`;

CREATE TABLE `iron_types` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `iron_types`
--

INSERT INTO `iron_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Temperature 40', '2020-11-19 05:36:39', NULL),
(2, 'Temperature 20', '2020-11-19 05:36:39', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `iron_types`
--
ALTER TABLE `iron_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `iron_types`
--
ALTER TABLE `iron_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

ALTER TABLE `coupons` ADD `max_usage` ENUM('o','m') NULL DEFAULT NULL AFTER `min_cart_amount`;

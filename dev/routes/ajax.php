<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------|
*/

Route::any('previewImage', 'AjaxController@previewImage')->name('previewImage');
Route::any('changeStatus', 'AjaxController@changeStatus')->name('changeStatus');
Route::any('changeFeaturedStatus', 'AjaxController@changeFeaturedStatus')->name('changeFeaturedStatus');
Route::any('getSubCategories', 'AjaxController@getSubCategories')->name('getSubCategories');
Route::any('getChildCategoriesList', 'AjaxController@getChildCategoriesList')->name('getChildCategoriesList');
Route::any('getCategoriesList', 'AjaxController@getCategoriesList')->name('getCategoriesList');
Route::any('getSellerSizecharts', 'AjaxController@getSellerSizecharts')->name('getSellerSizecharts');
Route::any('getChildCategories', 'AjaxController@getChildCategories')->name('getChildCategories');
Route::any('autocomplete','AjaxController@autocomplete')->name('childcategories.autocomplete');
Route::any('sellerautocomplete','AjaxController@sellerAutoComplete')->name('sellerAutoComplete');
Route::any('categoryautocomplete','AjaxController@categoryAutoComplete')->name('categoryAutoComplete');
Route::any('productvariantautocomplete','AjaxController@productVariantAutoComplete')->name('productVariantAutoComplete');
Route::any('brandsautocomplete','AjaxController@brandsAutoComplete')->name('brandsAutoComplete');
Route::get('auto-complete-address', 'AjaxController@googleAutoAddress')->name('googleAutoAddress');
Route::get('producttagautocomplete', 'AjaxController@ProductTagAutoComplete')->name('ProductTagAutoComplete');
Route::get('layoutColumnAutoComplete', 'AjaxController@layoutColumnAutoComplete')->name('layoutColumnAutoComplete');

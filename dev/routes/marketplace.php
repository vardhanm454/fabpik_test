<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
	return response()->json(['MARKETPLACE API'=>'OK']);
});

Route::get('authToken', 'Api\MarketplaceController@authToken');

Route::group(['middleware' => ['check_apikey']], function () {

	Route::get('productsCount', 'Api\MarketplaceController@productsCount');

	Route::get('products', 'Api\MarketplaceController@products');

	Route::group([
		'prefix' => 'orders',
	], function () {
		Route::get('', 'Api\MarketplaceController@orders');
		Route::get('pendency', 'Api\MarketplaceController@pendency');
		Route::post('acknowledge', 'Api\MarketplaceController@acknowledge');
		Route::get('labels', 'Api\MarketplaceController@labels');
		Route::post('labels', 'Api\MarketplaceController@postShipment');
		Route::post('fetchManifest', 'Api\MarketplaceController@fetchManifest');
		Route::post('dispatch', 'Api\MarketplaceController@orderDispatch');
		Route::post('cancel', 'Api\MarketplaceController@orderCancel');
	});

	Route::post('updateInventory', 'Api\MarketplaceController@updateInventory');

	Route::get('invoiceDetails', 'Api\MarketplaceController@invoiceDetails');

	Route::get('courierDetails', 'Api\MarketplaceController@courierDetails');

});


<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
	return response()->json(['SELLER API'=>'OK']);
});

Route::group([
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'Api\Seller\AuthController@login');
    Route::get('logout', 'Api\Seller\AuthController@logout');
    Route::post('unique_verification', 'Api\Seller\AuthController@userUniqueVerification');
    Route::post('mobile_otp', 'Api\Seller\AuthController@mobileOtp');
    Route::post('email_otp', 'Api\Seller\AuthController@emailOtp');
    Route::post('verifyEmailOtp', 'Api\Seller\AuthController@verifyEmailOtp');
    Route::post('seller_registration', 'Api\Seller\AuthController@sellerRegistration');
    Route::post('forgot_password', 'Api\Seller\AuthController@forgotPassword');
    Route::post('change_password', 'Api\Seller\AuthController@changePassword');
    Route::post('resend_email_verification', 'Api\Seller\AuthController@resendEmailVerification');
});

Route::group([
    'prefix' => 'dashboard'
], function ($router) {
    Route::get('order_details', 'Api\Seller\DashboardController@orderDetails');
    Route::get('ordersStats', 'Api\Seller\DashboardController@ordersStats');
    Route::get('settlementLogs', 'Api\Seller\DashboardController@settlementLogs');
    Route::get('settlementStats', 'Api\Seller\DashboardController@settlementStats');
});

Route::group([
    'prefix' => 'seller_verification'
], function ($router) {
    Route::get('company_types', 'Api\Seller\AccountController@companyTypes');
    Route::post('company_details_store', 'Api\Seller\AccountController@companyDetailsStore');
    Route::post('poc_details_store', 'Api\Seller\AccountController@pocDetailsStore');
    Route::post('bank_details', 'Api\Seller\AccountController@bankDetails');
    Route::get('states', 'Api\Seller\AccountController@states');
    Route::get('getDetails', 'Api\Seller\AccountController@getDetails');
    Route::get('getDetails/{id}', 'Api\Seller\AccountController@getDetails');
});

// ********************************************* //
// *****************  Account  **************** //
// ********************************************* //
Route::group([
    'prefix' => 'account',
], function () {
    Route::post('acceptCommissionDetails','Api\Seller\AccountController@acceptCommissionDetails');
    Route::get('getCommissionDetails','Api\Seller\AccountController@getCommissionDetails');
    Route::get('sellerSettings','Api\Seller\AccountController@sellerSettings');
    Route::post('changeRequest','Api\Seller\AccountController@changeRequest');
    Route::post('undoChanges/{id}','Api\Seller\AccountController@undoChanges');
    Route::post('finalSubmission','Api\Seller\AccountController@finalSubmission');
    Route::post('wareHouseDetails','Api\Seller\AccountController@wareHouseDetails');
    Route::get('changePassword', 'Api\Seller\AccountController@changePassword');

}); 

Route::group([
    'prefix' => 'orders'
], function ($router) {
    Route::get('orders_group_by_status', 'Api\Seller\OrdersController@ordersGroupByStatus');
    Route::get('get_order_by_status', 'Api\Seller\OrdersController@getOrderByStatus');
    Route::post('update_order_status', 'Api\Seller\OrdersController@updateOrderStatus');
    Route::get('getOrderDetails/{id}', 'Api\Seller\OrdersController@getOrderDetails');
});

Route::group([
    'prefix' => 'invoice'
], function ($router) {
    Route::get('user_invoice', 'Api\Seller\InvoiceController@userInvoice');
    Route::get('fabpik_invoice', 'Api\Seller\InvoiceController@fabpikInvoice');
    Route::post('user_and_fabpik_invoices', 'Api\Seller\InvoiceController@userAndFabpikInvoices');
    Route::get('download_seller_invoice/{orderid}', 'Api\Seller\InvoiceController@sellerInvoice');
    Route::get('download_customer_invoice/{orderid}', 'Api\Seller\InvoiceController@customerInvoice'); 
});

Route::group([
    'prefix' => 'settlement'
], function ($router) {
    Route::get('funds_settlement', 'Api\Seller\SettlementController@fundsSettlement');
});

Route::group([
    'prefix' => 'reviews',
], function () {
    Route::get('/','Api\Seller\OrdersController@reviews');
});
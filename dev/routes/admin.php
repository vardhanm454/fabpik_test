<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------|
*/

Route::get('/', function () {
    return (auth()->check())?redirect()->route('admin.dashboard'):redirect()->route('admin.login');
});

// Route Authentication Pages
Route::any('login', 'AuthController@login')->name('login');
// Route::any('register', 'AuthController@register')->name('register');
Route::any('forgot-password', 'PasswordController@forgotPassword')->name('forgotPassword');
Route::any('set-password', 'PasswordController@setPassword')->name('setPassword');
Route::any('sendPasswordResetToken', 'PasswordController@sendPasswordResetToken')->name('sendPasswordResetToken');
Route::get('reset-password/{token}/{email}', 'PasswordController@showPasswordResetForm')->name('reset-password');
Route::post('password-change/{token}', 'PasswordController@resetPassword')->name('password-change');

Route::group(['middleware' => ['auth_admin','role:Super Admin|Admin']], function () {

	// ********************************************* //
    // *****************  Account  **************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'account',
    ], function () {
        Route::any('profile','AccountController@profile')->name('account.profile');
        Route::any('change-password','AccountController@changePassword')->name('account.changePassword');
        Route::any('settings','AccountController@settings')->name('account.settings');
        Route::any('logout', 'AccountController@logout')->name('account.logout');
    });

    // ********************************************* //
    // ****************  Dashboard  *************** //
    // ********************************************* //
    Route::any('dashboard', 'DashboardController@index')->name('dashboard');

    // ********************************************* //
    // ******************  Brands  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'brands',
    ], function () {
        Route::get('','BrandController@index')->name('brands');
        Route::any('getAjaxListData','BrandController@getAjaxListData')->name('brands.getAjaxListData');
        Route::any('export','BrandController@export')->name('brands.export');
        Route::any('add','BrandController@add')->name('brands.add');
        Route::any('edit/{id}','BrandController@edit')->name('brands.edit');
        Route::any('delete/{id}','BrandController@delete')->name('brands.delete');
    });

    // ********************************************* //
    // ******************  Categories  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'categories',
    ], function () {
        Route::get('','CategoryController@index')->name('categories');
        Route::any('getAjaxListData','CategoryController@getAjaxListData')->name('categories.getAjaxListData');
        Route::any('export','CategoryController@export')->name('categories.export');
        Route::any('add','CategoryController@add')->name('categories.add');
        Route::any('edit/{id}','CategoryController@edit')->name('categories.edit');
        Route::any('delete/{id}','CategoryController@delete')->name('categories.delete');
        Route::any('addPriority','CategoryController@addPriority')->name('categories.addPriority');
    });

    // ********************************************* //
    // ******************  SubCategories  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'subcategories',
    ], function () {
        Route::get('','SubCategoryController@index')->name('subcategories');
        Route::any('getAjaxListData','SubCategoryController@getAjaxListData')->name('subcategories.getAjaxListData');
        Route::any('export','SubCategoryController@export')->name('subcategories.export');
        Route::any('add','SubCategoryController@add')->name('subcategories.add');
        Route::any('edit/{id}','SubCategoryController@edit')->name('subcategories.edit');
        Route::any('delete/{id}','SubCategoryController@delete')->name('subcategories.delete');
    });

    // ********************************************* //
    // *************  ChildCategories  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'childcategories',
    ], function () {
        Route::get('','ChildCategoryController@index')->name('childcategories');
        Route::any('getAjaxListData','ChildCategoryController@getAjaxListData')->name('childcategories.getAjaxListData');
        Route::any('export','ChildCategoryController@export')->name('childcategories.export');
        Route::any('getsubcategories/{id}','ChildCategoryController@getSubCategories')->name('childcategories.getSubCategories');
        Route::any('add','ChildCategoryController@add')->name('childcategories.add');
        Route::any('edit/{id}','ChildCategoryController@edit')->name('childcategories.edit');
        Route::any('delete/{id}','ChildCategoryController@delete')->name('childcategories.delete');
        Route::any('autocomplete','ChildCategoryController@autocomplete')->name('childcategories.autocomplete');
    });

    // ********************************************* //
    // ******************  Attributes  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'attributes',
    ], function () {
        Route::get('','AttributeController@index')->name('attributes');
        Route::any('getAjaxListData','AttributeController@getAjaxListData')->name('attributes.getAjaxListData');
        Route::any('export','AttributeController@export')->name('attributes.export');
        Route::any('add','AttributeController@add')->name('attributes.add');
        Route::any('edit/{id}','AttributeController@edit')->name('attributes.edit');
        Route::any('delete/{id}','AttributeController@delete')->name('attributes.delete');
        Route::any('values/{id}','AttributeController@values')->name('attributes.values');
        Route::any('values/{id}/add','AttributeController@valueAdd')->name('attributes.valueAdd');
        Route::any('values/{id}/edit/{vid}','AttributeController@valueEdit')->name('attributes.valueEdit');
        Route::any('values/delete/{vid}','AttributeController@valueDelete')->name('attributes.valueDelete');
    });

    // ********************************************* //
    // ******************  Size Charts  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'sizecharts',
    ], function () {
        Route::get('','SizeChartController@index')->name('sizecharts');
        Route::any('getAjaxListData','SizeChartController@getAjaxListData')->name('sizecharts.getAjaxListData');
        Route::any('export','SizeChartController@export')->name('sizecharts.export');
        Route::any('add','SizeChartController@add')->name('sizecharts.add');
        Route::any('edit/{id}','SizeChartController@edit')->name('sizecharts.edit');
        Route::any('delete/{id}','SizeChartController@delete')->name('sizecharts.delete');
    });


    // ********************************************* //
    // ******************  Style Layout  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'stylelayout',
    ], function () {
        Route::get('','StyleLayoutController@index')->name('stylelayout');
        Route::any('getAjaxListData','StyleLayoutController@getAjaxListData')->name('stylelayout.getAjaxListData');
        Route::any('export','StyleLayoutController@export')->name('stylelayout.export');
        Route::any('add','StyleLayoutController@add')->name('stylelayout.add');
        Route::any('edit/{id}','StyleLayoutController@edit')->name('stylelayout.edit');
        Route::any('delete/{id}','StyleLayoutController@delete')->name('stylelayout.delete');
    });

    // ********************************************* //
    // ****************** Style Layout Columns  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'layoutcolumns',
    ], function () {
        Route::get('','LayoutColumnsController@index')->name('layoutcolumns');
        Route::any('getAjaxListData','LayoutColumnsController@getAjaxListData')->name('layoutcolumns.getAjaxListData');
        Route::any('export','LayoutColumnsController@export')->name('layoutcolumns.export');
        Route::any('add','LayoutColumnsController@add')->name('layoutcolumns.add');
        Route::any('edit/{id}','LayoutColumnsController@edit')->name('layoutcolumns.edit');
        Route::any('history/{id}','LayoutColumnsController@history')->name('layoutcolumns.history');
        Route::any('delete/{id}','LayoutColumnsController@delete')->name('layoutcolumns.delete');
    });

    // ********************************************* //
    // ***********  Products & Variants  *********** //
    // ********************************************* //
    Route::group([
        'prefix' => 'products',
    ], function () {
        Route::get('','ProductController@index')->name('products')->middleware(['permission:ADM_PRD_V_PRODUCT']);
        Route::any('getAjaxListData','ProductController@getAjaxListData')->name('products.getAjaxListData');
        Route::any('export','ProductController@export')->name('products.export')->middleware(['permission:ADM_PRD_E_PRODUCT']);
        Route::any('fullexport','ProductController@fullExport')->name('products.fullexport')->middleware(['permission:ADM_PRD_E_PRODUCT']);
        Route::get('import','ProductController@import')->name('products.import')->middleware(['permission:ADM_PRD_C_PRODUCT']);
        Route::post('processImport','ProductController@processImport')->name('products.processImport');
        Route::get('update','ProductController@update')->name('products.update')->middleware(['permission:ADM_PRD_U_PRODUCT']);
        Route::post('processProductUpdate','ProductController@processProductUpdate')->name('products.processProductUpdate');
        Route::any('add','ProductController@add')->name('products.add')->middleware(['permission:ADM_PRD_C_PRODUCT']);
        Route::any('edit/{id}','ProductController@edit')->name('products.edit')->middleware(['permission:ADM_PRD_U_PRODUCT']);
        Route::any('delete/{id}','ProductController@delete')->name('products.delete')->middleware(['permission:ADM_PRD_D_PRODUCT']);
        Route::any('variantImages/{id}','ProductController@variantImages')->name('products.variantImages')->middleware(['permission:ADM_PRD_C_PRODUCT|ADM_PRD_U_PRODUCT']);
        Route::any('variants/{id}','ProductController@variants')->name('products.variants')->middleware(['permission:ADM_PRD_V_PRODUCT']);
        Route::any('getAjaxListVarientsData/{productId}','ProductController@getAjaxListVarientsData')->name('products.getAjaxListVarientsData');
        Route::any('variants/{id}/add','ProductController@variantAdd')->name('products.variantAdd')->middleware(['permission:ADM_PRD_C_PRODUCT']);
        Route::any('variants/{id}/edit/{vid}','ProductController@variantEdit')->name('products.variantEdit')->middleware(['permission:ADM_PRD_U_PRODUCT']);
        Route::any('variants/delete/{vid}','ProductController@variantDelete')->name('products.variantDelete')->middleware(['permission:ADM_PRD_D_PRODUCT']);
        Route::any('exportCategoriesList','ProductController@exportCategoriesList')->name('products.exportCategoriesList')->middleware(['permission:ADM_PRD_E_PRODUCT']);

        Route::any('emailCheck','ProductController@emailCheck')->name('products.emailCheck');

    });

    // ********************************************* //
    // ***********  Special Prices  *********** //
    // ********************************************* //

    Route::group([
        'prefix' => 'specialprices',
    ],function(){
        Route::any('','SpecialPriceController@index')->name('specialprices');
        Route::any('getAjaxSpecialPriceListData','SpecialPriceController@getAjaxSpecialPriceListData')->name('specialprices.getAjaxSpecialPriceListData');
    });

    // ********************************************* //
    // ***********  Product Tags  *********** //
    // ********************************************* //

    Route::group([
        'prefix' => 'producttags',
    ],function(){
        Route::any('','ProductTagsController@index')->name('producttags');
        Route::any('getAjaxListData','ProductTagsController@getAjaxListData')->name('producttags.getAjaxListData');
        Route::any('add','ProductTagsController@add')->name('producttags.add');
        Route::any('edit/{id}','ProductTagsController@edit')->name('producttags.edit');
        Route::any('history/{id}','ProductTagsController@history')->name('producttags.history');
        Route::any('delete/{id}','ProductTagsController@delete')->name('producttags.delete');
    });

    // ********************************************* //
    // ***********  Reviews  *********** //
    // ********************************************* //

    Route::group([
        'prefix' => 'reviews',
    ], function () {
        Route::get('','ReviewsController@index')->name('reviews');
        Route::any('getAjaxListData','ReviewsController@getAjaxListData')->name('reviews.getAjaxListData');
    });

    // ********************************************* //
    // ***********  Image Import  *********** //
    // ********************************************* //
    Route::group([
        'prefix' => 'image',
    ], function () {
        Route::get('','ImageImportController@index')->name('image');
        Route::post('imageStore','ImageImportController@imageStore')->name('image.imageStore');
        Route::post('removeImage','ImageImportController@removeImage')->name('image.removeImage');
        Route::post('sizeChartImageStore','ImageImportController@sizeChartImageStore')->name('image.sizeChartImageStore');
        Route::post('sizeChartRemoveImage','ImageImportController@sizeChartRemoveImage')->name('image.sizeChartRemoveImage');
    });

    // ********************************************* //
    // *************  Coupons  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'coupons',
    ], function () {
        Route::get('','CouponController@index')->name('coupons');
        Route::any('getAjaxListData','CouponController@getAjaxListData')->name('coupons.getAjaxListData');
        Route::any('export','CouponController@export')->name('coupons.export');
        Route::any('add','CouponController@add')->name('coupons.add');
        Route::any('edit/{id}','CouponController@edit')->name('coupons.edit');
        Route::any('delete/{id}','CouponController@delete')->name('coupons.delete');
    });

    // ********************************************* //
    // *************  Notifications  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'notifications',
    ], function () {
        Route::get('','NotificationsController@index')->name('notifications');
        Route::any('getAjaxListData','NotificationsController@getAjaxListData')->name('notifications.getAjaxListData');
        Route::any('edit/{id}','NotificationsController@edit')->name('notifications.edit');
        Route::any('delete/{id}','NotificationsController@delete')->name('notifications.delete');
        Route::any('sendNotification', 'NotificationsController@sendNotification')->name('notifications.sendNotification');
    });

    // ********************************************* //
    // *************  Newsletter  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'newsletter',
    ], function () {
        Route::get('','NewsletterController@index')->name('newsletter');
        Route::any('getAjaxListData','NewsletterController@getAjaxListData')->name('newsletter.getAjaxListData');
        Route::any('export','NewsletterController@export')->name('newsletter.export');
        Route::any('add','NewsletterController@add')->name('newsletter.add');
        Route::any('edit/{id}','NewsletterController@edit')->name('newsletter.edit');
        Route::any('delete/{id}','NewsletterController@delete')->name('newsletter.delete');
    });

    // ********************************************* //
    // *************  Coupon Groups  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'coupongroups',
    ], function () {
        Route::get('','CouponGroupController@index')->name('coupongroups');
        Route::any('getAjaxListData','CouponGroupController@getAjaxListData')->name('coupongroups.getAjaxListData');
        Route::any('getProductVariantListData','CouponGroupController@getProductVariantListData')->name('coupongroups.getProductVariantListData');
        Route::any('export','CouponGroupController@export')->name('coupongroups.export');
        Route::any('addGroupName','CouponGroupController@add')->name('coupongroups.addGroupName');
        Route::any('selectVariants/{id}','CouponGroupController@selectVariants')->name('coupongroups.selectVariants');
        Route::any('getAjaxCouponGroupProductVariantListData/{id}','CouponGroupController@getAjaxCouponGroupProductVariantListData')->name('coupongroups.getAjaxCouponGroupProductVariantListData');

        Route::any('finalVariants/{id}','CouponGroupController@finalVariants')->name('coupongroups.finalVariants');
        Route::any('getAjaxCouponGroupProductVariantFinalListData/{id}','CouponGroupController@getAjaxCouponGroupProductVariantFinalListData')->name('coupongroups.getAjaxCouponGroupProductVariantFinalListData');
        Route::any('storeFinalGroup','CouponGroupController@storeFinalGroup')->name('coupongroups.storeFinalGroup');

        Route::any('storeGroup','CouponGroupController@storeGroup')->name('coupongroups.storeGroup');
        Route::any('edit/{id}','CouponGroupController@edit')->name('coupongroups.edit');
        Route::any('delete/{id}','CouponGroupController@delete')->name('coupongroups.delete');
    });

    // ********************************************* //
    // *************  Deal of the day  ************* //
    // ********************************************* //

    Route::group([
        'prefix' => 'deals',
    ], function () {
        Route::get('','DealsController@index')->name('deals');
        Route::any('getAjaxListData','DealsController@getAjaxListData')->name('deals.getAjaxListData');
        Route::any('export','DealsController@export')->name('deals.export');
        Route::any('add','DealsController@add')->name('deals.add');
        Route::any('edit/{id}','DealsController@edit')->name('deals.edit');
        Route::any('delete/{id}','DealsController@delete')->name('deals.delete');
    });

    // ********************************************* //
    // *************  Orders  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'orders',
    ], function () {
        Route::get('','OrderController@index')->name('orders');
        Route::post('getAjaxListData','OrderController@getAjaxListData')->name('orders.getAjaxListData');
        Route::get('view/{id}','OrderController@view')->name('orders.view');
        Route::get('invoice/{id}','OrderController@invoice')->name('orders.invoice'); // id = parent order id
        Route::get('cust-invoice/{id}','OrderController@custInvoice')->name('orders.custInvoice'); // id = parent order id Customer Invoice
    });

    // ********************************************* //
    // *************  Parent Orders  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'parentorders',
    ], function () {
        Route::get('','ParentOrderController@index')->name('parentorders');
        Route::post('getAjaxListData','ParentOrderController@getAjaxListData')->name('parentorders.getAjaxListData');
        Route::get('view/{id}','ParentOrderController@view')->name('parentorders.view');
        Route::get('invoice/{id}','ParentOrderController@invoice')->name('parentorders.invoice'); // id = parent order id
        Route::get('cust-invoice/{id}','ParentOrderController@custInvoice')->name('parentorders.custInvoice'); // id = parent order id Customer Invoice
    });

    // ********************************************* //
    // *************  Child Orders  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'childorders',
    ], function () {
        Route::get('','ChildOrderController@index')->name('childorders');
        Route::post('getAjaxListData','ChildOrderController@getAjaxListData')->name('childorders.getAjaxListData');
        Route::get('view/{id}','ChildOrderController@view')->name('childorders.view');
        Route::get('invoice/{id}','ChildOrderController@invoice')->name('childorders.invoice'); // id = parent order id
        Route::get('cust-invoice/{id}','ChildOrderController@custInvoice')->name('childorders.custInvoice'); // id = parent order id Customer Invoice
        Route::get('downloadCustomerInvoice/{id}','ChildOrderController@downloadCustomerInvoice')->name('childorders.downloadCustomerInvoice');
        Route::any('sendVerificationCodes','ChildOrderController@sendVerificationCodes')->name('childorders.sendVerificationCodes');
        Route::any('updateShippingAddress/{id}','ChildOrderController@UpdateShippingAddress')->name('childorders.updateShippingAddress');
        Route::any('cancelOrder/{id}','ChildOrderController@cancelOrder')->name('childorders.cancelOrder');
        Route::any('refundInitiate/{id}','ChildOrderController@refundInitiate')->name('childorders.refundInitiate');
        Route::any('modifyCommission/{id}','ChildOrderController@modifyCommission')->name('childorders.modifyCommission');
    });

    // ********************************************* //
    // *************  Self Shipping Orders  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'selfshipping',
    ], function () {
        Route::get('','SelfShippingOrderController@index')->name('selfshipping');
        Route::post('getAjaxListData','SelfShippingOrderController@getAjaxListData')->name('selfshipping.getAjaxListData');

    });


    // ********************************************* //
    // *************  Stock Report  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'stockreport',
    ], function () {
        Route::get('sellerproductstock','StockReportController@sellerproductstock')->name('stockreport.sellerproductstock');
        Route::any('sellerproductstockexport','StockReportController@sellerproductstockexport')->name('stockreport.sellerproductstockexport');
        Route::post('getSellerProductStockAjaxListData','StockReportController@getSellerProductStockAjaxListData')->name('stockreport.getSellerProductStockAjaxListData');
        Route::get('lowstock','StockReportController@lowstock')->name('stockreport.lowstock');
        Route::any('lowstockexport','StockReportController@lowstockexport')->name('stockreport.lowstockexport');
        Route::post('getLowStockReportAjaxListData','StockReportController@getLowStockReportAjaxListData')->name('stockreport.getLowStockReportAjaxListData');
        Route::get('modifiedstock','StockReportController@modifiedstock')->name('stockreport.modifiedstock');
        Route::any('modifiedstockexport','StockReportController@modifiedstockexport')->name('stockreport.modifiedstockexport');
        Route::post('getModifiedStockReportAjaxListData','StockReportController@getModifiedStockReportAjaxListData')->name('stockreport.getModifiedStockReportAjaxListData');
        Route::get('expiredspecialprices','StockReportController@expiredspecialprices')->name('stockreport.expiredspecialprices');
        Route::post('getExpiredSpecialPricesAjaxListData','StockReportController@getExpiredSpecialPricesAjaxListData')->name('stockreport.getExpiredSpecialPricesAjaxListData');
        Route::any('expiredspecialpricesexport','StockReportController@expiredspecialpricesexport')->name('stockreport.expiredspecialpricesexport');

    });

    // ********************************************* //
    // **********  Payment & Commission  *********** //
    // ********************************************* //
    Route::group([
        'prefix' => 'payments',
    ], function () {
        Route::get('','PaymentController@index')->name('payments');
        Route::post('getAjaxListData','PaymentController@getAjaxListData')->name('payments.getAjaxListData');
        Route::get('view/{id}','PaymentController@view')->name('payments.view');
        Route::get('seler-invoice/{id}','PaymentController@sellerInvoice')->name('payments.sellerInvoice'); // id = Child order id Seller Invoice
        Route::get('downloadSellerInvoice/{id}','PaymentController@downloadSellerInvoice')->name('payments.downloadSellerInvoice');
    });

    // ********************************************* //
    // **********  Shipping Status  *********** //
    // ********************************************* //
    Route::group([
        'prefix' => 'shipping',
    ], function () {
        Route::get('','ShippingController@index')->name('shipping');
        Route::post('getAjaxListData','ShippingController@getAjaxListData')->name('shipping.getAjaxListData');
        Route::any('modifyAWBNumber/{id}','ShippingController@modifyAWBNumber')->name('shipping.modifyAWBNumber');
    });

    // ********************************************* //
    // ******************  Tickets  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'tickets',
    ], function () {
        Route::get('','TicketController@index')->name('tickets');
        Route::any('getAjaxListData','TicketController@getAjaxListData')->name('tickets.getAjaxListData');
        Route::get('view/{id}','TicketController@view')->name('tickets.view');
        Route::post('postComment/{id}','TicketController@postComment')->name('tickets.postComment');
        Route::any('closeTicket/{id}','TicketController@closeTicket')->name('tickets.closeTicket');
    });

    // ********************************************* //
    // ******************  Sellers  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'sellers',
    ], function () {
        Route::get('','SellerController@index')->name('sellers');
        Route::any('getAjaxListData','SellerController@getAjaxListData')->name('sellers.getAjaxListData');
        Route::any('add','SellerController@add')->name('sellers.add');
        Route::any('edit/{id}','SellerController@edit')->name('sellers.edit');
        Route::any('delete/{id}','SellerController@delete')->name('sellers.delete');
        Route::get('view/{id}','SellerController@view')->name('sellers.view');
        Route::any('verify/{id}','SellerController@verify')->name('sellers.verify');
        Route::post('admin-seller-basic-details','SellerController@basicDetails')->name('sellers.basicDetails');
        Route::post('admin-seller-poc-details','SellerController@pocDetails')->name('sellers.pocDetails');
        Route::post('admin-seller-bank-details','SellerController@bankDetails')->name('sellers.bankDetails');
        Route::post('admin-seller-company-details','SellerController@companyDetails')->name('sellers.companyDetails');
        Route::post('admin-seller-warehouse-details','SellerController@warehouseDetails')->name('sellers.warehouseDetails');
        Route::post('admin-seller-get-warehouse-details','SellerController@getWarehouseDetails')->name('sellers.getWarehouseDetails');
        Route::post('admin-seller-delete-warehouse-details','SellerController@deleteWarehouseDetails')->name('sellers.deleteWarehouseDetails');
        Route::post('admin-seller-approval','SellerController@approveSeller')->name('sellers.approveSeller');
        Route::post('admin-seller-reject','SellerController@rejectSeller')->name('sellers.rejectSeller');
        Route::post('register-seller','SellerController@registerSeller')->name('sellers.registerSeller');
        //Send OTP MSG91
        Route::post('admin-selers-send-otp', 'SellerController@processMobileVerification')->name('sellers.send-otp');
        Route::post('admin-seller-commission-details','SellerController@commissionDetails')->name('sellers.commissionDetails');
        Route::post('admin-seller-cod-change-details','SellerController@changeCodDetails')->name('sellers.changeCodDetails');
        //Get Pickup-Addresses
        Route::get('get-all-pickup-locations', 'SellerController@getAllPickupLocations')->name('sellers.getAllPickupLocations');
    });

    // ********************************************* //
    // ******************  Important Documents  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'importantdocs',
    ], function () {
        Route::get('','ImportantDocsController@index')->name('importantdocs');
        Route::any('getAjaxListData','ImportantDocsController@getAjaxListData')->name('importantdocs.getAjaxListData');
        Route::any('add','ImportantDocsController@add')->name('importantdocs.add');
        Route::any('edit/{id}','ImportantDocsController@edit')->name('importantdocs.edit');
        Route::any('view/{id}','ImportantDocsController@view')->name('importantdocs.view');
    });


    // ********************************************* //
    // ******************  Approvals  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'approvals',
    ], function () {
        Route::get('','ApprovalController@index')->name('approvals');
        Route::any('getAjaxListData','ApprovalController@getAjaxListData')->name('approvals.getAjaxListData');
        Route::any('acceptRequest/{id}','ApprovalController@acceptRequest')->name('approvals.acceptRequest');
        Route::any('rejectRequest/{id}','ApprovalController@rejectRequest')->name('approvals.rejectRequest');
    });

    // ********************************************* //
    // ******************  Customers  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'customers',
    ], function () {
        Route::get('','CustomerController@index')->name('customers');
        Route::any('getAjaxListData','CustomerController@getAjaxListData')->name('customers.getAjaxListData');
        Route::any('export','CustomerController@export')->name('customers.export');
        Route::any('add','CustomerController@add')->name('customers.add');
        Route::any('verifyOtp','CustomerController@verifyOtp')->name('customers.verifyOtp');
        Route::any('edit/{id}','CustomerController@edit')->name('customers.edit');
        Route::any('delete/{id}','CustomerController@delete')->name('customers.delete');
    });

    // ********************************************* //
    // ******************  Shipping Charge  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'shippingcharges',
    ], function () {
        Route::get('','ShippingChargeController@index')->name('shippingcharges');
        Route::any('getAjaxListData','ShippingChargeController@getAjaxListData')->name('shippingcharges.getAjaxListData');
        Route::post('add','ShippingChargeController@add')->name('shippingcharges.add');
        Route::any('edit/{id}','ShippingChargeController@edit')->name('shippingcharges.edit');
        Route::any('delete/{id}','ShippingChargeController@delete')->name('shippingcharges.delete');
    });

    // ********************************************* //
    // ******************  Handeling Charge  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'handelingcharges',
    ], function () {
        Route::get('','HandelingChargeController@index')->name('handelingcharges');
        Route::any('getAjaxListData','HandelingChargeController@getAjaxListData')->name('handelingcharges.getAjaxListData');
        Route::post('add','HandelingChargeController@add')->name('handelingcharges.add');
        Route::any('edit/{id}','HandelingChargeController@edit')->name('handelingcharges.edit');
        Route::any('delete/{id}','HandelingChargeController@delete')->name('handelingcharges.delete');
    });

    // ********************************************* //
    // ******************  User Management  ***************** //
    // ********************************************* //
    Route::group([
    'prefix' => 'staff',
    ], function () {
        Route::get('','StaffController@index')->name('staff');
        Route::any('getAjaxListData','StaffController@getAjaxListData')->name('staff.getAjaxListData');
        Route::any('create','StaffController@create')->name('staff.create');
        Route::any('edit/{id}','StaffController@edit')->name('staff.edit');
        Route::any('delete/{id}','StaffController@delete')->name('staff.delete');
        Route::any('export','StaffController@export')->name('staff.export');
    });

    // ********************************************* //
    // ******************  Roles  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'roles',
        ], function () {
            Route::get('','RoleController@index')->name('roles');
            Route::any('getAjaxListData','RoleController@getAjaxListData')->name('roles.getAjaxListData');
            Route::any('create','RoleController@create')->name('roles.create');
            Route::any('edit/{id}','RoleController@edit')->name('roles.edit');
            Route::any('delete/{id}','RoleController@delete')->name('roles.delete');
            Route::any('export','RoleController@export')->name('roles.export');
        });

    // ********************************************* //
    // ******************  Roles  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'permissions',
        ], function () {
            Route::get('','PermissionController@index')->name('permissions');
            Route::any('getAjaxListData','PermissionController@getAjaxListData')->name('permissions.getAjaxListData');
        });

    // ********************************************* //
    // ******************  Slider Management  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'slidermgmt',
    ], function () {
        Route::get('','SliderController@index')->name('slidermgmt');
        Route::any('getAjaxListData','SliderController@getAjaxListData')->name('slidermgmt.getAjaxListData');
        Route::any('create','SliderController@create')->name('slidermgmt.create');
        Route::any('edit/{id}','SliderController@edit')->name('slidermgmt.edit');
        Route::any('delete/{id}','SliderController@delete')->name('slidermgmt.delete');
        Route::any('export','SliderController@export')->name('slidermgmt.export');
    });
    // ********************************************* //
    // ***********  Mobile App  *********** //
    // ********************************************* //

    Route::group([
        'prefix' => 'mobile',
    ],function(){

        Route::any('','MobileController@index')->name('home');
        Route::any('category','MobileController@category')->name('category');
        Route::any('offers','MobileController@offers')->name('offers');

        ## COMMON FILES FOR EVERY BANNER
        Route::any('dynamicData','MobileController@dynamicData')->name('dynamicData');
        Route::any('editBanner','MobileController@editBanner')->name('editBanner');
        Route::any('updateBanner','MobileController@updateBanner')->name('updateBanner');
        Route::any('deleteBanner','MobileController@deleteBanner')->name('deleteBanner');
        ## COMMON FILES FOR EVERY BANNER

    });

    // ********************************************* //
    // ***********  Tax  *********** //
    // ********************************************* //

    Route::group([
        'prefix' => 'tax',
    ],function(){

        Route::any('','TaxController@index')->name('tax');
        Route::any('getAjaxListData','TaxController@getAjaxListData')->name('tax.getTaxAjaxListData');
        Route::any('taxcrud','TaxController@taxcrud')->name('tax.taxcrud');
        Route::any('taxexport','TaxController@taxexport')->name('tax.taxexport');


    });




    // ********************************************* //
    // *************  Commission Models  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'commissionmodels',
    ], function () {
        Route::get('','CommissionModelController@index')->name('commissionmodels');
        Route::any('getAjaxListData','CommissionModelController@getAjaxListData')->name('commissionmodels.getAjaxListData');
        Route::any('export','CommissionModelController@export')->name('commissionmodels.export');
        Route::any('add','CommissionModelController@add')->name('commissionmodels.add');
        Route::any('edit/{id}','CommissionModelController@edit')->name('commissionmodels.edit');
        Route::any('delete/{id}','CommissionModelController@delete')->name('commissionmodels.delete');
    });
});

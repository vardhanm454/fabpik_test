<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Seller Routes
|--------------------------------------------------------------------------|
*/

Route::get('/', function () {
    return (auth()->check())?redirect()->route('seller.dashboard'):redirect()->route('seller.login');
});

// Route Authentication Pages
Route::any('register', 'AuthController@register')->name('register');
Route::any('login', 'AuthController@login')->name('login');
Route::any('forgot-password', 'PasswordController@forgotPassword')->name('forgotPassword');
Route::any('set-password', 'PasswordController@setPassword')->name('setPassword');
Route::any('sendPasswordResetToken', 'PasswordController@sendPasswordResetToken')->name('sendPasswordResetToken');
Route::get('reset-password/{token}/{email}', 'PasswordController@showPasswordResetForm')->name('reset-password');
Route::post('password-change/{token}', 'PasswordController@resetPassword')->name('password-change');
//Send OTP MSG91
Route::post('/send-otp', 'AuthController@processMobileVerification')->name('send-otp');
Route::post('/resend-otp', 'AuthController@resendOtp')->name('resend-otp');


// ********************************************* //
// ****************  Pricing Calculator  *************** //
// ********************************************* //
Route::any('sell-online/pricing', 'PriceCalculatorController@index')->name('sell-online/pricing');

Route::group(['middleware' => ['auth_seller', 'role:Seller']], function () {

	// ********************************************* //
    // *****************  Account  **************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'account',
    ], function () {
        Route::any('profile','AccountController@profile')->name('account.profile');
        Route::any('change-password','AccountController@changePassword')->name('account.changePassword');
        Route::any('settings','AccountController@settings')->name('account.settings');
        Route::any('logout', 'AccountController@logout')->name('account.logout');
        Route::post('seller-basic-details','AccountController@basicDetails')->name('account.basicDetails');
        Route::post('seller-poc-details','AccountController@pocDetails')->name('account.pocDetails');
        Route::post('seller-bank-details','AccountController@bankDetails')->name('account.bankDetails');
        Route::post('seller-company-details','AccountController@companyDetails')->name('account.companyDetails');
        Route::post('seller-warehouse-details','AccountController@warehouseDetails')->name('account.warehouseDetails');
        Route::post('seller-get-warehouse-details','AccountController@getWarehouseDetails')->name('account.getWarehouseDetails');
        Route::post('seller-delete-warehouse-details','AccountController@deleteWarehouseDetails')->name('account.deleteWarehouseDetails');
        Route::post('accept-commission-details','AccountController@acceptCommissionDetails')->name('account.acceptCommissionDetails');
        Route::get('get-commission-details','AccountController@getCommissionDetails')->name('account.getCommissionDetails');
        Route::post('seller-cod-change-details','AccountController@changeCodDetails')->name('account.changeCodDetails');
        Route::post('seller-shipping-change-details','AccountController@changeShippingDetails')->name('account.changeShippingDetails');
        Route::post('finalSubmission','AccountController@finalSubmission')->name('account.finalSubmission');
        Route::get('notifications','AccountController@notifications')->name('account.notifications');
    });

    // ********************************************* //
    // ****************  Dashboard  *************** //
    // ********************************************* //
    Route::any('dashboard', 'DashboardController@index')->name('dashboard');
    Route::post('save-notification-token', 'DashboardController@saveToken')->name('saveNotificationToken');

    // ********************************************* //
    // ******************  Brands  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'brands',
    ], function () {
        Route::get('','BrandController@index')->name('brands');
        Route::any('getAjaxListData','BrandController@getAjaxListData')->name('brands.getAjaxListData');
        Route::any('export','BrandController@export')->name('brands.export');
    });

    // ********************************************* //
    // ******************  Categories  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'categories',
    ], function () {
        Route::get('','CategoryController@index')->name('categories');
        Route::any('getAjaxListData','CategoryController@getAjaxListData')->name('categories.getAjaxListData');
        Route::any('export','CategoryController@export')->name('categories.export');
    });

    // ********************************************* //
    // ******************  SubCategories  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'subcategories',
    ], function () {
        Route::get('','SubCategoryController@index')->name('subcategories');
        Route::any('getAjaxListData','SubCategoryController@getAjaxListData')->name('subcategories.getAjaxListData');
        Route::any('export','SubCategoryController@export')->name('subcategories.export');
    });

    // ********************************************* //
    // ***********  Reviews  *********** //
    // ********************************************* //

    Route::group([
        'prefix' => 'reviews',
    ], function () {
        Route::get('','ReviewsController@index')->name('reviews');
        Route::any('getAjaxListData','ReviewsController@getAjaxListData')->name('reviews.getAjaxListData');
    });

    Route::group([
        'prefix' => 'notifications',
    ], function () {


    });


    // ********************************************* //
    // *************  ChildCategories  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'childcategories',
    ], function () {
        Route::get('','ChildCategoryController@index')->name('childcategories');
        Route::any('getAjaxListData','ChildCategoryController@getAjaxListData')->name('childcategories.getAjaxListData');
        Route::any('export','ChildCategoryController@export')->name('childcategories.export');
        Route::any('autocomplete','ChildCategoryController@autocomplete')->name('childcategories.autocomplete');
    });

    // ********************************************* //
    // ******************  Attributes  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'attributes',
    ], function () {
        Route::get('','AttributeController@index')->name('attributes');
        Route::any('getAjaxListData','AttributeController@getAjaxListData')->name('attributes.getAjaxListData');
        Route::any('export','AttributeController@export')->name('attributes.export');
        Route::any('values/{id}','AttributeController@values')->name('attributes.values');
    });

    // ********************************************* //
    // ******************  Size Charts  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'sizecharts',
    ], function () {
        Route::get('','SizeChartController@index')->name('sizecharts');
        Route::any('getAjaxListData','SizeChartController@getAjaxListData')->name('sizecharts.getAjaxListData');
        Route::any('export','SizeChartController@export')->name('sizecharts.export');
        Route::any('add','SizeChartController@add')->name('sizecharts.add');
        Route::any('edit/{id}','SizeChartController@edit')->name('sizecharts.edit');
        Route::any('delete/{id}','SizeChartController@delete')->name('sizecharts.delete');
    });

    // ********************************************* //
    // ***********  Products & Variants  *********** //
    // ********************************************* //
    Route::group([
        'prefix' => 'products',
    ], function () {
        Route::get('','ProductController@index')->name('products');
        Route::any('getAjaxListData','ProductController@getAjaxListData')->name('products.getAjaxListData');
        Route::any('export','ProductController@export')->name('products.export');
        Route::any('fullexport','ProductController@fullExport')->name('products.fullexport');
        Route::get('import','ProductController@import')->name('products.import');
        Route::post('processImport','ProductController@processImport')->name('products.processImport');
        Route::get('update','ProductController@update')->name('products.update');
        Route::post('processProductUpdate','ProductController@processProductUpdate')->name('products.processProductUpdate');
        Route::any('add','ProductController@add')->name('products.add');
        Route::any('edit/{id}','ProductController@edit')->name('products.edit');
        Route::any('delete/{id}','ProductController@delete')->name('products.delete');
        Route::any('variantImages/{id}','ProductController@variantImages')->name('products.variantImages');
        Route::any('variants/{id}','ProductController@variants')->name('products.variants');
        Route::any('getAjaxListVarientsData/{productId}','ProductController@getAjaxListVarientsData')->name('products.getAjaxListVarientsData');
        Route::any('variants/{id}/add','ProductController@variantAdd')->name('products.variantAdd');
        Route::any('variants/{id}/edit/{vid}','ProductController@variantEdit')->name('products.variantEdit');
        Route::any('variants/delete/{vid}','ProductController@variantDelete')->name('products.variantDelete');
        Route::any('exportCategoriesList','ProductController@exportCategoriesList')->name('products.exportCategoriesList');
    });

    // ********************************************* //
    // ***********  Special Prices  *********** //
    // ********************************************* //

    Route::group([
        'prefix' => 'changeprices',
    ],function(){
        Route::any('','ChangePriceController@index')->name('changeprices');
        Route::any('getAjaxChangePriceListData','ChangePriceController@getAjaxChangePriceListData')->name('changeprices.getAjaxChangePriceListData');
    });


    // ********************************************* //
    // ***********  Image Import  *********** //
    // ********************************************* //
    Route::group([
        'prefix' => 'image',
    ], function () {
        Route::get('','ImageImportController@index')->name('image');
        Route::post('imageStore','ImageImportController@imageStore')->name('image.imageStore');
        Route::post('removeImage','ImageImportController@removeImage')->name('image.removeImage');
        Route::post('sizeChartImageStore','ImageImportController@sizeChartImageStore')->name('image.sizeChartImageStore');
        Route::post('sizeChartRemoveImage','ImageImportController@sizeChartRemoveImage')->name('image.sizeChartRemoveImage');
    });

    // ********************************************* //
    // *************  Orders  ************* //
    // ********************************************* //
    Route::group([
        'prefix' => 'orders',
    ], function () {
        Route::get('','OrderController@index')->name('orders');
        Route::post('getAjaxListData','OrderController@getAjaxListData')->name('orders.getAjaxListData');
        Route::get('view/{id}','OrderController@view')->name('orders.view');
        Route::get('invoice/{id}','OrderController@invoice')->name('orders.invoice'); // id = parent order id
        Route::get('cust-invoice/{id}','OrderController@custInvoice')->name('orders.custInvoice'); // id = parent order id Customer Invoice
        Route::get('chang-order-status/{id}','OrderController@changOrderStatus')->name('orders.changOrderStatus');
        Route::get('downloadCustomerInvoice/{id}','OrderController@downloadCustomerInvoice')->name('orders.downloadCustomerInvoice');

    });

    // ********************************************* //
    // **********  Payment & Commission  *********** //
    // ********************************************* //
    Route::group([
        'prefix' => 'payments',
    ], function () {
        Route::get('','PaymentController@index')->name('payments');
        Route::post('getAjaxListData','PaymentController@getAjaxListData')->name('payments.getAjaxListData');
        Route::get('view/{id}','PaymentController@view')->name('payments.view');
        Route::get('seler-invoice/{id}','PaymentController@sellerInvoice')->name('payments.sellerInvoice'); // id = Child order id Seller Invoice
        Route::get('downloadSellerInvoice/{id}','PaymentController@downloadSellerInvoice')->name('payments.downloadSellerInvoice');

    });

    // ********************************************* //
    // **********  Shipping Status  *********** //
    // ********************************************* //
    Route::group([
        'prefix' => 'shipping',
    ], function () {
        Route::get('','ShippingController@index')->name('shipping');
        Route::post('getAjaxListData','ShippingController@getAjaxListData')->name('shipping.getAjaxListData');
        Route::any('generateLabel/{shipmentid}','ShippingController@generateLabel')->name('shipping.generateLabel');
        Route::any('modifyAWBNumber/{id}','ShippingController@modifyAWBNumber')->name('shipping.modifyAWBNumber');
    });

    // ********************************************* //
    // ******************  Tickets  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'tickets',
    ], function () {
        Route::get('','TicketController@index')->name('tickets');
        Route::any('getAjaxListData','TicketController@getAjaxListData')->name('tickets.getAjaxListData');
        Route::any('add','TicketController@add')->name('tickets.add');
        Route::get('view/{id}','TicketController@view')->name('tickets.view');
        Route::any('delete/{id}','TicketController@delete')->name('tickets.delete');
        Route::post('postComment/{id}','TicketController@postComment')->name('tickets.postComment');
    });

    // ********************************************* //
    // ******************  Conversations  ***************** //
    // ********************************************* //
    Route::group([
        'prefix' => 'conversations',
    ], function () {
        Route::get('','ConversationController@index')->name('conversations');
    });

    // ********************************************* //
    // ******************  User Management  ***************** //
    // ********************************************* //
    Route::group([
    'prefix' => 'staffs',
    ], function () {
        Route::get('','UserManagementController@index')->name('staffs');
        Route::any('getAjaxListData','UserManagementController@getAjaxListData')->name('staffs.getAjaxListData');
        Route::any('create','UserManagementController@create')->name('staffs.create');
        Route::any('edit/{id}','UserManagementController@edit')->name('staffs.edit');
        Route::any('delete/{id}','UserManagementController@delete')->name('staffs.delete');
        Route::any('export','UserManagementController@export')->name('staffs.export');
    });

});
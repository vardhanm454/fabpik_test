<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\OrderDetail;
use App\Models\VariationImage;
use App\Models\NotificationLogs;
use App\Models\Shipingcharge;
use App\Models\OrderHandlingCharge;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return response()->json(['API'=>'OK']);
});

Route::get('/updateThumbnail', function () {
    $variation_images = VariationImage::all();
    foreach ($variation_images as $product) {
        $images = json_decode($product->images);
        if($images){
            if($product->thumbnail != null){
                VariationImage::where('id','=',$product->id)
                                ->whereNull('thumbnail_img_name')
                                ->update(['thumbnail_img_name'=>$images[($product->thumbnail-1)]]);
            }
        }
    }
    return response()->json(['success'=>true]);
});

Route::get('/updateNotificationLogs', function () {
    $users = User::selectRaw("users.id")
                              ->whereRaw("user_type = 'seller' OR user_type = 'customer'")
                              ->get();
    foreach ($users as $user) {
        $notification_log = new NotificationLogs();
        $notification_log->user_id = $user->id;
        $notification_log->save();
    }
    return response()->json(['success'=>true]);
});

Route::get('/updateCharges', function () {
    $orders = OrderDetail::selectRaw("order_details.*,sellers.commission,cm.commission_type,cm.commission_on,cm.vary_with_mrp_discount,cm.shipping_charges,cm.handling_charges,product_variants.price as seller_original_price,sellers.shipping_model as seller_shipping_model")
                           ->join("product_variants","product_variants.id","order_details.product_variant_id")
                           ->join("sellers","sellers.id","order_details.seller_id")
                           ->join("commission_models as cm","cm.id","sellers.commission_id")
                           ->get();
    foreach ($orders as $o_d) {
        if($o_d->price == $o_d->seller_original_price ){
            $o_d->seller_price = $o_d->seller_original_price;
            if($o_d->shipping_model == null){
                $o_d->shipping_model = $o_d->seller_shipping_model;
            }
            if($o_d->payout_status == 0){
                $totalShippingWeight = $o_d->quantity * $o_d->shipping_weight * 1000;
                $actualCommission = $o_d->commission;
                $sellerDiscountPercentage = ($o_d->discount *100)/$o_d->mrp;
                if($sellerDiscountPercentage > 0){
                    $percentage = $o_d->commission - (($sellerDiscountPercentage/100) * $o_d->commission);
                    $actualCommission = $percentage >= 8 ? $percentage : 8;
                }
                //commission on is mrp
                if($o_d->commission_on == 'm'){
                    if($o_d->vary_with_mrp_discount == 'y'){ 
                        $commission =($actualCommission/100) * ($o_d->mrp * $o_d->quantity);
                        $commission_sgst = $commission * (COMMISION_SGST/100);
                        $commission_cgst = $commission * (COMMISION_CGST/100);
                        $commission_igst = $commission * (COMMISION_IGST/100);          
                    }else{
                        $commission =($o_d->commission/100) * ($o_d->mrp * $o_d->quantity);
                        $commission_sgst = $commission * (COMMISION_SGST/100);
                        $commission_cgst = $commission * (COMMISION_CGST/100);
                        $commission_igst = $commission * (COMMISION_IGST/100); 
                    }
                }else{
                    //commission on selling price
                    if($o_d->vary_with_mrp_discount == 'y'){ 
                        $commission = ($actualCommission/100)* ($o_d->seller_original_price * $o_d->quantity);
                        $commission_sgst = (($actualCommission/100)* ($o_d->seller_original_price * $o_d->quantity)) * (COMMISION_SGST/100);
                        $commission_cgst = (($actualCommission/100)* ($o_d->seller_original_price * $o_d->quantity))* (COMMISION_CGST/100);
                        $commission_igst = (($actualCommission/100)* ($o_d->seller_original_price * $o_d->quantity))* (COMMISION_IGST/100);
                    }else{
                        $commission = ($o_d->commission/100)* ($o_d->seller_original_price * $o_d->quantity);
                        $commission_sgst = (($o_d->commission/100)* ($o_d->seller_original_price * $o_d->quantity)) * (COMMISION_SGST/100);
                        $commission_cgst = (($o_d->commission/100)* ($o_d->seller_original_price * $o_d->quantity))* (COMMISION_CGST/100);
                        $commission_igst = (($o_d->commission/100)* ($o_d->seller_original_price * $o_d->quantity))* (COMMISION_IGST/100);
                    }
                }
                $o_d->commission = $commission;
                $o_d->commission_sgst = $commission_sgst;
                $o_d->commission_cgst = $commission_cgst;
                $o_d->commission_igst = $commission_igst;
                
                $final_amount_to_pay_seller = ($o_d->seller_original_price * $o_d->quantity) - ($commission + $commission_igst);
        
                $shipping_charge = 0;
                $shipping_charge_sgst = 0;
                $shipping_charge_cgst = 0;
                $shipping_charge_igst = 0;
        
                if ($o_d->shipping_charges == 'y' && $o_d->shipping_model == 'f') {
                    $shippingCharge =  Shipingcharge::select("charge_amount")
                                                        ->whereRaw("min_amount <= ".$totalShippingWeight. " AND status = 1")
                                                        ->orderBy("min_amount", 'DESC')
                                                        ->first();
                    $shipping_charge = $shippingCharge->charge_amount;
                    $shipping_charge_sgst = (SHIPPING_SGST/100)*$shippingCharge->charge_amount;
                    $shipping_charge_cgst = (SHIPPING_CGST/100)*$shippingCharge->charge_amount;
                    $shipping_charge_igst = (SHIPPING_IGST/100)*$shippingCharge->charge_amount;
        
                    $final_amount_to_pay_seller -= ($shipping_charge + $shipping_charge_igst); 
                }
                $o_d->shipping_charge = $shipping_charge;
                $o_d->shipping_charge_sgst = $shipping_charge_sgst;
                $o_d->shipping_charge_cgst = $shipping_charge_cgst;
                $o_d->shipping_charge_igst = $shipping_charge_igst;
        
                $order_handling_charge = 0;
                $order_handling_charge_sgst = 0;
                $order_handling_charge_cgst = 0;
                $order_handling_charge_igst = 0;
        
                if($o_d->handling_charges == 'y'){
                    $totalSellingPriceForOrderHandling = $o_d->seller_original_price * $o_d->quantity;
                    $orderHandlingCharge =  OrderHandlingCharge::select("charge_amount")
                                                                    ->whereRaw("min_amount <= ".$totalSellingPriceForOrderHandling. " AND status = 1")
                                                                    ->orderBy("min_amount",'DESC')
                                                                    ->first();
                    
                    $order_handling_charge = $orderHandlingCharge->charge_amount;
                    $order_handling_charge_sgst = (ORDER_HANDLING_SGST/100)*$orderHandlingCharge->charge_amount;
                    $order_handling_charge_cgst = (ORDER_HANDLING_CGST/100)*$orderHandlingCharge->charge_amount;
                    $order_handling_charge_igst = (ORDER_HANDLING_IGST/100)*$orderHandlingCharge->charge_amount;
        
                    $final_amount_to_pay_seller -= ($order_handling_charge + $order_handling_charge_igst); 
                }
        
                $o_d->order_handling_charge = $order_handling_charge;
                $o_d->order_handling_charge_sgst = $order_handling_charge_sgst;
                $o_d->order_handling_charge_cgst = $order_handling_charge_cgst;
                $o_d->order_handling_charge_igst = $order_handling_charge_igst;
        
                $o_d->final_amount_to_pay_seller = $final_amount_to_pay_seller;
            }
            $o_d->save();
        }
    }

    return response()->json(['success'=>true]);
});

// Route::get('/shipped', function () {
//     $orderDetails = OrderDetail::with('order', 'productVarient')->whereIn('id', [16])->get();
//     // return $orderDetails[0];
//     $to = ['user_email'=>'avezmiraki@gmail.com'];
//     __sendEmails($to, 'order_shipped' , $orderDetails[0]);
// });

// Seller Apis

Route::post('handleWebhook', 'Api\Customer\WebhookController@handleWebhook');

Route::group([
    'middleware' => ['api','checksellerapiversion'],
    'prefix' => 'sellerapi'
], function () {
    require_once('sellerapi.php');
});
Route::group(['middleware' =>['checkuserapiversion']],function($router){
    Route::group([
        'middleware' => 'api',
        'prefix' => 'auth'
    ], function ($router) {
        Route::post('login', 'Api\AuthController@login')->name('login');
        Route::post('register', 'Api\AuthController@register')->name('register');
        Route::post('resendOtp', 'Api\AuthController@resendOtp')->name('resendOtp');
        Route::post('forgotPassword', 'Api\AuthController@forgotPassword')->name('forgotPassword');
        Route::post('resetPassword', 'Api\AuthController@resetPassword')->name('resetPassword');
        Route::post('verifyOtpRegister', 'Api\AuthController@verifyOtpRegister')->name('verifyOtpRegister');
        Route::post('verifyOtpLoginDetails', 'Api\AuthController@verifyOtpLoginDetails')->name('verifyOtpLoginDetails');
        Route::post('verifyOtpLogin', 'Api\AuthController@verifyOtpLogin')->name('verifyOtpLogin');
        Route::post('loginWithPassword', 'Api\AuthController@loginWithPassword')->name('loginWithPassword');
        Route::post('sendMailLoginOtp', 'Api\AuthController@sendMailLoginOtp')->name('sendMailLoginOtp');
        Route::post('forgot_password', 'Api\AuthController@forgot_password')->name('forgot_password');
        Route::post('change_password', 'Api\AuthController@change_password')->name('change_password');
    });

    Route::post('checkPincode', 'Api\CommonController@checkPincode')->name('checkPincode');
    Route::get('policy/{name}', 'Api\CommonController@policy')->name('policy');
    Route::get('mobile-sliders', 'Api\CommonController@mobileSliders')->name('mobileSliders');

Route::group([
    'prefix' => 'category'
], function () {
    Route::get('/', 'Api\CategoryController@categoryList');
    Route::get('/fivecategory', 'Api\CategoryController@fivecategory');
    // Route::get('/subCategoryList/{id}', 'Api\CategoryController@subCategoryList');
    // Route::get('/childCategoryList/{id}', 'Api\CategoryController@childCategoryList');
    Route::get('/categorySubCategoryList/{id}', 'Api\CategoryController@categorySubCategoryList');
    Route::get('/categorySubCategoryMobileList/{id}', 'Api\CategoryController@categorySubCategoryMobileList');
    Route::any('/subChildCategoryList/{id}', 'Api\CategoryController@subChildCategoryList');
    Route::any('/subChildCategoryMobileList/{id}', 'Api\CategoryController@subChildCategoryMobileList');
    Route::get('/onlyCategories', 'Api\CategoryController@onlyCategories');
});
Route::group([
    'prefix' => 'product'
], function () {
    Route::get('/popularCategory', 'Api\ProductController@popularCategory');
    Route::get('/search', 'Api\ProductController@search');
    Route::get('/getcouponsForProductDetails', 'Api\ProductController@getCouponsForProductDetails');
    Route::get('/autoSuggestionSearch', 'Api\ProductController@autoSuggestionSearch');
    Route::post('/productList', 'Api\ProductController@productList');
    Route::get('/productDetails/{product_variant_slug}/{unique_id}', 'Api\ProductController@productDetails');
    Route::get('/flashDeal', 'Api\ProductController@flashDeals');
    Route::get('/category/{id}', 'Api\ProductController@category');
    Route::get('/related/{id}', 'Api\ProductController@related');
    Route::post('getProductsFilters', 'Api\ProductController@getProductsFilters');
    Route::get('getGlobalSearchFilters', 'Api\ProductController@getGlobalSearchFilters');
    Route::get('getParentCategoires', 'Api\ProductController@getParentCategoires');
    Route::post('getSubCategories', 'Api\ProductController@getSubCategories');
    Route::post('getSubCategoriesBySlugs', 'Api\ProductController@getSubCategoriesBySlugs');
    Route::post('getChildCategories', 'Api\ProductController@getChildCategories');
    Route::post('getChildCategoriesBySlug', 'Api\ProductController@getChildCategoriesBySlug');
    Route::post('getFilteredData', 'Api\ProductController@getFilteredData');
    Route::get('checkPincode/{unique_id}', 'Api\ProductController@checkPincode');
    Route::post('checkenterPincode', 'Api\ProductController@checkenterPincode');
    Route::get('topSellingProducts', 'Api\ProductController@topSellingProducts');
    Route::get('similarProducts/{id}/{unique_id}', 'Api\ProductController@similarProducts');
    Route::get('getAllBrands', 'Api\ProductController@getAllBrands');
    Route::get('sliderImages/{view}', 'Api\ProductController@sliderImages');
    Route::get('deals', 'Api\ProductController@deals');
    Route::post('getAttributeFilters', 'Api\ProductController@getAttributeFilters');
    Route::post('nextVariantNavigation', 'Api\ProductController@nextVariantNavigation');
});

    Route::group([
        'prefix' => 'product'
    ], function () {
        Route::get('/popularCategory', 'Api\ProductController@popularCategory');
        Route::get('/search', 'Api\ProductController@search');
        Route::get('/getcouponsForProductDetails', 'Api\ProductController@getCouponsForProductDetails');
        Route::get('/autoSuggestionSearch', 'Api\ProductController@autoSuggestionSearch');
        Route::post('/productList', 'Api\ProductController@productList');
        Route::get('/productDetails/{product_variant_slug}/{unique_id}', 'Api\ProductController@productDetails');
        Route::get('/flashDeal', 'Api\ProductController@flashDeals');
        Route::get('/category/{id}', 'Api\ProductController@category');
        Route::get('/related/{id}', 'Api\ProductController@related');
        Route::post('getProductsFilters', 'Api\ProductController@getProductsFilters');
        Route::get('getGlobalSearchFilters', 'Api\ProductController@getGlobalSearchFilters');
        Route::get('getParentCategoires', 'Api\ProductController@getParentCategoires');
        Route::post('getSubCategories', 'Api\ProductController@getSubCategories');
        Route::post('getSubCategoriesBySlugs', 'Api\ProductController@getSubCategoriesBySlugs');
        Route::post('getChildCategories', 'Api\ProductController@getChildCategories');
        Route::post('getChildCategoriesBySlug', 'Api\ProductController@getChildCategoriesBySlug');
        Route::post('getFilteredData', 'Api\ProductController@getFilteredData');
        Route::get('checkPincode/{id}', 'Api\ProductController@checkPincode');
        Route::post('checkenterPincode', 'Api\ProductController@checkenterPincode');
        Route::get('topSellingProducts', 'Api\ProductController@topSellingProducts');
        Route::get('similarProducts/{id}/{unique_id}', 'Api\ProductController@similarProducts');
        Route::post('getAllBrands', 'Api\ProductController@getAllBrands');
        Route::get('sliderImages/{view}', 'Api\ProductController@sliderImages');
        Route::get('deals', 'Api\ProductController@deals');
        Route::post('getAttributeFilters', 'Api\ProductController@getAttributeFilters');
        Route::post('nextVariantNavigation', 'Api\ProductController@nextVariantNavigation');
    });

    Route::group([
        'prefix' => 'cart'
    ], function () {
        Route::get('/', 'Api\CartController@all');
        Route::get('/count', 'Api\CartController@count');
        Route::get('get/{id}', 'Api\CartController@get');
        Route::post('add', 'Api\CartController@add');
        Route::put('update', 'Api\CartController@updateCart');
        Route::post('getAttributes', 'Api\CartController@getAttributes');
        Route::delete('remove/{id}', 'Api\CartController@remove');
    });

    Route::group([
        'prefix' => 'guest',
    ], function () {
        Route::get('cart', 'Api\GuestCartController@cart');
        Route::post('addToCart', 'Api\GuestCartController@guestAddToCart');
        Route::post('removeItem', 'Api\GuestCartController@removeItemGuestCart');
        Route::get('cartCount', 'Api\GuestCartController@guestCartCount');
        Route::get('coupons', 'Api\GuestCartController@guestCoupons');
        Route::post('guestVirtualCart', 'Api\GuestCartController@guestVirtualCart');
    });
    Route::group([
        'prefix' => 'notifications'
    ], function () {
        Route::get('/', 'Api\Customer\NotificationsController@getNotifications');
        Route::get('checkNotificationEnabled', 'Api\Customer\NotificationsController@checkNotificationEnabled')->name('checkNotificationEnabled');
        Route::get('getNotificationsCount', 'Api\Customer\NotificationsController@getNotificationsCount')->name('getNotificationsCount');
        Route::get('enableNotifications', 'Api\Customer\NotificationsController@enableNotifications')->name('enableNotifications');
        Route::post('stockNotify', 'Api\Customer\NotificationsController@stockNotify')->name('stockNotify');
        Route::post('checkUserNotifiedForTheProduct', 'Api\Customer\NotificationsController@checkUserNotifiedForTheProduct')->name('checkUserNotifiedForTheProduct');
        Route::post('updateOrInsertFCMTokenPublic', 'Api\Customer\NotificationsController@updateOrInsertFCMTokenPublic')->name('updateOrInsertFCMTokenPublic');
        Route::post('updateOrInsertFCMTokenLoggedInUsers', 'Api\Customer\NotificationsController@updateOrInsertFCMTokenLoggedInUsers')->name('updateOrInsertFCMTokenLoggedInUsers');
        Route::get('updateVisitedTimeAndDate', 'Api\Customer\NotificationsController@updateVisitedTimeAndDate')->name('updateVisitedTimeAndDate');
    });
    Route::group(['middleware' => ['auth:api','checkCustomerActiveStatus']], function () {
        Route::post('changePassword', 'Api\AuthController@changePassword')->name('changePassword');
        Route::post('logout', 'Api\AuthController@logout')->name('logout');
        Route::post('refresh', 'Api\AuthController@refresh')->name('refresh');
        Route::get('userProfile', 'Api\AuthController@userProfile')->name('userProfile');
        Route::put('updateProfile', 'Api\AuthController@updateProfile')->name('updateProfile');

        Route::group([
            'prefix' => 'customer'
        ], function () {
            Route::group([
                'prefix' => 'addresses'
            ], function () {
                Route::get('/getState', 'Api\Customer\AddressController@getState');
                Route::get('/', 'Api\Customer\AddressController@all');
                Route::get('/sendGoogleAddressSuggestion', 'Api\Customer\AddressController@sendGoogleAddressSuggestion');
                Route::post('add', 'Api\Customer\AddressController@add');
                Route::get('/{id}', 'Api\Customer\AddressController@get');
                Route::put('update/{id}', 'Api\Customer\AddressController@update');
                Route::put('updatedefaultAddress/{id}', 'Api\Customer\AddressController@updatedefaultAddress');
                Route::delete('remove/{id}', 'Api\Customer\AddressController@remove');
            });

            Route::group([
                'prefix' => 'wishlists'
            ], function () {
                Route::get('/count', 'Api\Customer\WishlistController@count');
                Route::get('/', 'Api\Customer\WishlistController@all');
                Route::get('/variantIds', 'Api\Customer\WishlistController@variantIds');
                Route::post('/checkProductExistInWishlist', 'Api\Customer\WishlistController@checkProductExistInWishlist');
                Route::post('add', 'Api\Customer\WishlistController@add');
                Route::delete('remove/{id}', 'Api\Customer\WishlistController@remove');
            });

        });

        Route::group([
            'prefix' => 'checkout'
        ], function () {
            Route::get('/', 'Api\Customer\CheckoutController@getCheckoutDetails');
        });

        Route::group([
            'prefix' => 'payments'
        ], function () {
            Route::post('/generate_order_id', 'Api\Customer\PaymentController@generateOrderId');
            Route::get('/get_order_summary', 'Api\Customer\PaymentController@getOrderSummary');
            Route::post('/verify_payment_signature', 'Api\Customer\PaymentController@verifyPaymentSignature');
            Route::get('/reStock', 'Api\Customer\PaymentController@reStock');
        });

        Route::group([
            'prefix' => 'orders'
        ], function () {
            Route::post('/store_new_order', 'Api\Customer\OrderController@storeNewOrder');
            Route::post('/getOrderSuccess', 'Api\Customer\OrderController@getOrderSuccess');
            Route::get('/sendOtpForCod', 'Api\Customer\OrderController@sendOtpForCod');
            Route::post('/verifyOtpForCod', 'Api\Customer\OrderController@verifyOtpForCod');
            Route::get('/stockDeduction', 'Api\Customer\OrderController@stockDeduction');
        });

        Route::group([
            'prefix' => 'purchasehistory'
        ], function () {
            Route::post('/get_order_list', 'Api\Customer\PurchaseHistoryController@OrderList');
            Route::get('/get_orderinprocess_count', 'Api\Customer\PurchaseHistoryController@OrderInprocessCount');
            Route::get('/get_nextdelivery_count', 'Api\Customer\PurchaseHistoryController@NextDeliveryCount');
            Route::get('/get_filter_bystatus', 'Api\Customer\PurchaseHistoryController@OrderList');
            Route::get('/get_search_byId', 'Api\Customer\PurchaseHistoryController@OrderList');
            Route::get('/get_order_status_list', 'Api\Customer\PurchaseHistoryController@OrderStatusList');
            Route::get('/get_product_details_byid/{id}', 'Api\Customer\PurchaseHistoryController@getproductdetailsById');
            Route::get('/get_order_summary_byid/{id}', 'Api\Customer\PurchaseHistoryController@OrdersummaryById');
            Route::get('/get_shippingaddress_byid/{id}', 'Api\Customer\PurchaseHistoryController@getShippingAddressById');
            Route::put('/cancel_order', 'Api\Customer\PurchaseHistoryController@cancelOrder');
            Route::put('/return_order', 'Api\Customer\PurchaseHistoryController@returnOrder');
            Route::get('/download_invoice/{orderid}', 'Api\Customer\PurchaseHistoryController@customerInvoice');
            Route::get('/userOrdersInfoCount', 'Api\Customer\PurchaseHistoryController@userOrdersInfoCount');
            Route::get('/customerParentOrders', 'Api\Customer\PurchaseHistoryController@customerParentOrders');
        });

        Route::group([
            'prefix' => 'conversation'
        ], function () {
            Route::get('/conversationList', 'Api\Customer\ConversationController@conversationList');
            Route::post('/sendMessage', 'Api\Customer\ConversationController@sendMessage');
            Route::get('/getMessage', 'Api\Customer\ConversationController@getMessage');
            Route::post('/feedback', 'Api\Customer\ConversationController@feedback');
        });
    });

    Route::group([
        'prefix' => 'reviews'
    ], function () {
        Route::post('addreview', 'Api\Customer\ReviewController@addreview');
        Route::post('/getReviews/{pid}', 'Api\Customer\ReviewController@getReviews');
        Route::get('/getCustomerReviews', 'Api\Customer\ReviewController@getCustomerReviews');
        Route::post('/editReview', 'Api\Customer\ReviewController@editReview');
    });

    Route::group([
        'prefix' => 'coupon'
    ], function () {
        Route::get('/', 'Api\CouponsController@getAllCoupons');
        Route::post('/apply_coupon', 'Api\CouponsController@applyCoupon');
        Route::get('/remove_coupon', 'Api\CouponsController@removeCoupon');
        Route::get('/get_all_offers', 'Api\CouponsController@getAllOffers');
        Route::post('/getVirtualCart', 'Api\CouponsController@getVirtualCart');
    });

});

Route::group([
    'prefix' => 'shoplanding'
], function () {
    Route::get('/get_featued_newcollection_byId', 'Api\Seller\SellerShoplandingController@FeturedNewCollection');
});

// Market Place Apis
Route::group([
    'middleware' => ['api'],
    'prefix' => 'marketplace'
], function () {
    require_once('marketplace.php');
});

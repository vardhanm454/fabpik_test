<?php

use Illuminate\Support\Facades\Route;
use Rahulreghunath\Textlocal\Textlocal;

use App\Mail\TestMail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // session(['portal'=>'admin']);
    if (session()->get('portal') == 'admin')
		return redirect()->route('admin.login');

	return redirect()->route('seller.login');

	// return (auth()->check() && !is_null(auth()->user()->ref_id)) ? redirect()->route('seller.dashboard') : redirect()->route('admin.dashboard');
    // return __sendSms('6290844051');
    // return view('welcome');
    // return redirect()->route('admin.login');
});

Route::get('/sendsms', function () {
    // $sms = new Textlocal();
    // $sms->send('Hi there, thank you for sending your first test message from Textlocal. See how you can send effective SMS campaigns here: https://tx.gl/r/2nGVj/', '9290573910','600010'); //sender is optional

    // Account details
	$apiKey = urlencode('OTRhMzhhNDEzYzY4M2ZiOGZjY2NlZTZjYzZiYmZhNjk=');

	// Message details
	$numbers = array(919290573910);
	$sender = urlencode('600010');
	// $sender = urlencode('TXTLCL');
	$message = rawurlencode('Hi there, thank you for sending your first test message from Textlocal. See how you can send effective SMS campaigns here: https://tx.gl/r/2nGVj/');

	$numbers = implode(',', $numbers);

	// Prepare data for POST request
	$data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

	// Send the POST request with cURL
	$ch = curl_init('https://api.textlocal.in/send/');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close($ch);

	// Process your response here
	echo $response;
    dd(date("H:i:s"));
});

Route::get('/home', function () {
	return (auth()->check() && !is_null(auth()->user()->ref_id)) ? redirect()->route('seller.dashboard') : redirect()->route('admin.dashboard');
});

Route::any('filemanager', 'FileManagerController@index')->name('filemanager');
Route::any('filemanager/upload', 'FileManagerController@upload')->name('filemanager.upload');
Route::any('filemanager/folder', 'FileManagerController@folder')->name('filemanager.folder');
Route::any('filemanager/delete', 'FileManagerController@delete')->name('filemanager.delete');
Route::any('filemanager/rename', 'FileManagerController@rename')->name('filemanager.rename');

//Verify Email
Route::get('/verify/{token}', 'VerifyController@VerifyEmail')->name('verify');

Route::get('queue-worker', function () {
    Mail::to('ranjanm@mirakitech.com')->send(new TestMail());
    return 'Queue Worked';
});

//Convert images to webp
Route::get('/converwebp/{folder}', 'ImageConversionController@index');

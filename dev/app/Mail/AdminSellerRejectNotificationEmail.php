<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminSellerRejectNotificationEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $seller_details;

    public function __construct($seller_details)
    {
        $this->seller_details = $seller_details;
    }

    public function build()
    {
        return $this->view('emails.adminSellerRejectNotificationEmail');
    }
}


?>
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendOtpUserRegisterEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $otp;

    public function __construct($otp)
    {
        $this->otp = $otp;
    }

    public function build()
    {
        return $this->subject('Fabpik - Register OTP')
                            ->with($this->otp)
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.sendUserRegisterOtpEmail');
    }
}


?>
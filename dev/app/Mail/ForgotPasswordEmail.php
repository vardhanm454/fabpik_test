<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->subject('Welcome to FabPik – Registration Confirmation Email')
                            ->with(['data' => $this->customer, 'password'=>$this->password])
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.forgotPasswordEmail');
    }
    
}


?>
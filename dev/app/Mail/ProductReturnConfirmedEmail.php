<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductReturnConfirmedEmail extends Mailable implements ShouldQueue
{

    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        if($this->mailPerson == 'admin'){
            $subject = "Fabpik - Order ".$this->data->child_order_id." - return confirmation by seller ".$this->data->seller->seller_code;
            return $this->subject($subject)
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('mail@example.com', 'Fabpik')
                    ->view('emails.ProductReturnConfirmationAdminEmail');
        }       
    }
}
?>
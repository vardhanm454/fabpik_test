<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CustomerRegisterSuccessEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $customer;
    public $password;

    public function __construct($customer, $password)
    {
        $this->customer = $customer;
        $this->password = $password;
    }

    public function build()
    {
        return $this->subject('Welcome to FabPik – Registration Confirmation Email')
                    ->with(['data' => $this->customer, 'password'=>$this->password])
                    ->from('no-reply@fabpik.in', 'Fabpik')
                    ->view('emails.customerRegisterSuccessEmail');
    }
}


?>
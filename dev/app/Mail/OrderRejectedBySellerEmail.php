<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable; 
use Illuminate\Queue\SerializesModels;

class OrderRejectedBySellerEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        $subject='';
        if($this->mailPerson == 'seller'){
            $subject = 'Fabpik - Received your request to reject the order'.$this->data->child_order_id;
            return $this->subject($subject)
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('no-reply@fabpik.in', 'Fabpik')
                    ->view('emails.orderRejectedbySellerEmail');

        }else{
            $subject = 'Order '.$this->data->child_order_id.' Rejected By Seller';
            return $this->subject($subject)
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('no-reply@fabpik.in', 'Fabpik')
                    ->view('emails.orderRejectedbySellerEmail');
        }
        
    }
}


?>
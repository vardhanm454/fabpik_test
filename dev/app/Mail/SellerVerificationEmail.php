<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SellerVerificationEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $sellerDetails;

    public function __construct($sellerDetails)
    {
        $this->sellerDetails = $sellerDetails;
    }

    public function build()
    {
        return $this->view('emails.sellerVerifyEmailOtp')
                    ->subject('Fabpik –  Seller Login Credentials')   //Seller Email Verification
                    ->with($this->sellerDetails)
                    ->from('no-reply@fabpik.in', 'Fabpik');
    }
}


?>
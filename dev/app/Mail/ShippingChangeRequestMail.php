<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ShippingChangeRequestMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        $req='';
        ($this->data['shippingDetails']->new_value=='f')? $req='Fabpik' : $req='Self';

        if($this->mailPerson=='seller'){
            return $this->subject(' Fabpik – Request For Change Shippig Model to '.$req.'. Sent to FabPik Team' )
                                ->with($this->data)
                                ->from('no-reply@fabpik.in', 'Fabpik')
                                ->view('emails.ShippingChangeRequestSellerEmail');
        }else if($this->mailPerson=='admin'){
            return $this->subject(' Fabpik – Seller'.$this->data['sellerDetails']->seller_code.' - Change Shippig Model to '.$req)
                            ->bcc('seller-notifyorder@fabpik.in')
                            ->with($this->data)
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.ShippingChangeRequestEmail');
        }
        
    }
}


?>
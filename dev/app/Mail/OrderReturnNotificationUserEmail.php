<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderReturnNotificationUserEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
    }

    public function build()
    {
        return $this->subject('Order Returned')
                ->with(['data' => $this->data])
                ->from('mail@example.com', 'Fabpik')
                ->view('emails.ReturnOrderEmailUser');
    }
}
?>
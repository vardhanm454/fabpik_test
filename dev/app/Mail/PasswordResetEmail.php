<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordResetEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user,$password, $updatedDate;

    public function __construct($user,$password,$updatedDate)
    {
        $this->user = $user;
        $this->password = $password;
        $this->updatedDate = $updatedDate;
    }

    public function build()
    {
        return $this->view('emails.PasswordResetEmail');
    }
    
}


?>
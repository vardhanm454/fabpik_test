<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderAWBEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        if($this->mailPerson == 'seller'){
            return $this->subject(' Fabpik – Find the AWB for your Order '.$this->data->child_order_id)
                            ->bcc('seller-notifyorder@fabpik.in')
                            ->with(['data'=> $this->data])
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.OrderAWBEmail');
        }
        if($this->mailPerson == 'admin'){
            return $this->subject(' Fabpik – AWB Created for the Order '.$this->data->child_order_id)
                                ->bcc('seller-notifyorder@fabpik.in')
                                ->with(['data'=> $this->data])
                                ->from('no-reply@fabpik.in', 'Fabpik')
                                ->view('emails.OrderAdminAWBEmail');
        }
        
        
    }
}


?>
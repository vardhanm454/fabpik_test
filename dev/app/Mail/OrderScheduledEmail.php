<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderScheduledEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        if($this->mailPerson == 'seller'){
            return $this->subject(' Fabpik – Order '.$this->data->child_order_id.' - We will be soon there to pick up')
                            ->bcc('seller-notifyorder@fabpik.in')
                            ->with(['data'=> $this->data])
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.OrderScheduledEmail');
        }
        if($this->mailPerson == 'admin'){
            return $this->subject(' Fabpik – Order '.$this->data->child_order_id.' - Ready for pick up')
                                ->bcc('seller-notifyorder@fabpik.in')
                                ->with(['data'=> $this->data])
                                ->from('no-reply@fabpik.in', 'Fabpik')
                                ->view('emails.OrderScheduledAdminEmail');
        }
        
        
    }
}


?>
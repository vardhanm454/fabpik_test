<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordSellerMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = json_decode($data);
    }

    public function build()
    {
        return $this->subject('FabPik –  Password Reset – '.$this->data->name)
                    ->with(['data' => $this->data,])
                    ->from('no-reply@cs.fabpik.in', 'Fabpik')
                    ->view('emails.forgotPasswordSellerApi');
    }
}

?>
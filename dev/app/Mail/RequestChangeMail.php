<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RequestChangeMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        $req='';
        if($this->data['codDetails']->new_value=='n'){
            $req='Disable';
        }else{
            $req='Enable';
        }
        if($this->mailPerson=='seller'){
            return $this->subject(' Fabpik – Request For COD '.$req.' Sent to FabPik Team' )
                                ->with($this->data)
                                ->from('no-reply@fabpik.in', 'Fabpik')
                                ->view('emails.CodChangeRequestSellerEmail');
        }else if($this->mailPerson=='admin'){
            return $this->subject(' Fabpik – Seller'.$this->data['sellerDetails']->seller_code.' - Request For COD '.$req)
                            ->bcc('seller-notifyorder@fabpik.in')
                            ->with($this->data)
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.CodChangeRequestEmail');
        }
        
    }
}


?>
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderInfoCustomerEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $orderData;

    public function __construct($orderData)
    {
        $this->orderData = $orderData;
    }

    public function build()
    {
        return $this->subject("Fabpik - Your order ".$this->orderData['order']->parent_order_id." placed successfully")
                            ->with($this->orderData)
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.OrderInfo');
    }
}


?>
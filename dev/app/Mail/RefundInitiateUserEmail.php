<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RefundInitiateUserEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        // dd($this->data);
        return $this->subject('Refund Initiated')
                    ->with(['data' =>$this->data])
                    ->from('mail@example.com', 'Fabpik')
                    ->view('emails.refundInitiateUser');
    }
}


?>
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendRegisterSuccessEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $userDetails;

    public function __construct($userDetails)
    {
        $this->userDetails = $userDetails;
    }

    public function build()
    {
        return $this->subject('Fabpik - Thank you for registering with us.')
                            ->with($this->userDetails)
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.sendRegisterSuccessEmail');
    }
}


?>
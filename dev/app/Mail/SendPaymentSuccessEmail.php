<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendPaymentSuccessEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $paymentDetails;

    public function __construct($paymentDetails)
    {
        $this->paymentDetails = $paymentDetails;
    }

    public function build()
    {
        return $this->subject('Fabpik - Payment Success.')
                            ->with($this->paymentDetails)
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.sendPaymentSuccessEmail');
    }
}


?>
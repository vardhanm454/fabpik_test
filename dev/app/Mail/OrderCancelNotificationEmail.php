<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCancelNotificationEmail extends Mailable implements ShouldQueue
{

    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        if($this->mailPerson == 'user'){
            $subject = "Fabpik - Received Cancellation request for your order ".$this->data->child_order_id;
            return $this->subject($subject)
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('mail@example.com', 'Fabpik')
                    ->view('emails.orderCancel');
        }
        else if($this->mailPerson == 'seller'){
            $subject = "Fabpik - Cancellation request by user ".$this->data->order->first_name." - order ".$this->data->child_order_id;
            return $this->subject($subject)
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('mail@example.com', 'Fabpik')
                    ->view('emails.orderCancelSellerMail');
        }
        
        else if(($this->mailPerson == 'admin')){
            $subject = "Fabpik - Cancellation request by user ".$this->data->order->first_name." - order ".$this->data->child_order_id;
            return $this->subject($subject)
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('mail@example.com', 'Fabpik')
                    ->view('emails.orderCancelAdminMail');
        }

       
    }
}
?>
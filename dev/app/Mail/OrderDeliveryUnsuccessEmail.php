<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderDeliveryUnsuccessEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        if ($this->mailPerson == 'admin') {
            return $this->subject('Order '.$this->data->child_order_id.' Delivery unsuccessful')
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('no-reply@cs.fabpik.in', 'Fabpik')
                    ->view('emails.orderDeliveryUnsuccessfulEmail');
        }
        else if($this->mailPerson == 'user') {
            return $this->subject('order '.$this->data->child_order_id.' Delivery unsuccessful')
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('no-reply@cs.fabpik.in', 'Fabpik')
                    ->view('emails.orderDeliveryUnsuccessfulUser');
        }else if($this->mailPerson == 'seller') {
            return $this->subject('Fabpik - Undelivered order '.$this->data->child_order_id.' is returned to you ')
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('no-reply@cs.fabpik.in', 'Fabpik')
                    ->view('emails.orderDeliveryUnsuccessfulSeller');
        }
    }
}


?>
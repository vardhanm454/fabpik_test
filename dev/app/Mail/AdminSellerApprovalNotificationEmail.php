<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminSellerApprovalNotificationEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        return $this->subject(' Fabpik – seller '.$this->data->seller_code.' account is Approved')
                            ->with(['data'=>$this->data])
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.adminSellerApprovalNotificationEmail');
    }
}


?>
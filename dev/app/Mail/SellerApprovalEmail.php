<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SellerApprovalEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        return $this->subject(' Fabpik – Yayy your seller '.$this->data->seller_code.' account is Approved')
                            ->with(['data'=>$this->data])
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.sellerApprovalEmail');
        
        // return $this->view('emails.sellerApprovalEmail')
        //     ->subject('Fabpik – '.$seller_details->seller_code.' '.$seller_details->name.' Your Application Approved');
    }
}


?>
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminSentCommission extends Mailable implements ShouldQueue
// class AdminSentCommission extends Mailable
{
    use Queueable, SerializesModels;

    public $seller_data;

    public function __construct($seller_data)
    {
        $this->seller_data = $seller_data;
    }
    public function build()
    {
        
        return $this->subject('Fabpik - Comission approved')
                    ->with(['seller_data'=>$this->seller_data])
                    ->from('no-reply@fabpik.in', 'Fabpik')
                    ->view('emails.AdminSentCommission');
    }
}


?>
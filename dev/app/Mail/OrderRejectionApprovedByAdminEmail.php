<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderRejectionApprovedByAdminEmail extends Mailable implements ShouldQueue
{ 
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        if($this->mailPerson == 'seller'){
            $subject = 'Fabpik - Your request to reject order '. $this->data->child_order_id.' is approved';
            
            return $this->subject($subject)
                ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                ->from('no-reply@fabpik.in', 'Fabpik')
                ->view('emails.sellerRejectionApprovedbyAdmin');
        }else if($this->mailPerson == 'user'){
            $subject = 'Your order '. $this->data->child_order_id.' has problem';

            return $this->subject($subject)
                        ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                        ->from('no-reply@fabpik.in', 'Fabpik')
                        ->view('emails.orderRejectedbySellerUserEmail');
        }
        
    }
}
?>
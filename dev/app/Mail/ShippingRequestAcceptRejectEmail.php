<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ShippingRequestAcceptRejectEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;
    public $reqStatus;

    public function __construct($data, $mailPerson, $reqStatus)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
        $this->reqStatus = $reqStatus;
    }

    public function build()
    {
        $req='';
        $subject='';
        ($this->data->new_value=='f')? $req='FabPik' : $req='Self';
        ($this->reqStatus == 'accept')? $subject = ' Fabpik – Request For Changing Shipping Model to '.$req.' is Accepted by FabPik Team': $subject = ' Fabpik – Request For Changing Shipping Model to'.$req.' is Rejected by FabPik Team';
        
            return $this->subject($subject)
                            ->with(["data"=>$this->data])
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.ShippingChangeRequestSellerStatusEmail');
        
    }
}


?>
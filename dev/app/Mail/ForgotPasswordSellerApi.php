<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordSellerApi extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $password;

    public function __construct($password)
    {
        $this->password = $password;
    }

    public function build()
    {
        // return $this->view('emails.forgotPasswordEmail');
        return $this->subject('Your Password to login: '.$this->password)
                    ->markdown('emails.forgotPasswordSeller')->with([
                        'password' => $this->password
                    ]);
    }
    
}


?>
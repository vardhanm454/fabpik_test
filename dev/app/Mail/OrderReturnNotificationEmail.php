<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderReturnNotificationEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        
        return $this->subject('User '.$this->data->order->first_name.' has requested a return order '.$this->data->child_order_id)
                ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                ->from('mail@example.com', 'Fabpik')
                ->view('emails.returnOrderEmail');
    }
}
?>
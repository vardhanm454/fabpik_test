<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderInfoSellerEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $orderData;

    public function __construct($orderData)
    {
        $this->orderData = $orderData;
    }

    public function build()
    {
        return $this->subject(' Fabpik – '.$this->orderData['seller_details']->seller_code.' - Order in place - '.$this->orderData['sub_order']->child_order_id.' - '.$this->orderData['sub_order']->name)
                            ->bcc('seller-notifyorder@fabpik.in')
                            ->with($this->orderData)
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.OrderInfoSeller');
    }
}


?>
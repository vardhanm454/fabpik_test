<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RefundCompletedUserEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct()
    {
    }

    public function build()
    {
        return $this->subject('Order Delivered')
                    ->with(['data' => ''])
                    ->from('mail@example.com', 'Fabpik')
                    ->view('emails.refundCompletedUser');
    }
}


?>
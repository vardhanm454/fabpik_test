<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderShippedEmail extends Mailable implements ShouldQueue
{ 
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        if($this->mailPerson == 'user'){
            return $this->subject("Fabpik - Your order ".$this->data->child_order_id." is on the way")
                    ->with(['data' => $this->data])
                    ->from('mail@example.com', 'Fabpik')
                    ->view('emails.orderShippedSuccess');
        }
        if($this->mailPerson == 'admin'){
            return $this->subject("Fabpik - Your order ".$this->data->child_order_id." is Shipped")
                    ->with(['data' => $this->data])
                    ->from('mail@example.com', 'Fabpik')
                    ->view('emails.orderShippedSuccessAdmin');
        }
        if($this->mailPerson == 'seller'){
            return $this->subject("Fabpik - Order ".$this->data->child_order_id." is Shipped")
                    ->with(['data' => $this->data])
                    ->from('mail@example.com', 'Fabpik')
                    ->view('emails.orderShippedSuccessSeller');
        }
    }
}
?>
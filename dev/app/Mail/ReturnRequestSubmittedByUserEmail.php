<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReturnRequestSubmittedByUserEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        return $this->subject("Fabpik - Received Return request for your order ".$this->data->child_order_id)
                        ->with(['data' => $this->data])
                        ->from('mail@example.com', 'Fabpik')
                        ->view('emails.returnRequestSubmittedByUser');
    }
}


?>
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StockUpdateNotificationEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        return $this->subject($this->data['name'] . " is back.")
                            ->with(["data"=>$this->data])
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.StockUpdateNotificationEmail');
        
    }
}


?>
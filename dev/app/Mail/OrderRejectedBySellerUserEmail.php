<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable; 
use Illuminate\Queue\SerializesModels;

class OrderRejectedBySellerUserEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        return $this->subject('Fabpik - Sorry its taking time with your order '.$this->data->child_order_id)
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('no-reply@fabpik.in', 'Fabpik')
                    ->view('emails.orderRejectedbySellerUserEmail');
    }
}


?>
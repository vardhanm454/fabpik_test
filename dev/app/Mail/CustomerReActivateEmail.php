<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CustomerReActivateEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $customer_details;

    public function __construct($customer_details)
    {
        $this->customer_details = $customer_details;
    }

    public function build()
    {
        return $this->subject(' Fabpik – your account is Activated')
                            ->with(['customer_details'=>$this->customer_details])
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.customerReactivateEmail');
    }
}


?>
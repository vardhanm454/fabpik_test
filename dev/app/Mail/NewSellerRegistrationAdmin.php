<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewSellerRegistrationAdmin extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        if($this->mailPerson == 'seller'){
            
            return $this->subject('FabPik – Seller Registration – Thanks for registering with Fabpik ')
                ->with(['data' => $this->data])
                ->from('no-reply@fabpik.in', 'Fabpik')
                ->view('emails.sellerRegister');

        }else if($this->mailPerson == 'admin'){
            return $this->subject('FabPik – Seller Submission – '.$this->data['email'])
                ->with(['seller' => $this->data])
                ->from('no-reply@fabpik.in', 'Fabpik')
                ->view('emails.sellerRegisterAdmin');
                
        }else if($this->mailPerson == 'admin_register_seller'){
            return $this->subject('Seller Submission – '.$this->data['email'])
                ->with(['data' => $this->data])
                ->from('no-reply@fabpik.in', 'Fabpik')
                ->view('emails.AdminRegisterSeller');
        }
        
    }
}


?>
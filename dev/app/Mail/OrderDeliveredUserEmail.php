<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use PDF;

class OrderDeliveredUserEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;

    public function __construct($data, $mailPerson)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
    }

    public function build()
    {
        if ($this->mailPerson == 'user') {
            // $pdf = PDF::loadView('customer.orders.customerinvoice', ['orderDetails' => $this->data]);
            $pdf = PDF::loadView('invoices.customerinvoice', ['orderDetails' => $this->data]);
            return $this->subject('Your Order '.$this->data->child_order_id.' has Delivered')
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('no-reply@fabpik.in', 'Fabpik')
                    ->view('emails.orderDeliveredUser')
                    ->attachData($pdf->output(),'customer.pdf');
        }
        else if($this->mailPerson == 'seller') {
            return $this->subject('Fabpik - order '.$this->data->child_order_id.' is Delivered')
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('no-reply@fabpik.in', 'Fabpik')
                    ->view('emails.orderDeliveredSeller');
        }else if ($this->mailPerson == 'admin') {
            return $this->subject('Your Order '.$this->data->child_order_id.' has Delivered')
                    ->with(['data' => $this->data, 'mailPerson'=>$this->mailPerson])
                    ->from('no-reply@fabpik.in', 'Fabpik')
                    ->view('emails.orderDeliveredAdmin');
        }
    }
}


?>
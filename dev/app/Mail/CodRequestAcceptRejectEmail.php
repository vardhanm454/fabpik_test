<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CodRequestAcceptRejectEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    public $mailPerson;
    public $reqStatus;

    public function __construct($data, $mailPerson, $reqStatus)
    {
        $this->data = $data;
        $this->mailPerson = $mailPerson;
        $this->reqStatus = $reqStatus;
    }

    public function build()
    {
        $req='';
        $subject='';
        if($this->data->new_value=='n'){
            $req='Disable';
        }else{
            $req='Enable';
        }
        if($this->reqStatus == 'accept'){
            $subject = ' Fabpik – Request For COD '.$req.' is Accepted by FabPik Team';
        }else if($this->reqStatus == 'reject'){
            $subject = ' Fabpik – Request For COD '.$req.' is Rejected by FabPik Team';
        }
        return $this->subject($subject)
                            ->with(["data"=>$this->data])
                            ->from('no-reply@fabpik.in', 'Fabpik')
                            ->view('emails.CodChangeRequestSellerStatusEmail');
        
    }
}


?>
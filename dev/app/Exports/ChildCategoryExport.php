<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ChildCategoryExport implements FromView
{
    public $childcategories = [];

	public function __construct($childcategories)
    {
        $this->childcategories = $childcategories;
    }

    public function view(): View
    {
        return view('admin.childcategory.export', [
            'childcategories' => $this->childcategories
        ]);
    }
}
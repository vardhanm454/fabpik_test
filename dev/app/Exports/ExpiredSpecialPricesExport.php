<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExpiredSpecialPricesExport implements FromView
{
    public $prodVariants = [];

	public function __construct($prodVariants)
    {
        $this->prodVariants = $prodVariants;
    }

    public function view(): View
    {
        // dd('called');
        return view('admin.stockreport.expiredspecialpricesexport', [
            'prodVariants' => $this->prodVariants
        ]);
    }
}

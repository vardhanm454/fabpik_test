<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SizechartExport implements FromView
{
    public $sizecharts = [];

	public function __construct($sizecharts)
    {
        $this->sizecharts = $sizecharts;
    }

    public function view(): View
    {
        return view('admin.sizecharts.export', [
            'sizecharts' => $this->sizecharts
        ]);
    }
}

<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AttributeExport implements FromView
{
    public $attributes = [];

	public function __construct($attributes)
    {
        $this->attributes = $attributes;
    }

    public function view(): View
    {
        return view('admin.attributes.export', [
            'attributes' => $this->attributes
        ]);
    }
}
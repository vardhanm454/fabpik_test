<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class FullProductsExport implements FromView
{
    public $productslist = [];

	public function __construct($productslist, $catid, $seller_id)
    {
        $this->productslist = $productslist;
        $this->catid = $catid;
        $this->seller_id = $seller_id;
    }

    public function view(): View
    {
        return view('seller.products.productexport', [
            'products' => $this->productslist,
            'catid' => $this->catid,
            'seller_id' => $this->seller_id
        ]);
    }
}

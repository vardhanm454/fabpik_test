<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ModifiedStockExport implements FromView
{
    public $prodVariants = [];

	public function __construct($prodVariants)
    {
        $this->prodVariants = $prodVariants;
    }

    public function view(): View
    {
        // dd('called');
        return view('admin.stockreport.modifiedstockexport', [
            'prodVariants' => $this->prodVariants
        ]);
    }
}

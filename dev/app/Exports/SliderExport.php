<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// Models
use App\Slider;

class SliderExport implements FromView
{
    public $staffs = [];

	public function __construct($sliders)
    {
        $this->sliders = $sliders;
    }

    public function view(): View
    {
        return view('admin.slidermanagement.export', [
            'sliders' => $this->sliders
        ]);
    }
}

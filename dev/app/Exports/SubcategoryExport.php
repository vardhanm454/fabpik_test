<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SubcategoryExport implements FromView
{
    public $subcategories = [];

	public function __construct($subcategories)
    {
        $this->subcategories = $subcategories;
    }

    public function view(): View
    {
        return view('admin.subcategory.export', [
            'subcategories' => $this->subcategories
        ]);
    }
}

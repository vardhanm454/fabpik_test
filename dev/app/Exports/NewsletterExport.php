<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class NewsletterExport implements FromView
{
    public $newsletters = [];

	public function __construct($newsletters)
    {
        $this->newsletters = $newsletters;
    }

    public function view(): View
    {
        // dd('called');
        return view('admin.newsletter.export', [
            'newsletters' => $this->newsletters
        ]);
    }
}

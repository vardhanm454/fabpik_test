<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class TaxCoadesExport implements FromView
{
    public $taxcodes = [];

	public function __construct($taxcodes)
    {
        $this->taxcodes = $taxcodes;
    }

    public function view(): View
    {
        // dd('called');
        return view('admin.tax.export', [
            'taxcodes' => $this->taxcodes
        ]);
    }
}

<?php

namespace App\Exports;

use App\Staff;
//use Maatwebsite\Excel\Concerns\FromCollection;

use Illuminate\Contracts\View\View;

use Maatwebsite\Excel\Concerns\FromView;

class StaffExport implements FromView
{
   public $staffs = [];

	public function __construct($staffs)
    {
        $this->staffs = $staffs;
    }

    public function view(): View
    {
        return view('admin.usermanagement.export', [
            'staffs' => $this->staffs
        ]);
    }
}

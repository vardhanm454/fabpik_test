<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class DealsExport implements FromView
{
    public $deals = [];

	public function __construct($deals)
    {
        $this->deals = $deals;
    }

    public function view(): View
    {
        // dd('called');
        return view('admin.deals.export', [
            'deals' => $this->deals
        ]);
    }
}

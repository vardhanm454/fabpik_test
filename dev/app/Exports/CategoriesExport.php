<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CategoriesExport implements FromView
{
    public $categorieslist = [];

	public function __construct($categorieslist)
    {
        $this->categorieslist = $categorieslist;
    }

    public function view(): View
    {
        return view('seller.products.exportCategoriesList', [
            'categorieslist' => $this->categorieslist
        ]);
    }
}

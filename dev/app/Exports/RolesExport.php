<?php

namespace App\Exports;

use App\Staff;
//use Maatwebsite\Excel\Concerns\FromCollection;

use Illuminate\Contracts\View\View;

use Maatwebsite\Excel\Concerns\FromView;

class RolesExport implements FromView
{
   public $roles = [];

	public function __construct($roles)
    {
        $this->roles = $roles;
    }

    public function view(): View
    {
        return view('admin.roles.export', [
            'roles' => $this->roles
        ]);
    }
}

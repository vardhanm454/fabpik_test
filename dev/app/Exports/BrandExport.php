<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class BrandExport implements FromView
{
    public $brands = [];

	public function __construct($brands)
    {
        $this->brands = $brands;
    }

    public function view(): View
    {
        return view('admin.brand.export', [
            'brands' => $this->brands
        ]);
    }
}

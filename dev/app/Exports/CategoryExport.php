<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CategoryExport implements FromView
{
    public $categories = [];

	public function __construct($categories)
    {
        $this->categories = $categories;
    }

    public function view(): View
    {
        return view('admin.category.export', [
            'categories' => $this->categories
        ]);
    }
}

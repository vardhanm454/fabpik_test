<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class LowStockExport implements FromView
{
    public $prodVariants = [];

	public function __construct($prodVariants)
    {
        $this->prodVariants = $prodVariants;
    }

    public function view(): View
    {
        // dd('called');
        return view('admin.stockreport.lowstockexport', [
            'prodVariants' => $this->prodVariants
        ]);
    }
}

<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://apiv2.shiprocket.in/v1/external/settings/company/addpickup',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
	"pickup_location": "Home",
	"name": "Deadpool",
	"email": "deadpool@chimichanga.com",
	"phone": "",
	"address": "Mutant Facility, Sector 3 ",
	"address_2": "",
	"city": "Pune",
	"state":"Maharshtra",
	"country": "India",
	"pin_code": "110022"
	
}',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
    'Authorization: Bearer {{token}}'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
<?php

use Illuminate\Support\Str;
use Seshac\Shiprocket\Shiprocket;
use Craftsys\Msg91\Facade\Msg91;

//E-Mails
use App\Mail\OrderCancelNotificationEmail;
use App\Mail\OrderRejectedBySellerEmail;
use App\Mail\OrderRejectedBySellerUserEmail;
use App\Mail\OrderReturnNotificationUserEmail;
use App\Mail\ReturnRequestSubmittedByUserEmail;
use App\Mail\OrderShippedEmail;
use App\Mail\OrderReturnNotificationEmail;
use App\Mail\OrderReturnApprovedByAdminEmail;
use App\Mail\OrderRejectionApprovedByAdminEmail;
use App\Mail\OrderDeliveredUserEmail;
use App\Mail\OrderDeliveryUnsuccessEmail;
use App\Mail\SellerVerificationEmail;
use App\Mail\NewSellerRegistrationAdmin;
use App\Mail\OrderInfoSellerEmail;
use App\Mail\OrderReturnApprovedByAdminUserEmail;
use App\Mail\SellerApprovalEmail;
use App\Mail\AdminSellerApprovalNotificationEmail;
use App\Mail\OrderScheduledEmail;
use App\Mail\SellerPasswordUpdate;
use App\Mail\RequestChangeMail;
use App\Mail\CodRequestAcceptRejectEmail;
use App\Mail\ShippingChangeRequestMail;
use App\Mail\ShippingRequestAcceptRejectEmail;
use App\Mail\ProductReturnConfirmedEmail;
use App\Mail\UndeliveredProductReturnConfirmedEmail;
use App\Mail\OrderAWBEmail;
use App\Mail\StockUpdateNotificationEmail;
use App\Mail\UserAddressVerificationEmail;
use App\Mail\userAddressModificationEmail;
use App\Mail\UserCancelOrderEmail;
use App\Mail\UserCancelOrderModificationEmail;

//Models
use App\Models\User;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\OrderHistory;
use App\Models\ProductVariant;
use App\Models\ProductVariantOption;
use App\Models\Attribute;
use App\Models\State;
use App\Models\Seller;
use App\Models\SellerWarehouses;

// models
use App\Models\SmsLog;


## CONVERT ALL TYPES OF IMAGES INTO WEBP FORMAT
if (! function_exists('__convert_into_webp')) {

    function __convert_into_webp($file) {

        $fileInfo = pathinfo($file);
        

        switch (strtolower($fileInfo['extension'])) {
            case 'jpg':
                $image =  imagecreatefromjpeg($file);
                break;

            case 'jpeg':
                $image =  imagecreatefromjpeg($file);
                break;

            case 'png':
                $image =  imagecreatefrompng($file);
                break;

            default:
                # code...
                break;
        }

        ob_start();
        imagejpeg($image,NULL,80);
        $cont =  ob_get_contents();
        ob_end_clean();
        imagedestroy($image);
        $content =  imagecreatefromstring($cont);

        $webpFile = $fileInfo['dirname'].'/'.$fileInfo['filename'].'.webp';
        imagewebp($content,$webpFile);
        imagedestroy($content);

        return $webpFile;
    }
}


if (! function_exists('__common_asset')) {
    function __common_asset($path, $secure = null) {
        return asset('/common/' . trim($path, '/'), $secure);
    }
}

if (! function_exists('__frontend_asset')) {
    function __frontend_asset($path, $secure = null) {
        return asset('/'.FRONTEND_THEME_NAME.'/' . trim($path, '/'), $secure);
    }
}

if (! function_exists('__seller_asset')) {
    function __seller_asset($path, $secure = null) {
        return asset('/'.SELLER_THEME_NAME.'/' . trim($path, '/'), $secure);
    }
}

if (! function_exists('__admin_asset')) {
    function __admin_asset($path, $secure = null) {
        return asset('/'.ADMIN_THEME_NAME.'/' . trim($path, '/'), $secure);
    }
}

if (!function_exists('__deleteImage')) {
    function __deleteImage($image=null)
    {
        if (File::exists($image)) {
            File::delete($image);
        }
    }
}

if (! function_exists('__generateSlug')) {
    function __generateSlug($string='', $separator='-')
    {
        return Str::slug(strtolower($string), $separator);
    }
}

if (!function_exists('__generateOtp')) {
    function __generateOtp($length = 4)
    {
        $random = str_shuffle('0123456789');
        return substr($random, 0, $length);
    }
}

if (!function_exists('__generateEmailOtp')) {
    function __generateEmailOtp($length = 6)
    {
        $random = str_shuffle('0123456789');
        return substr($random, 0, $length);
    }
}

if (!function_exists('__saveImage')) {
    function __saveImage($imageInput, $type, $thumb=false)
    {
        $filename = md5(uniqid().uniqid().uniqid()) . '.' . $imageInput->getClientOriginalExtension();
        $image = Image::make($imageInput);

        switch ($type) {
            case 'brand':
                $destPath = PRODUCTS_IMG_UPLOAD_PATH;
                $width = ($thumb)?320:960;
                $height = ($thumb)?320:960;
                break;

            case 'category':
                $destPath = PRESCRIPTIONS_IMG_UPLOAD_PATH;
                $width = 640;
                $height = 640;
                break;

            default:
                # code...
                break;
        }

        // $image->widen($width)->save($destPath . '/' . $filename);
        $image->resize($width, $height/*, function ($constraint) {
                    $constraint->aspectRatio();
                }*/)->save($destPath . '/' . $filename);

        /*if($thumb) {
            $image->widen(320)->save($destPath . '/' . $filename;
        } else {
            if($image->width() > $image->height()) {
                $image->widen(960)->save($destPath . '/' . $filename . '_zoom.' . $imageInput->getClientOriginalExtension())
                      ->widen(320)->save($destPath . '/' . $filename . '_large.' . $imageInput->getClientOriginalExtension())
                      ->widen(100)->save($destPath . '/' . $filename . '_thumb.' . $imageInput->getClientOriginalExtension());
            } else {
                $image->heighten(960)->save($destPath . '/' . $filename . '_zoom.' . $imageInput->getClientOriginalExtension())
                      ->heighten(320)->save($destPath . '/' . $filename . '_large.' . $imageInput->getClientOriginalExtension())
                      ->heighten(100)->save($destPath . '/' . $filename . '_thumb.' . $imageInput->getClientOriginalExtension());
            }
        }*/

        return $filename;
    }
}

if (!function_exists('__setDatatableCurrPage')) {
    function __setDatatableCurrPage($for, $length=10, $page=1){
        Session::put('brand_perpage', Session::get('brand_perpage', 10));
        Session::put('brand_page', Session::get('brand_page', 1));
        Session::put('category_perpage', Session::get('category_perpage', 10));
        Session::put('category_page', Session::get('category_page', 1));
        Session::put('subcategory_perpage', Session::get('subcategory_perpage', 10));
        Session::put('subcategory_page', Session::get('subcategory_page', 1));
        Session::put('attribute_perpage', Session::get('attribute_perpage', 10));
        Session::put('attribute_page', Session::get('attribute_page', 1));
        Session::put('sizechart_perpage', Session::get('sizechart_perpage', 10));
        Session::put('sizechart_page', Session::get('sizechart_page', 1));
        Session::put('product_perpage', Session::get('product_perpage', 10));
        Session::put('product_page', Session::get('product_page', 1));

        Session::put('order_perpage', Session::get('order_perpage', 10));
        Session::put('order_page', Session::get('order_page', 1));
        Session::put('payment_perpage', Session::get('payment_perpage', 10));
        Session::put('payment_page', Session::get('payment_page', 1));
        Session::put('shipping_perpage', Session::get('shipping_perpage', 10));
        Session::put('shipping_page', Session::get('shipping_page', 1));

        Session::put('ticket_perpage', Session::get('ticket_perpage', 10));
        Session::put('ticket_page', Session::get('ticket_page', 1));

        Session::put('seller_perpage', Session::get('seller_perpage', 10));
        Session::put('seller_page', Session::get('seller_page', 1));

        Session::put('customer_perpage', Session::get('customer_perpage', 10));
        Session::put('customer_page', Session::get('customer_page', 1));

        Session::put('shippingcharge_perpage', Session::get('shippingcharge_perpage', 10));
        Session::put('shippingcharge_page', Session::get('shippingcharge_page', 1));
        Session::put('handelingcharge_perpage', Session::get('handelingcharge_perpage', 10));
        Session::put('handelingcharge_page', Session::get('handelingcharge_page', 1));


        switch ($for) {
            case 'brand':
                Session::put('brand_perpage', $length);
                Session::put('brand_page', $page);
                break;

            case 'category':
                Session::put('category_perpage', $length);
                Session::put('category_page', $page);
                break;

            case 'subcategory':
                Session::put('subcategory_perpage', $length);
                Session::put('subcategory_page', $page);
                break;

            case 'attribute':
                Session::put('attribute_perpage', $length);
                Session::put('attribute_page', $page);
                break;

            case 'sizechart':
                Session::put('sizechart_perpage', $length);
                Session::put('sizechart_page', $page);
                break;

            case 'product':
                Session::put('product_perpage', $length);
                Session::put('product_page', $page);
                break;

            case 'order':
                Session::put('order_perpage', $length);
                Session::put('order_page', $page);
                break;

            case 'payment':
                Session::put('payment_perpage', $length);
                Session::put('payment_page', $page);
                break;

            case 'shipping':
                Session::put('shipping_perpage', $length);
                Session::put('shipping_page', $page);
                break;

            case 'ticket':
                Session::put('ticket_perpage', $length);
                Session::put('ticket_page', $page);
                break;

            case 'seller':
                Session::put('seller_perpage', $length);
                Session::put('seller_page', $page);
                break;

            case 'customer':
                Session::put('customer_perpage', $length);
                Session::put('customer_page', $page);
                break;

            case 'shippingcharge':
                Session::put('shippingcharge_perpage', $length);
                Session::put('shippingcharge_page', $page);
                break;

            case 'handelingcharge':
                Session::put('handelingcharge_perpage', $length);
                Session::put('handelingcharge_page', $page);
                break;

            default:
                # code...
                break;
        }
    }
}

if (!function_exists('__generateSellerCode')) {
    function __generateSellerCode()
    {
        $maxSerial = DB::table('sellers')->max('seller_code');
        $serial = ($maxSerial)?$maxSerial+1:100000;

        return $serial;
    }
}

if (!function_exists('__generateProductSerial')) {
    function __generateProductSerial()
    {
        $maxSerial = DB::table('products')->max('unique_id');
        $serial = ($maxSerial)?$maxSerial+1:10000000;

        return $serial;
    }
}

if (!function_exists('__generateProductVariantSerial')) {
    function __generateProductVariantSerial()
    {
        $maxSerial = DB::table('product_variants')->max('unique_id');
        $serial = ($maxSerial)?$maxSerial+1:10000000;

        return $serial;
    }
}

if (!function_exists('__generateNewProductVariantSerial')) {
    function __generateNewProductVariantSerial($id, $uniqueid) // Product id, uniqueId
    {
        $variantCount = DB::table('product_variants')->where('product_id', $id)->count();
        $serial = $uniqueid.'-'.($variantCount+1);
        return $serial;
    }
}

if (!function_exists('__generateSellerSerial')) {
    function __generateSellerSerial()
    {
        $maxSerial = DB::table('sellers')->max('unique_id');
        $serial = ($maxSerial)?$maxSerial+1:10000;

        return $serial;
    }
}

if (!function_exists('__generateParentInvoiceNo')) {
    function __generateParentInvoiceNo()
    {
        $maxSerial = DB::table('orders')->max('fabpik_invoice_no');
        $serial = ($maxSerial)? (int) $maxSerial+1:10000000;

        return $serial;
    }
}

/**
 * 90001000 (Sequence for Parent IDs)
 */
if (!function_exists('__generateParentOrderId')) {
    function __generateParentOrderId()
    {
        $maxSerial = DB::table('orders')->max('parent_order_id');
        $serial = ($maxSerial)? (int) $maxSerial+1:90001000;

        return $serial;
    }
}

/**
 * S1001000 (Seller Invoice to User)
 */

if (!function_exists('__generateInvoiceNo')) {
    function __generateInvoiceNo()
    {
        $maxSerial = DB::table('order_details')->max('invoice_no');
        $serial = ($maxSerial)?$maxSerial+1:1001000;

        return $serial;
    }
}

/**
 * FAB11001 (Invoice Sequence from Fabpik to Seller)
 */
if (!function_exists('__generateSellerInvoiceNo')) {
    function __generateSellerInvoiceNo()
    {
        $maxSerial = DB::table('order_details')->max('seller_invoice_no');
        $serial = ($maxSerial)?$maxSerial+1:11001;

        return $serial;
    }
}

/**
 * 10001000 (Child Order IDs)
 */
if (!function_exists('__generateChildOrderId')) {
    function __generateChildOrderId()
    {
        $maxSerial = DB::table('order_details')->max('child_order_id');
        $serial = ($maxSerial)?$maxSerial+1:10001000;

        return $serial;
    }
}

if (!function_exists('__sendSms')) {
    function __sendSms($to, $action='', $params=[])
    {
        $authKey = env('Msg91_KEY', '317054AHQrBOCY5e3c27d8P1');
        // $senderId = env('Msg91_Sender', 'fabpik');
        $senderId = env('Msg91_Sender', 'FABPIN');

        switch ($action) {
            case 'login_otp':
                $flow_id = '5fe30f07b5cbd11e8a316dd3';
                break;

            case 'user_reg_success':
                $flow_id = '5fe30fe33d7d1e135a426c2e';
                break;

            case 'seller_registration':
                $flow_id = '5fe3103d2ab6800ab6585c63';
                break;

            case 'seller_reg_to_admin':
                $flow_id = '5fe310b65f58e6758422af9d';
                break;

            case 'seller_approved':
                $flow_id = '5fe310ecf5b54460347ab6b4';
                break;

            case 'order_place_user':
                $flow_id = '5fe311764fdbf03cb76c840a';
                break;

            case 'order_place_seller':
                $flow_id = '5fe31206b4d2322c2e2dccd5';
                break;

            case 'order_place_admin':
                $flow_id = '5fe3128837ef67756a25b389';
                break;

            case 'verify_mobile':   // Vars: otp
                $flow_id = '5fe312de97282b62e306f9ee';
                break;

            case 'order_cancelled':   // Vars: orderid
                $flow_id = '5fe31992a42d0f3b9632987f';
                break;

            case 'refund_request':   // Vars: orderid
                $flow_id = '5fe31bdc6844c7461d5e3008';
                break;

            case 'refund_complete':   // Vars: amount
                $flow_id = '5fe31a4dd18c782e6218cd63';
                break;

            case 'refund_initiate':   // Vars: orderid
                $flow_id = '5fe31ad27d44c25c50407ac0';
                break;

            default:
                # code...
                break;
        }

        try {
            // $flow_id = '5fe1e274d2e48a49684a8c4b';
            $params['mobiles'] = '91'.$to;
            $recipients = [$params];
            /*$recipients = array(
                array(
                    "mobiles" => "919XXXXXXXXX",
                    "VAR1" => "VALUE 1",
                    "VAR2" => "VALUE 2"
                ),
                array(
                    "mobiles" => "9189XXXXXXXX",
                    "VAR1" => "VALUE 1",
                    "VAR2" => "VALUE 2"
                )
            );*/

            //Prepare you post parameters
            $postData = array(
                "sender" => $senderId,
                "flow_id" => $flow_id,
                "recipients" => $recipients
            );
            $postDataJson = json_encode($postData);

            $url="https://control.msg91.com/api/v5/flow/";

            // header('Access-Control-Allow-Origin:*');
            // header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
            // header('Access-Control-Allow-Headers:Content-type, X-Auth-Token, Authorization, Origin, api_version');

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "$url",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $postDataJson,
                CURLOPT_HTTPHEADER => array(
                    "authkey: ".$authKey,
                    "content-type: application/json"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                // insert in SMS logs table
                SmsLog::insert(['mobile'=>$to,'action'=>$action,'response_data'=>$response]);
                return true;
            }
        } catch (\Exception $e) {
            dd($e);
            // something else went wrong
            // plese report if this happens :)
        }
    }
}

if (!function_exists('__sendEmails')) {
    function __sendEmails($to=[], $action='', $params=[])
    {
        $admin_mail = ADMIN_EMAIL;
        $result = 0;
        switch ($action) {
            case "order_placed": //when customer places an order.
                //Seller Email
                Mail::to($to['seller_email'])->send(new OrderInfoSellerEmail(['order'=>$params['order'],'sub_order'=>$params['sub_order'],'seller_details'=>$params['seller_details']]));

                if(!Mail::failures()){
                    $result = 1;
                }
                return $result;
                break;
            case "seller_registration":
                //Seller Email
                Mail::to($to['seller_email'])->send(new SellerVerificationEmail($params));
                if(!Mail::failures()){
                    $result = 1;
                }
                return $result;
                break;
            case "seller_registration_admin":
                //selller Email
                Mail::to($to['seller_email'])->send(new NewSellerRegistrationAdmin($params, 'seller'));
                //admin Email
                Mail::to($admin_mail)->send(new NewSellerRegistrationAdmin($params, 'admin'));
                if(!Mail::failures()){
                    $result = 1;
                }
                return $result;
                break;
            case "seller_approval":
                //seller Email
                Mail::to($to['seller_email'])->send(new SellerApprovalEmail($params));
                //admin Email
                Mail::to($admin_mail)->send(new AdminSellerApprovalNotificationEmail($params));
                break;
            case "order_cancelled":
                //User Email
                Mail::to($to['user_email'])->send(new OrderCancelNotificationEmail($params, 'user'));
                //admin Email
                Mail::to($admin_mail)->send(new OrderCancelNotificationEmail($params, 'admin'));
                //Seller Email
                Mail::to($to['seller_email'])->send(new OrderCancelNotificationEmail($params, 'seller'));
                if(!Mail::failures()){
                    $result = true;
                }
                break;
            case "order_rejected_by_seller":
                // Seller Email
                Mail::to($to['seller_email'])->send(new OrderRejectedBySellerEmail($params, 'seller'));
                //user email
                Mail::to($to['user_email'])->send(new OrderRejectedBySellerUserEmail($params, 'seller'));
                // admin Email - Seller Rejected
                Mail::to($admin_mail)->send(new OrderRejectedBySellerEmail($params, 'admin'));
                return 1;
                break;
            case "seller_rejected_approved_by_admin":
                // Seller Email
                Mail::to($to['seller_email'])->send(new OrderRejectionApprovedByAdminEmail($params, 'seller'));

                // User Email - Problem With the Order

                Mail::to($to['user_email'])->send(new OrderRejectionApprovedByAdminEmail($params, 'user'));
                return 1;
                break;
            case "order_confirmed":
                break;
            case "shipping_status":
                break;
            case "order_delivered":
                //User Email
                Mail::to($to['user_email'])->send(new OrderDeliveredUserEmail($params, 'user'));
                //seller Email
                Mail::to($to['seller_email'])->send(new OrderDeliveredUserEmail($params, 'seller'));
                //admin Email
                Mail::to($admin_mail)->send(new OrderDeliveredUserEmail($params, 'admin'));
                return 1;
                break;
            case "delivery_unsuccess":
                //User Email
                Mail::to($to['user_email'])->send(new OrderDeliveryUnsuccessEmail($params, 'user'));
                //admin Email
                Mail::to($admin_mail)->send(new OrderDeliveryUnsuccessEmail($params, 'admin'));
                //seller Email
                Mail::to($to['seller_email'])->send(new OrderDeliveryUnsuccessEmail($params, 'seller'));
                break;
            case "return_order":
                //User Email

                Mail::to($to['user_email'])->send(new ReturnRequestSubmittedByUserEmail($params));

                //admin Email

                Mail::to($admin_mail)->send(new OrderReturnNotificationEmail($params, 'admin'));

                // //Seller Email

                // Mail::to($to['seller_email'])->send(new OrderReturnNotificationEmail($params, 'seller'));
                return 1;
                break;
            case "return_approved_by_admin":
                // Seller Email.
                Mail::to($to['seller_email'])->send(new OrderReturnApprovedByAdminEmail($params, 'seller'));

                //User Email
                Mail::to($to['user_email'])->send(new OrderReturnApprovedByAdminUserEmail($params));

                //Admin Email
                Mail::to($admin_mail)->send(new OrderReturnApprovedByAdminEmail($params, 'admin'));

                return 1;
                break;

            case "refund_initiated":
                break;
            case "refund_done":
                break;
            case "ready_for_pickup":
                Mail::to($to['seller_email'])->send(new OrderScheduledEmail($params, 'seller'));

                Mail::to($admin_mail)->send(new OrderScheduledEmail($params, 'admin'));

                break;
            case "pickup_scheduled":
                //mail from admin to Seller - When the order is ready to ship / AWB Created
                Mail::to($to['seller_email'])->send(new OrderAWBEmail($params, 'seller'));

                Mail::to($admin_mail)->send(new OrderAWBEmail($params, 'seller'));

                if(!Mail::failures()){
                    $result = 1;
                }
                return $result;
                break;
            case "order_shipped":
                Mail::to($to['user_email'])->send(new OrderShippedEmail($params, 'user'));
                Mail::to($to['seller_email'])->send(new OrderShippedEmail($params, 'seller'));
                Mail::to($admin_mail)->send(new OrderShippedEmail($params, 'admin'));
                return 1;
                break;
            case "product_return_confirmed":
                Mail::to($admin_mail)->send(new ProductReturnConfirmedEmail($params, 'admin'));
                break;
            case "undelivered_product_return_confirmed":
                Mail::to($admin_mail)->send(new UndeliveredProductReturnConfirmedEmail($params, 'admin'));
                break;
            case "seller_cod_change_request":
                Mail::to($to['seller_email'])->send(new RequestChangeMail($params, 'seller'));
                Mail::to($admin_mail)->send(new RequestChangeMail($params, 'admin'));
                return 1;
                break;
            case "seller_cod_change_request_accept":
                Mail::to($to['seller_email'])->send(new CodRequestAcceptRejectEmail($params, 'seller', 'accept'));
                return 1;
                break;
            case "seller_cod_change_request_reject":
                Mail::to($to['seller_email'])->send(new CodRequestAcceptRejectEmail($params, 'seller', 'reject'));
                return 1;
                break;
            case "seller_shipping_change_request":
                Mail::to($to['seller_email'])->send(new ShippingChangeRequestMail($params, 'seller'));
                Mail::to($admin_mail)->send(new ShippingChangeRequestMail($params, 'admin'));
                return 1;
                break;
            case "seller_shipping_change_request_accept":
                Mail::to($to['seller_email'])->send(new ShippingRequestAcceptRejectEmail($params, 'seller', 'accept'));
                break;
            case "seller_shipping_change_request_reject":
                Mail::to($to['seller_email'])->send(new ShippingRequestAcceptRejectEmail($params, 'seller', 'reject'));
                break;
            case "stock_update":
                Mail::to($to['user_email'])->send(new StockUpdateNotificationEmail($params));
                break;
            case "change_address_verify_mobile":
                Mail::to($to['user_email'])->send(new UserAddressVerificationEmail($params));
                if(count(Mail::failures()) > 0){
                    return false;
                }else{
                    return true;
                }
                break;
            case "cancel_order_verify_mobile":
                Mail::to($to['user_email'])->send(new UserCancelOrderEmail($params));
                if(count(Mail::failures()) > 0){
                    return false;
                }else{
                    return true;
                }
                break;
            case "change_address_modify":
                Mail::to($to['user_email'])->send(new userAddressModificationEmail($params));
                if(count(Mail::failures()) > 0){
                    return false;
                }else{
                    return true;
                }
                break;
            case "cancel_order":
                Mail::to($to['user_email'])->send(new UserCancelOrderModificationEmail($params));
                if(count(Mail::failures()) > 0){
                    return false;
                }else{
                    return true;
                }
                break;
            default:
                # code...
                break;
        }
    }
}

// Order Status Related Functions
if (!function_exists('__changeStatus')) {
    function __changeStatus($status, $orderIds = [], $remark=null)
    {
        switch ($status) {
            case '2': // Order cancelled By User or Customer

                $orderCancelledError=[];

                //canel the order from the shipRocket
                $getShippmentOrderIds = OrderDetail::whereIn('id', $orderIds)->get();
                $ids=[];
                $token =  Shiprocket::getToken(); //get the shiprocket login barear token
                foreach ($getShippmentOrderIds as $i => $cancelOrderId) {

                    //if the order is confirmed, shiprocket_order_id and shipment_id is generated
                    if($cancelOrderId->child_order_id!=null && $cancelOrderId->shiprocket_order_id!=null && $cancelOrderId->shipment_id!=null){
                        // $ids[$i]=$cancelOrderId->shiprocket_order_id;

                        $response =  Shiprocket::order($token)->cancel([$cancelOrderId->shiprocket_order_id]);
                        $response = json_decode($response);
                        
                        if($response->status_code == 200){
                            __cancelOrderStatusUpdate($status, $cancelOrderId->id);
                        }
                        else if($response->status_code == 400){
                            $orderCancelledError[$i] = $cancelOrderId->child_order_id;
                        }
                    }

                    // if order yet to be confirmed / order confirmed, but before the order status is shipped(before AWB generated)
                    else if($cancelOrderId->child_order_id!=null && $cancelOrderId->shipping_status_id < 4){
                        __cancelOrderStatusUpdate($status, $cancelOrderId->id);
                    }

                    //unable to cancel the orders
                    else{
                        $orderCancelledError[$i] = $cancelOrderId->child_order_id;
                    }
                }

                if(empty($orderCancelledError)){
                    // return 1;
                    return json_encode(['status'=>1, 'message'=>'Selected orders cancelled successfully.', 'orderCancelledError'=>$orderCancelledError]);
                }else{
                    return json_encode(['status'=>0, 'message'=>'Oops.. Problem with your order cancellation, please contact support team.', 'orderCancelledError'=>$orderCancelledError]);
                }
                break;

            case '3': // Order Rejected by Seller
                OrderDetail::whereIn('id', $orderIds)->update(["order_status_id" => $status]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'order_status_id' => $status,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                    ];
                }
                OrderHistory::insert($data);

                /*
                ** send Email and SMS if Notify Email and SMS is set to "Y
                ** Getting Email and SMS based o status ID
                */
                $orderDetails = OrderDetail::with('order','seller', 'productVarient')->whereIn('id', $orderIds)->get();
                foreach ($orderDetails as $i=>$details) {
                    $to = [
                        'user_email'=>$details->order->email,
                        'seller_email'=>$details->seller->email
                    ];
                    $result = __sendEmails($to, 'order_rejected_by_seller' , $details);
                }
                return json_encode(['status'=>1, 'message'=>'Selected orders rejected.']);
                break;

            case '4': // Order Confirmed by Seller

                /**Order confirmed by Seller and the
                    * Seller Shipping Model is FABPIK,
                    * SHIPMENT ID is genereated by fabpik
                **/
                $invalidPickUpNames = [];

                foreach ($orderIds as $i=>$id) {

                    $getChildOrdDetails = OrderDetail::with(['productVarient', 'order.customer', 'seller.sellerWarehouses' => function($q){
                        $q->where('status', 1)->where('is_default',1);
                    }])->where('id', $id)->first();

                    if ($getChildOrdDetails->order_status_id == 1) {
                        if ($getChildOrdDetails->seller->shipping_model == 's') {
                            OrderDetail::where('child_order_id', '=', $getChildOrdDetails->child_order_id)
                                         ->update([
                                             'order_status_id' => $status,
                                             'shipping_status_id'=>1
                                         ]);

                            $data[$i]= [
                                 'order_status_id' => $status,
                                 'order_detail_id' => $id,
                                 'shipping_status_id' => 1,
                                 'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                             ];
                            OrderHistory::insert($data);
                        } elseif ($getChildOrdDetails->seller->shipping_model == 'f') {
                            if (empty($getChildOrdDetails->seller->sellerWarehouses[0])) {
                                $getChildOrdDetails = OrderDetail::with(['productVarient', 'order.customer', 'seller.sellerWarehouses' => function ($q) {
                                    $q->where('status', 1);
                                }])->where('id', $id)->first();
                            }

                            if (empty($getChildOrdDetails->seller->sellerWarehouses[0]) || $getChildOrdDetails->seller->sellerWarehouses[0]->shiprocket_pickup_nickname == null) {
                                $invalidPickUpNames[$i] = $getChildOrdDetails->child_order_id;
                            } else {
                                if ($getChildOrdDetails->seller->shipping_model == 'f') {
                                    $order_details = [
                                    'order_id' => $getChildOrdDetails->child_order_id,
                                    'order_date' => $getChildOrdDetails->created_at,
                                    'pickup_location' => $getChildOrdDetails->seller->sellerWarehouses[0]->shiprocket_pickup_nickname,
                                    'billing_customer_name' => $getChildOrdDetails->order->billing_first_name,
                                    'billing_last_name'=>$getChildOrdDetails->order->billing_last_name,
                                    'billing_address'=>$getChildOrdDetails->order->billing_address1.' '.$getChildOrdDetails->order->billing_address2,
                                    'billing_pincode'=>$getChildOrdDetails->order->billing_pincode,
                                    'billing_state'=> $getChildOrdDetails->order->billing_state,
                                    'billing_country'=>'India',
                                    'billing_email'=>$getChildOrdDetails->order->customer->email,
                                    'billing_phone'=>$getChildOrdDetails->order->billing_mobile,
                                    'shipping_is_billing'=>true,
                                    'shipping_customer_name'=>$getChildOrdDetails->order->shipping_first_name.' '.$getChildOrdDetails->order->shipping_last_name,
                                    'shipping_address'=>$getChildOrdDetails->order->shipping_address1.' '.$getChildOrdDetails->order->shipping_address2,
                                    'shipping_city'=>$getChildOrdDetails->order->shipping_city,
                                    'shipping_pincode'=>$getChildOrdDetails->order->shipping_pincode,
                                    'shipping_country'=>'India',
                                    'shipping_state'=>$getChildOrdDetails->order->shipping_state,
                                    'shipping_email'=>$getChildOrdDetails->order->customer->email,
                                    'shipping_phone'=>$getChildOrdDetails->order->shipping_mobile,
                                    'order_items'=>[[
                                            'name'=>$getChildOrdDetails->name,
                                            'sku'=>$getChildOrdDetails->productVarient->sku,
                                            'units'=>$getChildOrdDetails->quantity,
                                            'selling_price'=>$getChildOrdDetails->total_price,
                                            'discount'=>0
                                            ]
                                        ],
                                    'length'=>$getChildOrdDetails->shipping_length,
                                    'breadth'=>$getChildOrdDetails->shipping_breadth,
                                    'height'=>$getChildOrdDetails->shipping_height,
                                    'weight'=>$getChildOrdDetails->shipping_weight/1000,
                                    'payment_method'=>$getChildOrdDetails->order->payment_type == 'c' ? 'COD' : 'Prepaid',
                                    'sub_total'=>$getChildOrdDetails->total,
                                ];

                                    // dd($order_details);

                                    $shipRocketOrder = __generateShipmentId($order_details);
                                    $shipRocketData = json_decode($shipRocketOrder);
                                    // dd($shipRocketData);
                                    // return $shipRocketData;
                                    OrderDetail::where('child_order_id', '=', $getChildOrdDetails->child_order_id)
                                            ->update(['shipment_id'=>$shipRocketData->shipment_id,
                                                    'shiprocket_order_id'=>$shipRocketData->order_id,
                                                    'order_status_id' => $status,
                                                    'shipping_status_id'=>1]);
                                    $data[$i]= [
                                    'order_status_id' => $status,
                                    'order_detail_id' => $id,
                                    'shipping_status_id' => 1,
                                    'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                                ];
                                    OrderHistory::insert($data);
                                }
                            }
                        }
                    }
                }

                if(empty($invalidPickUpNames)){
                    // return 1;
                    return json_encode(['status'=>1, 'message'=>'Selected orders confirmed successfully.', 'invalidPickUpNames'=>$invalidPickUpNames]);
                }else{
                    return json_encode(['status'=>0, 'message'=>'Oops.. Problem with your warehouse details, please contact administrator.', 'invalidPickUpNames'=>$invalidPickUpNames]);
                }

                break;

            case '5': // Return Requested by User

                OrderDetail::whereIn('id', $orderIds)->update(["order_status_id" => $status,
                                                                "payment_status_id" => 3] );

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'order_status_id' => $status,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                    ];
                }
                OrderHistory::insert($data);

                /*
                ** send Email and SMS if Notify Email and SMS is set to "Y"
                ** Getting Email and SMS based o status ID
                */
                $notifyStatus = OrderDetail::getOrderStatusData($status);
                $orderDetails = OrderDetail::with('order','seller', 'productVarient', 'orderHistory')->whereIn('id', $orderIds)->get();
                foreach ($orderDetails as $i=>$details) {
                    $to = [
                        'user_email'=>$details->order->email,
                        'seller_email'=>$details->seller->email
                    ];
                    $result = __sendEmails($to, 'return_order' , $details);
                }
                return $result;
                break;

            case '6': // Return Approved by Admin

                OrderDetail::whereIn('id', $orderIds)->update(["order_status_id" => $status, 'shipping_status_id'=>9]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'order_status_id' => $status,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                    ];
                }
                OrderHistory::insert($data);

                /*
                ** send Email and SMS if Notify Email and SMS is set to "Y
                ** Getting Email and SMS based o status ID
                */
                $notifyStatus = OrderDetail::getOrderStatusData($status);
                $orderDetails = OrderDetail::with('order','seller', 'productVarient', 'orderHistory')->whereIn('id', $orderIds)->get();
                foreach ($orderDetails as $i=>$details) {
                    $to = [
                        'seller_email'=>$details->seller->email,
                        'user_email'=>$details->order->email
                    ];
                    $result = __sendEmails($to, 'return_approved_by_admin' , $details);
                }
                // return $result;
                return json_encode(['status'=>1, 'message'=>'Selected orders return approved.', 'errors'=>false]);
                break;

            case '7': // Seller Rejection Approved by Admin

                OrderDetail::whereIn('id', $orderIds)->update(["order_status_id" => $status,
                "payment_status_id" => 3]
                                                    );

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'order_status_id' => $status,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                    ];
                }
                OrderHistory::insert($data);

                /*
                ** send Email and SMS if Notify Email and SMS is set to "Y
                ** Getting Email and SMS based o status ID
                */
                $notifyStatus = OrderDetail::getOrderStatusData($status);
                $orderDetails = OrderDetail::with('order','seller', 'productVarient')->whereIn('id', $orderIds)->get();
                foreach ($orderDetails as $i=>$details) {
                    $to = [
                        'user_email'=>$details->order->email,
                        'seller_email'=>$details->seller->email
                    ];
                    $result = __sendEmails($to, 'seller_rejected_approved_by_admin' , $details);
                }
                // return $result;
                return json_encode(['status'=>1, 'message'=>'Selected Orders Rejection Approved by Admin.', 'errors'=>false]);
                break;

            case '8': // Delivered

                foreach($orderIds as $i=>$id){
                    OrderDetail::where('id', $id)->update([
                        "order_status_id" => $status,
                        "shipping_status_id" => 5,
                        "payment_status_id" => 1,
                        "invoice_no" => __generateInvoiceNo(),
                        "seller_invoice_no"=> __generateSellerInvoiceNo(),
                        "delivered_date" => date("Y-m-d")
                    ]);
                }

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'order_status_id' => $status,
                        'comment' => $remark,
                        'shipping_status_id' => 5,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                    ];
                }
                OrderHistory::insert($data);

                /*
                    **Checking for COD Orders
                    **If all the Child Orders Payment Status is set to 1 means paid
                    **Then Payment Status
                */

                foreach ($orderIds as $i=>$id) {
                    $orderCount = DB::select( DB::raw("SELECT count(id) as recordCount FROM `order_details` WHERE order_id IN (SELECT order_id FROM order_details WHERE id = :id) AND order_status_id < 8"), ['id' => $id, ]);
                    if($orderCount[0]->recordCount == 0){
                        $orderUpdate = DB::update( DB::raw( "UPDATE orders SET order_status_id = 8, payment_status_id = 1 WHERE id =  (SELECT order_id FROM order_details WHERE id=:id LIMIT 1) " ), [ 'id' => $id, ]);
                    }
                }

                /*
                ** send Email and SMS if Notify Email and SMS is set to "Y
                ** Getting Email and SMS based o status ID
                */
                $notifyStatus = OrderDetail::getOrderStatusData($status);
                $orderDetails = OrderDetail::with('order','seller', 'productVarient')->whereIn('id', $orderIds)->get();

                foreach ($orderDetails as $i=>$details) {
                    $to = [
                        'user_email'=>$details->order->email,
                        'seller_email'=>$details->seller->email
                    ];
                    $result = __sendEmails($to, 'order_delivered' , $details);
                }
                // return $result;
                return json_encode(['status'=>1, 'message'=>'Order status Changed to Delivered.', 'err'=>false]);
                break;

            case '9': // Completed Status - after 7 Days

                OrderDetail::whereIn('id', $orderIds)->update(["order_status_id" => $status]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'order_status_id' => $status,
                        'comment' => $remark,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                    ];
                }
                OrderHistory::insert($data);
                return json_encode(['status'=>1, 'message'=>'Order status Changed to Completed.', 'err'=>false]);
                break;

            case '10':
                $token =  Shiprocket::getToken();
                $orderDetails = OrderDetail::select('shipment_id', 'id')->whereIn('id', $orderIds)->get()->toArray();
                $shiprocketErrors = [];
                foreach ($orderDetails as $order) {
                    if ($order['shipment_id'] != null) {
                        $response =  Shiprocket::courier($token)->generateAWB(['shipment_id' => $order['shipment_id']]);
                        // $response =  Shiprocket::courier($token)->generateAWB(['shipment_id' => 102404436]);
                        //update AWB Number for all these orders.
                        $response = json_decode($response);

                        if(isset($response->status_code)) {
                            if(isset($response->message)){
                                return json_encode(['status'=>0, 'message'=>$response->message, 'errors'=>true]);
                            }else{
                                return json_encode(['status'=>0, 'message'=>'Error with ShipRocket', 'errors'=>true]);
                            }
                            break;
                        } else if ($response) {

                            if (isset($response->response->data->awb_code)) {

                                $order['tracking_no'] = $response->response->data->awb_code;
                                $order['courier_id'] = $response->response->data->courier_company_id;
                                $order['courier_name'] = $response->response->courier_name;
                                $order['shipping_status_id'] = 3;
                                $order['order_status_id'] = 10;

                                $updateOrderDetail = OrderDetail::where('id', $order['id'])->update([
                                                                                    'tracking_no' => $order['tracking_no'],
                                                                                    'courier_id' => $order['courier_id'],
                                                                                    'courier_name' => $order['courier_name'],
                                                                                    'shipping_status_id' => $order['shipping_status_id'],
                                                                                    'order_status_id' => $order['order_status_id']
                                                                                    ]);
                                //Store Statutes in Order History Table
                                OrderHistory::insert([
                                    'order_status_id' => $status,
                                    'shipping_status_id' => 3,
                                    'order_detail_id' => $order['id'],
                                    'order_id' => (OrderDetail::select('order_id')->where('id', $order['id'])->first())->order_id,
                                ]);

                                // Send Email to Seller - When the order is ready to ship
                                $orderDetail = OrderDetail::with('order', 'seller', 'productVarient')->where('id', $order['id'])->first();
                                $to = [
                                    'seller_email'=>$orderDetail->seller->email
                                ];
                                $result = __sendEmails($to, 'pickup_scheduled', $orderDetail);


                                //Request for Shipment Pickup
                                $pickupDetails = [ 'shipment_id' =>  $order['shipment_id'] ];
                                $requestPickupResponse =  Shiprocket::courier($token)->requestPickup($pickupDetails);
                                $requestPickupResponse = json_decode($requestPickupResponse);

                                if (isset($requestPickupResponse->status_code)) {
                                    if ( $requestPickupResponse->status_code == 400) {
                                            /**
                                             * if already Pickup in Queue Genereting the Manifest
                                             */

                                            if($requestPickupResponse->message == "Already in Pickup Queue."){

                                                /**
                                                *
                                                * Generate Manifest
                                                **/

                                                $shipmentIds = [ 'shipment_id' => [$order['shipment_id']] ];
                                                $manifestDetails = Shiprocket::generate($token)->manifest($shipmentIds);
                                                $manifestDetails = json_decode($manifestDetails);

                                                if (isset($manifestDetails->status_code)) {
                                                    if ($manifestDetails->status_code == 400) {
                                                        return json_encode(['status'=>0, 'message'=>$manifestDetails->message, 'errors'=>true]);
                                                    }
                                                    break;
                                                }

                                                $updateOrderDetail = OrderDetail::where('id', $order['id'])->update([ 'manifest_url' => $manifestDetails->manifest_url ]);
                                            }else{
                                                return json_encode(['status'=>0, 'message'=>$requestPickupResponse->message, 'errors'=>true]);
                                            }
                                    }
                                    break;
                                } else if(isset($requestPickupResponse->pickup_status)){

                                    if($requestPickupResponse->pickup_status == 1){
                                        $updatePickupScheduledDate = OrderDetail::where('id', $order['id'])->update(['pickup_scheduled_date' => date('Y-m-d H:i:s', strtotime($requestPickupResponse->response->pickup_scheduled_date))]);

                                        /**
                                            *
                                            * Generate Manifest
                                        **/

                                        $shipmentIds = [ 'shipment_id' => [$order['shipment_id']] ];
                                        $manifestDetails = Shiprocket::generate($token)->manifest($shipmentIds);
                                        $manifestDetails = json_decode($manifestDetails);

                                        if (isset($manifestDetails->status_code)) {
                                            if ($manifestDetails->status_code == 400) {
                                                return json_encode(['status'=>0, 'message'=>$manifestDetails->message, 'errors'=>true]);
                                            }
                                            break;
                                        }

                                        $updateOrderDetail = OrderDetail::where('id', $order['id'])->update([ 'manifest_url' => $manifestDetails->manifest_url ]);
                                    }
                                }


                            }
                        }
                    }else{
                        return json_encode(['status'=>0, 'message'=>'Shipment id is required for generating AWB number.', 'errors'=>true]);
                        break;
                    }
                }
                return json_encode(['status'=>1, 'message'=>'AWB Genrated succesfully for the selected orders.', 'errors'=>false]);
                break;

            default:
                # code...
                break;
        }
    }
}

//Cancel order satus update common function
if (!function_exists('__cancelOrderStatusUpdate')) {
    function __cancelOrderStatusUpdate($status, $cancelOrderId){
        if ($status!=null && $status!='' && $cancelOrderId!=null && $cancelOrderId!='') {

            //Change status Store in Order Details (Child Orders)
            OrderDetail::where('id', $cancelOrderId)->update(["order_status_id" => $status]);

            //Store Statutes in Order History Table
            $data = [
                'order_status_id' => $status,
                'order_detail_id' => $cancelOrderId,
                'order_id' => (OrderDetail::select('order_id')->where('id', $cancelOrderId)->first())->order_id,
            ];
            OrderHistory::insert($data);

            /*
                ** send Email and SMS if Notify Email and SMS is set to "Y"
                ** Getting Email and SMS based on status ID
            */
            $orderDetails = OrderDetail::with('order', 'seller', 'productVarient', 'orderHistory')->where('id', $cancelOrderId)->first();
            $to = [
                'user_email'=>$orderDetails->order->email,
                'seller_email'=>$orderDetails->seller->email
            ];
            __sendEmails($to, 'order_cancelled', $orderDetails);
        }
    }
}

// Ship Rocket Shipping Status Related Functions
if (!function_exists('__changeShipRocketShippingStatus')) {
    function __changeShipRocketShippingStatus($res, $orderIds = [])
    {
        $res = json_decode($res);
        if($res->tracking_data->track_status == 0){
            return json_encode(['status'=>0, 'message'=>$res->tracking_data->error, 'err'=>true]);
        }
        $status = $res->tracking_data->shipment_status;
        // $status = $res;
        // \Log::info(json_encode($res));
        $result = false;
        switch ($status) {
            case "3": // Pickup Scheduled/Generated
                //Change status and Store in Order Details (Child Orders)
                OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => 3]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {

                    $totalOrderDetails = OrderDetail::select('order_id')->where('id', $id)->first();
                    $orderHistoryData = OrderHistory::where('shipping_status_id', 3)
                                                ->where('order_detail_id', $id)
                                                ->where('order_id', $totalOrderDetails->order_id)
                                                ->first();

                    if (empty($orderHistoryData)) {
                        $data[$i]= [
                            'shipping_status_id' => 3,
                            'order_detail_id' => $id,
                            'order_id' => $totalOrderDetails->order_id,
                            'notify' => 1,
                        ];
                        OrderHistory::insert($data);
                    }
                }                

                return json_encode(['status'=>1, 'message'=>'Status Changed to Pickup Scheduled/Generated', 'err'=>false]);
                break;

            case "6": // Shipped
                
                //Change status Store in Order Details (Child Orders)
                OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => 4]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {

                    $totalOrderDetails = OrderDetail::with('order','seller', 'productVarient')->where('id', $id)->first();

                    $orderHistoryData = OrderHistory::where('shipping_status_id', 4)
                                                ->where('order_detail_id', $id)
                                                ->where('order_id', $totalOrderDetails->order_id)
                                                ->first();

                    if (empty($orderHistoryData)) {
                        $data[$i]= [
                            'shipping_status_id' => 4,
                            'order_detail_id' => $id,
                            'order_id' => $totalOrderDetails->order_id,
                            
                        ];
                        OrderHistory::insert($data);
                    }

                    if( $orderHistoryData->notify == 0){
                        $to = [
                            'user_email'=>$totalOrderDetails->order->email,
                            'seller_email'=>$totalOrderDetails->seller->email,
                        ];
                        $result = __sendEmails($to, 'order_shipped' , $totalOrderDetails);

                        OrderHistory::where('shipping_status_id', 4)
                                                ->where('order_detail_id', $id)
                                                ->where('order_id', $totalOrderDetails->order_id)
                                                ->update(['notify' => 1]);
                    }
                }                

                return json_encode(['status'=>1, 'message'=>'Status Changed to Shipped', 'err'=>false]);
                break;

            case "18": // In Transit
                    //Change status Store in Order Details (Child Orders)
                    OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => 4]);

                    //Store Statutes in Order History Table
                    foreach ($orderIds as $i=>$id) {

                        $totalOrderDetails = OrderDetail::with('order','seller', 'productVarient')->where('id', $id)->first();

                        $orderHistoryData = OrderHistory::where('shipping_status_id', 4)
                                                    ->where('order_detail_id', $id)
                                                    ->where('order_id', $totalOrderDetails->order_id)
                                                    ->first();

                        if (empty($orderHistoryData)) {
                            $data[$i]= [
                                'shipping_status_id' => 4,
                                'order_detail_id' => $id,
                                'order_id' => $totalOrderDetails->order_id,
                                'notify' => 1,
                            ];
                            OrderHistory::insert($data);
                        }
                    }                    

                    return json_encode(['status'=>1, 'message'=>'Status Changed to In Transit', 'err'=>false]);
                    break;

            case "7": // Delivered

                /*
                ** setting the all statuses in ORDER DETAILS Table
                ** Generate Invoice Number and store it in "invoice_no" column
                **
                */
                foreach ($orderIds as $i=>$id) {
                    //check the status is already updated or not
                    $isExists = OrderDetail::where('id', $id)
                                            ->where("order_status_id", 8)
                                            ->where("shipping_status_id", 5)
                                            ->where("payment_status_id", 1)
                                            ->whereNotNull("delivered_date")
                                            ->whereNotNull("invoice_no")
                                            ->whereNotNull("seller_invoice_no")
                                            ->exists();

                    if (!$isExists) {
                        //Update all Status in Order Detail Table
                        OrderDetail::where('id', $id)->update([
                            "order_status_id" => 8,
                            "shipping_status_id" => 5,
                            "payment_status_id" => 1,
                            "invoice_no" => __generateInvoiceNo(),
                            "seller_invoice_no"=> __generateSellerInvoiceNo(),
                            "delivered_date" => date("Y-m-d", strtotime($res->tracking_data->shipment_track[0]->delivered_date))
                        ]);
                    }
                    
                    $totalOrderDetails = OrderDetail::with('order','seller', 'productVarient')->where('id', $id)->first();

                    $orderHistoryData = OrderHistory::where('shipping_status_id', 5)
                                                    ->where('order_status_id', 8)
                                                    ->where('order_detail_id', $id)
                                                    ->where('order_id', $totalOrderDetails->order_id)
                                                    ->first();
                    
                    //Store Statutes in Order History Table
                    if(empty($orderHistoryData)){
                        $data[$i]= [
                            'order_status_id' => 8,
                            'shipping_status_id' => 5,
                            'order_detail_id' => $id,
                            'order_id' => $totalOrderDetails->order_id,
                        ];
                        OrderHistory::insert($data);
                    }
                    
                    /*
                    ** send Email and SMS if Notify Email and SMS is set to "Y
                    ** Getting Email and SMS based o status ID
                    */
                    if($orderHistoryData->notify == 0){
                        $to = [
                            'user_email'=>$totalOrderDetails->order->email,
                            'seller_email'=>$totalOrderDetails->seller->email
                        ];

                        $result = __sendEmails($to, 'order_delivered' , $totalOrderDetails);

                        OrderHistory::where('shipping_status_id', 5)
                                        ->where('order_status_id', 8)
                                        ->where('order_detail_id', $id)
                                        ->where('order_id', $totalOrderDetails->order_id)
                                        ->update(['notify' => 1]);

                    }                   
                }

                /*
                **Checking for COD Orders
                **If all the Child Orders Payment Status is set to 1 means paid
                **Then Payment Status
                */

                foreach ($orderIds as $i=>$id) {
                    $orderCount = DB::select( DB::raw("SELECT count(id) as recordCount FROM `order_details` WHERE order_id IN (SELECT order_id FROM order_details WHERE id = :id) AND order_status_id < 8"), ['id' => $id, ]);
                    if($orderCount[0]->recordCount == 0){
                        $orderUpdate = DB::update( DB::raw( "UPDATE orders SET order_status_id = 8 WHERE id =  (SELECT order_id FROM order_details WHERE id=:id LIMIT 1) " ), [ 'id' => $id, ]);
                    }
                }

                return json_encode(['status'=>1, 'message'=>'Status Changed to Delivered', 'err'=>false]);
                break;

            case "9": // RTO Initiated
                //Change status Store in Order Details (Child Orders)
                
                OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => 6,
                                                                "order_status_id" => 11
                                                            ]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $totalOrderDetails = OrderDetail::with('order','seller', 'productVarient')->where('id', $id)->first();

                    $orderHistoryData = OrderHistory::where('shipping_status_id', 6)
                                                    ->where('order_status_id', 11)
                                                    ->where('order_detail_id', $id)
                                                    ->where('order_id', $totalOrderDetails->order_id)
                                                    ->first();

                    if (empty($orderHistoryData)) {
                        $data[$i]= [
                            'shipping_status_id' => 6,
                            'order_status_id' => 11,
                            'order_detail_id' => $id,
                            'order_id' => $totalOrderDetails->order_id,
                            'notify' => 1,
                        ];
                        OrderHistory::insert($data);
                    }
                }                

                return json_encode(['status'=>1, 'message'=>'Status Changed to RTO Initiated', 'err'=>false]);
                break;

            case "10": // RTO Delivered
                
                //Change status Store in Order Details (Child Orders)
                OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => 7]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $totalOrderDetails = OrderDetail::with('order','seller', 'productVarient')->where('id', $id)->first();

                    $orderHistoryData = OrderHistory::where('shipping_status_id', 7)
                                                    ->where('order_detail_id', $id)
                                                    ->where('order_id', $totalOrderDetails->order_id)
                                                    ->first();

                    if (empty($orderHistoryData)) {
                        $data[$i]= [
                            'shipping_status_id' => 7,
                            'order_detail_id' => $id,
                            'order_id' => $totalOrderDetails->order_id,
                            'notify' => 1,
                        ];
                        OrderHistory::insert($data);
                    }
                }                

                return json_encode(['status'=>1, 'message'=>'Status Changed to RTO Delivered', 'err'=>false]);
                break;

            case "21": // Undelivered
                //Change status Store in Order Details (Child Orders)
                OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => 6,
                                                                "order_status_id" => 11
                                                            ]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {

                    $totalOrderDetails = OrderDetail::with('order','seller', 'productVarient')->where('id', $id)->first();

                    $orderHistoryData = OrderHistory::where('shipping_status_id', 6)
                                                    ->where('order_status_id', 11)
                                                    ->where('order_detail_id', $id)
                                                    ->where('order_id', $totalOrderDetails->order_id)
                                                    ->first();
                    if (empty($orderHistoryData)) {
                        $data[$i]= [
                            'shipping_status_id' => 6,
                            'order_status_id' => 11,
                            'order_detail_id' => $id,
                            'order_id' => $totalOrderDetails->order_id,
                        ];
                        OrderHistory::insert($data);
                    }

                    /*
                    ** send Email and SMS if Notify Email and SMS is set to "Y
                    ** Getting Email and SMS based o status ID
                    */
                    if($orderHistoryData->notify == 0){
                        $to = [
                            'user_email'=>$totalOrderDetails->order->email,
                            'seller_email'=>$totalOrderDetails->seller->email
                        ];

                        $result = _sendEmails($to, 'delivery_unsuccess' , $totalOrderDetails);

                        OrderHistory::where('shipping_status_id', 6)
                                        ->where('order_status_id', 11)
                                        ->where('order_detail_id', $id)
                                        ->where('order_id', $totalOrderDetails->order_id)
                                        ->update(['notify' => 1]);
                    }

                }

                return json_encode(['status'=>1, 'message'=>'Status Changed to Undelivered', 'err'=>false]);
                break;
            default:
                # code...
                break;
        }
        return $result;
    }
}

// Seller Shipping Status Related Functions
if (!function_exists('__changeSellerShippingStatus')) {
    function __changeSellerShippingStatus($status, $orderIds = [])
    {
        $result = false;
        switch ($status) {
            case '2': // change order shipping status to "ready to pickup" from seller end

                $token =  Shiprocket::getToken();
                $orderDetails = OrderDetail::whereIn('id', $orderIds)->get()->toArray();
                $shiprocketErrors = [];
                foreach ($orderDetails as $order) {

                    if($order['shipping_status_id']==1 && $order['order_status_id']==4){
                        if ($order['shipment_id'] != null) {

                            /**
                             * Generate AWB Number
                             *
                             * */

                            $response =  Shiprocket::courier($token)->generateAWB(['shipment_id' => $order['shipment_id']]);
                            $response = json_decode($response);

                            if (isset($response->status_code)) {
                                if (isset($response->message)) {
                                    return json_encode(['status'=>0, 'message'=>'Some issue occured, Please contact FabPik Administrator', 'errors'=>true]);
                                    // return json_encode(['status'=>0, 'message'=>$response->message.' - AWB', 'errors'=>true]);
                                }
                                break;
                            } elseif ($response) {
                                if (isset($response->response->data->awb_code)) {

                                    //Change the Order status to "ready to pickup"
                                    OrderDetail::where('id', $order['id'])->update(["shipping_status_id" => $status]);

                                    //Store Statutes in Order History Table
                                    OrderHistory::insert([
                                        'order_status_id' => 4,
                                        'shipping_status_id' => 3,
                                        'order_detail_id' => $order['id'],
                                        'order_id' => (OrderDetail::select('order_id')->where('id', $order['id'])->first())->order_id,
                                    ]);

                                    /**
                                     * Store the AWB number into DB and change the Status of the Order
                                     */

                                    $order['tracking_no'] = $response->response->data->awb_code;
                                    $order['shipping_status_id'] = 3;
                                    $order['order_status_id'] = 10;

                                    $updateOrderDetail = OrderDetail::where('id', $order['id'])->update(['tracking_no' => $order['tracking_no'],
                                                                                        'shipping_status_id' => $order['shipping_status_id'],
                                                                                        'order_status_id' => $order['order_status_id']
                                                                                        ]);

                                    //Store Statutes in Order History Table
                                    OrderHistory::insert([
                                        'order_status_id' => 10,
                                        'shipping_status_id' => 3,
                                        'order_detail_id' => $order['id'],
                                        'order_id' => (OrderDetail::select('order_id')->where('id', $order['id'])->first())->order_id,
                                    ]);

                                    // Send Email to Seller - When the order is ready to ship
                                    $orderDetail = OrderDetail::with('order', 'seller', 'productVarient')->where('id', $order['id'])->first();
                                    $to = [
                                        'seller_email'=>$orderDetail->seller->email,
                                        'user_email'=>$orderDetail->order->email,
                                    ];
                                    $result = __sendEmails($to, 'pickup_scheduled', $orderDetail);


                                    /**
                                     *
                                     * Request for Shipment Pickup and Generate Manifest
                                     **/
                                    $pickupDetails = [ 'shipment_id' =>  $order['shipment_id'] ];
                                    $requestPickupResponse =  Shiprocket::courier($token)->requestPickup($pickupDetails);
                                    $requestPickupResponse = json_decode($requestPickupResponse);
                                    if (isset($requestPickupResponse->status_code)) {
                                        if ($requestPickupResponse->status_code == 400) {

                                            /**
                                             * if already Pickup in Queue Genereting the Manifest
                                             */

                                            if($requestPickupResponse->message == "Already in Pickup Queue."){

                                                /**
                                                *
                                                * Generate Manifest
                                                **/

                                                $shipmentIds = [ 'shipment_id' => [$order['shipment_id']] ];
                                                $manifestDetails = Shiprocket::generate($token)->manifest($shipmentIds);
                                                $manifestDetails = json_decode($manifestDetails);

                                                if (isset($manifestDetails->status_code)) {
                                                    if ($manifestDetails->status_code == 400) {
                                                        return json_encode(['status'=>0, 'message'=>'Some issue occured, Please contact FabPik Administrator', 'errors'=>true]);
                                                        // return json_encode(['status'=>0, 'message'=>$manifestDetails->message.' - Generate Manifest', 'errors'=>true]);
                                                    }
                                                    break;
                                                }

                                                $updateOrderDetail = OrderDetail::where('id', $order['id'])->update([
                                                                                                    'manifest_url' => $manifestDetails->manifest_url
                                                                                                    ]);
                                            }
                                            else{
                                                return json_encode(['status'=>0, 'message'=>'Some issue occured, Please contact FabPik Administrator', 'errors'=>true]);
                                                // return json_encode(['status'=>0, 'message'=>$requestPickupResponse->message.' - Request for Shipment ', 'errors'=>true]);
                                            }

                                        }
                                        break;
                                    }else if(isset($requestPickupResponse->pickup_status)){

                                        if($requestPickupResponse->pickup_status == 1){

                                            $updatePickupScheduledDate = OrderDetail::where('id', $order['id'])->update(['pickup_scheduled_date' => date('Y-m-d H:i:s', strtotime($requestPickupResponse->response->pickup_scheduled_date))]);

                                            /**
                                                *
                                                * Generate Manifest
                                            **/

                                            $shipmentIds = [ 'shipment_id' => [$order['shipment_id']] ];
                                            $manifestDetails = Shiprocket::generate($token)->manifest($shipmentIds);
                                            $manifestDetails = json_decode($manifestDetails);

                                            if (isset($manifestDetails->status_code)) {
                                                if ($manifestDetails->status_code == 400) {
                                                    return json_encode(['status'=>0, 'message'=>'Some issue occured, Please contact FabPik Administrator', 'errors'=>true]);
                                                    // return json_encode(['status'=>0, 'message'=>$manifestDetails->message.' - Generate Manifest', 'errors'=>true]);
                                                }
                                                break;
                                            }

                                            $updateOrderDetail = OrderDetail::where('id', $order['id'])->update([
                                                                                                'manifest_url' => $manifestDetails->manifest_url
                                                                                                ]);

                                        }
                                    }

                                }else {
                                    return json_encode(['status'=>0, 'message'=>'Problem with your Order, Please try again once or contact Administrator', 'errors'=>true]);
                                }
                            }
                        } else {
                            return json_encode(['status'=>0, 'message'=>'Problem with your Order, Please contact Administrator', 'errors'=>true]);
                        }
                    }else{
                        return json_encode(['status'=>0, 'message'=>'Please check the order before change the status to Ready For Pickup', 'errors'=>true]);
                    }
                }

                return json_encode(['status'=>1, 'message'=>'Selected orders pickup scheduled Successfully', 'errors'=>false]);
                break;
            case "7": // Undelivered Product Returned
                //Change status Store in Order Details (Child Orders)
                OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => $status,
                                                                "order_status_id" => 11,
                                                                "payment_status_id" => 3]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'shipping_status_id' => $status,
                        'order_status_id' => 11,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                    ];
                }
                OrderHistory::insert($data);

                /*
                ** send Email and SMS if Notify Email and SMS is set to "Y
                ** Getting Email and SMS based o status ID
                */
                return json_encode(['status'=>1, 'message'=>'Selected orders pickup scheduled Successfully', 'errors'=>false]);
                break;

            case "12" : // Product Return confirmed
                //Change status Store in Order Details (Child Orders)
                OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => $status,
                                                                "order_status_id" => 12,
                                                                "payment_status_id" => 3]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'shipping_status_id' => $status,
                        'order_status_id' => 12,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                    ];
                }
                OrderHistory::insert($data);

                /*
                ** send Email and SMS if Notify Email and SMS is set to "Y
                ** Getting Email and SMS based o status ID
                */
                $orderDetails = OrderDetail::with('order', 'seller',  'productVarient')->whereIn('id', $orderIds)->get();
                foreach ($orderDetails as $i=>$details) {
                    $to = [
                        'seller_email'=>$details->seller->email
                    ];
                    $result = __sendEmails($to, 'product_return_confirmed' , $details);
                }
                return json_encode(['status'=>1, 'message'=>'Thanks for your confirmation.', 'errors'=>false]);
                break;
            case "8": // Undelivered Product Returned confirmed by Seller
                OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => $status,
                                                                "order_status_id" => 11,]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'shipping_status_id' => $status,
                        'order_status_id' => 11,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                        ];
                    }
                OrderHistory::insert($data);
                $orderDetails = OrderDetail::with('order', 'seller', 'productVarient')->whereIn('id', $orderIds)->get();
                foreach ($orderDetails as $i=>$details) {
                    $to = [
                        'seller_email'=>$details->seller->email
                    ];
                    $result = __sendEmails($to, 'undelivered_product_return_confirmed' , $details);
                }
                return json_encode(['status'=>1, 'message'=>'Thanks for your confirmation.', 'errors'=>false]);
                break;



            /**
            *   The below cases only for sellers who have an shipping model an "SELF"
            */

            case "3": // Pickup Scheduled
                break;
            case "4": // Shipped
                OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => $status]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'shipping_status_id' => $status,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                    ];
                }
                OrderHistory::insert($data);

                // Send Email to Seller
                $orderDetails = OrderDetail::with('order', 'seller',  'productVarient')->whereIn('id', $orderIds)->get();
                foreach ($orderDetails as $i=>$details) {
                        $to = [
                            'user_email'=>$details->order->email,
                            'seller_email'=>$details->seller->email
                        ];
                        $result = __sendEmails($to, 'order_shipped' , $details);
                }
                return json_encode(['status'=>1, 'message'=>'Selected orders pickup scheduled Successfully', 'errors'=>false]);
                break;
            case "5": // Delivered
                OrderDetail::whereIn('id', $orderIds)->update([
                                                    "order_status_id" => 8,
                                                    "shipping_status_id" => 5,
                                                    "payment_status_id" => 1,
                                                    "invoice_no" => __generateInvoiceNo(),
                                                    "seller_invoice_no"=> __generateSellerInvoiceNo(),
                                                    "delivered_date" => date("Y-m-d")
                                                ]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'order_status_id' => 8,
                        'shipping_status_id' => 5,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                    ];
                }
                OrderHistory::insert($data);

                /*
                **Checking for COD Orders
                **If all the Child Orders Payment Status is set to 1 means paid
                **Then Payment Status
                */

                foreach ($orderIds as $i=>$id) {
                    $orderCount = DB::select( DB::raw("SELECT count(id) as recordCount FROM `order_details` WHERE order_id IN (SELECT order_id FROM order_details WHERE id = :id) AND order_status_id < 8"), ['id' => $id, ]);
                    if($orderCount[0]->recordCount == 0){
                        $orderUpdate = DB::update( DB::raw( "UPDATE orders SET order_status_id = 8 WHERE id =  (SELECT order_id FROM order_details WHERE id=:id LIMIT 1) " ), [ 'id' => $id, ]);
                    }
                }

                /*
                ** send Email and SMS if Notify Email and SMS is set to "Y
                ** Getting Email and SMS based o status ID
                */
                $orderDetails = OrderDetail::with('order','seller', 'productVarient')->whereIn('id', $orderIds)->get();
                foreach ($orderDetails as $i=>$details) {
                    $to = [
                        'user_email'=>$details->order->email,
                        'seller_email'=>$details->seller->email
                    ];
                    $result = __sendEmails($to, 'order_delivered' , $details);
                }
                return json_encode(['status'=>1, 'message'=>'Selected orders pickup scheduled Successfully', 'errors'=>false]);
                break;
            case "6": // Delivery Unsuccessful
                //Change status Store in Order Details (Child Orders)
                OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => 6,"order_status_id" => 11,]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'shipping_status_id' => 6,
                        'order_status_id' => 11,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                    ];
                }
                OrderHistory::insert($data);

                /*
                ** send Email and SMS if Notify Email and SMS is set to "Y
                ** Getting Email and SMS based o status ID
                */
                $orderDetails = OrderDetail::with('order','seller', 'productVarient')->whereIn('id', $orderIds)->get();
                foreach ($orderDetails as $i=>$details) {
                    $to = [
                        'user_email'=>$details->order->email,
                        'seller_email'=>$details->seller->email
                    ];
                    $result = __sendEmails($to, 'delivery_unsuccess' , $details);
                }
                break;
            case "9": // Return - Schedule Return Pickup
                break;
            case "10": // Return Pickup Initiated
                //Change status Store in Order Details (Child Orders)
                OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => $status]);

                //Store Statutes in Order History Table
                foreach ($orderIds as $i=>$id) {
                    $data[$i]= [
                        'shipping_status_id' => $status,
                        'order_detail_id' => $id,
                        'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                        ];
                }
                OrderHistory::insert($data);
                return json_encode(['status'=>1, 'message'=>'Selected orders pickup scheduled Successfully', 'errors'=>false]);
                break;
            case "11": // Product Returned
                 //Change status Store in Order Details (Child Orders)
                 OrderDetail::whereIn('id', $orderIds)->update([
                                                                "shipping_status_id" => $status,
                                                                "order_status_id" => 12
                                                            ]);

                    //Store Statutes in Order History Table
                    foreach ($orderIds as $i=>$id) {
                        $data[$i]= [
                            'shipping_status_id' => $status,
                            'order_status_id' => 12,
                            'order_detail_id' => $id,
                            'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                            ];
                    }
                    OrderHistory::insert($data);
                    return json_encode(['status'=>1, 'message'=>'Selected orders pickup scheduled Successfully', 'errors'=>false]);
                break;

            default:
                # code...
                break;
        }
    }
}

// Admin Shipping Status Related Functions
if (!function_exists('__changeAdminShippingStatus')) {
    function __changeAdminShippingStatus($status, $orderIds = [])
    {
        $result = false;
        switch ($status) {
            case '10': // change order shipping status to "Return Pickup Initiated" from Admin end
                $invalidPickUpNames = [];
                foreach ($orderIds as $i=>$id) {
                    $getChildOrdDetails = OrderDetail::with(['productVarient', 'order.customer', 'seller.sellerWarehouses' => function($q){
                        $q->where('status', 1)->where('is_default',1);
                    }])->where('id', $id)->first();


                    if (empty($getChildOrdDetails->seller->sellerWarehouses[0])) {
                        $getChildOrdDetails = OrderDetail::with(['productVarient', 'order.customer', 'seller.sellerWarehouses' => function($q){
                            $q->where('status', 1);
                        }])->where('id', $id)->first();
                    }

                    if(empty($getChildOrdDetails->seller->sellerWarehouses[0]) || $getChildOrdDetails->seller->sellerWarehouses[0]->shiprocket_pickup_nickname == null){
                        $invalidPickUpNames[$i] = $getChildOrdDetails->child_order_id;
                    } else{
                        if ($getChildOrdDetails->seller->shipping_model == 'f') {
                            $order_details = [
                                'order_id' => $getChildOrdDetails->child_order_id,
                                'order_date' => $getChildOrdDetails->created_at,
                                'channel_id' => '',
                                'pickup_customer_name' => $getChildOrdDetails->order->shipping_first_name.' '.$getChildOrdDetails->order->shipping_last_name,
                                'pickup_address' => $getChildOrdDetails->order->shipping_address1.' '.$getChildOrdDetails->order->shipping_address2,
                                'pickup_city' => $getChildOrdDetails->order->shipping_city,
                                'pickup_state' => $getChildOrdDetails->order->shipping_state,
                                'pickup_country' => 'India',
                                'pickup_pincode' => $getChildOrdDetails->order->shipping_pincode,
                                'pickup_email' => $getChildOrdDetails->order->customer->email,
                                'pickup_phone' => $getChildOrdDetails->order->shipping_mobile,
                                'pickup_location_id' => '',

                                'shipping_customer_name'=>$getChildOrdDetails->seller->name,
                                'shipping_address'=>$getChildOrdDetails->seller->sellerWarehouses[0]->address,
                                'shipping_city'=>$getChildOrdDetails->seller->sellerWarehouses[0]->city,
                                'shipping_pincode'=>$getChildOrdDetails->seller->sellerWarehouses[0]->pincode,
                                'shipping_country'=>'India',
                                'shipping_state'=>(State::find($getChildOrdDetails->seller->sellerWarehouses[0]->state_id))->name,
                                'shipping_email'=>$getChildOrdDetails->seller->email,
                                'shipping_phone'=>$getChildOrdDetails->seller->mobile,

                                'pickup_location' => $getChildOrdDetails->seller->sellerWarehouses[0]->shiprocket_pickup_nickname,

                                'order_items'=>[[
                                   'name'=>$getChildOrdDetails->name,
                                   'sku'=>$getChildOrdDetails->productVarient->sku,
                                   'units'=>$getChildOrdDetails->quantity,
                                   'selling_price'=>$getChildOrdDetails->total_price,
                                   'discount'=>0
                                    ]
                                ],
                                'length'=>$getChildOrdDetails->shipping_length,
                                'breadth'=>$getChildOrdDetails->shipping_breadth,
                                'height'=>$getChildOrdDetails->shipping_height,
                                'weight'=>$getChildOrdDetails->shipping_weight/1000,
                                'payment_method'=>$getChildOrdDetails->order->payment_type == 'c' ? 'COD' : 'Prepaid',
                                'sub_total'=>$getChildOrdDetails->total,
                            ];

                            // return json_decode($result, true);

                            OrderDetail::where('child_order_id','=',$getChildOrdDetails->child_order_id)
                                        ->update([
                                                'order_status_id' => $status,
                                                'shipping_status_id'=>1
                                            ]);
                            $data[$i]= [
                                'order_status_id' => $status,
                                'order_detail_id' => $id,
                                'shipping_status_id' => 1,
                                'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                            ];
                            OrderHistory::insert($data);

                        }else if ($getChildOrdDetails->seller->shipping_model == 's') {

                            // OrderDetail::where('child_order_id','=',$getChildOrdDetails->child_order_id)
                            //             ->update([
                            //                 'order_status_id' => $status,
                            //                 'shipping_status_id'=>1
                            //             ]);

                            // $data[$i]= [
                            //     'order_status_id' => $status,
                            //     'order_detail_id' => $id,
                            //     'shipping_status_id' => 1,
                            //     'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                            // ];
                            // OrderHistory::insert($data);
                        }
                    }


                }

                  //Change status Store in Order Details (Child Orders)
                  OrderDetail::whereIn('id', $orderIds)->update(["shipping_status_id" => 10]);

                  //Store Statutes in Order History Table
                  foreach ($orderIds as $i=>$id) {
                      $data[$i]= [
                          'shipping_status_id' => 10,
                          'order_detail_id' => $id,
                          'order_id' => (OrderDetail::select('order_id')->where('id', $id)->first())->order_id,
                      ];
                  }
                  OrderHistory::insert($data);
                break;
            default:
                # code...
                break;
        }
    }
}

// Payment Status Related Functions
if (!function_exists('__changePaymentStatus')) {
    function __changePaymentStatus($status, $orderIds = [])
    {
        switch ($status) {

            case '4': //if the Payment is Online Payment status is set to "Refund Done"
                //Change status Store in Order Details (Child Orders)
                OrderDetail::whereIn('id', $orderIds)->update(["payment_status_id" => $status]);
                //change the Status in Order (Parent Orders) set to Payment Status Id to "Refund Done"
                foreach ($orderIds as $i=>$id) {
                    $orderUpdate = DB::update(DB::raw("UPDATE orders SET payment_status_id = 4 WHERE id =  (SELECT order_id FROM order_details WHERE id=:id LIMIT 1) "), [ 'id' => $id, ]);
                }
                break;
            default:
                # code...
                break;
            return $result;
        }
    }
}

if (!function_exists('__getShipRocketOrderStatus')) {
    function __getShipRocketOrderStatus($trackingNo)
    {
        try{
            $token =  Shiprocket::getToken();
            $response =  Shiprocket::track($token)->throughAwb($trackingNo);
            $response = json_decode($response);
            // return $response->tracking_data->shipment_status;
            return json_encode($response);
        } catch (\Exception $e) {
            dd($e);
            // something else went wrong
            // plese report if this happens :)
        }

    }
}

if (!function_exists('__generateShipmentId')) {
    function __generateShipmentId($orderDetails)
    {
        // return $orderDetails;
        $token =  Shiprocket::getToken();
        $response =  Shiprocket::order($token)->create($orderDetails);
        return $response;
    }
}

/**
 * Pickup-Addresses
 * Add a New Pickup Location
 */
if(!function_exists('__addNewPickupLocation')){
    function __addNewPickupLocation( $getLocationDetails, $seller_id ){
        //get the seller required details to add the new pickup location in shiprocket.
        $sellerDetails = Seller::find($seller_id);
        $stateName = State::find($getLocationDetails['state_id']);

        $wareHouseCount = SellerWarehouses::withTrashed()->where('seller_id', $seller_id)->count();
        
        $pickup_location_name = $sellerDetails->seller_code.'-'.($wareHouseCount+1);        
        
        $newLocation = [
            "pickup_location" => $pickup_location_name,
	        "name" => $getLocationDetails['name'],
	        "email" => $sellerDetails->email,
	        "phone" => $getLocationDetails['phone'],
	        "address" => $getLocationDetails['address'],
	        "address_2" => ($getLocationDetails['address_two'] == null ) ? '' : $getLocationDetails['address_two'],
	        "city" => $getLocationDetails['city'],
	        "state" => $stateName->name,
	        "country" => 'India',
	        "pin_code" => $getLocationDetails['pincode'],
        ];

        if (env('APP_ENV') == 'production') {
            $token =  Shiprocket::getToken();
            $location = Shiprocket::pickup($token)->addLocation($newLocation);
            $result = json_decode($location);

            if (isset($result->status_code)) {
                if ($result->status_code == 422) {
                    return json_encode([ 'status'=>false, 'result'=>$result, 'error'=>true ]);
                }
            } elseif (isset($result->success)) {
                if ($result->success == true) {
                    return json_encode([ 'status'=>true, 'result'=>$result, 'error'=>false ]);
                }
            }
        }else{

            $obj = (object) [
                'status' => true,
                'address' => (object) [
                            'pickup_code' => $pickup_location_name
                        ],
            ];
            
            $result = json_encode($obj);

            return json_encode([ 'status'=>true, 'result'=>$obj, 'error'=>false ]);
        }
    }
}

/**
 * Get All Pickup Locations
 */

if(!function_exists('__getAllPickupLocations')){
    function __getAllPickupLocations(){
        $token =  Shiprocket::getToken();
        $locations = Shiprocket::pickup($token)->getLocations();
        return $locations;
    }
}

if (!function_exists('__getVariantOptions')) {
    function __getVariantOptions($id) // product_variant_id as parameter
    {
        $primaryOptions = $secondaryOptions = [];
        $primaryOptionValName = null;

        // get product id, primary attr and secondary attr
        // SELECT pv.product_id, c.primary_attribute, c.secondary_attribute FROM `product_variants` as pv JOIN products as p on p.id=pv.product_id JOIN categories as c on c.id=p.primary_category WHERE pv.id=1
        $primaryData = ProductVariant::selectRaw("product_variants.product_id, c.primary_attribute, c.secondary_attribute, pa.display_name as primary_attribute_name, sa.display_name as secondary_attribute_name")
                        ->join('products as p','p.id','=','product_variants.product_id')
                        ->join('categories as c','c.id','=','p.primary_category')
                        ->join('attributes as pa','c.primary_attribute','=','pa.id')
                        ->leftjoin('attributes as sa','c.secondary_attribute','=','sa.id')
                        ->where('product_variants.id', $id)
                        ->where('p.status', 1)
                        ->whereNull('p.deleted_at')
                        ->where('c.status', 1)
                        ->whereNull('c.deleted_at')
                        ->first();

        if($primaryData) {
            // get primary option value name
            $primaryOption = ProductVariantOption::selectRaw("ao.option_name")
                                ->join('attribute_options as ao','ao.id','=','product_variant_options.attribute_option_id')
                                ->where('product_variant_options.product_variant_id', $id)
                                ->where('product_variant_options.attribute_id', $primaryData->primary_attribute)
                                ->where('ao.status', 1)
                                ->whereNull('ao.deleted_at')
                                ->first();
            $primaryOptionValName = $primaryOption->option_name;

            // get all primary options
            // SELECT pvo.product_variant_id, pv.unique_id, pv.slug, pv.stock, pvo.attribute_option_id, ao.option_name, ao.colour_code FROM `product_variant_options` as pvo JOIN product_variants as pv on pv.id=pvo.product_variant_id JOIN attribute_options as ao on ao.id=pvo.attribute_option_id where pv.product_id=1 and pvo.attribute_id=1 and ao.status=1 and ao.deleted_at IS NULL AND pvo.deleted_at IS NULL AND pv.deleted_at IS NULL
            $primaryOptions = ProductVariantOption::selectRaw("product_variant_options.product_variant_id, pv.unique_id, pv.slug, pv.stock, product_variant_options.attribute_option_id, ao.option_name, ao.colour_code")
                                ->join('product_variants as pv','pv.id','=','product_variant_options.product_variant_id')
                                ->join('attribute_options as ao','ao.id','=','product_variant_options.attribute_option_id')
                                ->where('pv.product_id', $primaryData->product_id)
                                ->where('product_variant_options.attribute_id', $primaryData->primary_attribute)
                                ->where('ao.status', 1)
                                ->whereNull('ao.deleted_at')
                                ->whereNull('pv.deleted_at')
                                ->groupBy('product_variant_options.attribute_id','product_variant_options.attribute_option_id')
                                ->get();

            if(!is_null($primaryData->secondary_attribute)) {
                // get product variant primary option value
                // SELECT attribute_option_id FROM `product_variant_options` WHERE product_variant_id=1 and attribute_id=1
                $prmOptionVal = ProductVariantOption::selectRaw('attribute_option_id')
                                    ->where('product_variant_id', $id)
                                    ->where('attribute_id', $primaryData->primary_attribute)
                                    ->first();

                // selected color variants
                // SELECT pvo.product_variant_id FROM `product_variant_options` as pvo JOIN product_variants as pv on pv.id=pvo.product_variant_id JOIN attribute_options as ao on ao.id=pvo.attribute_option_id where pv.product_id=1 and pvo.attribute_option_id=2
                $selPrimaryAttrVariants = ProductVariantOption::join('product_variants as pv','pv.id','=','product_variant_options.product_variant_id')
                                    ->join('attribute_options as ao','ao.id','=','product_variant_options.attribute_option_id')
                                    ->where('pv.product_id', $primaryData->product_id)
                                    ->where('product_variant_options.attribute_option_id', $prmOptionVal->attribute_option_id)
                                    ->where('ao.status', 1)
                                    ->whereNull('ao.deleted_at')
                                    ->whereNull('pv.deleted_at')
                                    ->pluck('product_variant_options.product_variant_id');

                // get all secondary options according to the product variant's primary option value
                // SELECT pvo.product_variant_id, pv.unique_id, pv.slug, pv.stock, pvo.attribute_option_id, ao.option_name, ao.colour_code FROM `product_variant_options` as pvo JOIN product_variants as pv on pv.id=pvo.product_variant_id JOIN attribute_options as ao on ao.id=pvo.attribute_option_id where pv.product_id=1 and pvo.attribute_id=2 and ao.status=1 and ao.deleted_at IS NULL AND pvo.deleted_at IS NULL AND pv.deleted_at IS NULL AND pvo.product_variant_id IN(SELECT pvo.product_variant_id FROM `product_variant_options` as pvo JOIN product_variants as pv on pv.id=pvo.product_variant_id JOIN attribute_options as ao on ao.id=pvo.attribute_option_id where pv.product_id=1 and pvo.attribute_option_id=2)
                $secondaryOptions = ProductVariantOption::selectRaw("product_variant_options.product_variant_id, pv.unique_id, pv.slug, pv.stock, product_variant_options.attribute_option_id, ao.option_name, ao.colour_code")
                                        ->join('product_variants as pv','pv.id','=','product_variant_options.product_variant_id')
                                        ->join('attribute_options as ao','ao.id','=','product_variant_options.attribute_option_id')
                                        ->where('pv.product_id', $primaryData->product_id)
                                        ->where('product_variant_options.attribute_id', $primaryData->secondary_attribute)
                                        ->whereIn('product_variant_options.product_variant_id', $selPrimaryAttrVariants)
                                        ->where('ao.status', 1)
                                        ->whereNull('ao.deleted_at')
                                        ->whereNull('pv.deleted_at')
                                        ->get();

            }
        }

        return ['selectedPrimaryAttr'=>$primaryOptionValName,'primaryAttributeName'=>$primaryData->primary_attribute_name,'secondaryAttributeName'=>$primaryData->secondary_attribute_name,'primaryOptions'=>$primaryOptions,'secondaryOptions'=>$secondaryOptions];
    }
}

if (!function_exists('__getSellerCode')) {
    function __getSellerCode($id) // Seller Primary id as a param
    {
        $seller_code = DB::table('sellers')->where('id', $id)->select('seller_code')->first();
        return $seller_code->seller_code;
    }
}

//get the product variant primary and secondary attribute values
if (!function_exists('__getVariantOptionValues')) {
    function __getVariantOptionValues($id) // product_variant_id as parameter
    {
        $productVarientOptions = ProductVariantOption::where('product_variant_id', $id)->get();
        $attributes = [];
        foreach ($productVarientOptions as $i=>$options) {
            $getAttributes = Attribute::with(['attributeOptions' => function ($q) use ($options) {
                $q->where('id', '=', $options->attribute_option_id);
            }])->where('id', $options->attribute_id)->first();
            // $attributes[$i]['atrributeName'] = $getAttributes->display_name;
            $attributes[$i]['atrributeValue'] = isset($getAttributes->attributeOptions[0])?$getAttributes->attributeOptions[0]->option_name:'';
        }
        return ['PrimaryAttrValue'=>(isset($attributes[0])?$attributes[0]['atrributeValue']:''), 'SecondaryAttrValue'=>(isset($attributes[1])?$attributes[1]['atrributeValue']:'')];
    }
}

//get the product variant primary and secondary attribute values and names
if (!function_exists('__getVariantOptionNameValues')) {
    function __getVariantOptionNameValues($id) // product_variant_id as parameter
    {
        $productVarientOptions = ProductVariantOption::where('product_variant_id', $id)->get();
        $attributes = [];
        foreach ($productVarientOptions as $i=>$options) {
            $getAttributes = Attribute::with(['attributeOptions' => function ($q) use ($options) {
                $q->where('id', '=', $options->attribute_option_id);
            }])->where('id', $options->attribute_id)->first();
            $attributes[$i]['atrributeName'] = $getAttributes->display_name;
            $attributes[$i]['atrributeValue'] = isset($getAttributes->attributeOptions[0])?$getAttributes->attributeOptions[0]->option_name:'';
        }

        $result = array(
            'PrimaryAttr'=>[
                'value'=>(isset($attributes[0])?$attributes[0]['atrributeValue']:''),
                'name'=>(isset($attributes[0])?$attributes[0]['atrributeName']:'')
            ],
            'SecondaryAttr'=>[
                'value'=>(isset($attributes[1])?$attributes[1]['atrributeValue']:''),
                'name'=>(isset($attributes[1])?$attributes[1]['atrributeName']:'')
            ]
        );

        return $result;
    }
}

//get the product variant Images
if (!function_exists('__getVariantImages')) {
    function __getVariantImages($id) // product_variant_id as parameter
    {
        $productVarientImages = DB::table('view_product_images')->select("images", "thumbnail")->where('product_variant_id', $id)->first();

        $image = ($productVarientImages != null)?(json_decode($productVarientImages->images)):null;
        return ($image)?( ($productVarientImages->thumbnail)?$image[$productVarientImages->thumbnail - 1]:$image[0] ):'';
    }
}

//Modify address in Shiprocket
if (!function_exists('__modifyOrderShippingAddress')) {
    function __modifyOrderShippingAddress($params=[]) // Postfileds as parameter
    {
        $token =  Shiprocket::getToken();
        $endpoint = 'external/orders/address/update';
        $requestType = 'POST';
        $postfields = '{
            "order_id": '.$params['order_id'].',
            "shipping_customer_name": "'.$params['shipping_customer_name'].'",
            "shipping_phone": "'.$params['shipping_phone'].'",
            "shipping_address": "'.$params['shipping_address'].'",
            "shipping_address_2": "'.$params['shipping_address_2'].'",
            "shipping_city": "'.$params['shipping_city'].'",
            "shipping_state": "'.$params['shipping_state'].'",
            "shipping_country": "India",
            "shipping_pincode": "'.$params['shipping_pincode'].'"
          }';

        $result = __ShiprocketApiEndPointCalls($token, $endpoint, $requestType, $postfields);

        return json_decode($result, true);
    }
}

/**
 * Shiprocket API Endpoint API Call
 * Params are token, endpoint, postfields
 *
 */

if (!function_exists('__ShiprocketApiEndPointCalls')) {
    function __ShiprocketApiEndPointCalls($token, $endpoint, $requestType, $params){

        $apicall = 'https://apiv2.shiprocket.in/v1/';
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $apicall.$endpoint ,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $requestType,
        CURLOPT_POSTFIELDS =>$params,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$token,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }
}


## RANDOM PASSWORD
if (!function_exists('__generatePassword')) {
    function __generatePassword($length = 8)
    {
        $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
        return substr($random, 0, $length);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use DB;

class CoreController extends Controller
{
	public $loggedInUser;

	public function __construct()
    {
		config()->set('app.portal', request()->is('seller/*')?'seller':'admin');
		
        // get current logged in user
		$this->loggedInUser = auth()->user();

		$notificationCount = [];

		$this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();
                
			//Notification Count for seller
            if (!is_null($this->loggedInUser) && !is_null($this->loggedInUser->id)) {
                $sql="SELECT COUNT(t1.id) as notificationcount FROM `notifications` AS `t1` JOIN `notification_logs` AS `t2` ON JSON_SEARCH(t1.users, 'one', t2.user_id) WHERE
			(t2.visited_on < t1.notify_on OR t2.visited_on IS NULL) AND t1.notify_on < NOW() AND t1.notified_to = 's' AND `t2`.`user_id` = ".$this->loggedInUser->id." AND `t1`.`deleted_at` IS NULL";

                $notificationCount = DB::select($sql);
				
                View::share('notificationCount', $notificationCount);
            }
			// dd($this->data['notifications']);      

            return $next($request);
        });

		
    }

	public static function frontView($path=null, $data=[])
	{
		$viewPath = FRONTEND_THEME_NAME.".{$path}";
		return view($viewPath, $data); 
	}

	public static function sellerView($path=null, $data=[])
	{
		$viewPath = SELLER_THEME_NAME.".{$path}";
		return view($viewPath, $data); 
	}

    public static function adminView($path=null, $data=[])
	{
		$viewPath = ADMIN_THEME_NAME.".{$path}";
		return view($viewPath, $data); 
	}
}
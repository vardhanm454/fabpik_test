<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

// Models
use App\Models\Subcategory;
use App\Models\Sizechart;
use App\Models\ChildCategory;
use App\Models\Category;
use App\Models\Seller;

class AjaxController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            return $next($request);
        });
    }

    /**
     * [previewImage description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function previewImage(Request $request)
    {
        if(empty($request->image)) $request->image = 'no-image.jpg';
        /*switch ($request->type) {
            case 'brand':
                $request->image = (empty($request->image))?UPLOAD_PATH.'/no-image.png':UPLOAD_PATH.'/brands/'.$request->image;
                break;
            case 'category':
                $request->image = (empty($request->image))?UPLOAD_PATH.'/no-image.png':UPLOAD_PATH.'/category/'.$request->image;
                break;
            case 'subcategory':
                $request->image = (empty($request->image))?UPLOAD_PATH.'/no-image.png':UPLOAD_PATH.'/subcategory/'.$request->image;
                break;
            case 'childcategory':
                $request->image = (empty($request->image))?UPLOAD_PATH.'/no-image.png':UPLOAD_PATH.'/childcategory/'.$request->image;
                break;
                
            default:
                break;
        }*/
        $image = str_replace('\\', '', UPLOAD_PATH.'/'.$request->image);

        // if(!File::exists(base_path($image))) $image = 'no-image.png';
        $filePath = base_path($image);
        // dd($filePath);
        $image_mime = image_type_to_mime_type(exif_imagetype($filePath));

        header('Content-Type: '.$image_mime);
        readfile($filePath);
    }

    /**
     * [changeStatus description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function changeStatus(Request $request)
    {
        $status = ($request->status=='true')?1:0;
        return DB::table($request->table)
                ->where('id', $request->id)
                ->update([
                    'status' => $status,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
    }

    /**
     * [change Featured Status description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function changeFeaturedStatus(Request $request)
    {
        $status = ($request->status=='true')?1:0;
        return DB::table($request->table)
                ->where('id', $request->id)
                ->update([
                    'featured' => $status,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
    }

    /**
     * [Get the Sub Categories List Based on Parent Category]
     * @param  Request $request [description]
     * @param  [type]  $id      [subcategory id]
     * @return [type]           [description]
    */
    public function getSubCategories(Request $request)
    {
        $subcategories = Subcategory::where('category_id', $request->id)->select('id', 'title')->where('status', 1)->get();
        return response()->json(['success'=>1, 'message'=>"", 'list'=>$subcategories], 200);
    }

    /**
     * [Get the Child Category List Based on Category ID]
     * @param  Request $request [description]
     * @param  [type]  $id      [Category id]
     * @return [type]           [description]
    */
    public function getChildCategoriesList(Request $request)
    {
        $childcategories = ChildCategory::where('subcategory_id', $request->id)->where('status', 1)->get();
        return response()->json(['success'=>1, 'message'=>"", 'list'=>$childcategories], 200);
    }

     /**
     * [Get the Category List Based on Category ID]
     * @param  Request $request [description]
     * @param  [type]  $id      [Category id]
     * @return [type]           [description]
    */
    public function getCategoriesList(Request $request)
    {
        $categories = Category::where('id', $request->id)->where('status', 1)->get();
        return response()->json(['success'=>1, 'message'=>"", 'list'=>$categories], 200);
    }

    /**
     * [Get the Size chart List Based on Seller ID]
     * @param  Request $request [description]
     * @param  [type]  $id      [seller id]
     * @return [type]           [description]
    */
    public function getSellerSizecharts(Request $request)
    {
        $sizecharts = Sizechart::where('seller_id', $request->id)->get();
        return response()->json(['success'=>1, 'message'=>"", 'list'=>$sizecharts], 200);
    }

    /**
     * [Get the Child Category List Based on Category ID]
     * @param  Request $request [description]
     * @param  [type]  $id      [Category id]
     * @return [type]           [description]
    */
    public function getChildCategories(Request $request)
    {
        $childcategories = ChildCategory::where('category_id', $request->id)->where('status', 1)->get();
        return response()->json(['success'=>1, 'message'=>"", 'list'=>$childcategories], 200);
    }

    /**
     * [child categories auto complete with pull path]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function autocomplete(Request $request)
    {
        $term = $request->q.'%';

        $categories = DB::table('view_categories')
                        ->where(function($q) use($term) {
                            $q->where('child_title', 'LIKE', $term.'%')
                                ->orWhere('subcategory_title', 'LIKE', $term.'%')
                                ->orWhere('category_title', 'LIKE', $term.'%');
                        });
        if(!empty($request->c)) $categories = $categories->where('category_id', $request->c);
                        
        $categories = $categories->get();

        $response = [];
        foreach ($categories as $key => $category) {
            $response[] = [
                'id' => $category->path_id,
                'text' => strtoupper($category->path_title)
            ];
        }

        return response()->json($response);
    }

    /**
     * [Seller auto complete with pull path]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function sellerAutoComplete(Request $request)
    {
        $term = $request->q.'%';

        // $sellers = DB::table('sellers')
        //                 ->where(function($q) use($term) {
        //                     $q->where('name', 'LIKE', $term.'%');
        //                 })->where('status',1)->whereNull('deleted_at')->limit(20);
        
        $sellers = DB::table('sellers')
                        ->where(function($q) use($term) {
                            $q->where('company_name', 'LIKE', '%%'.$term.'%');
                        })->where('status',1)->whereNull('deleted_at')->limit(20);
                        
        $sellers = $sellers->get();

        $response = [];
        foreach ($sellers as $key => $seller) {
            $response[] = [
                'id' => $seller->id,
                'text' => $seller->company_name
            ];
        }

        return response()->json($response);
    }

    /**
     * [categories auto complete with pull path]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function categoryAutoComplete(Request $request)
    {
        $term = $request->q.'%';

        $categories = DB::table('categories')
                        ->where(function($q) use($term) {
                            $q->where('title', 'LIKE', '%%'.$term.'%');
                        })->where('status',1)->whereNull('deleted_at')->limit(20);
        
        // if (!empty($request->c)) {
        //     $sellerCategories = Seller::whereIn('id',$request->c)->get();
        //     $categories = $categories->whereIn('id', explode(",",$sellerCategories[0]->categories));
        // }
        
        $categories = $categories->get();

        $response = [];
        foreach ($categories as $key => $category) {
            $response[] = [
                'id' => $category->id,
                'text' => $category->title
            ];
        }

        return response()->json($response);
    }

    /**
     * [categories auto complete with pull path]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function productVariantAutoComplete(Request $request)
    {
        $term = $request->q;

        $productVariants = DB::table('product_variants')
                            ->join('products','products.id','=','product_variants.product_id')
                            ->join('brands','brands.id','=','products.brand_id')
                            ->join('sellers','sellers.id','products.seller_id')
                            ->leftjoin('view_product_images as variation_images','variation_images.product_variant_id','=','product_variants.id')
                            // ->join('variation_images','variation_images.id','products.id')
                            ->where(function($q) use($term) {
                                $q->where('product_variants.name', 'LIKE', '%'.$term.'%');
                            })->where('sellers.status',1)
                            ->selectRaw("product_variants.id as pvid,product_variants.name as pvname,product_variants.slug,product_variants.sku,product_variants.mrp as pvmrp,product_variants.discount,
                            (((product_variants.mrp - product_variants.price ) / product_variants.mrp) * 100 ) AS pvdisc,product_variants.price as pvprice,brands.name AS brand_name,product_variants.unique_id,'product' AS 'type',sellers.name as sname, sellers.seller_code as scode, variation_images.thumbnail, variation_images.images")
                            ->whereNull('product_variants.deleted_at');
                        
        $productVariants = $productVariants->orderBy('stock', 'DESC')->limit(20)->get();
        $response = [];
        foreach ($productVariants as $key => $productVariant) {
            $attrValue = __getVariantOptionValues($productVariant->pvid);
            $response[] = [
                'id' => $productVariant->pvid,
                'text' => $productVariant->pvname,
                'mrp' => $productVariant->pvmrp,
                'price' => $productVariant->pvprice,
                'sname' => $productVariant->sname,
                'scode' => $productVariant->scode,
                'disc' => $productVariant->pvdisc,
                'image' => (json_decode($productVariant->images)[$productVariant->thumbnail-1]),
                'primaryattrvalue' => $attrValue['PrimaryAttrValue'],
                'secondaryattrvalue' => $attrValue['SecondaryAttrValue']
            ];
        }

        return response()->json($response);
    }

     /**
     * [Brands auto complete with pull path]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function brandsAutoComplete(Request $request)
    {
        $term = $request->q.'%';
        $brands = DB::table('brands')
                        ->where(function($q) use($term) {
                            $q->where('name', 'LIKE', $term.'%');
                        })->where('status',1)->whereNull('deleted_at')->whereNull('created_by')->limit(20);
        
        $brands = $brands->get();

        $response = [];
        foreach ($brands as $key => $brand) {
            $response[] = [
                'id' => $brand->id,
                'text' => $brand->name
            ];
        }

        return response()->json($response);
    }

    /**
     * [Product Tags auto completeProductTagAutoComplete]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function ProductTagAutoComplete(Request $request)
    {
        $term = $request->q.'%';

        $tags = DB::table('product_tags')
                        ->where(function($q) use($term) {
                            $q->where('name', 'LIKE', '%%'.$term.'%');
                        })->where('status',1)->whereNull('deleted_at')->limit(20);
        
        // if (!empty($request->c)) {
        //     $sellerCategories = Seller::whereIn('id',$request->c)->get();
        //     $categories = $categories->whereIn('id', explode(",",$sellerCategories[0]->categories));
        // }
        
        $tags = $tags->get();

        $response = [];
        foreach ($tags as $key => $tag) {
            $response[] = [
                'id' => $tag->id,
                'text' => $tag->name
            ];
        }

        return response()->json($response);
    }

    /**
     * [Style Layout Column auto complete with pull path]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function layoutColumnAutoComplete(Request $request)
    {
        $term = $request->q.'%';

        $layoutColumns = DB::table('layout_columns')
                        ->where(function($q) use($term) {
                            $q->where('name', 'LIKE', '%%'.$term.'%');
                        });
        if(!empty($request->c)) $layoutColumns = $layoutColumns->where('id', $request->c);
                        
        $layoutColumns = $layoutColumns->whereNull('deleted_at')->get();

        $response = [];
        foreach ($layoutColumns as $key => $column) {
            $response[] = [
                'id' => $column->id,
                'text' => strtoupper($column->name)
            ];
        }

        return response()->json($response);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
Use File;

use App\Models\Seller;

class FileManagerController extends Controller
{
	public $basePath = UPLOAD_PATH;

	public function __construct()
    {
        $this->middleware(function ($request, $next) {
			// get current logged in user
			$this->loggedInUser = auth()->user();
			
			if(auth()->check()) {
				//getting seller ID
				$sellerDetails = Seller::select('seller_code')->where('id', $this->loggedInUser->ref_id)->first();
				if($sellerDetails != null){
	                if ($sellerDetails->seller_code == null) {
	                    $seller_code = __generateSellerCode();
	                    Seller::update(['seller_code'=>$seller_code])->where('id', $this->loggedInUser->ref_id);
	                    $folder = ('uploads'.DIRECTORY_SEPARATOR.$seller_code);
	                    //create folders for seller
	                    if (!File::exists($folder)) {
	                        mkdir($folder, 0777);
	                        chmod($folder, 0777);
	                        
	                        mkdir($folder.'/products', 0777);
	                        chmod($folder.'/products', 0777);
	    
	                        mkdir($folder.'/sizechart', 0777);
	                        chmod($folder.'/sizechart', 0777);
	                    }
	                    
	                    $this->basePath = UPLOAD_PATH.'/'.$seller_code;
	                }else {
						$this->basePath = UPLOAD_PATH.'/'.$sellerDetails->seller_code;

						$folder = ('uploads'.DIRECTORY_SEPARATOR.$sellerDetails->seller_code);
						//create folders for seller
						if (!File::exists($folder)) {
							mkdir($folder, 0777);
							chmod($folder, 0777);
							
							mkdir($folder.'/products', 0777);
							chmod($folder.'/products', 0777);

							mkdir($folder.'/sizechart', 0777);
							chmod($folder.'/sizechart', 0777);
						}
					}
				}
			}			
			
            return $next($request);
		});
    }
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function index(Request $request) 
    {
    	$filterName = '';
    	if (!empty($request->filter_name)) {
			$filterName = rtrim(str_replace(array('*', '/', '\\'), '', $request->filter_name));
		}

		$uploadPath = base_path($this->basePath);

		$directory = $uploadPath;

		// Make sure we have the correct directory
		if (!empty($request->directory)) {
			$directory = rtrim($uploadPath . '/' . str_replace('*', '', $request->directory));
		};
		$directory = urldecode($directory);
		$directory = urldecode(urldecode($directory));

		$page = ($request->page)?$request->page:1;

		$directories = [];
		$files = [];

		$data['images'] = [];

		if (substr(str_replace('\\', '/', realpath($directory) . '/' . $filterName), 0, strlen($uploadPath)) == str_replace('\\', '/', $uploadPath)) {
			// Get directories
			$directories = glob($directory . '/' . $filterName . '*', GLOB_ONLYDIR);

			if (!$directories) {
				$directories = [];
			}

			// Get files
			$files = glob($directory . '/' . $filterName . '*.{webp}', GLOB_BRACE);
			//jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF
			// sort files by last modified date
			usort($files, function($x, $y) {
				return filemtime($x) < filemtime($y);
			});

			if (!$files) {
				$files = [];
			}
		}

		// foreach($files as $item){
		// 	echo basename($item) . " => Last Modified On " . @date('F d, Y, H:i:s', filemtime($item)) . "<br/>";
		// }
		// die;
		
		// Merge directories and files
		$images = array_merge($directories, $files);

		// Get total number of files and directories
		// $image_total = count($images);

		// Split the array based on current page number and max number of items per page of 10
		// $images = array_splice($images, ($page - 1) * 16, 16);

		foreach ($images as $image) {
			$name = str_split(basename($image), 14);

			if (is_dir($image)) {
				$url = '';

				if ($request->target) {
					$url .= '&target=' . $request->target;
				}

				if ($request->thumb) {
					$url .= '&thumb=' . $request->thumb;
				}

				$data['images'][] = [
					'thumb' => '',
					'name'  => implode(' ', $name),
					'type'  => 'directory',
					'path'  => substr($image, strlen($uploadPath.'/')),
					'href'  => route('filemanager', ['directory'=>urlencode(substr($image, strlen($uploadPath.'/')))]) . $url
				];
			} elseif (is_file($image)) {
				$data['images'][] = [
					'thumb' => url($this->basePath . '/' . substr($image, strlen($uploadPath.'/'))),
					'name'  => implode(' ', $name),
					'type'  => 'image',
					'path'  => substr($image, strlen($uploadPath.'/')),
					// 'href'  => $server . 'image/' . substr($image, strlen($uploadPath))
					'href'  => url($this->basePath . '/' . substr($image, strlen($uploadPath.'/')))
				];
			}
		}

		$data['directory'] = ($request->directory)?urlencode($request->directory):'';
		$data['filter_name'] = ($request->filter_name)?urlencode($request->filter_name):'';
		// Return the target ID for the file manager to set the value
		$data['target'] = ($request->target)?urlencode($request->target):'';
		// Return the thumbnail for the file manager to show a thumbnail
		$data['thumb'] = ($request->thumb)?urlencode($request->thumb):'';
		// Parent
		$parentParams = [];
		if ($request->directory) {

			$pos = strrpos($request->directory, '/');

			if ($pos) {
				$parentParams['directory'] = urlencode(substr($request->directory, 0, $pos));
			}
		}

		if ($request->target) {
			$parentParams['target'] = $request->target;
		}

		if ($request->thumb) {
			$parentParams['thumb'] = $request->thumb;
		}

		$data['parent'] = route('filemanager', $parentParams);

		// Refresh
		$refreshParams = [];

		if ($request->directory) {
			$refreshParams['directory'] = urlencode($request->directory);
		}

		if ($request->target) {
			$refreshParams['target'] = $request->target;
		}

		if ($request->thumb) {
			$refreshParams['thumb'] = $request->thumb;
		}

		$data['refresh'] = route('filemanager', $refreshParams);

		$urlParams = [];

		if ($request->directory) {
			$urlParams['directory'] = urlencode(html_entity_decode($request->directory, ENT_QUOTES, 'UTF-8'));
		}

		if ($request->filter_name) {
			$urlParams['filter_name'] = urlencode(html_entity_decode($request->filter_name, ENT_QUOTES, 'UTF-8'));
		}

		if ($request->target) {
			$urlParams['target'] = $request->target;
		}

		if ($request->thumb) {
			$urlParams['thumb'] = $request->thumb;
		}

		// $data['images'] = $this->paginate($data['images']);
		$data['imageRows'] = array_chunk($data['images'], 6);

		return view('utility.filemanager', $data);
    }

    public function folder(Request $request)
    {
    	$json = [];

    	$uploadPath = base_path($this->basePath);

    	$directory = $uploadPath;

		// Make sure we have the correct directory
		if (!empty($request->directory)) {
			$directory = rtrim($uploadPath . '/' . str_replace('*', '', $request->directory));
		};
		$directory = urldecode(urldecode($directory));
		// Check its a directory
		if (!is_dir($directory) || substr(str_replace('\\', '/', realpath($directory)), 0, strlen($uploadPath)) != str_replace('\\', '/', $uploadPath)) {
			$json['error'] = 'Warning: Directory does not exist!';
		}

		if ($request->method() == 'POST') {
			// Sanitize the folder name
			$folder = basename(html_entity_decode($request->folder, ENT_QUOTES, 'UTF-8'));

			// Validate the filename length
			if ((strlen($folder) < 3) || (strlen($folder) > 128)) {
				$json['error'] = 'Warning: Folder name must be between 3 and 128!';
			}

			// Check if directory already exists or not
			if (is_dir($directory . '/' . $folder)) {
				$json['error'] = 'Warning: A file or directory with the same name already exists!';
			}
		}

		if (!isset($json['error'])) {
			mkdir($directory . '/' . $folder, 0777);
			chmod($directory . '/' . $folder, 0777);

			@touch($directory . '/' . $folder . '/' . 'index.html');

			$json['success'] = 'Success: Directory created!';
		}

		return response()->json($json);
    }

    public function upload(Request $request)
    {
    	$json = [];

		$uploadPath = base_path($this->basePath);
    	$directory = $this->basePath;

		// Make sure we have the correct directory
		if (!empty($request->directory)) {
			$directory = rtrim($uploadPath . '/' . str_replace('*', '', $request->directory), '/');
		};
		$directory = urldecode(urldecode($directory));
		//double decoding some times url is double encoded.

		// Check its a directory
		if (!is_dir($directory) || substr(str_replace('\\', '/', realpath($directory)), 0, strlen($uploadPath)) != str_replace('\\', '/', $uploadPath)) {
			$json['error'] = 'Warning: Directory does not exist!';
		}

		if (!$json) {
			// Check if multiple files are uploaded or just one
			$files = [];

			if($request->hasfile('file'))
			{
				foreach($request->file('file') as $file)
				{
					$name = time().'.'.$file->extension();

					// Sanitize the filename
					$filename = basename(html_entity_decode($name, ENT_QUOTES, 'UTF-8'));

					// Validate the filename length
					if ((strlen($filename) < 3) || (strlen($filename) > 255)) {
						$json['error'] = 'Warning: Filename must be between 3 and 255!';
					}

					// Allowed file extension types
					$allowed = [
						'jpg',
						'jpeg',
						'gif',
						'png'
					];

					if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
						$json['error'] = 'Warning: Incorrect file type!';
					}

					if (!$json) {
						$fullpath = $file->move($directory, $filename);
						## CONVERT INTO WEBP FORMAT BY USING HELPER FUNCTION
						$webp_file = __convert_into_webp($fullpath);
					}					
				}
			}
		}

		if (!$json) {
			$json['success'] = 'Success: Your file has been uploaded!';
		}

		return response()->json($json);
    }

    public function delete(Request $request)
    {
    	$json = [];

    	$paths = [];
    	if (isset($request->path)) {
			$paths = $request->path;
		}

		// Loop through each path to run validations
		foreach ($paths as $path) {
			// Check path exsists
			if ($path == 'products' || $path == 'sizechart' || $path == $this->basePath || substr(str_replace('\\', '/', realpath($this->basePath .'/'. $path)), 0, strlen($this->basePath)) == str_replace('\\', '/', $this->basePath)) {
				$json['error'] = 'Warning: You can not delete this directory!';

				break;
			}
		}

		if (!$json) {
			// Loop through each path
			foreach ($paths as $path) {
				$path = rtrim($this->basePath .'/'. $path, '/');

				// If path is just a file delete it
				if (is_file($path)) {
					unlink($path);

				// If path is a directory beging deleting each file and sub folder
				} elseif (is_dir($path)) {
					$files = [];

					// Make path into an array
					$path = array($path);

					// While the path array is still populated keep looping through
					while (count($path) != 0) {
						$next = array_shift($path);

						foreach (glob($next) as $file) {
							// If directory add to path array
							if (is_dir($file)) {
								$path[] = $file . '/*';
							}

							// Add the file to the files to be deleted array
							$files[] = $file;
						}
					}

					// Reverse sort the file array
					rsort($files);

					foreach ($files as $file) {
						// If file just delete
						if (is_file($file)) {
							unlink($file);

						// If directory use the remove directory function
						} elseif (is_dir($file)) {
							rmdir($file);
						}
					}
				}
			} 

			$json['success'] = 'Success: Your file or directory has been deleted!';
		}

		return response()->json($json);
    }

    public function rename()
    {
    	if(request()->path) {
    		$path = 'uploads/'.request()->path;
	    	if ($handle = opendir($path)) {
			    while (false !== ($fileName = readdir($handle))) {
			        $newName = str_replace(" ","-",$fileName);
			        // echo $newName."<br/>";
			        @rename($path.'/'.$fileName, $path.'/'.$newName);
			    }
			    closedir($handle);
			}
    	}    	
    }

}
<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use App\Mail\ForgotPasswordSellerMail;
use App\Mail\NewSellerRegistrationAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Redirect;
use Validator;
use Mail;
use Auth;
use Hash;
use Session;
use Msg91;


// Models
use App\Models\User;

class PasswordController extends CoreController
{
    public $loggedInUser;

    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest')->except('logout');

        // get current logged in user
        $this->loggedInUser = auth()->user();
    }

    public function forgotPassword(Request $request)
    {
        if ($request->doSubmit) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email'
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {
                $user = User::where('email', $request->email)->where('user_type','LIKE','seller%')->exists();
                if ($user) {
                    // $password= $this->passwordGeneration(8);
                    $password= __generatePassword(8);
                    User::where('email', $request->email)->where('user_type','LIKE','seller%')
                        ->update(array("password"=>Hash::make($password)));
                    
                    $seller_details = User::where('email', $request->email)->first();
                    $details = [
                        'email'=> $request->email,
                        'password'=> $password,
                        'name'=> $seller_details->name,
                        'updated_date'=> $seller_details->updated_at,
                    ];
                    Mail::to($request->email)->send(new ForgotPasswordSellerMail(json_encode($details)));
                    
                    return Redirect::route('seller.login')->with(['toast'=>'1','status'=>'success','title'=>'Mail Sent','message'=>'Password has been sent to your mail']);
                    
                } else {

                    return Redirect::back()->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'User doesnt exist']);
                }
            }
        }
        return parent::sellerView('auth.forgot-password');
    }

    public function passwordGeneration($n)
    {
        $generator = "abcmhkghijk";
        $result = "";
        for ($i = 1; $i <= $n; $i++) {
            $result .= substr($generator, (rand()%(strlen($generator))), 1);
        }
        return $result;
    }
}

<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use DB;
use Session;
use Validator;
use Excel;

// Models
use App\Models\Brand;

// Exports
use App\Exports\BrandExport;

class BrandController extends CoreController
{
	public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'brands';

    public function __construct() {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });        
    }

    /**
     * [brands listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('brands_perpage', 10);
        $this->data['dt_page'] = Session::get('brands_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3';
        $this->data['dt_ajax_url'] = route('seller.brands.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Brands';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Brands',
            ],
        ];

        return parent::sellerView('brand.index', $this->data);
    }

    /**
     * [get all brands for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
            2 => 'updated_at',
            3 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('brand', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType) 
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {

            DB::beginTransaction();
            try {
                Brand::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Brands are deleted successfully.';
            }
            catch(Exception $e) {
                DB::rollback(); 
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => 1,
        ];
        $brands = Brand::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $brands->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        $canChange = ( $this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin') );

        foreach($brands as $index=>$brand) {
            $status = $statusList[$brand->status];

            $actionBtns = ($canChange)?'<div class="">
            <a href="" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a></div>':'';

            $records["data"][] = [
                ++$index,
                $brand->name,
                '<img src="'.route('ajax.previewImage',['image'=>$brand->image,'type'=>'brand']).'" class="brand-thumbnail-img" alt="'.$brand->name.'" width="100"/>',
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>'
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export brands]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null,
        ];
        $brands = Brand::getExportData($criteria);

        return Excel::download(new BrandExport($brands), 'brands.xlsx');
    }
}

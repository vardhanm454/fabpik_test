<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Validator;
use Session;
use Excel;
use DB;

// Models
use App\Models\Ticket;
use App\Models\TicketComment;
use App\Models\seller;


class TicketController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'tickets';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [sizecharts listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('ticket_perpage', 10);
        $this->data['dt_page'] = Session::get('ticket_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('seller.tickets.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Ticket List';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Support Tickets',
            ],
        ];

        return parent::sellerView('tickets.index', $this->data);
    }

    /**
     * [get all seller Tickets for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'issue_title',
            2 => 'updated_at'
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('ticket', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType) 
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {

            DB::beginTransaction();
            try {
                Ticket::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Records are deleted successfully.';
            }
            catch(Exception $e) {
                DB::rollback(); 
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'seller' => $this->loggedInUser->ref_id,
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
        ];
        $tickets = Ticket::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $tickets->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $statusList = [
            ["danger" => "Inactive"],
            ["primary" => "Active"]
        ];

        $canChange = ( $this->loggedInUser->hasRole('Seller') || $this->loggedInUser->hasRole('Admin') );

        foreach($tickets as $ticket) {
            if($ticket->issue_status==0){
                $statusText = '<span class="badge badge-light-danger badge-roundless"> Pending </span>';
            }else if($ticket->issue_status==1){
                $statusText = '<span class="badge badge-light-primary badge-roundless"> Processing </span>';
            }else{
                $statusText = '<span class="badge badge-light-success badge-roundless"> Completed </span>';
            }

            $actionBtns = ($canChange)?'<div class=""><a href="'.route('seller.tickets.view', ['id'=>$ticket->id]).'" class="btn btn-icon-only default btn-circle tooltips" data-toggle="tooltip" data-placement="top" data-original-title="View Details"><span aria-hidden="true" class="icon-eye"></span></a>':'';

            $records["data"][] = [
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$ticket->id.'"/><span></span></label>',
                '<a href="'.route('seller.tickets.view', ['id'=>$ticket->id]).'" class="tooltips" data-toggle="tooltip" data-placement="top" data-original-title="View Details">'.$ticket->ticket_no.'</a>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.date('d M Y', strtotime($ticket->created_at)).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.($ticket->updated_at)?date('d M Y', strtotime($ticket->updated_at)):''.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$ticket->issue_title.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$ticket->category.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;"><a href="'.url('/').'/'.UPLOAD_PATH.'/support_ticket/'.$ticket->attachment.'" width="200px" height="200px" target="_blank">'.$ticket->attachment.'</a></div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$statusText.'</div>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [Raise Ticket by Seller]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request){
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                'issue_title' => 'required|string',
                'seller_id'=>'required|integer',
                'issue'=>'required|string',
                'categories'=>'required|string',
                'attachment'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            if($request->hasFile('attachment')){
                $attachment = time().'.'.$request->attachment->extension(); 
                $request->attachment->move(UPLOAD_PATH.'/support_ticket', $attachment);
            }else{
                $attachment = NULL;
            }

            try {
                $ticketNo = mt_rand(100000, 999999);
                $tblData = new Ticket();
                $tblData->issue_title = $request->issue_title;
                $tblData->ticket_no = $ticketNo;
                $tblData->seller_id = $request->seller_id;
                $tblData->issue = $request->issue;
                $tblData->category = $request->categories;
                $tblData->attachment = $attachment;
                
                if ($tblData->save()) {
                    return redirect()
                        ->route('seller.tickets')
                        ->with(['toast'=>'1','status'=>'success','title'=>'Ticket','message'=>'Success! New Ticket Raised successfully.']);
                } else {
                    return redirect()
                            ->route('seller.tickets')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Ticket','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                // dd($e);
                return redirect()
                            ->route('seller.tickets')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Ticket','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['seller_id'] = auth()->user()->ref_id;

        $this->data['title'] = 'Add New Support Ticket';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'seller.tickets',
                'title' => 'Support Tickets',
            ],
            (object) [
                'url' => '',
                'title' => 'Raise New Ticket',
            ]
        ];

        return parent::sellerView('tickets.addTicket', $this->data);
    }

    /**
     * [View Ticket by Seller]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function view(Request $request, $id){
        // $this->data['ticket_details'] = Ticket::find($id);
        // $this->data['ticket_comments'] = TicketComment::with('user')->where('help_desk_id',$id)->get();
        $this->data['ticket_details'] = Ticket::with(['comments' => function ($query) {
                                                    $query->orderBy('id', 'asc');
                                                }])->where('id', $id)->first();
        $this->data['seller_id'] = auth()->user()->ref_id;

        $this->data['title'] = 'Ticket Details';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'seller.tickets',
                'title' => 'Support Tickets',
            ],
            (object) [
                'url' => '',
                'title' => 'View Ticket Details',
            ]
        ];

        return parent::sellerView('tickets.view', $this->data);
    }

    /**
     * [post ticket comment]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postComment(Request $request , $id){
        $validator = Validator::make($request->all(), [
                'sellerComment' => 'required|string',
                'sellerAttachment'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()->back() ->withErrors($validator) ->withInput();
        }

        $attachment = null;
        if($request->hasFile('sellerAttachment')){
            $attachment = time().'.'.$request->sellerAttachment->extension(); 
            $request->sellerAttachment->move(UPLOAD_PATH.'/support_ticket', $attachment);
        }

        try {
            $tblData = new TicketComment();
            $tblData->comments = $request->sellerComment;
            $tblData->comment_by = auth()->user()->id;
            $tblData->attachment = $attachment;
            $tblData->help_desk_id = $id;
            
            if ($tblData->save()) {
                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'success','title'=>'Ticket','message'=>'Success! Replied successfully.']);
            } else {
                return redirect()
                       ->back()
                        ->with(['toast'=>'1','status'=>'error','title'=>'Ticket','message'=>'Error! Some error occured, please try again.']);
            }
        } catch (Exception $e) {
            // dd($e);
            return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'error','title'=>'Ticket','message'=>'Error! Some error occured, please try again!']);
        }


    }
}

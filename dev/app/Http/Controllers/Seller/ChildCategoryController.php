<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Excel;
use DB;

// Models
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\ChildCategory;

// Exports
use App\Exports\ChildCategoryExport;

class ChildCategoryController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'childcategories';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Child Categories listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('childcategories_perpage', 10);
        $this->data['dt_page'] = Session::get('childcategories_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('seller.childcategories.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Child Categories';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Child Categories',
            ],
        ];

        return parent::sellerView('childcategory.index', $this->data);
    }

    /**
     * [get all Child Categories and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'title',
            2 => 'title',
            3 => 'title',
            4 => 'image',
            5 => 'icon',
            6 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('subcategory', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                ChildCategory::whereIn('id', $request->input('id'))->delete();
                DB::commit();
                $records["customActionMessage"] = 'Selected Child Categories are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => 1,
            'fcategoryid' => ($request->fcategoryid)?:null,
            'fsubcategoryid' => ($request->fsubcategoryid)?:null,
        ];
        $childcategories = ChildCategory::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $childcategories->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($childcategories as $index=>$childcategory) {
            $status = $statusList[$childcategory->status];
            
            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('seller.childcategories.edit', ['id'=>$childcategory->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="'.route('seller.childcategories.delete', ['id'=>$childcategory->id]).'" del-url="'.route('seller.childcategories.delete', ['id'=>$childcategory->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><span aria-hidden="true" class="icon-trash"></span></a></div>':'';

            $records["data"][] = [
                ++$index,
                $childcategory->title,
                ($childcategory->subcategory)?$childcategory->subcategory->title:'',
                ($childcategory->category)?$childcategory->category->title:'',
                '<img src="'.route('ajax.previewImage', ['image'=>$childcategory->image,'type'=>'childcategory']).'" class="childcategorybanner-thumbnail-img" alt="'.$childcategory->title.'" height="75" width="75"/>',
                '<img src="'.route('ajax.previewImage', ['image'=>$childcategory->icon,'type'=>'childcategory']).'" class="childcategoryicon-thumbnail-img" alt="'.$childcategory->title.'" height="50" width="50"/>',
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export Child Categories]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null
        ];
        $childcategories = ChildCategory::getExportData($criteria);
        return Excel::download(new ChildCategoryExport($childcategories), 'Childcategories.xlsx');
    }

    /**
     * [child categories auto complete with pull path]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function autocomplete(Request $request)
    {
        $term = $request->q.'%';

        $categories = DB::table('view_categories')
                        ->where(function($q) use($term) {
                            $q->where('child_title', 'LIKE', $term.'%')
                                ->orWhere('subcategory_title', 'LIKE', $term.'%')
                                ->orWhere('category_title', 'LIKE', $term.'%');
                        });
        if(!empty($request->c)) $categories = $categories->where('category_id', $request->c);
                        
        $categories = $categories->get();

        $response = [];
        foreach ($categories as $key => $category) {
            $response[] = [
                'id' => $category->path_id,
                'text' => strtoupper($category->path_title)
            ];
        }

        return response()->json($response);
    }

 
}
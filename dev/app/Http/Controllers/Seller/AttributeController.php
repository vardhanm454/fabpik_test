<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Validator;
use Excel;
use DB;

// Models
use App\Models\Category;
use App\Models\Attribute;
use App\Models\AttributeOption;

// Exports
use App\Exports\AttributeExport;

class AttributeController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'attributes';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [All Attributes listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('attributes_perpage', 10);
        $this->data['dt_page'] = Session::get('attributes_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '5,6,7';
        $this->data['dt_ajax_url'] = route('seller.attributes.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Attributes';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Attributes',
            ],
        ];

        return parent::sellerView('attributes.index', $this->data);
    }

    /**
     * [get all Attributes for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
            2 => 'display_name',
            3 => 'display_name',
            6 => 'status',
            7 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('attribute', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Attribute::whereIn('id', $request->input('id'))->delete();

                // reset categories primary attribute
                DB::statement("UPDATE `categories` SET `primary_attribute` = null WHERE `primary_attribute` IN(".implode(',', $request->input('id')).") ");

                // reset categories secondary attribute
                DB::statement("UPDATE `categories` SET `secondary_attribute` = null WHERE `secondary_attribute` IN(".implode(',', $request->input('id')).") ");

                DB::commit();
                $records["customActionMessage"] = 'Selected Attributes are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
        ];
        $attributes = Attribute::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $attributes->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        foreach ($attributes as $index=>$attribute) {
            //getting all categories
            // $categoriesName = [];
            // foreach ($attribute->attributeCategory as $attrCategory) {
            //     $categoriesName[] = $attrCategory->category->title;
            // }

            //getting all attribute options(Values)
            $values = [];
            foreach ($attribute->attributeOptions as $options) {
                $color = '';
                $calss = '';
                if ($options->colour_code != null) {
                    $calss .= "badge badge-default margin-bottom-5";
                    $color .= "background:".$options->colour_code;
                }
                $values[] = '<span class="'.$calss.'" style="'.$color.'">&nbsp;</span> &nbsp;'.$options->option_name;
            }

            $status = $statusList[$attribute->status];

            $records["data"][] = [
                ++$index,
                $attribute->name,
                $attribute->display_name,
                '<span style="white-space: break-spaces;">'.((!empty($values))?implode(', ', $values).'<br/>':'').'',
                date('d M, Y', strtotime($attribute->created_at)),
                date('d M, Y', strtotime($attribute->updated_at)),
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    '<a href="'.route('seller.attributes.values', ['id'=>$attribute->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Manage Values"><span aria-hidden="true" class="icon-tag"></span></a>'.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [Show the Attribute Values]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function values(Request $request, $id)
    {
        $this->data['id'] = $id;
        $attribute = Attribute::where('id', $id)->select('name', 'attr_type')->first();
        $this->data['attr_type'] = $attribute->attr_type;
        $this->data['attributeValues'] = AttributeOption::where('attribute_id', $id)->get();
        
        $this->data['title'] = 'Manage Attribute Value';
        $this->data['formtitle'] = 'Add Attribute Value';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'seller.attributes',
                'title' => 'Attributes',
            ],
            (object) [
                'url' => '',
                'title' => $attribute->name,
            ],
            (object) [
                'url' => false,
                'title' => 'Values',
            ]
        ];

        return parent::sellerView('attributes.attributeValues', $this->data);
    }

    /**
     * [export Attributes]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null
        ];
        $attributes = Attribute::getExportData($criteria);
        return Excel::download(new AttributeExport($attributes), 'attribute.xlsx');
    }
}

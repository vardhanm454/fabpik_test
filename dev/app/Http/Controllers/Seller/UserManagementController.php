<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Session;
use Excel;
use DB;
use Hash;
use Mail;
use Carbon\Carbon;

// Models
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\Staff;
use App\Models\User;

// Exports
use App\Exports\StaffExport;

// Repo
use Facades\App\Repository\PermissionRepo;
use Facades\App\Repository\StaffRepo;

class UserManagementController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'staffs';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    public function index() 
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('usermgmt_perpage', 10);
        $this->data['dt_page'] = Session::get('usermgmt_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('seller.staffs.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','femail','fmobile'];

        $this->data['title'] = 'Staffs';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Staffs',
            ]
        ];

        return parent::sellerView('usermanagement.index', $this->data);
    }

    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
            2 => 'email',
            3 => 'designation',
            4 => 'mobile_no',      
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        __setDatatableCurrPage('usermgmt', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Staff::whereIn('id', $request->input('id'))->delete();
                DB::commit();
                
                // flush the cache
                StaffRepo::flush();

                $records["customActionMessage"] = 'Selected Staffs are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
      
        $criteria = (object)[
            'length' => intval($request->length),
            'name' => (!is_null($request->fname))?$request->fname:null,
            'email' => (!is_null($request->femail))?$request->femail:null,
            'mobile' => (!is_null($request->fmobile))?$request->fmobile:null,
            'seller' => $this->loggedInUser->ref_id,
        ];
        
        $staffs = StaffRepo::allPaginated($orderColumn, $orderDir, $criteria, $iPage);
        
        $iTotalRecords = $staffs->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($staffs as $staff) {             
            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$staff->id.'"/><span></span></label>',
                $staff->name,
                $staff->email,
                $staff->designation,
                $staff->mobile_no,
                '<div class="btn-group btn-group-sm btn-group-solid">
                    <div class="">
                        <a href="'.route('seller.staffs.edit', ['id'=>$staff->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
                        <a href="javascript:;" del-url="'.route('seller.staffs.delete', ['id'=>$staff->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>
                    </div>
                </div>',
            ];        
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }
    
    public function export(Request $request)
    {
        $criteria = (object)[
            'name' => (!is_null($request->fname))?$request->fname:null,
            'email' => (!is_null($request->femail))?$request->femail:null,
            'mobile' => (!is_null($request->fmobile))?$request->fmobile:null,
            'seller' => $this->loggedInUser->ref_id,
        ];
        $staffs = Staff::getExportData($criteria);
        return Excel::download(new StaffExport($staffs),'staffs.xlsx');
    }
     
    public function create(Request $request)
    {   
        if($request->save){
            $validator = Validator::make($request->all(), [
                "name" => "required|max:50",
                "email" => "required|unique:staffs,email",
                "designation" => "required|max:50",
                "mobile_no" => "required|digits:10",
                "permissions" => "required"
            ], []);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
            try {
                $staff = new Staff();
                $staff->name = $request->name;
                $staff->email = $request->email;
                $staff->designation = $request->designation;
                $staff->mobile_no = $request->mobile_no;
                $staff->seller_id = $this->loggedInUser->ref_id;
                $staff->save();
                
                $password = 'changeMe@123';

                $user = new User();
                $user->ref_id = $staff->id;
                $user->user_type = 'seller-staff';
                $user->name = $request->name;
                $user->email = $request->email;
                $user->mobile = $request->mobile_no;
                $user->password = Hash::make($password);
                $user->save();
                 
                $user->syncPermissions([$request->permissions]);

                DB::commit();
                
                // flush the cache
                StaffRepo::flush();

                return redirect()
                        ->route('seller.staffs')
                        ->with(['toast'=>'1','status'=>'success','title'=>'Staffs','message'=>'Success! New Staff Added successfully.']);

            } catch (Exception $e) {
                DB::rollback();
                return back()
                        ->with(['toast'=>'1','status'=>'error','title'=>'Staffs','message'=>'Error! Some issue occured, please try again later.']);
            }
        }

        $this->data['permissions'] = PermissionRepo::modulewise();
        $this->data['title'] = 'New Staff';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'seller.staffs',
                'title' => 'Staffs',
            ],
            (object) [
                'url' => false,
                'title' => 'New Staff',
            ],
        ];
        return parent::sellerView('usermanagement.addEditStaff', $this->data);
    }
     
    public function edit(Request $request,$id)
    {         
        $staff = StaffRepo::get($id);
        $user = User::where(['ref_id'=>$id,'user_type'=>'seller-staff'])->first();
        
        if(!$user) {
            return redirect()
                    ->route('seller.staffs')
                    ->with(['toast'=>'1','status'=>'error','title'=>'Staffs','message'=>'Error! User does not exist.']);
        }

        if($request->save) {
            $validator = Validator::make($request->all(), [
                "name" => "required|max:50",
                "email" => "required|unique:staffs,email,".$id.",id",
                "designation" => "required|max:50",
                "mobile_no" => "required|digits:10",
                "permissions" => "required"
            ], []);

            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
            try {
                // update staff details
                $staff->name = $request->name;
                $staff->email = $request->email;
                $staff->designation = $request->designation;
                $staff->mobile_no = $request->mobile_no;
                $staff->seller_id = $this->loggedInUser->ref_id;
                $staff->save();

                // update user details
                $user->name = $staff->name;
                $user->email = $staff->email;
                $user->mobile = $staff->mobile_no;
                $user->save();

                // update user permissions
                $user->syncPermissions([$request->permissions]);

                DB::commit();
                
                // flush the cache
                StaffRepo::flush();

                if ($request->save == 'save') {
                    return redirect()
                            ->route('seller.staffs')
                            ->with(['toast'=>'1','status'=>'success','title'=>'Staffs','message'=>'Success! Staff data updated successfully.']);
                } else {
                    return redirect()
                            ->back()
                            ->with(['toast'=>'1','status'=>'success','title'=>'Staffs','message'=>'Success! Staff data updated successfully.']);
                }
            } catch (Exception $e) {                    
                DB::rollback();
                return back();
            }
        }

        $this->data['id'] = $id;
        $this->data['staff'] = $staff;
        
        $this->data['permissions'] = PermissionRepo::modulewise();
        $this->data['assignedPerms'] = $user->getDirectPermissions()->pluck('id')->toArray();
        $this->data['title'] = 'Edit Staff';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'seller.staffs',
                'title' => 'Staffs',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['staff']->name,
            ],
        ];

        return parent::sellerView('usermanagement.addEditStaff', $this->data);
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $delete = Staff::where('id',$id)->delete();
            $deleteUser = User::where(['ref_id'=>$id,'user_type'=>'admin-staff'])->delete();

            DB::commit();
            
            // flush the cache
            StaffRepo::flush();

            return response()->json(['success'=>1, 'message'=>""], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }
}
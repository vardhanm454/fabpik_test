<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;
use Seshac\Shiprocket\Shiprocket;
use Response;

// Models
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\OrderHistory;
use App\Models\PaymentStatus;
use App\Models\ShippingStatus;
use App\Models\Seller;
use  App\Models\CourierPartner;

// Exports

class ShippingController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'shipping';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('shipping_perpage', 10);
        $this->data['dt_page'] = Session::get('shipping_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('seller.shipping.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname', 'ffromdate', 'ftodate', 'fsstatus', 'fmobile', 'fsku', 'ftrackid', 'fostatus', 'forderid'];

        $this->data['title'] = 'Shipping';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Shipping',
            ]
        ];

        $this->data['seller'] = Seller::find($this->loggedInUser->ref_id);
        $this->data['shippingStatus'] = ShippingStatus::get();
        $this->data['orderStatus'] = OrderStatus::get();

        return parent::sellerView('shipping.index', $this->data);
    }

    /**
     * [get all Order Details with shipping Status and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'created_at',
            2 => 'child_order_id',
            3 => 'child_order_id',
            4 => 'child_order_id',
            5 => 'child_order_id',
            6 => 'child_order_id',
            7 => 'child_order_id',
            8 => 'child_order_id',
            9 => 'quantity',
            10 => 'quantity',
            11 => 'quantity',
            12 => 'quantity',
            13 => 'quantity',
            14 => 'quantity',
            15 => 'quantity',
            16 => 'shipping_status_id',
            17 => 'shipping_status_id',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('shipping', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                OrderDetail::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Records are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        if ($request->input('customActionName') == "changestatus") {
            // DB::beginTransaction();
            try {       
                $statusResult = json_decode(__changeSellerShippingStatus($request->input('status'), $request->input('id')));
              
                if($statusResult->errors == true){
                    // DB::rollback();
                    $records["customActionStatus"] = "NOT-OK";
                    $records["customActionMessage"] = $statusResult->message;
                }else {
                    // DB::commit();
                
                    $records["customActionStatus"] = "OK";
                    $records["customActionMessage"] = $statusResult->message;
                }
               
            } catch (Exception $e) {
                // DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'seller' => $this->loggedInUser->ref_id,
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'fsstatus' => (!is_null($request->fsstatus))?$request->fsstatus:null,
            'fmobile' => (!is_null($request->fmobile))?$request->fmobile:null,
            'fsku' => (!is_null($request->fsku))?$request->fsku:null,
            'fostatus' => (!is_null($request->fostatus))?$request->fostatus:null,
            'ftrackid' => (!is_null($request->ftrackid))?$request->ftrackid:null,
            'fcorderid' => (!is_null($request->forderid))?$request->forderid:null,
        ];
        $orderDetails = OrderDetail::getAjaxShippingListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $orderDetails->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            "Pending" => "danger",
            "Ready for Pickup" => "info",
            "Pickup Scheduled" => "warning",
            "Shipped" => "success",
            "Delivered" => "success",
            "Delivery Unsuccessful" => "danger",
            "Undelivered Product Returned" => "primary",
            "Undelivered Product Return Confirmed" => "primary",
            "Schedule Return Pickup" => "info",
            "Delivery Unsuccessful" => "danger",
            "Return Pickup Initiated" => "dark",
            "Product Returned to Seller" => "warning",
            "Product Return Confirmed" => "success",
        ];

        foreach ($orderDetails as $orders) {

            $awbUpdate= ($orders->shipping_model == 's') ? '<a href="javascript:;" class="btn font-yellow-gold pd-lr-0" title="Modify/Update AWB" data-toggle="modal" data-action="modify-awb" data-target="#modifyAWBModal" data-awb_child_order_id="'.$orders->id.'" data-current_awb_number="'.$orders->tracking_no.'" data-keyboard="false" data-backdrop="static">
                <span aria-hidden="true" class="fa-address-book"><small>AWB</small></span>
            </a>' : '';

            $manifestUrl = ($orders->manifest_url != null) ?  $orders->manifest_url : 'javascript:;';
            $labelUrl = ($orders->shipment_id) ? route('seller.shipping.generateLabel', ['shipmentid'=>$orders->shipment_id]) : 'javascript:;';
        
            $actionBtns = '<div class="">
                <a href="'.$manifestUrl.'" target="_blank" class="btn font-yellow-gold pd-lr-0" title="Download Manifest">
                    <span aria-hidden="true" class="fa fa-file-text"></span>
                </a>                    
                <a href="'.$labelUrl.'" class="btn font-yellow-gold pd-lr-0" title="Download Shippment Label">
                    <span aria-hidden="true" class="icon-cloud-download"></span>
                </a>
                '.$awbUpdate.'
            </div>';

            $ship_status = OrderDetail::getShippingData($orders->shipping_status_id)->seller_text;
            $status = $statusList[$ship_status];
            
            $records["data"][] = [               
                '<div style="width:20px;text-align:center;margin:0 auto;"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$orders->id.'"/><span></span></label></div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.date('d-m-Y H:i A', strtotime($orders->created_at)).'</div>',
                '<a href="'.route('seller.orders.view', ['id'=>$orders->id]).'" class="" title="View Order Details">'.$orders->child_order_id.'</a>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'. ( ($orders->productVarient) ? $orders->productVarient->name : '' ).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$orders->quantity.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'. ( ($orders->productVarient) ? $orders->productVarient->shipping_weight : '' ).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'. ( ($orders->productVarient) ? $orders->productVarient->shipping_length*$orders->productVarient->shipping_breadth*$orders->productVarient->shipping_height : '' ).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.date('d-m-Y H:i A', strtotime($orders->created_at. ' + '.( ($orders->productVarient) ? $orders->productVarient->min_ship_hours : '0' ) .' days')).'</div>',
                ($orders->delivered_date)?date('d-m-Y H:i A', strtotime($orders->delivered_date)):'',
                '<div class="text-center" style="white-space: normal;word-break:break-word;"><span class="badge badge-light-'.($status).' badge-roundless">'.$ship_status.'</span></div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$actionBtns.'</div>',
                $orders->order->first_name.' '.$orders->order->last_name,
                $orders->productVarient->unique_id,
                $orders->order->shipping_pincode,
                $orders->tracking_no,
                date('d M Y', strtotime($orders->excepted_delivery_date)),
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    public function generateLabel(Request $request, $shipmentid=''){
        // $shipmentIds = [ 'shipment_id' => [121221,122112] ];
        $shipmentIds = [ 'shipment_id' => [$shipmentid] ];
        $token =  Shiprocket::getToken();
        $response = Shiprocket::generate($token)->label($shipmentIds);
        $filename = $shipmentid.'.pdf';
        try{
            if(isset($response['label_url'])){
                if(!is_null($response['label_url'])){

                    OrderDetail::where('shipment_id', $shipmentid)->update([
                                                'label_url' => $response['label_url'],
                                                ]);                    
                    $tempName = tempnam(sys_get_temp_dir(), $filename);

                    copy($response['label_url'], $tempName);
                    return Response::download($tempName, $filename);
                }else{
                    return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'error','title'=>'Label Generate','message'=>'issue with generating label, please contact Administrator..']);    
                }
                
            }else{
                return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'error','title'=>'Label Generate','message'=>'Error! issue with label, please contact Administrator.']);
            }
        }catch (Exception $e){
            // dd($e);
            return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'error','title'=>'Label Generate','message'=>'Error! Some error occured, please contact Administrator.']);
        }
        
    }

    /**
     * [modify AWB Details]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function modifyAWBNumber(Request $request, $id)
    {
        try {

            //check the AWB number is exists DB;
            $isExists = OrderDetail::where('id', $id)->where("tracking_no", $request->awb_number)->exists();

            if (!$isExists) {

                if($request->courier_partner_id == 'o'){
                    $courierDetails = new CourierPartner();
                    $courierDetails->name = $request->courier_partner_name;
                    $courierDetails->is_append = 'n';
                    $courierDetails->tracking_url = $request->tracking_url;
                    $courierDetails->created_by = $this->loggedInUser->ref_id;
                    $courierDetails->save();
                }else{
                     //get the Courier Partner Details;
                     $courierDetails =  CourierPartner::where('id', $request->courier_partner_id)->first();
                }
               
                $trackingDetails = [
                            'tracking_no' => $request->awb_number,
                            'courier_id' => $courierDetails->id,
                            'courier_name' => $courierDetails->name,
                            'tracking_url' => $courierDetails->tracking_url,
                        ];

                $updateDetails = OrderDetail::where('id', $id)->update($trackingDetails);

                if ($updateDetails) {
                    return response()->json(['success'=>true, 'message'=>"AWB details updated successfully", 'err'=>false, 'errors'=>[]], 200);
                }
                return response()->json(['success'=>false, 'message'=>'Unable Update the Details, try after some time.', 'err'=>true, 'errors'=>[]], 200);
            }
            return response()->json(['success'=>false, 'message'=>"AWB Number is already assigned to another order, please check once.", 'err'=>true, 'errors'=>[]], 200);

        } catch (Exception $e){
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'err'=>true, 'errors'=>[]], 200);
        }
    }
}
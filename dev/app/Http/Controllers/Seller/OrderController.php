<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;
use PDF;

// Models
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\OrderHistory;
use App\Models\PaymentStatus;
use App\Models\ShippingStatus;
use App\Models\Seller;
use App\Models\ProductVariant;
use App\Models\ProductVariantOption;
use App\Models\AttributeOption;
use App\Models\Attribute;

// Exports

class OrderController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'orders';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('orders_perpage', 50);
        $this->data['dt_page'] = Session::get('orders_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('seller.orders.getAjaxListData');
        $this->data['dt_search_colums'] = ['ffromdate', 'ftodate', 'fostatus', 'forderid', 'fproductname', 'fproductsku', 'fminsellprice', 'fmaxsellprice'];

        $this->data['title'] = 'Orders';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Orders',
            ]
        ];

        $this->data['sellers'] = Seller::get();
        $this->data['orderStatus'] = OrderStatus::get();

        return parent::sellerView('orders.index', $this->data);
    }

    /**
     * [get all Parent Orders and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'id',
            2 => 'created_at',
            3 => 'child_order_id',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('order', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                OrderDetail::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Orders are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        if ($request->input('customActionName') == "changestatus") {
            DB::beginTransaction();
            try {       
                $res = __changeStatus($request->input('status'), $request->input('id'));
                $result = json_decode($res);
                if($result->status == 0){
                    DB::rollback();
                    $records["customActionStatus"] = "NOT-OK";
                    $records["customActionMessage"] = $result->message;
                }else if($result->status == 1){
                    DB::commit();
                    $records["customActionStatus"] = "OK";
                    $records["customActionMessage"] = $result->message;
                }
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'seller' => $this->loggedInUser->ref_id,
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'fostatus' => (!is_null($request->fostatus))?$request->fostatus:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fchildid' => (!is_null($request->forderid))?$request->forderid:null,
            'fproductname' => (!is_null($request->fproductname))?$request->fproductname:null,
            'fproductsku' => (!is_null($request->fproductsku))?$request->fproductsku:null,
            'fminsellprice' => (!is_null($request->fminsellprice))?$request->fminsellprice:null,
            'fmaxsellprice' => (!is_null($request->fmaxsellprice))?$request->fmaxsellprice:null,
        ];
        $orders = OrderDetail::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $orders->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Seller'));

        $featuredList = [
            ["danger" => "No"],
            ["primary" => "Yes"]
        ];

        $statusList = [
            "Cancelled" => "danger",
            "Pending From Seller" => "warning",
            "Rejected by Seller" => "danger",
            "Confirmed by Seller" => "success",
            "Return Requested" => "info",
            "Return Approved by Admin" => "success",
            "Rejection Approved by Admin" => "warning",
            "Delivered" => "success",
            "Completed" => "success",
            "Order Confirmed" => "primary",
            "Delivery Unsuccessful" => "danger",
            "Not Delivered" => "danger",
            "Product Returned" => "info",
        ];

        foreach ($orders as $index=>$order) {

            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('seller.orders.view', ['id'=>$order->id]).'" class="btn font-yellow-gold pd-lr-0" title="View Order Details">
                <span aria-hidden="true" class="icon-eye"></span>
            </a>
            <!-- <a href="javascript:;" title="Download Invoice" class="btn font-blue-steel pd-lr-0" data-placement="top" data-original-title="Change Order Status" data-toggle="tooltip" data-action="change-status">
                <span aria-hidden="true" class="icon-settings"></span>
            </a> -->
            </div>':'';

            $order_status = OrderDetail::getOrderStatusData($order->order_status_id)->seller_text;
            $status = $statusList[$order_status];

            /**
             * if the order created data is less than 07-july-2021, then show the price cloumn
             * order created date is more than 07-july-2021, then show the seller_price cloumn
            */
            $unitPrice = (strtotime('2021-07-07 00:00:00') > strtotime($order->created_at)) ? $order->price : $order->seller_price;
               
            $total_discount = isset($order->deal_price) ? ($order->mrp-$unitPrice) : $order->discount;
            
            $records["data"][] = [
                '<div style="width:20px;text-align:center;margin:0 auto;"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$order->id.'"/><span></span></label></div>',
                '<div class="text-center" style="width:80px;white-space: normal;word-break:break-word;">'.date('d-m-Y H:i A', strtotime($order->created_at)).'</div>',
                '<div class="text-center fw-600" style="white-space: normal;word-break:break-word;"><a href="'.route('seller.orders.view', ['id'=>$order->id]).'" class="" title="View Order Details">'.$order->child_order_id.'</div>',

                '<div class="text-center" style="width: 200px;white-space: normal;word-break:break-word;margin: 0 auto";><a href="https://fabpik.in/products/'.$order->productVarient->slug.'/'.$order->productVarient->unique_id.'" target="_blank" class="" title="View Product Details">'.$order->productVarient->name.'</a></div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$order->mrp.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.number_format((float)$order->discount, '2').'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.number_format((float)$unitPrice, '2').'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$order->quantity.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.($unitPrice)*($order->quantity).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;"><span class="badge badge-light-'.($status).' badge-roundless">'.$order_status.'</span></div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.
                    $actionBtns.
                '</div>',
                '<a href="https://fabpik.in/products/'.$order->productVarient->slug.'/'.$order->productVarient->unique_id.'" target="_blank" class="" title="View Product Details">'.$order->productVarient->sku.'</a>',
                $order->order->shipping_pincode,
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.!is_null($order->user_customization_text) ? $order->user_customization_text : '-'.'</div>',                
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [View Parent Products]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function view($id)
    {
        $this->data['title'] = 'Order Details';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Orders',
            ],
            (object) [
                'url' => '',
                'title' => 'Order Details',
            ]
        ];

        $this->data['id'] = $id;
        $this->data['orderDetails'] = OrderDetail::with('order','seller', 'productVarient', 'orderHistory')->where('id', $id)->first();
        $this->data['orderHistory'] = orderHistory::where('order_detail_id', $id)->orderBy('id','desc')->get();
        // $this->data['orders'] = Order::with('customer', 'customerAddress')->where('id', $this->data['orderDetails']->order_id)->first();
        
        $productVarientOptions = ProductVariantOption::where('product_variant_id', $this->data['orderDetails']->productVarient->id)->get();
        $attributes = [];
        foreach ($productVarientOptions as $i=>$options) {

            $getAttributes = Attribute::with(['attributeOptions' => function($q) use ($options){
                    $q->where( 'id', '=', $options->attribute_option_id );
            }])->where('id', $options->attribute_id)->first();
            // $attributes[$i]['atrributeName'] = $getAttributes->display_name;
            // $attributes[$i]['atrributeValue'] = $getAttributes->attributeOptions[0]->option_name;
            $attributes[$i]['atrributeName'] = isset($getAttributes->display_name) ? $getAttributes->display_name : '-';
            $attributes[$i]['atrributeValue'] = isset($getAttributes->attributeOptions[0]) ? $getAttributes->attributeOptions[0]->option_name : '-';
        }
        $this->data['attributeOptions'] = $attributes;
        $this->data['paymentStatus'] = PaymentStatus::get();
        $this->data['orderStatus'] = OrderStatus::get();
        $this->data['shippingStatus'] = ShippingStatus::get();

        return parent::sellerView('orders.orderDetails', $this->data);
    }

    /**
     * [View Invoice Products]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function custInvoice($id)
    {
        $this->data['title'] = 'Customer Invoice';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Orders',
            ],
            (object) [
                'url' => '',
                'title' => 'Customer Invoice',
            ]
        ];
        $this->data['orderDetails'] = OrderDetail::with('order','seller', 'productVarient', 'orderHistory')->where('id', $id)->first();
        $productVarientOptions = ProductVariantOption::where('product_variant_id', $this->data['orderDetails']->productVarient->id)->get();
        $attributes = [];
        foreach ($productVarientOptions as $i=>$options) {
            
            $getAttributes = Attribute::with(['attributeOptions' => function($q) use ($options){
                    $q->where( 'id', '=', $options->attribute_option_id );
            }])->where('id', $options->attribute_id)->first();

            $attributes[$i]['atrributeName'] = $getAttributes->display_name;
            $attributes[$i]['atrributeValue'] = $getAttributes->attributeOptions[0]->option_name;
        }
        $this->data['attributeOptions'] = $attributes;
        return parent::sellerView('orders.customerinvoice', $this->data);
    }
    
    /**
     * [Change Order Status]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function changOrderStatus(Request $request, $id)
    {
        DB::beginTransaction();
        try {                
            $res = __changeStatus($request->input('status'), [$id]);
            $result = json_decode($res);

            if($result->status == 0){
                DB::rollback();
            }else if($result->status == 1){
                DB::commit();
            }

            return response()->json(['success'=>$result->status, 'message'=>$result->message], 200);    
            
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }

    
    /**
     * [Download Invoice Products]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function downloadCustomerInvoice($id)
    {
        // __downloadCustomerInvoice($id);
        $data['orderDetails'] = OrderDetail::with('order', 'seller', 'productVarient', 'orderHistory')->where('id', $id)->first();
        $productVarientOptions = ProductVariantOption::where('product_variant_id', $data['orderDetails']->productVarient->id)->get();
        $attributes = [];
        foreach ($productVarientOptions as $i=>$options) {
            $getAttributes = Attribute::with(['attributeOptions' => function ($q) use ($options) {
                $q->where('id', '=', $options->attribute_option_id);
            }])->where('id', $options->attribute_id)->first();

            $attributes[$i]['atrributeName'] = $getAttributes->display_name;
            $attributes[$i]['atrributeValue'] = $getAttributes->attributeOptions[0]->option_name;
        }
        $data['attributeOptions'] = $attributes;

        // $pdf = PDF::loadView('customer.orders.customerinvoice', $data);
        $pdf = PDF::loadView('invoices.customerinvoice', $data);
        return $pdf->download($data['orderDetails']->child_order_id.'.pdf');
    }
}

<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models

// Exports

class ConversationController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'conversations';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [conversations listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['title'] = 'Conversations';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Conversations',
            ],
        ];

        return parent::sellerView('conversations.conversations', $this->data);
    }
}
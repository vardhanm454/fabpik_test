<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models
use App\Models\Product;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Seller;
use App\Models\ChildCategory;
use App\Models\ProductCategory;
use App\Models\Sizechart;
use App\Models\VariationImage;
use App\Models\ProductVariant;
use App\Models\ProductVariantOption;
use App\Models\Country;
use App\Models\IronType;
use App\Models\WashingType;
use App\Models\Review;
use App\Models\Customer;

class ReviewsController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'reviews';

    public function __construct() {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });        
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */

    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('reviews_perpage', 10);
        $this->data['dt_page'] = Session::get('reviews_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,10';
        $this->data['dt_ajax_url'] = route('seller.reviews.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus','fcustomers','fprovariants', 'funiqueid', 'fpricefrom','fpriceto', 'ffeatured', 'ffromdate', 'ftodate', 'fcategory', 'fsku'];

        $this->data['title'] = 'Reviews';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => '',
                'title' => 'Reviews',
            ],
        ];

        //get all sellers
        $customers = Customer::get();
        $this->data['customers'] = $customers;

        return parent::sellerView('reviews.index', $this->data);
    }

    /**
     * [get all Products and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */

    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'id',
            2 => 'child_order_id',
            3 => 'name',
            6 => 'review_comment',
            7 => 'rating',
            9 => 'published',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('Product', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Review::whereIn('id', $request->input('id'))->delete();
                DB::commit();
                $records["customActionMessage"] = 'Selected Reviews are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        if ($request->input('customActionName') == "ReviewPublish") {
            DB::beginTransaction();
            try {
                Review::whereIn('id',$request->input('id'))
                        ->update(['published'=>$request->input('status')]);
                DB::commit();
                
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Publish Status Changed successfully.';
                
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            // 'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
            'fcustomers' => (!is_null($request->fcustomers))?$request->fcustomers:null,
            'fpricefrom' => (!is_null($request->fpricefrom))?$request->fpricefrom:null,
            'fpriceto' => (!is_null($request->fpriceto))?$request->fpriceto:null,
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'ffeatured' => (!is_null($request->ffeatured))?$request->ffeatured:null,
            'fcategory' => (!is_null($request->fcategory))?$request->fcategory:null,
            'fsku' => (!is_null($request->fsku))?$request->fsku:null,
            'fprovariants' => (!is_null($request->fprovariants))?$request->fprovariants:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:$this->loggedInUser->ref_id,
        ];

        $reviews = Review::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $reviews->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];
        $index = 0;
        foreach ($reviews as $review) {
            $status = $statusList[$review->status];
            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$review->id.'"/><span></span></label>',
                ++$index,
                $review->child_order_id,
                '<span class="">'.($review->customer_name).'</span>',
                '<span class="">'.($review->customer_email).'</span>',
                '<span class="">'.($review->name).'</span>',
                $review->review_title,
                $review->review_comment,
                $review->rating,
                date("d-m-Y", strtotime($review->updated_at)),
                $review->published == 1 ? 'Yes' : 'No',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [Delete Single Product data]
     * @param  Request $request [description]
     * @param  [type]  $id      [product id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            $delete = Product::where('id', $id)->delete();
            ProductVariant::where('product_id', $id)->delete();
            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }
}

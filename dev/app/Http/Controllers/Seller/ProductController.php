<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;
use File;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// Models
use App\Models\Product;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Seller;
use App\Models\ChildCategory;
use App\Models\ProductCategory;
use App\Models\Sizechart;
use App\Models\VariationImage;
use App\Models\ProductVariant;
use App\Models\ProductVariantOption;
use App\Models\Country;
use App\Models\IronType;
use App\Models\WashingType;
use App\Models\AssignedProductTag;
use App\Models\StockNotify;
use App\Models\Notifications;
use App\Models\Customer;

// Imports
use App\Imports\ProductImport;
use App\Imports\SampleProductImport;
use App\Imports\ProductUpdate;

// Exports
use App\Exports\ProductExport;
use App\Exports\CategoriesExport;
use App\Exports\FullProductsExport;

class ProductController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'products';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('products_perpage', 50);
        $this->data['dt_page'] = Session::get('products_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,10';
        $this->data['dt_ajax_url'] = route('seller.products.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus','fseller','fpricefrom','fpriceto', 'funiqueid','fpcategory', 'ffeatured', 'ffromdate', 'ftodate', 'fcategory', 'fsku'];

        $this->data['title'] = 'Products';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Products',
            ],
        ];

        //get all sellers
        $sellers = Seller::get();
        $this->data['sellers'] = $sellers;
        $this->data['categoryList'] = Category::where('status', 1)->get();

        return parent::sellerView('products.index', $this->data);
    }

    /**
     * [get all Products and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'id',
            2 => 'name',
            3 => 'id',
            4 => 'id',
            5 => 'id',
            6 => 'sku',
            7 => 'created_at',
            8 => 'updated_at',
            9 => 'status',
            10 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('Product', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Product::whereIn('id', $request->input('id'))->delete();
                ProductVariant::whereIn('product_id', $request->input('id'))->delete();
                DB::commit();
                $records["customActionMessage"] = 'Selected Products are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        if ($request->input('customActionName') == "tagupdate") {
            DB::beginTransaction();
            try {
                // delete old Assigned Product Tags
                AssignedProductTag::whereIn('product_id', $request->input('id'))->delete();
                
                if (!is_null($request->input('product_tags'))) {
                    foreach ($request->input('id') as $pid) {
                        foreach ($request->input('product_tags') as $tags) {
                            $assignedTag = new AssignedProductTag();
                            $assignedTag->product_id = $pid;
                            $assignedTag->tag_id = $tags;
                            $assignedTag->created_by = $this->loggedInUser->id;
                            $assignedTag->save();
                        }
                    }
                }

                // Product::whereIn('id', $request->input('id'))->update([
                //     'product_tags' => implode( ',', $request->input('product_tags') ),
                // ]);

                DB::commit();
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Selected Products Tags updated successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        $criteria = (object)[
            'length' => intval($request->length),
            'seller' => $this->loggedInUser->ref_id,
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fpricefrom' => (!is_null($request->fpricefrom))?$request->fpricefrom:null,
            'fpriceto' => (!is_null($request->fpriceto))?$request->fpriceto:null,
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'ffeatured' => (!is_null($request->ffeatured))?$request->ffeatured:null,
            'fcategory' => (!is_null($request->fcategory))?$request->fcategory:null,
            'fsku' => (!is_null($request->fsku))?$request->fsku:null,
            'funiqueid' => (!is_null($request->funiqueid))?$request->funiqueid:null,
            'fpcategory' => (!is_null($request->fpcategory))?$request->fpcategory:null,
        ];
        $products = Product::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $products->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin') || $this->loggedInUser->hasRole('Seller'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($products as $index=>$product) {
            $status = $statusList[$product->status];
            // $featured = $featuredList[$product->featured];

            // $actionBtns = ($canChange)?'<a href="'.route('seller.products.edit', ['id'=>$product->id]).'" class="btn blue tooltips" data-toggle="tooltip" data-placement="top" data-original-name="Edit"><i class="fa fa-edit"></i></a>&nbsp;<a href="javascript:;" del-url="'.route('seller.products.delete', ['id'=>$product->id]).'" class="btn red dt-list-delete tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>':'';
            $actionBtns = ($canChange)?'<div class="btn-group">
                <a class="" href="javascript:;" data-toggle="dropdown">
                    <i class="fa fa-ellipsis-v fa-lg" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="'.route('seller.products.variants', ['id'=>$product->id]).'" title="View Variants" class="">
                        <span aria-hidden="true" class="icon-eye"></span> View Variants
                        </a>
                    </li>
                    <li>
                     <a href="'.route('seller.products.variantImages', ['id'=>$product->id]).'" title="" class="">
                        <span aria-hidden="true" class="icon-picture"></span> Variant Images
                     </a>
                    </li> 
                    <li>
                     <a href="'.route('seller.products.edit', ['id'=>$product->id]).'" title="" class="">
                        <span aria-hidden="true" class="icon-note"></span> Edit
                     </a>
                    </li> 
                    <li>
                     <a href="javascript:;" del-url="'.route('seller.products.delete', ['id'=>$product->id]).'" class="dt-list-delete" title="">
                        <span aria-hidden="true" class="icon-trash"></span> Delete
                     </a>
                    </li>
                </ul>
         </div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$product->id.'"/><span></span></label>',
                '<a href="'.route('seller.products.edit', ['id'=>$product->id]).'" title="" class="">'.$product->unique_id.'</a>',
                '<span class="">'.($product->name).'</span>',
                '<img src="'.route('ajax.previewImage', ['image'=>$product->images,'type'=>'category']).'" class="product-thumbnail-img" alt="'.$product->name.'" width="75"/>',
                '<a href="#">'.($product->seller->name).'</a>',
                $product->category->title,
                $product->sku,
                date('d M Y', strtotime($product->created_at)),
                date('d M Y', strtotime($product->updated_at)),
                // '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>',
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export Products]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null,
            'seller' => $this->loggedInUser->ref_id,
        ];
        $products = Product::getExportData($criteria);
        return Excel::download(new ProductExport($products), 'products.xlsx');
    }

    /**
     * [Add new Product]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function add(Request $request)
    {
        if ($request->save) {
            // dd($request->all());
            $rules = [
                'product_name' => 'required',
                'brand' => 'required',
                'primary_category'=>'required',
                // 'product_hsn'=>'required|unique:products,hsn_code',
                'product_categories'=>'required',
                'product_length'=>'required|numeric',
                'product_height'=>'required|numeric',
                'product_breadth'=>'required|numeric',
                'product_unit'=>'required|alpha',
                'shipping_hours'=>'required|numeric',
                'items_in_package'=>'required',
                'product_weight'=>'required|regex:/^\d+(\.\d{1,5})?$/',
                'featured_seller'=>'required',
                'no_of_items'=>'required|numeric',
                'country_of_origin'=>'required',
                'description'=>'required',
                'product_mrp'=>'required|numeric|min:0',
                // 'product_selling_price'=>isset($request->product_selling_price) ? 'required|regex:/^\d+(\.\d{1,2})?$/|min:0' : '',
                'product_invoice_tax'=>'required|numeric',
                'product_sku'=>'required',
                // 'product_discount'=>isset($request->product_discount) ? 'lte:product_mrp|numeric|min:0' : ''
                'product_discount'=>'lte:product_mrp|numeric|min:0',
                'product_selling_price'=>'required|regex:/^\d+(\.\d{1,2})?$/|min:0',
                'min_stock_quantity'=>'required|gte:1',
                'is_customized' => 'required',
            ];

            if ($request->primary_category == CLOTHING_CATEGORY) {
                $rules['iron_type'] = "required";
                $rules['washing_type'] = "required";
                $rules['dress_material'] = "required";
                $rules['size_chart'] = 'required';
            }

            $validator = Validator::make($request->all(), $rules, [
                'shipping_hours.required' => 'Minimum shipping hours field is required.',
                'product_discount.lte' => 'The product discount must be less than MRP and Minimum is 0.',
                'is_customized.required' => 'Customizable field is required.',
            ]);

            if (!empty($request->product_sku)) {
                $isExist = Product::where('sku', $request->product_sku)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('product_sku', 'SKU already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            try {
                DB::beginTransaction();
                $product = new Product();
                $product->name = $request->product_name;
                $product->slug = __generateSlug($request->product_name);
                $product->unique_id = __generateProductSerial();
                $product->brand_id = $request->brand;
                $product->primary_category = $request->primary_category;
                $product->sku = $request->product_sku;
                $product->hsn_code = $request->product_hsn;
                // $product->product_tags = implode(',',$request->product_tags);

                if ($request->primary_category == CLOTHING_CATEGORY) {
                    $product->iron_type = $request->iron_type;
                    // $product->washing_type = implode(',',$request->washing_type);  
                    $product->washing_type = $request->washing_type;  
                    $product->dress_material = $request->dress_material;
                } else {
                    $product->iron_type = null;
                    $product->washing_type = null;
                    $product->dress_material = null;
                }

                $product->seller_id = $this->loggedInUser->ref_id;
                $product->length = $request->product_length;
                $product->breadth = $request->product_breadth;
                $product->height = $request->product_height;
                $product->unit = $request->product_unit;
                $product->size_chart_id = $request->size_chart;
                $product->min_ship_hours = $request->shipping_hours;
                $product->weight = $request->product_weight;
                $product->return_avbl = intval($request->is_return);
                $product->cancel_avl = intval($request->is_cancel_available);
                $product->refund_avl = intval($request->is_refundable);
                $product->seller_featured = intval($request->featured_seller);
                $product->no_of_items = $request->no_of_items;
                $product->items_in_package = $request->items_in_package;
                $product->country_of_origin = $request->country_of_origin;
                $product->description = $request->description;
                $product->mrp = $request->product_mrp;
                $product->min_stock_quantity = $request->min_stock_quantity;
                $product->is_customized = $request->is_customized;
                // ( isset($request->product_discount) && !is_null($request->product_discount) )? $product->seller_discount = $request->product_discount : $product->seller_discount = 0;
                // ( isset($request->product_selling_price) && !is_null($request->product_discount) ) ? $product->sell_price = $request->product_selling_price : $product->sell_price = $request->product_mrp;
                
                $product->sell_price = $request->product_selling_price;
                $product->seller_discount = $request->product_discount;

                $product->fabpik_seller_price = $request->product_selling_price;
                $product->fabpik_seller_discount = $request->product_discount;
                $product->fabpik_seller_discount_percentage = 100*($request->product_mrp-$request->product_selling_price)/$request->product_mrp;
                
                $product->tax = $request->product_invoice_tax;
                $product->meta_title = $request->product_meta_title;
                $product->meta_keywords = $request->meta_keywords;
                $product->meta_description = $request->product_meta_description;
                $product->save();

                foreach ($request->product_categories as $childCategory) {
                    @list($categoryId,$subCategoryId,$childCategoryId) = explode('-', $childCategory);

                    $tblData = new ProductCategory();
                    $tblData->product_id = $product->id;
                    $tblData->category_id = $categoryId;
                    $tblData->subcategory_id = $subCategoryId;
                    $tblData->childcategory_id = $childCategoryId;
                    $tblData->save();
                }

                if (!is_null($request->product_tags)) {
                    foreach ($request->product_tags as $tags) {
                        $assignedTag = new AssignedProductTag();
                        $assignedTag->product_id = $product->id;
                        $assignedTag->tag_id = $tags;
                        $assignedTag->created_by = $this->loggedInUser->id;
                        $assignedTag->save();
                    }
                }

                DB::commit();
                return redirect()
                        ->route('seller.products.variantImages', $product->id)
                        ->with(['toast'=>'1','status'=>'success','title'=>'Products','message'=>'Success! Product Added successfully. Upload variant images now.']);
            } catch (Exception $e) {
                DB::rollback();
                return redirect()
                    ->route('seller.products')
                    ->with(['toast'=>'1','status'=>'error','title'=>'Products','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['title'] = 'Add Product';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'seller.products',
                'title' => 'Products',
            ],
            (object) [
                'url' => '',
                'title' => 'Add Products',
            ],
        ];

        //get all drop down actions
        $this->data['sizecharts'] = Sizechart::where('seller_id', $this->loggedInUser->ref_id)->get();
        $this->data['sellerDetails'] = Seller::where('id', $this->loggedInUser->ref_id)->first();
        $this->data['categories'] = Category::select('id','title')->get();
        $this->data['brands'] = Brand::select('id','name', 'created_by')->whereNull('created_by')->orWhere('created_by', $this->loggedInUser->ref_id)->get();
        $this->data['sellers'] = Seller::select('id','name')->get();
        $this->data['countries'] = Country::select('id','name')->get();
        $this->data['washingTypes'] = WashingType::select('id','name')->get();
        $this->data['ironTypes'] = IronType::select()->get('id','name');
        return parent::sellerView('products.addEditProducts', $this->data);
    }

    /**
     * [Edit the existing Product]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function edit(Request $request, $id)
    {
        $editProduct = Product::find($id);
        
        if ($request->save) {
            // dd(implode(',',$request->product_tags));
            $rules = [
                'product_name' => 'required',
                'brand' => 'required',
                'product_categories'=>'required',
                 // 'product_hsn'=>'required|unique:products,hsn_code',
                'primary_category'=>'required',
                'product_length'=>'required|numeric',
                'product_height'=>'required|numeric',
                'product_breadth'=>'required|numeric',
                'product_unit'=>'required|alpha',
                'shipping_hours'=>'required|numeric',
                'product_weight'=>'required|regex:/^\d+(\.\d{1,5})?$/',
                'featured_seller'=>'required',
                'no_of_items'=>'required|numeric',
                'items_in_package'=>'required',
                'country_of_origin'=>'required',
                'description'=>'required',
                'product_mrp'=>'required|numeric|min:0',
                // 'product_selling_price'=>isset($request->product_selling_price) ? 'required|regex:/^\d+(\.\d{1,2})?$/|min:0' : '',
                // 'product_discount'=> isset($request->product_discount) ? 'lte:product_mrp|numeric|min:0' : '',
                'product_selling_price'=> 'required|regex:/^\d+(\.\d{1,2})?$/|min:0',
                'product_discount'=> 'lte:product_mrp|numeric|min:0',
                'product_invoice_tax'=>'required|numeric',
                'product_status'=>'required',
                'product_sku'=>'required',
                'min_stock_quantity'=>'required|gte:1',
                'is_customized' => 'required',
            ];

            if ($request->primary_category == CLOTHING_CATEGORY) {
                $rules['iron_type'] = "required";
                $rules['washing_type'] = "required";
                $rules['dress_material'] = "required";
                $rules['size_chart'] = 'required';
            }

            $validator = Validator::make($request->all(), $rules, [
                'shipping_hours.required' => 'Minimum shipping hours field is required.',
                'product_discount.lte' => 'The product discount must be less than MRP and Minimum is 0.'
            ]);
            
            // if (!empty($request->product_sku)) {
            //     $isExist = Product::where('sku', $request->product_sku)->where('id', '<>', $id)->exists();
            //     if ($isExist) {
            //         $validator->after(function ($validator) {
            //             $validator->errors()->add('product_sku', 'SKU already exist!');
            //         });
            //     }
            // }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            try {
                DB::beginTransaction();
                $products = [
                    'name' => $request->product_name,
                    'slug' => __generateSlug($request->product_name),
                    'brand_id' => $request->brand,
                    'primary_category' => $request->primary_category,
                    'length' => $request->product_length,
                    'breadth' => $request->product_breadth,
                    'height' => $request->product_height,
                    'unit' => $request->product_unit,
                    
                    'min_ship_hours' => $request->shipping_hours,
                    'weight' => $request->product_weight,
                    'seller_featured' => $request->featured_seller,
                    'no_of_items' => $request->no_of_items,
                    'items_in_package' => $request->items_in_package,
                    'country_of_origin' => $request->country_of_origin,
                    'description' => $request->description,
                    'mrp' => $request->product_mrp,
                    'sell_price' => $request->product_selling_price,
                    'seller_discount' => $request->product_discount,
                    // 'fabpik_seller_price' => $request->product_selling_price,
                    // 'fabpik_seller_discount' => $request->product_discount,
                    'tax' => $request->product_invoice_tax,
                    'meta_title' => $request->product_meta_title,
                    'meta_keywords' => $request->meta_keywords,
                    'meta_description' => $request->product_meta_description,
                    'status' => $request->product_status,
                    'sku' => $request->product_sku,
                    'hsn_code' => $request->product_hsn,
                    'min_stock_quantity'=>$request->min_stock_quantity,
                    'is_customized' => $request->is_customized,
                    // 'product_tags' => implode(',',$request->product_tags),
                ];

                if ($request->primary_category == CLOTHING_CATEGORY) {
                    $products['iron_type'] = $request->iron_type;
                    // $products['washing_type'] = implode(',',$request->washing_type);
                    $products['washing_type'] = $request->washing_type;
                    $products['dress_material'] = $request->dress_material;
                    $products['size_chart_id'] = $request->size_chart;
                }else if ($request->primary_category == FOOTWEAR_CATEGORY) {
                    $products['size_chart_id'] = $request->size_chart;
                }

                // $product['fabpik_seller_price'] = $request->product_mrp - ( ( $editProduct->fabpik_seller_discount_percentage/100 ) * $request->product_mrp);
                // $product['fabpik_seller_discount'] = ( $editProduct->fabpik_seller_discount_percentage/100 ) * $request->product_mrp ;
 
                $products['cancel_avl'] = ($request->is_cancel_available) ? intval($request->is_cancel_available) : 0;
                $products['refund_avl'] = ($request->is_refundable) ? intval($request->is_refundable) : 0;
                $products['return_avbl'] = ($request->is_return) ? intval($request->is_return) : 0;
                // $products['seller_featured'] = ($request->seller_featured) ? intval($request->seller_featured) : 0;
                // $products['fabpik_featured'] = ($request->fabpik_featured) ? intval($request->fabpik_featured) : 0;

                //logging the table columns which are being updated by seller
                $sellerDetails = Seller::select('seller_code','name')->where('id', $this->loggedInUser->ref_id)->first();
                $changedColumns = [];

                //comparing old values and new values
                if($editProduct->name !== $request->product_name){array_push($changedColumns,'name');}
                if($editProduct->slug !== __generateSlug($request->product_name)){array_push($changedColumns,'slug');}
                if((int)$editProduct->brand_id !== (int)$request->brand){array_push($changedColumns,'brand');}
                if((int)$editProduct->primary_category !== (int)$request->primary_category){array_push($changedColumns,'primary_category');}
                if((float)$editProduct->length !== (float)$request->product_length){array_push($changedColumns,'length');}
                if((float)$editProduct->breadth !== (float)$request->product_breadth){array_push($changedColumns,'breadth');}
                if((float)$editProduct->height !== (float)$request->product_height){array_push($changedColumns,'height');}
                if($editProduct->unit !== $request->product_unit){array_push($changedColumns,'unit');}
                if((int)$editProduct->min_ship_hours !== (int)$request->shipping_hours){array_push($changedColumns,'min_ship_hours');}
                if((float)$editProduct->weight !== (float)$request->product_weight){array_push($changedColumns,'weight');}
                if((int)$editProduct->seller_featured !== (int)$request->featured_seller){array_push($changedColumns,'seller_featured');}
                if((int)$editProduct->no_of_items !== (int)$request->no_of_items){array_push($changedColumns,'no_of_items');}
                if($editProduct->items_in_package !== $request->items_in_package){array_push($changedColumns,'items_in_package');}
                if((int)$editProduct->country_of_origin !== (int)$request->country_of_origin){array_push($changedColumns,'country_of_origin');}
                if($editProduct->description !== $request->description){array_push($changedColumns,'description');}
                if((float)$editProduct->mrp !== (float)$request->product_mrp){array_push($changedColumns,'mrp');}
                if((float)$editProduct->sell_price !== (float)$request->product_selling_price){array_push($changedColumns,'sell_price');}
                if((float)$editProduct->seller_discount !== (float)$request->product_discount){array_push($changedColumns,'seller_discount');}
                if((float)$editProduct->tax !== (float)$request->product_invoice_tax){array_push($changedColumns,'tax');}
                if($editProduct->meta_title !== $request->product_meta_title){array_push($changedColumns,'meta_title');}
                if($editProduct->meta_keywords !== $request->meta_keywords){array_push($changedColumns,'meta_keywords');}
                if($editProduct->meta_description !== $request->product_meta_description){array_push($changedColumns,'meta_description');}
                if((int)$editProduct->status !== (int)$request->product_status){array_push($changedColumns,'status');}
                if($editProduct->sku !== $request->product_sku){array_push($changedColumns,'sku');}
                if($editProduct->hsn_code !== $request->product_hsn){array_push($changedColumns,'hsn_code');}
                if((int)$editProduct->min_stock_quantity !== (int)$request->min_stock_quantity){array_push($changedColumns,'min_stock_quantity');}
                if((int)$editProduct->is_customized !== (int)$request->is_customized){array_push($changedColumns,'is_customized');}
                if($editProduct->iron_type !== $request->iron_type){array_push($changedColumns,'iron_type');}
                if($editProduct->washing_type !== $request->washing_type){array_push($changedColumns,'washing_type');}
                if($editProduct->dress_material !== $request->dress_material){array_push($changedColumns,'dress_material');}
                if((int)$editProduct->cancel_avl !== (int)$request->is_cancel_available){array_push($changedColumns,'cancel_avl');}
                if((int)$editProduct->refund_avl !== (int)$request->is_refundable){array_push($changedColumns,'refund_avl');}
                if((int)$editProduct->return_avbl !== (int)$request->is_return){array_push($changedColumns,'return_avbl');}

                

                // update parent product data
                $update = Product::where('id', $id)->update($products);

                //get old product category
                $oldChildCategories = ProductCategory::where('product_id', $id)->pluck('childcategory_id')->toArray();

                // delete old product category
                ProductCategory::where('product_id', $id)->delete();

                $newChildCategories = [];
                foreach ($request->product_categories as $childCategory) {
                    @list($categoryId,$subCategoryId,$childCategoryId) = explode('-',$childCategory);
                    
                    array_push($newChildCategories,$childCategoryId);
                    // insert category data
                    $tblData = new ProductCategory();
                    $tblData->product_id = $id;
                    $tblData->category_id = $categoryId;
                    $tblData->subcategory_id = $subCategoryId;
                    $tblData->childcategory_id = $childCategoryId;
                    $tblData->save();
                }

                //compare old and new childcategories
                $compareCat = array_diff($oldChildCategories,$newChildCategories);

                if(count($compareCat) > 0){
                $changedColumns = array_push($changedColumns,'category');
                }

                // get old Assigned Product Tags
                $oldAssignedTags = AssignedProductTag::where('product_id', $id)->pluck('tag_id')->toArray();

                // delete old Assigned Product Tags
                AssignedProductTag::where('product_id', $id)->delete();
                
                $newAssignedTags = [];
                if (!is_null($request->product_tags)) {
                    foreach ($request->product_tags as $tags) {

                        array_push($newAssignedTags,$tags);
                        $assignedTag = new AssignedProductTag();
                        $assignedTag->product_id = $id;
                        $assignedTag->tag_id = $tags;
                        $assignedTag->created_by = $this->loggedInUser->id;
                        $assignedTag->save();
                    }
                }

                //compare old and new tags
                $compareTag = array_diff($oldAssignedTags,$newAssignedTags);


                if(count($compareTag) > 0){
                $changedColumns = array_push($changedColumns,'tags');
                }

                $folder = ('uploads'.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.'.log');
                
                //create log file for seller if not exists
                if (!File::exists($folder)) {
                    
                    //creating seller log
                    $log = ['sellerId' => $sellerDetails->seller_code,
                    'sellerName' => $sellerDetails->name];

                    $sellerLog = new Logger($sellerDetails->seller_code);
                    $sellerLog->pushHandler(new StreamHandler('uploads'.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.'.log'), Logger::INFO);
                    $sellerLog->info('Seller Registered.',$log);
                    // dd($orderLog);                
                }

                // dd($changedColumns);
                //append the log data to the existing file in upload folder
                if(count($changedColumns) > 0){
                    $changedColumns = implode(',', $changedColumns);
                    File::append('uploads'.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.'.log',PHP_EOL.date("d-m-Y H:i:s").'|Product:'.$editProduct->unique_id.'|Changed|Columns:'.$changedColumns.'|User:'.$this->loggedInUser->id);
                }
                
                DB::commit();

                if($request->save == 'save')
                    return redirect()
                        ->route('seller.products.variantImages', $id)
                        ->with(['toast'=>'1','status'=>'success','title'=>'Products','message'=>'Success! Product data updated successfully.']);
                else
                    return redirect()
                            ->back()
                            ->with(['toast'=>'1','status'=>'success','title'=>'Products','message'=>'Success! Product Added successfully.']);
            } catch (Exception $e) {
                DB::rollback();
                return redirect()
                    ->route('seller.products')
                    ->with(['toast'=>'1','status'=>'error','title'=>'Products','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['id'] = $id;
        $this->data['editProduct'] = $editProduct;
        $this->data['editProduct']->categories = ProductCategory::where('product_id', $id)->get();
        $this->data['imgSet'] = json_decode($editProduct->images);
        
        $this->data['sellerDetails'] = Seller::where('id', $this->loggedInUser->ref_id)->first();

        //get all drop down actions
        $this->data['categories'] = Category::select('id','title')->get();
        $this->data['brands'] = Brand::select('id','name', 'created_by')->whereNull('created_by')->orWhere('created_by', $this->loggedInUser->ref_id)->get();
        $this->data['sellers'] = Seller::select('id','name')->get();
        $this->data['sizecharts'] = Sizechart::where('seller_id', $editProduct->seller_id)->get();
        $this->data['countries'] = Country::select('id','name')->get();
        $this->data['washingTypes'] = WashingType::select('id','name')->get();
        $this->data['ironTypes'] = IronType::select('id','name')->get();

        $this->data['title'] = 'Edit Product';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'seller.products',
                'title' => 'Products',
            ],
            (object) [
                'url' => '',
                'title' => 'Edit Products',
            ],(object) [
                'url' => '',
                'title' => $editProduct->name,
            ],
        ];

        return parent::sellerView('products.addEditProducts', $this->data);
    }

    /**
     * [Delete Single Product data]
     * @param  Request $request [description]
     * @param  [type]  $id      [product id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            $delete = Product::where('id', $id)->delete();
            ProductVariant::where('product_id', $id)->delete();
            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }

    /**
     * [Add Product Variant Images]
     * @param  Request $request [description]s
     * @return [type]           [description]
     */
    public function variantImages(Request $request, $id)
    {
        if ($request->save) {
             //get seller ID
             $sellerCode = Seller::select('seller_code')->where('id', auth()->user()->ref_id)->first();
             if (!is_null($request->primary_variant_options)) {
                 foreach ($request->primary_variant_options as $option) {
                     $defaultImage = 'default_image_'.$option;
                     $images = [];
 
                     foreach ($request->images[$option] as $image) {
                         $pieces = explode("/", $image);
                         if ($sellerCode->seller_code == $pieces[0]) {
                             array_push($images, ($image!=null)?($image):null);
                         } else {
                             array_push($images, ($image!=null)?($sellerCode->seller_code.'/'.$image):null);
                         }
                     }
                
                     $thumbnail_image = $images[((!is_null($request->$defaultImage)) ? ($request->$defaultImage-1) : 1)];
                     VariationImage::updateOrCreate([
                     'product_id' => $id,
                     'attribute_option_id' => $option
                 ], [
                     'product_id' => $id,
                     'attribute_option_id' => $option,
                     'thumbnail' => (!is_null($request->$defaultImage)) ? ($request->$defaultImage) : 1,
                     'images' => json_encode($images),
                 ]);

                     VariationImage::where('product_id', $id)->where('attribute_option_id', $option)->update(['thumbnail_img_name' => $thumbnail_image]);
                 }
             }
            return redirect()
                        ->route('seller.products.variants', $id)
                        ->with(['toast'=>'1','status'=>'success','title'=>'Variant Images','message'=>'Success! Images added successfully.']);
        }

        $product = Product::select('name')->find($id);
        
        $this->data['title'] = 'Variant Images';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'seller.products',
                'title' => 'Products',
            ],
            (object) [
                'url' => '',
                'title' => $product->name,
            ],
            (object) [
                'url' => false,
                'title' => 'Variant Images',
            ],
        ];

        //get the primary attribute and secondary attribute id based on product primary category
        $this->data['primaryAttributes'] = Product::getPrimaryAttrOptions($id);

        $varientImages = VariationImage::where('product_id', $id)->get();
        foreach ($varientImages as $varientImage) {
            $this->data['getVarientImages'][$varientImage->attribute_option_id] = [
                'thumbnail' => $varientImage->thumbnail,
                'images' => json_decode($varientImage->images)
            ];
        }

        $this->data['id'] = $id;
        return parent::sellerView('products.varientImages', $this->data);
    }

    /**
    * [Add new Product Variant]
    * @param  Request $request [description]
    * @return [type]           [description]
    */
    public function variants(Request $request, $id)
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('products_perpage', 10);
        $this->data['dt_page'] = Session::get('products_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('seller.products.getAjaxListVarientsData', ['productId'=>$id]);
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Product Variants';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'seller.products',
                'title' => 'Products',
            ],
            (object) [
                'url' => '',
                'title' => 'Product Variants',
            ],
        ];
       
        $this->data['id'] = $id;
        return parent::sellerView('products.productVarients', $this->data);
    }

    /**
     * [get all Product varients and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListVarientsData(Request $request, $productId)
    {
        $columnList = [
            0 => 'id',
            1 => 'id',
            2 => 'name',
            3 => 'sku',
            4 => 'id',
            5 => 'id',
            6 => 'id',
            7 => 'mrp',
            8 => 'discount',
            9 => 'price',
            10 => 'stock',
            11 => 'id',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('ProductVariant', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                ProductVariant::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Variants are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null
        ];
        $productVariants = ProductVariant::getAjaxListVarientsData($criteria, $iPage, $orderColumn, $orderDir, $productId);

        $iTotalRecords = $productVariants->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin') || $this->loggedInUser->hasRole('Seller'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($productVariants as $index=>$productvarient) {
            // $status = $statusList[$product->status];
            // $featured = $featuredList[$product->featured];

            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('seller.products.variantEdit', ['id'=>$productId, 'vid'=>$productvarient->id], '.edit').'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-name="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('seller.products.variantDelete', ['vid'=>$productvarient->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a></div>':'';
            $attributes = __getVariantOptionValues($productvarient->id);
            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$productvarient->id.'"/><span></span></label>',
                $productvarient->unique_id,
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$productvarient->name.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$productvarient->sku.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$attributes['PrimaryAttrValue'].'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$attributes['SecondaryAttrValue'].'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">
                <img src="'.url('/').'/'.UPLOAD_PATH.'/'.__getVariantImages($productvarient->id).'" width="50" height="">
                </div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$productvarient->mrp.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.number_format((float)$productvarient->discount, '1').'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$productvarient->price.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$productvarient->stock.'</div>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [Add new Product Variant]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function variantAdd(Request $request, $id)
    {
        if ($request->save) {
            $input = [
                'varient_name' => 'required',
                'variant_mrp' => 'required|numeric',
                // 'variant_selling_price' => isset($request->variant_selling_price) ? 'required|regex:/^\d+(\.\d{1,2})?$/' : '',
                // 'variant_discount' => isset($request->variant_discount) ? 'lt:variant_mrp' : '',
                'variant_selling_price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'variant_discount' =>  'integer|min:0|lt:variant_mrp',
                'variant_weight' => 'required|regex:/^\d+(\.\d{1,5})?$/',
                'variant_length' => 'required|numeric',
                'variant_height' => 'required|numeric',
                'variant_breadth' => 'required|numeric',
                'variant_stock' => 'required|numeric',
                'variant_shipping_hours' => 'numeric',
                'variant_min_quantity' => 'required|numeric|lte:variant_stock|min:1',
                'variant_option_primary' => 'required',
                'product_sku' => 'required|unique:product_variants,sku',
                // 'product_hsn' => 'required|numeric|unique:product_variants,hsn_code',
                'min_stock_quantity'=>'required|gte:1',
            ];

            if($request->primary_attribute_display_name == 'Books & Art'){
                $input['product_isbn'] = 'required|regex:/^(?=(?:\D*\d){10}(?:(?:\D*\d){3})?$)[\d-]+$/|unique:product_variants,isbn_code';
            }

            $messages = [
                'variant_option_primary.required' => 'Primary Option is required.'
            ];

            if($request->secondary_attribute_id != null) {
                $input['variant_option_secondary'] = 'required';
                $messages['variant_option_secondary.required'] = 'Secondary Option is required.';
            }

            $validator = Validator::make($request->all(), $input, $messages);

            $products_primary = Product::with(['productvariant', 'productvariant.product_variant_options'=> function($q) use($request){
                $q->where('attribute_id', $request->primary_attribute_id);
                $q->where('attribute_option_id', $request->variant_option_primary);
            }])->where('id', $id)->first();
            
            $isPrimaryExists = false;
            $isSecondaryExists = false;
            if(count($products_primary->productvariant) != 0){
                foreach($products_primary->productvariant as $variants){
                    if(count($variants->product_variant_options) !=0 ){
                        $isPrimaryExists = true;
                    }
                }
            }

            if ($request->secondary_attribute_id != null) {

                $products_secondary = Product::with(['productvariant', 'productvariant.product_variant_options'=> function($q) use($request){
                    $q->where('attribute_id', $request->secondary_attribute_id);
                    $q->where('attribute_option_id', $request->variant_option_secondary);
                }])->where('id', $id)->first();

                if(count($products_secondary->productvariant) != 0){
                    foreach($products_secondary->productvariant as $variants){
                        if(count($variants->product_variant_options) !=0 ){
                            $isSecondaryExists = true;
                        }
                    }
                }

                if($isPrimaryExists && $isSecondaryExists){
                    $validator->after(function ($validator) {
                        $validator->errors()->add('variant_option_primary', 'Product Varient already exist!');
                    });
                }
            }else{
                if ($isPrimaryExists) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('variant_option_primary', 'Product Varient already exist!');
                    });
                }
            }   
            
            if (!empty($request->product_sku)) {
                $isExist = ProductVariant::where('sku', $request->product_sku)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('product_sku', 'SKU already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
            try {                
                $productVarients = new ProductVariant();
                $productVarients->product_id = $id;
                $productVarients->name = $request->varient_name;
                $productVarients->slug = __generateSlug($request->varient_name);
                $productVarients->unique_id = __generateNewProductVariantSerial($id, $request->product_unique_id);
                $productVarients->mrp = $request->variant_mrp;
                // $productVarients->price = ( isset($request->variant_selling_price) && !is_null($request->variant_selling_price) ) ? $request->variant_selling_price : $request->variant_mrp;
                // $productVarients->discount = ( isset($request->variant_discount) && !is_null($request->variant_discount) ) ? $request->variant_discount : 0;
                $productVarients->price = $request->variant_selling_price;
                $productVarients->discount = $request->variant_discount;

                $productVarients->fabpik_seller_price = $request->variant_selling_price;
                $productVarients->fabpik_seller_discount = $request->variant_discount;
                $productVarients->fabpik_seller_discount_percentage = 100*($request->variant_mrp-$request->variant_selling_price)/$request->variant_mrp;

                $productVarients->stock = $request->variant_stock;
                $productVarients->min_order_qty = $request->variant_min_quantity;
                $productVarients->min_ship_hours = $request->variant_shipping_hours;
                $productVarients->shipping_weight = $request->variant_weight;
                $productVarients->shipping_length = $request->variant_length;
                $productVarients->shipping_breadth = $request->variant_breadth;
                $productVarients->shipping_height = $request->variant_height;
                $productVarients->sku = $request->product_sku;
                $productVarients->min_stock_quantity = $request->min_stock_quantity;
                // $productVarients->hsn_code = $request->product_hsn;
                $productVarients->isbn_code = $request->product_isbn;
                if ($request->is_default) {
                    ProductVariant::where('product_id', $id)->update(['is_default' => 0]);
                    $productVarients->is_default = intval($request->is_default);
                }
                $productVarients->save();
            
                $productVarientOptions = new ProductVariantOption();
                $productVarientOptions->product_variant_id = $productVarients->id;
                $productVarientOptions->attribute_id = $request->primary_attribute_id;
                $productVarientOptions->attribute_option_id = $request->variant_option_primary;
                $productVarientOptions->save();

                if(!is_null($request->variant_option_secondary)){
                    $productVarientOptionsSecodary = new ProductVariantOption();
                    $productVarientOptionsSecodary->product_variant_id = $productVarients->id;
                    $productVarientOptionsSecodary->attribute_id = $request->secondary_attribute_id;
                    $productVarientOptionsSecodary->attribute_option_id = $request->variant_option_secondary;
                    $productVarientOptionsSecodary->save();
                }

                DB::commit();
                return redirect()
                        ->route('seller.products.variants',['id'=>$id])
                        ->with(['toast'=>'1','status'=>'success','title'=>'Products','message'=>'Success! Product Added successfully.']);
            } catch (Exception $e) {
                DB::rollback();
                return redirect()
                        ->route('seller.products.variants',['id'=>$id])
                        ->with(['toast'=>'1','status'=>'error','title'=>'Products','message'=>'Error! Some error occured, please try again.']);
            }
        }
        $this->data['title'] = 'Add New Variant';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'seller.products',
                'title' => 'Products',
            ],
            (object) [
                'url' => '',
                'title' => 'Add New Variant',
            ],
        ];

        $this->data['sellerDetails'] = Seller::where('id', $this->loggedInUser->ref_id)->first();
        
        //get the primary attribute and secondary attribute id based on product category id
        $this->data['primaryattributes'] = Product::getPrimaryAttrOptions($id);
        $this->data['secondaryattributes'] = Product::getSecondaryAttrOptions($id);
 
        $this->data['productInfo'] = Product::where('id', $id)->select('unique_id', 'mrp','name','no_of_items', 'seller_discount', 'sell_price', 'weight', 'length', 'height', 'breadth', 'min_ship_hours', 'min_stock_quantity')->first();
        $this->data['id'] = $id;
        return parent::sellerView('products.addEditProductVarient', $this->data);
    }

    /**
     * [Add Existing Product Variant]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function variantEdit(Request $request, $id, $vid)
    {
        $this->data['productVariantData'] = ProductVariant::where('id', $vid)->first();

        if ($request->save) {
        
            $validator = Validator::make($request->all(), [
                'varient_name' => 'required',
                'variant_mrp' => 'required|numeric',
                'variant_selling_price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'variant_discount' => 'integer|min:0|lt:variant_mrp',
                'variant_weight' => 'required|regex:/^\d+(\.\d{1,5})?$/',
                'variant_length' => 'required|numeric',
                'variant_height' => 'required|numeric',
                'variant_breadth' => 'required|numeric',
                'variant_stock' => 'required|numeric',
                'variant_min_quantity' => ($request->variant_stock == 0)?('required|numeric'):('required|numeric|lte:variant_stock|min:1'),
                'variant_option_primary' => 'required',
                'product_sku' => 'required',
                'variant_shipping_hours' => 'numeric',
                'min_stock_quantity'=>'required|gte:1',
                'product_isbn' => ($request->primary_attribute_display_name == 'Books & Art')?('required|regex:/^(?=(?:\D*\d){10}(?:(?:\D*\d){3})?$)[\d-]+$/|unique:product_variants,isbn_code,'.$vid):'',
            ], []);
            
            if (!empty($request->product_sku)) {
                $isExist = ProductVariant::where('sku', $request->product_sku)->where('id', '<>', $vid)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('product_sku', 'SKU already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            $productVrnt = ProductVariant::where([['id', '=', $vid], ['product_id' , '=', $id]])->first();

            try {                
                //all default Values set to zero
                ProductVariant::where('product_id' , $id)->update(['is_default'=>0]);

                $tblData = [
                    'product_id' => $id,
                    'name' => $request->varient_name,
                    'mrp' => $request->variant_mrp,
                    'price' => $request->variant_selling_price,
                    'discount' => $request->variant_discount, 
                    'stock' => $request->variant_stock,
                    'min_order_qty' => $request->variant_min_quantity,    
                    'min_ship_hours' => $request->variant_shipping_hours,
                    'shipping_weight' => $request->variant_weight,
                    'shipping_length' => $request->variant_length,
                    'shipping_breadth' => $request->variant_breadth,
                    'shipping_height' => $request->variant_height,
                    'is_default' =>isset($request->is_default)?1:0,
                    'sku' => $request->product_sku,
                    'hsn_code' => $request->product_hsn,
                    'min_stock_quantity' => $request->min_stock_quantity,
                ];
                
                // $tblData['fabpik_seller_price'] = $request->variant_mrp - ( ( $productVrnt->fabpik_seller_discount_percentage/100 ) * $request->variant_mrp );
                // $tblData['fabpik_seller_discount'] = ( $productVrnt->fabpik_seller_discount_percentage/100 ) * $request->variant_mrp ;
                
                $tblData['fabpik_seller_price'] = DB::raw("ROUND ( ".$request->variant_selling_price." - ( (`fabpik_addon_discount`/100)*".$request->variant_selling_price ." ), 2 )" );
                $tblData['fabpik_seller_discount'] = DB::raw("ROUND ( `mrp` - (". $request->variant_selling_price. " - ( (`fabpik_addon_discount`/100)*".$request->variant_selling_price ." ) ), 2 )" );
                $tblData['fabpik_seller_discount_percentage'] = DB::raw("ROUND ( ( ( ( `mrp` - (". $request->variant_selling_price. " - ( (`fabpik_addon_discount`/100)*".$request->variant_selling_price ." ) ) ) / `mrp` ) * 100 ), 2 )" );
                
                
                if($request->primary_attribute_display_name == 'Books & Art'){
                    $tblData['isbn_code'] = $request->product_isbn;
                }

                //logging the table columns which are being updated by seller
                $sellerDetails = Seller::select('seller_code','name')->where('id', $this->loggedInUser->ref_id)->first();
                $changedColumns = [];

                //comparing old values and new values
                if((int)$productVrnt->product_id !== (int)$id){array_push($changedColumns,'product_id');}
                if($productVrnt->name !== $request->varient_name){array_push($changedColumns,'name');}
                if((float)$productVrnt->mrp !== (float)$request->variant_mrp){array_push($changedColumns,'mrp');}
                if((float)$productVrnt->price !== (float)$request->variant_selling_price){array_push($changedColumns,'price');}
                if((float)$productVrnt->discount !== (float)$request->variant_discount){array_push($changedColumns,'discount');}
                if((int)$productVrnt->stock !== (int)$request->variant_stock){array_push($changedColumns,'stock');}
                if((int)$productVrnt->min_order_qty !== (int)$request->variant_min_quantity){array_push($changedColumns,'min_order_qty');}
                if((int)$productVrnt->min_ship_hours !== (int)$request->variant_shipping_hours){array_push($changedColumns,'min_ship_hours');}
                if((float)$productVrnt->shipping_weight !== (float)$request->variant_weight){array_push($changedColumns,'shipping_weight');}
                if((float)$productVrnt->shipping_length !== (float)$request->variant_length){array_push($changedColumns,'shipping_length');}
                if((float)$productVrnt->shipping_breadth !== (float)$request->variant_breadth){array_push($changedColumns,'shipping_breadth');}
                if((float)$productVrnt->shipping_height !== (float)$request->variant_height){array_push($changedColumns,'shipping_height');}
                if($productVrnt->is_default !== (isset($request->is_default)?1:0)){array_push($changedColumns,'is_default');}
                if($productVrnt->sku !== $request->product_sku){array_push($changedColumns,'sku');}
                if($productVrnt->hsn_code !== $request->product_hsn){array_push($changedColumns,'hsn_code');}
                if((int)$productVrnt->min_stock_quantity !== (int)$request->min_stock_quantity){array_push($changedColumns,'min_stock_quantity');}
                if($request->primary_attribute_display_name == 'Books & Art'){
                    if($productVrnt->isbn_code !== $request->product_isbn){array_push($changedColumns,'isbn_code');}
                }
                
                $update = ProductVariant::where([['id', '=', $vid], ['product_id' , '=', $id]])->update($tblData);

                //fetch the data from the Stock Notify Table
                
                $notifyStock = StockNotify::where('product_variant_id', $vid)->select('user_id')->get();
                if( !$notifyStock->isEmpty() ){
                    
                    //getting all user ids
                    
                    $user_ids = [];

                    foreach($notifyStock as $notify) {
                        $user_ids[] = strval($notify->user_id);
                    }

                    $createNotification = new Notifications();
                    $createNotification->title = $request->varient_name.' Stock Notification';
                    $createNotification->content = $request->varient_name.' Stock Available';
                    $createNotification->notify_on =  date("Y-m-d H:i:s");                 
                    $createNotification->users = json_encode($user_ids);
                    $createNotification->notified_to = 'c';
                    $createNotification->created_by = $this->loggedInUser->ref_id;
                    $createNotification->save();

                    $customerDetails = Customer::whereIn('id', $user_ids)->get();
                    foreach ($customerDetails as $details) {
                        $to['user_email'] = $details->email;
                        __sendEmails($to, 'stock_update', $this->data['productVariantData']);
                    }
                    StockNotify::where('product_variant_id', $vid)->select('user_id')->delete();
                }
                
                
                // get old Product Variantions options
                $oldProductVariantOptions = ProductVariantOption::where('product_variant_id', $vid)->select('attribute_id','attribute_option_id')->get();
                $attributeCount = 0;
                $attributeOptionCount = 0;
                foreach($oldProductVariantOptions as $option){
                    if((int)$option->attribute_id !== (int)$request->primary_attribute_id){++$attributeCount;}
                    if((int)$option->attribute_option_id !== (int)$request->variant_option_primary){++$attributeOptionCount;}
                    if(!is_null($request->variant_option_secondary)){
                        if((int)$option->attribute_id !== (int)$request->secondary_attribute_id){++$attributeCount;}
                        if((int)$option->attribute_option_id !== (int)$request->variant_option_secondary){++$attributeOptionCount;}
                    }
                }
                // dd($attributeCount);
                if($attributeCount > 0){
                    array_push($changedColumns,'attribute_id');
                }
                if($attributeOptionCount){
                    array_push($changedColumns,'attribute_option_id');
                }

                //delete the Product Variations Options and insert again
                ProductVariantOption::where('product_variant_id', $vid)->delete();

                $productVarientOptions = new ProductVariantOption();
                $productVarientOptions->product_variant_id = $vid;
                $productVarientOptions->attribute_id = $request->primary_attribute_id;
                $productVarientOptions->attribute_option_id = $request->variant_option_primary;
                $productVarientOptions->save();
                
                if (!is_null($request->variant_option_secondary)) {
                    $productVarientOptionsSecodary = new ProductVariantOption();
                    $productVarientOptionsSecodary->product_variant_id = $vid;
                    $productVarientOptionsSecodary->attribute_id = $request->secondary_attribute_id;
                    $productVarientOptionsSecodary->attribute_option_id = $request->variant_option_secondary;
                    $productVarientOptionsSecodary->save();
                }

                $folder = ('uploads'.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.'.log');
                
                //create log file for seller if not exists
                if (!File::exists($folder)) {
                    
                    //creating seller log
                    $log = ['sellerId' => $sellerDetails->seller_code,
                    'sellerName' => $sellerDetails->name];

                    $sellerLog = new Logger($sellerDetails->seller_code);
                    $sellerLog->pushHandler(new StreamHandler('uploads'.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.'.log'), Logger::INFO);
                    $sellerLog->info('Seller Registered.',$log);
                    // dd($orderLog);                
                }
                
                //append the log data to the existing file in upload folder
                if(count($changedColumns) > 0){
                    $changedColumns = implode(',', $changedColumns);
                    File::append('uploads'.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.'.log',PHP_EOL.date("d-m-Y H:i:s").'|Product Variant:'.$productVrnt->unique_id.'|Changed|Columns:'.$changedColumns.'|User:'.$this->loggedInUser->id);
                }

                DB::commit();

                if($request->save == 'save')
                    return redirect()
                        ->route('seller.products.variants',['id'=>$id])
                        ->with(['toast'=>'1','status'=>'success','title'=>'Products','message'=>'Success! Variant Product Updated successfully.']);
                else
                    return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'success','title'=>'Products','message'=>'Success! Variant Product Updated successfully.']);               
            } catch (Exception $e) {
                DB::rollback();
                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'error','title'=>'Products','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['title'] = 'Edit Variant';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'seller.products',
                'title' => 'Products',
            ],
            (object) [
                'url' => '',
                'title' => 'Edit Variant',
            ],
        ];

        $this->data['sellerDetails'] = Seller::where('id', $this->loggedInUser->ref_id)->first();
        
        //get the primary attribute and secondary attribute id based on product category id
        $this->data['primaryattributes'] = Product::getPrimaryAttrOptions($id);
        $this->data['secondaryattributes'] = Product::getSecondaryAttrOptions($id);
 
        $this->data['productInfo'] = Product::where('id', $id)->select('mrp', 'seller_discount', 'sell_price', 'weight', 'length', 'height', 'breadth', 'min_ship_hours', 'min_stock_quantity')->first();

        $productVariantOptions = ProductVariantOption::where('product_variant_id', $vid)->get();
        foreach ($productVariantOptions as $varientOptions) {
            $this->data['varientOptions'][$varientOptions->attribute_id] = [
                'attribute_option_id' => $varientOptions->attribute_option_id
            ];
        }
        // dd($this->data['varientOptions']);

        $this->data['id'] = $id;
        $this->data['vid'] = $vid;
        return parent::sellerView('products.addEditProductVarient', $this->data);
    }

    /**
     * [Delete Variant data]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
    */
    public function variantDelete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $delete = ProductVariant::where('id', $id)->delete();
            DB::commit();
            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }

    /**
     * [Bulk Products import]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function import()
    {
        $this->data['categories'] = Category::getCategories();
        $this->data['title'] = 'Import Products';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = 'importproduct';
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'seller.products',
                'title' => 'Import Products',
            ]
        ];
        return parent::sellerView('products.importProducts', $this->data);
    }

    public function processImport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:xls,xlsx', //,csv,txt
        ], [
            'file.required' => 'Please upload a file',
            'file.mimes' => 'Only excel(.xls or .xlsx) file types allowed'
        ]);

        if($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $file = $request->file('file');
        $fileName = md5(uniqid().uniqid()).'.'.$file->getClientOriginalExtension();

        // file stored in server
        $file->move(UPLOAD_PATH.'/imports', $fileName);

        try {
            $import = new ProductImport(auth()->user()->ref_id);
            // $import = new SampleProductImport(auth()->user()->ref_id);
            $import->import(UPLOAD_PATH.'/imports/'.$fileName);

            if($import->getErrors()) {
                // delete file
                @unlink(UPLOAD_PATH.'/imports/'.$fileName);
                
                return redirect()->route('seller.products.import')->with('failures', $import->getErrors());
            }

            // delete file
            @unlink(UPLOAD_PATH.'/imports/'.$fileName);

            return back()->with(['toast'=>'1','status'=>'success','title'=>'Product Import','message'=>'Product(s) imported successfully!']);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();

            // delete file
            @unlink(UPLOAD_PATH.'/imports/'.$fileName);

            return back()->withFailures($failures)->with(['toast'=>'1','status'=>'error','title'=>'Product Import','message'=>'Product import error.']);
        } catch(\Exception $e) {
            dd($e);
            // delete file
            @unlink(UPLOAD_PATH.'/imports/'.$fileName);

            return back()->with(['toast'=>'1','status'=>'error','title'=>'Product Import','message'=>'Error! Please check your import products data.']);
        }        
    }

    /**
     * [Bulk Products update]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function update()
    {
        $this->data['categories'] = Category::getCategories();
        $this->data['title'] = 'Update Products';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = 'updateproduct';
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'seller.products',
                'title' => 'Update Products',
            ]
        ];
        return parent::sellerView('products.updateProducts', $this->data);
    }

    public function processProductUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:xls,xlsx', //,csv,txt
        ], [
            'file.required' => 'Please upload a file',
            'file.mimes' => 'Only excel(.xls or .xlsx) file types allowed'
        ]);

        if($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $file = $request->file('file');
        $fileName = md5(uniqid().uniqid()).'.'.$file->getClientOriginalExtension();

        // file stored in server
        $file->move(UPLOAD_PATH.'/imports', $fileName);

        try {
            $import = new ProductUpdate(auth()->user()->ref_id);
            // $import = new SampleProductImport(auth()->user()->ref_id);
            $import->import(UPLOAD_PATH.'/imports/'.$fileName);

            if($import->getErrors()) {
                // delete file
                @unlink(UPLOAD_PATH.'/imports/'.$fileName);
                
                return redirect()->route('seller.products.update')->with('failures', $import->getErrors());
            }

            // delete file
            @unlink(UPLOAD_PATH.'/imports/'.$fileName);

            return back()->with(['toast'=>'1','status'=>'success','title'=>'Product Import','message'=>'Product(s) imported successfully!']);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();

            // delete file
            @unlink(UPLOAD_PATH.'/imports/'.$fileName);

            return back()->withFailures($failures)->with(['toast'=>'1','status'=>'error','title'=>'Product Import','message'=>'Product import error.']);
        } catch(\Exception $e) {
            dd($e);
            // delete file
            @unlink(UPLOAD_PATH.'/imports/'.$fileName);

            return back()->with(['toast'=>'1','status'=>'error','title'=>'Product Import','message'=>'Error! Please check your import products data.']);
        }        
    }
    
    /**
     * Export all Categories, Sub and Child Categories List 
    */
    public function exportCategoriesList(Request $request)
    {
        $categorieslist = ChildCategory::getCategoriesExportData();
        return Excel::download(new CategoriesExport($categorieslist), 'categorieslist.xlsx');
    }

    /**
     * Export all Products by Category wise 
    */
    public function fullExport(Request $request)
    {
        $criteria = (object)[
            'fseller' => !is_null($request->fseller)?($request->fseller):null,
            'fpcategory' => (!is_null($request->fpcategory))?$request->fpcategory:null,
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
            'fpricefrom' => (!is_null($request->fpricefrom))?$request->fpricefrom:null,
            'fpriceto' => (!is_null($request->fpriceto))?$request->fpriceto:null,
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'ffeatured' => (!is_null($request->ffeatured))?$request->ffeatured:null,
            'fcategory' => (!is_null($request->fcategory))?$request->fcategory:null,
            'fsku' => (!is_null($request->fsku))?$request->fsku:null,
            'funiqueid' => (!is_null($request->funiqueid))?$request->funiqueid:null,
            'fids' => (!is_null($request->fids))?$request->fids:null,
        ];

        $productslist = Product::getProductExportData($criteria);
        
        return Excel::download(new FullProductsExport($productslist, $request->fpcategory, $request->fseller), 'productslist.xlsx');
    }
}

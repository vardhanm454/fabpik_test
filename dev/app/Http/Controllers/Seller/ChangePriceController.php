<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models
use App\Models\Product;
use App\Models\ProductVariant;

class ChangePriceController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'changeprices';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('products_perpage', 10);
        $this->data['dt_page'] = Session::get('products_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('seller.changeprices.getAjaxChangePriceListData');
        $this->data['dt_search_colums'] = ['fname', 'fstatus', 'fseller', 'fpricefrom', 'fpriceto', 'ffromdate', 'ftodate', 'ffeatured', 'fcategory', 'fsku', 'funiqueid', 'fcommissiontype'];

        $this->data['title'] = 'Change Prices';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Change Prices',
            ],
        ];

        return parent::sellerView('changeprices.index', $this->data);
    }

    /**
     * [get all Products and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxChangePriceListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'unique_id',
            2 => 'name',
            3 => 'id',
            4 => 'sku',
            5 => 'mrp',
            6 => 'sell_price',
            7 => 'created_at',
            8 => 'updated_at',
            9 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('Product', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Product::whereIn('id', $request->input('id'))->delete();
                ProductVariant::whereIn('product_id', $request->input('id'))->delete();
                DB::commit();
                $records["customActionMessage"] = 'Selected Products are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        if ($request->input('customActionName') == "changeprice") {
            DB::beginTransaction();
            try {
                $tblUpdateDate = [];
                $tblVariantUpdateDate = [];
                if ($request->discount_type == 'f') {
                    $tblVariantUpdateDate = [
                        'price' => DB::raw('`mrp` - '.$request->discount_amount ),
                        'discount' => $request->discount_amount,
                        'fabpik_seller_price' => DB::raw(" ROUND( (`mrp`- ". $request->discount_amount .") - ( ( `fabpik_addon_discount` / 100 ) * ( `mrp` - ". $request->discount_amount . ") ), 2 )" ),
                        'fabpik_seller_discount' => DB::raw(" ROUND( `mrp` - ( (`mrp`- ". $request->discount_amount .") - ( ( `fabpik_addon_discount` / 100 ) * ( `mrp` - ". $request->discount_amount . ") ) ), 2 )" ),
                        'fabpik_seller_discount_percentage' => DB::raw(" ROUND( ( ( ( `mrp` - ( (`mrp`- ". $request->discount_amount .") - ( ( `fabpik_addon_discount` / 100 ) * ( `mrp` - ". $request->discount_amount . ") ) ) ) / `mrp` ) * 100 ), 2)" ),
                    ];
                }else if($request->discount_type == 'p'){
                    $tblVariantUpdateDate = [                        
                        'price' => DB::raw('`mrp` - '.('`mrp` * '.($request->discount_amount/100))),
                        'discount' => DB::raw('`mrp` * '.($request->discount_amount/100)),
                        'fabpik_seller_price' => DB::raw("ROUND( (`mrp`- (`mrp` * ". ($request->discount_amount/100) .") ) - ( ( `fabpik_addon_discount` / 100 ) * ( `mrp` - (`mrp` * ". ($request->discount_amount/100) . ") ) ), 2)" ),
                        'fabpik_seller_discount' => DB::raw("ROUND( `mrp` - ( (`mrp`- (`mrp` * ". ($request->discount_amount/100) .") ) - ( ( `fabpik_addon_discount` / 100 ) * ( `mrp` - (`mrp` * ". ($request->discount_amount/100) . ") ) ) ), 2 )" ),
                        'fabpik_seller_discount_percentage' => DB::raw("ROUND( ( ( (`mrp` - ( (`mrp`- (`mrp` * ". ($request->discount_amount/100) .") ) - ( ( `fabpik_addon_discount` / 100 ) * ( `mrp` - (`mrp` * ". ($request->discount_amount/100) . ") ) ) ) ) / `mrp` ) * 100 ), 2 )" ),
                    ];
                }

                //Compare is sale price greater than mrp
                $prodVariantDetails = ProductVariant::whereIn('id', $request->ids)->pluck('mrp');
                $isUpdate = true;
                foreach($prodVariantDetails as $mrp){
                    if ($request->discount_type == 'f') {
                        if($mrp < $request->discount_amount){
                            $isUpdate = false;
                            break;
                        }
                    }
                }

                if (!$isUpdate) {
                    $records["customActionStatus"] = "NOT-OK";
                    $records["customActionMessage"] = 'Sale Price is greater than MRP, Please check once.';
                }else{
                    $update = ProductVariant::whereIn('id', $request->ids)->update($tblVariantUpdateDate);
                    DB::commit();
                    $records["customActionStatus"] = "OK";
                    $records["customActionMessage"] = 'Selected Products Data updated successfully.';
                }

            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            // 'fseller' => $this->loggedInUser->ref_id,
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:($this->loggedInUser->ref_id),
            'fpricefrom' => (!is_null($request->fpricefrom))?$request->fpricefrom:null,
            'fpriceto' => (!is_null($request->fpriceto))?$request->fpriceto:null,
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'ffeatured' => (!is_null($request->ffeatured))?$request->ffeatured:null,
            'fcategory' => (!is_null($request->fcategory))?$request->fcategory:null,
            'fsku' => (!is_null($request->fsku))?$request->fsku:null,
            'funiqueid' => (!is_null($request->funiqueid))?$request->funiqueid:null
        ];
        // DB::enableQueryLog();
        $prodVariants = ProductVariant::getAjaxChangePriceListData($criteria, $iPage, $orderColumn, $orderDir);
        // dd(DB::getQueryLog());
        $iTotalRecords = $prodVariants->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin') || $this->loggedInUser->hasRole('Seller'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($prodVariants as $index=>$prodVariant) {
                $status = $statusList[$prodVariant->product->status];
                $records["data"][] = [
                    '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$prodVariant->id.'"/><span></span></label>',
                    '<a href="'.route('seller.products.variantEdit', ['id'=>$prodVariant->product->id, 'vid'=>$prodVariant->id]).'" target="_blank" class="" title="View Order Details">'.$prodVariant->unique_id.'</a>',
                    '<span class=""><a href="https://fabpik.in/products/'.$prodVariant->slug.'/'.$prodVariant->unique_id.'" target="_blank" class="" title="View Product Details">'.($prodVariant->name).'</a></span>',
                    '<img src="'.url('/').'/'.UPLOAD_PATH.'/'.__getVariantImages($prodVariant->id).'" width="50" height="">',
                    $prodVariant->product->category->title,
                    $prodVariant->sku,
                    $prodVariant->mrp,
                    $prodVariant->price,
                    number_format((float)($prodVariant->discount), '2'),
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                    date('d M Y', strtotime($prodVariant->created_at)),
                    date('d M Y', strtotime($prodVariant->updated_at)),
                ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }
}
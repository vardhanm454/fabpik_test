<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;
use Hash;
use Session;
use Validator;
use Excel;
use Mail;

use DB;

// Models
use App\Models\Seller;
use App\Models\SellerWarehouses;
use App\Models\User;
use App\Models\Account;
use App\Models\State;
use App\Models\TermsAndCondtion;
use App\Models\SellerSettingChangeLog;
use App\Models\Brand;
use App\Models\Notifications;
use App\Models\NotificationLogs;
use App\Models\CommissionModel;


//Mails
use App\Mail\SellerPasswordUpdate;
use App\Mail\SellerAcceptCommission;

class AccountController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'manage-profile';
    public $activeSubmenu = 'profile';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Manage Profile Page View]
     * @return [type] [description]
     */
    public function profile()
    {
        $this->data['title'] = 'Manage Profile';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Manage Profile',
            ],
        ];
        $this->data['states'] = State::get();
        $this->data['seller_details'] = Seller::find(auth()->user()->ref_id);
        $this->data['seller_warehouse_details'] = SellerWarehouses::where('seller_id', auth()->user()->ref_id)->where('status', 1)->get();
        $this->data['isVerified'] = Seller::where(['id'=>auth()->user()->ref_id,'approval_status'=>1])->exists();
        $this->data['settingLogs'] = SellerSettingChangeLog::where('created_by',auth()->user()->ref_id)->whereNull('approved_by')->whereNull('approved_at')->whereNull('is_deleted')->get();
        return parent::sellerView('account.index', $this->data);
    }

    /**
     * [Logout from seller account]
     * @return [type] [description]
     */
    public function logout()
    {
        Auth::logout();
        
        return redirect()
                ->route('seller.login')
                ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Thank you. You have been succesfully logged out']);
    }

    /**
     * [Store Seller Basic Details]
     * @return [type] [description]
     */
    public function basicDetails(Request $request)
    {
        // dd($request->all());
        $input = [
            'name' => 'required',
            'email' => 'required|email',
            'mobile' => 'required|regex:/^[6789]\d{9}$/',
            'categories' => 'required', 
            // 'store_name' => 'required|regex:/^[A-Za-z][A-Za-z0-9]*(?:[A-Za-z0-9-\s]+)*$/',
            'store_name' => 'required',
            'brand_name' => 'required',
            'shop_url' => 'required|regex:/^[A-Za-z][A-Za-z0-9]*(?:[A-Za-z0-9-]+)*$/',
        ];
        $validator = Validator::make($request->all(), $input);

        $isUserExists = User::where('email', strtolower($request->email))->where('ref_id', '<>', auth()->user()->ref_id)->where('user_type','seller')->exists();
        $isMobileExists = User::where('mobile', $request->mobile)->where('ref_id', '<>', auth()->user()->ref_id)->where('user_type','seller')->exists();
        if (!is_null($request->shop_url)) {
            $isUrlExists = Seller::where('url_slug', $request->shop_url)->where('id', '<>', auth()->user()->ref_id)->exists();
            if ($isUrlExists) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('shop_url', 'URL already exist!');
                });
            }
        }


        /*Check the brand exists in Brands Table, 
         *  if not exists in brands table check it in sellers table brand_name column
         *  if exists throw an error else insert the brand in both seller and brands table.
        */
        $getCommonBrand = Brand::where('name', $request->brand_name)->whereNull('created_by');
        // $getSellerBrand = Seller::where('brand_name', $request->brand_name)->where('id', '<>', auth()->user()->ref_id);
        $getSellerBrand = Brand::where('name', $request->brand_name)->whereNotNull('created_by')->where('created_by', '<>', auth()->user()->ref_id);
        if (!is_null($request->brand_name)) {
            $isCommonBrandExist = $getCommonBrand->exists();
            if (!$isCommonBrandExist) {
                $isBrandExist = $getSellerBrand->exists();
                if ($isBrandExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('brand_name', 'Brand already exist!');
                    });
                }
            }
        }

        if ($isUserExists) {
            $validator->after(function ($validator) {
                $validator->errors()->add('email', 'email already exist!');
            });
        }

        if ($isMobileExists) {
            $validator->after(function ($validator) {
                $validator->errors()->add('mobile', 'mobile already exist!');
            });
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput(['tab'=>'tab_basic_details']);
        }

        $updatedData =  [
            'name' => trim($request->name),
            'email' => strtolower($request->email),
            'mobile' => $request->mobile,
            'categories' => implode(',',$request->categories),
            'chat_enable' => $request->chat_enable,
            'url_slug' => $request->shop_url,
            'store_name' => $request->store_name,
        ];

        if($isCommonBrandExist){
            $getCommonBrand = $getCommonBrand->first();
            $updatedData['brand_name'] = $getCommonBrand->name;
            $updatedData['brand_id'] = $getCommonBrand->id;
        }else{
            if (!$isBrandExist) {
                $brandexists = Brand::where('created_by', auth()->user()->ref_id)->first();
                if ($brandexists == null) {
                    $tblData = new Brand();
                    $tblData->name = $request->brand_name;
                    $tblData->slug = __generateSlug($request->brand_name);
                    $tblData->show_on_home = 0;
                    $tblData->created_by = auth()->user()->ref_id;
                    $tblData->save();
                    $updatedData['brand_name'] = $tblData->name;
                    $updatedData['brand_id'] = $tblData->id;
                }else{;
                    Brand::where('created_by', auth()->user()->ref_id)->update([
                        'name' => $request->brand_name,
                    ]);
                    $updatedData['brand_name'] = $request->brand_name;
                    $updatedData['brand_id'] = $brandexists->id;
                }
            }
        }
      
        $sellerUpdate = Seller::where('id', auth()->user()->ref_id)->update($updatedData);
        if ($request->has('email') || $request->has('mobile')) {
            $userUpdate = User::where('ref_id', auth()->user()->ref_id)
                                ->where("user_type",'=',"seller")
                                ->update(['name' => trim($request->name),
                                          'email' => strtolower($request->email),
                                          'mobile' => $request->mobile,
                                        ]);
        }
        return redirect()
                ->back()
                ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Seller Basic Details has been Updated Successfuly!'])->withInput(['tab'=>'tab_1-1']);;
    }

    /**
     * [Change Seller Password]
     * @return [type] [description]
     */
    public function changePassword(Request $request)
    {
        if ($request->save) {
            
            $rules = [
                'current_password' => 'required',
                'password' => 'required|confirmed|regex:/^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\W).*$/',
                'password_confirmation'=>'required',
            ];

            $validator = Validator::make($request->all(), $rules, [
                'password.regex' => 'Please Enter Valid Password, Password must be 8 charaters, 1 uppercase and 1 special charater',
            ]);

            if ($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            if (!(Hash::check($request->current_password, auth()->user()->password))) {
                $validator = ['current_password'=>'Current Password does not match!'];
                return redirect()->back()->withErrors($validator)->withInput(['tab'=>'tab_2-2']);
            }
            if ($request->current_password==$request->password) {
                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Current Password and New Password cannot be same!']);
            }
            if ($request->password!=$request->password_confirmation) {
                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Confirm Password and New Password does not match!']);
            }
    
            $userUpdate = User::where('ref_id', auth()->user()->ref_id)->where('user_type','LIKE','seller%')->update([
                'password' => Hash::make($request->password),
                'system_generated_password' => 'n'
            ]);
            $seller_details = Seller::with('user')->where('id', auth()->user()->ref_id)->first();
            if ($userUpdate) {
                Mail::to($seller_details->email)->send(new SellerPasswordUpdate($seller_details));
                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Password Changed Successfuly!']);
            }
        }

        $this->data['title'] = 'Change Password';
        $this->data['activeMenu'] = '';
        $this->data['activeSubmenu'] = '';
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Change Password',
            ],
        ];
        return parent::sellerView('account.changePassword', $this->data);
        
    }

    /**
     * [Store Seller Company Details]
     * @return [type] [description]
     */
    public function companyDetails(Request $request)
    {
        try {

            $input = [
                'company_name' => 'required|min:3',
                // 'company_reg_no' => 'required',
                'legal_entity' => 'required',
                'company_type' => 'required',
                'gst_registered' => 'required',
                // 'office_phone_no' => 'required',
                'company_address' => 'required',
                'company_state' => 'required',
                'company_pincode' => 'required',
            ];
            $validator = Validator::make($request->all(), $input);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->with(['tab'=>'tab_company_details']);
            }
            
            if ($request->has('company_reg_no')) {
                $gstCertificate = null;
                $incorporationCertificate = null;

                
                $tblData = [
                        'company_name' => trim($request->company_name),
                        'company_reg_no' => trim($request->company_reg_no),
                        'legal_entity' => $request->legal_entity,
                        'company_type' => $request->company_type,
                        'office_phone_no' => $request->office_phone_no,
                        'company_address' => $request->company_address,
                        'company_state' => $request->company_state,
                        'company_pincode' => $request->company_pincode,
                        
                    ];

                if($request->gst_registered == 1 && $request->hasFile('gst_certificate')){
                    $gstCertificate = time().'.'.$request->gst_certificate->extension();
                    $request->gst_certificate->move(UPLOAD_PATH.'/seller/gst_certificate', $gstCertificate);
                    $tblData['gst_certificate'] = $gstCertificate;
                        
                }
                if ($request->legal_entity == 'r' && $request->hasFile('incorporation_certificate')) {
                    $incorporationCertificate = time().'.'.$request->incorporation_certificate->extension();
                    $request->incorporation_certificate->move(UPLOAD_PATH.'/seller/incorporation_certificate', $incorporationCertificate);
                    $tblData['incorporation_certificate'] = $incorporationCertificate;
                }

                if($request->gst_registered == 0){
                    $tblData['gst_registered'] = $request->gst_registered;
                    $tblData['gst_no'] = null;
                }else if($request->gst_registered == 1){
                    $tblData['gst_registered'] = $request->gst_registered;
                    $tblData['gst_no'] = $request->gst_no;
                }

                $sellerUpdate = Seller::where('id', auth()->user()->ref_id)->update($tblData);
                if ($sellerUpdate) {
                    return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Seller Company Details has been Updated Successfuly!'])->withInput(['tab'=>'tab_3-3']);
                } else {
                    return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Some Error in saving Company Details!'])->withInput(['tab'=>'tab_3-3']);
                }
            } else {
                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Please Enter all Company details.'])->withInput(['tab'=>'tab_3-3']);
            }
        } catch (Exception $e){
            // dd($e);
            return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>''])->withInput(['tab'=>'tab_3-3']);
        }
    }


    public function notifications()
    {
        NotificationLogs::updateOrCreate([
            'user_id' => $this->loggedInUser->id
        ], [
            'user_id' => $this->loggedInUser->id,
            'visited_on' => date("Y-m-d h:i:s"),
        ]);

        $this->data['notifications'] = Notifications::where('notified_to','s')
                                                ->whereRaw('notify_on<NOW()')
                                                ->whereRaw("JSON_SEARCH(users, 'all', '".$this->loggedInUser->ref_id."')")
                                                ->orWhereNull('users')
                                                ->whereNull('deleted_at')
                                                ->get();
        // dd($this->data['notifications']);
        // $this->data['notifications'] = Notifications::where('seller_id', auth()->user()->ref_id)
        //                                         ->where('notified_to','s')
        //                                         ->whereNull('deleted_at')
        //                                         ->get();
        $this->data['title'] = 'Notification';
        $this->data['activeMenu'] = '';
        $this->data['activeSubmenu'] = '';
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Notification',
            ],
        ];
       return parent::sellerView('account.notifications' ,$this->data);
    }
    
    /**
     * [Store/Update Seller Warehouse Details]
     * @return [type] [description]
     */
    public function warehouseDetails(Request $request)
    {
        //echo "<pre>"; print_r($request->all()); die;
        if ($request->is_default==1) {
            $sellerWarehouses = SellerWarehouses::where('seller_id', auth()->user()->ref_id)->where('is_default', $request->is_default)->first();
            if (!empty($sellerWarehouses)) {
                $defaultAddress = SellerWarehouses::find($sellerWarehouses->id);
                $defaultAddress->is_default = '0';
                $defaultAddress->save();
            }
        }
        if (!empty($request->warehouse_id)) {
            $sellerWarehouseUpdate = SellerWarehouses::where('id', $request->warehouse_id)->where('seller_id', auth()->user()->ref_id)->update([
                'name' => trim($request->warehouse_name),
                'is_default' => $request->is_default,
                'address' => $request->warehouse_address,
                'address_two' => $request->warehouse_addresstwo,
                'phone' => $request->warehouse_phone,
                'landmark' => $request->warehouse_landmark,
                'city' => $request->warehouse_city,
                'state_id' => $request->warehouse_state,
                'pincode' => $request->warehouse_pincode,
            ]);
            return redirect()
                ->back()
                ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'warehouse Details has been Updated Successfuly!'])->withInput(['tab'=>'tab_4-4']);
        } else {
            $seller_id = auth()->user()->ref_id;
            $newWarehouseDetails = [
                                    'seller_id' => $seller_id,
                                    'name' => trim($request->warehouse_name),
                                    'is_default' => $request->is_default,
                                    'phone' => $request->warehouse_phone,
                                    'address' => $request->warehouse_address,
                                    'address_two' => $request->warehouse_addresstwo,
                                    'landmark' => $request->warehouse_landmark,
                                    'city' => $request->warehouse_city,
                                    'state_id' => $request->warehouse_state,
                                    'pincode' => $request->warehouse_pincode,
                                ];
            
                /**
                 * 
                 * Store the new pickup location in shiprocket
                 * */

                // $result = json_encode(['status'=>true]);
                // $result = json_decode($result);
                
            $result = json_decode( __addNewPickupLocation($newWarehouseDetails, $seller_id) );
            
            if($result->status == false){
                $message = 'Oops, Inavlid data!';
                $address = null;
                $addresstwo = null;
                $pickup_location = null;
                if(isset($result->result->errors)){
                    // $message = 'Please check the Address';

                    if( isset($result->result->errors->address) )
                        $address = $result->result->errors->address[0];

                    if( isset($result->result->errors->address_2) ) {
                        $addresstwo = 'Please check the Address 2';
                    }

                    if( isset($result->result->errors->pickup_location) ) {
                        $pickup_location = $result->result->errors->pickup_location[0];
                    }
                }
                return redirect()
                ->back()
                ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>$message, 'address'=>$address, 'addresstwo'=>$addresstwo ])->withInput(['tab'=>'tab_warehouse_details']);

            }else if($result->status == true){

                // dd($result->result);
                $newWarehouseDetails['shiprocket_pickup_nickname'] = $result->result->address->pickup_code;

                $sellerWarehouseUpdate = SellerWarehouses::create($newWarehouseDetails);

                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'warehouse Details Added Successfuly!'])->withInput(['tab'=>'tab_warehouse_details']);
            }          
        }
    }

    /**
     * [Get Seller Warehouse Details By Id]
     * @return [type] [description]
     */
    public function getWarehouseDetails(Request $request)
    {
        if (!empty($request->id)) {
            $warehouseDetails = SellerWarehouses::find($request->id)->toArray();
            if ($warehouseDetails) {
                return $warehouseDetails;
            } else {
                return "error";
            }
        }
    }

    /**
     * [Delete Seller Warehouse Details By Id]
     * @return [type] [description]
     */
    public function deleteWarehouseDetails(Request $request)
    {
        if (!empty($request->id)) {
            $delete = SellerWarehouses::where('id', $request->id)->delete();
            if ($delete) {
                return "success";
            } else {
                return "error";
            }
        }
    }

    /**
     * [Store Seller POC (Point Of Contact) Details]
     * @return [type] [description]
     */
    public function pocDetails(Request $request)
    {
        $sellerUpdate = Seller::where('id', auth()->user()->ref_id)->update([
            'poc_name' => trim($request->poc_name),
            'poc_email' => strtolower($request->poc_email),
            'poc_mobile' => $request->poc_mobile,
            'poc_phone' => $request->poc_phone,
            'poc_designation' => $request->poc_designation,
        ]);
        return redirect()
                ->back()
                ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Seller POC Details has been Updated Successfuly!'])->withInput(['tab'=>'tab_6-6']);
    }

    /**
     * [Store Seller Bank Details]
     * @return [type] [description]
     */
    public function bankDetails(Request $request)
    {
        if ($request->has('confirm_account_no')) {
            $cancelled_cheque_file = null;
            if ($request->hasFile('cancelled_cheque_file')) {
                $cancelled_cheque_file = time().'.'.$request->cancelled_cheque_file->extension();
                $request->cancelled_cheque_file->move(UPLOAD_PATH.'/seller/cancelled_cheque_files', $cancelled_cheque_file);
            }else if($request->has('cheque_uploded')){
                $cancelled_cheque_file = $request->cheque_uploded;
            }else {
                $cancelled_cheque_file = null;
            }

            $tblData = [
                'bank_account_name' => trim($request->bank_account_name),
                'bank_name' => trim($request->bank_name),
                'bank_account_no' => $request->bank_account_no,
                'ifsc_code' => $request->ifsc_code,
                'swift_code' => $request->swift_code,
                'branch_name' => $request->branch_name,
                'cancelled_cheque_file' => $cancelled_cheque_file,
            ];
              
            $sellerUpdate = Seller::where('id', auth()->user()->ref_id)->update($tblData);

            if ($sellerUpdate) {
                return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Seller Bank Details has been Updated Successfuly!'])->withInput(['tab'=>'tab_5-5']);
            } else {
                return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Some Error in saving Bank Details!'])->withInput(['tab'=>'tab_5-5']);
            }
        }else {
            return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Some Error in saving Bank Details!'])->withInput(['tab'=>'tab_5-5']);
        }
    }

    /**
     * [Accept Seller Commission]
     * @return [type] [description]
    */
    public function acceptCommissionDetails(Request $request)
    {

        $sellerUpdate = Seller::where('id', auth()->user()->ref_id)->update([
                                                                        'commission_accept' => $request->is_accept,
                                                                        'commission_accept_date' => date("Y-m-d h:i:s"),
                                                                    ]);

        if ($sellerUpdate) {
            try {
                $sellerDetails = Seller::where('id',auth()->user()->ref_id)
                            ->first();
                // dd($seller_email);
                $sellerCommissionType = CommissionModel::where('id',$sellerDetails->commission_id)->first();

                $seller_data = [
                    'name' => $sellerDetails->name,
                    'accepted_date' => $sellerDetails->commission_accept_date,
                    'comission_value' => $sellerDetails->commission,
                    'comission_type' => $sellerCommissionType->name, 
                ];

                $admin_mail = ADMIN_EMAIL;
                // dd($admin_mail);
                // $admin_mail = 'vardhanm@mirakitech.com';
                Mail::to($admin_mail)->send(new SellerAcceptCommission($seller_data));
                
                DB::commit();
                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>''])->withInput(['tab'=>'tab_7-7']);
            } 
            catch (Exception $e) {
                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Some Thing went Wrong, Try After Some Time!'])->withInput(['tab'=>'tab_7-7']);
                DB::rollback();
            }
        }
    }

    /**
     * [Get Seller Commission Based on type]
     * @return [type] [description]
    */
    public function getCommissionDetails(Request $request)
    {
        $polices = TermsAndCondtion::where('name', $request->commission_name)->first();
        return response()->json(['success'=>1, 'message'=>"", 'polices'=>$polices], 200);
    }
    
    /**
     * [Change Seller COD Model]
     * @return [type] [description]
    */
    public function changeCodDetails(Request $request)
    {
        if($request->undo){
            SellerSettingChangeLog::where('created_by', auth()->user()->ref_id)->where('id', $request->request_id)
                                        ->where('settings_column_name', $request->cod_column_name)
                                        ->whereNull('approved_by')
                                        ->whereNull('approved_at')
                                        ->update(['is_deleted'=>date('Y-m-d H:i:s')]);
            return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Request submited'])->withInput(['tab'=>'tab_settings']);
        }

        if ($request->cod_old_value != $request->cod_enable) {

            $isRequestExist = SellerSettingChangeLog::where('created_by', auth()->user()->ref_id)
                                    ->where('settings_column_name', $request->cod_column_name)
                                    ->whereNull('approved_by')
                                    ->whereNull('approved_at')
                                 ->whereNull('is_deleted')->exists();
            
            if($isRequestExist){

                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'error','title'=>'Already Requested','message'=>'You already raise the request, it is under review, Please wait'])->withInput(['tab'=>'tab_settings']);
            }
            $codChange = new SellerSettingChangeLog();
            $codChange->settings_column_name = $request->cod_column_name;
            $codChange->old_value  = $request->cod_old_value;
            $codChange->new_value  = $request->cod_enable;
            $codChange->created_by  = auth()->user()->ref_id;

            if ($codChange->save()) {
                //send mail to admin and seller your request is raised
                $seller_details = Seller::find(auth()->user()->ref_id);
                $to=[
                    'seller_email' => auth()->user()->email,
                ];
                $details = [
                    'codDetails' => $codChange,
                    'sellerDetails' => $seller_details,
                    'req_type' => 'codChange',
                ];
                
                __sendEmails($to, 'seller_cod_change_request', $details);

                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Your request has sent to fabpik team for review'])->withInput(['tab'=>'tab_settings']);
            } else {
                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Some Thing went Wrong, Try After Some Time!'])->withInput(['tab'=>'tab_settings']);
            }
        }else {
            return redirect()->back()->withInput(['tab'=>'tab_settings']);
        }
    }

    /**
     * [Change Seller Shipping Model]
     * @return [type] [description]
    */
    public function changeShippingDetails(Request $request)
    {
        if($request->undo){

            SellerSettingChangeLog::where('created_by', auth()->user()->ref_id)->where('id', $request->request_id)
                                        ->where('settings_column_name', $request->shipping_model_column_name)
                                        ->whereNull('approved_by')
                                        ->whereNull('approved_at')
                                        ->update(['is_deleted'=>date('Y-m-d H:i:s')]);
            return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Request submited'])->withInput(['tab'=>'tab_settings']);
        }

        if ($request->shipping_model_old_value != $request->shipping_model) {
            
            $isRequestExist = SellerSettingChangeLog::where('created_by', auth()->user()->ref_id)
                                        ->where('settings_column_name', $request->shipping_model_column_name)
                                        ->whereNull('approved_by')
                                        ->whereNull('approved_at')
                                    ->whereNull('is_deleted')->exists();

            if($isRequestExist){

            return redirect()
                ->back()
                ->with(['toast'=>'1','status'=>'error','title'=>'Already Requested','message'=>'You already raise the request, it is under review, Please wait'])->withInput(['tab'=>'tab_settings']);
            }
            $shippingModel = new SellerSettingChangeLog();
            $shippingModel->settings_column_name = $request->shipping_model_column_name;
            $shippingModel->old_value  = $request->shipping_model_old_value;
            $shippingModel->new_value  = $request->shipping_model;
            $shippingModel->created_by  = auth()->user()->ref_id;

            if ($shippingModel->save()) {
                //send mail to admin and seller your request is raised
                $seller_details = Seller::find(auth()->user()->ref_id);
                $to=[
                    'seller_email' => auth()->user()->email,
                ];
                $details = [
                    'shippingDetails' => $shippingModel,
                    'sellerDetails' => $seller_details,
                    'req_type' => 'codChange',
                ];

                __sendEmails($to, 'seller_shipping_change_request', $details);

                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Your request has sent to fabpik team for review'])->withInput(['tab'=>'tab_settings']);
            } else {
                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Some Thing went Wrong, Try After Some Time!'])->withInput(['tab'=>'tab_settings']);
            }
        }else {
            return redirect()->back()->withInput(['tab'=>'tab_settings']);
        }
    }

    /**
     * [Seller Final Submission]
     * @return [type] [description]
    */
    public function finalSubmission(Request $request)
    {   
        //get seller details.
        $sellerDetails = Seller::find(auth()->user()->ref_id);
        
        //Get the Seller warehouse details
        $sellerWarehouseDetails = SellerWarehouses::where('seller_id', auth()->user()->ref_id)->first();
        if($sellerWarehouseDetails == null){
            return response()->json(['success'=>0, 'message'=>"Please add atleast one warehouse.", 'data'=>''], 200);
        }

        $requiredKeys = ['name', 'store_name', 'url_slug', 'email', 'mobile', 'categories' , 'company_name', 'company_reg_no', 'legal_entity', 'company_type', 'gst_registered', 'company_address', 'company_state', 'company_pincode', 'bank_account_name', 'bank_account_no', 'ifsc_code', 'bank_name', 'branch_name', 'cancelled_cheque_file', 'poc_name', 'poc_designation', 'poc_email', 'poc_mobile'];

        $allFill = true;
        foreach($requiredKeys as $detail){
            if($sellerDetails->$detail === null){
                // dd($detail);
                $allFill = false;
                break;
            }
        }

        if($allFill){
            $sellerDetails->final_submission = 1;
            $sellerDetails->save();
            return response()->json(['success'=>1, 'message'=>"Thank you, our team will get back you soon!", 'data'=>$allFill], 200);
        }

        return response()->json(['success'=>0, 'message'=>"Submit required details to get your account approved", 'data'=>$allFill], 200);
    }
}

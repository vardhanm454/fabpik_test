<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Session;
use Redirect;

class VerifyController extends Controller
{
    public function VerifyEmail($token = null)
    {

    	if($token == null) {
        return Redirect::route('seller.login')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Account Login','message'=>'Invalid Login attempt']);
    	}

       $user = User::where('remember_token',$token)->first();
       //dd($user);

       if($user == null ){
        return Redirect::route('seller.login')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Account Login','message'=>'Invalid Login attempt']);
       }

       $userUpdate = User::where('remember_token',$token)->update([

        'email_verified_at' => date("Y-m-d h:i:s"),
        'remember_token' => ''

       ]);
       
        return Redirect::route('seller.login')
                        ->with(['toast'=>'1','status'=>'success','title'=>'Account Login','message'=>'Your account is activated, you can log in now']);

    }

}

?>
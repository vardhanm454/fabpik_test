<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Excel;
use Validator;
use DB;
use File;

//Models
use App\Models\Seller;

class ImageImportController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'importimage';
    public $basePath;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();
            $this->basePath = UPLOAD_PATH;

            return $next($request);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index(Request $request)
    {
        $this->data['sellers'] = Seller::where('status', 1)->where('approval_status', 1)->get();
        $this->data['title'] = 'Import Images';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Import Images',
            ]
        ];

        $filterName = '';
    	if (!empty($request->filter_name)) {
			$filterName = rtrim(str_replace(array('*', '/', '\\'), '', $request->filter_name));
		}
		$uploadPath = base_path($this->basePath);

		$directory = $uploadPath;

		// Make sure we have the correct directory
		if (!empty($request->directory)) {
			$directory = rtrim($uploadPath . '/' . str_replace('*', '', $request->directory));
		};
		$directory = urldecode($directory);

		$page = ($request->page)?$request->page:1;

		$directories = [];
		$files = [];

		$this->data['images'] = [];

		if (substr(str_replace('\\', '/', realpath($directory) . '/' . $filterName), 0, strlen($uploadPath)) == str_replace('\\', '/', $uploadPath)) {
			// Get directories
			$directories = glob($directory . '/' . $filterName . '*', GLOB_ONLYDIR);

			if (!$directories) {
				$directories = [];
			}

			// Get files
			$files = glob($directory . '/' . $filterName . '*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE);

			if (!$files) {
				$files = [];
			}
		}

		// Merge directories and files
		$images = array_merge($directories, $files);

		// Get total number of files and directories
		// $image_total = count($images);

		// Split the array based on current page number and max number of items per page of 10
		// $images = array_splice($images, ($page - 1) * 16, 16);

		foreach ($images as $image) {
			$name = str_split(basename($image), 14);

			if (is_dir($image)) {
				$url = '';

				if ($request->target) {
					$url .= '&target=' . $request->target;
				}

				if ($request->thumb) {
					$url .= '&thumb=' . $request->thumb;
				}

				$this->data['images'][] = [
					'thumb' => '',
					'name'  => implode(' ', $name),
					'type'  => 'directory',
					'path'  => substr($image, strlen($uploadPath.'/')),
					'href'  => route('filemanager', ['directory'=>urlencode(substr($image, strlen($uploadPath.'/')))]) . $url
				];
			} elseif (is_file($image)) {
				$this->data['images'][] = [
					'thumb' => url($this->basePath . '/' . substr($image, strlen($uploadPath.'/'))),
					'name'  => implode(' ', $name),
					'type'  => 'image',
					'path'  => substr($image, strlen($uploadPath.'/')),
					// 'href'  => $server . 'image/' . substr($image, strlen($uploadPath))
					'href'  => url($this->basePath . '/' . substr($image, strlen($uploadPath.'/')))
				];
			}
		}

		$this->data['directory'] = ($request->directory)?urlencode($request->directory):'';

		$this->data['filter_name'] = ($request->filter_name)?urlencode($request->filter_name):'';

		// Return the target ID for the file manager to set the value
		$this->data['target'] = ($request->target)?urlencode($request->target):'';

		// Return the thumbnail for the file manager to show a thumbnail
		$this->data['thumb'] = ($request->thumb)?urlencode($request->thumb):'';

		// Parent
		$parentParams = [];

		if ($request->directory) {
			$pos = strrpos($request->directory, '/');

			if ($pos) {
				$parentParams['directory'] = urlencode(substr($request->directory, 0, $pos));
			}
		}

		if ($request->target) {
			$parentParams['target'] = $request->target;
		}

		if ($request->thumb) {
			$parentParams['thumb'] = $request->thumb;
		}

		$this->data['parent'] = route('filemanager', $parentParams);

		// Refresh
		$refreshParams = [];

		if ($request->directory) {
			$refreshParams['directory'] = urlencode($request->directory);
		}

		if ($request->target) {
			$refreshParams['target'] = $request->target;
		}

		if ($request->thumb) {
			$refreshParams['thumb'] = $request->thumb;
		}

		$this->data['refresh'] = route('filemanager', $refreshParams);

		$urlParams = [];

		if ($request->directory) {
			$urlParams['directory'] = urlencode(html_entity_decode($request->directory, ENT_QUOTES, 'UTF-8'));
		}

		if ($request->filter_name) {
			$urlParams['filter_name'] = urlencode(html_entity_decode($request->filter_name, ENT_QUOTES, 'UTF-8'));
		}

		if ($request->target) {
			$urlParams['target'] = $request->target;
		}

		if ($request->thumb) {
			$urlParams['thumb'] = $request->thumb;
		}

		// $data['images'] = $this->paginate($data['images']);
		$this->data['imageRows'] = array_chunk($this->data['images'], 6);

        return parent::adminView('images.index', $this->data);
    }

    /**
     * Products Image Upload Code
     *
     * @return void
    */
    public function imageStore(Request $request)
    {
        dd($request->seller);
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:jpg,jpeg,gif,png', //,jpg,jpeg,gif,png
            'seller' => 'required',
        ], [
            'file.required' => 'Please upload a file',
            'file.mimes' => 'Only excel(.jpg (or) .jpeg (or) .gif (or) .png) file types allowed'
        ]);

        if($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        if ($request->hasfile('file')) {

            //getting seller ID
            $sellerDetails = Seller::select('seller_code')->where('id', $request->seller)->first();
            
            $folder = ('uploads'.DIRECTORY_SEPARATOR.$sellerDetails->seller_code);
            //create folders for seller
            if (!File::exists($folder)) {
                mkdir($folder, 0777);
                chmod($folder, 0777);
                
                mkdir($folder.'/products', 0777);
                chmod($folder.'/products', 0777);

                mkdir($folder.'/sizechart', 0777);
                chmod($folder.'/sizechart', 0777);

            }

            $today = date('dmY');
            //check the folder is exists or not with the name of today date, if not create it.
            $fileUploadPath = (UPLOAD_PATH.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$today);
            
            //create folders for seller
            if (!File::exists($fileUploadPath)) {
                mkdir($fileUploadPath, 0777);
                chmod($fileUploadPath, 0777);
            }

            $image = $request->file('file');
            // Allowed file extension types
		    $allowed = [ 'jpg', 'jpeg', 'gif', 'png' ];
            $newName = str_replace(" ","-",$image->getClientOriginalName());
            $image->move($fileUploadPath, $newName);
   
            return response()->json(['success'=>$newName]);
        }
    }

    /**
     * Products Image Remove Code
     *
     * @return void
    */
    public function removeImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:jpg,jpeg,gif,png', //,jpg,jpeg,gif,png
            'seller' => 'required',
        ], [
            'file.required' => 'Please upload a file',
            'file.mimes' => 'Only excel(.jpg (or) .jpeg (or) .gif (or) .png) file types allowed'
        ]);

        if($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        if ($request->has('filename')) {
            
            //getting seller ID
            $sellerDetails = Seller::select('seller_code')->where('id', $request->seller)->first();
            $today = date('dmY');
            $fileUploadPath = (UPLOAD_PATH.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$today);
            
            $filename =  str_replace(" ","-",$request->get('filename')) ;
            $path = $fileUploadPath.DIRECTORY_SEPARATOR.$filename;
            // dd($path);
            if (file_exists($path)) {
                unlink($path);
            }
            return response()->json(['success'=>$filename]);
        }        
    }

    /**
     * Size Chart Image Upload Code
     *
     * @return void
    */
    public function sizeChartImageStore(Request $request)
    {
        if ($request->hasfile('file')) {

            //getting seller ID
            $sellerDetails = Seller::select('seller_code')->where('id', $this->loggedInUser->ref_id)->first();
            $folder = ('uploads'.DIRECTORY_SEPARATOR.$sellerDetails->seller_code);
            //create folders for seller
            if (!File::exists($folder)) {
                mkdir($folder, 0777);
                chmod($folder, 0777);
                
                mkdir($folder.'/products', 0777);
                chmod($folder.'/products', 0777);

                mkdir($folder.'/sizechart', 0777);
                chmod($folder.'/sizechart', 0777);

            }

            $today = date('dmY');
            //check the folder is exists or not with the name of today date, if not create it.
            $fileUploadPath = (UPLOAD_PATH.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.DIRECTORY_SEPARATOR.'sizechart'.DIRECTORY_SEPARATOR.$today);
            
            //create folders for seller
            if (!File::exists($fileUploadPath)) {
                mkdir($fileUploadPath, 0777);
                chmod($fileUploadPath, 0777);
            }

            $image = $request->file('file');
            // Allowed file extension types
		    $allowed = [ 'jpg', 'jpeg', 'gif', 'png' ];
            $newName = str_replace(" ","-",$image->getClientOriginalName());
            $image->move($fileUploadPath, $newName);
   
            return response()->json(['success'=>$newName]);
        }
    }

    /**
     * Size Chart Image Remove Code
     *
     * @return void
    */
    public function sizeChartRemoveImage(Request $request)
    {
        if ($request->has('sizechartfilename')) {
            
            //getting seller ID
            $sellerDetails = Seller::select('seller_code')->where('id', $this->loggedInUser->ref_id)->first();
            
            $today = date('dmY');
            $fileUploadPath = (UPLOAD_PATH.DIRECTORY_SEPARATOR.$sellerDetails->seller_code.DIRECTORY_SEPARATOR.'sizechart'.DIRECTORY_SEPARATOR.$today);
            
            $filename =  str_replace(" ","-",$request->get('sizechartfilename')) ;
            $path = $fileUploadPath.DIRECTORY_SEPARATOR.$filename;
            // dd($path);
            if (file_exists($path)) {
                unlink($path);
            }
            return response()->json(['success'=>$filename]);
        }        
    }
}

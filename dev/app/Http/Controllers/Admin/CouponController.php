<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models
use App\Models\Coupon;
use App\Models\Seller;

// Exports
use App\Exports\CouponExport;

class CouponController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'coupons';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [coupons listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('coupons_perpage', 10);
        $this->data['dt_page'] = Session::get('coupons_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.coupons.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus', 'freference'];

        $this->data['title'] = 'Coupons';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Coupons',
            ]
        ];

        return parent::adminView('coupon.index', $this->data);
    }

    /**
     * [get all coupons for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'id',
            2 => 'amount',
            3 => 'max_usage',
            4 => 'sub_type',
            5 => 'status',
            6 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('coupon', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Coupon::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Coupons are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
            'freference' => (!is_null($request->freference))?$request->freference:null,
        ];

        $coupons = Coupon::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $coupons->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));
        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($coupons as $coupon) {
            $status = $statusList[$coupon->status];

            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.coupons.edit', ['id'=>$coupon->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('admin.coupons.delete', ['id'=>$coupon->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>':'';

            //Checking discount type and adding suffix.
            $disType = ($coupon->dis_type =='f')?'Rs OFF':'% OFF';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$coupon->id.'"/><span></span></label>',
                '<span class="text-uppercase">'.$coupon->code.'</span>',
                $coupon->amount.' '.$disType,
                ($coupon->max_usage=='o')?'Once':'Multiple',
                ($coupon->sub_type=='m')?'MRP':'Selling Price',
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                // '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export coupons]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null,
        ];
        $coupons = Coupon::getExportData($criteria);

        return Excel::download(new CouponExport($coupons), 'coupons.xlsx');
    }

    /**
     * [add Coupon data]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {
        $this->data['sellers'] = Seller::select('id','name')->where('status',1)->get();
        if ($request->save) {

           $validator = Validator::make($request->all(), [
                'code' => 'required|string',
                'dis_type' => 'required|string',
                'amount'=>'required|integer|gte:1',
                'min_cart_amount'=>'required|integer',
                'max_usage'=>'required|string',
                'expiry_date'=>'required|after_or_equal:start_date',
                'start_date'=>'required',
                'sub_type'=>'required',
                'description'=>'required',
            ]);

            $validator->sometimes('amount', 'lt:min_cart_amount', function($request) {
                return $request->dis_type == 'f';
            });
            
            if($request->dis_type == 'f' && $request->sub_type != 's'){
                $validator->after(function ($validator) {
                    $validator->errors()->add('sub_type', 'Flat Discount Only Applied on Selling Price.');
                });
            }

            if (!empty($request->code)) {
                $isExist = Coupon::where('code', $request->code)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('code', 'Coupon name already exist!');
                    });
                }
            }
            if($request->dis_type == 'p' && $request->amount > 100){
                $validator->after(function ($validator) {
                    $validator->errors()->add('amount', 'Amount shouldn\'t be greater than 100');
                });
            }

            //check the duplicate mobile numbers
            $checkMobileNumbersDuplicates = ($request->mobile_numbers!= null) ? explode( "\n", str_replace( "\r", "", $request->mobile_numbers ) )  : null;
            if(!is_null($checkMobileNumbersDuplicates)){
                if( count($checkMobileNumbersDuplicates) !== count(array_unique($checkMobileNumbersDuplicates)) ){
                    $validator->after(function ($validator) {
                        $validator->errors()->add('mobile_numbers', 'Duplicate mobile numbers, please check');
                    });
                }
    
                foreach($checkMobileNumbersDuplicates as $number){
                    // dd($number);
                    if(!preg_match("/^[6789]\d{9}$/", $number)) {
                        // $phone not  valid
                        $validator->after(function ($validator) {
                            $validator->errors()->add('mobile_numbers', 'Please enter valid mobile numbers, check once!');
                        });
                        break;
                    }
                }
            }

            if ($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            try {
                $coupons = new Coupon();
                $coupons->code = $request->code;
                $coupons->dis_type = $request->dis_type;
                $coupons->amount = $request->amount;
                $coupons->min_cart_amount = $request->min_cart_amount;
                $coupons->max_usage = $request->max_usage;
                $coupons->start_date = date('Y-m-d H:i:s', strtotime($request->start_date));
                $coupons->expiry_date = date('Y-m-d H:i:s', strtotime($request->expiry_date));
                $coupons->sub_type = $request->sub_type;
                $coupons->description = $request->description;
                $coupons->seller_ids = ($request->fseller!= null)?json_encode($request->fseller):null;
                $coupons->category_ids = ($request->fcategory!= null)?json_encode($request->fcategory):null;
                $coupons->product_varient_ids = ($request->fprovariants!= null)?json_encode($request->fprovariants):null;
                $coupons->mobile_numbers =  ($checkMobileNumbersDuplicates!=null) ? json_encode($checkMobileNumbersDuplicates) : null;
                $coupons->coupon_group_id = $request->coupon_groups;
                
                if ($coupons->save()) {
                    return redirect()
                        ->route('admin.coupons')
                        ->with(['toast'=>'1','status'=>'success','title'=>'Coupons','message'=>'Success! New Coupon added successfully.']);
                } else {
                    return redirect()
                        ->route('admin.coupons')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Coupons','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                return redirect()
                        ->route('admin.coupons')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Coupons','message'=>'Error! Some error occured, please try again.']);
            }
        }
        
        $this->data['title'] = 'Add Coupon';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.coupons',
                'title' => 'Coupons',
            ]
        ];
        return parent::adminView('coupon.addEditCoupons', $this->data);
    }

    /**
     * [edit Coupon data]
     * @param  Request $request [description]
     * @param  [type]  $id      [brand id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $coupon = Coupon::find($id);
        $this->data['sellers'] = Seller::select('id','name')->where('status',1)->get();
        if ($request->save) {
          
            $validator = Validator::make(
                $request->all(),
                [
                    'code' => 'required|string',
                    'dis_type' => 'required|string',
                    'amount'=>'required|integer|gte:1',
                    'min_cart_amount'=>'required|integer',
                    'max_usage'=>'required|string',
                    'start_date'=>'required',
                    'expiry_date'=>'required|after_or_equal:start_date',
                    'sub_type'=>'required',
                    'status' => 'required',
                    'description' => 'required',
                ]
            );
            
            $validator->sometimes('amount', 'lt:min_cart_amount', function($request) {
                return $request->dis_type == 'f';
            });

            if (!empty($request->code)) {
                $isExist = Coupon::where('code', $request->code)->where('id', '<>', $id)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('code', 'Coupon name already exist!');
                    });
                }
            }

            if($request->dis_type == 'f' && $request->sub_type != 's'){
                $validator->after(function ($validator) {
                    $validator->errors()->add('sub_type', 'Flat Discount Only Applied on Selling Price.');
                });
            }

            if($request->dis_type == 'p' && $request->amount > 100){
                $validator->after(function ($validator) {
                    $validator->errors()->add('amount', 'Amount shouldn\'t be greater than 100');
                });
            }

            //check the duplicate mobile numbers
            $checkMobileNumbersDuplicates = ($request->mobile_numbers!= null) ? explode( "\n", str_replace( "\r", "", $request->mobile_numbers ) )  : null;
            if (!is_null($checkMobileNumbersDuplicates)) {
                if (count($checkMobileNumbersDuplicates) !== count(array_unique($checkMobileNumbersDuplicates))) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('mobile_numbers', 'Duplicate mobile numbers, please check');
                    });
                }

                foreach ($checkMobileNumbersDuplicates as $number) {
                    // dd($number);
                    if (!preg_match("/^[6789]\d{9}$/", $number)) {
                        // $phone not  valid
                        $validator->after(function ($validator) {
                            $validator->errors()->add('mobile_numbers', 'Please enter valid mobile numbers, check once!');
                        });
                        break;
                    }
                }
            }

            if ($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            try {

                $tblData = [
                    'code' => $request->code,
                    'dis_type' => $request->dis_type,
                    'amount' => $request->amount,
                    'min_cart_amount' => $request->min_cart_amount,
                    'max_usage' => $request->max_usage,
                    'start_date' => date('Y-m-d H:i:s', strtotime($request->start_date)),
                    'expiry_date' => date('Y-m-d H:i:s', strtotime($request->expiry_date)),
                    'sub_type' => $request->sub_type,
                    'status' => $request->status,
                    'description' => $request->description,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'seller_ids' => ($request->fseller!= null)?json_encode($request->fseller):null,
                    'category_ids' => ($request->fcategory!= null)?json_encode($request->fcategory):null,
                    'product_varient_ids' => ($request->fprovariants!= null)?json_encode($request->fprovariants):null,
                    // 'mobile_numbers' => ($request->mobile_numbers!= null)?json_encode($request->mobile_numbers):null,
                    "mobile_numbers" =>  ($checkMobileNumbersDuplicates!=null) ? json_encode($checkMobileNumbersDuplicates) : null,
                    'coupon_group_id' => $request->coupon_groups,
                ];

                $update = Coupon::where('id', $id)->update($tblData);

                if ($update) {
                    if ($request->save == 'save') {
                        return redirect()
                                ->route('admin.coupons')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Coupons','message'=>'Success! Coupon data updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Coupons','message'=>'Success! Coupon data updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.coupons')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Coupons','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                return redirect()
                        ->route('admin.coupons')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Coupons','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['coupon'] = $coupon;
        $this->data['id'] = $id;
        
        $this->data['title'] = 'Edit Coupon';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.coupons',
                'title' => 'Coupons',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['coupon']->code,
            ],
        ];

        $this->data['editcoupon'] = $coupon;
        $this->data['id'] = $id;

        return parent::adminView('coupon.addEditCoupons', $this->data);
    }

    /**
     * [Delete Single Coupon data]
     * @param  Request $request [description]
     * @param  [type]  $id      [coupon id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            $delete = Coupon::where('id', $id)->delete();
            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;
use File;

// Models
use App\Models\StyleLayout;
use App\Models\LayoutColumn;
use App\Models\Sizechart;

// Imports

// Exports

class StyleLayoutController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'stylelayout';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('sizechart_perpage', 50);
        $this->data['dt_page'] = Session::get('sizechart_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,5,6,7,8';
        $this->data['dt_ajax_url'] = route('admin.stylelayout.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname', 'fcategory', 'freference'];

        $this->data['title'] = 'Style Layout';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => '',
                'title' => 'Style Layout',
            ],
        ];

        return parent::adminView('stylelayout.index', $this->data);
    }

    /**
     * [get all Products and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'id',
            2 => 'unique_id',
            3 => 'name',
            5 => 'seller_id',
            6 => 'primary_category',
            9 => 'status',
            10 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('Product', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                $isDelete = true;

                $recordCount = Sizechart::whereIN('style_layout_id', $request->input('id'))->count();
                if($recordCount > 0){
                    DB::rollback();
                    $records["customActionStatus"] = "NOT-OK";
                    $records["customActionMessage"] = 'Some Style Layouts are assigned to Sizechart.';
                }else{
                    
                    StyleLayout::whereIn('id', $request->input('id'))->update([
                                                'deleted_by' => $this->loggedInUser->id,
                                                'deleted_at' => date("Y-m-d H:i:s"),
                                                ]);

                    StyleLayout::whereIn('id', $request->input('id'))->delete();
                    
                    DB::commit();
                    $records["customActionStatus"] = "NOT-OK";
                    $records["customActionMessage"] = 'Selected Layouts are deleted successfully.';
                }
                
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?$request->fname:null,
            'fcategory' =>  ($request->fcategory)?$request->fcategory:null,
            'freference' =>  ($request->freference)?$request->freference:null,
        ];
        
        // dd($criteria);
        $stylelayouts = StyleLayout::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        
        $iTotalRecords = $stylelayouts->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $layoutNames = LayoutColumn::pluck('name', 'id');

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];
        $index = 0;
        foreach ($stylelayouts as $layout) {
            
            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.stylelayout.edit', ['id'=>$layout->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('admin.stylelayout.delete', ['id'=>$layout->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a></div>':'';
            
            $columns = json_decode($layout->columns);
            $columnNames = "";
            foreach ($columns as $columnid) {
                $columnNames .= $layoutNames[$columnid].", ";
            }
            
            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" id="single-check" value="'.$layout->id.'"/><span></span></label>',
                $layout->title,
                $layout->category->title,
                (isset($columnNames)) ? substr($columnNames, 0, -2) : '-',
                (!is_null($layout->createdUser)) ? $layout->createdUser->name : '-',
                date('d-m-Y', strtotime($layout->created_at)),
                (!is_null($layout->modifiedUser)) ? $layout->modifiedUser->name : '-',
                (!is_null($layout->updated_by) ? date('d-m-Y', strtotime($layout->updated_at)) : '-'),
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
                '',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [add Style Layout]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {

        if ($request->save) {
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'category' => 'required',
                'columns'=>'required',
                'frontImage' => 'required',
            ],[
                'frontImage.required' => 'Front image is required.'
            ]);

            if (!empty($request->title)) {
                $isExist = StyleLayout::where('title', $request->title)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('title', 'Title already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            $images = ['front'=>null, 'back'=>null, 'right'=>null, 'left'=>null];

            $folder = ('uploads'.DIRECTORY_SEPARATOR.'stylelaouts');
            //create folders for style layouts
            if (!File::exists($folder)) {
                mkdir($folder, 0777);
                chmod($folder, 0777);
            }
            //Front Image
            if ($file = $request->hasFile('imgfront')) {
                $file = $request->file('imgfront') ;            
                $fileName = $file->getClientOriginalName() ;
                $destinationPath = $folder;
                $file->move($destinationPath,$fileName);
                $images['front'] = $folder.DIRECTORY_SEPARATOR.$fileName;
            }

            //back Image
            if ($file = $request->hasFile('imgback')) {
                $file = $request->file('imgback') ;            
                $fileName = $file->getClientOriginalName() ;
                $destinationPath = $folder ;
                $file->move($destinationPath,$fileName);
                $images['back'] = $folder.DIRECTORY_SEPARATOR.$fileName;
            }

            //right Image
            if ($file = $request->hasFile('imgright')) {
                $file = $request->file('imgright') ;            
                $fileName = $file->getClientOriginalName() ;
                $destinationPath = $folder;
                $file->move($destinationPath,$fileName);
                $images['right'] = $folder.DIRECTORY_SEPARATOR.$fileName;
            }

            //left Image
            if ($file = $request->hasFile('imgleft')) {
                $file = $request->file('imgleft') ;            
                $fileName = $file->getClientOriginalName() ;
                $destinationPath = $folder;
                $file->move($destinationPath,$fileName);
                $images['left'] = $folder.DIRECTORY_SEPARATOR.$fileName;
            }

            try {
                DB::beginTransaction();

                $layout = new StyleLayout();
                $layout->title = $request->title;
                $layout->category_id = $request->category;
                $layout->columns = json_encode($request->columns);
                $layout->images = json_encode($images);
                $layout->created_by = $this->loggedInUser->id;
            
                if ($layout->save()) {
                    DB::commit();
                    return redirect()
                        ->route('admin.stylelayout')
                        ->with(['toast'=>'1','status'=>'success','title'=>'New Layout','message'=>'Success! New Layout added successfully.']);
                } else {
                    DB::rollback();
                    return redirect()
                        ->route('admin.stylelayout')
                        ->with(['toast'=>'1','status'=>'error','title'=>'New Layout','message'=>'Error! Some error occured, please try again.']);
                }
            }catch (Exception $e){
                DB::rollback();
                return redirect()
                        ->route('admin.stylelayout')
                        ->with(['toast'=>'1','status'=>'error','title'=>'New Layout','message'=>'Error! Some error occured, please try again.']);
            }

        }
        $this->data['title'] = 'Add Style Layout';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.stylelayout',
                'title' => 'Style Layout',
            ],
            (object) [
                'url' => false,
                'title' => 'Add New Style Layout',
            ],
        ];
        return parent::adminView('stylelayout.addEditStyleLayout', $this->data);
    }

    /**
     * [Edit existing Style Layout]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $this->data['styleLayout'] = StyleLayout::find($id);
        $this->data['id'] = $id;

        if ($request->save) {
            try{
                $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'category' => 'required',
                    'columns'=>'required',
                    'frontImage' => 'required',
                ],[
                    'frontImage.required' => 'Front image is required.'
                ]);
    
                if (!empty($request->title)) {
                    
                    $isExist = StyleLayout::where('title', $request->title)->where('id', '<>', $id)->exists();
                    
                    if ($isExist) {
                        $validator->after(function ($validator) {
                            $validator->errors()->add('title', 'Title already exist!');
                        });
                    }
                }
    
                if ($validator->fails()) {
                    return redirect()
                                ->back()
                                ->withErrors($validator)
                                ->withInput();
                }

                $images = ['front'=>null, 'back'=>null, 'right'=>null, 'left'=>null];

                $folder = ('uploads'.DIRECTORY_SEPARATOR.'stylelaouts');
                //create folders for style layouts
                if (!File::exists($folder)) {
                    mkdir($folder, 0777);
                    chmod($folder, 0777);
                }
                //Front Image
                if ($file = $request->hasFile('imgfront')) {
                    $file = $request->file('imgfront') ;            
                    $fileName = $file->getClientOriginalName() ;
                    $destinationPath = $folder;
                    $file->move($destinationPath,$fileName);
                    $images['front'] = $folder.DIRECTORY_SEPARATOR.$fileName;
                }else{
                    $images['front'] = $request->frontImage;
                }

                //back Image
                if ($file = $request->hasFile('imgback')) {
                    $file = $request->file('imgback') ;            
                    $fileName = $file->getClientOriginalName() ;
                    $destinationPath = $folder ;
                    $file->move($destinationPath,$fileName);
                    $images['back'] = $folder.DIRECTORY_SEPARATOR.$fileName;
                }else{
                    $images['back'] = $request->backImage;
                }

                //right Image
                if ($file = $request->hasFile('imgright')) {
                    $file = $request->file('imgright') ;            
                    $fileName = $file->getClientOriginalName() ;
                    $destinationPath = $folder;
                    $file->move($destinationPath,$fileName);
                    $images['right'] = $folder.DIRECTORY_SEPARATOR.$fileName;
                }else{
                    $images['right'] = $request->rightImage;
                }

                //left Image
                if ($file = $request->hasFile('imgleft')) {
                    $file = $request->file('imgleft') ;            
                    $fileName = $file->getClientOriginalName() ;
                    $destinationPath = $folder;
                    $file->move($destinationPath,$fileName);
                    $images['left'] = $folder.DIRECTORY_SEPARATOR.$fileName;
                }else{
                    $images['left'] = $request->leftImage;
                }


                $tblData = [
                    'title' => $request->title,
                    'category_id' => $request->category,
                    'columns' => json_encode($request->columns),
                    'images' => json_encode($images),
                    'updated_by' => $this->loggedInUser->id,                              
                ];

                $update = StyleLayout::where('id', $id)->update($tblData);

                if ($update) {
                    if ($request->save == 'save') {
                        return redirect()
                                ->route('admin.stylelayout')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Coupons','message'=>'Success! Coupon data updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Coupons','message'=>'Success! Coupon data updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.stylelayout')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Coupons','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                return redirect()
                        ->route('admin.stylelayout')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Coupons','message'=>'Error! Some error occured, please try again.']);
            }
        }
        
        $this->data['title'] = 'Edit Style Layout';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.stylelayout',
                'title' => 'Style Layout',
            ],
            (object) [
                'url' => false,
                'title' => 'Edit Style Layout',
            ],
        ];

        $this->data['layoutImages'] = json_decode($this->data['styleLayout']->images);

        return parent::adminView('stylelayout.addEditStyleLayout', $this->data);
    }

     /**
     * [Delete Single Style Layout data]
     * @param  Request $request [description]
     * @param  [type]  $id      [Layout id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {

            $recordCount = Sizechart::where('style_layout_id', $id)->count();
            if ($recordCount > 0) {
                return response()->json(['success'=>false, 'message'=>"Style Layout is assigned to Sizechart", 'error'=>true], 200);
            }

            $layout = StyleLayout::findOrFail($id);
            $layout->deleted_by = $this->loggedInUser->id;
            $layout->deleted_at = date("Y-m-d H:i:s");
            $layout->update();

            if($layout){
                DB::commit();
                return response()->json(['success'=>true, 'message'=>"Style Layout Deleted Successfully.", 'error'=>false], 200);
            }
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
        }
    }
}
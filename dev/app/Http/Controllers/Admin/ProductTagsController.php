<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use DB;
use Session;
use Validator;
use Excel;

// Models
use App\Models\ProductTag;
use App\Models\ProductTagsLog;
use App\Models\AssignedProductTag;

// Exports

class ProductTagsController extends CoreController
{
	public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'producttags';

    public function __construct() {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });        
    }

    /**
     * [brands listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('tags_perpage', 10);
        $this->data['dt_page'] = Session::get('tags_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,5,6,7,8,9';
        $this->data['dt_ajax_url'] = route('admin.producttags.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Product Tags';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Product Tags',
            ],
        ];

        return parent::adminView('producttags.index', $this->data);
    }

    /**
     * [get all brands for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
            2 => 'updated_at',
            3 => 'status',
            4 => 'status',
            5 => 'status',
            6 => 'status',
            7 => 'status',
            8 => 'status',
            9 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('brand', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType) 
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {

            DB::beginTransaction();
            try {

                $recordCount = AssignedProductTag::whereIn('tag_id', $request->input('id'))->count();

                if($recordCount > 0){
                    $records["customActionStatus"] = "NOT-OK";
                    $records["customActionMessage"] = 'Tags are assigned to products.';
                }else{

                    ProductTag::whereIn('id', $request->input('id'))->delete();
                    
                    DB::commit();
                    $records["customActionStatus"] = "OK";
                    $records["customActionMessage"] = 'Selected Tags are deleted successfully.';
                }
            }
            catch(Exception $e) {
                DB::rollback(); 
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
        ];
        $tags = ProductTag::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $tags->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ( $this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin') );

        foreach($tags as $i=>$tag) {

            $status = ($tag->status==0)?'<span class="badge badge-light-danger badge-roundless">Inactive</span>':'<span class="badge badge-light-success badge-roundless">Active</span>';

            $actionBtns = ($canChange)?'<div class="">
            <a href="#" class="btn btn-icon-only default btn-circle open-EditTagModal" data-toggle="modal" data-placement="top" data-target="#editProductTagModal" data-original-title="Edit" data-id="'.$tag->id.'" data-name="'.$tag->name.'" data-myvalue="'.$tag->status.'" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>

            <a href="'.route('admin.producttags.history', ['id'=>$tag->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="History" title="History"><span aria-hidden="true" class="fa fa-calendar"></span></a>

            <a href="javascript:;" del-url="'.route('admin.producttags.delete', ['id'=>$tag->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>

            
            
            </div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$tag->id.'"/><span></span></label>',
                $i+1,
                $tag->name,
                $tag->user->name,
                date('d-m-Y', strtotime($tag->created_at)),
                (!is_null($tag->modifiedUser)) ? $tag->modifiedUser->name : '-',
                date('d-m-Y', strtotime($tag->updated_at)),
                $tag->assignedProductTags->count(),
                $status,
                '<a href="'.route('admin.products').'?search=1&freference='.$tag->id.'" class="" data-toggle="tooltip" data-placement="top" data-original-title="References" title="References" target="_blank">References</a>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [add new Tag]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {
        if (!empty($request->name)) {
            $isExist = ProductTag::where('name', $request->name)->exists();
            if ($isExist) {
                return response()->json(['success'=>false, 'message'=>"Tag name already exists.", 'error'=>true], 200);
            }
        }

        DB::beginTransaction();
            
        try {
            $tags = new ProductTag();
            $tags->name = $request->name;
            $tags->created_by = $this->loggedInUser->id;


            if($tags->save()) {
                DB::commit();
                return response()->json(['success'=>true, 'message'=>"Success! Tag added successfully.", 'error'=>false], 200);
            }
            else
                return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);

        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again after sometime", 'error'=>true], 200);;
        }
    }

        /**
     * [edit the existing Tag]
     * @param  Request $request [description]
     * @param  [type]  $id      [brand id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        if(!empty($request->name)) {
            $isExist = ProductTag::where('name', $request->name)->where('id', '<>', $id)->exists();
            if ($isExist) {
                return response()->json(['success'=>false, 'message'=>"Tag name already exists.", 'error'=>true], 200);
            }
        }

        DB::beginTransaction();
        try {

            if( ($request->name != $request->from_tag_name) || ($request->status != $request->from_tag_status) ){
               
               $tagLog = new ProductTagsLog();
               $tagLog->tag_id = $id;
               $tagLog->from_name = $request->from_tag_name;
               $tagLog->to_name = $request->name;
               $tagLog->from_status = $request->from_tag_status;
               $tagLog->to_status = $request->status;
               $tagLog->modified_by = $this->loggedInUser->id;
               $tagLog->save();

            }

            $tblData = [
                'name' => $request->name,
                'modified_by' => $this->loggedInUser->id,
                'status' => $request->status,
            ];

            $update = ProductTag::where('id',$id)->update($tblData);

            if($update) {
                DB::commit();
                return response()->json(['success'=>true, 'message'=>"Success! Product Tag updated successfully.", 'error'=>false], 200); 
            }
            else{
                return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
            }            
        }
        catch(Exception $e) {
            DB::rollback(); 
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again after sometime.", 'error'=>true], 200);
        }
    }


    /**
    * [History Style Layout Column]
    * @param  Request $request [description]
    * @param  [type]  $id      [category id]
    * @return [type]           [description]
   */
   public function history(Request $request, $id)
   {

       $tags = ProductTag::find($id);
       $tagHistories = ProductTagsLog::with('modifiedUser')->where('tag_id', $id)->get();

       if($tagHistories->count() == 0){
        return redirect()
                ->back()
                ->with(['toast'=>'1','status'=>'error','title'=>'Tags','message'=>'No History Found.']);
       }

       $this->data['tags'] = $tags;
       $this->data['tagHistories'] = $tagHistories;
       $this->data['id'] = $id;
        
       $this->data['title'] = 'Product Tag History';
       $this->data['activeMenu'] = $this->activeMenu;
       $this->data['activeSubmenu'] = $this->activeSubmenu;
       $this->data['breadcrumbs'] = (object) [
           (object) [
               'url' => '',
               'title' => 'Product Management',
           ],
           (object) [
               'url' => 'admin.producttags',
               'title' => 'Product Tags',
           ],
           (object) [
               'url' => false,
               'title' => $this->data['tags']->name,
           ],
        ];
        return parent::adminView('producttags.history', $this->data);
   }


    /**
     * [Delete Single Product Tag]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $isDelete = true;

            $recordCount = AssignedProductTag::where('tag_id', $id)->count();
            if($recordCount > 0){
                return response()->json(['success'=>false, 'message'=>"Tag is assigned to products", 'error'=>true], 200);
            }

            if ($isDelete) {
                $delete = ProductTag::where('id', $id)->delete();
                if ($delete) {
                    DB::commit();
                    return response()->json(['success'=>true, 'message'=>"Tag Deleted Successfully", 'error'=>false], 200);
                }
            }
            
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
        }
    }

}

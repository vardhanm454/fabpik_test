<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models
use App\Models\Deals;
use App\Models\Seller;
use App\Models\ProductVariant;

// Exports
use App\Exports\DealsExport;

class DealsController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'deals';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [coupons listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('coupons_perpage', 10);
        $this->data['dt_page'] = Session::get('coupons_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.deals.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus','fprovariants'];

        $this->data['title'] = 'Deals';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Deals',
            ]
        ];

        return parent::adminView('deals.index', $this->data);
    }

    /**
     * [get all coupons for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
            2 => 'mrp',
            3 => 'price',
            4 => 'deal_price',
            5 => 'start_time',
            6 => 'end_time',
            7 => 'max_orders',
            8 => 'created_by',
            9 => 'updated_by'
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('deals', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];
        
        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Deals::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Deals are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fprovariants' => ($request->fprovariants)?:null
        ];
        // dd($criteria);
        // dd($request->all());
        $deals = Deals::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $deals->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));
        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($deals as $deal) {
            // $status = $statusList[$deals->status];

            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.deals.edit', ['id'=>$deal->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('admin.deals.delete', ['id'=>$deal->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>':'';

            //Checking discount type and adding suffix.
            // $disType = ($coupon->dis_type =='f')?'Rs OFF':'% OFF';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$deal->id.'"/><span></span></label>',
                '<span class="text-uppercase">'.$deal->name.'</span>',
                $deal->mrp,
                $deal->price,
                $deal->deal_price,
                $deal->start_time,
                $deal->end_time,
                $deal->publish_date,
                $deal->max_orders,
                $deal->created_by_user,
                $deal->created_by_user,
                $actionBtns
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export coupons]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null,
        ];
        $deals = Deals::getExportData($criteria);

        return Excel::download(new DealsExport($deals), 'deals.xlsx');
    }

    /**
     * [add Coupon data]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {
        
        if ($request->save) {
            // dd($request->all());
            $validator = Validator::make($request->all(), [
                'start_date' => 'required',
                'expiry_date' => 'required|after_or_equal:start_date',
                'publish_date'=>'required'
            ]);
            
            $validator->after(function ($validator) use($request) {
                if(!isset($request->products)){
                    $validator->errors()->add('empty_fields', "Please input all the fields.");
                }else{
                    foreach ($request->products as  $product) {
                            if($product['fprovariants'] == null || $product['max_orders'] == null || $product['deal_price'] == null){
                                $validator->errors()->add('empty_fields', "Please input all the fields.");
                                // break;
                            }
                        }
                }
            });

            if ($validator->fails()) {
                // dd($validator->messages());
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }
           
            if(!empty($request->products)) {
                $validator->after(function ($validator) use($request) {
                    $dealPriceZeroCheck = [];
                    $postPrds = [];
                    $dealPriceZeroCheck = [];
                    foreach ($request->products as $product) {
                        $postPrds[] = $product['fprovariants'];
                        if($product['deal_price'] == 0) {
                            $dealPriceZeroCheck[] = $product['deal_price'];
                        }
                    }
                    if(count($dealPriceZeroCheck)){
                        $validator->errors()->add('deal_price_invalid', 'Please Enter Deal Price Above 0.');
                    }
                    if (count($postPrds) !== count(array_unique($postPrds))) {
                        $validator->errors()->add('pvariants', 'Please remove duplicate product variants.');
                    }

                    $stocks = ProductVariant::select('id', DB::raw("CONCAT(name,'||',stock) AS stock"))->whereIn('id', $postPrds)->get()->pluck('stock', 'id');
                    $stockExceed = [];
                    $dealExistsProducts = [];
                    $publishTimeExistsProducts = [];
                    $start_date = date('Y-m-d H:i:s', strtotime($request->start_date));
                    $end_date = date('Y-m-d H:i:s', strtotime($request->expiry_date));
                    $publish_date = date('Y-m-d H:i:s', strtotime($request->publish_date));
                    foreach ($request->products as $product) {
                        list($pName, $pStock) = explode('||', $stocks[$product['fprovariants']]);
                        if($pStock < $product['max_orders']) $stockExceed[] = $stocks[$product['fprovariants']];
                        if($product['fprovariants'] == null || $product['max_orders'] == null || $product['deal_price'] == null) $emptyFields[] = $product;
                        $product_v_id =  $product['fprovariants'];
                        $dealAlreadyExists = Deals::whereRaw("(deals.start_time BETWEEN '$start_date' AND '$end_date' OR deals.end_time BETWEEN '$start_date' AND '$end_date' OR '$start_date' BETWEEN deals.start_time AND deals.end_time OR '$end_date' BETWEEN deals.start_time AND deals.end_time) AND product_variant_id = $product_v_id")
                                                    ->exists();
                        $publishTimeExists = Deals::whereRaw("('$publish_date' BETWEEN deals.start_time AND deals.end_time) AND product_variant_id = $product_v_id")
                                                    ->exists();
                        $product_name = ProductVariant::select("name")->where('id', $product_v_id)->first();
                        if($dealAlreadyExists){
                            $dealExistsProducts[] = "Deal Already Exist For ". $product_name->name;
                        }
                        
                    }
                    $publishTimeExists = Deals::whereRaw("'$publish_date' BETWEEN deals.start_time AND deals.end_time")
                    ->exists();
                    if($publishTimeExists) $validator->errors()->add('publish_time_error', "Other Deals Exists for the given publish date.");
                    if(!empty($stockExceed)) $validator->errors()->add('pvariants_stock_error', $stockExceed);
                    if(!empty($dealExistsProducts)) $validator->errors()->add('deal_exists', $dealExistsProducts);
                    if(!empty($emptyFields)) $validator->errors()->add('empty_fields', "Please input all the fields.");
                });
            }

            if ($validator->fails()) {
                // dd($validator->messages());
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }
            try {
                foreach ($request->products as $product) {
                    $deal = new Deals();
                    $deal->start_time = date('Y-m-d H:i:s', strtotime($request->start_date));
                    $deal->end_time = date('Y-m-d H:i:s', strtotime($request->expiry_date));
                    $deal->publish_date = date('Y-m-d H:i:s', strtotime($request->publish_date));
                    $deal->product_variant_id = $product['fprovariants'];
                    $deal->deal_price = $product['deal_price'];
                    $productForId = ProductVariant::find($product['fprovariants']);
                    $deal->product_id = $productForId->product_id;
                    $deal->created_by = $this->loggedInUser->id;
                    $deal->updated_by = $this->loggedInUser->id;
                    $deal->max_orders = $product['max_orders'];
                    $deal->total_max_orders = $product['max_orders'];
                    $deal->save();
                }
                return redirect()
                       ->route('admin.deals')
                       ->with(['toast'=>'1','status'=>'success','title'=>'Deals','message'=>'Success! New Deals added successfully.']);
            } catch (Exception $e) {
                return redirect()
                        ->route('admin.deals')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Deals','message'=>'Error! Some error occured, please try again.']);
            }
        }
        
        $this->data['title'] = 'Add Deals';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.deals',
                'title' => 'Deals',
            ]
        ];
        return parent::adminView('deals.addEditDeals', $this->data);
    }

    /**
     * [edit Coupon data]
     * @param  Request $request [description]
     * @param  [type]  $id      [brand id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {

        $deal = Deals::find($id);
        if ($request->save) {
            
            // dd(date('Y-m-d H:i:s', strtotime($request->start_date)));
            $validator = Validator::make($request->all(), [
                'start_date' => 'required',
                'expiry_date' => 'required|after_or_equal:start_date',
                'publish_date'=>'required'
            ]);
            $validator->after(function ($validator) use($request) {
                if($request->products[0]["fprovariants"] == null || $request->products[0]["max_orders"] == null || $request->products[0]["deal_price"] == null) {
                    $validator->errors()->add('empty_fields', "Please input all the fields.");
                }
            });
            if($validator->fails()){
                // dd($request->products);
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }
            if(!empty($request->products[0])) {
                $validator->after(function ($validator) use($request) {
                    // if($request->products[0]["fprovariants"] == null || $request->products[0]["max_orders"] == null || $request->products[0]["deal_price"] == null) {
                        
                    //     if(!empty($emptyFields)) $validator->errors()->add('empty_fields', "Please input all the fields.");
                    //     if ($validator->fails()) {
                    //         return redirect()
                    //                     ->back()
                    //                     ->withErrors($validator)
                    //                     ->withInput();
                    //     }
                    // }
                    $stocks = ProductVariant::select('id', DB::raw("CONCAT(name,'||',stock) AS stock"))->where('id', $request->products[0]["fprovariants"])->get()->pluck('stock', 'id');
                    $stockExceed = [];
                    $dealExistsProducts = [];
                    $start_date = date('Y-m-d H:i:s', strtotime($request->start_date));
                    $end_date = date('Y-m-d H:i:s', strtotime($request->expiry_date));
                        list($pName, $pStock) = explode('||', $stocks[$request->products[0]["fprovariants"]]);
                        if($pStock < $request->products[0]["max_orders"]) $stockExceed[] = $stocks[$request->products[0]["fprovariants"]];
                        
                        $product_v_id =  $request->products[0]["fprovariants"];
                        // $dealAlreadyExists = Deals::whereRaw("(deals.start_time BETWEEN '$start_date' AND '$end_date' OR deals.end_time BETWEEN '$start_date' AND '$end_date' OR '$start_date' BETWEEN deals.start_time AND deals.end_time OR '$end_date' BETWEEN deals.start_time AND deals.end_time) AND product_variant_id = $product_v_id")
                        //                             ->get();
                                                    
                        // if(count($dealAlreadyExists) >= 1){
                        //     $product_name = ProductVariant::select("name")->where('id', $product_v_id)->first();
                        //     $dealExistsProducts[] = "Deal Already Exist For ". $product_name->name;
                        //     array_push($dealExistsProducts, "Deal Already Exist For ". $product_name->name);
                        // }
                    

                    if(!empty($stockExceed)) $validator->errors()->add('pvariants_stock_error', $stockExceed);
                    if(!empty($dealExistsProducts)) $validator->errors()->add('deal_exists', $dealExistsProducts);
                    if(!empty($emptyFields)) $validator->errors()->add('empty_fields', "Please input all the fields.");
                });
            }   
            if ($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            try {
                $product = ProductVariant::find($request->products[0]['fprovariants']);
                $tblData = [
                    'deal_price' => $request->products[0]['deal_price'],
                    'start_time' => date('Y-m-d H:i:s', strtotime($request->start_date)),
                    'end_time' => date('Y-m-d H:i:s', strtotime($request->expiry_date)),
                    'publish_date' => date('Y-m-d H:i:s', strtotime($request->publish_date)),
                    'product_variant_id' => ($request->products[0]['fprovariants']!= null)?$request->products[0]['fprovariants']:null,
                    'product_id'=>$product->product_id,
                    'max_orders'=>$request->products[0]['max_orders'],
                    'total_max_orders'=>$request->products[0]['max_orders'],

                ];

                $update = Deals::where('id', $id)->update($tblData);

                if ($update) {
                    if ($request->save == 'save') {
                        return redirect()
                                ->route('admin.deals')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Deals','message'=>'Success! Deal data updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Deals','message'=>'Success! Deal data updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.deals')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Deals','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                return redirect()
                        ->route('admin.deals')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Deals','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['deal'] = $deal;
        $this->data['id'] = $id;
        
        $this->data['title'] = 'Edit Deal';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.deals',
                'title' => 'Deals',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['deal']->deal_price,
            ],
        ];

        $this->data['editdeal'] = $deal;
        $this->data['id'] = $id;

        return parent::adminView('deals.addEditDeals', $this->data);
    }

    /**
     * [Delete Single Coupon data]
     * @param  Request $request [description]
     * @param  [type]  $id      [coupon id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            $delete = Deals::where('id', $id)->delete();
            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;
use PDF;

// Models
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\PaymentStatus;
use App\Models\ShippingStatus;
use App\Models\Seller;
use App\Models\ProductVariantOption;
use App\Models\Attribute;

// Exports

class SelfShippingOrderController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'orders';
    public $activeSubmenu = 'selfshipping';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('payments_perpage', 10);
        $this->data['dt_page'] = Session::get('payments_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.selfshipping.getAjaxListData');
        $this->data['dt_search_colums'] = ['ffromdate', 'ftodate', 'fostatus', 'fseller', 'fpstatus', 'forderid', 'fpayoutdate'];

        $this->data['title'] = 'Self-Shipping Orders';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Self-Shipping Orders',
            ]
        ];

        $this->data['sellers'] = Seller::get();
        $this->data['orderStatus'] = OrderStatus::get();
        $this->data['paymentStatus'] = PaymentStatus::get();

        return parent::adminView('selfshipping.index', $this->data);
    }

    
    /**
     * [get all Parent Orders and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'created_at',
            5 => 'mrp',
            6 => 'price',
            9 => 'commission',
            13 => 'final_amount_to_pay_seller',
            14 => 'order_status_id',
            15 => 'payout_status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('order', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        
        if ($request->input('customActionName') == "changestatus") {
            DB::beginTransaction();
            try {
                OrderDetail::whereIn('id', $request->input('id'))->update([
                    "payout_status" => $request->input('status'),
                    "payout_date_from_admin" => date('Y-m-d'),
                ]);

                DB::commit();
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Payout Status Changed successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'fostatus' => (!is_null($request->fostatus))?$request->fostatus:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fpstatus' => (!is_null($request->fpstatus))?$request->fpstatus:null,
            'fpayoutdate' => (!is_null($request->fpayoutdate))?date('Y-m-d', strtotime($request->fpayoutdate)):null,
            'forderid' => (!is_null($request->forderid))?$request->forderid:null,
        ];
        $orders = OrderDetail::getAjaxSelfShippingListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $orders->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($orders as $order) {            
            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.payments.view', ['id'=>$order->id]).'" class="btn btn-icon-only default btn-circle" title="View Order Details" target="_blank">
                <span aria-hidden="true" class="icon-eye"></span> 
            </a>
            <!--<a href="'.route('admin.payments.sellerInvoice', ['id'=>$order->id]).'" class="btn btn-icon-only default btn-circle" target="_blank" title="Download Invoice">
                <span aria-hidden="true" class="icon-cloud-download"></span>
            </a>-->
         </div>':'';

         $comission_igst = 0;
         $handling_charge_igst = 0;
         if(isset($order->commission_igst)){
             $comission_igst = $order->commission_igst;
         }
         if(isset($order->order_handling_charge_igst)){
            $handling_charge_igst = $order->order_handling_charge_igst;
        }
        $final_amount_to_get_from_seller = $order->commission + $order->commission_sgst + $order->commission_cgst + $comission_igst + $order->order_handling_charge + $order->order_handling_charge_sgst + $order->order_handling_charge_cgst + $handling_charge_igst;
        $handling_charge = $order->order_handling_charge + $order->order_handling_charge_sgst + $order->order_handling_charge_cgst + $handling_charge_igst;
        // dd($comission_igst);

         $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$order->id.'"/><span></span></label>',
                date('d M Y', strtotime($order->created_at)),
                '<a href="'.route('admin.childorders.view', ['id'=>$order->id]).'">'.$order->child_order_id.'</a>',
                '<a href="'.route('admin.sellers.view', ['id'=>$order->seller_id]).'">'. $order->seller->seller_code.'</a>',
                $order->seller->name,
                $order->mrp,
                $order->price,
                $order->quantity,
                $order->total,
                number_format((float)$order->commission, '1'),
                ($order->sgst + $order->cgst),
                number_format((float)$handling_charge, '1'),
                number_format((float)$final_amount_to_get_from_seller, '1'),
                OrderDetail::getOrderStatusData($order->order_status_id)->admin_text,
                ($order->payout_status == 1)?'Yes':'No',
                ($order->payout_date_from_seller)?date('d M Y', strtotime($order->payout_date_from_seller)):'',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Session;
use Excel;
use DB;
use Hash;
use Mail;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

// Models
use App\Models\Role;
use App\Models\Permission;

// Exports
use App\Exports\RolesExport;

class RoleController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'usermgmt';
    public $activeSubmenu = 'roles';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('roles_perpage', 10);
        $this->data['dt_page'] = Session::get('roles_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3';
        $this->data['dt_ajax_url'] = route('admin.roles.getAjaxListData');
        $this->data['dt_search_colums'] = ['name'];

        $this->data['title'] = 'Roles';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'User Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Roles',
            ],
        ];

        return parent::adminView('roles.index', $this->data);
    }

    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name'
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('roles', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Role::whereIn('id', $request->input('id'))->delete();
                DB::commit();

                $records["customActionMessage"] = 'Selected Roles are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        $criteria = (object)[
            'length' => intval($request->length),
            'name' => (!is_null($request->name))?$request->name:null,
            'seller' => null,
        ];

        $roles = Role::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $roles->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        $index = $iDisplayStart;
        foreach ($roles as $role) {
            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$role->id.'"/><span></span></label>',
                ++$index,
                $role->name,
                '<div class="btn-group btn-group-sm btn-group-solid">
                    <div class="">
                        <a href="'.route('admin.roles.edit', ['id'=>$role->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
                        <a href="javascript:;" del-url="'.route('admin.roles.delete', ['id'=>$role->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>
                    </div>
                </div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    public function export(Request $request)
    {
        $criteria = (object)[
            'length' => intval($request->length),
            'name' => (!is_null($request->name))?$request->name:null,
            'seller' => null
        ];
        $roles = Role::getExportData($criteria);
        return Excel::download(new RolesExport($roles),'roles.xlsx');
    }

    public function create(Request $request)
    {
        if($request->save) {
            $validator = Validator::make($request->all(), [
                'name'=>'required|string',
                'permissions' => 'required'
            ]);

            if($request->name) {
                $isExists = Role::where('name', $request->name)->whereNull('seller_id')->exists();
                if($isExists) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add(
                            'name', 'Role is already exist!'
                        );
                    });
                }
            }

            if($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            DB::beginTransaction();
            try {
                $role = new Role();
                $role->name = $request->name;
                $role->guard_name = $request->guard_name;
                $role->created_by = $this->loggedInUser->id;
                $role->created_at = date('Y-m-d H:i:s');
                $role->save();

                // assign the permissions to newrole
                $role->syncPermissions($request->permissions);

                DB::commit();
                return back()
                        ->with(['toast'=>'1','status'=>'success','title'=>'Role','message'=>'Success! New Role added successfully.']);
            } catch(Exception $e) {
                DB::rollback();
                return back()->with(['toast'=>'1','status'=>'error','title'=>'Role','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['moduleWisePerms'] = Permission::getModuleWisePermissions(['panel'=>0]); // panel = 0 it'll only return permissions for Admin panel
        // dd($this->data['moduleWisePerms']);

        $this->data['asignedPerms'] = [];

        $this->data['title'] = 'Add role';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'User Management',
            ],
            (object) [
                'url' => 'admin.roles',
                'title' => 'Role',
            ],
            (object) [
                'url' => false,
                'title' => 'Add',
            ],
        ];

        return parent::adminView('roles.addEditRoles', $this->data);
    }

    public function edit(Request $request, $id)
    {
        $this->data['id'] = $id;
        $this->data['role'] = $role = Role::find($id);

        if($request->save) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'permissions' => 'required',
            ]);

            if($request->name) {
                $isExists = Role::where('name', $request->name)->whereNull('seller_id')->where('id', '<>', $id)->exists();
                if($isExists) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add(
                            'name', 'Role is already exist!'
                        );
                    });
                }
            }

            if($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            DB::beginTransaction();
            try {
                $role->name = $request->name;
                $role->updated_by = $this->loggedInUser->id;
                $role->updated_at = date('Y-m-d H:i:s');
                $role->save();

                // update role's permissions
                $role->syncPermissions($request->permissions);

                DB::commit();

                return redirect()
                        ->route('admin.roles')
                        ->with(['toast'=>'1','status'=>'success','title'=>'Role','message'=>'Success! Role data updated successfully.']);
            } catch(Exception $e) {
                DB::rollback();
                return back()->with(['toast'=>'1','status'=>'error','title'=>'Role','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['moduleWisePerms'] = Permission::getModuleWisePermissions(['panel'=>0]); // panel = 0 it'll only return permissions for Admin panel

        $this->data['asignedPerms'] = $role->getAllPermissions()->pluck('id')->toArray();
        // dd($this->data['asignedPerms']);

        $this->data['title'] = 'Edit role';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'User Management',
            ],
            (object) [
                'url' => 'admin.roles',
                'title' => 'Role',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['role']->name,
            ],
        ];

        return parent::adminView('roles.addEditRoles', $this->data);
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $delete = Role::where('id',$id)->delete();

            DB::commit();


            return response()->json(['success'=>1, 'message'=>""], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }

    private function _resetRolePermission($role, $perms=[]) {
        $permissions = Permission::select('name')->get();
        foreach ($permissions as $key => $permission) {
            $role->revokePermissionTo($permission->name);
        }

        if(!empty($perms)) {
            foreach ($perms as $key => $perm) {
                $role->givePermissionTo($perm);
            }
        }
    }
}
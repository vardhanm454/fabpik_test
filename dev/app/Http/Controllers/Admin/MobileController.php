<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;
use File;
use Auth;
use Carbon\Carbon;

// Models
use App\Models\Newsletter;
use App\Models\MobileAppSliders;

// Exports
use App\Exports\NewsletterExport;

class MobileController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'newsletter';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = $this->loggedInUser;

            return $next($request);
        });
    }

    /**
     * [newsletter listing page]
     * @return [type] [description]
     */
    public function index(Request $request)
    {
        ## 1. CHECK REQUEST IS AJAX OR NOT
        if($request->ajax())
        {
            switch ($request->post('panel'))
            {
                case 'info':

                $validator = Validator::make($request->all(), [
                    'title' => 'string|regex:/^[a-zA-Z0-9\s]+$/||min:5|max:20|nullable',
                    'image' => 'required|image|mimes:webp',
                ]);
                if($validator->passes())
                {
                    $folder = (UPLOAD_PATH.DIRECTORY_SEPARATOR.'mobileAppSliders');
                    ## CREATE FOLDER
                    if (!File::exists($folder)) {
                        mkdir($folder, 0777, true);
                        chmod($folder, 0777);
                    }
                    //Image
                    if ($file = $request->hasFile('image')) {
                        $file = $request->file('image') ;
                        $fileName = date('YmdHis')."_".$file->getClientOriginalName() ;
                        $destinationPath = $folder;
                        $file->move($destinationPath,$fileName);
                        $image = $fileName;   //$folder.DIRECTORY_SEPARATOR.$fileName;
                    }
                    ## VERIFY ANY DATA EXIST WITH HOME H AND PANNEL INFO
                    try {
                            DB::beginTransaction();

                            MobileAppSliders::updateOrCreate([
                                'page' => 'h',
                                'panel' => 'info'
                            ], [
                                'page' => 'h',
                                'panel' => 'info',
                                'title' => $request->title,
                                'image' => $image,
                                'created_by' => $this->loggedInUser->ref_id,
                                'updated_at' => null
                            ]);
                            DB::commit();
                            return response()->json(['status' => 200, 'message' => 'Success! Mobile info added successfully']);
                        } catch (Exception $e){
                            DB::rollback();
                            return response()->json(['status' => 201, 'message' => 'Error! Some error occured, please try again']);
                        }

                }
                else
                {
                    return response()->json(['errors' => $validator->errors()]);
                }

                break;

                case 'middle':
                case 'bottom':    

                $validator = Validator::make($request->all(), [
                    'title' => 'string|regex:/^[a-zA-Z0-9\s]+$/|min:5|max:20|nullable',
                    'image' => 'required|image|mimes:webp',
                    'category' => 'nullable',
                    'brand' => 'nullable',
                    'coupan' => 'nullable',
                    'minPrice' => 'numeric|nullable',
                    'maxPrice' => 'numeric|nullable',
                    'minDiscount' => 'numeric|nullable',
                    'maxDiscount' => 'numeric|nullable',

                ]);
                if($validator->passes())
                {
                    
                    $folder = (UPLOAD_PATH.DIRECTORY_SEPARATOR.'mobileAppSliders');
                    ## CREATE FOLDER
                    if (!File::exists($folder)) {
                        mkdir($folder, 0777, true);
                        chmod($folder, 0777);
                    }
                    //Front Image
                    if ($file = $request->hasFile('image')) {
                        $file = $request->file('image') ;
                        $fileName = date('YmdHis')."_".$file->getClientOriginalName() ;
                        $destinationPath = $folder;
                        $file->move($destinationPath,$fileName);
                        $image = $fileName;                 //$folder.DIRECTORY_SEPARATOR.$fileName;
                    }


                    $data = array(
                        'category_id' => (!is_null($request->category))?array(implode(',',$request->post('category'))):false,
                        'sub_category_id' => false,
                        'child_category_id' => false,
                        'brand' => (!is_null($request->brand))?array(implode(',',$request->post('brand'))):false,
                        'min_price' => (!is_null($request->minPrice))?$request->minPrice:false,
                        'max_price' => (!is_null($request->maxPrice))?$request->maxPrice:false,
                        'min_discount' => (!is_null($request->minDiscount))?$request->minDiscount:false,
                        'max_discount' => (!is_null($request->maxDiscount))?$request->maxDiscount:false,
                        // 'coupon' => (!is_null($request->coupan))?array(implode(',',$request->post('coupan'))):false,
                        'coupon' => (($request->coupan[0] == 'false')) ? false : array(implode(',',$request->post('coupan')))
                    );

                    $middleSlider = new MobileAppSliders();
                    $middleSlider->page = 'h';
                    $middleSlider->panel = (($request->post('panel') === 'middle' ?'middle' : 'bottom'));
                    $middleSlider->title = $request->title;
                    $middleSlider->image = $image;
                    $middleSlider->filter_params = json_encode(array($data));
                    $middleSlider->created_by = $this->loggedInUser->ref_id;
                    $middleSlider->updated_at = null;
                    $middleSlider->save();

                    // return response()->json(['status' =>200, 'result' =>$res]);
                    return response()->json(['status' =>200, 'message' =>'Success! Banner added successfully.']);
                }
                else
                {
                    return response()->json(['errors' => $validator->errors()]);
                }

                break;

                default:
                    return redirect()
                    ->route('admin.home')
                    ->with(['toast'=>'1','status'=>'error','title'=>'Warning','message'=>'Something Went Wrong.']);
                    

            }
        }

        $this->data['title'] = 'Home Page';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Mobile Home Page',
            ]
        ];

        $this->data['info'] = MobileAppSliders::where('page','h')->where('panel','info')->first();
        $this->data['middleBanners'] = MobileAppSliders::where('page','h')->where('panel','middle')->where('deleted_at', null)->orderBy('id','desc')->get();
        $this->data['bottomBanners'] = MobileAppSliders::where('page','h')->where('panel','bottom')->where('deleted_at', null)->orderBy('id','desc')->get();

        return parent::adminView('mobileapp.home', $this->data);
    }

    ## GET DYNAMIC DATA
    public function dynamicData(Request $request)
    {
        if($request->ajax())
        {
            ## 1. ALL CATEGORIES
            $categories = DB::table('categories')->where('status',1)->select('id','title')->orderBy('id','desc')->get();
            $cate = '<option value="">--Select Categories--</option>';
		    foreach($categories as $key => $value)
		    {
		        $cate .= '<option value="'.$value->id.'">'.$value->title.'</option>';
			}

            ## 2. ALL BRANDS
            $brands = DB::table('brands')->where('status',1)->select('id','name')->orderBy('id','desc')->get();
            $brand = '<option value="">--Select Brands--</option>';
		    foreach($brands as $key => $value)
		    {
		        $brand .= '<option value="'.$value->id.'">'.$value->name.'</option>';
			}

            ## 3. ALL COUPANS
            $coupans = DB::table('coupons')->where('status',1)->select('id','code')->orderBy('id','desc')->get();
            $coupan = '<option value="false">Select Coupon</option> <option value="">Select Coupon</option>';
		    foreach($coupans as $key => $value)
		    {
		        $coupan .= '<option value="'.$value->id.'">'.$value->code.'</option>';
			}


            return response()->json([
                'status' => 200,
                'categories' => $cate,
                'brands' => $brand,
                'coupans' => $coupan
            ]);
        }
    }

    ## EDIT SLIDERS
    public function editBanner(Request $request)
    {
        if($request->ajax())
        {
            ## 1.PAGE COMING FROM FRONTEND (ex: h, c, o)
            $page = htmlspecialchars(strip_tags($request->post('page')));
            ## 2.SLIDER ID COMING FROM FRONTEND
            $slider_id = htmlspecialchars(strip_tags($request->post('id')));
            ## 3.GET THE DATA ASSOCIATED WITH THIS ID FROM MOBILE APP SLIDERS TABLE
            $result = DB::table('mobile_app_sliders')->where('id',$slider_id)->first();
            ## 5.MOVE JSON DATA TO PRIVATE HELPER FUNCTION
            $result = $this->editMobileSliders($result, $page, $slider_id);
            return $result;

        }
    }

    ## EDIT SLIDERS
    public function updateBanner(Request $request)
    {
        if($request->ajax())
        {
            ## 1.VALIDATING INPUT FIELDS
            $validator = Validator::make($request->all(), [
                'title' => 'string|regex:/^[a-zA-Z0-9\s]+$/|nullable',
                'image' => 'image|mimes:webp|nullable',
                'category' => 'nullable',
                'brand' => 'nullable',
                'coupan' => 'nullable',
                'url' => 'url|nullable',
                'minPrice' => 'numeric|nullable',
                'maxPrice' => 'numeric|nullable',
                'minDiscount' => 'numeric|nullable',
                'maxDiscount' => 'numeric|nullable',
    
            ]);

            if($validator->passes())
            {
                
                ## 2. VALIDATION PASSES SEND PANNEL TYPE && BANNERID ALL RESPONSE TO PRIVATE FUNCTION
                $panel = htmlspecialchars(strip_tags($request->post('panel')));
                $banner_id = htmlspecialchars(strip_tags($request->post('banner_id')));
                $requestFile = $request->file('image');
                $requestAll = $request->post();
                ## 3. SEND DATA TO PRIVATE FUNCTION
                $result = $this->updateMobileSliders($requestFile, $requestAll, $panel, $banner_id);
                return $result;
            }
            else
            {
                return response()->json(['errors' => $validator->errors()]);
            }


        }
    }

    ## DELETE DYNAMIC DATA
    public function deleteBanner(Request $request)
    {
        ## 1. CHECK REQUEST IS AJAX OR NOT
        if($request->ajax())
        {
            switch ($request->post('page'))
            {
                 case 'm': 
                 case 'b':
                 case 'c': 
                 case 'o':     
                    $banner_id = htmlspecialchars(strip_tags($request->post('id')));
                    ## UNLINK IMAGE PATH
                    $banner_image = MobileAppSliders::where('id', $banner_id)->first();
                    if($banner_image->image == null || $banner_image->image == '')
                    {
                        
                        $delete = MobileAppSliders::where('id', $banner_id)->delete();
                    }
                    else
                    {
                        $folder = (UPLOAD_PATH.DIRECTORY_SEPARATOR.'mobileAppSliders');
                        $path = $folder.'/'.$banner_image->image;
                        if($path)
                        {
                            unlink($path);
                            $delete = MobileAppSliders::where('id', $banner_id)->delete();
                        }
                        else
                        {
                            $delete = MobileAppSliders::where('id', $banner_id)->delete();
                        }
                        
                    }
                    if($delete)
                    {
                        return response()->json(['success'=>1, 'message'=>""], 200);
                    }
                    else
                    {
                        return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 201);
                    }
                 break;

                 default:
                        return redirect()
                        ->route('admin.home')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Warning','message'=>'Something Went Wrong.']);
            }
        }
    }
    
    ## CATEGORY FUNCTION
    public function category(Request $request)
    {

        ## 1. CHECK REQUEST IS AJAX OR NOT
        if($request->ajax())
        {

                $validator = Validator::make($request->all(), [
                    'title' => 'string|regex:/^[a-zA-Z0-9\s]+$/|min:5|max:20|nullable',
                    'image' => 'required|image|mimes:webp',
                    'url' => 'url|nullable',
                    'category' => 'nullable',
                    'brand' => 'nullable',
                    'coupan' => 'nullable',
                    'minPrice' => 'numeric|nullable',
                    'maxPrice' => 'numeric|nullable',
                    'minDiscount' => 'numeric|nullable',
                    'maxDiscount' => 'numeric|nullable',

                ]);
                if($validator->passes())
                {
                    $folder = (UPLOAD_PATH.DIRECTORY_SEPARATOR.'mobileAppSliders');
                    ## CREATE FOLDER
                    if (!File::exists($folder)) {
                        mkdir($folder, 0777, true);
                        chmod($folder, 0777);
                    }
                    //Front Image
                    if ($file = $request->hasFile('image')) {
                        $file = $request->file('image') ;
                        $fileName = date('YmdHis')."_".$file->getClientOriginalName() ;
                        $destinationPath = $folder;
                        $file->move($destinationPath,$fileName);
                        $image = $fileName;            //$folder.DIRECTORY_SEPARATOR.$fileName;
                    }


                    $data = array(
                        'category_id' => (!is_null($request->category))?array(implode(',',$request->post('category'))):false,
                        'sub_category_id' => false,
                        'child_category_id' => false,
                        'brand' => (!is_null($request->brand))?array(implode(',',$request->post('brand'))):false,
                        'url' => (!is_null($request->url))?$request->url:false,
                        'min_price' => (!is_null($request->minPrice))?$request->minPrice:false,
                        'max_price' => (!is_null($request->maxPrice))?$request->maxPrice:false,
                        'min_discount' => (!is_null($request->minDiscount))?$request->minDiscount:false,
                        'max_discount' => (!is_null($request->maxDiscount))?$request->maxDiscount:false,
                        'coupon' => (!is_null($request->coupan))?array(implode(',',$request->post('coupan'))):false,
                    );

                    $middleSlider = new MobileAppSliders();
                    $middleSlider->page = 'c';
                    $middleSlider->panel = '';
                    $middleSlider->title = $request->title;
                    $middleSlider->image = $image;
                    $middleSlider->filter_params = json_encode(array($data));
                    $middleSlider->created_by = $this->loggedInUser->ref_id;
                    $middleSlider->updated_at = null;
                    $middleSlider->save();

                    // return response()->json(['status' =>200, 'result' =>$res]);
                    return response()->json(['status' =>200, 'message' =>'Success! Middle Banner added successfully.']);
                }
                else
                {
                    return response()->json(['errors' => $validator->errors()]);
                }


        }


        $this->data['exclusives'] = MobileAppSliders::where('page','c')->where('deleted_at', null)->orderBy('id','desc')->get();
        $this->data['title'] = 'Category Page';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Mobile Category Page',
            ]
        ];

        return parent::adminView('mobileapp.category', $this->data);
    }

    ## OFFER FUNCTION
    public function offers( Request $request)
    {

        ## 1. CHECK REQUEST IS AJAX OR NOT
        if($request->ajax())
        {

                $validator = Validator::make($request->all(), [
                    'title' => 'string|regex:/^[a-zA-Z0-9\s]+$/|min:5|max:20|nullable',
                    'image' => 'required|image|mimes:webp',
                    'url' => 'url|nullable',
                    'category' => 'nullable',
                    'brand' => 'nullable',
                    'coupan' => 'nullable',
                    'minPrice' => 'numeric|nullable',
                    'maxPrice' => 'numeric|nullable',
                    'minDiscount' => 'numeric|nullable',
                    'maxDiscount' => 'numeric|nullable',

                ]);
                if($validator->passes())
                {
                    $folder = (UPLOAD_PATH.DIRECTORY_SEPARATOR.'mobileAppSliders');
                    ## CREATE FOLDER
                    if (!File::exists($folder)) {
                        mkdir($folder, 0777, true);
                        chmod($folder, 0777);
                    }
                    //Front Image
                    if ($file = $request->hasFile('image')) {
                        $file = $request->file('image') ;
                        $fileName = date('YmdHis')."_".$file->getClientOriginalName() ;
                        $destinationPath = $folder;
                        $file->move($destinationPath,$fileName);
                        $image = $fileName;   //$folder.DIRECTORY_SEPARATOR.$fileName;
                    }


                    $data = array(
                        'category_id' => (!is_null($request->category))?array(implode(',',$request->post('category'))):false,
                        'sub_category_id' => false,
                        'child_category_id' => false,
                        'brand' => (!is_null($request->brand))?array(implode(',',$request->post('brand'))):false,
                        'url' => (!is_null($request->url))?$request->url:false,
                        'min_price' => (!is_null($request->minPrice))?$request->minPrice:false,
                        'max_price' => (!is_null($request->maxPrice))?$request->maxPrice:false,
                        'min_discount' => (!is_null($request->minDiscount))?$request->minDiscount:false,
                        'max_discount' => (!is_null($request->maxDiscount))?$request->maxDiscount:false,
                        'coupon' => (!is_null($request->coupan))?array(implode(',',$request->post('coupan'))):false,
                    );

                    $middleSlider = new MobileAppSliders();
                    $middleSlider->page = 'o';
                    $middleSlider->panel = '';
                    $middleSlider->title = $request->title;
                    $middleSlider->image = $image;
                    $middleSlider->filter_params = json_encode(array($data));
                    $middleSlider->created_by = $this->loggedInUser->ref_id;
                    $middleSlider->updated_at = null;
                    $middleSlider->save();

                    // return response()->json(['status' =>200, 'result' =>$res]);
                    return response()->json(['status' =>200, 'message' =>'Success! Offer Banner added successfully.']);
                }
                else
                {
                    return response()->json(['errors' => $validator->errors()]);
                }


        }


        $this->data['offers'] = MobileAppSliders::where('page','o')->where('deleted_at', null)->orderBy('id','desc')->get();
        $this->data['title'] = 'Offers Page';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Offers Category Page',
            ]
        ];

        return parent::adminView('mobileapp.offers', $this->data);
    }

    ################# PRIVATE FUNCTIONS FOR EDIT  ######################
    private function editMobileSliders($result, $page, $slider_id){
        

        ## DECODED THE ENCODED JSON  DATA
        $data = json_decode($result->filter_params, true);
        switch ( $page )
        {
            case 'h':
            case 'c':
            case 'o':     

                ## 1. CATEGORY DATA
                $category_id =  $data[0]['category_id'];
                if(!($category_id == null))
                {
                    foreach($category_id as $c_id) {
                        $exe_cat = explode(",",$c_id);
                
                    }
                    
                    $all_categories = DB::table('categories')->where('status',1)->select('id','title')->orderBy('id','desc')->get();
                    
                    $cate = '<option value="">--Select Categories--</option>';
                    foreach($all_categories as $key => $value)
                    {                
                        $cate .= '<option value="'.$value->id.'" '.((in_array($value->id,$exe_cat)) ? 'selected' : '').'>'.$value->title.'</option>';
                    
                    }
                }
                else
                {
                    $categories = DB::table('categories')->where('status',1)->select('id','title')->orderBy('id','desc')->get();
                    $cate = '<option value="">--Select Categories--</option>';
                    foreach($categories as $key => $value)
                    {
                        $cate .= '<option value="'.$value->id.'">'.$value->title.'</option>';
                    }
                }
        

                ## 2. BRANDS DATA
                $brand_id =  $data[0]['brand'];
                if(!($brand_id == null))
                {
                    foreach($brand_id as $b_id) {
                        $exe_brand = explode(",",$b_id);
                
                    }
                    
                    $all_brands = DB::table('brands')->where('status',1)->select('id','name')->orderBy('id','desc')->get();

                    $brand = '<option value="">--Select Brands--</option>';
                    foreach($all_brands as $key => $value)
                    {                
                        $brand .= '<option value="'.$value->id.'" '.((in_array($value->id,$exe_brand)) ? 'selected' : '').'>'.$value->name.'</option>';
                    
                    }
                }
                else
                {
                    $brands = DB::table('brands')->where('status',1)->select('id','name')->orderBy('id','desc')->get();
                    $brand = '<option value="">--Select Brands--</option>';
                    foreach($brands as $key => $value)
                    {
                        $brand .= '<option value="'.$value->id.'">'.$value->name.'</option>';
                    }
                }

                ## 3. COUPANS DATA
                $coupon_id =  $data[0]['coupon'];
                if(!($coupon_id == null))
                {
                    foreach($coupon_id as $cp_id) {
                        $exe_coupon = explode(",",$cp_id);
                
                    }
                    
                    $all_coupnas = DB::table('coupons')->where('status',1)->select('id','code')->orderBy('id','desc')->get();

                    $coupan = '<option value="false">Select Coupon</option> <option value="">Select Coupon</option>';
                    // $coupan = '<option value="">--Select Coupan--</option>';
                    foreach($all_coupnas as $key => $value)
                    {                
                        $coupan .= '<option value="'.$value->id.'" '.((in_array($value->id,$exe_coupon)) ? 'selected' : '').'>'.$value->code.'</option>';
                    
                    }
                }
                else
                {
                    $coupans = DB::table('coupons')->where('status',1)->select('id','code')->orderBy('id','desc')->get();
                    $coupan = '<option value="false">Select Coupon</option> <option value="">Select Coupon</option>';
                    foreach($coupans as $key => $value)
                    {
                        $coupan .= '<option value="'.$value->id.'">'.$value->code.'</option>';
                    }
                }

                ## URL CONDITION FOR (h,c,o)
                if($page === 'h')
                {
                    $url = '';
                }
                else
                {
                    if($data[0]['url'] == false)
                    {
                        $url = '';
                    }
                    else
                    {
                        $url = $data[0]['url'];
                    }
                }

                ## SEND RESPONSE DATA TO editBanner FUNCTION
                $imgBaseUrl = env('APP_URL').DIRECTORY_SEPARATOR.UPLOAD_PATH.DIRECTORY_SEPARATOR.'mobileAppSliders';
                return response()->json([
                    'status' => 200,
                    'title' => $result->title,
                    'image' => $imgBaseUrl.'/'.$result->image,
                    'categories' => $cate,
                    'brands' => $brand,
                    'coupans' => $coupan,
                    'minPrice' => (($data[0]['min_price'] == false ? ''  :$data[0]['min_price'] )),
                    'maxPrice' => (($data[0]['max_price'] == false ? ''  :$data[0]['max_price'] )),   
                    'minDiscount' => (($data[0]['min_discount'] == false ? ''  :$data[0]['min_discount'] )),
                    'maxDiscount' => (($data[0]['max_discount'] == false ? ''  :$data[0]['max_discount'] )),
                    'url' => $url,
                    'banner_id' => $slider_id
                ]);

            break;

            default:
                return redirect()
                ->route('admin.home')
                ->with(['toast'=>'1','status'=>'error','title'=>'Warning','message'=>'Something Went Wrong.']);
           


        }
        
    }

    ################# PRIVATE FUNCTIONS FOR EDIT  ######################
    private function updateMobileSliders($requestFile, $requestAll, $panel, $banner_id){
        
        //dd($requestAll);
         ## GET SLIDER DATA 
         $img_path = DB::table('mobile_app_sliders')->where('id',$banner_id)->first();
         ## CHECK IMAGE IS COMING OR NOT
         if($file = $requestFile)
         {
             ## UNLINK IMAGE
             $folder = (UPLOAD_PATH.DIRECTORY_SEPARATOR.'mobileAppSliders');
             $path = $folder.'/'.$img_path->image;
             unlink($path);
         }

             $folder = (UPLOAD_PATH.DIRECTORY_SEPARATOR.'mobileAppSliders');
             ## CREATE FOLDER
             if (!File::exists($folder)) {
                mkdir($folder, 0777, true);
                chmod($folder, 0777);
             }
             ## UPLOAD IMAGE
             if ($file = $requestFile) {
                $file = $requestFile;            
                $fileName = date('YmdHis')."_".$file->getClientOriginalName() ;
                $destinationPath = $folder;
                $file->move($destinationPath,$fileName);
                $image = $fileName;                 //$folder.DIRECTORY_SEPARATOR.$fileName;
             }

             $data = array(
                'category_id' => (!is_null(@$requestAll['category']))?array(implode(',',$requestAll['category'])):false,
                'sub_category_id' => false,
                'child_category_id' => false,
                'url' => (!is_null($requestAll['url'])) ? $requestAll['url'] : false,
                'brand' => (!is_null(@$requestAll['brand'])) ? array(implode(',',$requestAll['brand'])) : false,
                'min_price' => (!is_null($requestAll['minPrice'])) ? $requestAll['minPrice'] : false,
                'max_price' => (!is_null($requestAll['maxPrice'])) ? $requestAll['maxPrice'] : false,
                'min_discount' => (!is_null($requestAll['minDiscount'])) ? $requestAll['minDiscount'] : false,
                'max_discount' => (!is_null($requestAll['maxDiscount'])) ? $requestAll['maxDiscount'] : false,
                // 'coupon' => (!is_null($requestAll['coupan'])) ? array(implode(',',$requestAll['coupan'])) : false,
                'coupon' => (($requestAll['coupan'][0] == 'false')) ? false : array(implode(',',$requestAll['coupan']))
            );
            
            ## ACCORDING TO PANEL WE HAVE TO CHANEG THE VALUES OF PAGE AND PANEL
            if($panel === 'middle'){ $page = 'h';$panel_name = 'middle'; }
            if($panel === 'bottom'){ $page = 'h';$panel_name = 'bottom'; }
            if($panel === 'exclusive'){ $page = 'c';$panel_name = ''; }
            if($panel === 'offer'){ $page = 'o';$panel_name = ''; }
            
            MobileAppSliders::where('id',$banner_id)->update([

                'page'=> $page,
                'panel'=> $panel_name,
                'title'=> $requestAll['title'],
                'image'=> (($requestFile ? $image :$img_path->image)),
                'filter_params'=> json_encode(array($data)),
                'created_by'=> $this->loggedInUser->ref_id,
                'updated_at' => Carbon::now()

            ]);

            return response()->json(['status' =>200, 'message' =>'Success! Banner Updated Successfully.']);

        
    }









}

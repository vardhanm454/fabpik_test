<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Redirect;
use Session;
use Validator;
use Mail;
use Avatar;
use Hash;
use File;
use Carbon\Carbon;

// Models
use App\Models\User;
use App\Models\Seller;
use App\Models\SellerWarehouses;
use App\Models\Category;
use App\Models\VerifyOtp;
use App\Models\Brand;
use App\Models\Notifications;
use App\Models\CommissionModel;

//emails
use App\Mail\SellerApprovalEmail;
use App\Mail\AdminSellerApprovalNotificationEmail;
use App\Mail\SellerRejectEmail;
use App\Mail\SellerDeactivateEmail;
use App\Mail\AdminSellerRejectNotificationEmail;
use App\Mail\VerificationEmail;
use App\Mail\NewSellerRegistrationAdmin;
use App\Mail\SellerReActivateEmail;
use App\Mail\AdminSentCommission;

class SellerController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'sellers';
    public $activeSubmenu = '';

    public function __construct() {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });        
    }

     public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('sellers_perpage', 10);
        $this->data['dt_page'] = Session::get('sellers_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,5,6,7,8,9,10';
        $this->data['dt_ajax_url'] = route('admin.sellers.getAjaxListData');
        $this->data['dt_search_colums'] = ['fstatus', 'fapproval', 'fcategory','freference', 'ffromdate', 'fcompanyname', 'ftodate', 'fname', 'fmobile', 'femail', 'fsellercode', 'fcommission'];

        $this->data['title'] = 'Sellers List';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Sellers',
            ],
        ];

        $this->data['categories'] = Category::where('status',1)->get();
        $this->data['commissionModels'] = CommissionModel::get();

        return parent::adminView('seller.index', $this->data);
    }

    /**
     * [get all Sellers and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'created_at',
            2 => 'name',
            3 => 'company_name',
            4 => 'company_type',
            5 => 'email',
            6 => 'mobile',
            7 => 'mobile',
            8 => 'created_at',
            9 => 'approval_status',
            10 => 'status',
            11 => 'status',
            12 => 'status',
            13 => 'status',
            14 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('seller', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Seller::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Sellers are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        if ($request->input('customActionName') == "changestatus") {
            DB::beginTransaction();
            try {
                Seller::whereIn('id', $request->input('id'))->update([
                    "status" => $request->input('status'),
                    "inactive_reason" => $request->input('inactive_reason')
                ]);

                if($request->input('status') == '1'){
                    // send seller account activation email
                    $ids = $request->input('id');
                    foreach($ids as $id){
                        $seller_details = Seller::findOrFail($id);
                        $sellerReActivateEmail = Mail::to($seller_details->email)->send(new SellerReActivateEmail($seller_details));
                    }
                }else{
                    $ids = $request->input('id');
                    foreach($ids as $id){
                        $seller_details = Seller::findOrFail($id);
                        $sellerDeactivateEmail = Mail::to($seller_details->email)->send(new SellerDeactivateEmail($seller_details));
                    }
                }
            
                DB::commit();
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Seller Status Changed successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        if ($request->input('customActionName') == "notify") {
            DB::beginTransaction();
            try {
                if($request->input('select_all') == '1'){

                    $tblData = new Notifications();
                    $tblData->title = $request->input('subject');
                    $tblData->notify_on = date("Y-m-d H:i:s", strtotime($request->input('notify_on')));
                    $tblData->content = $request->input('content');
                    $tblData->notified_to = 's';
                    $tblData->created_by = $this->loggedInUser->id;
                    $tblData->save();
                }
                else{
                    // $sellers = Seller::whereIn('id',$request->input('id'))->whereRaw('status',1)->whereRaw('approval_status',1)->whereNull('deleted_at')->select('id')->get();
                    // $sellers = User::whereIn('ref_id',$request->input('id'))->where('user_type','seller')->select('id')->get();
                    $sellers = User::whereIn('ref_id',$request->input('id'))->where('user_type','seller')->pluck('id');
                    $sellers = json_encode($sellers);
                    $sellers = json_decode($sellers);
                    $sellers = array_map('strval', $sellers);
                    $tblData = new Notifications();
                    $tblData->title = $request->input('subject');
                    $tblData->notify_on = date("Y-m-d H:i:s", strtotime($request->input('notify_on')));
                    $tblData->content = $request->input('content');
                    $tblData->created_by = $this->loggedInUser->id;
                    $tblData->notified_to = 's';
                    $tblData->users = json_encode($sellers);
                    $tblData->save(); 
                }
                
                DB::commit();
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Notification Updated successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
            'fcategory' => (!is_null($request->fcategory))?$request->fcategory:null,
            'fapproval' => (!is_null($request->fapproval))?$request->fapproval:null,
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'fname' => (!is_null($request->fname))?$request->fname:null,
            'fmobile' => (!is_null($request->fmobile))?$request->fmobile:null,
            'femail' => (!is_null($request->femail))?$request->femail:null,
            'freference' => (!is_null($request->freference))?$request->freference:null,
            'fcompanyname' => (!is_null($request->fcompanyname))?$request->fcompanyname:null,
            'fsellercode' => (!is_null($request->fsellercode))?$request->fsellercode:null,
            'fcommission' => (!is_null($request->fcommission))?$request->fcommission:null,
            
        ];
        
        $sellers = Seller::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $sellers->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $featuredList = [
            ["danger" => "No"],
            ["primary" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];
        foreach ($sellers as $k=>$seller) {
            $catArr = Category::find($seller->categories);
            if(!empty($catArr)): $catName = $catArr->title; else: $catName = ''; endif;
            
            $approvalStatus = is_null($seller->approval_status) ? ( '<span class="badge badge-light-danger badge-roundless">Pending</span>' ) : ( ($seller->approval_status==0)?'<span class="badge badge-light-danger badge-roundless">Not Approved</span>':'<span class="badge badge-light-success badge-roundless">Approved</span>' );

            $accountStatus = ($seller->status==0)?'<span class="badge badge-light-danger badge-roundless">Inactive</span>':'<span class="badge badge-light-success badge-roundless">Active</span>';

            $actionBtns = ($canChange)?'
            
            <a href="'.route('admin.sellers.edit', ['id'=>$seller->id]).'" title="Edit" class="btn btn-icon-only default btn-circle"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="'.route('admin.sellers.view', ['id'=>$seller->id]).'" class="btn btn-icon-only default btn-circle" title="View & Verify"><span aria-hidden="true" class="icon-eye"></span> </a>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$seller->id.'"/><span></span></label>',
                $k+1,
                $seller->seller_code,
                $seller->name,
                $seller->company_name,
                $seller->company_type,
                ($seller->commission_id) ? '<a href="'.route('admin.commissionmodels').'?search=1&fname='.CommissionModel::getCommission($seller->commission_id)->name.'" title="Commission Model" target="_blank">'.CommissionModel::getCommission($seller->commission_id)->name.'</a>' : '-',
                $seller->email,
                $seller->mobile,
                $catName,
                $seller->created_at->format('d M Y'),
                $seller->approved_on,
                $approvalStatus,
                $accountStatus,
                '<div class="">
                '.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }


    /**
     * [Manage Profile Page View]
     * @return [type] [description]
     */
    public function edit(Request $request, $id)
    {
        $this->data['title'] = 'Edit Seller Profile';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.sellers',
                'title' => 'Sellers',
            ],
            (object) [
                'url' => false,
                'title' => 'Edit Seller Profile',
            ],
        ];
        $this->data['seller_details'] = Seller::find($id);
        $this->data['seller_warehouse_details'] = SellerWarehouses::where('seller_id',$id)->where('status',1)->get();

        return parent::adminView('seller.edit', $this->data);
    }

    /**
     * [Store Seller Basic Details]
     * @return [type] [description]
     */
    public function basicDetails(Request $request){
        $validator = Validator::make($request->all(), []);
        $isUserExists = User::where('email', strtolower($request->email))->where('ref_id', '<>', $request->seller_id)->where('user_type','seller')->exists();
        $isMobileExists = User::where('mobile', $request->mobile)->where('ref_id', '<>', $request->seller_id)->where('user_type','seller')->exists();
        if($request->convenience_fee == null && $request->min_order_amount != null){
            $validator->after(function ($validator) {
                $validator->errors()->add('convenience_fee', 'The convenience fee field is required when min order amount is present.');
            });
        }
        if( $request->convenience_fee != null && $request->min_order_amount == null){
            $validator->after(function ($validator) {
                $validator->errors()->add('min_order_amount', 'The min order amount field is required when convenience fee is present.');
            });
        }
        if (!is_null($request->shop_url)) {
            $isUrlExists = Seller::where('url_slug', $request->shop_url)->where('id', '<>', $request->seller_id)->exists();
            if ($isUrlExists) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('shop_url', 'URL already exist!');
                });
            }
        }

         /*Check the brand exists in Brands Table, 
         *  if not exists in brands table check it in sellers table brand_name column
         *  if exists throw an error else insert the brand in both seller and brands table.
        */
        $getCommonBrand = Brand::where('name', $request->brand_name)->whereNull('created_by');
        // $getSellerBrand = Seller::where('brand_name', $request->brand_name)->where('id', '<>', $request->seller_id);
        $getSellerBrand = Brand::where('name', $request->brand_name)->whereNotNull('created_by')->where('created_by', '<>', $request->seller_id);
        if (!is_null($request->brand_name)) {
            $isCommonBrandExist = $getCommonBrand->exists();
            if (!$isCommonBrandExist) {
                $isBrandExist = $getSellerBrand->exists();
                if ($isBrandExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('brand_name', 'Brand already exist!');
                    });
                }
            }
        }

        if ($isUserExists) {
            $validator->after(function ($validator) {
                $validator->errors()->add('email', 'email already exist!');
            });
        }

        if ($isMobileExists) {
            $validator->after(function ($validator) {
                $validator->errors()->add('mobile', 'mobile already exist!');
            });
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput(['tab'=>'tab_1-1']);
        }

        $updatedData =  [
            'name' => trim($request->name),
            'email' => strtolower($request->email),
            'mobile' => $request->mobile,
            'categories' => implode(',',$request->categories),
            'chat_enable' => $request->chat_enable,
            'url_slug' => $request->shop_url,
            'store_name' => $request->store_name,
            'shipping_model' => $request->shipping_model,
            'convenience_fee' => $request->convenience_fee,
            'min_order_amount' => $request->min_order_amount,
            // 'brand_name' => $request->brand_name,
            // 'store_name' => $request->store_name,
        ];


        if($isCommonBrandExist){
            $getCommonBrand = $getCommonBrand->first();
            $updatedData['brand_name'] = $getCommonBrand->name;
            $updatedData['brand_id'] = $getCommonBrand->id;
        }else{
            if (!$isBrandExist) {
                $brandexists = Brand::where('created_by', $request->seller_id)->first();
                if ($brandexists == null) {
                    $tblData = new Brand();
                    $tblData->name = $request->brand_name;
                    $tblData->slug = __generateSlug($request->brand_name);
                    $tblData->show_on_home = 0;
                    $tblData->created_by = $request->seller_id;
                    $tblData->save();
                    $updatedData['brand_name'] = $tblData->name;
                    $updatedData['brand_id'] = $tblData->id;
                }else{;
                    Brand::where('created_by', $request->seller_id)->update([
                        'name' => $request->brand_name,
                    ]);
                    $updatedData['brand_name'] = $request->brand_name;
                    $updatedData['brand_id'] = $brandexists->id;
                }
            }
        }
      
        $sellerUpdate = Seller::where('id',$request->seller_id)->update($updatedData);
        if($request->has('email') || $request->has('mobile')){
            $userUpdate = User::where('ref_id',$request->seller_id)->where('user_type','seller')->update([
                'name' => trim($request->name),
                'email' => strtolower($request->email),
                'mobile' => $request->mobile,
            ]);
        }

        return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                        ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Seller Basic Details has been Updated Successfuly!']);
    }

    /**
     * [Store Seller Company Details]
     * @return [type] [description]
     */
    public function companyDetails(Request $request){
        $sellerUpdate = Seller::findOrFail($request->seller_id);
        try{
            if($request->has('company_reg_no')){
                if($request->gst_registered == 1 &&  $request->hasFile('gst_certificate')) {
                    $gstCertificate = time().'.'.$request->gst_certificate->extension();
                    $request->gst_certificate->move(UPLOAD_PATH.'/seller/gst_certificate', $gstCertificate);

                    $sellerUpdate->gst_certificate = $gstCertificate;
                }
                
                if($request->hasFile('incorporation_certificate')){
                    $incorporationCertificate = time().'.'.$request->incorporation_certificate->extension(); 
                    $request->incorporation_certificate->move(UPLOAD_PATH.'/seller/incorporation_certificate', $incorporationCertificate);
                    $sellerUpdate->incorporation_certificate = $incorporationCertificate;
                }
                $sellerUpdate->company_name = trim($request->company_name);
                $sellerUpdate->company_reg_no = trim($request->company_reg_no);
                $sellerUpdate->legal_entity = $request->legal_entity;
                $sellerUpdate->company_type = $request->company_type;
                $sellerUpdate->gst_registered = $request->gst_registered;
                $sellerUpdate->gst_no = $request->gst_no;
                $sellerUpdate->office_phone_no = $request->office_phone_no;
                $sellerUpdate->company_address = $request->company_address;
                $sellerUpdate->company_state = $request->company_state;
                $sellerUpdate->company_pincode = $request->company_pincode;
                
                if($sellerUpdate->save()){
                    return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                            ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Seller Company Details has been Updated Successfuly!']);
                }else{
                    return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                            ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Some Error in saving Company Details!']);
                }
            }else{
                return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                            ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Please Enter all Company details.']);
            }
        }catch (Exception $e){
            return redirect()
            ->back()
            ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>''])->withInput(['tab'=>'tab_3-3']);
        }

    }

    /**
     * [Store/Update Seller Warehouse Details]
     * @return [type] [description]
     */
    public function warehouseDetails(Request $request){
        if($request->is_default==1){
            $sellerWarehouses = SellerWarehouses::where('seller_id',$request->seller_id)->where('is_default',$request->is_default)->first();
            if(!empty($sellerWarehouses)){
                $defaultAddress = SellerWarehouses::find($sellerWarehouses->id);
                $defaultAddress->is_default = '0';
                $defaultAddress->save();
            }
        }
        if(!empty($request->warehouse_id)){
            $validator = Validator::make($request->all(), []);
            $isPickupNameExists = SellerWarehouses::where('shiprocket_pickup_nickname', $request->shiprocket_pickup_nickname)->where('id', '<>', $request->warehouse_id)->exists();
            // dd($isPickupNameExists);
            if ($isPickupNameExists) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('shiprocket_pickup_nickname', 'Shiprocket Pickup Nickname already exist!');
                });
            }
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput(['tab'=>'tab_4-4']);;
            }
            
            $sellerWarehouseUpdate = SellerWarehouses::where('id',$request->warehouse_id)->where('seller_id',$request->seller_id)->update([
                'name' => trim($request->warehouse_name),
                'is_default' => $request->is_default,
                'address' => $request->warehouse_address,
                'address_two' => $request->warehouse_addresstwo,
                'phone' => $request->warehouse_phone,
                'landmark' => $request->warehouse_landmark,
                'city' => $request->warehouse_city,
                'state_id' => $request->warehouse_state,
                'pincode' => $request->warehouse_pincode,
                'shiprocket_pickup_nickname' => $request->shiprocket_pickup_nickname,
            ]);
            return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'warehouse Details has been Updated Successfuly!']);
        }else{
            $seller_id = $request->seller_id;
            $newWarehouseDetails = [
                'seller_id' => $seller_id,
                'name' => trim($request->warehouse_name),
                'is_default' => $request->is_default,
                'phone' => $request->warehouse_phone,
                'address' => $request->warehouse_address,
                'address_two' => $request->warehouse_addresstwo,
                'landmark' => $request->warehouse_landmark,
                'city' => $request->warehouse_city,
                'state_id' => $request->warehouse_state,
                'pincode' => $request->warehouse_pincode,
                // 'shiprocket_pickup_nickname' => $request->shiprocket_pickup_nickname,
            ];
            
            //Store the new pickup location in shiprocket
            $result = json_decode( __addNewPickupLocation($newWarehouseDetails, $seller_id) );
            if($result->status == false){
                $message = 'Oops, Inavlid data!';
                $address = null;
                $addresstwo = null;
                $pickup_location = null;
                if(isset($result->result->errors)){
                    // $message = 'Please check the Address';

                    if( isset($result->result->errors->address) )
                        $address = $result->result->errors->address[0];

                    if( isset($result->result->errors->address_2) ) {
                        $addresstwo = 'Please check the Address 2';
                    }

                    if( isset($result->result->errors->pickup_location) ) {
                        $pickup_location = $result->result->errors->pickup_location[0];
                    }
                }
                return redirect()
                ->back()
                ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>$message, 'address'=>$address, 'addresstwo'=>$addresstwo ])->withInput(['tab'=>'tab_warehouse_details']);

            }else if($result->status == true){

                // dd($result->result);
                $newWarehouseDetails['shiprocket_pickup_nickname'] = $result->result->address->pickup_code;

                $sellerWarehouseUpdate = SellerWarehouses::create($newWarehouseDetails);

                return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'warehouse Details Added Successfuly!'])->withInput(['tab'=>'tab_warehouse_details']);
            }    
        }
    }

    /**
     * [Get Seller Warehouse Details By Id]
     * @return [type] [description]
     */
    public function getWarehouseDetails(Request $request){
        if(!empty($request->id)){
            $warehouseDetails = SellerWarehouses::find($request->id)->toArray();
            if($warehouseDetails){
                return $warehouseDetails;
            }else{
                return "error";
            }
            
        }
    }

    /**
     * [Delete Seller Warehouse Details By Id]
     * @return [type] [description]
     */
    public function deleteWarehouseDetails(Request $request){
        if(!empty($request->id)){
            $delete = SellerWarehouses::where('id',$request->id)->delete();
            if($delete){
                return "success";
            }else{
                return "error";
            }
            
        }
    }

    /**
     * [Store Seller POC (Point Of Contact) Details]
     * @return [type] [description]
     */
    public function pocDetails(Request $request){
        $sellerUpdate = Seller::where('id',$request->seller_id)->update([
            'poc_name' => trim($request->poc_name),
            'poc_email' => strtolower($request->poc_email),
            'poc_mobile' => $request->poc_mobile,
            'poc_phone' => $request->poc_phone,
            'poc_designation' => $request->poc_designation,
        ]);
        return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Seller POC Details Added Successfuly!']);
    }

    /**
     * [Store Seller Bank Details]
     * @return [type] [description]
     */
    public function bankDetails(Request $request){
        $sellerUpdate = Seller::findOrFail($request->seller_id);
        if($request->has('confirm_account_no')){
            if($request->bank_account_no!=$request->confirm_account_no){
                return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                        ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Account Numbers do not match!']);
            }

            if($request->hasFile('cancelled_cheque_file')) {
                $cancelled_cheque_file = time().'.'.$request->cancelled_cheque_file->extension();  
                $request->cancelled_cheque_file->move(UPLOAD_PATH.'/seller/cancelled_cheque_files', $cancelled_cheque_file);
                $sellerUpdate->cancelled_cheque_file = $cancelled_cheque_file;
            }

            $sellerUpdate->bank_account_name = trim($request->bank_account_name);
            $sellerUpdate->bank_name = trim($request->bank_name);
            $sellerUpdate->bank_account_no = $request->bank_account_no;
            $sellerUpdate->ifsc_code = $request->ifsc_code;
            $sellerUpdate->swift_code = $request->swift_code;
            $sellerUpdate->branch_name = $request->branch_name;
            if($sellerUpdate->save()){
                return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                        ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Seller Bank Details has been Updated Successfuly!']);
            }else{
                return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                        ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Some Error in saving Bank Details!']);
            }
            
        }
    }

    /**
     * [Profile Page View]
     * @return [type] [description]
     */
    public function view(Request $request, $id)
    {
        $this->data['title'] = 'View Seller Profile';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.sellers',
                'title' => 'Sellers',
            ],
            (object) [
                'url' => false,
                'title' => 'View Seller Profile',
            ],
        ];
        $this->data['seller_details'] = Seller::find($id);
        $this->data['seller_warehouse_details'] = SellerWarehouses::where('seller_id',$id)->where('status',1)->get();

        return parent::adminView('seller.view', $this->data);
    }

    /**
     * [Seller Approval]
     * @return [type] [description]
     */
    public function approveSeller(Request $request){
        $seller_details = Seller::findOrFail($request->id);
        if($seller_details->approval_status==1){
            return Redirect::route('admin.sellers.view', ['id'=>$request->id])
                        ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Already Approved!']); 
        }else{
            $seller_details->approval_status = 1;
            $seller_details->approved_on = date("Y-m-d h:i:s");
            if ($seller_details->save()) {
                $to=[
                    'seller_email'=>$seller_details->email,
                ];
                __sendEmails($to, 'seller_approval', $seller_details);
                //Approval Mail to Seller and Notification to Admin
                // $sellerApprovalEmail = Mail::to($seller_details->email)->send(new SellerApprovalEmail($seller_details));
                // $adminSellerApprovalNotificationEmail = Mail::to(User::where('customer_id','=',NULL)->where('seller_id','=',NULL)->first()->email)->send(new AdminSellerApprovalNotificationEmail($seller_details));
            }
        }
    }

    /**
     * [Seller Reject]
     * @return [type] [description]
     */
    public function rejectSeller(Request $request){
        $seller_details = Seller::findOrFail($request->id);
        if($seller_details->approval_status==0){
            return Redirect::route('admin.sellers.view', ['id'=>$request->id])
                        ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Already Not Approved!']); 
        }else{
            $seller_details->approval_status = 0;
            $seller_details->approved_on = NULL;
            if($seller_details->save()){

                //Reject Mail to Seller
                $sellerRejectEmail = Mail::to($seller_details->email)->send(new SellerRejectEmail($seller_details));
                $adminSellerRejectNotificationEmail = Mail::to(User::where('ref_id','=',NULL)->where('ref_id','=',NULL)->first()->email)->send(new AdminSellerRejectNotificationEmail($seller_details));


                return Redirect::route('admin.sellers.view', ['id'=>$request->id])
                        ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Seller Vverification Rejected successfully!']);
            }
        }
    }

    /**
     * [Add Seller]
     * @return [type] [description]
     */
    public function add(Request $request)
    {
        $this->data['title'] = 'Add Seller';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.sellers',
                'title' => 'Sellers',
            ],
            (object) [
                'url' => false,
                'title' => 'Add Seller',
            ],
        ];

        return parent::adminView('seller.add', $this->data);
    }

     /**
     * [Function to send and verify OTP]
     * @param  Request $request [mobile]
     * @return [type]           [message]
     */
    public function processMobileVerification(Request $request){
        if($request->has('mobile')){
            $respData = $request->all();
            
            $validator = Validator::make($respData, [
                'email' => 'required|email|unique:users,email',
                'mobile' => 'required|integer|digits:10|unique:users,mobile',
            ]);
             
            if($validator->fails()) {
                return response()->json(["type"=>"validationErrors", 'message'=>"errors", 'errors'=>$validator->getMessageBag()->toArray()], 200);
            }
            switch ($respData["action"]) {
                case "sent_otp":

                    $otp = __generateOtp();
                    $this->saveOTP($request->mobile, $otp);

                    if($this->sendOtp($request->mobile, $otp, 'verify_mobile')) {
                        return response()->json(["type"=>"success", "message"=>""]);
                    }else{
                        return response()->json(["type"=>"error", "message"=>""]);
                    }
                    break;

                case "verify_otp":
                    $otpData = VerifyOtp::where('mobile',  $respData['mobile'])->first();
                    if($respData['otp'] == $otpData->otp){
                        echo json_encode(["type"=>"success", "message"=>"Your mobile number is verified!"]);
                    }else{
                        echo json_encode(["type"=>"error", "message"=>"OTP Mis-Match, Check Once"]);
                    }
                    break;
            }
        }
    }
    
    /**
     * [Seller Registration]
     * @return [type] [description]
     */
    public function registerSeller(Request $request){
        //Check if mobile number is verified y OTP or not
        if($request->checkVerify=='verified'){
            $validator = Validator::make($request->all(), [
               'fullname' => 'required|string',
               'email' => 'required|email|unique:users,email',
               'mobile' => 'required|integer|digits:10|unique:users,mobile',
               'password' => 'required',
               'shop_name' => 'required|string|between:2,100',
               'shop_address' => 'required|string',
            ]);
            
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            //Create New Seller
            $seller = new Seller();
            $seller->name = trim($request->fullname);
            $seller->email = strtolower($request->email);
            $seller->seller_code = __generateSellerCode();
            $seller->mobile = $request->mobile;
            $seller->company_name = $request->shop_name;
            $seller->url_slug = $request->shop_url;
            $seller->company_address = $request->shop_address;
            $seller->save();

            $avatar = md5(uniqid().uniqid()).'.jpg';
            Avatar::create($seller->name)->save('uploads'.DIRECTORY_SEPARATOR.'avatar'.DIRECTORY_SEPARATOR.$avatar, 100);

            //Create a new Seller User
            $user = new User();
            $user->seller_id = $seller->id;
            $user->name = $seller->name;
            $user->avatar = $avatar;
            $user->email = $seller->email;
            $user->mobile = $seller->mobile;
            $user->password = Hash::make($request->password);
            $user->remember_token = Str::random(32);
            $user->email_verified_at = date("Y-m-d h:i:s");
            $user->save();

            // assigining Seller role to the User
            $user->assignRole('Seller');

            $folder = ('uploads'.DIRECTORY_SEPARATOR.$seller->seller_code);
            //create folders for seller
            if (!File::exists($folder)) {
                mkdir($folder, 0777);
                chmod($folder, 0777);
                
                mkdir($folder.'/products', 0777);
                chmod($folder.'/products', 0777);

                mkdir($folder.'/sizechart', 0777);
                chmod($folder.'/sizechart', 0777);
            }

            //Send Email for Verification to User

            // $sellerVerificationEmail = Mail::to($user->email)->send(new VerificationEmail($user));
            $adminEmailAfterSellerRegistration = Mail::to(User::where('ref_id','=',NULL)->where('ref_id','=',NULL)->first()->email)->send(new NewSellerRegistrationAdmin($seller, 'admin'));
            Mail::to($user->email)->send(new NewSellerRegistrationAdmin($seller, 'admin_register_seller'));

            return Redirect::route('admin.sellers')
                        ->with(['toast'=>'1','status'=>'success','title'=>'Seller Registered Successfuly','message'=>'Please ask seller Login to continue !']);
        }else{
            return Redirect::route('admin.sellers.add')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Please verify seller mobile number first!']);
        }
    }

    public function sendOtp($mobile, $otp, $type='verify_mobile')
    {
        // return Utility::sendSms($mobile, "Your verification code is {$otp} for {$type} in Fabpik.");
        __sendSms($mobile, $type, ['otp'=>$otp]);
        return true;
    }

    public function saveOTP($mobile, $otp)
    {
        return VerifyOtp::updateOrInsert([
            'mobile' => $mobile
        ],
        [
            'mobile' => $mobile,
            'otp' => $otp,
            'otp_expiry' => Carbon::now()->addMinutes(3)
        ]);
    }

    /**
     * [Seller Commsion Details]
     * @return [type] [description]
    */
    public function commissionDetails(Request $request){
        $sellerUpdate = Seller::findOrFail($request->seller_id);
        $sellerUpdate->commission = $request->commission_value;
        // $sellerUpdate->commission_type = $request->commission_type;
        $sellerUpdate->commission_id = $request->commission_type;
        $sellerUpdate->commission_accept = 0;
        // dd($request->all());
        DB::beginTransaction();
        if($sellerUpdate->save()){
            try {
                $seller_email = Seller::where('id',$request->seller_id)
                            ->first();

                $seller_data = [
                    'comission_value' => $sellerUpdate->commission,
                ];
                $to=[
                    'seller_email'=>$seller_email->email,
                    // 'seller_email'=>'vardhanm@mirakitech.com',

                ];
                // dd($to);
                Mail::to($to['seller_email'])->send(new AdminSentCommission($seller_data));

                DB::commit();
                return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                        ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>'Seller Commission Details has been Updated Successfuly!']);
            } 
            catch (Exception $e) {
                return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                        ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Some Error in saving Commission Details!']);
                DB::rollback();
            }
        }
    }
    
    /**
     * [Seller COD Model change]
     * @return [type] [description]
    */
    
    public function changeCodDetails(Request $request)
    {
       
        if ($request->cod_old_value != $request->cod_enable) {
            $sellerUpdate = Seller::findOrFail($request->seller_id);
            $sellerUpdate->cod_enable = $request->cod_enable;
            if ($sellerUpdate->save()) {
                return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                    ->with(['toast'=>'1','status'=>'success','title'=>'Thank You','message'=>''])->withInput(['tab'=>'tab_cod']);
            } else {
                return Redirect::route('admin.sellers.edit', ['id'=>$request->seller_id])
                    ->with(['toast'=>'1','status'=>'error','title'=>'Error','message'=>'Some Thing went Wrong, Try After Some Time!'])->withInput(['tab'=>'tab_cod']);
            }
        }else {
            return redirect()->back()->withInput(['tab'=>'tab_cod']);
        }
    }

    /**
     * get All Seller Pickup Locations
     * 
    */
    public function getAllPickupLocations(){

        $locations = __getAllPickupLocations();
        dd($locations);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\PaymentStatus;
use App\Models\ShippingStatus;
use App\Models\Seller;

// Exports

class ShippingController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'shipping';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('shipping_perpage', 10);
        $this->data['dt_page'] = Session::get('shipping_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.shipping.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname', 'ffromdate', 'ftodate', 'fcorderid', 'fsstatus', 'fseller', 'fnumber', 'fdelivereddate', 'fexcepteddeliverydate', 'fpincode', 'fproductsku', 'fproductname'];

        $this->data['title'] = 'Shipping';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Shipping',
            ]
        ];

        $this->data['sellers'] = Seller::get();
        $this->data['shippingStatus'] = ShippingStatus::get();

        return parent::adminView('shipping.index', $this->data);
    }

    /**
     * [get all Order Details with shipping Status and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'created_at',
            2 => 'child_order_id',
            3 => 'child_order_id',
            4 => 'child_order_id',
            5 => 'child_order_id',
            6 => 'child_order_id',
            7 => 'child_order_id',
            8 => 'child_order_id',
            9 => 'quantity',
            10 => 'child_order_id',
            11 => 'child_order_id',
            12 => 'child_order_id',
            13 => 'child_order_id',
            14 => 'child_order_id',
            15 => 'child_order_id',
            16 => 'shipping_status_id',
            17 => 'child_order_id',
            18 => 'child_order_id',
            19 => 'child_order_id',
            20 => 'child_order_id',            
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('shipping', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                OrderDetail::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Records are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        if ($request->input('customActionName') == "changestatus") {
            DB::beginTransaction();
            try {
                __changeAdminShippingStatus($request->input('status'), $request->input('id'));
                DB::commit();
                
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Order Status Changed successfully.';
                
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'fsstatus' => (!is_null($request->fsstatus))?$request->fsstatus:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fnumber' => (!is_null($request->fnumber))?$request->fnumber:null,
            'fproductname' => (!is_null($request->fproductname))?$request->fproductname:null,
            'fproductsku' => (!is_null($request->fproductsku))?$request->fproductsku:null,
            'fpincode' => (!is_null($request->fpincode))?$request->fpincode:null,
            'fexcepteddeliverydate' => (!is_null($request->fexcepteddeliverydate))?date('Y-m-d', strtotime($request->fexcepteddeliverydate)):null,
            'fdelivereddate' => (!is_null($request->fdelivereddate))?date('Y-m-d', strtotime($request->fdelivereddate)):null,
            'fcorderid' => (!is_null($request->fcorderid))?$request->fcorderid:null,
        ];
        $orderDetails = OrderDetail::getAjaxShippingListData($criteria, $iPage, $orderColumn, $orderDir);
        
        $iTotalRecords = $orderDetails->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            "Pending" => "danger",
            "Ready for Pickup" => "info",
            "Pickup Scheduled" => "warning",
            "Shipped" => "success",
            "Delivered" => "success",
            "Delivery Unsuccessful" => "danger",
            "Undelivered Product Returned" => "primary",
            "Undelivered Product Return Confirmed" => "primary",
            "Schedule Return Pickup" => "info",
            "Delivery Unsuccessful" => "danger",
            "Return Pickup Initiated" => "dark",
            "Product Returned to Seller" => "warning",
            "Product Return Confirmed" => "success",
        ];

        /**
         * AWB will be changed when the order is in below status
         * 3 => Pickup Scheduled
         * 4 => Shipped
         * 5 => Delivered
         * 9 => Return - Schedule Return Pickup
         * 10 => Return Pickup Initiated
        */
        $awbShippingStatus = [2, 3, 4, 9, 10];

        foreach ($orderDetails as $orders) {

            $modifyAWB = '';
            
            if(in_array($orders->shipping_status_id, $awbShippingStatus)){
                //Modify AWB
                $modifyAWB = '<button type="button" class="btn btn-icon-only default btn-circle" title="Modify/Update AWB" data-toggle="modal" data-action="modify-awb" data-target="#modifyAWBModal" data-awb_child_order_id="'.$orders->id.'" data-current_awb_number="'.$orders->tracking_no.'" data-keyboard="false" data-backdrop="static">
                                    <span aria-hidden="true" class="fa-address-book"><small>AWB</small></span>
                                    </button>';
            }

            $actionBtns = ($canChange)?'<div class=""> '.$modifyAWB.' </div>':'';
            
            $ship_status = OrderDetail::getShippingData($orders->shipping_status_id)->seller_text;
            $status = $statusList[$ship_status];
            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$orders->id.'"/><span></span></label>',
                date('d-m-Y H:i A', strtotime($orders->created_at)),
                '<a href="'.route('admin.childorders.view', ['id'=>$orders->id]).'" class="" title="View Order Details">'.$orders->child_order_id.'</a>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$orders->seller->seller_code.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$orders->seller->name.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$orders->order->first_name.' '.$orders->order->last_name.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'. ( ($orders->productVarient) ? $orders->productVarient->name : '' ).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'. ( ($orders->productVarient) ? $orders->productVarient->unique_id : '' ).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'. ( ($orders->productVarient) ? $orders->productVarient->sku : '' ).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$orders->quantity.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'. ( ($orders->productVarient) ? $orders->productVarient->shipping_weight : '' ).'</div>', 
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'. ( ($orders->productVarient) ? $orders->productVarient->shipping_length*$orders->productVarient->shipping_breadth*$orders->productVarient->shipping_height : '' ) .'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$orders->order->shipping_pincode.'</div>', 
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$orders->tracking_no.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.OrderDetail::getOrderStatusData($orders->order_status_id)->admin_text.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.date('d-m-Y H:i A', strtotime($orders->excepted_delivery_date)).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.date('d-m-Y H:i A', strtotime($orders->created_at. ' + '.( ($orders->productVarient) ? $orders->productVarient->min_ship_hours : '0' ) .' days')).'</div>',
                ($orders->delivered_date)?date('d-m-Y H:i A', strtotime($orders->delivered_date)):'',
                '<div class="text-center" style="white-space: normal;word-break:break-word;"><span class="badge badge-light-'.($status).' badge-roundless">'.$ship_status.'</span></div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;"> <div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div> </div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }


    /**
     * [modify AWB Details]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function modifyAWBNumber(Request $request, $id)
    {
        try {

            //check the AWB number is exists DB;
            $isExists = OrderDetail::where('id', $id)->where("tracking_no", $request->awb_number)->exists();

            if (!$isExists) {
                $result = json_decode(__getShipRocketOrderStatus($request->awb_number));
           
                if ($result->tracking_data->track_status != 0) {
                    $trackingDetails = [
                                'tracking_no' => $result->tracking_data->shipment_track[0]->awb_code,
                                'shipment_id' => $result->tracking_data->shipment_track[0]->shipment_id,
                                'shiprocket_order_id' => $result->tracking_data->shipment_track[0]->order_id,
                                'tracking_url' => $result->tracking_data->track_url
                            ];

                    $updateDetails = OrderDetail::where('id', $id)->update($trackingDetails);

                    if ($updateDetails) {
                        return response()->json(['success'=>true, 'message'=>"AWB details updated successfully", 'err'=>false, 'errors'=>$result], 200);
                    }
                    return response()->json(['success'=>false, 'message'=>'Unable Update the Details, try after some time.', 'err'=>true, 'errors'=>$result], 200);
                }
                return response()->json(['success'=>false, 'message'=>"There is no activities found with that AWB Number", 'err'=>true, 'errors'=>$result], 200);
            }
            return response()->json(['success'=>false, 'message'=>"AWB Number is already assigned to another order, please check once.", 'err'=>true, 'errors'=>[]], 200);

        } catch (Exception $e){
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'err'=>true, 'errors'=>[]], 200);
        }
    }
}
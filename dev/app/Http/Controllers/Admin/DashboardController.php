<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use DB;

// Models
use App\Models\Category;
use App\Models\Brand;
use App\Models\Attribute;
use App\Models\Coupon;
use App\Models\Product;
use App\Models\Seller;
use App\Models\Customer;

use App\Models\SellerWarehouses;
use App\Models\User;
use App\Models\Account;
use App\Models\State;
use App\Models\OrderDetail;
use App\Models\Ticket;

class DashboardController extends CoreController
{
	public $loggedInUser;
    public $data;
    public $activeMenu = 'dashboard';
    public $activeSubmenu = '';

    public function __construct() {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });        
    }

    public function index()
    {
        $this->data = [
            'title' => 'Dashboard',
            'activeMenu' => $this->activeMenu,
            'activeSubmenu' => $this->activeSubmenu,
            'breadcrumbs' => (object) [
                (object) [
                    'url' => false,
                    'title' => 'Dashboard',
                ],
            ]
        ];

        $this->data['ordersToday'] = OrderDetail::whereIn('shipping_status_id', [1,2,3,4,5])
                                                ->whereIn('order_status_id', [1,4,8,9,10])
                                                ->whereRaw("DATE(`created_at`) = CURDATE()")
                                                ->count();

        $this->data['TotalOrders'] = OrderDetail::count();

        $this->data['totalSales'] = OrderDetail::whereIn('shipping_status_id', [2,3,4,5])
                                                ->whereIn('order_status_id', [1,4,8,9,10])
                                                // ->whereRaw("DATE(`created_at`) = CURDATE()")
                                                ->sum('total');

        $this->data['totalSalesToday'] = OrderDetail::whereIn('shipping_status_id', [1,2,3,4,5])
                                                    ->whereIn('order_status_id', [1,4,8,9,10])
                                                    ->whereRaw("DATE(`created_at`) = CURDATE()")
                                                    ->sum('total');

        $this->data['ordersToShip'] = OrderDetail::whereIn('shipping_status_id', [2,3])
                                                ->whereIn('order_status_id', [1,4,8,9,10])
                                                ->count();

        $this->data['ordersDelivered'] = OrderDetail::whereIn('shipping_status_id', [5])
                                                    ->whereIn('order_status_id', [8,9])
                                                    ->count();

        $this->data['totalAmount'] = OrderDetail::whereIn('shipping_status_id', [2,3,4,5])
                                                ->whereIn('order_status_id', [1,4,8,9,10])
                                                ->sum('total');

        $this->data['payoutAmount'] = OrderDetail::where('payout_status', 1)
                                                ->whereIn('shipping_status_id', [2,3,4,5])
                                                ->whereIn('order_status_id', [1,4,8,9,10])
                                                ->sum('final_amount_to_pay_seller');
        
        $this->data['pendingpayoutAmount'] = OrderDetail::whereIn('shipping_status_id', [2,3,4,5])
                                                ->whereIn('order_status_id', [1,4,8,9,10])
                                                ->sum('final_amount_to_pay_seller');

        // $this->data['totalPendingAmount'] = $this->data['totalAmount'] - $this->data['payoutAmount'];
        $this->data['totalPendingAmount'] = $this->data['pendingpayoutAmount'] - $this->data['payoutAmount'];

        // get today orders
        $this->data['todayOrdersStats'] = [];
        $totalTodayOrders = 0;
        $today = date('d-m-Y');
        $orderToday = DB::select("SELECT DATE_FORMAT(created_at, '%d-%m-%Y') AS day, count(*) as total FROM order_details WHERE DATE_FORMAT(created_at, '%d-%m-%Y')='".$today."' GROUP BY DATE_FORMAT(created_at, '%d-%m-%Y')");

        if (count($orderToday) >= 1) {
            $totalTodayOrders = $orderToday[0]->total;
        }

        $this->data['todayOrdersStats'][] = [
        'day' => $today,
        'orders' => $totalTodayOrders,
        ];

        // get last 7 days total orders and sales
        $this->data['ordersStats'] = [];
        for ($i = 6; $i >= 0; $i--) {
            $totalOrders = 0;
            $totalSale = 0;
            $day = date('d-m-Y', strtotime("-$i days"));
        
            $order = DB::select("SELECT DATE_FORMAT(created_at, '%d-%m-%Y') AS day, count(*) as total FROM order_details WHERE DATE_FORMAT(created_at, '%d-%m-%Y')='".$day."' GROUP BY DATE_FORMAT(created_at, '%d-%m-%Y')");
            if (count($order) >= 1) {
                $totalOrders = $order[0]->total;
            }

            $this->data['ordersStats'][] = [
        'day' => $day,
        'orders' => $totalOrders,
        ];
        }

        // get last 6 months
        $this->data['orderSixMonthStats'] = [];
        for ($i = -2; $i < 6; $i++) {
            $totalSale = 0;
            $totalOrders = 0;
            $month = date('m-Y', strtotime("-$i month"));
        
            $ordcount = DB::select("SELECT DATE_FORMAT(created_at, '%m-%Y') AS month, count(*) as total FROM order_details WHERE DATE_FORMAT(created_at, '%m-%Y')='".$month."' GROUP BY DATE_FORMAT(created_at, '%d-%m-%Y')");
            if (count($ordcount) >= 1) {
                $totalOrders = $ordcount[0]->total;
            }

            $this->data['orderSixMonthStats'][] = [
        'month' => str_replace('-', '/', $month),
        'orders' => $totalOrders,
        ];
        }

        //top selling
        $this->data['topselling'] = DB::select("SELECT product_variant_id, seller_id, name,  SUM(quantity) as total_sold FROM `order_details` GROUP BY seller_id, product_variant_id ORDER BY total_sold DESC LIMIT 10");

        //featured
        $this->data['featured'] = DB::select("SELECT `name`, mrp, sell_price FROM `products` WHERE seller_featured = 1 ORDER BY id DESC LIMIT 5");

        //Latest Orders
        $this->data['latestOrders'] = DB::select("SELECT od.id, c.name, od.created_at, od.total, os.seller_text FROM order_details od JOIN orders o ON od.order_id = o.id JOIN customers c ON c.id = o.customer_id JOIN order_statuses os ON os.id = od.order_status_id ORDER BY od.id DESC LIMIT 5");

        //support tickets
        $this->data['ticket_details'] = Ticket::get();

        return parent::adminView('dashboard', $this->data);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Excel;
use DB;

// Models
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\ChildCategory;
use App\Models\ProductCategory;

// Exports
use App\Exports\ChildCategoryExport;

class ChildCategoryController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'childcategories';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Child Categories listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('childcategories_perpage', 10);
        $this->data['dt_page'] = Session::get('childcategories_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.childcategories.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Child Categories';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Child Categories',
            ],
        ];

        return parent::adminView('childcategory.index', $this->data);
    }

    /**
     * [get all Child Categories and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'title',
            2 => 'title',
            3 => 'title',
            4 => 'image',
            5 => 'icon',
            6 => 'status',
            7 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('subcategory', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                ChildCategory::whereIn('id', $request->input('id'))->delete();
                ProductCategory::where('childcategory_id', $request->input('id'))->update([
                    'is_active' => 0,
                ]);
                DB::commit();
                $records["customActionMessage"] = 'Selected Child Categories are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
            'fcategoryid' => ($request->fcategoryid)?:null,
            'fsubcategoryid' => ($request->fsubcategoryid)?:null,
        ];
        $childcategories = ChildCategory::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $childcategories->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];
        
        foreach ($childcategories as $childcategory) {
            $status = $statusList[$childcategory->status];
            
            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.childcategories.edit', ['id'=>$childcategory->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="'.route('admin.childcategories.delete', ['id'=>$childcategory->id]).'" del-url="'.route('admin.childcategories.delete', ['id'=>$childcategory->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><span aria-hidden="true" class="icon-trash"></span></a></div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$childcategory->id.'"/><span></span></label>',
                $childcategory->title,
                ($childcategory->subcategory)?$childcategory->subcategory->title:'',
                ($childcategory->category)?$childcategory->category->title:'',
                '<img src="'.route('ajax.previewImage', ['image'=>$childcategory->image,'type'=>'childcategory']).'" class="childcategorybanner-thumbnail-img" alt="'.$childcategory->title.'" height="75" width="75"/>',
                '<img src="'.route('ajax.previewImage', ['image'=>$childcategory->icon,'type'=>'childcategory']).'" class="childcategoryicon-thumbnail-img" alt="'.$childcategory->title.'" height="50" width="50"/>',
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                // '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export Child Categories]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null
        ];
        $childcategories = ChildCategory::getExportData($criteria);
        return Excel::download(new ChildCategoryExport($childcategories), 'Childcategories.xlsx');
    }

    /**
     * [Add New Child Categories]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function add(Request $request)
    {
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                "parentcategory"=>"required",
                "subcategory"=>"required",
                "name"=>"required",
                "description"=>"required|max:255",
                "bannerimage"=>"required",
                "iconimage"=>"required"
            ], []);

            /*if (!empty($request->name)) {
                $isExist = ChildCategory::where('title', $request->name)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Child Category name already exist!');
                    });
                }
            }*/

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            try {
                $tblData = new ChildCategory();
                $tblData->category_id = $request->parentcategory;
                $tblData->subcategory_id = $request->subcategory;
                $tblData->title = $request->name;
                $tblData->slug = __generateSlug($request->name);
                $tblData->description = $request->description;
                $tblData->image = $request->bannerimage;
                $tblData->icon = $request->iconimage;
                $tblData->meta_title = $request->meta_title;
                $tblData->meta_keywords = $request->meta_keywords;
                $tblData->meta_description = $request->meta_description;
                // $tblData->status = $request->status;
                $tblData->updated_at = date('Y-m-d H:i:s');

                if ($tblData->save()) {
                    DB::commit();

                    return redirect()
                            ->route('admin.childcategories')
                            ->with(['toast'=>'1','status'=>'success','title'=>'Sub Category','message'=>'Success! Child Category Added successfully.']);
                } else {
                    DB::rollback();

                    return redirect()
                            ->route('admin.childcategories')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Sub Category','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                DB::rollback();
                return back();
            }
        }

        $this->data['title'] = 'Add Child Category';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.childcategories',
                'title' => 'Child Categories',
            ],
            (object) [
                'url' => false,
                'title' => 'Add Child Category',
            ]
        ];

        $this->data['categories'] = Category::select('id', 'title')->where('status', 1)->get();
        return parent::adminView('childcategory.addEditChildCategory', $this->data);
    }

    /**
     * [edit Child Category data]
     * @param  Request $request [description]
     * @param  [type]  $id      [subcategory id]
     * @return [type]           [description]
    */
    public function edit(Request $request, $id)
    {
        $ChildCategory = ChildCategory::find($id);
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                "parentcategory"=>"required",
                "subcategory"=>"required",
                "name"=>"required",
                "description"=>"required|max:255",
                "bannerimage"=>"required",
                "iconimage"=>"required",
                "status"=>"required",
            ], []);

            /*if (!empty($request->name)) {
                $isExist = ChildCategory::where('title', $request->name)->where('id', '<>', $id)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Child Category name already exist!');
                    });
                }
            }*/

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            try {
                $tblData = [
                    'category_id' => $request->parentcategory,
                    'subcategory_id' => $request->subcategory,
                    'title' => $request->name,
                    'slug' => __generateSlug($request->name),
                    'description' => $request->description,
                    'image' => $request->bannerimage,
                    'icon' => $request->iconimage,
                    'meta_title' => $request->meta_title,
                    'meta_keywords' => $request->meta_keywords,
                    'meta_description' => $request->meta_description,
                    'status' => $request->status,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $update = ChildCategory::where('id', $id)->update($tblData);

                if ($update) {
                    DB::commit();

                    if ($request->save == 'save') {
                        return redirect()
                                ->route('admin.childcategories')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Child Categories','message'=>'Success! Child Category data updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Child Categories','message'=>'Success! Child Category data updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.childcategories')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Child Categories','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                DB::rollback();
                return back();
            }
        }

        $this->data['childcategory'] = $ChildCategory;
        $this->data['id'] = $id;
       
        $this->data['title'] = 'Edit Child Category';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.childcategories',
                'title' => 'Child Category',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['childcategory']->title,
            ],
        ];

        //get all the Categories list and Sub Category list based on category
        $this->data['categories'] = Category::select('id', 'title')->where('status', 1)->get();
        $this->data['subcategories'] = Subcategory::select('id', 'title')->where('category_id', $this->data['childcategory']->category_id)->where('status', 1)->get();
        return parent::adminView('childcategory.addEditChildCategory', $this->data);
    }

    /**
     * [Delete Child Category data]
     * @param  Request $request [description]
     * @param  [type]  $id      [subcategory id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $delete = ChildCategory::where('id', $id)->delete();
            DB::commit();
            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }

    /**
     * [child categories auto complete with pull path]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function autocomplete(Request $request)
    {
        $term = $request->q.'%';

        $categories = DB::table('view_categories')
                        ->where(function($q) use($term) {
                            $q->where('child_title', 'LIKE', $term.'%')
                                ->orWhere('subcategory_title', 'LIKE', $term.'%')
                                ->orWhere('category_title', 'LIKE', $term.'%');
                        });
        if(!empty($request->c)) $categories = $categories->where('category_id', $request->c);
                        
        $categories = $categories->get();

        $response = [];
        foreach ($categories as $key => $category) {
            $response[] = [
                'id' => $category->path_id,
                'text' => strtoupper($category->path_title)
            ];
        }

        return response()->json($response);
    }
}
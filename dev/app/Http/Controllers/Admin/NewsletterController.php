<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models
use App\Models\Newsletter;

// Exports
use App\Exports\NewsletterExport;

class NewsletterController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'newsletter';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [newsletter listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('newsletter_perpage', 10);
        $this->data['dt_page'] = Session::get('newsletter_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.newsletter.getAjaxListData');
        $this->data['dt_search_colums'] = ['ffromdate','ftodate'];

        $this->data['title'] = 'Newsletter';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Newsletter',
            ]
        ];

        return parent::adminView('newsletter.index', $this->data);
    }

    /**
     * [get all newsletters for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'subject',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('coupon', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Newsletter::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Newsletters are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'ffromdate' => (!is_null($request->ffromdate))?$request->ffromdate:null,
            'ftodate' => (!is_null($request->ftodate))?$request->ftodate:null
        ];
        $newsletters = Newsletter::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $newsletters->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));
        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($newsletters as $newsletter) {

            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.newsletter.edit', ['id'=>$newsletter->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('admin.newsletter.delete', ['id'=>$newsletter->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$newsletter->id.'"/><span></span></label>',
                $newsletter->subject,
                $newsletter->content,
                date('d-m-Y H:i:s', strtotime($newsletter->notify_on)),
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export newsletters]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null,
        ];
        $newsletters = Newsletter::getExportData($criteria);

        return Excel::download(new NewsletterExport($newsletters), 'newsletter.xlsx');
    }

    /**
     * [add newsletter data]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {
        if ($request->save) {
            // // dd(date('Y-m-d H:i:s', strtotime($request->start_date)));
            // dd(date('Y-m-d H:i:s', strtotime("20-12-2021 12:13:23")));
            $validator = Validator::make($request->all(), [
                'subject' => 'required|string',
                'content' => 'required|string',
                'notify_on'=>'required|after_or_equal:today',
            ]);

            if ($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            try {
                $newsletters = new Newsletter();
                $newsletters->subject = $request->subject;
                $newsletters->content = $request->content;
                $newsletters->notify_on = date('Y-m-d H:M:S', strtotime($request->notify_on));
                $newsletters->created_by = $this->loggedInUser;
                $newsletters->created_at = date('d-m-Y H:i:s');
                
                if ($newsletters->save()) {
                    return redirect()
                        ->route('admin.newsletter')
                        ->with(['toast'=>'1','status'=>'success','title'=>'newsletter','message'=>'Success! New Coupon added successfully.']);
                } else {
                    return redirect()
                        ->route('admin.newsletter')
                        ->with(['toast'=>'1','status'=>'error','title'=>'newsletter','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                return redirect()
                        ->route('admin.newsletter')
                        ->with(['toast'=>'1','status'=>'error','title'=>'newsletter','message'=>'Error! Some error occured, please try again.']);
            }
        }
        
        $this->data['title'] = 'Add Newsletter';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.newsletter',
                'title' => 'Newsletter',
            ]
        ];
        return parent::adminView('newsletter.addEdit', $this->data);
    }

    /**
     * [edit newsletter data]
     * @param  Request $request [description]
     * @param  [type]  $id      [brand id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $newsletter = Newsletter::find($id);
        if ($request->save) {
            // dd($request->mobile_numbers);
            $validator = Validator::make(
                $request->all(),
            [
                'subject' => 'required|string',
                'content' => 'required|string',
                'notify_on'=>'required|after_or_equal:today',
            ]
            );
            
            if ($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            try {

                $tblData = [
                    'subject' => $request->code,
                    'content' => $request->dis_type,
                    'notify_on' => date('Y-m-d H:M:S', strtotime($request->start_date)),
                    'updated_at' => date('d-m-Y H:i:s'),
                    'updated_by' => $this->loggedInUser,
                ];

                $update = Newsletter::where('id', $id)->update($tblData);

                if ($update) {
                    if ($request->save == 'save') {
                        return redirect()
                                ->route('admin.newsletter')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Newsletter','message'=>'Success! Coupon data updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Newsletter','message'=>'Success! Coupon data updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.newsletter')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Newsletter','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                return redirect()
                        ->route('admin.newsletter')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Newsletter','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['newsletter'] = $newsletter;
        $this->data['id'] = $id;
        
        $this->data['title'] = 'Edit Newsletter';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.newsletter',
                'title' => 'Newsletter',
            ],
            (object) [
                'url' => false,
                'title' => 'Edit',
            ],
        ];

        $this->data['editnewsletter'] = $newsletter;
        $this->data['id'] = $id;

        return parent::adminView('newsletter.addEdit', $this->data);
    }

    /**
     * [Delete Single Newsletter data]
     * @param  Request $request [description]
     * @param  [type]  $id      [coupon id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            $delete = Newsletter::where('id', $id)->delete();
            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }
}

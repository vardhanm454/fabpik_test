<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Excel;
use DB;

// Models
use App\Models\Category;
use App\Models\Attribute;
use App\Models\AttributeCategory;
use App\Models\ProductCategory;

// Exports
use App\Exports\CategoryExport;

// Repository
use Facades\App\Repository\CategoriesRepo;

class CategoryController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'categories';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Categories listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('categories_perpage', 10);
        $this->data['dt_page'] = Session::get('categories_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.categories.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Categories';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Categories',
            ],
        ];

        return parent::adminView('category.index', $this->data);
    }

    /**
     * [get all Categories and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'title',
            2 => 'image',
            3 => 'icon',
            4 => 'commission',
            5 => 'tax',
            6 => 'status',
            7 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('category', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Category::deleteChildCategory($request->input('id'));
                Category::deleteSubCategory($request->input('id'));
                Category::whereIn('id', $request->input('id'))->delete();

                ProductCategory::where('category_id', $request->input('id'))->update([
                        'is_active' => 0,
                ]);

                DB::commit();              

                // flush categories cache
                CategoriesRepo::flush();

                $records["customActionMessage"] = 'Selected Categories are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null
        ];
        $categories = Category::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $categories->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($categories as $category) {
            $status = $statusList[$category->status];
            $featured = $featuredList[$category->featured];

            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.categories.edit', ['id'=>$category->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span>
            </a>
            <a href="javascript:;" del-url="'.route('admin.categories.delete', ['id'=>$category->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><span aria-hidden="true" class="icon-trash"></span>
            </a>
            </div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$category->id.'"/><span></span></label>',
                $category->title,
                '<img src="'.route('ajax.previewImage', ['image'=>$category->image,'type'=>'category']).'" class="categorybanner-thumbnail-img" alt="'.$category->title.'" height="75" width="75"/>',
                '<img src="'.route('ajax.previewImage', ['image'=>$category->icon,'type'=>'category']).'" class="categoryicon-thumbnail-img" alt="'.$category->title.'" height="50" width="50"/>',
                // '<span class="label label-sm label-'.(key($featured)).'">'.(current($featured)).'</span>',
                $category->commission,
                $category->tax,
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                // '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export Categories]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null
        ];
        $categories = Category::getExportData($criteria);
        return Excel::download(new CategoryExport($categories), 'categories.xlsx');
    }

    /**
     * [Add New Categories]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                "name"=>"required|regex:/^(?!\d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 @&$]*)?$/",
                "description"=>"required|max:255",
                "banner_image"=>"required",
                "icon_image"=>"required",
                "primary_attribute"=>"required",
                // "secondary_attribute"=>"required",
                "commission"=>"required|regex:/^\d+(\.\d{1,2})?$/",
                "tax"=>"required|regex:/^\d+(\.\d{1,2})?$/"
            ], []);

            if (!empty($request->name)) {
                $isExist = Category::where('title', $request->name)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Category name already exist!');
                    });
                }
            }

            if(!empty($request->primary_attribute) 
                && !empty($request->secondary_attribute) 
                && ($request->primary_attribute == $request->secondary_attribute) ) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('secondary_attribute', 'Primary and Secondary attributes can not be same.');
                });
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            try {
                $tblData = new Category();
                $tblData->title = $request->name;
                $tblData->slug = __generateSlug($request->name);
                $tblData->description = $request->description;
                $tblData->image = $request->banner_image;
                $tblData->icon = $request->icon_image;
                $tblData->primary_attribute = $request->primary_attribute;
                $tblData->secondary_attribute = $request->secondary_attribute;
                $tblData->commission = $request->commission;
                $tblData->tax = $request->tax;
                $tblData->meta_title = $request->meta_title;
                $tblData->meta_keywords = $request->meta_keywords;
                $tblData->meta_description = $request->meta_description;
                $tblData->updated_at = date('Y-m-d H:i:s');
                $tblData->brands = json_encode($request->brands);

                if ($tblData->save()) {
                    DB::commit();

                    // flush categories cache
                    CategoriesRepo::flush();

                    return redirect()
                            ->route('admin.categories')
                            ->with(['toast'=>'1','status'=>'success','title'=>'Categories','message'=>'Success! Category Added successfully.']);
                } else {
                    DB::rollback();
                    return redirect()
                            ->route('admin.categories')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Categories','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                // dd($e);
                DB::rollback();
                return back();
            }
        }

        $this->data['attributes'] = Attribute::active()->pluck('name', 'id');

        $this->data['title'] = 'Add Category';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.categories',
                'title' => 'Categories',
            ],
            (object) [
                'url' => false,
                'title' => 'Add Category',
            ],
        ];
        return parent::adminView('category.addEditCategories', $this->data);
    }

    /**
     * [edit Category data]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $category = Category::find($id);
        
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                "name"=>"required|regex:/^(?!\d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 @&$]*)?$/",
                "description"=>"required|max:255",
                "banner_image"=>"required",
                "icon_image"=>"required",
                "primary_attribute"=>"required",
                // "secondary_attribute"=>"required",
                "commission"=>"required|numeric",
                "tax"=>"required|numeric",
                "status"=>"required",
            ], []);

            if (!empty($request->name)) {
                $isExist = Category::where('title', $request->name)->where('id', '<>', $id)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Category name already exist!');
                    });
                }
            }

            if(!empty($request->primary_attribute) 
                && !empty($request->secondary_attribute) 
                && ($request->primary_attribute == $request->secondary_attribute) ) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('secondary_attribute', 'Primary and Secondary attributes can not be same.');
                });
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            
            DB::beginTransaction();

            try {
                $tblData = [
                    'title' => $request->name,
                    'slug' => __generateSlug($request->name),
                    'description' => $request->description,
                    'image' => $request->banner_image,
                    'icon' => $request->icon_image,
                    'primary_attribute' => $request->primary_attribute,
                    'secondary_attribute' => $request->secondary_attribute,
                    'commission' => $request->commission,
                    'tax' => $request->tax,
                    'meta_title' => $request->meta_title,
                    'meta_keywords' => $request->meta_keywords,
                    'meta_description' => $request->meta_description,
                    'status' => $request->status,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'brands' => json_encode($request->brands),
                ];

                $update = Category::where('id', $id)->update($tblData);

                if ($update) {
                    DB::commit();                  

                    // flush categories cache
                    CategoriesRepo::flush();

                    if ($request->save == 'save') {
                        return redirect()
                                ->route('admin.categories')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Categories','message'=>'Success! Category data updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Categories','message'=>'Success! Category data updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.categories')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Categories','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                // dd($e);
                DB::rollback();
                return back();
            }
        }

        $this->data['attributes'] = Attribute::active()->pluck('name', 'id');
        
        $this->data['id'] = $id;
        $this->data['category'] = $category;

        $this->data['title'] = 'Edit Category';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.categories',
                'title' => 'Category',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['category']->title,
            ],
        ];
        return parent::adminView('category.addEditCategories', $this->data);
    }

    /**
     * [Delete Single Category data]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $delete = Category::where('id', $id)->delete();
            Category::deleteSubCategory([$id]);
            Category::deleteChildCategory([$id]);
            DB::commit();

            // flush categories cache
            CategoriesRepo::flush();

            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }

    /**
     * [Add Category Priorities]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
    */

    public function addPriority(Request $request){
        $this->data['categoryList'] = Category::getCategories();
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                'name.*' => 'required|distinct|numeric'
            ], [
                'name.*.required' => 'This field is required.',
                'name.*.distinct' => 'This field value need to be unique.',
                'name.*.numeric' => 'This field must be a number.'
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            // dd($request->name);
            DB::beginTransaction();
            foreach($request->name as $id=>$val){
                $updatePrority = Category::where('id', $id)->update(['priority'=> (int)$val]);
                if(!$updatePrority){
                    DB::rollback();
                    return redirect()
                                ->route('admin.categories.addPriority')
                                ->with(['toast'=>'1','status'=>'error','title'=>'Category Priorities','message'=>'Error! Some error occured, please try again.']);
                }
            }
            DB::commit();
            return redirect()
                        ->route('admin.categories.addPriority')
                        ->with(['toast'=>'1','status'=>'success','title'=>'Category Priorities','message'=>'Success! Category Prorities Updated Successfully.']);
        }
        $this->data['title'] = 'Category Priority';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.categories',
                'title' => 'Categories',
            ],
            (object) [
                'url' => false,
                'title' => 'Category Priorities',
            ],
        ];
        return parent::adminView('category.addEditCategoryPriorities', $this->data);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Session;
use Excel;
use DB;
use Hash;
use Mail;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

// Models
use App\Models\Permission;

class PermissionController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'usermgmt';
    public $activeSubmenu = 'permissions';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('permissions_perpage', 10);
        $this->data['dt_page'] = Session::get('permissions_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2';
        $this->data['dt_ajax_url'] = route('admin.permissions.getAjaxListData');
        $this->data['dt_search_colums'] = ['module_name','name'];

        $this->data['title'] = 'Permissions';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'User Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Permissions',
            ],
        ];
        $this->data['permissions'] = Permission::groupBy('module_name')->get();

        return parent::adminView('permissions.index', $this->data);
    }

    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'id',
            2 => 'module_name',
            3 => 'name'
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('permissions', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        $criteria = (object)[
            'length' => intval($request->length),
            'module_name' => (!is_null($request->module_name))?$request->module_name:null,
            'name' => (!is_null($request->name))?$request->name:null
        ];

        $permissions = Permission::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $permissions->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        $index = $iDisplayStart;
        foreach ($permissions as $permission) {
            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$permission->id.'"/><span></span></label>',
                ++$index,
                $permission->module_name,
                $permission->name,
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }
}
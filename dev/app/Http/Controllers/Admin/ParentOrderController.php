<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\PaymentStatus;
use App\Models\ShippingStatus;

// Exports

class ParentOrderController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'orders';
    public $activeSubmenu = 'parentorders';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index()
    { 
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('orders_perpage', 10);
        $this->data['dt_page'] = Session::get('orders_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.parentorders.getAjaxListData');
        $this->data['dt_search_colums'] = ['ffromdate', 'ftodate', 'fostatus', 'fptype', 'fpstatus', 'fparentid', 'fcustomername', 'fcustomermobile'];

        $this->data['title'] = 'Parent Orders';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Parent Order Management',
            ]
        ];

        $this->data['orderStatus'] = OrderStatus::get();
        $this->data['paymentStatus'] = PaymentStatus::get();

        return parent::adminView('parentorders.index', $this->data);
    }

    /**
     * [get all Parent Orders and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'created_at',
            2 => 'parent_order_id',
            3 => 'first_name',
            4 => 'mobile',
            5 => 'mobile',
            6 => 'mobile',
            7 => 'mobile',
            8 => 'mobile',
            9 => 'total',
            10 => 'total',
            11 => 'payment_type',
            12 => 'payment_type',
            13 => 'payment_type',
            14 => 'payment_type',
            15 => 'payment_type',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('order', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        if ($request->input('customActionName') == "changestatus") {
            DB::beginTransaction();
            try {
                // Order::whereIn('id', $request->input('id'))->update([
                //     "order_status_id" => $request->input('status'),
                //     "payment_status_id" => 1
                // ]);

                /**
                 * get all child order ids and send those to helper class function 
                **/              

                $childOrders = OrderDetail::whereIn('order_id', $request->input('id'))->pluck('id');

                __changeStatus($request->input('status'), $childOrders, $request->remark);

                DB::commit();
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Order Status Changed successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'fostatus' => (!is_null($request->fostatus))?$request->fostatus:null,
            'fptype' => (!is_null($request->fptype))?$request->fptype:null,
            'fpstatus' => (!is_null($request->fpstatus))?$request->fpstatus:null,
            'fparentid' => (!is_null($request->fparentid))?$request->fparentid:null,
            'fcustomername' => (!is_null($request->fcustomername))?$request->fcustomername:null,
            'fcustomermobile' => (!is_null($request->fcustomermobile))?$request->fcustomermobile:null,
        ];
        $orders = Order::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $orders->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($orders as $index=>$order) {  
            // $actionBtns = ($canChange)?'<a href="'.route('admin.categories.edit', ['id'=>$order->id]).'" class="btn blue tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>&nbsp;<a href="javascript:;" del-url="'.route('admin.categories.delete', ['id'=>$order->id]).'" class="btn red dt-list-delete tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>':'';
            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.parentorders.view', ['id'=>$order->id]).'" class="btn btn-icon-only default btn-circle" title="View Order Details">
                <span aria-hidden="true" class="icon-eye"></span> 
            </a>
            </div>':'';

            $mrp = $order->orderDetail->sum('total') + ( ($order->orderDetail->sum('total_seller_discount'))?$order->orderDetail->sum('total_seller_discount'):0 );
            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$order->id.'"/><span></span></label>',
                date('d-m-Y H:i A', strtotime($order->created_at)),
                $order->parent_order_id,
                $order->first_name,
                $order->mobile,
                count($order->orderDetail),
                number_format((float)$mrp, 2, '.', ''),
                $order->handling_charge,
                ($order->orderDetail->sum('total_seller_discount'))?$order->orderDetail->sum('total_seller_discount'):0,
                ($order->coupon_id==null)?'No':'Yes',
                ( ($order->coupon_code==null) ? '-' : $order->coupon_code ),
                number_format((float)$order->total, 2, '.', ''),
                ($order->payment_type=='c')?'COD':'Online',
                OrderDetail::getPaymentStatusData($order->payment_status_id)->admin_text,
                // OrderDetail::getOrderStatusData($order->order_status_id)->admin_text,
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [View Parent Products]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function view($id)
    {
        $this->data['title'] = 'Order Details';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.parentorders',
                'title' => 'Orders',
            ],
            (object) [
                'url' => 'javascript:;',
                'title' => 'Order Details',
            ]
        ];

        $this->data['id'] = $id;
        $this->data['orderDetails'] = Order::with('orderDetail', 'customer', 'customerAddress')
                                            ->where('id', $id)->first();
        $this->data['childOrders'] = OrderDetail::with('order','seller', 'productVarient', 'orderHistory')->where('order_id', $id)->get();
        $this->data['paymentStatus'] = PaymentStatus::get();
        $this->data['orderStatus'] = OrderStatus::get();
        $this->data['shippingStatus'] = ShippingStatus::get();

        return parent::adminView('parentorders.parentOrderDetails', $this->data);
    }

    /**
     * [View Invoice Products]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function invoice()
    {
        return parent::adminView('parentorders.invoice');
    }
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Str;
use Session;
use DB;
use Excel;

// Models
use App\Models\Slider;

// Exports
use App\Exports\SliderExport;

// Repos
use Facades\App\Repository\SliderRepo;

class SliderController extends CoreController
{
	public $loggedInUser;
    public $data;
    public $activeMenu = 'system';
    public $activeSubmenu = 'slider';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }
    
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('slidermgmt_perpage', 10);
        $this->data['dt_page'] = Session::get('slidermgmt_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,5,6';
        $this->data['dt_ajax_url'] = route('admin.slidermgmt.getAjaxListData');
        $this->data['dt_search_colums'] = ['ftitle','fstatus'];

        $this->data['title'] = 'Slider Management';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Slider Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Sliders',
            ],
        ];

        return parent::adminView('slidermanagement.index', $this->data);
    }

    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'title',
            2 => 'image_path',
            3 => 'status', 
            4 => 'start_date',
            5 => 'end_date'        
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('slider', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];
         
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Slider::whereIn('id', $request->input('id'))->delete();
                DB::commit();
                
                // flush the cache
                SliderRepo::flush();            

                $records["customActionMessage"] = 'Selected Sliders are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'title' => (!is_null($request->ftitle))?$request->ftitle:null,
            'status' => (!is_null($request->fstatus))?$request->fstatus:null
        ];
        $sliders = SliderRepo::allPaginated($orderColumn, $orderDir, $criteria, $iPage);

        $iTotalRecords = $sliders->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($sliders as $slider) {            
            $status = $statusList[$slider->status];
            
            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$slider->id.'"/><span></span></label>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$slider->title.'</div>',
                '<img src="'.route('ajax.previewImage', ['image'=>$slider->image_path,'type'=>'slider']).'" class="categorybanner-thumbnail-img" alt="'.$slider->title.'" height="125" width="200"/>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$slider->slider_url.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$slider->display_order.'</div>',
                ($slider->view == '0') ? 'Desktop' : 'Mobile',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.date('d-m-Y H:i',strtotime($slider->start_date)).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.date('d-m-Y H:i',strtotime($slider->end_date)).'</div>',
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">
                    <div class="">
                        <a href="'.route('admin.slidermgmt.edit', ['id'=>$slider->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span>
                        </a>
                        <a href="javascript:;" del-url="'.route('admin.slidermgmt.delete', ['id'=>$slider->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><span aria-hidden="true" class="icon-trash"></span>
                        </a>
                    </div>
                </div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    public function create(Request $request)
    {
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                "title"=>"required|regex:/^(?!\d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 @&$]*)?$/",
                "image"=>"required",
                "display_order"=>"required",
                "status"=>"required",
                "view"=>"required",
                "slider_url"=>"required|url",
                "start_date"=>"required",
                "end_date"=>"required|after:start_date",                 
            ], []);

            if (!empty($request->display_order)) {
                $isExist = Slider::where('display_order', $request->display_order)
                                ->where('view',$request->view)
                                ->whereNull('deleted_at')
                                ->whereRaw('end_date > NOW()')->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('display_order', 'Display order already exist!');
                    });
                }
            }
//parent product count, variant product count
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
// dd($request->slider_url);
            DB::beginTransaction();

            try {
                $tblData = new Slider();
                $tblData->title = $request->title;
                $tblData->image_path = $request->image;
                $tblData->slider_url = $request->slider_url;
                $tblData->display_order = $request->display_order;
                $tblData->status = $request->status;
                $tblData->view = $request->view;
                $tblData->start_date = date('Y-m-d H:i:s',strtotime($request->start_date));
                $tblData->end_date = date('Y-m-d H:i:s',strtotime($request->end_date));
                $tblData->updated_at = date('Y-m-d H:i:s');

                if ($tblData->save()) {
                    DB::commit(); 
                    
                    // flush the cache
                    SliderRepo::flush();
 
                    return redirect()
                            ->route('admin.slidermgmt')
                            ->with(['toast'=>'1','status'=>'success','title'=>'Slider','message'=>'Success! Slider Added successfully.']);
                } else {
                    DB::rollback();
                    return redirect()
                            ->route('admin.slidermgmt')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Slider','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                DB::rollback();
                return back();
            }
        }

        $this->data['title'] = 'Add Slider';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.slidermgmt',
                'title' => 'Slider Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Add Slider',
            ],
        ];
        return parent::adminView('slidermanagement.addEdit', $this->data);
    }

    public function edit(Request $request, $id)
    {
        $slider = SliderRepo::get($id);
        
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                
                "title"=>"required|regex:/^(?!\d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 @&$]*)?$/",
                "image"=>"required",
                "display_order"=>"required",
                "slider_url"=>"required|url",
                "start_date"=>"required",
                "end_date"=>"required",                 
               
            ], []);
 
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            
            DB::beginTransaction();

            try {
                $tblData = [
                    'title' => $request->title,
                    'display_order' => $request->display_order,
                    'image_path' => $request->image,
                    'slider_url' => $request->slider_url,
                    'status' => $request->status,
                    'start_date' => date('Y-m-d H:i:s',strtotime($request->start_date)),
                    'end_date' => date('Y-m-d H:i:s',strtotime($request->end_date)),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];

                $update = Slider::where('id', $id)->update($tblData);

                if ($update) {
                    DB::commit(); 
                    
                    // flush the cache
                    SliderRepo::flush();

                    if ($request->save == 'save') {
                        return redirect()
                                ->route('admin.slidermgmt')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Slider','message'=>'Success! Slider data updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Slider','message'=>'Success! Slider data updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.slidermgmt')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Categories','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                DB::rollback();
                return back();
            }
        }

        $this->data['id'] = $id;
        $this->data['slider'] = $slider;

        $this->data['title'] = 'Edit Slider';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Slider Management',
            ],
            (object) [
                'url' => 'admin.slidermgmt',
                'title' => 'Slider',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['slider']->title,
            ],
        ];
        return parent::adminView('slidermanagement.addEdit', $this->data);
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $delete = Slider::where('id', $id)->delete();
             DB::commit(); 
                    
             // flush the cache
             SliderRepo::flush();

            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }

    public function export(Request $request)
    {
        $criteria = (object)[
            'title' => ($request->title)?:null,
            'status' => ($request->status)?:null
        ];
        $sliders = SliderRepo::allExport($criteria);
        return Excel::download(new SliderExport($sliders), 'sliders.xlsx');
    }
}

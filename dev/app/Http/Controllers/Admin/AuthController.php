<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Redirect;
use Validator;
use Auth;
use Hash;
use Session;
use Avatar;

// Models
use App\Models\User;

class AuthController extends CoreController
{
	public $loggedInUser;

    public function __construct() {
        parent::__construct();

        $this->middleware('guest')->except('logout');

        // get current logged in user
        $this->loggedInUser = auth()->user();
    }

    protected function credentials(Request $request)
    {
        if(is_numeric($request->email))
            return ['mobile'=>$request->email,'password'=>$request->password];
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL))
            return ['email' => $request->email, 'password'=>$request->password];
    }

    /**
     * [login description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function login(Request $request)
    {
        // Avatar::create('Super Admin')->save('uploads'.DIRECTORY_SEPARATOR.md5(uniqid().uniqid()).'.jpg', 100); die;
        /*$user = new User();
        $user->name = 'Super Admin';
        $user->mobile = '1234567890';
        $user->email = 'admin@admin.com';
        $user->password = Hash::make('admin@123');
        $user->save();

        $user->assignRole('Super Admin');

        dd($user);*/

        if(Auth::check()) Auth::logout();

        if($request->doSubmit) {

            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'password'  => 'required',
            ],
            [
				'email.required' =>'Please enter your email',
				'password.required'=> 'Please enter your password'
			]);

			if ($validator->fails()) {
				return Redirect::back()
							->withErrors($validator)
							->withInput();
            }

            $credentials = $this->credentials($request);

            if(!Auth::attempt($credentials, $request->remember))
                    return Redirect::back()
                            ->with(['toast'=>'1','status'=>'error','title'=>'Account Login','message'=>'Invalid Credentials!']);

            $user = Auth::user();

            if($user->status != 1) {
                Auth::logout();
                return Redirect::back()
                            ->with(['toast'=>'1','status'=>'error','title'=>'Account Login','message'=>'Your user account is not active, please contact with site administrator.']);
            }

            Session::flash('wlcm_msg', '1');
            return Redirect::intended('admin/dashboard');
        }

        $data = [
            'title' => 'Login'
        ];

        return parent::adminView('auth.login', $data);
    }

    /**
     * [register description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function register(Request $request)
    {
        // Avatar::create('Super Admin')->save('uploads'.DIRECTORY_SEPARATOR.md5(uniqid().uniqid()).'.jpg', 100); die;
        /*$user = new User();
        $user->name = 'Super Admin';
        $user->mobile = '1234567890';
        $user->email = 'admin@admin.com';
        $user->password = Hash::make('admin@123');
        $user->save();

        $user->assignRole('Super Admin');

        dd($user);*/

        // $user = new User();
        // $user->name = 'Super Admin';
        // $user->mobile = '1234567890';
        // $user->email = 'aakashr@fabpik.in';
        // $user->password = Hash::make('Faizal@2611');
        // $user->save();

        // $user->assignRole('Super Admin');

        // dd($user);

    }
}

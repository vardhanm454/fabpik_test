<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;
use Mail;
use PDF;
use Msg91;
use Carbon\Carbon;
use Razorpay\Api\Api;

//Mail
use App\Mail\RefundInitiateUserEmail;

// Models
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\OrderHistory;
use App\Models\PaymentStatus;
use App\Models\ShippingStatus;
use App\Models\Seller;
use App\Models\ProductVariantOption;
use App\Models\Attribute;
use App\Models\VerifyOtp;
use App\Models\OrderAddressModifyLog;
use App\Models\OrderRefundLogs;
use App\Models\OrderCancelLog;
use App\Models\CommissionModel;
use App\Models\CommissionUpdateLog;

// Exports

class ChildOrderController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'orders';
    public $activeSubmenu = 'childorders';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('orders_perpage', 10);
        $this->data['dt_page'] = Session::get('orders_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,10,11,12,15';
        $this->data['dt_ajax_url'] = route('admin.childorders.getAjaxListData');
        $this->data['dt_search_colums'] = ['ffromdate', 'ftodate', 'fostatus', 'fseller', 'fcustomeremail', 'fmodel', 'fparentid', 'fchildid', 'fcustomername', 'fcustomermobile', 'fproductsku', 'fproductname'];

        $this->data['title'] = 'Child Orders';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Child Orders',
            ]
        ];

        $this->data['sellers'] = Seller::get();
        $this->data['orderStatus'] = OrderStatus::get();

        return parent::adminView('childorders.index', $this->data);
    }

    /**
     * [get all Parent Orders and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'created_at',
            2 => 'id',
            3 => 'child_order_id',
            4 => 'child_order_id',
            5 => 'child_order_id',
            6 => 'child_order_id',
            7 => 'quantity',
            8 => 'total',
            9 => 'order_status_id',
            10 => 'id',
            11 => 'order_status_id',
            12 => 'order_status_id',
            13 => 'order_status_id',
            14 => 'id'
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('order', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                OrderDetail::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Orders are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        if ($request->input('customActionName') == "changestatus") {
            // DB::beginTransaction();
            try {
                $statusResult = json_decode(__changeStatus($request->input('status'), $request->input('id'), $request->input('remark')));

                // if($statusResult->errors == true){
                if($statusResult->status != 1){
                    // DB::rollback();
                    $records["customActionStatus"] = "NOT-OK";
                    $records["customActionMessage"] = $statusResult->message;
                }else {
                    // DB::commit();

                    $records["customActionStatus"] = "OK";
                    $records["customActionMessage"] = $statusResult->message;
                }

            } catch (Exception $e) {
                // DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        if ($request->input('customActionName') == "changepaymentstatus") {
            DB::beginTransaction();
            try {
                __changePaymentStatus($request->input('status'), $request->input('id'));

                DB::commit();

                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Payment Status Changed successfully.';

            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        if ($request->input('customActionName') == "refundInitiate") {
            DB::beginTransaction();
            try {

                $paymentDetails = DB::table('payments')
                                    ->join('orders','orders.parent_order_id','payments.order_id')
                                    ->join('order_details','order_details.order_id','orders.id')
                                    ->where('orders.id',$request->input('id'))
                                    ->where('order_details.child_order_id',$request->child_order_id)
                                    ->select('payments.razorpay_payment_id','order_details.total')
                                    ->get();

                // dd($paymentDetails);

                \Log::info($paymentDetails);

                // $api = new Api(RAZORPAY_KEY_ID, RAZORPAY_KEY_SECRET);

                // foreach ($paymentDetails as $paymentDetail) {
                //     // dd($paymentDetail->razorpay_payment_id);
                //     $totalAmount = ($paymentDetail->total)*100;
                //     $payment = $api->payment->fetch($paymentDetail->razorpay_payment_id);
                //     $refund = $payment->refund(array('amount' => $totalAmount));

                //     $tblData = new OrderRefundLogs();
                //     $tblData->child_order_id = $request->child_order_id;
                //     $tblData->amount = $paymentDetail->total;
                //     $tblData->reason = $request->refund_initiate_reason;
                //     $tblData->created_by = $this->loggedInUser->id;
                //     $tblData->created_at = date('Y-m-d H:i:s');

                // Mail::to($request->refund_useremail)->send(new RefundInitiateUserEmail($customer_details));

                // }
                DB::commit();

                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Payment Status Changed successfully.';

            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'fostatus' => (!is_null($request->fostatus))?$request->fostatus:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fmodel' => (!is_null($request->fmodel))?$request->fmodel:null,
            'fparentid' => (!is_null($request->fparentid))?$request->fparentid:null,
            'fchildid' => (!is_null($request->fchildid))?$request->fchildid:null,
            'fcustomername' => (!is_null($request->fcustomername))?$request->fcustomername:null,
            'fcustomermobile' => (!is_null($request->fcustomermobile))?$request->fcustomermobile:null,
            'fproductsku' => (!is_null($request->fproductsku))?$request->fproductsku:null,
            // 'fcustomeremail' => (!is_null($request->fcustomeremail))?$request->fcustomeremail:null,
            'fproductname' => (!is_null($request->fproductname))?$request->fproductname:null,
        ];
        $orders = OrderDetail::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $orders->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $featuredList = [
            ["danger" => "No"],
            ["primary" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["primary" => "Active"]
        ];

        foreach ($orders as $index=>$order) {
            $changeAddress = '';
            $cancelOrder = '';
            $initiateRefund = '';
            if ($order->order_status_id == 1 || $order->order_status_id == 4 ) {

                //change adderess
                $changeAddress = '<button type="button" class="btn btn-icon-only default btn-circle" title="Modify Address" data-toggle="modal" data-action="modify-address" data-target="#modifyAddressModal" data-order_id="'.$order->order->id.'" data-child_order_id="'.$order->id.'" data-shippingaddressone="'.$order->order->shipping_address1.'" data-shipping_name="'.$order->order->shipping_first_name.'" data-shipping_mobile="'.$order->order->shipping_mobile.'" data-shipping_email="'.$order->order->shipping_email.'" data-current_order_status="'.$order->order_status_id.'" data-shippingaddresstwo="'.$order->order->shipping_address2.'" data-shippingcity="'.$order->order->shipping_city.'" data-shippingstate="'.$order->order->shipping_state.'" data-shippingstateid="'.$order->order->billing_state_id.'" data-shippingpincode="'.$order->order->shipping_pincode.'" data-usermobile="'.$order->order->mobile.'" data-useremail="'.$order->order->email.'" data-keyboard="false" data-backdrop="static">
                                    <span aria-hidden="true" class="fa-address-book"><b>A</b></span>
                                    </button>';
                                
                //cancel order
                $cancelOrder = '<button type="button" class="btn btn-icon-only default btn-circle" title="Cancel Order" data-toggle="modal" data-action="cancel-order" data-target="#cancelOrderModal" data-order_id="'.$order->order->id.'" data-child_order_id="'.$order->child_order_id.'" data-usermobile="'.$order->order->mobile.'" data-useremail="'.$order->order->email.'" data-keyboard="false" data-backdrop="static">
                                    <span class="fa fa-times"></span>
                                </button>';
            }
            //intiate refund
            if (($order->order_status_id == 2 || $order->order_status_id == 7 || $order->order_status_id == 11 || $order->order_status_id == 12) && ($order->order->payment_status_id == 1) && ($order->order->payment_type=='o') && is_null($order->deal_price) ) {                                
               
                $initiateRefund = '<button type="button" class="btn btn-icon-only default btn-circle" title="Refund Initiate" data-toggle="modal" data-action="refund-initiate" data-target="#refundInitiateModal" data-order_id="'.$order->order->id.'" data-child_order_id="'.$order->child_order_id.'" data-usermobile="'.$order->order->mobile.'" data-useremail="'.$order->order->email.'" data-userpaymenttype="'.$order->order->payment_type.'" data-keyboard="false" data-backdrop="static">
                                    <span class="fa fa-money"></span>
                                </button>';
            }

            //Commission Details
            $commissionName = CommissionModel::getCommission($order->seller->commission_id);
            $shipping_model = ( ($order->shipping_model == 'f') ? 'FabShip' : 'Self Shipping' );
            $shipping_weight = $order->shipping_weight;

            $commissionDetails = '<button type="button" class="btn btn-icon-only default btn-circle" title="Commission Details" data-toggle="modal" data-action="commissio-details" data-target="#commissionDetailsModal" data-payout_status="'.$order->payout_status.'" data-child_order_id="'.$order->id.'" data-commission_value="'.$order->commission.'" data-shipping_charges="'.$order->shipping_charge.'" data-handling_charges="'.$order->order_handling_charge.'" data-commission_name="'.$commissionName->name.'" data-commission_type="'.(($commissionName->commission_type == 'f') ? 'Fixed' : 'Variable').'" data-order_shipping_model="'.$shipping_model.'" data-order_weight="'.$shipping_weight.'" data-order_mrp="'.$order->mrp.'" data-order_price="'.$order->price.'" data-order_quantity="'.$order->quantity.'" data-order_volume="'.($order->shipping_length.'*'.$order->shipping_breadth.'*'.$order->shipping_height).'" data-keyboard="false" data-backdrop="static">
                                        <span class=""><b>%</b></span>
                                    </button>';

            $actionBtns = ($canChange)?'<div class="">
                                            <a href="'.route('admin.childorders.view', ['id'=>$order->id]).'" class="btn btn-icon-only default btn-circle" title="View Order Details">
                                                <span aria-hidden="true" class="icon-eye"></span>
                                            </a>
                                            <!-- <a href="'.route('admin.childorders.custInvoice', ['id'=>$order->id]).'" target="_blank" title="Download Invoice" class="btn btn-icon-only default btn-circle">
                                                <span aria-hidden="true" class="icon-cloud-download"></span>
                                            </a> -->
                                            '.$commissionDetails.'
                                            '.$changeAddress.'
                                            '.$initiateRefund.'
                                            '.$cancelOrder.'
                                        </div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$order->id.'"/><span></span></label>',
                date('d-m-Y H:i A', strtotime($order->created_at)),
                $order->order->parent_order_id,
                '<a href="'.route('admin.childorders.view', ['id'=>$order->id]).'" class="" title="View Order Details">'.$order->child_order_id.'</a>',
                $order->seller->seller_code,
                $order->seller->name,
                $order->order->first_name.' '.$order->order->last_name,
                $order->order->mobile,
                ($order->productVarient) ? '<a href="https://fabpik.in/products/'.$order->productVarient->slug.'/'.$order->productVarient->unique_id.'" target="_blank" class="" title="View Product Details">'.$order->productVarient->name.'</a>' : '',
                ($order->productVarient) ? '<a href="https://fabpik.in/products/'.$order->productVarient->slug.'/'.$order->productVarient->unique_id.'" target="_blank" class="" title="View Product Details">'.$order->productVarient->sku.'</a>' : '',
                $order->quantity,
                'Amount: '.$order->total.'<br>'.'Payment Type: '.(($order->order->payment_type=='c')? 'COD': 'Online'),
                OrderDetail::getOrderStatusData($order->order_status_id)->admin_text,
                OrderDetail::getPaymentStatusData($order->payment_status_id)->admin_text,
                OrderDetail::getShippingData($order->shipping_status_id)->admin_text,
                !is_null($order->user_customization_text) ? $order->user_customization_text : '-',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [View Parent Products]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function view($id)
    {
        $this->data['title'] = 'Order Details';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.childorders',
                'title' => 'Orders',
            ],
            (object) [
                'url' => 'javascript:;',
                'title' => 'Order Details',
            ]
        ];

        $this->data['id'] = $id;
        $this->data['orderDetails'] = OrderDetail::with('order','seller', 'productVarient', 'orderHistory')->where('id', $id)->first();
        $this->data['orderHistory'] = orderHistory::where('order_detail_id', $id)->orderBy('id','desc')->get();

        $productVarientOptions = ProductVariantOption::where('product_variant_id', $this->data['orderDetails']->productVarient->id)->get();
        $attributes = [];
        foreach ($productVarientOptions as $i=>$options) {
            $getAttributes = Attribute::with(['attributeOptions' => function($q) use ($options){
                    $q->where( 'id', '=', $options->attribute_option_id );
            }])->where('id', $options->attribute_id)->first();
            $attributes[$i]['atrributeName'] = isset($getAttributes->display_name) ? $getAttributes->display_name : '-';
            $attributes[$i]['atrributeValue'] = isset($getAttributes->attributeOptions[0]) ? $getAttributes->attributeOptions[0]->option_name : '-';
        }
        $this->data['attributeOptions'] = $attributes;

        $this->data['paymentStatus'] = PaymentStatus::get();
        $this->data['orderStatus'] = OrderStatus::get();
        $this->data['shippingStatus'] = ShippingStatus::get();

        return parent::adminView('childorders.childOrderDetails', $this->data);
    }

    /**
     * [View Invoice Products]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function custInvoice($id)
    {
        $this->data['title'] = 'Customer Invoice';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.childorders',
                'title' => 'Child Orders',
            ],
            (object) [
                'url' => 'javascript:;',
                'title' => 'Customer Invoice',
            ]
        ];
        // $this->data['orderDetails'] = OrderDetail::with('seller', 'product')->where('id', $id)->first();
        // $this->data['orders'] = Order::where('id', $this->data['orderDetails']->order_id)->first();
        $this->data['orderDetails'] = OrderDetail::with('order','seller', 'productVarient', 'orderHistory')->where('id', $id)->first();

        $productVarientOptions = ProductVariantOption::where('product_variant_id', $this->data['orderDetails']->productVarient->id)->get();
        $attributes = [];
        foreach ($productVarientOptions as $i=>$options) {
            $getAttributes = Attribute::with(['attributeOptions' => function($q) use ($options){
                    $q->where( 'id', '=', $options->attribute_option_id );
            }])->where('id', $options->attribute_id)->first();
            $attributes[$i]['atrributeName'] = $getAttributes->display_name;
            $attributes[$i]['atrributeValue'] = $getAttributes->attributeOptions[0]->option_name;
        }
        $this->data['attributeOptions'] = $attributes;

        return parent::adminView('childorders.customerinvoice', $this->data);
    }

    /**
     * [Download Invoice Products]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function downloadCustomerInvoice($id)
    {
        // __downloadCustomerInvoice($id);
        $data['orderDetails'] = OrderDetail::with('order', 'seller', 'productVarient', 'orderHistory')->where('id', $id)->first();
        $productVarientOptions = ProductVariantOption::where('product_variant_id', $data['orderDetails']->productVarient->id)->get();
        $attributes = [];
        foreach ($productVarientOptions as $i=>$options) {
            $getAttributes = Attribute::with(['attributeOptions' => function ($q) use ($options) {
                $q->where('id', '=', $options->attribute_option_id);
            }])->where('id', $options->attribute_id)->first();

            $attributes[$i]['atrributeName'] = $getAttributes->display_name;
            $attributes[$i]['atrributeValue'] = $getAttributes->attributeOptions[0]->option_name;
        }
        $data['attributeOptions'] = $attributes;

        // $pdf = PDF::loadView('customer.orders.customerinvoice', $data);
        $pdf = PDF::loadView('invoices.customerinvoice', $data);
        return $pdf->download($data['orderDetails']->child_order_id.'.pdf');
    }


    /**
     * [Update Shipping Address ]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function sendVerificationCodes(Request $request)
    {
        try {

            // $verificationcode = "1234";
            $verificationcode = __generateOtp();
            $this->saveOTP('mobile', $request->usermobile, $verificationcode);
            $this->saveOTP('email', $request->useremail, $verificationcode);

            // Send Email for Verification to User
            $to = [
                'user_email'=>$request->useremail,
            ];
            $details = [
                'otp' => $verificationcode
            ];

            if ($request->request_type == 'modify_order_address') {
                $userAddressVerificationEmail = __sendEmails($to, 'change_address_verify_mobile', $details);
                // $userAddressVerificationEmail = true;
                __sendSms($request->usermobile,'verify_mobile', ['otp'=>$verificationcode]);

                if ($userAddressVerificationEmail) {
                    return response()->json(['success'=>true, 'otp'=>$verificationcode, 'message'=>"Success! Verification Code Sent.", 'error'=>false], 200);
                } else {
                    return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
                }
            }elseif($request->request_type == 'cancel_order'){
                // dd($to);
                $userOrderCancelEmail = __sendEmails($to, 'cancel_order_verify_mobile', $details);

                __sendSms($request->usermobile,'verify_mobile', ['otp'=>$verificationcode]);
                // dd($userOrderCancelEmail);
                if ($userOrderCancelEmail) {
                    return response()->json(['success'=>true, 'message'=>"Success! Verification Code Sent.", 'error'=>false], 200);
                } else {
                    return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
                }
            }

        } catch (Exception $e){
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
        }
    }

    /**
     * [Update Shipping Address ]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function UpdateShippingAddress(Request $request, $id)
    {
        try {
            $otp = $request->verify_otp;
            $otpData = VerifyOtp::where('mobile', $request->usermobile)->first();

            /**
             * Verify OTP code place here
             */ 
            
            if ($otp == $otpData->otp) {

                $result = '';
                if($request->current_order_status == 4){
                    $orderDetails = OrderDetail::where('id', $request->child_order_id)->first();
                   
                    if (isset($orderDetails->shipment_id) && isset($orderDetails->shiprocket_order_id) && isset($orderDetails->child_order_id)) {
                        $postparams = [
                            'order_id' => $orderDetails->shiprocket_order_id,
                            'shipping_customer_name' => $request->shipping_name,
                            'shipping_phone' => $request->shipping_mobile,
                            'shipping_address' => $request->shipping_address,
                            'shipping_address_2' => $request->shipping_address_2,
                            'shipping_city' => $request->shipping_city,
                            'shipping_state' => $request->shipping_state,
                            'shipping_country' => 'India',
                            'shipping_pincode' => $request->shipping_pincode,
                        ];

                        $result = __modifyOrderShippingAddress($postparams);
                        
                        if( isset($result['errors']) ){
                            return response()->json(['success'=>false, 'message'=>'Oops! Invalid Data.', 'err'=>true, 'errors'=>$result['errors']], 200);
                        }
                    }
                }

                $updatedAddressData = [
                    'shipping_address1' => $request->shipping_address,
                    'shipping_address2' => $request->shipping_address_2,
                    'shipping_city' => $request->shipping_city,
                    'shipping_state' => $request->shipping_state,
                    'shipping_state_id' => $request->shipping_state_id,
                    'shipping_pincode' => $request->shipping_pincode,
                    'billing_address1' => $request->shipping_address,
                    'billing_address2' => $request->shipping_address_2,
                    'billing_city' => $request->shipping_city,
                    'billing_state' => $request->shipping_state,
                    'billing_state_id' => $request->shipping_state_id,
                    'billing_pincode' => $request->shipping_pincode,
                ];

                $updateAddress = Order::where('id', $id)->update($updatedAddressData);
                
                /**
                 * Update Log
                 */

                $orderAddressModifyLog = new OrderAddressModifyLog();
                $orderAddressModifyLog->order_id = $id;
                $orderAddressModifyLog->order_detail_id = $request->child_order_id;
                $orderAddressModifyLog->old_shipping_address1 = $request->shipping_address_hidden;
                $orderAddressModifyLog->old_shipping_address2 = $request->shipping_address_2_hidden;
                $orderAddressModifyLog->old_shipping_city = $request->shipping_city_hidden;
                $orderAddressModifyLog->old_shipping_state = $request->shipping_state_hidden;
                $orderAddressModifyLog->old_shipping_state_id = $request->shipping_state_id_hidden;
                $orderAddressModifyLog->old_shipping_pincode = $request->shipping_pincode_hidden;
                $orderAddressModifyLog->current_shipping_address1 = $request->shipping_address;
                $orderAddressModifyLog->current_shipping_address2 = $request->shipping_address_2;
                $orderAddressModifyLog->current_shipping_city = $request->shipping_city;
                $orderAddressModifyLog->current_shipping_state = $request->shipping_state;
                $orderAddressModifyLog->current_shipping_state_id = $request->shipping_state_id;
                $orderAddressModifyLog->current_shipping_pincode = $request->shipping_pincode;
                $orderAddressModifyLog->created_by = $this->loggedInUser->id;
                $orderAddressModifyLog->save();

                /**
                 * send mail once Address is updated.
                 * 
                 * Send Email for Verification to User.
                 * 
                 */

                $to = [
                    'user_email'=>$request->useremail,
                ];
                $userAddressModificationEmail = __sendEmails($to, 'change_address_modify', $orderAddressModifyLog);

                return response()->json(['success'=>true, 'message'=>"Success! Address modified sucessfully .", 'err'=>false, 'errors'=>[]], 200);
            } else {
                return response()->json(['success'=>false, 'message'=>"Verification code mismatch, please check again.", 'err'=>true, 'errors'=>[]], 200);
            }
        } catch (Exception $e){
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'err'=>true, 'errors'=>[]], 200);
        }
    }

    /**
     * [Cancel the order ]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function cancelOrder(Request $request, $id)
    {
        try {
            $otp = $request->verify_otp;
            $otpData = VerifyOtp::where('mobile', $request->usermobile)->first();

            /**
             * Verify OTP code place here
             */ 
            
            if ($otp == $otpData->otp) {

                $result = '';

                $updatedCancelledData = [
                    'cancellation_reason' => $request->order_cancel_reason,
                    'cancelled_by' => $this->loggedInUser->id,
                    'cancelled_at' => Carbon::now(),
                    'order_status_id' => 2
                ];

                $cancelOrder = OrderDetail::where('child_order_id', $request->child_order_id)->update($updatedCancelledData);
                // dd($cancelOrder);
                //Store Statutes in Order History Table
                $data = [
                    'order_status_id' => 2,
                    'order_detail_id' => $request->child_order_id,
                    'order_id' => $id,
                ];  
                OrderHistory::insert($data);

                /**
                 * Update Log
                 */

                $orderCancelLog = new OrderCancelLog();
                $orderCancelLog->child_order_id = $request->child_order_id;
                $orderCancelLog->reason = $request->order_cancel_reason;
                $orderCancelLog->created_by = $this->loggedInUser->id;
                $orderCancelLog->save();

                /**
                 * send mail once order is cancelled.
                 * 
                 * Send Email for Verification to User.
                 * 
                 */

                $to = [
                    'user_email'=>$request->useremail,
                ];
                // dd($request->child_order_id);
                $userOrderCancelEmail = __sendEmails($to, 'cancel_order', $orderCancelLog);

                return response()->json(['success'=>true, 'message'=>"Success! Order Cancelled sucessfully .", 'err'=>false, 'errors'=>[]], 200);
            } else {
                return response()->json(['success'=>false, 'message'=>"Verification code mismatch, please check again.", 'err'=>true, 'errors'=>[]], 200);
            }
        } catch (Exception $e){
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'err'=>true, 'errors'=>[]], 200);
        }
    }
    
    /**
     * [Refund Initiate ]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function refundInitiate(Request $request, $id)
    {
        try {
            $paymentDetails = DB::table('payments')
                                ->join('orders','orders.parent_order_id','payments.order_id')
                                ->join('order_details','order_details.order_id','orders.id')
                                ->where('orders.id',$id)
                                ->where('order_details.child_order_id',$request->child_order_id)
                                ->select('payments.razorpay_payment_id','order_details.total')
                                ->get();


                \Log::info($paymentDetails);

                $api = new Api(RAZORPAY_KEY_ID, RAZORPAY_KEY_SECRET);

                foreach ($paymentDetails as $paymentDetail) {
                // dd($paymentDetail->total);
                    // dd($paymentDetail->razorpay_payment_id);
                    $totalAmount = ($paymentDetail->total);
                    $payment = $api->payment->fetch($paymentDetail->razorpay_payment_id);
                    $refund = $payment->refund(array('amount' => $totalAmount));

                    $data = new OrderRefundLogs();
                    $data->child_order_id = $request->child_order_id;
                    $data->amount = $paymentDetail->total;
                    $data->reason = $request->refund_initiate_reason;
                    $data->created_by = $this->loggedInUser->id;
                    $data->created_at = date('Y-m-d H:i:s');
                    $data->save();

                    Order::where("id",$id)
                               ->update(["payment_status_id"=>3]);

                    Mail::to($request->useremail)->send(new RefundInitiateUserEmail($data));

                }
                DB::commit();
                return response()->json(['success'=>true, 'message'=>"Success! Refund has been initiated successfully.", 'err'=>false, 'errors'=>[]], 200);


        } catch (Exception $e){
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'err'=>true, 'errors'=>[]], 200);
        }
    }

    /**
     * [Modify Commission Details]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function modifyCommission(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $getCommissionDetails = OrderDetail::where('id', $id)->first();

            $tblUpdatedValues = [
                'commission' => number_format((float)$request->commission_value, '2'),
                'commission_sgst' => number_format((float)$request->commission_value * (COMMISION_SGST/100), '2'),
                'commission_cgst' => number_format((float)$request->commission_value * (COMMISION_CGST/100), '2'),
                'shipping_charge' => number_format((float)$request->shipping_charges, '2'),
                'shipping_charge_sgst' => number_format((float)$request->shipping_charges * (SHIPPING_SGST/100), '2'),
                'shipping_charge_cgst' => number_format((float)$request->shipping_charges * (SHIPPING_CGST/100), '2'),
                'order_handling_charge' => number_format((float)$request->handling_charges, '2'),
                'order_handling_charge_sgst' => number_format((float)$request->handling_charges * (ORDER_HANDLING_SGST/100), '2'),
                'order_handling_charge_cgst' => number_format((float)$request->handling_charges * (ORDER_HANDLING_CGST/100), '2'),
            ];

            $updateCommissionDetails = OrderDetail::where('id', $id)->update($tblUpdatedValues);
            if($updateCommissionDetails){
                $logUpdate = new CommissionUpdateLog();
                $logUpdate->order_detail_id = $id;

                if(  number_format((float) $getCommissionDetails->commission, '2') !== $tblUpdatedValues['commission']){
                    $logUpdate->commission = $getCommissionDetails->commission ;
                    $logUpdate->updated_commission = $tblUpdatedValues['commission'];
                    $logUpdate->commission_sgst = $getCommissionDetails->commission_sgst ;
                    $logUpdate->updated_commission_sgst = $tblUpdatedValues['commission_sgst'];
                    $logUpdate->commission_cgst = $getCommissionDetails->commission_cgst ;
                    $logUpdate->updated_commission_cgst = $tblUpdatedValues['commission_cgst'];
                }

                if(  number_format((float) $getCommissionDetails->shipping_charge, '2') !== $tblUpdatedValues['shipping_charge']){
                    $logUpdate->shipping_charge = $getCommissionDetails->shipping_charge;
                    $logUpdate->updated_shipping_charge = $tblUpdatedValues['shipping_charge'];
                    $logUpdate->shipping_charge_sgst = $getCommissionDetails->shipping_charge_sgst ;
                    $logUpdate->updated_shipping_charge_sgst = $tblUpdatedValues['shipping_charge_sgst'];
                    $logUpdate->shipping_charge_cgst = $getCommissionDetails->shipping_charge_cgst ;
                    $logUpdate->updated_shipping_charge_cgst = $tblUpdatedValues['shipping_charge_cgst'];
                }

                if(  number_format((float) $getCommissionDetails->order_handling_charge, '2') !== $tblUpdatedValues['order_handling_charge']){
                    $logUpdate->order_handling_charge = $getCommissionDetails->order_handling_charge ;
                    $logUpdate->updated_order_handling_charge = $tblUpdatedValues['order_handling_charge'];
                    $logUpdate->order_handling_charge_sgst = $getCommissionDetails->order_handling_charge_sgst ;
                    $logUpdate->updated_order_handling_charge_sgst = $tblUpdatedValues['order_handling_charge_sgst'];
                    $logUpdate->order_handling_charge_cgst = $getCommissionDetails->order_handling_charge_cgst ;
                    $logUpdate->updated_order_handling_charge_cgst = $tblUpdatedValues['order_handling_charge_cgst'];
                }
                $logUpdate->created_by = $this->loggedInUser->id;
                $logUpdate->comments = $request->comments;
                $logUpdate->save();

                DB::commit();
                return response()->json(['success'=>true, 'message'=>"Commission details updated successfully.", 'err'=>false, 'errors'=>[]], 200);
            }

            DB::rollback();

            return response()->json(['success'=>false, 'message'=>"Commission details modification error.", 'err'=>true, 'errors'=>[]], 200);
        } catch (Exception $e){
            DB::rollback();
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'err'=>true, 'errors'=>[]], 200);
        }
    }


    /**
     * Store the OTP
     */
    public function saveOTP($type = "mobile", $param, $otp)
    {
        if($type == "mobile"){
            return VerifyOtp::updateOrInsert([
                'mobile' => $param
            ],
            [
                'mobile' => $param,
                'otp' => $otp,
                'otp_expiry' => Carbon::now()->addMinutes(3)
            ]);
        } else if($type == "email"){
            return VerifyOtp::updateOrInsert([
                'email' => $param
            ],
            [
                'email' => $param,
                'otp' => $otp,
                'otp_expiry' => Carbon::now()->addMinutes(3)
            ]);
        }        
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;

class AccountController extends CoreController
{
	public $loggedInUser;

    public function __construct() {
        parent::__construct();

        // get current logged in user
        $this->loggedInUser = auth()->user();
    }

    public function logout()
    {        
        auth()->logout();

        return redirect()
                ->route('admin.login')
                ->with(['status'=>'success','title'=>'Thank You','message'=>'Thank you. You have been succesfully logged out']);
    }
}
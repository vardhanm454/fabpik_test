<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models

// Exports

class OrderController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'orders';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index()
    { 
        $this->data['title'] = 'Parent Order';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Parent Order Management',
            ]
        ];

        return parent::adminView('parentorders.index', $this->data);
    }

    /**
     * [View Parent Products]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function view($request)
    {
        $this->data['title'] = 'Order Details';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'Orders',
            ],
            (object) [
                'url' => 'admin.products',
                'title' => 'Order Details',
            ]
        ];

        // $this->data['id'] = $id;
        return parent::adminView('parentorders.parentOrderDetails', $this->data);
    }

    /**
     * [View Invoice Products]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function invoice()
    {
        return parent::adminView('parentorders.invoice');
    }
}
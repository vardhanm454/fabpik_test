<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Redirect;
use Session;
use Validator;

//Modals
use App\Models\TermsAndCondtion;

class ImportantDocsController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'importantdocs';
    public $activeSubmenu = '';

    public function __construct() {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });        
    }

    public function index()
    {

        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('documents_perpage', 10);
        $this->data['dt_page'] = Session::get('documents_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.importantdocs.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];
  
        $this->data['title'] = 'Documents List';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Documents',
            ],
        ];

        return parent::adminView('importantdocs.index', $this->data);
    }

    /**
     * [get all Terms and Condtions and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
            2 => 'updated_at'
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('seller', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                TermsAndCondtion::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Documents are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        $criteria = (object)[
            'length' => intval($request->length),
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
        ];

        $termsandcondtions = TermsAndCondtion::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $termsandcondtions->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $featuredList = [
            ["danger" => "No"],
            ["primary" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];
        $k=1;
        foreach ($termsandcondtions as $condtions) {
            $status = $statusList[$condtions->status];

            $actionBtns = ($canChange)?'
            
            <a href="'.route('admin.importantdocs.edit', ['id'=>$condtions->id]).'" title="Edit" class="btn btn-icon-only default btn-circle"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="'.route('admin.importantdocs.view', ['id'=>$condtions->id]).'" class="btn btn-icon-only default btn-circle" title="View & Verify"><span aria-hidden="true" class="icon-eye"></span> </a>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$condtions->id.'"/><span></span></label>',
                $condtions->name,
                $condtions->display_name,
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                '<div class="">'.$actionBtns.'</div>',
            ];
            $k++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [Add new Conditions]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function add(Request $request)
    {
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'display_name' => 'required|string',
                'description'=>'required',         
            ]);

            if (!empty($request->name)) {
                $isExist = TermsAndCondtion::where('name', $request->name)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Name already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
            try {
                $tblData = new TermsAndCondtion();
                $tblData->name = $request->name;
                $tblData->display_name = $request->display_name;
                $tblData->description = $request->description;

                if($tblData->save()) {
                    DB::commit();

                    return redirect()
                            ->route('admin.importantdocs')
                            ->with(['toast'=>'1','status'=>'success','title'=>'Documents','message'=>'Success! Added successfully.']);
                }
                else
                    return redirect()
                            ->route('admin.importantdocs')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Documents','message'=>'Error! Some error occured, please try again.']);
            } catch (Exception $e) {
                // dd($e);
                DB::rollback();
                return back();
            }
        }
  
        $this->data['title'] = 'Add New Document';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Documents',
            ],
        ];

        return parent::adminView('importantdocs.addEditDocs', $this->data);
    }

    /**
     * [Edit the existing Conditions]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function edit(Request $request, $id)
    {
        $condtions = TermsAndCondtion::find($id);

        if ($request->save) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'display_name' => 'required|string',
                'description'=>'required',    
            ]);

            if (!empty($request->name)) {
                $isExist = TermsAndCondtion::where('name', $request->name)->where('id', '<>', $id)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Name already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
            try {
                $tblData = new TermsAndCondtion();
                $tblData=[
                    'name' => $request->name,
                    'display_name' => $request->display_name,
                    'description' => $request->description,
                    'status' => $request->status,
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                $update = TermsAndCondtion::where('id',$id)->update($tblData);

                if($update) {
                    DB::commit();

                    if($request->save == 'save') {
                        return redirect()
                                ->route('admin.importantdocs')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Documents','message'=>'Success! Data updated successfully.']);
                    }
                    else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Documents','message'=>'Success! Data updated successfully.']);
                    }
                }
                else
                    return redirect()
                            ->route('admin.importantdocs')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Documents','message'=>'Error! Some error occured, please try again.']);
            } catch (Exception $e) {
                // dd($e);
                DB::rollback();
                return back();
            }
        }

        $this->data['condtions'] = $condtions;
        $this->data['id'] = $id;
  
        $this->data['title'] = 'Edit Document';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.importantdocs',
                'title' => 'Documents',
            ],
            (object) [
                'url' => false,
                'title' => $condtions->name,
            ],
        ];

        return parent::adminView('importantdocs.addEditDocs', $this->data);
    }

    /**
     * [View the existing Conditions]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function view(Request $request, $id)
    {
        $condtions = TermsAndCondtion::find($id);

        $this->data['condtions'] = $condtions;
        $this->data['id'] = $id;
        $this->data['type'] = 'view';
  
        $this->data['title'] = 'View Document';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.importantdocs',
                'title' => 'Documents',
            ],
            (object) [
                'url' => false,
                'title' => $condtions->name,
            ],
        ];

        return parent::adminView('importantdocs.addEditDocs', $this->data);
    }
}

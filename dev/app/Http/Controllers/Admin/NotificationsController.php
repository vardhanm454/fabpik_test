<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models
use App\Models\Notifications;
use App\Models\FCMToken;

class NotificationsController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'notifications';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Notifications listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('notifications_perpage', 10);
        $this->data['dt_page'] = Session::get('notifications_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.notifications.getAjaxListData');
        $this->data['dt_search_colums'] = ['ffromdate','ftodate','ftype'];

        $this->data['title'] = 'Notifications';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Notifications',
            ]
        ];

        return parent::adminView('notifications.index', $this->data);
    }

    /**
     * [get all Notifications for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'title',
            2 => 'content',
            3 => 'notify_on',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('notifications', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                $cur_time= date("Y-m-d H:i:s");

                Notifications::whereIn('id', $request->input('id'))
                ->where('notify_on','>',$cur_time)->delete();
                // $delete=Notifications::whereIn('id', $request->input('id'))
                // ->where('notify_on','>',$cur_time)->get();
                // dd($delete);

                DB::commit();
                $records["customActionMessage"] = 'Selected Notifications are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'ftype' => (!is_null($request->ftype))?$request->ftype:null,
        ];
        $notifications = Notifications::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $notifications->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));
        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($notifications as $notification) {
            $cur_time= date("Y-m-d H:i:s");
            $actionBtns='No Actions Available';
            $status ='Sent';
            if($notification->notify_on>$cur_time){
            $status ='To be sent';
            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.notifications.edit', ['id'=>$notification->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('admin.notifications.delete', ['id'=>$notification->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>':'';
            }
            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$notification->id.'"/><span></span></label>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$notification->title.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$notification->content.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.date("d-m-Y H:i:s",strtotime($notification->notify_on)).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.($notification->notified_to=='c')?'Customer':'Seller'.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$status.'</div>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.     
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }


    /**
     * [edit notifications data]
     * @param  Request $request [description]
     * @param  [type]  $id      [brand id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $notifications = Notifications::find($id);
        if ($request->save) {
          
            $validator = Validator::make(
                $request->all(),
                [
                    'title' => 'required|string',
                    'notify_on' => 'required',
                    'content'=>'required|string',
                ]
            );
            
            if ($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            try {

                $tblData = [
                    'title' => $request->title,
                    'notify_on' => date('Y-m-d H:i:s', strtotime($request->notify_on)),
                    'content' => $request->content,
                    'updated_by' => $this->loggedInUser->id,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];

                $update = Notifications::where('id', $id)->update($tblData);

                if ($update) {
                    if ($request->save == 'save') {
                        return redirect()
                                ->route('admin.notifications')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Notifications','message'=>'Success! Notification data updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Notifications','message'=>'Success! Notification data updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.notifications')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Notifications','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                return redirect()
                        ->route('admin.notifications')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Notifications','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['notifications'] = $notifications;
        $this->data['id'] = $id;
        
        $this->data['title'] = 'Edit Notification';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.notifications',
                'title' => 'Notifications',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['notifications']->code,
            ],
        ];

        $this->data['editnotifications'] = $notifications;
        $this->data['id'] = $id;

        return parent::adminView('notifications.edit', $this->data);
    }

    /**
     * [Delete Single notifications data]
     * @param  Request $request [description]
     * @param  [type]  $id      [coupon id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            $delete = Notifications::where('id', $id)->delete();
            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }

    /**
     *send notifications to sellers using the firebase
     *
     * @return response()
     */
    public function sendNotification(Request $request)
    {
        $firebaseToken = FCMToken::whereNotNull('user_id')->pluck('fcm_token')->all();
          
        $SERVER_API_KEY = FIREBASE_FCM_SERVER_KEY;
  
        $data = [
            "registration_ids" => $firebaseToken,
            "notification" => [
                // "title" => $request->title,
                // "body" => $request->body,
                "title" => 'title',
                "body" => 'body',
            ]
        ];
        
        $dataString = json_encode($data);
    
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];
    
        $ch = curl_init();
      
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
               
        $response = curl_exec($ch);
  
        // dd($response);
    }
}

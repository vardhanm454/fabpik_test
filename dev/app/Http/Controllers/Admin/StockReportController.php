<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Seller;

// Exports
use App\Exports\LowStockExport;
use App\Exports\ModifiedStockExport;
use App\Exports\SellerProductStockExport;
use App\Exports\ExpiredSpecialPricesExport;

class StockReportController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'stockreport';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function sellerproductstock()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('products_perpage', 10);
        $this->data['dt_page'] = Session::get('products_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3';
        $this->data['dt_ajax_url'] = route('admin.stockreport.getSellerProductStockAjaxListData');
        $this->data['dt_search_colums'] = ['fseller','fcode'];

        $this->data['title'] = 'Seller Product Report';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = 'sellerproductstock';
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Stock Report',
            ],
            (object) [
                'url' => false,
                'title' => 'Seller Product Report',
            ],
        ];

        //get all sellers
        $sellers = Seller::get();
        $this->data['sellers'] = $sellers;
        $this->data['brands'] = Brand::get();
        $this->data['categoryList'] =  Category::where('status', 1)->get();

        return parent::adminView('stockreport.sellerproductstock', $this->data);
    }

    /**
     * [get all Products and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getSellerProductStockAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('Product', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fcode' => (!is_null($request->fcode))?$request->fcode:null,

        ];
        $prodVariants = ProductVariant::getSellerProductStockAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        
        $iTotalRecords = $prodVariants->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];
        $index = 0;
        foreach ($prodVariants as $prodVariant) {

            $productCount = DB::table('products')->where('seller_id',$prodVariant->id)->whereNull('deleted_at')->count();
            
            $VariantproductCount = DB::table('product_variants as t1')->join('products as t2','t2.id','t1.product_id')->whereNull('t1.deleted_at')->where('t2.seller_id',$prodVariant->id)->count();
            
            $records["data"][] = [
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->seller_code.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->name.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$productCount.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$VariantproductCount.'</div>',                
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }
    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function lowstock()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('products_perpage', 10);
        $this->data['dt_page'] = Session::get('products_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '3,4';
        $this->data['dt_ajax_url'] = route('admin.stockreport.getLowStockReportAjaxListData');
        $this->data['dt_search_colums'] = ['fid','fpid','fsku','fseller','pname','sfrom','fcategory','sto','fbrand'];

        $this->data['title'] = 'Low Stock Report';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = 'lowstock';
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Stock Report',
            ],
            (object) [
                'url' => false,
                'title' => 'Low Stock Report',
            ],
        ];

        //get all sellers
        $sellers = Seller::get();
        $this->data['sellers'] = $sellers;
        $this->data['brands'] = Brand::get();
        $this->data['categoryLists'] =  Category::where('status', 1)->get();

        return parent::adminView('stockreport.lowstock', $this->data);
    }

    /**
     * [get all Products and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getLowStockReportAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'pv_unique_id',
            1 => 'sku',
            3 => 'name',
            4 => 'stock',

        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('Product', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'seller' => $this->loggedInUser->ref_id,
            'fid' => (!is_null($request->fid))?$request->fid:null,
            'fpid' => (!is_null($request->fpid))?$request->fpid:null,
            'fsku' => (!is_null($request->fsku))?$request->fsku:null,
            'pname' => (!is_null($request->pname))?$request->pname:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fcategory' => (!is_null($request->fcategory))?$request->fcategory:null,
            'fbrand' => (!is_null($request->fbrand))?$request->fbrand:null,
        ];
        // DB::enableQueryLog();
        $prodVariants = ProductVariant::getLowStockReportAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        // dd(DB::getQueryLog());
        $iTotalRecords = $prodVariants->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin') || $this->loggedInUser->hasRole('Seller'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];
        foreach ($prodVariants as $index=>$prodVariant) {
            $records["data"][] = [
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->pv_unique_id.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->p_unique_id.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->sku.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->name.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->stock.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.'Seller Name: '.$prodVariant->s_name.'<br>Seller Id:'.$prodVariant->seller_code.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->c_name.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->brand_name.'</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function modifiedstock()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('products_perpage', 10);
        $this->data['dt_page'] = Session::get('products_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '3,4';
        $this->data['dt_ajax_url'] = route('admin.stockreport.getModifiedStockReportAjaxListData');
        $this->data['dt_search_colums'] = ['fid','fpid','fsku','fseller','pname','reportrange','fbrand'];

        $this->data['title'] = 'Modification Report';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = 'modifiedstock';
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Stock Report',
            ],
            (object) [
                'url' => false,
                'title' => 'Modification Report',
            ],
        ];

        //get all sellers
        $sellers = Seller::get();
        $this->data['sellers'] = $sellers;
        $this->data['brands'] = Brand::get();
        $this->data['categoryList'] =  Category::where('status', 1)->get();

        return parent::adminView('stockreport.modifiedstock', $this->data);
    }

    /**
     * [get all Products and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getModifiedStockReportAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'pv_unique_id',
            1 => 'sku',
            2 => 'name',
            3 => 'stock',

        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('Product', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fid' => (!is_null($request->fid))?$request->fid:null,
            'fpid' => (!is_null($request->fpid))?$request->fpid:null,
            'fsku' => (!is_null($request->fsku))?$request->fsku:null,
            'pname' => (!is_null($request->pname))?$request->pname:null,
            'reportrange' => (!is_null($request->reportrange))?$request->reportrange:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fbrand' => (!is_null($request->fbrand))?$request->fbrand:null,
        ];
        // DB::enableQueryLog();
        $prodVariants = ProductVariant::getModifiedStockReportAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        // dd(DB::getQueryLog());
        $iTotalRecords = $prodVariants->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin') || $this->loggedInUser->hasRole('Seller'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];
        foreach ($prodVariants as $prodVariant) {
            $records["data"][] = [
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->pv_unique_id.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->p_unique_id.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->sku.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->name.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.'Seller Name: '.$prodVariant->s_name.'<br>Seller Code:'.$prodVariant->seller_code.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$prodVariant->brand_name.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.date('m/d/Y',strtotime($prodVariant->updated_at)).'</div>',

            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }


    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function expiredspecialprices()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('products_perpage', 10);
        $this->data['dt_page'] = Session::get('products_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '3,4';
        $this->data['dt_ajax_url'] = route('admin.stockreport.getExpiredSpecialPricesAjaxListData');
        $this->data['dt_search_colums'] = ['fid'];

        $this->data['title'] = 'Expired Special Prices Report';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = 'expiredspecialprices';
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Stock Report',
            ],
            (object) [
                'url' => false,
                'title' => 'Expired Special Prices Report',
            ],
        ];

        return parent::adminView('stockreport.expiredspecialprices', $this->data);
    }

    /**
     * [get all Products and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getExpiredSpecialPricesAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'unique_id',
            2 => 'name',
            3 => 'id',
            4 => 'sku',
            5 => 'mrp',
            6 => 'sell_price',
            7 => 'created_at',
            8 => 'updated_at',
            9 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('Product', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'spsfromdate' => (!is_null($request->spsfromdate))?date('Y-m-d', strtotime($request->spsfromdate)):null,

        ];
        // DB::enableQueryLog();
        $products = ProductVariant::getExpiredSpecialPricesAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        // dd(DB::getQueryLog());
        $iTotalRecords = $products->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin') || $this->loggedInUser->hasRole('Seller'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];
        foreach ($products as $index=>$product) {
            $status = $statusList[$product->product->status];
            // dd($product->img);
            $variants = __getVariantOptionValues($product->id);
            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$product->id.'"/><span></span></label>',
                '<a href="'.route('admin.product.variantEdit', ['id'=>$product->product->id, 'vid'=>$product->id]).'" target="_blank" class="" title="View Order Details">'.$product->unique_id.'</a>',
                '<span class=""><a href="https://fabpik.in/product/'.$product->slug.'/'.$product->unique_id.'" target="_blank" class="" title="View Product Details">'.($product->name).'</a></span>',
                '<img src="'.url('/').'/'.UPLOAD_PATH.'/'.__getVariantImages($product->id).'" width="50" height="">',
                $product->product->category->title,
                $product->sku,
                $product->mrp,
                $product->price,
                number_format((float)($product->discount), '2'),
                $product->fabpik_seller_price,
                number_format((float)($product->fabpik_seller_discount), '1'),
                // number_format((float)($product->fabpik_seller_discount/$product->mrp)*100, '1'),
                number_format((float) ($product->fabpik_seller_discount_percentage), '1' ),
                date('d-M-Y h:i A', strtotime($product->special_price_start_date)),
                date('d-M-Y h:i A', strtotime($product->special_price_end_date)),
                $variants['PrimaryAttrValue'],
                $variants['SecondaryAttrValue'],
                date('d M Y', strtotime($product->created_at)),
                date('d M Y', strtotime($product->updated_at)),
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                '',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export stocks]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function sellerproductstockexport(Request $request)
    {
        $criteria = (object)[
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fcode' => (!is_null($request->fcode))?$request->fcode:null,
        ];
        $prodVariants = ProductVariant::getSellerProductStockExportData($criteria);

        return Excel::download(new SellerProductStockExport($prodVariants), 'sellerproductsreport.xlsx');
    }
    /**
     * [export stocks]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function modifiedstockexport(Request $request)
    {
        $criteria = (object)[
            'fid' => (!is_null($request->fid))?$request->fid:null,
            'fpid' => (!is_null($request->fpid))?$request->fpid:null,
            'fsku' => (!is_null($request->fsku))?$request->fsku:null,
            'pname' => (!is_null($request->pname))?$request->pname:null,
            'reportrange' => (!is_null($request->reportrange))?$request->reportrange:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fbrand' => (!is_null($request->fbrand))?$request->fbrand:null,
        ];
        $prodVariants = ProductVariant::getModifiedStockReportExportData($criteria);

        return Excel::download(new ModifiedStockExport($prodVariants), 'modifiedstockreport.xlsx');
    }
    /**
     * [export stocks]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function lowstockexport(Request $request)
    {
        $criteria = (object)[
            'seller' => $this->loggedInUser->ref_id,
            'fid' => (!is_null($request->fid))?$request->fid:null,
            'fpid' => (!is_null($request->fpid))?$request->fpid:null,
            'fsku' => (!is_null($request->fsku))?$request->fsku:null,
            'pname' => (!is_null($request->pname))?$request->pname:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fcategory' => (!is_null($request->fcategory))?$request->fcategory:null,
            'fbrand' => (!is_null($request->fbrand))?$request->fbrand:null,
        ];
        $prodVariants = ProductVariant::getLowStockReportExportData($criteria);

        return Excel::download(new LowStockExport($prodVariants), 'lowstockreport.xlsx');
    }

    /**
     * [export stocks]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function expiredspecialpricesexport(Request $request)
    {
        $criteria = (object)[
            'fid' => (!is_null($request->fid))?$request->fid:null,
            'pname' => (!is_null($request->pname))?$request->pname:null,
            'fcategory' => (!is_null($request->fcategory))?$request->fcategory:null,
            'fbrand' => (!is_null($request->fbrand))?$request->fbrand:null,
        ];
        $prodVariants = ProductVariant::expiredspecialpricesexport($criteria);

        return Excel::download(new ExpiredSpecialPricesExport($prodVariants), 'expiredspecialpricesreport.xlsx');
    }
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;
use PDF;

// Models
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\PaymentStatus;
use App\Models\ShippingStatus;
use App\Models\Seller;
use App\Models\ProductVariantOption;
use App\Models\Attribute;

// Exports

class PaymentController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'payments';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Products listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('payments_perpage', 10);
        $this->data['dt_page'] = Session::get('payments_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.payments.getAjaxListData');
        $this->data['dt_search_colums'] = ['ffromdate', 'ftodate', 'fostatus', 'fseller', 'fpstatus', 'forderid', 'fpayoutdate', 'fpaymentstatus'];

        $this->data['title'] = 'Payment & Commission';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Payment & Commission',
            ]
        ];

        $this->data['sellers'] = Seller::get();
        $this->data['orderStatus'] = OrderStatus::get();
        $this->data['paymentStatus'] = PaymentStatus::get();

        return parent::adminView('payments.index', $this->data);
    }

    
    /**
     * [get all Parent Orders and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'created_at',
            5 => 'mrp',
            6 => 'price',
            9 => 'commission',
            13 => 'final_amount_to_pay_seller',
            14 => 'order_status_id',
            15 => 'payout_status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('order', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                OrderDetail::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Payments are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        if ($request->input('customActionName') == "changestatus") {
            DB::beginTransaction();
            try {
                OrderDetail::whereIn('id', $request->input('id'))->update([
                    "payout_status" => $request->input('status'),
                    "payout_date_from_admin" => date('Y-m-d'),
                ]);

                DB::commit();
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Payout Status Changed successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'fostatus' => (!is_null($request->fostatus))?$request->fostatus:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
            'fpaymentstatus' => (!is_null($request->fpaymentstatus))?$request->fpaymentstatus:null,
            'fpstatus' => (!is_null($request->fpstatus))?$request->fpstatus:null,
            'fpayoutdate' => (!is_null($request->fpayoutdate))?date('Y-m-d', strtotime($request->fpayoutdate)):null,
            'forderid' => (!is_null($request->forderid))?$request->forderid:null,
        ];
        
        $orders = OrderDetail::getAjaxPaymentsListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $orders->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($orders as $index=>$order) {   
            /**
             * if the order created data is less than 07-july-2021, then show the price cloumn
             * order created date is more than 07-july-2021, then show the seller_price cloumn
            */
            $unitPrice = (strtotime('2021-07-07 00:00:00') > strtotime($order->created_at)) ? $order->price : $order->seller_price;
            
            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.payments.view', ['id'=>$order->id]).'" class="btn btn-icon-only default btn-circle" title="View Order Details">
                <span aria-hidden="true" class="icon-eye"></span> 
            </a>
            <!--<a href="'.route('admin.payments.sellerInvoice', ['id'=>$order->id]).'" class="btn btn-icon-only default btn-circle" target="_blank" title="Download Invoice">
                <span aria-hidden="true" class="icon-cloud-download"></span>
            </a>-->
            </div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$order->id.'"/><span></span></label>',
                date('d M Y', strtotime($order->created_at)),
                '<a href="'.route('admin.childorders.view', ['id'=>$order->id]).'">'.$order->child_order_id.'</a>',
                '<a href="'.route('admin.sellers.view', ['id'=>$order->seller_id]).'">'. $order->seller->seller_code.'</a>',
                $order->seller->name,
                $order->mrp,
                $unitPrice,
                $order->quantity,
                $order->total,
                number_format((float)$order->commission, '1'),
                ($order->sgst + $order->cgst),
                $order->shipping_charge,
                ($order->shipping_charge_sgst + $order->shipping_charge_cgst),
                number_format((float)$order->final_amount_to_pay_seller, '1'), 
                OrderDetail::getOrderStatusData($order->order_status_id)->admin_text,
                ($order->payout_status == 1)?'Yes':'No',
                ($order->payout_date_from_admin)?date('d M Y', strtotime($order->payout_date_from_admin)):'',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [Print the seller Invoice]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function sellerInvoice($id) {
        $this->data['title'] = 'Seller Invoice';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.childorders',
                'title' => 'Child Orders',
            ],
            (object) [
                'url' => 'javascript:;',
                'title' => 'Seller Invoice',
            ]
        ];
        // $this->data['orderDetails'] = OrderDetail::with('seller', 'product')->where('id', $id)->first();
        // $this->data['orders'] = Order::where('id', $this->data['orderDetails']->order_id)->first();
        $this->data['orderDetails'] = OrderDetail::with('order','seller', 'productVarient', 'orderHistory')->where('id', $id)->first();
        $productVarientOptions = ProductVariantOption::where('product_variant_id', $this->data['orderDetails']->productVarient->id)->get();
        $attributes = [];
        foreach ($productVarientOptions as $i=>$options) {
            $getAttributes = Attribute::with(['attributeOptions' => function($q) use ($options){
                    $q->where( 'id', '=', $options->attribute_option_id );
            }])->where('id', $options->attribute_id)->first();
            $attributes[$i]['atrributeName'] = isset($getAttributes->display_name) ? $getAttributes->display_name : '';
            $attributes[$i]['atrributeValue'] = isset($getAttributes->attributeOptions[0]->option_name) ? $getAttributes->attributeOptions[0]->option_name : '';
        }
        $this->data['attributeOptions'] = $attributes;

        return parent::adminView('payments.sellerinvoice', $this->data);
    }

    /**
     * [View Parent Products]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function view($id)
    {
        $this->data['title'] = 'Order Details';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.payments',
                'title' => 'Orders',
            ],
            (object) [
                'url' => 'javascript:;',
                'title' => 'Order Details',
            ]
        ];

        $this->data['id'] = $id;
        // $this->data['orderDetails'] = Order::with('orderDetail', 'customer', 'customerAddress', 'products')
                                        // ->where('id', $id)->first();
        $this->data['orderDetails'] = OrderDetail::with('order','seller', 'productVarient', 'orderHistory')->where('id', $id)->first();
        $productVarientOptions = ProductVariantOption::where('product_variant_id', $this->data['orderDetails']->productVarient->id)->get();
        $attributes = [];
        foreach ($productVarientOptions as $i=>$options) {
            $getAttributes = Attribute::with(['attributeOptions' => function($q) use ($options){
                    $q->where( 'id', '=', $options->attribute_option_id );
            }])->where('id', $options->attribute_id)->first();
            $attributes[$i]['atrributeName'] = isset($getAttributes->display_name) ? $getAttributes->display_name : '';
            $attributes[$i]['atrributeValue'] = isset($getAttributes->attributeOptions[0]->option_name) ? $getAttributes->attributeOptions[0]->option_name : '';
        }
        $this->data['attributeOptions'] = $attributes;

        $this->data['paymentStatus'] = PaymentStatus::get();
        $this->data['orderStatus'] = OrderStatus::get();
        $this->data['shippingStatus'] = ShippingStatus::get();

        return parent::adminView('payments.orderDetails', $this->data);
    }

     /**
     * [Download Payment Invoice]
      * @param  Request $request [description]
      * @param  [type]  $id      [product id]
      * @return [type]            [description]
    */
    public function downloadSellerInvoice($id)
    {
        // __downloadCustomerInvoice($id);
        ### 1. VERIFY SELLER IS FROM TELANGANA STATE OR ANY OTHER STATE
        ### 2. TO VERIFY COMBINE STATE TABLE TO SELLER ID INSIDE ORDER DETAILS
                         
        $data['orderDetails'] = OrderDetail::with('order', 'seller', 'productVarient', 'orderHistory')->where('id', $id)->first();
        //dd($data['orderDetails']);
        $productVarientOptions = ProductVariantOption::where('product_variant_id', $data['orderDetails']->productVarient->id)->get();
        $attributes = [];
        foreach ($productVarientOptions as $i=>$options) {
            $getAttributes = Attribute::with(['attributeOptions' => function ($q) use ($options) {
                $q->where('id', '=', $options->attribute_option_id);
            }])->where('id', $options->attribute_id)->first();

            $attributes[$i]['atrributeName'] = isset($getAttributes->display_name) ? $getAttributes->display_name : '-';
            $attributes[$i]['atrributeValue'] = isset($getAttributes->attributeOptions[0]) ? $getAttributes->attributeOptions[0]->option_name : '-';
        }
        $data['attributeOptions'] = $attributes;

        $pdf = PDF::loadView('invoices.sellerinvoicedownload', $data);
        return $pdf->download($data['orderDetails']->child_order_id.'.pdf');
    }
}
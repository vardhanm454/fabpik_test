<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Validator;
use Excel;
use DB;

// Models
use App\Models\Category;
use App\Models\Attribute;
use App\Models\AttributeOption;

// Exports
use App\Exports\AttributeExport;

class AttributeController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'attributes';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [All Attributes listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('attributes_perpage', 10);
        $this->data['dt_page'] = Session::get('attributes_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '5,6,7';
        $this->data['dt_ajax_url'] = route('admin.attributes.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Attributes';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Attributes',
            ],
        ];

        return parent::adminView('attributes.index', $this->data);
    }

    /**
     * [get all Attributes for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
            2 => 'display_name',
            3 => 'display_name',
            6 => 'status',
            7 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('attribute', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Attribute::whereIn('id', $request->input('id'))->delete();

                // reset categories primary attribute
                DB::statement("UPDATE `categories` SET `primary_attribute` = null WHERE `primary_attribute` IN(".implode(',',$request->input('id')).") ");

                // reset categories secondary attribute
                DB::statement("UPDATE `categories` SET `secondary_attribute` = null WHERE `secondary_attribute` IN(".implode(',',$request->input('id')).") ");

                DB::commit();
                $records["customActionMessage"] = 'Selected Attributes are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
        ];
        
        $attributes = Attribute::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $attributes->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        foreach ($attributes as $attribute) {
            //getting all categories
            // $categoriesName = [];
            // foreach ($attribute->attributeCategory as $attrCategory) {
            //     $categoriesName[] = $attrCategory->category->title;
            // }

            //getting all attribute options(Values)
            $values = [];
            foreach ($attribute->attributeOptions as $options) {
                $color = '';
                $calss = '';
                if($options->colour_code != null){
                    $calss .= "badge badge-default margin-bottom-5";
                    $color .= "background:".$options->colour_code;
                }
                $values[] = '<span class="'.$calss.'" style="'.$color.'">&nbsp;</span> &nbsp;'.$options->option_name;
            }

            $status = $statusList[$attribute->status];

            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.attributes.edit', ['id'=>$attribute->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>

            <a href="'.route('admin.attributes.values', ['id'=>$attribute->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Manage Values"><span aria-hidden="true" class="icon-tag"></span></a>

            <a href="javascript:;" del-url="'.route('admin.attributes.delete', ['id'=>$attribute->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><span aria-hidden="true" class="icon-trash"></span></a><div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$attribute->id.'"/><span></span></label>',
                $attribute->name,
                $attribute->display_name,
                '<span style="white-space: break-spaces;">'.((!empty($values))?implode(', ', $values).'<br/>':'').'',
                date('d M, Y', strtotime($attribute->created_at)),
                date('d M, Y', strtotime($attribute->updated_at)),
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export Attributes]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null
        ];
        $attributes = Attribute::getExportData($criteria);
        return Excel::download(new AttributeExport($attributes), 'attribute.xlsx');
    }

    /**
     * [Add New Attribute]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function add(Request $request)
    {
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                "name" => "required",
                "attribute_type" => "required",
                "display_name" => "required",
            ], [
                "name.required" => "The Attribute Name Filed Required",
                "attribute_type.required" => "The Attribute Type is Required",
                "categories.name.*.required" => "Select atleast one category",
            ]);

            if (!empty($request->name)) {
                $isExist = Attribute::where('name', $request->name)->where('attr_type', $request->attribute_type)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Attribute name already exist!');
                    });
                }
            }

            /*if( count($request->categories['name']) >= 1 
                && count($request->categories['primary']) >= 1
                && count(array_diff($request->categories['primary'], $request->categories['name'])) >= 1 ) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('categories', 'Choose primary from selected categories!');
                });
            }*/

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
            try {
                $attribute = new Attribute();
                $attribute->name = $request->name;
                $attribute->display_name = $request->display_name;
                $attribute->attr_type = $request->attribute_type;
                $attribute->save();

                /*foreach ($request->categories['name'] as $category) {
                    $attrCategory = new AttributeCategory();
                    $attrCategory->attribute_id = $attribute->id;
                    $attrCategory->category_id = $category;
                    $attrCategory->save();
                }

                // set primary attribute in category
                if(!empty($request->categories['primary'])) {
                    Category::whereIn('id', $request->categories['primary'])->update(['primary_attr'=>$attribute->id]);
                }*/

                DB::commit();
                return redirect()
                        ->route('admin.attributes')
                        ->with(['toast'=>'1','status'=>'success','title'=>'Attributes','message'=>'Success! Attribute Added successfully.']);
            } catch (Exception $e) {
                DB::rollback();
                return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'error','title'=>'Attributes','message'=>'Error! Some error occured, please try again.']);
            }
        }

        // $this->data['categories'] = Category::pluck('title', 'id');

        $this->data['title'] = 'Add New Attribute';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.attributes',
                'title' => 'Attributes',
            ],
        ];
        return parent::adminView('attributes.addEditAttribute', $this->data);
    }

    /**
     * [Edit/Update the Existing Attribute]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function edit(Request $request, $id)
    {
        $this->data['id'] = $id;
        $this->data['attribute'] = Attribute::find($id);
        // $this->data['attrCategories'] = AttributeCategory::where('attribute_id', $id)->pluck('category_id');
        // $this->data['attrPrimaries'] = Category::where('primary_attr', $id)->pluck('id');

        if ($request->save) {
            $validator = Validator::make($request->all(), [
                "name" => "required",
                "attribute_type" => "required",
                "display_name" => "required",
            ], [
                "name.required" => "The Attribute Name Filed Required",
                "attribute_type.required" => "The Attribute Type is Required",
                "categories.name.*.required" => "Select atleast one category",
            ]);

            if (!empty($request->name)) {
                $isExist = Attribute::where('name', $request->name)->where('id', '<>', $id)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Attribute name already exist!');
                    });
                }
            }

            /*if( count($request->categories['name']) >= 1 
                && count($request->categories['primary']) >= 1
                && count(array_diff($request->categories['primary'], $request->categories['name'])) >= 1 ) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('categories', 'Choose primary from selected categories!');
                });
            }*/

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
            try {

                // Update Attribute data
                $update = Attribute::where('id', $id)->update([
                    'name' => $request->name,
                    'display_name' => $request->display_name,
                    'status' => $request->status,
                    'attr_type' => $request->attribute_type,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                
                /*// Delete old Attribute categories
                AttributeCategory::where('attribute_id', $id)->delete();

                // Add new attribute categories
                foreach ($request->categories['name'] as $category) {
                    $attrCategory = new AttributeCategory();
                    $attrCategory->attribute_id = $id;
                    $attrCategory->category_id = $category;
                    $attrCategory->save();
                }

                // set primary attribute in category
                DB::statement("UPDATE `categories` SET `primary_attr` = null WHERE `primary_attr` = ".$id);
                if(!empty($request->categories['primary'])) {
                    Category::whereIn('id', $request->categories['primary'])->update(['primary_attr'=>$id]);
                }*/

                if ($update) {
                    DB::commit();

                    if ($request->save == 'save') {
                        return redirect()
                                ->route('admin.attributes')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Attributes','message'=>'Success! Attribute updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Attributes','message'=>'Success! Attribute updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.attributes')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Attributes','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                DB::rollback();
                return back();
            }
        }

        // $this->data['categories'] = Category::pluck('title', 'id');

        $this->data['title'] = 'Edit Attribute';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.attributes',
                'title' => 'Attributes',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['attribute']->name,
            ],
        ];        
        return parent::adminView('attributes.addEditAttribute', $this->data);
    }

    /**
     * [Delete Single Attribute data]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            $delete = Attribute::where('id', $id)->delete();
            if ($delete) {                
                // reset categories primary attribute
                DB::statement("UPDATE `categories` SET `primary_attribute` = null WHERE `primary_attribute` = " . $id);

                // reset categories secondary attribute
                DB::statement("UPDATE `categories` SET `secondary_attribute` = null WHERE `secondary_attribute` = " . $id);
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }

    /**
     * [Show the Attribute Values]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function values(Request $request, $id)
    {
        $this->data['id'] = $id;
        $attribute = Attribute::where('id', $id)->select('name','attr_type')->first();
        $this->data['attr_type'] = $attribute->attr_type;
        $this->data['attributeValues'] = AttributeOption::where('attribute_id', $id)->get();
        
        $this->data['title'] = 'Manage Attribute Value';
        $this->data['formtitle'] = 'Add Attribute Value';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.attributes',
                'title' => 'Attributes',
            ],
            (object) [
                'url' => '',
                'title' => $attribute->name,
            ],
            (object) [
                'url' => false,
                'title' => 'Values',
            ]
        ];

        return parent::adminView('attributes.attributeValues', $this->data);
    }

    /**
     * [add the values]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function valueAdd(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            "name" => $request->attr_type=="c"?"required|regex:/^[a-zA-Z\s]*$/":"required",
            "colorcode" => $request->attr_type=='c'?"required":""
        ], [
            "name.required" => "The Value Filed Required",
            "colorcode.required" => "Select a color"
        ]);

        if (!empty($request->name)) {
            $isExist = AttributeOption::where(['attribute_id'=>$id,'option_name'=>$request->name])->exists();
            if ($isExist) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('name', 'Option name already exist!');
                });
            }
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {
            $attrOption = new AttributeOption();
            $attrOption->attribute_id = $id;
            $attrOption->option_name = $request->name;

            if ($request->attr_type=='c') $attrOption->colour_code = $request->colorcode;

            if ($attrOption->save()) {
                return redirect()
                        ->route('admin.attributes.values', ['id'=>$id])
                        ->with(['toast'=>'1','status'=>'success','title'=>'Attributes','message'=>'Success! Value Added successfully.']);
            } else {
                return redirect()
                        ->route('admin.attributes.values', ['id'=>$id])
                        ->with(['toast'=>'1','status'=>'error','title'=>'Attributes','message'=>'Error! Some error occured, please try again.']);
            }
        } catch (Exception $e) {
            return redirect()
                    ->route('admin.attributes.values', ['id'=>$id])
                    ->with(['toast'=>'1','status'=>'error','title'=>'Attributes','message'=>'Error! Some error occured, please try again.']);
        }
    }

    /**
     * [Edit the values]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function valueEdit(Request $request, $id, $vid)
    {
        $this->data['id'] = $id;
        $this->data['vid'] = $vid;
        $attribute = Attribute::where('id', $id)->select('name','attr_type')->first();
        $this->data['attr_type'] = $attribute->attr_type;
        $this->data['attributeValues'] = AttributeOption::where('attribute_id', $id)->get();

        $this->data['optionData'] = AttributeOption::find($vid);
        
        if($request->save) {
            $validator = Validator::make($request->all(), [
                // "name" => "required",
                // "colorcode" => $request->attr_type=='c'?"required":""
                "name" => $request->attr_type=="c"?"required|regex:/^[a-zA-Z\s]*$/":"required",
                "colorcode" => $request->attr_type=='c'?"required":""
            ], [
                "name.required" => "The value field required",
                "colorcode.required" => "Select a color"
            ]);

            if (!empty($request->name)) {
                $isExist = AttributeOption::where(['attribute_id'=>$id,'option_name'=>$request->name])->where('id','<>',$vid)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Option name already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            try {
                $tblData["option_name"] = $request->name;
                if ($request->attr_type == 'c') $tblData["colour_code"]= $request->colorcode;

                $update = AttributeOption::where('attribute_id', $id)->where('id', $vid)->update($tblData);

                if ($update) {
                    return redirect()
                            ->route('admin.attributes.values', ['id'=>$id])
                            ->with(['toast'=>'1','status'=>'success','title'=>'Attributes','message'=>'Success! Value Edited successfully.']);
                } else {
                    return redirect()
                            ->route('admin.attributes.values', ['id'=>$id])
                            ->with(['toast'=>'1','status'=>'error','title'=>'Attributes','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                return redirect()
                        ->route('admin.attributes.values', ['id'=>$id])
                        ->with(['toast'=>'1','status'=>'error','title'=>'Attributes','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['title'] = 'Manage Attribute Value';
        $this->data['formtitle'] = 'Edit Attribute Value';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.attributes',
                'title' => 'Attributes',
            ],
            (object) [
                'url' => '',
                'title' => $attribute->name,
            ],
            (object) [
                'url' => false,
                'title' => $this->data['optionData']->option_name,
            ]
        ];

        return parent::adminView('attributes.attributeValues', $this->data);
    }

    /**
     * [Delete the values]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function valueDelete(Request $request, $vid)
    {
        $delete = AttributeOption::where('id', $vid)->delete();
        if (!$delete) {
            return redirect()
                    ->route('admin.attributes')
                    ->with(['toast'=>'1','status'=>'error','title'=>'Attributes','message'=>'Error! Some error occured, please try again.']);
        } else {
            return redirect()
                    ->route('admin.attributes')
                    ->with(['toast'=>'1','status'=>'success','title'=>'Attributes','message'=>'Success! Value Deleted successfully.']);
        }
    }
}

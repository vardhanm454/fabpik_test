<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Excel;
use DB;

// Models
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\ProductCategory;

// Exports
use App\Exports\SubcategoryExport;

class SubCategoryController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'subcategories';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [SubCategories listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('subcategories_perpage', 10);
        $this->data['dt_page'] = Session::get('subcategories_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.subcategories.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Sub Categories';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Sub Categories',
            ],
        ];

        return parent::adminView('subcategory.index', $this->data);
    }

    /**
     * [get all Sub-Categories and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'title',
            2 => 'title',
            3 => 'image',
            4 => 'icon',
            5 => 'status',
            6 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('subcategory', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Subcategory::whereIn('id', $request->input('id'))->delete();
                Subcategory::deleteChildCategory($request->input('id'));
                
                ProductCategory::where('subcategory_id', $request->input('id'))->update([
                    'is_active' => 0,
                ]);
                
                DB::commit();
                $records["customActionMessage"] = 'Selected Sub Categories are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
            'fcategoryid' => ($request->fcategoryid)?:null,
        ];
        $subcategories = Subcategory::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $subcategories->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($subcategories as $subcategory) {
            $status = $statusList[$subcategory->status];

            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.subcategories.edit', ['id'=>$subcategory->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('admin.subcategories.delete', ['id'=>$subcategory->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a></div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$subcategory->id.'"/><span></span></label>',
                $subcategory->title,
                ($subcategory->category)?$subcategory->category->title:'',
                '<img src="'.route('ajax.previewImage', ['image'=>$subcategory->image,'type'=>'subcategory']).'" class="subcategorybanner-thumbnail-img" alt="'.$subcategory->title.'" height="75" width="75"/>',
                '<img src="'.route('ajax.previewImage', ['image'=>$subcategory->icon,'type'=>'subcategory']).'" class="subcategoryicon-thumbnail-img" alt="'.$subcategory->title.'" height="50" width="50"/>',
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                // '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export Sub Categories]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null
        ];
        $subcategories = Subcategory::getExportData($criteria);
        return Excel::download(new SubcategoryExport($subcategories), 'Subcategories.xlsx');
    }

    /**
     * [Add New SubCategories]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                "parentcategory"=>"required",
                "name"=>"required",
                "description"=>"required|max:255",
                "banner_image"=>"required",
                "icon_image"=>"required",
            ], []);

            /*if (!empty($request->name)) {
                $isExist = Subcategory::where('title', $request->name)->where('category_id','<>',$request->parentcategory)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Sub Category name already exist!');
                    });
                }
            }*/

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            try {
                $tblData = new subcategory();
                $tblData->category_id = $request->parentcategory;
                $tblData->title = $request->name;
                $tblData->slug = __generateSlug($request->name);
                $tblData->description = $request->description;
                $tblData->image = $request->banner_image;
                $tblData->icon = $request->icon_image;
                $tblData->meta_title = $request->meta_title;
                $tblData->meta_keywords = $request->meta_keywords;
                $tblData->meta_description = $request->meta_description;
                // $tblData->status  = $request->status;
                // $tblData->updated_at = date('Y-m-d H:i:s');

                if ($tblData->save()) {
                    DB::commit();
                    return redirect()
                            ->route('admin.subcategories')
                            ->with(['toast'=>'1','status'=>'success','title'=>'Sub Category','message'=>'Success! Sub Category Added successfully.']);
                } else {
                    DB::rollback();
                    return redirect()
                            ->route('admin.subcategories')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Sub Category','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                DB::rollback();
                return back();
            }
        }
        $this->data['title'] = 'Add Sub Category';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.subcategories',
                'title' => 'Sub Category',
            ],
            (object) [
                'url' => false,
                'title' => 'Add Sub Category',
            ]
        ];

        $this->data['categories'] = Category::select('id','title')->where('status', 1)->get();
        return parent::adminView('subcategory.addEditSubcategory', $this->data);
    }
    
    /**
     * [edit Sub Category data]
     * @param  Request $request [description]
     * @param  [type]  $id      [subcategory id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $subcategory = Subcategory::find($id);
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                "parentcategory"=>"required",
                "name"=>"required",
                "description"=>"required|max:255",
                "banner_image"=>"required",
                "icon_image"=>"required",
                "status"=>"required"
            ], []);

            /*if (!empty($request->name)) {
                $isExist = Subcategory::where('title', $request->name)->where('id', '<>', $id)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Sub Category name already exist!');
                    });
                }
            }*/

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            try {
                $tblData = [
                    'category_id' => $request->parentcategory,
                    'title' => $request->name,
                    'slug' => __generateSlug($request->name),
                    'description' => $request->description,
                    'image' => $request->banner_image,
                    'icon' => $request->icon_image,
                    'meta_title' => $request->meta_title,
                    'meta_keywords' => $request->meta_keywords,
                    'meta_description' => $request->meta_description,
                    'status' => $request->status,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $update = Subcategory::where('id', $id)->update($tblData);

                if ($update) {
                    DB::commit();

                    if ($request->save == 'save') {
                        return redirect()
                                ->route('admin.subcategories')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Sub Categories','message'=>'Success! Sub Category data updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Sub Categories','message'=>'Success! Sub Category data updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.subcategories')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Sub Categories','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                DB::rollback();
                return back();
            }
        }

        $this->data['subcategory'] = $subcategory;
        $this->data['id'] = $id;

        $this->data['title'] = 'Edit Sub Category';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.subcategories',
                'title' => 'Sub Category',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['subcategory']->title,
            ]
        ];
        $this->data['categories'] = Category::select('id','title')->where('status', 1)->get();
        return parent::adminView('subcategory.addEditSubcategory', $this->data);
    }

    /**
     * [Delete Single Category data]
     * @param  Request $request [description]
     * @param  [type]  $id      [subcategory id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $delete = Subcategory::where('id', $id)->delete();
            Subcategory::deleteChildCategory([$id]);
            DB::commit();
            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }
}

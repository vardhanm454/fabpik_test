<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use DB;
use Session;
use Validator;
use Excel;
use Hash;
use Mail;
use Carbon\Carbon;

// Models
use App\Models\Customer;
use App\Models\Notifications;
use App\Models\User;
use App\Models\VerifyOtp;

// Exports
use App\Exports\CustomerExport;

//Mails
use App\Mail\CustomerRegisterSuccessEmail;
use App\Mail\CustomerReActivateEmail;
use App\Mail\CustomerDeactivateEmail;

class CustomerController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'customers';
    public $activeSubmenu = 'Customers';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Customers listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('customers_perpage', 10);
        $this->data['dt_page'] = Session::get('customers_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.customers.getAjaxListData');
        $this->data['dt_search_colums'] = ['fstatus', 'ffromdate', 'ftodate', 'fname', 'fmobile', 'femail'];

        $this->data['title'] = 'Customers';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Customers',
            ]
        ];

        return parent::adminView('customers.index', $this->data);
    }

    /**
     * [get all Customers for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
            2 => 'email',
            3 => 'mobile',
            4 => 'created_at',
            5 => 'updated_at',
            6 => 'status',
            7 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('customer', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Customer::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Customers are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        if ($request->input('customActionName') == "changestatus") {
            DB::beginTransaction();
            try {
                Customer::whereIn('id', $request->input('id'))->update([
                    "status" => $request->input('status'),
                    "inactive_reason" => $request->input('inactive_reason')
                ]);

                if($request->input('status') == '1'){
                    // send seller account activation email
                    $ids = $request->input('id');
                    foreach($ids as $id){
                        $customer_details = Customer::findOrFail($id);
                        Mail::to($customer_details->email)->send(new CustomerReActivateEmail($customer_details));
                    }
                }else{
                    $ids = $request->input('id');
                    foreach($ids as $id){
                        $customer_details = Customer::findOrFail($id);
                        Mail::to($customer_details->email)->send(new CustomerDeactivateEmail($customer_details));
                    }
                }

                DB::commit();
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Status Changed successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        if ($request->input('customActionName') == "notify") {
            DB::beginTransaction();
            try {
                // dd($request->input('select_all'));
                if($request->input('select_all') == '1'){

                    $tblData = new Notifications();
                    $tblData->title = $request->input('subject');
                    $tblData->notify_on = date("Y-m-d H:i:s", strtotime($request->input('notify_on')));
                    $tblData->content = $request->input('content');
                    $tblData->notified_to = 'c';
                    $tblData->created_by = $this->loggedInUser->id;
                    $tblData->save();
                }
                else{
                    $customers =  User::whereIn('ref_id',$request->input('id'))->where('user_type','customer')->pluck('id');
                    $customers = json_encode($customers);
                    $customers = json_decode($customers);
                    $customers = array_map('strval', $customers);
                    $tblData = new Notifications();
                    $tblData->title = $request->input('subject');
                    $tblData->notify_on = date("Y-m-d H:i:s", strtotime($request->input('notify_on')));
                    $tblData->content = $request->input('content');
                    $tblData->created_by = $this->loggedInUser->id;
                    $tblData->notified_to = 'c';
                    $tblData->users = json_encode($customers);
                    $tblData->save(); 
                }
                
                DB::commit();
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = 'Notification Updated successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            // 'length' => intval($request->length),
            // 'fname' => ($request->fname)?:null,
            // 'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null
            'length' => intval($request->length),
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'fname' => (!is_null($request->fname))?$request->fname:null,
            'fmobile' => (!is_null($request->fmobile))?$request->fmobile:null,
            'femail' => (!is_null($request->femail))?$request->femail:null,
        ];
        $customers = Customer::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $customers->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($customers as $customer) {
            $status = $statusList[$customer->status];
            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.customers.edit', ['id'=>$customer->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('admin.customers.delete', ['id'=>$customer->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a></div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$customer->id.'"/><span></span></label>',
                $customer->name,
                $customer->email,
                $customer->mobile,
                date('d M Y', strtotime($customer->created_at)),
                date('d M Y', strtotime($customer->updated_at)),
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export Customers]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null
        ];
        $customers = Customer::getExportData($criteria);
        return Excel::download(new CustomerExport($customers), 'customers.xlsx');
    }

    /**
     * [add new customer]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function add(Request $request)
    {
        $this->data['title'] = 'Add Customer';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'Customers',
            ]
        ];
        if ($request->continue) {
            $validator = Validator::make($request->all(), [
                        'email' => 'unique:users',
                        'mobile' => 'unique:users',
                    ]);

            if ($validator->fails()) {
                return response()->json(['success'=>2, 'message'=>"errors", 'errors'=>$validator->getMessageBag()->toArray()], 200);
            }
            //sendOTP Code
            $otp = __generateOtp();
            $this->saveOTP($request->mobile, $otp);

            if($this->sendOtp($request->mobile, $otp, 'verify_mobile')) {
                $data = ['mobile' => $request->mobile];
                return response()->json(['success'=>3, 'message'=>"Success"], 200);
            }else{
                return response()->json(['success'=>2, 'message'=>"errors", 'errors'=>'Somethin went wrong try after some time.'], 200);
            }
        }
        if ($request->save) {
            $validator = Validator::make($request->all(), []);
            
            $otpData = VerifyOtp::where('mobile', $request->mobile)->first();
            if($request->mobile_otp != $otpData->otp){
                return response()->json(['success'=>4, 'message'=>"errors", 'errors'=>'OTP Mismatch'], 200);       
            }

            $password = 123456;
            // Create a Customer
            $customer = new Customer();
            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->mobile = $request->mobile;
            $customer->save();

            // Create a new Customer User
            $user = new User();
            $user->customer_id = $customer->id;
            $user->name = $customer->name;
            $user->email = $customer->email;
            $user->mobile = $customer->mobile;
            $user->password = Hash::make($password);
            $user->save();

            //Send Email Code
            $customerRegisterMail = Mail::to($user->email)->send(new CustomerRegisterSuccessEmail($customer,$password));
            return response()->json(['success'=>1, 'message'=>"Success"], 200);
        }
                
        return parent::adminView('customers.addCustomer', $this->data);
    }

    /**
     * [edit Customer data]
     * @param  Request $request [description]
     * @param  [type]  $id      [customer id]
     * @return [type]           [description]
    */
    public function edit(Request $request, $id)
    {
        $editcustomer = Customer::where('id', $id)->first();

        $this->data['title'] = 'Edit Customer';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'Customers',
            ]
        ];
        $this->data['editcustomer'] = $editcustomer;
        $this->data['id'] = $id;

        if ($request->continue) {
            if($editcustomer->mobile != $request->mobile){
                $validator = Validator::make($request->all(), [
                    'mobile' => 'unique:users',
                ]);
    
                if ($validator->fails()) {
                    return response()->json(['success'=>2, 'message'=>"errors", 'errors'=>$validator->getMessageBag()->toArray()], 200);
                }
                //sendOTP Code
                $otp = __generateOtp();
                $this->saveOTP($request->mobile, $otp);

                if($this->sendOtp($request->mobile, $otp, 'verify_mobile')) {
                    $data = ['mobile' => $request->mobile];
                    return response()->json(['success'=>3, 'message'=>"Success"], 200);
                }else{
                    return response()->json(['success'=>2, 'message'=>"errors", 'errors'=>'Somethin went wrong try after some time.'], 200);
                }

                return response()->json(['success'=>3, 'message'=>"Success"], 200);
            }else{
                $tbldata = [
                    'name' => $request->name,
                ];
                $update = Customer::where('id', $id)->update($tbldata);
                if ($update) {
                    return response()->json(['success'=>1, 'message'=>"Success"], 200);
                }
            }            
        } if ($request->save) {
            $otpData = VerifyOtp::where('mobile', $request->mobile)->first();
            if($request->mobile_otp != $otpData->otp){
                return response()->json(['success'=>4, 'message'=>"errors", 'errors'=>'OTP Mismatch'], 200);
            }
            $tbldata = [
                'name' => $request->name,
                'mobile' => $request->mobile,
            ];
            $update = Customer::where('id', $id)->update($tbldata);
            if ($update) {
                return response()->json(['success'=>1, 'message'=>"Success"], 200);
            }

            //Send Email to inform user regarding mobile update

            return response()->json(['success'=>1, 'message'=>"Success"], 200);
        }

        return parent::adminView('customers.editCustomer', $this->data);
    }

    /**
     * [Delete Single Customer data]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $delete = Customer::where('id', $id)->delete();
            $delete_user = User::where('ref_id', $id)->delete();
            DB::commit();
            if($delete && $delete_user){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }

    public function sendOtp($mobile, $otp, $type='verify_mobile')
    {
        // return Utility::sendSms($mobile, "Your verification code is {$otp} for {$type} in Fabpik.");
        __sendSms($mobile, $type, ['otp'=>$otp]);
        return true;
    }

    public function saveOTP($mobile, $otp)
    {
        return VerifyOtp::updateOrInsert([
            'mobile' => $mobile
        ],
        [
            'mobile' => $mobile,
            'otp' => $otp,
            'otp_expiry' => Carbon::now()->addMinutes(3)
        ]);
    }
}

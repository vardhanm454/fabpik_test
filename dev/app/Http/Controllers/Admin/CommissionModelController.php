<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Excel;
use DB;

// Models
use App\Models\CommissionModel;
use App\Models\Seller;

// Exports

// Repository

class CommissionModelController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'commissionmodels';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [Categories listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('commissionmodel_perpage', 10);
        $this->data['dt_page'] = Session::get('commissionmodel_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,5,6,7,8,9,10';
        $this->data['dt_ajax_url'] = route('admin.commissionmodels.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname', 'fstatus', 'fcommissiontype', 'fshippingcharges', 'fhandlingcharges', 'foncommission'];

        $this->data['title'] = 'Commission Model';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Commission Models',
            ],
            (object) [
                'url' => false,
                'title' => '',
            ],
        ];

        return parent::adminView('commissionmodel.index', $this->data);
    }

    /**
     * [get all Categories and for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('category', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                $isRecordDeleted = true;
                foreach($request->input('id') as $id){
                    $recordCount = Seller::where('commission_id', $id)->count();

                    if ($recordCount > 0) {
                        $isRecordDeleted = false;
                        break;
                        // return response()->json(['success'=>2, 'message'=>"Commission Model is assigned to sellers", 'error'=>true], 200);
                    }else{
                        $deleteCommission = CommissionModel::findOrFail($id);
                        $deleteCommission->deleted_by = $this->loggedInUser->id;
                        $deleteCommission->deleted_at = date("Y-m-d H:i:s");

                        $delete = $deleteCommission->update();
                    }
                }

                if($isRecordDeleted){
                    DB::commit();
                    $records["customActionMessage"] = 'Selected Commission Model are deleted successfully.';
                }else{
                    DB::rollback();
                    $records["customActionStatus"] = "NOT-OK";
                    $records["customActionMessage"] = 'Commission Model is assigned to sellers.';
                }
                
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus) && !empty($request->fstatus) )?$request->fstatus:null,
            'fcommissiontype' => (!is_null($request->fcommissiontype) && !empty($request->fcommissiontype) )?$request->fcommissiontype:null,
            'fshippingcharges' => (!is_null($request->fshippingcharges) && !empty($request->fshippingcharges) )?$request->fshippingcharges:null,
            'fhandlingcharges' => (!is_null($request->fhandlingcharges) && !empty($request->fhandlingcharges) )?$request->fhandlingcharges:null,
            'foncommission' => (!is_null($request->foncommission) && !empty($request->foncommission) )?$request->foncommission:null,
        ];
        $commissionmodels = CommissionModel::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $commissionmodels->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($commissionmodels as $model) {
            $status = $statusList[$model->status];

            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.commissionmodels.edit', ['id'=>$model->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span>
            </a>
            <a href="javascript:;" del-url="'.route('admin.commissionmodels.delete', ['id'=>$model->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><span aria-hidden="true" class="icon-trash"></span>
            </a>
            </div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$model->id.'"/><span></span></label>',
                $model->name,
                ($model->commission_type == 'v') ? 'Variable' : 'Fixed',
                ($model->commission_on == 's') ? 'Sell Price' : 'MRP',
                ($model->vary_with_mrp_discount == 'y') ? 'Yes' : 'No',
                ($model->shipping_charges == 'y') ? 'Yes' : 'No',
                ($model->handling_charges == 'y') ? 'Yes' : 'No', 
                (!is_null($model->created_by)) ? $model->createdUser->name : '',
                date('d-m-Y', strtotime($model->created_at)),
                '<a href="'.route('admin.sellers').'?search=1&freference='.$model->id.'" class="" data-toggle="tooltip" data-placement="top" data-original-title="References" title="References" target="_blank">References</a>',
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export Categories]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null
        ];
        $categories = Category::getExportData($criteria);
        return Excel::download(new CategoryExport($categories), 'commissionmodels.xlsx');
    }

    /**
     * [Add New Categories]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {
        if ($request->save) {
            
            $validator = Validator::make($request->all(), [
                "name"=>"required",
                "commission_type"=>"required",
                "commission_on"=>"required",
                "vary_with_mrp_discount"=>"required",
                "shipping_charges"=>"required",
                "handling_charges"=>"required"
            ], []);


            if (!empty($request->name)) {
                $isExist = CommissionModel::where('name', $request->name)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Commission name already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            try{

                DB::beginTransaction();
                $commissionModel = new CommissionModel();
               
                $commissionModel->name = $request->name;
                $commissionModel->commission_type = $request->commission_type;
                $commissionModel->commission_on = $request->commission_on;
                $commissionModel->vary_with_mrp_discount = $request->vary_with_mrp_discount;
                $commissionModel->shipping_charges = $request->shipping_charges;
                $commissionModel->handling_charges = $request->handling_charges;
                $commissionModel->description = $request->description;
                $commissionModel->created_by = $this->loggedInUser->id;

                if($commissionModel->save()){
                    DB::commit();
                    return redirect()
                            ->route('admin.commissionmodels')
                            ->with(['toast'=>'1','status'=>'success','title'=>'Commission Model','message'=>'Success! Commission Model Added successfully.']);
                }else{
                    DB::rollback();
                    return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'error','title'=>'Commission Model','message'=>'Error! Some error occured, please try again.']);
                }

            } catch (Exception $e){
                DB::rollback();
                return redirect()
                            ->back()
                            ->with(['toast'=>'1','status'=>'error','title'=>'Commission Model','message'=>'Error! Some error occured, please try again.']);
            }

        }

        $this->data['title'] = 'Add Commission Model';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.commissionmodels',
                'title' => 'Commission Model',
            ],
            (object) [
                'url' => false,
                'title' => 'Add Commission Model',
            ],
        ];

        return parent::adminView('commissionmodel.addEditCommissionModel', $this->data);
    }

    /**
     * [edit Category data]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $this->data['commissionmodel'] = CommissionModel::find($id);
        $this->data['id'] = $id;

        if ($request->save) {
            $validator = Validator::make($request->all(), [
                "name"=>"required",
                "commission_type"=>"required",
                "commission_on"=>"required",
                "vary_with_mrp_discount"=>"required",
                "shipping_charges"=>"required",
                "handling_charges"=>"required",
            ], []);


            if (!empty($request->name)) {
                $isExist = CommissionModel::where('name', $request->name)->where('id', '<>', $id)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Commission name already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
            try {

                $tblData = [
                    'name' => $request->name,
                    'commission_type' => $request->commission_type,
                    'commission_on' => $request->commission_on,
                    'vary_with_mrp_discount' => $request->vary_with_mrp_discount,
                    'shipping_charges' => $request->shipping_charges,
                    'handling_charges' => $request->handling_charges,
                    'description' => $request->description,
                    'status' => $request->status,
                    'updated_by' => $this->loggedInUser->id,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];


                $update = CommissionModel::where('id', $id)->update($tblData);
               
                if ($update) {                    

                    DB::commit();

                    if ($request->save == 'save') {
                        return redirect()
                            ->route('admin.commissionmodels')
                            ->with(['toast'=>'1','status'=>'success','title'=>'Commission Model','message'=>'Success! Commission Model Edited successfully.']);
                    }else{
                        return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'success','title'=>'Commission Model','message'=>'Success! Commission Model Edited successfully.']);
                    }

                } 

                DB::rollback();
                return redirect()
                            ->back()
                            ->with(['toast'=>'1','status'=>'error','title'=>'Commission Model','message'=>'Error! Some error occured, please try again.']);

            } catch(Exception $e){
                dd($e);
                DB::rollback();
                return redirect()
                            ->back()
                            ->with(['toast'=>'1','status'=>'error','title'=>'Commission Model','message'=>'Error! Some error occured, please try again.']);
            }
        }
        
        $this->data['id'] = $id;

        $this->data['title'] = 'Edit Commission Model';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.commissionmodels',
                'title' => 'Commission Model',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['commissionmodel']->name,
            ],
        ];


        return parent::adminView('commissionmodel.addEditCommissionModel', $this->data);
    }

    /**
     * [Delete Single Commission Model data]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {

            $recordCount = Seller::where('commission_id', $id)->count();

            if ($recordCount > 0) {
               return response()->json(['success'=>2, 'message'=>"Commission Model is assigned to sellers", 'error'=>true], 200);
            }

            $deleteCommission = CommissionModel::findOrFail($id);
            $deleteCommission->deleted_by = $this->loggedInUser->id;
            $deleteCommission->deleted_at = date("Y-m-d H:i:s");

            $delete = $deleteCommission->update();

            DB::commit();

            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }
}

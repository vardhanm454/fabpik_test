<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Validator;
use Excel;
use DB;

// Models
use App\Models\SellerSettingChangeLog;
use App\Models\Seller;

class ApprovalController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'approvals';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [All Attributes listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('approvals_perpage', 10);
        $this->data['dt_page'] = Session::get('approvals_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '5,6,7';
        $this->data['dt_ajax_url'] = route('admin.approvals.getAjaxListData');
        $this->data['dt_search_colums'] = ['ftype','foldvalue', 'fnewvalue', ];

        $this->data['title'] = 'Approvals';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Approvals',
            ],
        ];

        return parent::adminView('approvals.index', $this->data);
    }

    /**
     * [get all Attributes for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'settings_column_name',
            1 => 'old_value',
            2 => 'new_value',
            3 => 'created_by',
            4 => 'created_at',
            5 => 'approved_by',
            6 => 'approved_at',
            7 => 'id',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('attribute', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
        ];
        
        $logs = SellerSettingChangeLog::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $logs->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        foreach ($logs as $log) {
            $approval = (!is_null($log->approved_by) && !is_null($log->approved_at));
            // var_dump($log);
            $actionBtns = ($canChange)?'<div class="">
            <a href="javascript:;" del-title="accept" del-url="'.route('admin.approvals.acceptRequest', ['id'=>$log->id]).'" class="btn btn-icon-only default btn-circle dt-list-view" data-toggle="tooltip" data-placement="top" data-original-title="Accept" title="Accept the request" ><span aria-hidden="true" class="fa fa-check"></span></a>

            <a href="javascript:;" del-title="reject" del-url="'.route('admin.approvals.rejectRequest', ['id'=>$log->id]).'" class="btn btn-icon-only default btn-circle dt-list-view" data-toggle="tooltip" data-placement="top" data-original-title="Reject" title="Reject the request"><span aria-hidden="true" class="fa fa-ban"></span></a>
            <div>':'';
            $oldValue = '';
            $newValue = '';
            if($log->settings_column_name == 'cod_enable'){
                $oldValue = ($log->old_value == 'y')?'Yes':'No';
                $newValue = ($log->new_value == 'y')?'Yes':'No';
               
            }else if($log->settings_column_name == 'shipping_model'){
                $oldValue = ($log->old_value == 'f')?'Fabship':'Self';
                $newValue = ($log->new_value == 'f')?'Fabship':'Self';
            }
            $records["data"][] = [
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.ucwords(str_replace('_', ' ', $log->settings_column_name)).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$oldValue.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.$newValue.'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;"><a href="'.route('admin.sellers.view', ['id'=>$log->seller->id]).'" title="View Seller" target="_blank">'.$log->seller->name.'</a></div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.date('d M Y', strtotime($log->created_at)).'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.(isset($log->approved_by)?$log->user->name:'-').'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.((!empty($log->approved_at))?date('d M Y', strtotime($log->approved_at)):'-').'</div>',
                '<div class="text-center" style="white-space: normal;word-break:break-word;">'.(($approval)?'<span class="badge badge-light-success badge-roundless">Approved</span>':'<div class="btn-group btn-group-sm btn-group-solid">'.$actionBtns.'</div>').'</div>',
                '<a href="'.route('admin.sellers.view', ['id'=>$log->seller->id]).'" title="View Seller" target="_blank">'.$log->seller->seller_code.'</a>',
                $log->seller->company_name,
                $log->seller->email,
                $log->seller->mobile,
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

     /**
     * [Accept the Request]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function acceptRequest(Request $request, $id)
    {
        $logDetails = SellerSettingChangeLog::find($id);
        $acceptRequest = Seller::where('id', $logDetails->created_by)->update([
            $logDetails->settings_column_name => $logDetails->new_value
        ]);

        $logUpdate = SellerSettingChangeLog::where('id', $id)->update([
            'approved_by' => $this->loggedInUser->id,
            'approved_at' => date('Y-m-d H:M:S'),
        ]);

        if ($logUpdate && $acceptRequest) {
            
            $logs = SellerSettingChangeLog::with('seller')->where('id', $id)->first();
            $to=[
                "seller_email"=>$logs->seller->email
            ];
            if($logs->settings_column_name == "cod_enable" ) 
                __sendEmails($to, 'seller_cod_change_request_accept', $logs);

            if($logs->settings_column_name == "shipping_model" )
                __sendEmails($to, 'seller_shipping_change_request_accept', $logs);

            return response()->json(['success'=>1, 'type'=>'accept', 'message'=>"",], 200);
           
        } else {
            return response()->json(['success'=>0, 'type'=>'accept', 'message'=>"",], 200);
            
        }
    }

    /**
     * [Reject the Request]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function rejectRequest(Request $request, $id)
    {
        $logUpdate = SellerSettingChangeLog::where('id', $id)->update([
            'approved_by' => $this->loggedInUser->id,
            'approved_at' => date('Y-m-d H:M:S'),
            'is_deleted' => date('Y-m-d H:M:S'),
        ]);

        if ($logUpdate) {
            $logs = SellerSettingChangeLog::with('seller')->where('id', $id)->first();
            $to=[
                "seller_email"=>$logs->seller->email
            ];
            if ($logs->settings_column_name == "cod_enable") {
                __sendEmails($to, 'seller_cod_change_request_reject', $logs);
            }
            
            if ($logs->settings_column_name == "shipping_model") {
                __sendEmails($to, 'seller_shipping_change_request_reject', $logs);
            } 
            
            return response()->json(['success'=>1, 'type'=>'reject', 'message'=>"",], 200);
          
        } else {
          
            return response()->json(['success'=>0, 'type'=>'reject', 'message'=>"",], 200);
        }
    }
}

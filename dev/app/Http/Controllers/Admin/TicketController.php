<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Validator;
use Session;
use Excel;
use DB;

// Models
use App\Models\Ticket;
use App\Models\TicketComment;
use App\Models\seller;

class TicketController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'tickets';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [sizecharts listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('ticket_perpage', 10);
        $this->data['dt_page'] = Session::get('ticket_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.tickets.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Ticket List';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Support Tickets',
            ],
        ];

        return parent::adminView('tickets.index', $this->data);
    }

    /**
     * [get all seller Tickets for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'issue_title',
            2 => 'updated_at'
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];
        // dd(intval($request->length));
        $iPage = (intval($request->start) / intval($request->length)) + 1;
        
        __setDatatableCurrPage('ticket', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Ticket::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Tickets are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
        ];
        // DB::enableQueryLog();
        $tickets = Ticket::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        // dd(DB::getQueryLog());

        $iTotalRecords = $tickets->total();
        
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        $statusList = [
            ["danger" => "Inactive"],
            ["primary" => "Active"]
        ];

        foreach ($tickets as $ticket) {
            if ($ticket->issue_status==0) {
                $statusText = '<span class="badge badge-light-danger badge-roundless"> Pending </span>';
            } elseif ($ticket->issue_status==1) {
                $statusText = '<span class="badge badge-light-primary badge-roundless"> Processing </span>';
            } else {
                $statusText = '<span class="badge badge-light-success badge-roundless"> Completed </span>';
            }

            $actionBtns = ($canChange)?'<div class=""><a href="'.route('admin.tickets.view', ['id'=>$ticket->id]).'" class="btn btn-icon-only default btn-circle tooltips" data-toggle="tooltip" data-placement="top" data-original-title="View Details"><span aria-hidden="true" class="icon-eye"></span></a></div>':'';
            $records["data"][] = [
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$ticket->id.'"/><span></span></label>',
                '<a href="'.route('admin.tickets.view', ['id'=>$ticket->id]).'" class="tooltips" data-toggle="tooltip" data-placement="top" data-original-title="View Details">'.$ticket->ticket_no.'</a>',
                date('d M Y', strtotime($ticket->created_at)),
                ($ticket->updated_at)?date('d M Y', strtotime($ticket->updated_at)):'',
                ($ticket->seller) ? ( '<a href="sellers?search=1&fcompanyname='.$ticket->seller->company_name.'" target="_blank">'.$ticket->seller->company_name.' </a>' ) : '-',
                $ticket->issue_title,
                $ticket->category,
                '<a href="'.url('/').'/'.UPLOAD_PATH.'/support_ticket/'.$ticket->attachment.'" width="200px" height="200px" target="_blank">'.$ticket->attachment.'</a>',
                $statusText,
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [View Ticket by Seller]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function view(Request $request, $id)
    {
        $this->data['seller_id'] = auth()->user()->ref_id;
        $this->data['ticket_details'] = Ticket::with(['seller', 'comments' => function ($query) {
                                                        $query->orderBy('id', 'asc');
                                                    }])->where('id', $id)->first();
        $this->data['user'] = $this->loggedInUser;
        $this->data['title'] = 'Ticket Details';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.tickets',
                'title' => 'Support Tickets',
            ],
            (object) [
                'url' => '',
                'title' => 'View Ticket Details',
            ]
        ];

        return parent::adminView('tickets.view', $this->data);
    }

    /**
     * [post ticket comment]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postComment(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'comments' => 'required',
            'attachment'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $attachment = null;
        if($request->hasFile('attachment')){
            $attachment = time().'.'.$request->attachment->extension(); 
            $request->attachment->move(UPLOAD_PATH.'/support_ticket', $attachment);
        }
        $newComments = new TicketComment();
        $newComments->comments = $request->comments;
        $newComments->help_desk_id = $id;
        $newComments->attachment = $attachment;
        $newComments->comment_by = auth()->user()->id;
        if ($newComments->save()) {
            return redirect()
                ->back()
                ->with(['toast'=>'1','status'=>'success','title'=>'Support','message'=>'Success! Comment Posted successfully.']);
        }
    }

    /**
     * [post ticket comment]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function closeTicket(Request $request, $id)
    {
        try{
            $update = Ticket::where('id', $id)->update(['issue_status'=>2]);
            if ($update) {
                return redirect()->route('admin.tickets')->with(['toast'=>'1','status'=>'success','title'=>'Support','message'=>'Ticket Closed.']);
            }else{
                return redirect()->back()->with(['toast'=>'1','status'=>'error','title'=>'Support','message'=>'Something Went Wrong, Try After Sometime.']);
            }
        } catch (Exception $e){
            return redirect()->back()->with(['toast'=>'1','status'=>'error','title'=>'Support','message'=>'Something Went Wrong, Try After Sometime.']);
        }
        
    }
}

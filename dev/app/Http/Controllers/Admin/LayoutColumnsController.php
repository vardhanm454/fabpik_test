<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use DB;
use Session;
use Validator;
use Excel;

// Models
use App\Models\LayoutColumn;
use App\Models\LayoutColumnLog;
use App\Models\StyleLayout;

// Exports

class LayoutColumnsController extends CoreController
{
	public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'layoutcolumns';

    public function __construct() {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });        
    }

    /**
     * [brands listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('layoutcolumns_perpage', 10);
        $this->data['dt_page'] = Session::get('layoutcolumns_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,5,6,7,8,9';
        $this->data['dt_ajax_url'] = route('admin.layoutcolumns.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname'];

        $this->data['title'] = 'Style Layout Columns';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Style Layout Columns',
            ],
        ];

        return parent::adminView('stylelayoutcolumns.index', $this->data);
    }

    /**
     * [get all brands for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
            2 => 'updated_at',
            3 => 'status',
            4 => 'status',
            5 => 'status',
            6 => 'status',
            7 => 'status',
            8 => 'status',
            9 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('brand', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType) 
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {

            DB::beginTransaction();
            try {

                $isDelete = true;

                $getData = StyleLayout::select('columns')->get();
                
                foreach( $request->input('id') as $id ){
                    foreach ($getData as $column) {
                        $columns = json_decode($column->columns);
                        if (in_array($id, $columns)) {
                            $isDelete = false;
                            break;
                        }
                    }
                }

                if($isDelete){

                    $delete =  LayoutColumn::whereIn('id', $request->input('id'))->delete();

                    if($delete){
                        DB::commit();
                        $records["customActionStatus"] = "OK";
                        $records["customActionMessage"] = "Selected Columns Deleted Successfully";

                    }else {
                        DB::rollback(); 
                        $records["customActionStatus"] = "NOT-OK";
                        $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
                    }
    
                }else{
                    DB::rollback(); 
                    $records["customActionStatus"] = "NOT-OK";
                    $records["customActionMessage"] = 'Some Columns are assigned to stylesheet.';
                }
            }
            catch(Exception $e) {
                DB::rollback(); 
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
        ];

        $cloumns = LayoutColumn::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        $iTotalRecords = $cloumns->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ( $this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin') );

        foreach($cloumns as $i=>$column) {
             $status = ($column->status==0)?'<span class="badge badge-light-danger badge-roundless">Inactive</span>':'<span class="badge badge-light-success badge-roundless">Active</span>';

            $actionBtns = ($canChange)?'<div class="">
            <a href="#" class="btn btn-icon-only default btn-circle open-EditColumnModal" data-toggle="modal" data-placement="top" data-target="#editLayoutColumnsStatus" data-original-title="Edit" data-id="'.$column->id.'" data-name="'.$column->name.'" data-myvalue="'.$column->status.'" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>

            <a href="'.route('admin.layoutcolumns.history', ['id'=>$column->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="History" title="History"><span aria-hidden="true" class="fa fa-calendar"></span></a>

            <a href="javascript:;" del-url="'.route('admin.layoutcolumns.delete', ['id'=>$column->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>

            </div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$column->id.'"/><span></span></label>',
                $i+1,
                $column->name,
                $column->user->name,
                date('d-m-Y', strtotime($column->created_at)),
                (!is_null($column->modifiedUser)) ? $column->modifiedUser->name : '-',
                date('d-m-Y', strtotime($column->updated_at)),
                $status,
                '<a href="'.route('admin.stylelayout').'?search=1&freference='.$column->id.'" class="" data-toggle="tooltip" data-placement="top" data-original-title="References" title="References" target="_blank">References</a>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [add new Column]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {

        if (!empty($request->name)) {
            $isExist = LayoutColumn::where('name', $request->name)->exists();
            if ($isExist) {
                return response()->json(['success'=>false, 'message'=>"Column name already exists.", 'error'=>true], 200);
            }
        }

        try {
            DB::beginTransaction();

            $columns = new LayoutColumn();
            $columns->name = $request->name;
            $columns->created_by = $this->loggedInUser->id;
        
            if($columns->save()) {
                DB::commit();   
                return response()->json(['success'=>true, 'message'=>"Success! Column added successfully.", 'error'=>false], 200);     
            }
            else{
                return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
            }                    
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
        }
    }

    /**
     * [edit the existing Column]
     * @param  Request $request [description]
     * @param  [type]  $id      [brand id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        // dd($request->all());
        if (!empty($request->name)) {
            $isExist = LayoutColumn::where('name', $request->name)->where('id', '<>', $id)->exists();
            if ($isExist) {
                return response()->json(['success'=>false, 'message'=>"Column name already exists.", 'error'=>true], 200);
            }
        }

        try {
            DB::beginTransaction();

            if( ($request->name != $request->from_column_name) || ($request->to_status != $request->from_column_status) ){
                $columnLog = new LayoutColumnLog();
                $columnLog->column_id = $id;
                $columnLog->from_name = $request->from_column_name;
                $columnLog->to_name = $request->name;
                $columnLog->from_status = $request->from_column_status;
                $columnLog->to_status = $request->status;
                $columnLog->modified_by = $this->loggedInUser->id;
                $columnLog->save();
            }

            $tblData = [
                'name' => $request->name,
                'modified_by' => $this->loggedInUser->id,
                'status' => $request->to_status,
            ];
        
            $update = LayoutColumn::where('id',$id)->update($tblData);
        
            if($update) {
                DB::commit();   
                return response()->json(['success'=>true, 'message'=>"Success! Column Editied successfully.", 'error'=>false], 200);     
            }
            else{
                return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
            }                    
        } catch (Exception $e) {
            // dd($e);

            DB::rollback();
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again after sometime.", 'error'=>true], 200);
        }
    }

    /**
    * [History Style Layout Column]
    * @param  Request $request [description]
    * @param  [type]  $id      [category id]
    * @return [type]           [description]
   */
   public function history(Request $request, $id)
   {
        $columns = LayoutColumn::find($id);
        $columnHistories = LayoutColumnLog::with('modifiedUser')->where('column_id', $id)->get();

        if($columnHistories->count() == 0){
            return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'error','title'=>'Style Layout Columns','message'=>'No History Found.']);
        }

        $this->data['columns'] = $columns;
        $this->data['columnHistories'] = $columnHistories;
        $this->data['id'] = $id;
        
        $this->data['title'] = 'Column Modification History';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.layoutcolumns',
                'title' => 'Style Layout Columns',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['columns']->name,
            ],
        ];
        return parent::adminView('stylelayoutcolumns.history', $this->data);
   }

    /**
     * [Delete Single Style Layout Column]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            
            $isDelete = true;

            $getData = StyleLayout::select('columns')->get();

            foreach($getData as $column){
                $columns = json_decode($column->columns);
                if( in_array( $id ,  $columns) ){
                    $isDelete = false;
                    break;
                }
            }

            if($isDelete){

                $delete = LayoutColumn::where('id', $id)->delete();
                if($delete){
                    DB::commit();
                    return response()->json(['success'=>true, 'message'=>"Column Deleted Successfully", 'error'=>false], 200);
                }

            }else{
                DB::rollback();
                return response()->json(['success'=>false, 'message'=>"Column is assigned to stylesheet", 'error'=>true], 200);
            }
            
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
        }
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use DB;
use Session;
use Validator;
use Excel;

// Models
use App\Models\Brand;
use App\Models\Seller;


// Exports
use App\Exports\BrandExport;

class BrandController extends CoreController
{
	public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'brands';

    public function __construct() {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });        
    }

    /**
     * [brands listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('brands_perpage', 10);
        $this->data['dt_page'] = Session::get('brands_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.brands.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Brands';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Brands',
            ],
        ];

        return parent::adminView('brand.index', $this->data);
    }

    /**
     * [get all brands for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
            2 => 'updated_at',
            3 => 'status',
            4 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('brand', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType) 
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {

            DB::beginTransaction();
            try {
                Brand::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Brands are deleted successfully.';
            }
            catch(Exception $e) {
                DB::rollback(); 
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
        ];
        $brands = Brand::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);
        //dd($brands);
        $iTotalRecords = $brands->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        $canChange = ( $this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin') );

        foreach($brands as $brand) {
            $status = $statusList[$brand->status];
            
             //dd($brand->seller->name);
       

            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.brands.edit', ['id'=>$brand->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('admin.brands.delete', ['id'=>$brand->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a></div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$brand->id.'"/><span></span></label>',
                $brand->name,
                (isset($brand->seller->name))?$brand->seller->name:'',
                
                

                '<img src="'.route('ajax.previewImage',['image'=>$brand->image,'type'=>'brand']).'" class="brand-thumbnail-img" alt="'.$brand->name.'" width="100"/>',
                '<span class="badge badge-light-'.(key($status)).' badge-roundless">'.(current($status)).'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export brands]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null,
        ];
        $brands = Brand::getExportData($criteria);

        return Excel::download(new BrandExport($brands), 'brands.xlsx');
    }

    /**
     * [add new brand]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|regex:/^(?!\d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 @&$]*)?$/',
                'image' => 'required|string',
                'show_on_home'=>'required|integer',
                // 'seller'=>'required'         
            ],
            [
                // 'name.required' => 'The Name field is required.',
            ]);

            if (!empty($request->name)) {
                $isExist = Brand::where('name', $request->name)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Brand name already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
            
            try {
                $tblData = new Brand();
                $tblData->name = $request->name;
                $tblData->slug = __generateSlug($request->name);
                $tblData->show_on_home = $request->show_on_home;
                $tblData->image = $request->image;
                $tblData->meta_title = $request->meta_title;
                $tblData->meta_keywords = $request->meta_keywords;
                $tblData->meta_description = $request->meta_description;
                $tblData->created_by = $request->seller;


                if($tblData->save()) {
                    DB::commit();

                    return redirect()
                            ->route('admin.brands')
                            ->with(['toast'=>'1','status'=>'success','title'=>'Brands','message'=>'Success! Brand added successfully.']);
                }
                else
                    return redirect()
                            ->route('admin.brands')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Brands','message'=>'Error! Some error occured, please try again.']);
            } catch (Exception $e) {
                // dd($e);
                DB::rollback();
                return back();
            }
        }
        $sellers = seller::all();
        $this->data['sellers'] = $sellers;
        
        $this->data['title'] = 'Add Brand';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.brands',
                'title' => 'Brands',
            ],
            (object) [
                'url' => false,
                'title' => 'Add Brand',
            ],
        ];
        return parent::adminView('brand.addEditBrand', $this->data);
    }

    /**
     * [edit brand data]
     * @param  Request $request [description]
     * @param  [type]  $id      [brand id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $brand = Brand::find($id);

        $sellers = seller::all();
        //dd($seller);


        if($request->save) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|regex:/^(?!\d+$)(?:[a-zA-Z0-9][a-zA-Z0-9 @&$]*)?$/',
                'image' => 'required|string',
                'show_on_home'=>'required|integer', 
                // 'seller' =>'required'       
            ],
            [
                // 'name.required' => 'The Name field is required.',
            ]);

            if(!empty($request->name)) {
                $isExist = Brand::where('name', $request->name)->where('id', '<>', $id)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Brand name already exist!');
                    });
                }
            }

            if($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            DB::beginTransaction();
            try {
                $tblData = [
                    'name' => $request->name,
                    'slug' => __generateSlug($request->name),
                    'image' => $request->image,
                    'show_on_home' => $request->show_on_home,
                    'meta_title' => $request->meta_title,
                    'meta_keywords' => $request->meta_keywords,
                    'meta_description' => $request->meta_description,
                    'status' => $request->status,
                    'created_by'=>$request->seller,
                    'updated_at' => date('Y-m-d H:i:s')
                ];

                $update = Brand::where('id',$id)->update($tblData);

                if($update) {
                    DB::commit();

                    if($request->save == 'save') {
                        return redirect()
                                ->route('admin.brands')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Brands','message'=>'Success! Brand data updated successfully.']);
                    }
                    else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Brands','message'=>'Success! Brand data updated successfully.']);
                    }
                }
                else
                    return redirect()
                            ->route('admin.brands')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Brands','message'=>'Error! Some error occured, please try again.']);
            }
            catch(Exception $e) {
                DB::rollback(); 
                return back();
            }
        }

        $this->data['brand'] = $brand;
        $this->data['id'] = $id;
        $this->data['sellers'] = $sellers;
        
        $this->data['title'] = 'Edit Brand';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.brands',
                'title' => 'Brands',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['brand']->name,
            ],
        ];
        return parent::adminView('brand.addEditBrand', $this->data);
    }

    /**
     * [Delete Single Category data]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            $delete = Brand::where('id', $id)->delete();
            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }
}

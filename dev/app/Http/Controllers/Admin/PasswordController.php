<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use DB;
use Validator;
use Auth;
use Str;
use Hash;
use Session;
use Exception;
use Mail;

// Models
use App\Models\User;
use App\Models\Role;

// Mails
use App\Mail\ForgotPassword;

class PasswordController extends CoreController
{
	public $loggedInUser;

	public function __construct()
    {
        parent::__construct();

        // get current logged in user
        $this->loggedInUser = auth()->user();
    }

	// Forgot Password
	public function forgotPassword()
	{
		$data = [
    		'title' => 'Forgot Password'
    	];

        return parent::adminView('auth.forgot-password', $data);
	}

	public function setPassword()
	{
		$data = [
    		'title' => 'Set Password'
    	];

        return parent::adminView('auth.set-password', $data);
	}

	public function sendPasswordResetToken(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'email'=>'required|email',
        ]);

        $user = User::where ('email', $request->email)->first();
        if ( !$user ) {
        	$validator->after(function ($validator) {
	        	$validator->errors()->add('email', 'No account found with entered email.');
	        });
        }

        if($validator->fails()) {
        	return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $token = Str::random(40);
		//create a new token to be sent to the user. 
		DB::table('password_resets')->insert([
			'email' => $request->email,
			'token' => $token,
			'created_at' => date('Y-m-d H:i:s')
		]);

		$mailData = (object) ['email'=>$request->email,'token'=>$token];
		
		Mail::to($request->email)->send(new ForgotPassword($mailData));
		// Mail::to('ranjanmiraki@gmail.com')->send(new ForgotPassword($mailData));
		return redirect()
                ->back()
                ->with(['toast'=>'1','status'=>'success','title'=>'Forgot Password','message'=>'Success! Reset Password link sent successfully.']);
	}

	public function showPasswordResetForm($token, $email)
	{
		$tokenData = DB::table('password_resets')->where(['token'=>$token,'email'=>$email])->first();
		if ( !$tokenData ) return redirect()
									->route('login')
									->with(['toast'=>'1','status'=>'error','title'=>'Reset Password','message'=>'Invalid token and/or email id']);
		$data = [
    		'title' => 'Reset Password',
    		'token' => $token,
    		'email' => $email
    	];
		return parent::adminView('auth.password', $data);

	}

	public function resetPassword(Request $request, $token)
	{
		$validator = Validator::make($request->all(), [
            'password'=>'required',
            'con_password'=>'required',
        ]);

        if( $request->password !== $request->con_password ) {
        	$validator->after(function ($validator) {
	        	$validator->errors()->add('password', 'Please confirm your new password.');
	        });
        }

        if($validator->fails()) {
        	return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

		$password = $request->password;
		$tokenData = DB::table('password_resets')
						->where('token', $token)->first();

		$user = User::where('email', $tokenData->email)->first();
		if ( !$user ) return redirect()
								->route('login')
								->with(['toast'=>'1','status'=>'error','title'=>'Reset Password','message'=>'Invalid token and/or email id']);

		$user->password = Hash::make($request->password);
		$user->update();

		DB::table('password_resets')->where('email', $user->email)->delete();

		return redirect()
				->route('login')
				->with(['toast'=>'1','status'=>'success','title'=>'Reset Password','message'=>'Your password has been reset successfully.']);
	}
}

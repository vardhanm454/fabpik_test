<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Session;
use Excel;
use DB;
use Hash;
use Mail;
use Carbon\Carbon;

// Models
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\Staff;
use App\Models\User;

// Exports
use App\Exports\StaffExport;

// Repo
use Facades\App\Repository\PermissionRepo;
use Facades\App\Repository\StaffRepo;

class StaffController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'usermgmt';
    public $activeSubmenu = 'staff';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('staff_perpage', 10);
        $this->data['dt_page'] = Session::get('staff_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.staff.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','femail','fmobile','fstatus'];

        $this->data['title'] = 'Staffs';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Staffs',
            ]
        ];

        return parent::adminView('staff.index', $this->data);
    }

    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'name',
            2 => 'email',
            3 => 'status',
            4 => 'mobile_no',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;
        __setDatatableCurrPage('staff', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                User::whereIn('ref_id', $request->input('id'))->where('user_type', 'admin-staff')->delete();
                Staff::whereIn('id', $request->input('id'))->delete();
                DB::commit();

                $records["customActionMessage"] = 'Selected Staffs are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        $criteria = (object)[
            'length' => intval($request->length),
            'name' => (!is_null($request->fname))?$request->fname:null,
            'email' => (!is_null($request->femail))?$request->femail:null,
            'mobile' => (!is_null($request->fmobile))?$request->fmobile:null,
            'status' => (!is_null($request->fstatus))?$request->fstatus:null,
            'seller' => 0,
        ];

        $staffs = Staff::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $staffs->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        foreach ($staffs as $staff) {
            $actionBtns = '';

            // Edit Button
            $actionBtns .= ($this->loggedInUser->ref_id != $staff->id)?'<a href="'.route('admin.staff.edit', ['id'=>$staff->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>':'';

            // Delete Button
            $actionBtns .= ($this->loggedInUser->ref_id != $staff->id)?'<a href="javascript:;" del-url="'.route('admin.staff.delete', ['id'=>$staff->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$staff->id.'"/><span></span></label>',
                $staff->name,
                $staff->email,
                $staff->mobile_no,
                ($staff->status==1) ? 'Active' : 'Inactive',
                '<div class="btn-group btn-group-sm btn-group-solid">
                    <div class="">
                    '.$actionBtns.'
                    </div>
                </div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    public function export(Request $request)
    {
        $criteria = (object)[
            'name' => (!is_null($request->fname))?$request->fname:null,
            'email' => (!is_null($request->femail))?$request->femail:null,
            'mobile' => (!is_null($request->fmobile))?$request->fmobile:null,
            'seller' => 0,
        ];
        $staffs = Staff::getExportData($criteria);
        return Excel::download(new StaffExport($staffs),'staffs.xlsx');
    }

    public function create(Request $request)
    {
        if($request->save){
            $validator = Validator::make($request->all(), [
                "name" => "required|max:50",
                "email" => "required|unique:staffs,email",
                "mobile_no" => "required|digits:10",
                "status" => "required",
                "roles" => "required"
            ], []);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
            try {
                $staff = new Staff();
                $staff->name = $request->name;
                $staff->email = $request->email;
                $staff->mobile_no = $request->mobile_no;
                $staff->status = $request->status;
                $staff->seller_id = 0;
                $staff->save();

                $password = 'Fabpik@123';

                $user = new User();
                $user->ref_id = $staff->id;
                $user->user_type = 'admin-staff';
                $user->name = $request->name;
                $user->email = $request->email;
                $user->mobile = $request->mobile_no;
                $user->password = Hash::make($password);
                $user->save();

                $user->syncRoles($request->roles);

                DB::commit();

                return redirect()
                        ->route('admin.staff')
                        ->with(['toast'=>'1','status'=>'success','title'=>'User Management','message'=>'Success! New Staff Added successfully.']);

            } catch (Exception $e) {
                DB::rollback();
                return back()
                        ->with(['toast'=>'1','status'=>'error','title'=>'User Management','message'=>'Error! Some issue occured, please try again later.']);
            }
        }

        $this->data['roles'] = Role::whereNull('seller_id')->get();
        $this->data['title'] = 'New Staff';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.staff',
                'title' => 'Staffs',
            ],
            (object) [
                'url' => false,
                'title' => 'New Staff',
            ],
        ];
        return parent::adminView('staff.addEditStaff', $this->data);
    }

    public function edit(Request $request,$id)
    {
        $this->data['id'] = $id;
        $this->data['staff'] = $staff = Staff::find($id);

        // get user info for this staff
        $user = User::where(['ref_id'=>$staff->id,'user_type'=>'admin-staff'])->first();

        if($request->save) {
            $validator = Validator::make($request->all(), [
                "name" => "required|max:50",
                "email" => "required|unique:staffs,email,".$id.",id",
                "mobile_no" => "required|digits:10",
                "status" => "required",
                "roles" => "required"
            ], []);

            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
            try {
                // update staff details
                $staff->name = $request->name;
                $staff->email = $request->email;
                $staff->mobile_no = $request->mobile_no;
                $staff->status = $request->status;
                $staff->save();

                // update user details
                $user->name = $staff->name;
                $user->email = $staff->email;
                $user->mobile = $staff->mobile_no;
                $user->status = $staff->status;
                $user->save();

                // update user roles
                $user->syncRoles($request->roles);

                DB::commit();

                if ($request->save == 'save') {
                    return redirect()
                            ->route('admin.staff')
                            ->with(['toast'=>'1','status'=>'success','title'=>'User Management','message'=>'Success! Staff data updated successfully.']);
                } else {
                    return redirect()
                            ->back()
                            ->with(['toast'=>'1','status'=>'success','title'=>'User Management','message'=>'Success! Staff data updated successfully.']);
                }
            } catch (Exception $e) {
                DB::rollback();
                return back();
            }
        }

        $this->data['roles'] = Role::whereNull('seller_id')->get();
        $this->data['assignedRoles'] = $user->roles()->pluck('id')->toArray();
        $this->data['title'] = 'Edit Staff';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;

        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.staff',
                'title' => 'Staffs',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['staff']->name,
            ],
        ];

        return parent::adminView('staff.addEditStaff', $this->data);
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            User::where(['ref_id'=>$id,'user_type'=>'admin-staff'])->delete();
            Staff::where('id',$id)->delete();

            DB::commit();
            return response()->json(['success'=>1, 'message'=>""], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }
}
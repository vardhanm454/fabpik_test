<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use Carbon\Carbon;
use DB;

// Models
use App\Models\TaxCodes;
use App\Models\Seller;

// Exports
use App\Exports\CouponExport;
use App\Exports\TaxCoadesExport;
use Illuminate\Support\Facades\Auth;

class TaxController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'tax';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [coupons listing page]
     * @return [type] [description]
     */
    public function index(Request $request)
    {

        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('coupons_perpage', 10);
        $this->data['dt_page'] = Session::get('coupons_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.tax.getTaxAjaxListData');
        $this->data['dt_search_colums'] = ['fcode'];

        $this->data['title'] = 'Tax codes';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Tax codes',
            ]
        ];

        return parent::adminView('tax.index', $this->data);
    }

    public function taxcrud(Request $request)
    {
        ## THIS ONE FUNCTION IS FOR FOUR OPERATIONS [ CRUD ]
        ## CHECK AJAX REQUEST IS COMING OR NOT
        if($request->ajax())
        {
            switch ($request->post('mode'))
            {
                case 'insert':
                case 'update':
                    $data = $request->post();
                    $result = $this->privateInsertUpdateTaxCode($data);
                    return $result;
                break;

                case 'edit':
                    $taxId = htmlspecialchars(strip_tags($request->post('id')));
                    $result = DB::table('taxcodes')->where('id',$taxId)->select('id','code','igst','cgst','sgst')->first();
                    return response()->json(['status' => 200, 'result'=> $result]);
                break;

                case 'delete':
                    $taxId = htmlspecialchars(strip_tags($request->post('id')));
                    $result = DB::table('taxcodes')->where('id',$taxId)->delete();
                    return response()->json(['status' => 200]);
                break;
                
                default:
                return redirect()
                ->route('admin.home')
                ->with(['toast'=>'1','status'=>'error','title'=>'Warning','message'=>'Something Went Wrong.']);
            }
        }

    }

    ############### PRIVATE FUNCTION FOR INSERT EDIT UPADTE DELET #################
    private function privateInsertUpdateTaxCode($data){
      
        if($data['mode'] == 'insert')
        {
            $validator = Validator::make($data, [

                'code' => 'required|unique:taxcodes,code|string|regex:/^[a-zA-Z0-9\s]+$/|min:5|max:10',
                'igst' => 'required|numeric|between:1,99.9',
                'cgst' => 'required|numeric|between:1,99.9',
                'sgst' => 'required|numeric|between:1,99.9',
            ]);

            if($validator->passes())
            {
                
                    $data = array(
                        'code' => htmlspecialchars(strip_tags($data['code'])),
                        'igst' => htmlspecialchars(strip_tags($data['igst'])),
                        'cgst' => htmlspecialchars(strip_tags($data['cgst'])),
                        'sgst' => htmlspecialchars(strip_tags($data['sgst'])),
                        'created_by' => $this->loggedInUser->ref_id,
                        'created_at' => Carbon::now()
                    );

                    DB::table('taxcodes')->insert($data);
                    return response()->json(['status' => 200, 'message'=> 'New TaxCode Added Successfully']);
            }
            else
            {
                return response()->json(['errors' => $validator->errors()]);
            }


        }

        if($data['mode'] == 'update')
        {
            $id = $data['tex_id'];
            $validator = Validator::make($data, [
                'code' => 'required|string|regex:/^[a-zA-Z0-9\s]+$/|min:5|max:10|unique:taxcodes,code,'.$id,
                'igst' => 'required|numeric|between:1,99.9',
                'cgst' => 'required|numeric|between:1,99.9',
                'sgst' => 'required|numeric|between:1,99.9',
            ]);

            if($validator->passes())
            {
                
                    $data = array(
                        'code' => htmlspecialchars(strip_tags($data['code'])),
                        'igst' => htmlspecialchars(strip_tags($data['igst'])),
                        'cgst' => htmlspecialchars(strip_tags($data['cgst'])),
                        'sgst' => htmlspecialchars(strip_tags($data['sgst'])),
                        'updated_by' => $this->loggedInUser->ref_id,
                        'updated_at' => Carbon::now()
                    );

                    DB::table('taxcodes')->where('id', $id)->update($data);
                    return response()->json(['status' => 200, 'message'=> 'TaxCode Updated Successfully']);
            }
            else
            {
                return response()->json(['errors' => $validator->errors()]);
            }


        }
        

        
    }

    /**
     * [get all coupons for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'code',
            2 => 'igst',
            3 => 'cgst',
            4 => 'sgst',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('coupon', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                TaxCodes::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Coupons are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fcode' => (!is_null($request->fcode))?$request->fcode:null,
        ];

        $taxcodes = TaxCodes::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $taxcodes->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));
        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        foreach ($taxcodes as $tax) {

            $actionBtns = ($canChange)?'<div class="">
            <a href="javascript:void(0)" rel="'.$tax->id.'" rel1="edit" class="btn btn-icon-only edit-taxcode default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:void(0)" rel="'.$tax->id.'" rel1="delete" class="btn btn-icon-only delete-taxcode default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>':'';

            //Checking discount type and adding suffix.
            $disType = ($tax->dis_type =='f')?'Rs OFF':'% OFF';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$tax->id.'"/><span></span></label>',
                '<span class="text-uppercase">'.$tax->code.'</span>',
                '<span class="text-uppercase">'.$tax->igst.'</span>',
                '<span class="text-uppercase">'.$tax->cgst.'</span>',
                '<span class="text-uppercase">'.$tax->sgst.'</span>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export coupons]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function taxexport(Request $request)
    {
        $criteria = (object)[
            'fcode' => ($request->fcode)?:null,
        ];
        $taxcodes = TaxCodes::getExportData($criteria);

        return Excel::download(new TaxCoadesExport($taxcodes), 'taxcodes.xlsx');
    }

    /**
     * [add Coupon data]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {
        $this->data['sellers'] = Seller::select('id','name')->where('status',1)->get();
        if ($request->save) {

           $validator = Validator::make($request->all(), [
                'code' => 'required|string',
                'dis_type' => 'required|string',
                'amount'=>'required|integer|gte:1',
                'min_cart_amount'=>'required|integer',
                'max_usage'=>'required|string',
                'expiry_date'=>'required|after_or_equal:start_date',
                'start_date'=>'required',
                'sub_type'=>'required',
                'description'=>'required',
            ]);

            $validator->sometimes('amount', 'lt:min_cart_amount', function($request) {
                return $request->dis_type == 'f';
            });
            
            if($request->dis_type == 'f' && $request->sub_type != 's'){
                $validator->after(function ($validator) {
                    $validator->errors()->add('sub_type', 'Flat Discount Only Applied on Selling Price.');
                });
            }

            if (!empty($request->code)) {
                $isExist = Coupon::where('code', $request->code)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('code', 'Coupon name already exist!');
                    });
                }
            }
            if($request->dis_type == 'p' && $request->amount > 100){
                $validator->after(function ($validator) {
                    $validator->errors()->add('amount', 'Amount shouldn\'t be greater than 100');
                });
            }

            //check the duplicate mobile numbers
            $checkMobileNumbersDuplicates = ($request->mobile_numbers!= null) ? explode( "\n", str_replace( "\r", "", $request->mobile_numbers ) )  : null;
            if(!is_null($checkMobileNumbersDuplicates)){
                if( count($checkMobileNumbersDuplicates) !== count(array_unique($checkMobileNumbersDuplicates)) ){
                    $validator->after(function ($validator) {
                        $validator->errors()->add('mobile_numbers', 'Duplicate mobile numbers, please check');
                    });
                }
    
                foreach($checkMobileNumbersDuplicates as $number){
                    // dd($number);
                    if(!preg_match("/^[6789]\d{9}$/", $number)) {
                        // $phone not  valid
                        $validator->after(function ($validator) {
                            $validator->errors()->add('mobile_numbers', 'Please enter valid mobile numbers, check once!');
                        });
                        break;
                    }
                }
            }

            if ($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            try {
                $coupons = new Coupon();
                $coupons->code = $request->code;
                $coupons->dis_type = $request->dis_type;
                $coupons->amount = $request->amount;
                $coupons->min_cart_amount = $request->min_cart_amount;
                $coupons->max_usage = $request->max_usage;
                $coupons->start_date = date('Y-m-d H:i:s', strtotime($request->start_date));
                $coupons->expiry_date = date('Y-m-d H:i:s', strtotime($request->expiry_date));
                $coupons->sub_type = $request->sub_type;
                $coupons->description = $request->description;
                $coupons->seller_ids = ($request->fseller!= null)?json_encode($request->fseller):null;
                $coupons->category_ids = ($request->fcategory!= null)?json_encode($request->fcategory):null;
                $coupons->product_varient_ids = ($request->fprovariants!= null)?json_encode($request->fprovariants):null;
                $coupons->mobile_numbers =  ($checkMobileNumbersDuplicates!=null) ? json_encode($checkMobileNumbersDuplicates) : null;
                $coupons->coupon_group_id = $request->coupon_groups;
                
                if ($coupons->save()) {
                    return redirect()
                        ->route('admin.coupons')
                        ->with(['toast'=>'1','status'=>'success','title'=>'Coupons','message'=>'Success! New Coupon added successfully.']);
                } else {
                    return redirect()
                        ->route('admin.coupons')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Coupons','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                return redirect()
                        ->route('admin.coupons')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Coupons','message'=>'Error! Some error occured, please try again.']);
            }
        }
        
        $this->data['title'] = 'Add Coupon';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.coupons',
                'title' => 'Coupons',
            ]
        ];
        return parent::adminView('coupon.addEditCoupons', $this->data);
    }

    /**
     * [edit Coupon data]
     * @param  Request $request [description]
     * @param  [type]  $id      [brand id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $coupon = Coupon::find($id);
        $this->data['sellers'] = Seller::select('id','name')->where('status',1)->get();
        if ($request->save) {
          
            $validator = Validator::make(
                $request->all(),
                [
                    'code' => 'required|string',
                    'dis_type' => 'required|string',
                    'amount'=>'required|integer|gte:1',
                    'min_cart_amount'=>'required|integer',
                    'max_usage'=>'required|string',
                    'start_date'=>'required',
                    'expiry_date'=>'required|after_or_equal:start_date',
                    'sub_type'=>'required',
                    'status' => 'required',
                    'description' => 'required',
                ]
            );
            
            $validator->sometimes('amount', 'lt:min_cart_amount', function($request) {
                return $request->dis_type == 'f';
            });

            if (!empty($request->code)) {
                $isExist = Coupon::where('code', $request->code)->where('id', '<>', $id)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('code', 'Coupon name already exist!');
                    });
                }
            }

            if($request->dis_type == 'f' && $request->sub_type != 's'){
                $validator->after(function ($validator) {
                    $validator->errors()->add('sub_type', 'Flat Discount Only Applied on Selling Price.');
                });
            }

            if($request->dis_type == 'p' && $request->amount > 100){
                $validator->after(function ($validator) {
                    $validator->errors()->add('amount', 'Amount shouldn\'t be greater than 100');
                });
            }

            //check the duplicate mobile numbers
            $checkMobileNumbersDuplicates = ($request->mobile_numbers!= null) ? explode( "\n", str_replace( "\r", "", $request->mobile_numbers ) )  : null;
            if (!is_null($checkMobileNumbersDuplicates)) {
                if (count($checkMobileNumbersDuplicates) !== count(array_unique($checkMobileNumbersDuplicates))) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('mobile_numbers', 'Duplicate mobile numbers, please check');
                    });
                }

                foreach ($checkMobileNumbersDuplicates as $number) {
                    // dd($number);
                    if (!preg_match("/^[6789]\d{9}$/", $number)) {
                        // $phone not  valid
                        $validator->after(function ($validator) {
                            $validator->errors()->add('mobile_numbers', 'Please enter valid mobile numbers, check once!');
                        });
                        break;
                    }
                }
            }

            if ($validator->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            try {

                $tblData = [
                    'code' => $request->code,
                    'dis_type' => $request->dis_type,
                    'amount' => $request->amount,
                    'min_cart_amount' => $request->min_cart_amount,
                    'max_usage' => $request->max_usage,
                    'start_date' => date('Y-m-d H:i:s', strtotime($request->start_date)),
                    'expiry_date' => date('Y-m-d H:i:s', strtotime($request->expiry_date)),
                    'sub_type' => $request->sub_type,
                    'status' => $request->status,
                    'description' => $request->description,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'seller_ids' => ($request->fseller!= null)?json_encode($request->fseller):null,
                    'category_ids' => ($request->fcategory!= null)?json_encode($request->fcategory):null,
                    'product_varient_ids' => ($request->fprovariants!= null)?json_encode($request->fprovariants):null,
                    // 'mobile_numbers' => ($request->mobile_numbers!= null)?json_encode($request->mobile_numbers):null,
                    "mobile_numbers" =>  ($checkMobileNumbersDuplicates!=null) ? json_encode($checkMobileNumbersDuplicates) : null,
                    'coupon_group_id' => $request->coupon_groups,
                ];

                $update = Coupon::where('id', $id)->update($tblData);

                if ($update) {
                    if ($request->save == 'save') {
                        return redirect()
                                ->route('admin.coupons')
                                ->with(['toast'=>'1','status'=>'success','title'=>'Coupons','message'=>'Success! Coupon data updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Coupons','message'=>'Success! Coupon data updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.coupons')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Coupons','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                return redirect()
                        ->route('admin.coupons')
                        ->with(['toast'=>'1','status'=>'error','title'=>'Coupons','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['coupon'] = $coupon;
        $this->data['id'] = $id;
        
        $this->data['title'] = 'Edit Coupon';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.coupons',
                'title' => 'Coupons',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['coupon']->code,
            ],
        ];

        $this->data['editcoupon'] = $coupon;
        $this->data['id'] = $id;

        return parent::adminView('coupon.addEditCoupons', $this->data);
    }

    /**
     * [Delete Single Coupon data]
     * @param  Request $request [description]
     * @param  [type]  $id      [coupon id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            $delete = Coupon::where('id', $id)->delete();
            if($delete){
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }
}

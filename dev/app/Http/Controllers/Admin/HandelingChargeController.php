<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use DB;
use Session;
use Validator;
use Excel;

// Models
use App\Models\Handelingcharge;

// Exports

class HandelingChargeController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'system';
    public $activeSubmenu = 'handlingcharges';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [handlingcharges listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('handlingcharges_perpage', 10);
        $this->data['dt_page'] = Session::get('handlingcharges_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.handelingcharges.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Handling Charges.';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'System',
            ],
            (object) [
                'url' => false,
                'title' => 'Handling Charges.',
            ]
        ];
        $this->data['formtitle'] = 'Add Handling Charges';
        //get all shippingcharges for datatable
        $handelingcharge = Handelingcharge::get();
        $this->data['handelingcharge'] = $handelingcharge;
        return parent::adminView('handelingcharges.addEditHadelingCharges', $this->data);
    }

    /**
     * [get all handelingcharge for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'min_amount',
            2 => 'updated_at'
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('shippingcharges', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Shipingcharge::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Charges are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }
        
        $criteria = (object)[
            'length' => intval($request->length),
            'fstatus' => ($request->fstatus)?:null,
        ];
        $shippingcharges = Shipingcharge::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $shippingcharges->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $statusList = [
            ["danger" => "Inactive"],
            ["primary" => "Active"]
        ];

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        foreach ($shippingcharges as $shippingcharge) {
            $status = $statusList[$shippingcharge->status];

            $actionBtns = ($canChange)?'<a href="'.route('admin.shippingcharges.edit', ['id'=>$shippingcharge->id]).'" class="btn btn-icon-only default btn-circle tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('admin.shippingcharges.delete', ['id'=>$shippingcharge->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$shippingcharge->id.'"/><span></span></label>',
                '',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [add new shipping charges]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function add(Request $request)
    {
        $this->data['title'] = 'Handling Charges.';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'System',
            ],
            (object) [
            'url' => false,
                'title' => 'Handling Charges.',
            ]
        ];
        $this->data['formtitle'] = 'Add Handling Charges.';

        $validator = Validator::make($request->all(), [
            'min_amount' => 'required|integer',
            'max_amount' => 'required|integer|gt:min_amount',
            'charge_amount'=>'required|integer',
        ]);

        $isExist = Handelingcharge::where('min_amount', $request->min_amount)
                            ->where('max_amount', $request->max_amount)->exists();
            if ($isExist) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('min_amount', 'Handling Charges already exist!');
                });
            }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $tblData = new Handelingcharge();
        $tblData->min_amount = $request->min_amount;
        $tblData->max_amount = $request->max_amount;
        $tblData->charge_amount = $request->charge_amount;
        if ($tblData->save()) {
            return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'success','title'=>'Handling Charges.','message'=>'Success! Handling Charges. added successfully.']);
        } else {
            return redirect()
                    ->back()
                    ->with(['toast'=>'1','status'=>'success','title'=>'Handling Charges.','message'=>'Error! Some error occured, please try again.']);
        }
    }

    /**
     * [edit Shipping Charges  data]
     * @param  Request $request [description]
     * @param  [type]  $id      [brand id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        //get all shippingcharges for edit
        $edithandelingCharges = Handelingcharge::where('id', $id)->first();
        //get all shippingcharges for data table
        $handelingcharge = Handelingcharge::get();

        $this->data['title'] = 'Hadeling Charges';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'javascript:;',
                'title' => 'System',
            ],
            (object) [
            'url' => false,
                'title' => 'Shipping Charges',
            ]
        ];
        $this->data['formtitle'] = 'Edit Handling Charges';
        $this->data['edithandlingcharges'] = $edithandlingcharges;
        $this->data['handelingcharge'] = $handelingcharge;
        
        $this->data['id'] = $id;

        if ($request->save) {
            $validator = Validator::make($request->all(), [
            'min_amount' => 'required|integer',
            'max_amount' => 'required|integer|gt:min_amount',
            'charge_amount'=>'required|integer',
            ]);

            $isExist = Handelingcharge::where('min_amount', $request->min_amount)
                            ->where('max_amount', $request->max_amount)->where('id', '<>', $id)->exists();
            if ($isExist) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('min_amount', 'Shipping Charges already exist!');
                });
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $tblData =[
                'min_amount' => $request->min_amount,
                'max_amount' => $request->max_amount,
                'charge_amount' => $request->charge_amount,
            ];
            
            $update = Handelingcharge::where('id', $id)->update($tblData);
            if ($update) {
                return redirect()
                    ->route('admin.handlingcharges')
                    ->with(['toast'=>'1','status'=>'success','title'=>'Shipping Charges','message'=>'Success! Shipping Charges Edited successfully.']);
            } else {
                return redirect()
                    ->route('admin.handlingcharges')
                    ->with(['toast'=>'1','status'=>'success','title'=>'Shipping Charges','message'=>'Error! Some error occured, please try again.']);
            }
        }
        return parent::adminView('handelingcharges.addEditHadelingCharges', $this->data);
    }

    /**
     * [Delete Handeling data]
     * @param  Request $request [description]
     * @param  [type]  $id      [category id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            $delete = Handelingcharge::where('id', $id)->delete();
            if($delete){
                return redirect()
                ->route('admin.handlingcharges')
                ->with(['toast'=>'1','status'=>'success','title'=>'Hadeling Charges','message'=>'Success! Handling Charges. Deleted successfully.']);
            }
            return redirect()
            ->route('admin.handlingcharges')
            ->with(['toast'=>'1','status'=>'success','title'=>'Hadeling Charges','message'=>'Error! Some error occured, please try again.']);
        } catch (Exception $e) {
            return redirect()
                    ->route('admin.handlingcharges')
                    ->with(['toast'=>'1','status'=>'success','title'=>'Hadeling Charges','message'=>'Error! Some error occured, please try again.']);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Validator;
use Session;
use Excel;
use DB;

// Models
use App\Models\Sizechart;
use App\Models\Seller;

// Exports
use App\Exports\SizechartExport;

class SizeChartController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'prod-mgmt';
    public $activeSubmenu = 'sizecharts';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [sizecharts listing page]
     * @return [type] [description]
    */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('sizechart_perpage', 10);
        $this->data['dt_page'] = Session::get('sizechart_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4';
        $this->data['dt_ajax_url'] = route('admin.sizecharts.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus', 'fseller'];

        $this->data['title'] = 'Size Charts';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Product Management',
            ],
            (object) [
                'url' => false,
                'title' => 'Size Charts',
            ],
        ];
        $this->data['sellers'] = Seller::get();
        return parent::adminView('sizecharts.index', $this->data);
    }

    /**
     * [get all Size Charts for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'image',
            2 => 'name',
            3 => 'seller_id',
            4 => 'created_at',
            5 => 'updated_at',
            6 => 'updated_at',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('sizechart', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        if ($request->input('customActionName') == "delete") {
            DB::beginTransaction();
            try {
                Sizechart::whereIn('id', $request->input('id'))->delete();

                DB::commit();
                $records["customActionMessage"] = 'Selected Size Charts are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null,
            'fseller' => (!is_null($request->fseller))?$request->fseller:null,
        ];
        $sizecharts = Sizechart::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $sizecharts->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        foreach ($sizecharts as $sizechart) {
            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.sizecharts.edit', ['id'=>$sizechart->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('admin.sizecharts.delete', ['id'=>$sizechart->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a></div>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$sizechart->id.'"/><span></span></label>',
                '<img src="'.route('ajax.previewImage', ['image'=>$sizechart->image,'type'=>'brand']).'" class="sizechart-thumbnail-img" alt="'.$sizechart->name.'" height="75" width="75"/>',
                $sizechart->name,
                isset($sizechart->seller)?$sizechart->seller->name:'',
                date('d M Y', strtotime($sizechart->created_at)),
                date('d M Y', strtotime($sizechart->updated_at)),
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [export brands]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function export(Request $request)
    {
        $criteria = (object)[
            'fname' => ($request->fname)?:null,
            'fstatus' => ($request->fstatus)?:null,
        ];
        $sizecharts = Sizechart::getExportData($criteria);

        return Excel::download(new SizechartExport($sizecharts), 'Sizecharts.xlsx');
    }

    /**
     * [add new sizechart]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function add(Request $request)
    {
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'image' => 'required',
                'seller_id'=>'required|integer',
                'description'=>'required|string|max:255',
            ]);

            if(!empty($request->name)) {
                $isExist = Sizechart::where('name', $request->name)
                                        ->where('seller_id',$request->seller_id)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Size Chart name already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                $tblData = new Sizechart();
                $tblData->name = $request->name;
                $tblData->image = $request->image;
                $tblData->seller_id = $request->seller_id;
                $tblData->description = $request->description;
                $tblData->updated_at = date('Y-m-d H:i:s');
                if ($request->is_default) {
                    Sizechart::where('seller_id', $request->seller_id)->update(['is_default' => 0]);
                    $tblData->is_default = intval($request->is_default);
                }
                
                if ($tblData->save()) {
                    return redirect()
                        ->route('admin.sizecharts')
                        ->with(['toast'=>'1','status'=>'success','title'=>'Size Charts','message'=>'Success! Size Chart added successfully.']);
                } else {
                    return redirect()
                            ->route('admin.sizecharts')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Size Charts','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                // dd($e);
                return redirect()
                            ->route('admin.sizecharts')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Size Charts','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['sellers'] = Seller::get();

        $this->data['title'] = 'Add New Size Chart';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.sizecharts',
                'title' => 'Size Chart',
            ]
        ];

        return parent::adminView('sizecharts.addEditSizechart', $this->data);
    }

    /**
     * [Existing Edit sizechart]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function edit(Request $request, $id)
    {
        $sizechart = Sizechart::find($id);
        if ($request->save) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|alpha',
                'image' => 'required',
                'seller_id'=>'required|integer',
                'description'=>'required|max:255',
            ]);

            if(!empty($request->name)) {
                $isExist = Sizechart::where('name', $request->name)->where('id', '<>', $id)
                                        ->where('seller_id',$request->seller_id)->exists();
                if ($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('name', 'Size Chart name already exist!');
                    });
                }
            }

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            try {
                $tblData = [
                    'name' => $request->name,
                    'image' => $request->image,
                    'seller_id' => $request->seller_id,
                    'description' => $request->description,
                    'is_default' => $request->is_default,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                if ($request->is_default) {
                    Sizechart::where('seller_id', $request->seller_id)->update(['is_default' => 0]);
                    $tblData['is_default'] = intval($request->is_default);
                }else{
                    $tblData['is_default'] = 0;
                }

                $update = Sizechart::where('id', $id)->update($tblData);
                
                if ($update) {
                    if ($request->save == 'save') {
                        return redirect()
                        ->route('admin.sizecharts')
                        ->with(['toast'=>'1','status'=>'success','title'=>'Size Charts','message'=>'Success! Size Chart Updated successfully.']);
                    } else {
                        return redirect()
                                ->back()
                                ->with(['toast'=>'1','status'=>'success','title'=>'Size Charts','message'=>'Success! Size Chart data updated successfully.']);
                    }
                } else {
                    return redirect()
                            ->route('admin.sizecharts')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Size Charts','message'=>'Error! Some error occured, please try again.']);
                }
            } catch (Exception $e) {
                // dd($e);
                return redirect()
                            ->route('admin.sizecharts')
                            ->with(['toast'=>'1','status'=>'error','title'=>'Size Charts','message'=>'Error! Some error occured, please try again.']);
            }
        }

        $this->data['sellers'] = Seller::get();

        $this->data['sizechart'] = $sizechart;
        $this->data['id'] = $id;

        $this->data['title'] = 'Edit Size Chart';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => '',
                'title' => 'Product Management',
            ],
            (object) [
                'url' => 'admin.sizecharts',
                'title' => 'Size Chart',
            ],
            (object) [
                'url' => false,
                'title' => $this->data['sizechart']->name,
            ],
        ];

        return parent::adminView('sizecharts.addEditSizechart', $this->data);
    }

    /**
     * [Delete sizechart]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $delete = Sizechart::where('id', $id)->delete();
            DB::commit();
            if ($delete) {
                return response()->json(['success'=>1, 'message'=>""], 200);
            }
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>2, 'message'=>"Error! Some error occured, please try again."], 200);
        }
    }
}

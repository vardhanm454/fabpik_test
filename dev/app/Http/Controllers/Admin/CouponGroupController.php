<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Session;
use Excel;
use Validator;
use DB;

// Models
use App\Models\Coupon;
use App\Models\CouponGroup;
use App\Models\Seller;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\ChildCategory;
use App\Models\ProductVariant;

// Exports
use App\Exports\CouponExport;

class CouponGroupController extends CoreController
{
    public $loggedInUser;
    public $data;
    public $activeMenu = 'coupongroup';
    public $activeSubmenu = '';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            // get current logged in user
            $this->loggedInUser = auth()->user();

            return $next($request);
        });
    }

    /**
     * [coupons listing page]
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('coupons_perpage', 10);
        $this->data['dt_page'] = Session::get('coupons_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,5,6';
        $this->data['dt_ajax_url'] = route('admin.coupongroups.getAjaxListData');
        $this->data['dt_search_colums'] = ['fname','fstatus'];

        $this->data['title'] = 'Coupon Groups';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => false,
                'title' => 'Coupon Groups',
            ]
        ];

        return parent::adminView('coupongroups.index', $this->data);
    }

    /**
     * [get all coupons for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getAjaxListData(Request $request)
    {
        $columnList = [
            0 => 'id',
            1 => 'group_name'
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('coupon', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }
        if ($request->input('customActionName') == "delete") {

            DB::beginTransaction();
            try {
                $recordCount = Coupon::whereIn('coupon_group_id',  $request->input('id'))->count();

                if($recordCount > 0){
                    $records["customActionStatus"] = "NOT-OK";
                    $records["customActionMessage"] = 'Groups are already assigned to coupon.';
                }else{
                    CouponGroup::whereIn('id', $request->input('id'))->delete();

                    DB::commit();
                    $records["customActionMessage"] = 'Selected Coupon Groups are deleted successfully.';
                }
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }

        $criteria = (object)[
            'length' => intval($request->length),
            'fname' => ($request->fname)?:null,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
        ];

        $coupons = CouponGroup::getAjaxListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords = $coupons->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));
        $statusList = [
            ["0" => "Inactive"],
            ["1" => "Active"]
        ];

        foreach ($coupons as $coupon) {


            $actionBtns = ($canChange)?'<div class="">
            <a href="'.route('admin.coupongroups.edit', ['id'=>$coupon->id]).'" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
            <a href="javascript:;" del-url="'.route('admin.coupongroups.delete', ['id'=>$coupon->id]).'" class="btn btn-icon-only default btn-circle dt-list-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>':'';

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$coupon->id.'"/><span></span></label>',
                '<span class="text-uppercase">'.$coupon->group_name.'</span>',
                '<span class="desc">'.$coupon->description.'</span>',
                $coupon->user->name,
                ($coupon->status == 1)?'Active' : 'Inactive',
                '<a href="'.route('admin.coupons').'?search=1&freference='.$coupon->id.'" class="" data-toggle="tooltip" data-placement="top" data-original-title="References" title="References" target="_blank">References</a>',
                '<div class="btn-group btn-group-sm btn-group-solid">'.
                    $actionBtns.
                '</div>',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);
    }

    /**
     * [add Coupon Group Name]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function add(Request $request)
    {

        if($request->save){

            $validator = Validator::make($request->all(),[
                'group_name' => 'required',
                'description' => 'required|max:255',
            ],[
                'group_name.required' => 'Group name field is required.',
                'description.required' => 'Description field is required.',
            ]);

            $isExist =  CouponGroup::where('group_name', $request->group_name)->exists();

            if ($isExist) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('group_name', 'Group name already exist!');
                });
            }

            if($validator->fails()){
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            try {
                DB::beginTransaction();

                $coupongroup = new CouponGroup();
                $coupongroup->group_name = $request->group_name;
                $coupongroup->description = $request->description;
                $coupongroup->created_by = $this->loggedInUser->id;

                if ($coupongroup->save()) {
                    DB::commit();
                    if ($request->save == 'save') {
                        return redirect()
                            ->back();
                    }
                    else{
                        return redirect()
                            ->route('admin.coupongroups.selectVariants', ['id'=>$coupongroup->id]);
                    }
                }
            } catch (Exception $e){
                DB::rollback();
                return redirect()
                        ->back('admin.coupongroups.addGroupName');
            }

        }

        $this->data['title'] = 'Add Coupon Group';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.coupongroups',
                'title' => 'Coupon Groups',
            ]
        ];

        return parent::adminView('coupongroups.addCouponGroup', $this->data);
    }

    /**
     * [add Coupon Group Name]
     * @param  Request $request [description]
     * @return [type]           [description]
    */

    public function selectVariants($groupId)
    {

        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('coupons_perpage', 10);
        $this->data['dt_page'] = Session::get('coupons_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,5';
        $this->data['dt_ajax_url'] = route('admin.coupongroups.getAjaxCouponGroupProductVariantListData', ['id'=>$groupId]);
        $this->data['dt_search_colums'] = ['fpricefrom', 'fpriceto', 'fpdiscountfrom', 'fpdiscountto', 'fdiscountfrom', 'fdiscountto', 'fname', 'fbrand', 'fcategory', 'fsubcategory', 'fchildcategory', 'fcompanyname', 'fseller', 'fspricefrom', 'fspriceto', 'fsdiscountfrom', 'fsdiscountto', 'fspdiscountfrom', 'fspdiscountto'];

        $this->data['title'] = 'Select Product Variants';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.coupongroups',
                'title' => 'Coupon Groups',
            ],
            (object) [
                'url' => false,
                'title' => 'Select Product Variants',
            ]
        ];

        $this->data['groupId'] = $groupId;

        return parent::adminView('coupongroups.selectVariants', $this->data);
    }

    /**
     * [get all Product Variants for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxCouponGroupProductVariantListData(Request $request, $groupId){

            $columnList = [
                0 => 'id',
                1 => 'unique_id',
                2 => 'name',
                3 => 'id',
                4 => 'sku',
                5 => 'mrp',
                6 => 'sell_price',
                7 => 'created_at',
                8 => 'updated_at',
                9 => 'status',
            ];

            $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
            $orderColumn = $columnList[$order['column']];
            $orderDir = $order['dir'];

            $iPage = (intval($request->start) / intval($request->length)) + 1;

            __setDatatableCurrPage('coupon', intval($request->length), $iPage);

            $records = [];
            $records["data"] = [];

            if (isset($request->customActionType)
                && $request->customActionType == "group_action") {
                $records["customActionStatus"] = "OK";
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
            }

            //get the already saved product variant Ids
            $prodVariantIds = CouponGroup::where('id', $groupId)->pluck('product_varient_ids');
            $prodIds = (!is_null($prodVariantIds)) ? json_decode($prodVariantIds[0]) : null;

            $criteria = (object)[
                'length' => intval($request->length),
                'fpricefrom' => (!is_null($request->fpricefrom)) ? $request->fpricefrom : null,
                'fpriceto' => (!is_null($request->fpriceto)) ? $request->fpriceto : null,
                'fpdiscountfrom' => (!is_null($request->fpdiscountfrom)) ?$request->fpdiscountfrom : null,
                'fpdiscountto' => (!is_null($request->fpdiscountto)) ? $request->fpdiscountto : null,
                'fdiscountfrom' => (!is_null($request->fdiscountfrom)) ? $request->fdiscountfrom : null,
                'fdiscountto' => (!is_null($request->fdiscountto)) ? $request->fdiscountto : null,

                'fspricefrom' => (!is_null($request->fspricefrom)) ? $request->fspricefrom : null,
                'fspriceto' => (!is_null($request->fspriceto)) ? $request->fspriceto : null,
                'fspdiscountfrom' => (!is_null($request->fspdiscountfrom)) ?$request->fspdiscountfrom : null,
                'fspdiscountto' => (!is_null($request->fspdiscountto)) ? $request->fspdiscountto : null,
                'fsdiscountfrom' => (!is_null($request->fsdiscountfrom)) ? $request->fsdiscountfrom : null,
                'fsdiscountto' => (!is_null($request->fsdiscountto)) ? $request->fsdiscountto : null,

                'fname' => (!is_null($request->fname)) ? $request->fname : null,
                'fbrand' => (!is_null($request->fbrand)) ? $request->fbrand : null,
                'fcategory' => (!is_null($request->fcategory)) ? $request->fcategory : null,
                'fsubcategory' => (!is_null($request->fsubcategory)) ? $request->fsubcategory : null,
                'fchildcategory' => (!is_null($request->fchildcategory)) ? $request->fchildcategory : null,
                'fcompanyname' => (!is_null($request->fcompanyname)) ? $request->fcompanyname : null,
                'fseller' => (!is_null($request->fseller)) ? $request->fseller : null,

                'fselectedprodvariant' => (!is_null($prodIds)) ? $prodIds : null,
            ];

            $prodVariants = ProductVariant::getAjaxCouponGroupProductVariantListData($criteria, $iPage, $orderColumn, $orderDir);

            $iTotalRecords =  $prodVariants->total();

            $iDisplayLength = intval($request->length);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($request->start);
            $sEcho = intval($request->draw);

            $end = $iDisplayStart + $iDisplayLength;

            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

            foreach ($prodVariants as $index=>$prodVariant) {

                $variants = __getVariantOptionValues($prodVariant->id);

                $records["data"][] = [
                    '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes checkBoxClass checkbox1" value="'.$prodVariant->id.'"/><span></span></label>',

                    '<a href="'.route('admin.products.variantEdit', ['id'=>$prodVariant->product->id, 'vid'=>$prodVariant->id]).'" target="_blank" class="" title="View Order Details">'.$prodVariant->unique_id.'</a>',

                    '<span class=""><a href="https://fabpik.in/products/'.$prodVariant->slug.'/'.$prodVariant->unique_id.'" target="_blank" class="" title="View Product Details">'.($prodVariant->name).'</a></span>',

                    '<img class="zoom" src="'.url('/').'/'.UPLOAD_PATH.'/'.__getVariantImages($prodVariant->id).'" width="50" height="">',

                    $prodVariant->product->category->title,
                    $prodVariant->sku,
                    $prodVariant->mrp,
                    $prodVariant->price,
                    number_format((float)($prodVariant->discount), '2'),
                    $prodVariant->fabpik_seller_price,
                    number_format((float)($prodVariant->fabpik_seller_discount), '1'),
                    // number_format((float)($prodVariant->fabpik_seller_discount/$prodVariant->mrp)*100, '1'),
                    number_format((float) ($prodVariant->fabpik_seller_discount_percentage), '1' ),
                    (!is_null($prodVariant->special_price_start_date)) ? date('d M Y h:i:s A', strtotime($prodVariant->special_price_start_date)) : '-',
                    (!is_null($prodVariant->special_price_end_date)) ? date('d M Y h:i:s A', strtotime($prodVariant->special_price_end_date)) : '-',
                    $variants['PrimaryAttrValue'],
                    $variants['SecondaryAttrValue'],
                    date('d M Y', strtotime($prodVariant->created_at)),
                    date('d M Y', strtotime($prodVariant->updated_at)),
                    '',
                ];
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            return response()->json($records);

    }

    /**
     * [add Coupon data]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function storeGroup(Request $request)
    {

        if($request->savecontinue){
            if (!$request->checkall && !isset($request->id)) {
                return redirect()
                    ->route('admin.coupongroups.finalVariants', ['id'=>$request->groupId]);
            }
        }

        $validator = Validator::make($request->all(), [], []);

        if (!isset($request->id)) {
            $validator->after(function ($validator) {
                $validator->errors()->add('variantCheckbox', 'Please select atleast one product !!!');
            });
        }

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        try {

            $prodVariantIds = CouponGroup::where('id', $request->groupId)->pluck('product_varient_ids');
            $prodIds = json_decode($prodVariantIds[0]);

            $allVariantIds = [];
            if($request->checkall){

                $criteria = (object)[
                    'fseller' => (!is_null($request->fseller_hidden)) ? $request->fseller_hidden : null,
                    'fbrand' => (!is_null($request->fbrand_hidden)) ? $request->fbrand_hidden : null,
                    'fname' => (!is_null($request->fname_hidden)) ? $request->fname_hidden : null,
                    'fpricefrom' => (!is_null($request->fpricefrom_hidden)) ? $request->fpricefrom_hidden : null,
                    'fpriceto' => (!is_null($request->fpriceto_hidden)) ? $request->fpriceto_hidden : null,
                    'fdiscountfrom' => (!is_null($request->fdiscountfrom_hidden)) ?$request->fdiscountfrom_hidden : null,
                    'fdiscountto' => (!is_null($request->fdiscountto_hidden)) ? $request->fdiscountto_hidden : null,
                    'fpdiscountfrom' => (!is_null($request->fpdiscountfrom_hidden)) ? $request->fpdiscountfrom_hidden : null,
                    'fpdiscountto' => (!is_null($request->fpdiscountto_hidden)) ? $request->fpdiscountto_hidden : null,
                    'fcategory' => (!is_null($request->fcategory_hidden)) ? $request->fcategory_hidden : null,
                    'fsubcategory' => (!is_null($request->fsubcategory_hidden)) ? $request->fsubcategory_hidden : null,
                    'fchildcategory' => (!is_null($request->fchildcategory_hidden)) ? $request->fchildcategory_hidden : null,
                    'prodIds' => !is_null($prodIds) ? $prodIds : null,

                    'fspricefrom' => (!is_null($request->fspricefrom_hidden)) ? $request->fspricefrom_hidden : null,
                    'fspriceto' => (!is_null($request->fspriceto_hidden)) ? $request->fspriceto_hidden : null,
                    'fspdiscountfrom' => (!is_null($request->fspdiscountfrom_hidden)) ?$request->fspdiscountfrom_hidden : null,
                    'fspdiscountto' => (!is_null($request->fspdiscountto_hidden)) ? $request->fspdiscountto_hidden : null,
                    'fsdiscountfrom' => (!is_null($request->fsdiscountfrom_hidden)) ? $request->fsdiscountfrom_hidden : null,
                    'fsdiscountto' => (!is_null($request->fsdiscountto_hidden)) ? $request->fsdiscountto_hidden : null,
                ];

                $productVariantData = ProductVariant::getSelectAllProductVariantListData($criteria);

                foreach($productVariantData  as $variantData){
                    $allVariantIds[] = strval($variantData->id);
                }

            }

            if( !empty($allVariantIds) ){
                $finalIds = $allVariantIds;
            }else{
                $finalIds = $request->id;
            }

            if(!is_null($prodIds))
                $finalArray = array_merge($prodIds, $finalIds);
            else
                $finalArray = $finalIds;


            $updateGroup = CouponGroup::where('id', $request->groupId)->update([
                                                                        'product_varient_ids' => ($finalArray),
                                                                        'created_by' => $this->loggedInUser->id
                                                                    ]);

            if ($updateGroup) {
                if($request->save == 'prodvarsave'){
                    return redirect()
                        ->back()
                        ->with(['toast'=>'1','status'=>'success','title'=>'Coupon Group','message'=>'Success! New Coupon Group added successfully.']);
                }else{
                    return redirect()
                        ->route('admin.coupongroups.finalVariants', ['id'=>$request->groupId])
                        ->with(['toast'=>'1','status'=>'success','title'=>'Coupon Group','message'=>'Success! New Coupon Group added successfully.']);
                }
            } else {
                return redirect()
                    ->route('admin.coupongroups')
                    ->with(['toast'=>'1','status'=>'error','title'=>'Coupon Group','message'=>'Error! Some error occured, please try again.']);
            }
        } catch (Exception $e) {
            return redirect()
                    ->route('admin.coupongroups')
                    ->with(['toast'=>'1','status'=>'error','title'=>'Coupon Group','message'=>'Error! Some error occured, please try again.']);
        }
    }

    /**
     * [add Coupon Final Group variants]
     * @param  Request $request [description]
     * @return [type]           [description]
    */

    public function finalVariants($groupId){

        $this->data['datatable_listing'] = true;
        $this->data['dt_ordering'] = 1;
        $this->data['dt_perpage'] = Session::get('coupons_perpage', 10);
        $this->data['dt_page'] = Session::get('coupons_page', 1);
        $this->data['dt_tools_columns'] = '';
        $this->data['dt_center_columns'] = '2,3,4,5';
        $this->data['dt_ajax_url'] = route('admin.coupongroups.getAjaxCouponGroupProductVariantFinalListData', ['id'=>$groupId]);
        $this->data['dt_search_colums'] = ['fpricefrom', 'fpriceto', 'fpdiscountfrom', 'fpdiscountto', 'fdiscountfrom', 'fdiscountto', 'fname', 'fcategories', 'fbrand', 'fcategory', 'fsubcategory', 'fchildcategory', 'fcompanyname', 'fseller', 'fspricefrom', 'fspriceto', 'fsdiscountfrom', 'fsdiscountto', 'fspdiscountfrom', 'fspdiscountto'];

        $this->data['title'] = 'Select Product Variants';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.coupongroups',
                'title' => 'Coupon Groups',
            ],
            (object) [
                'url' => false,
                'title' => 'Final Review',
            ]
        ];

        $this->data['groupId'] = $groupId;

        return parent::adminView('coupongroups.finalVariants', $this->data);
    }

    /**
     * [get all Product Variants for datatable through Ajax]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function getAjaxCouponGroupProductVariantFinalListData(Request $request, $groupId){

        $columnList = [
            0 => 'id',
            1 => 'unique_id',
            2 => 'name',
            3 => 'id',
            4 => 'sku',
            5 => 'mrp',
            6 => 'sell_price',
            7 => 'created_at',
            8 => 'updated_at',
            9 => 'status',
        ];

        $order = (isset($_REQUEST['order']))?$_REQUEST['order'][0]:['column'=>1, 'dir'=>'desc'];
        $orderColumn = $columnList[$order['column']];
        $orderDir = $order['dir'];

        $iPage = (intval($request->start) / intval($request->length)) + 1;

        __setDatatableCurrPage('coupon', intval($request->length), $iPage);

        $records = [];
        $records["data"] = [];

        if (isset($request->customActionType)
            && $request->customActionType == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        if ($request->input('customActionName') == "delete-coupon-group") {
            DB::beginTransaction();
            try {
                $allVariantIds = [];
                if($request->checkall == "true"){

                    $deletefilter = (object)[
                        'fseller' => (!is_null($request->fseller)) ? $request->fseller : null,
                        'fbrand' => (!is_null($request->fbrand)) ? $request->fbrand : null,
                        'fname' => (!is_null($request->fname)) ? $request->fname : null,
                        'fpricefrom' => (!is_null($request->fpricefrom)) ? $request->fpricefrom : null,
                        'fpriceto' => (!is_null($request->fpriceto)) ? $request->fpriceto : null,
                        'fdiscountfrom' => (!is_null($request->fdiscountfrom)) ?$request->fdiscountfrom : null,
                        'fdiscountto' => (!is_null($request->fdiscountto)) ? $request->fdiscountto : null,
                        'fpdiscountfrom' => (!is_null($request->fpdiscountfrom)) ? $request->fpdiscountfrom : null,
                        'fpdiscountto' => (!is_null($request->fpdiscountto)) ? $request->fpdiscountto : null,
                        'fcategory' => (!is_null($request->fcategory)) ? $request->fcategory : null,
                        'fsubcategory' => (!is_null($request->fsubcategory)) ? $request->fsubcategory : null,
                        'fchildcategory' => (!is_null($request->fchildcategory)) ? $request->fchildcategory : null,
                        'prodIds' => null,
                        'fspricefrom' => (!is_null($request->fspricefrom)) ? $request->fspricefrom : null,
                        'fspriceto' => (!is_null($request->fspriceto)) ? $request->fspriceto : null,
                        'fspdiscountfrom' => (!is_null($request->fspdiscountfrom)) ?$request->fspdiscountfrom : null,
                        'fspdiscountto' => (!is_null($request->fspdiscountto)) ? $request->fspdiscountto : null,
                        'fsdiscountfrom' => (!is_null($request->fsdiscountfrom)) ? $request->fsdiscountfrom : null,
                        'fsdiscountto' => (!is_null($request->fsdiscountto)) ? $request->fsdiscountto : null,
                    ];
                    $productVariantData = ProductVariant::getSelectAllProductVariantListData($deletefilter);

                    foreach($productVariantData  as $variantData){
                        $allVariantIds[] = strval($variantData->id);
                    }

                    foreach ($allVariantIds as $val) {
                        $valuePosition =  CouponGroup::where('id', $request->input('groupId'))->select(DB::raw("JSON_SEARCH(product_varient_ids, 'all', '".$val."') as result"))->first();

                        CouponGroup::where('id', $request->input('groupId'))->update([
                                                'product_varient_ids' => DB::raw("JSON_REMOVE(product_varient_ids, '".str_replace('"', "", $valuePosition->result)."')")
                                                    ]);
                    }
                } else{
                    foreach ($request->input('id') as $val) {
                        $valuePosition =  CouponGroup::where('id', $request->input('groupId'))->select(DB::raw("JSON_SEARCH(product_varient_ids, 'all', '".$val."') as result"))->first();

                        CouponGroup::where('id', $request->input('groupId'))->update([
                                                'product_varient_ids' => DB::raw("JSON_REMOVE(product_varient_ids, '".str_replace('"', "", $valuePosition->result)."')")
                                                    ]);
                    }
                }
                DB::commit();
                $records["customActionMessage"] = 'Selected records are deleted successfully.';
            } catch (Exception $e) {
                DB::rollback();
                $records["customActionStatus"] = "NOT-OK";
                $records["customActionMessage"] = 'Oops..some error occured. Please try again later.';
            }
        }


        //get the already saved product variant Ids
        $prodVariantIds = CouponGroup::where('id', $groupId)->pluck('product_varient_ids');
        $prodIds = (!is_null($prodVariantIds)) ? json_decode($prodVariantIds[0]) : null;

        $criteria = (object)[
            'length' => intval($request->length),
            'fpricefrom' => (!is_null($request->fpricefrom)) ? $request->fpricefrom : null,
            'fpriceto' => (!is_null($request->fpriceto)) ? $request->fpriceto : null,
            'fpdiscountfrom' => (!is_null($request->fpdiscountfrom)) ?$request->fpdiscountfrom : null,
            'fpdiscountto' => (!is_null($request->fpdiscountto)) ? $request->fpdiscountto : null,
            'fname' => (!is_null($request->fname)) ? $request->fname : null,
            'fcategories' => (!is_null($request->fcategories)) ? $request->fcategories : null,
            'fbrand' => (!is_null($request->fbrand)) ? $request->fbrand : null,
            'fcategory' => (!is_null($request->fcategory)) ? $request->fcategory : null,
            'fsubcategory' => (!is_null($request->fsubcategory)) ? $request->fsubcategory : null,
            'fchildcategory' => (!is_null($request->fchildcategory)) ? $request->fchildcategory : null,
            'fcompanyname' => (!is_null($request->fcompanyname)) ? $request->fcompanyname : null,
            'fseller' => (!is_null($request->fseller)) ? $request->fseller : null,
            'fdiscountfrom' => (!is_null($request->fdiscountfrom)) ? $request->fdiscountfrom : null,
            'fdiscountto' => (!is_null($request->fdiscountto)) ? $request->fdiscountto : null,
            'fselectedprodvariant' => (!is_null($prodIds)) ? $prodIds : null,
            'fspricefrom' => (!is_null($request->fspricefrom)) ? $request->fspricefrom : null,
            'fspriceto' => (!is_null($request->fspriceto)) ? $request->fspriceto : null,
            'fspdiscountfrom' => (!is_null($request->fspdiscountfrom)) ?$request->fspdiscountfrom : null,
            'fspdiscountto' => (!is_null($request->fspdiscountto)) ? $request->fspdiscountto : null,
            'fsdiscountfrom' => (!is_null($request->fsdiscountfrom)) ? $request->fsdiscountfrom : null,
            'fsdiscountto' => (!is_null($request->fsdiscountto)) ? $request->fsdiscountto : null,
        ];

        $prodVariants = ProductVariant::getAjaxCouponGroupProductVariantFinalListData($criteria, $iPage, $orderColumn, $orderDir);

        $iTotalRecords =  $prodVariants->total();

        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $end = $iDisplayStart + $iDisplayLength;

        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $canChange = ($this->loggedInUser->hasRole('Super Admin') || $this->loggedInUser->hasRole('Admin'));

        foreach ($prodVariants as $index=>$prodVariant) {

            $variants = __getVariantOptionValues($prodVariant->id);

            $records["data"][] = [
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes checkBoxClass" value="'.$prodVariant->id.'"/><span></span></label>',
                '<a href="'.route('admin.products.variantEdit', ['id'=>$prodVariant->product->id, 'vid'=>$prodVariant->id]).'" target="_blank" class="" title="View Order Details">'.$prodVariant->unique_id.'</a>',
                '<span class=""><a href="https://fabpik.in/products/'.$prodVariant->slug.'/'.$prodVariant->unique_id.'" target="_blank" class="" title="View Product Details">'.($prodVariant->name).'</a></span>',
                '<img class="zoom" src="'.url('/').'/'.UPLOAD_PATH.'/'.__getVariantImages($prodVariant->id).'" width="50" height="">',
                $prodVariant->product->category->title,
                $prodVariant->sku,
                $prodVariant->mrp,
                $prodVariant->price,
                number_format((float)($prodVariant->discount), '2'),
                $prodVariant->fabpik_seller_price,
                number_format((float)($prodVariant->fabpik_seller_discount), '1'),
                // number_format((float)($prodVariant->fabpik_seller_discount/$prodVariant->mrp)*100, '1'),
                number_format((float) ($prodVariant->fabpik_seller_discount_percentage), '1' ),
                (!is_null($prodVariant->special_price_start_date)) ? date('d M Y h:i:s A', strtotime($prodVariant->special_price_start_date)) : '-',
                (!is_null($prodVariant->special_price_end_date)) ? date('d M Y h:i:s A', strtotime($prodVariant->special_price_end_date)) : '-',
                $variants['PrimaryAttrValue'],
                $variants['SecondaryAttrValue'],
                date('d M Y', strtotime($prodVariant->created_at)),
                date('d M Y', strtotime($prodVariant->updated_at)),
                '',
            ];
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return response()->json($records);

    }

    /**
     * [add Coupon data]
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function storeFinalGroup(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [], []);

        if (!isset($request->id)) {
            $validator->after(function ($validator) {
                $validator->errors()->add('variantCheckbox', 'Please select atleast one product !!!');
            });
        }

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        try {
            $updateGroup = CouponGroup::where('id', $request->groupId)->update([
                                                                        'product_varient_ids' => json_encode($request->id),
                                                                        'created_by' => $this->loggedInUser->id
                                                                    ]);

            if ($updateGroup) {
                return redirect()
                    ->route('admin.coupongroups')
                    ->with(['toast'=>'1','status'=>'success','title'=>'Coupon Group','message'=>'Success! New Coupon Group added successfully.']);
            } else {
                return redirect()
                    ->route('admin.coupongroups')
                    ->with(['toast'=>'1','status'=>'error','title'=>'Coupon Group','message'=>'Error! Some error occured, please try again.']);
            }
        } catch (Exception $e) {
            return redirect()
                    ->route('admin.coupongroups')
                    ->with(['toast'=>'1','status'=>'error','title'=>'Coupon Group','message'=>'Error! Some error occured, please try again.']);
        }
    }

        /**
     * [edit Coupon Group data]
     * @param  Request $request [description]
     * @param  [type]  $id      [brand id]
     * @return [type]           [description]
     */
    public function edit(Request $request, $id)
    {
        $coupon = CouponGroup::find($id);
        if($request->save){

            $validator = Validator::make($request->all(),[
                'group_name' => 'required',
                'description' => 'required|max:255',
                'status' => 'required',
            ],[
                'group_name.required' => 'Group name field is required.',
                'description.required' => 'Description field is required.',
            ]);

            $isExist =  CouponGroup::where('group_name', $request->group_name)->where('id', '<>', $id)->exists();

            if ($isExist) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('group_name', 'Group name already exist!');
                });
            }

            if($validator->fails()){
                return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            try {
                DB::beginTransaction();

                $editCouponGroup = CouponGroup::where('id', $id)->update([
                                                            'group_name' => $request->group_name,
                                                            'description' => $request->description,
                                                            'status' => $request->status
                                                            ]);


                if ($editCouponGroup) {
                    DB::commit();

                    if($request->save == "save"){
                        return redirect()
                        ->route('admin.coupongroups.edit',['id'=>$id]);
                    }else{
                        return redirect()
                        ->route('admin.coupongroups.selectVariants', ['id'=>$id]);
                    }
                }
            } catch (Exception $e){
                DB::rollback();
                return redirect()
                        ->back('admin.coupongroups.addGroupName');
            }

        }

        $this->data['title'] = 'Edit Coupon Group';
        $this->data['activeMenu'] = $this->activeMenu;
        $this->data['activeSubmenu'] = $this->activeSubmenu;
        $this->data['breadcrumbs'] = (object) [
            (object) [
                'url' => 'admin.coupongroups',
                'title' => 'Coupon Groups',
            ]
        ];

        $this->data['editcoupon'] = $coupon;
        $this->data['id'] = $id;

        return parent::adminView('coupongroups.addCouponGroup', $this->data);
    }

    /**
     * [Delete Single Coupon data]
     * @param  Request $request [description]
     * @param  [type]  $id      [coupon id]
     * @return [type]           [description]
    */
    public function delete(Request $request, $id)
    {
        try {
            $recordCount = Coupon::where('coupon_group_id',  $id)->count();
            if($recordCount > 0){
                return response()->json(['success'=>false, 'message'=>"Group already assigned to coupon", 'error'=>true], 200);
            }

            $delete = CouponGroup::where('id', $id)->delete();
            if($delete){
                return response()->json(['success'=>true, 'message'=>"Group deleted successfully", 'error'=>false], 200);
            }
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['success'=>false, 'message'=>"Error! Some error occured, please try again.", 'error'=>true], 200);
        }
    }
}

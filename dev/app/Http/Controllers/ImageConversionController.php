<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ImageConversionController extends Controller
{
    public function index($folder = null)
    {
        // $folder = '100020';
        if(!$folder) dd('Folder name required.');

        shell_exec("sudo chmod -R 777 uploads/");

        $this->listFolderFiles('uploads/'.$folder.'/products');
    }

    public function listFolderFiles($dir)
    {
        $ffs = scandir($dir);

        unset($ffs[array_search('.', $ffs, true)]);
        unset($ffs[array_search('..', $ffs, true)]);
        unset($ffs[array_search('.DS_Store', $ffs, true)]);
        unset($ffs[array_search('sizechart', $ffs, true)]);

        // prevent empty ordered elements
        if (count($ffs) < 1)
            return;

        echo '<ol>';
        foreach($ffs as $ff){
            if(is_dir($dir.'/'.$ff)) {
                $this->listFolderFiles($dir.'/'.$ff);
            } else {
                echo '<li>'.$dir.'/'.$ff.'</li>';
                echo '<li>'.$this->convert($dir.'/'.$ff).'</li>'; // die;
            }
        }
        echo '</ol>';
    }

    /*public function convert($file)
    {
        $fileInfo = pathinfo($file);

        switch (strtolower($fileInfo['extension'])) {
            case 'jpg':
                $image =  imagecreatefromjpeg($file);
                break;

            case 'jpeg':
                $image =  imagecreatefromjpeg($file);
                break;

            case 'png':
                $image =  imagecreatefrompng($file);
                break;

            default:
                # code...
                break;
        }

        ob_start();
        imagejpeg($image,NULL,80);
        $cont =  ob_get_contents();
        ob_end_clean();
        imagedestroy($image);
        $content =  imagecreatefromstring($cont);

        $webpFile = $fileInfo['dirname'].'/'.$fileInfo['filename'].'.webp';
        imagewebp($content,$webpFile);
        imagedestroy($content);

        return $webpFile;
    }*/

    public function convert($file)
    {
        $fileInfo = pathinfo($file);

        $webpFile = $fileInfo['dirname'].'/'.$fileInfo['filename'].'.webp';
        // echo "cwebp -q 60 {$file} -o {$webpFile}";
        shell_exec("cwebp -q 60 {$file} -o {$webpFile}");

        return $webpFile;
    }

}
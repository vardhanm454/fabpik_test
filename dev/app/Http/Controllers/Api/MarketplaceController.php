<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Hash;
use Validator;
use Illuminate\Validation\Rule;

// Models
use App\Models\User;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderHistory;
use App\Models\OrderStatus;
use App\Models\ShippingStatus;

class MarketplaceController extends ApiController
{
    public $loggedInUser;

    /**
    * Create a new AuthController instance.
    *
    * @return void
    */
    public function __construct() {
        // $this->middleware('auth:api', ['except' => ['authToken','productsCount','products','pendency','orders','acknowledge','updateInventory','invoiceDetails','courierDetails','postShipment','labels']]);
        // $this->loggedInUser = User::find(134);
    }

    public function __getUserByApiKey($request = null)
    {
        $this->loggedInUser = NULL;

        if(auth('api')->user())
            return $this->loggedInUser = auth('api')->user();
    }

    // Get Authentication
    public function authToken(Request $request)
    {
        // PWD: 123456

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails())
            return response()->json(['status'=>'FAILED', 'errorMessages'=>$validator->messages()->toArray()], 400);

        $user = User::where(['email'=>$request->username,'user_type'=>'seller'])->first();
        if(!$user)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Username does not exist'], 400);

        //check password match or not
        if (!Hash::check($request->password, $user->marketplace_password)
            || (!$token = auth('api')->login($user))){
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Wrong Password!'], 400);
        }

        return response()->json(['status'=>'SUCCESS', 'accessToken'=>$token], 200);
    }

    // Get Products Count
    public function productsCount(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        $validator = Validator::make($request->all(), [
            'publishedStatus' => 'required|in:PUBLISHED,UNPUBLISHED',
        ]);

        if ($validator->fails())
            return response()->json(['status'=>'FAILED', 'errorMessages'=>$validator->messages()->toArray()], 400);

        $productCount['totalResults'] = 0;
        if($request->publishedStatus == 'PUBLISHED') $productCount = Product::getMarketPlaceProductCount($this->loggedInUser->ref_id);

        return response()->json(['count' => $productCount['totalResults']], 200);
    }

    // Get Products
    public function products(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        $validator = Validator::make($request->all(), [
            'pageNumber' => 'required',
            'pageSize' => 'required',
            // 'skus' => 'required',
            'publishedStatus' => 'required|in:PUBLISHED,UNPUBLISHED',
        ]);

        if ($validator->fails())
            return response()->json(['status'=>'FAILED', 'errorMessages'=>$validator->messages()->toArray()], 400);

        $filters = [
            'pageNumber'=> $request->pageNumber,
            'pageSize'=> $request->pageSize,
            'skus'=> $request->skus,
            'publishedStatus'=> $request->publishedStatus,
        ];

        $products = [];
        if($request->publishedStatus == 'PUBLISHED') $products = Product::getMarketPlaceProducts($filters,$this->loggedInUser->ref_id);

        return response()->json(['products' => $products], 200);
    }

    // Get On-hold Orders
    public function pendency(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        $validator = Validator::make($request->all(), [
            'pageNumber' => 'required',
            'pageSize' => 'required',
            'orderDateFrom' => 'date_format:Y-m-d\TH:i:s',
            'orderDateTo' => 'required|date_format:Y-m-d\TH:i:s',
        ]);

        if ($validator->fails())
            return response()->json(['status'=>'FAILED', 'errorMessages'=>$validator->messages()->toArray()], 400);

        $offset = ($request->pageNumber - 1) * $request->pageSize;

        $orders = Order::select("orders.id as id","order_details.child_order_id as orderId","p.unique_id as productId","product_variants.unique_id as variantId","product_variants.unique_id as orderItemId","product_variants.sku","order_details.name as title","order_details.quantity",'order_statuses.name as status')
                    ->join('order_details','order_details.order_id','=','orders.id')
                    ->join('product_variants','product_variants.id','=','order_details.product_variant_id')
                    ->join('order_statuses','order_details.order_status_id','=','order_statuses.id')
                    ->join('products AS p','p.id','product_variants.product_id')
                    ->join('sellers AS s','s.id','p.seller_id')
                    ->where('order_details.seller_id',$this->loggedInUser->ref_id)
                    ->where("order_details.order_status_id",1);

        if($request->orderDateFrom != null || $request->orderDateFrom != ""){
            $orders->whereRaw("orders.created_at >= '".$request->orderDateFrom."'");
        }

        $orders->whereRaw("orders.created_at <= '".$request->orderDateTo."'");
        $orders->offset($offset);
        $orders->limit($request->pageSize);
        $orders = $orders->get();

        $check_offset = (int)$offset+(int)$request->pageSize;

        $check_for_more = Order::select("orders.id as id","order_details.child_order_id as orderId","p.unique_id as productId","product_variants.unique_id as variantId","product_variants.sku","order_details.name as title","order_details.quantity",'order_statuses.name as status')
                        ->join('order_details','order_details.order_id','=','orders.id')
                        ->join('product_variants','product_variants.id','=','order_details.product_variant_id')
                        ->join('order_statuses','order_details.order_status_id','=','order_statuses.id')
                        ->join('products AS p','p.id','product_variants.product_id')
                        ->join('sellers AS s','s.id','p.seller_id')
                        ->where('order_details.seller_id',$this->loggedInUser->ref_id)
                        ->where("order_details.order_status_id",1);

        if($request->orderDateFrom != null || $request->orderDateFrom != ""){
            $check_for_more->whereRaw("orders.created_at >= '".$request->orderDateFrom."'");
        }

        $check_for_more->whereRaw("orders.created_at <= '".$request->orderDateTo."'");
        $check_for_more->offset($check_offset);
        $check_for_more->limit($request->pageSize);
        $check_for_more = $check_for_more->count();

        $hasMore = ($check_for_more>0)?true:false;

        $pendencies = [];
        if ($orders) {
            foreach ($orders as $orderItem) {
                $pendencies[] = [
                    "orderId" => $orderItem->orderId,
                    "orderStatus" => "PENDING_VERIFICATION",
                    "orderItems" => [
                        "orderItemId" => $orderItem->orderId,
                        "productId" => $orderItem->productId,
                        "variantId" => $orderItem->variantId,
                        "sku" => $orderItem->sku,
                        "title" => $orderItem->title,
                        "status" => "PENDING_VERIFICATION",
                        "quantity" => $orderItem->quantity,
                        "onHold" => false,
                        "packetNumber" => 0
                    ]
                ];
            }
        }

        return response()->json(['pendencies'=>$pendencies, 'hasMore'=>$hasMore], 200);
    }

    // Get Orders / Get Order Status
    public function orders(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        $validator = Validator::make($request->all(), [
            'pageNumber' => 'required',
            'pageSize' => 'required',
            'orderDateFrom' => 'nullable|date_format:Y-m-d\TH:i:s',
            'orderDateTo' => 'nullable|date_format:Y-m-d\TH:i:s',
            'orderStatus' => "in:PENDING_VERIFICATION,CREATED,PROCESSING,COMPLETE,CANCELLED"
        ]);

        if ($validator->fails())
            return response(['status'=>'FAILED', 'errorMessages'=>$validator->messages()->toArray()], 400);

        $offset = ($request->pageNumber - 1) * $request->pageSize;

        $orders = OrderDetail::join('products AS p','p.id','order_details.product_id')
                            ->join('product_variants as pv','pv.id','=','order_details.product_variant_id')
                            ->join('orders as o', 'o.id', '=', 'order_details.order_id')
                            ->where('order_details.seller_id',$this->loggedInUser->ref_id);

        if(!empty($request->orderIds)) {
            $orderIds = explode(',', $request->orderIds);
            $orders = $orders->whereIn('order_details.child_order_id', $orderIds);
        }

        if(!empty($request->orderItemIds)) {
            $orderItemIds = explode(',', $request->orderItemIds);
            $orders = $orders->whereIn('order_details.child_order_id', $orderItemIds);
        }

        if(!empty($request->orderDateFrom)) {
            $orderDateFrom = date('Y-m-d H:i:s', strtotime($request->orderDateFrom));
            $orders = $orders->where('order_details.created_at', ">=", $orderDateFrom);
        }

        if(!empty($request->orderDateTo)) {
            $orderDateTo = date('Y-m-d H:i:s', strtotime($request->orderDateTo));$orders = $orders->where('order_details.created_at', "<=", $orderDateTo);
        }

        //
        if(!empty($request->orderStatus)) {
            $whereOrderStatuses = [
                'PENDING_VERIFICATION' => [
                    'o' => [1,3],
                    's' => [1]
                ],
                'CREATED' => [
                    'o' => [0],
                    's' => [0]
                ],
                'PROCESSING' => [
                    'o' => [4,5,6,7,8,10],
                    's' => [2,3,4,5,6,7,8,9,10,11,12]
                ],
                'COMPLETE' => [
                    'o' => [9],
                    's' => [0]
                ],
                'CANCELLED' => [
                    'o' => [2],
                    's' => [0]
                ],
            ];

            $whereOrderStatus = $whereOrderStatuses[$request->orderStatus];

            $orders = $orders->where(function($q) use($whereOrderStatus){
                            $q->whereIn('order_details.order_status_id', $whereOrderStatus['o'])
                                ->orWhereIn('order_details.shipping_status_id', $whereOrderStatus['s']);
                        });
        }

        $orders = $orders->selectRaw("
                            order_details.unique_id,
                            order_details.created_at,
                            order_details.order_status_id,
                            order_details.shipping_status_id,
                            p.min_ship_hours,
                            o.payment_type,
                            order_details.cod_charge,
                            order_details.total_seller_discount,
                            order_details.total,
                            order_details.shipping_charge,
                            order_details.shipping_charge_sgst,
                            order_details.shipping_charge_cgst,
                            p.unique_id as p_unique_id,
                            pv.unique_id as pv_unique_id,
                            pv.sku,
                            order_details.name,
                            order_details.price,
                            order_details.quantity,
                            o.shipping_address1,
                            o.shipping_address2,
                            o.shipping_city,
                            o.shipping_email,
                            CONCAT(o.shipping_first_name,' ',o.shipping_last_name) as shipping_name,
                            o.shipping_mobile,
                            o.shipping_pincode,
                            o.shipping_state,
                            o.billing_address1,
                            o.billing_address2,
                            o.billing_city,
                            o.billing_email,
                            CONCAT(o.billing_first_name,' ',o.billing_last_name) as billing_name,
                            o.billing_mobile,
                            o.billing_pincode,
                            o.billing_state,
                            pv.description
                        ")
                        ->offset($offset)
                        ->limit($request->pageSize)
                        ->get();

        $reponse = [];
        $index = 0;
        if($orders) {

            foreach ($orders as $order) {

                $orderStatus = "CREATED";
                $ordItemStatus = "CREATED";

                ////////////////////////////////////////////////////
                if(in_array($order->order_status_id, [1,3])) {
                    $orderStatus = "PENDING_VERIFICATION";
                    $ordItemStatus = "CREATED";
                }

                if(in_array($order->order_status_id, [2])) {
                    $orderStatus = "CANCELLED";
                    $ordItemStatus = "CANCELLED";
                }

                if(in_array($order->order_status_id, [4,6,7])) {
                    $orderStatus = "PROCESSING";
                    $ordItemStatus = "PROCESSING";
                }

                if(in_array($order->order_status_id, [5])) {
                    $orderStatus = "PROCESSING";
                    $ordItemStatus = "RETURN_REQUESTED";
                }

                if(in_array($order->order_status_id, [8])) {
                    $orderStatus = "PROCESSING";
                    $ordItemStatus = "DELIVERED";
                }

                if(in_array($order->order_status_id, [9])) {
                    $orderStatus = "COMPLETE";
                    $ordItemStatus = "DELIVERED";
                }

                if(in_array($order->order_status_id, [10])) {
                    $orderStatus = "PROCESSING";
                    $ordItemStatus = "READY_TO_DISPATCH";
                }
                ////////////////////////////////////////////////////

                if(in_array($order->shipping_status_id, [1])) {
                    $orderStatus = "PENDING_VERIFICATION";
                    $ordItemStatus = "CREATED";
                }

                if(in_array($order->shipping_status_id, [2,3])) {
                    $orderStatus = "PROCESSING";
                    $ordItemStatus = "READY_TO_DISPATCH";
                }

                if(in_array($order->shipping_status_id, [4])) {
                    $orderStatus = "PROCESSING";
                    $ordItemStatus = "DISPATCHED";
                }

                if(in_array($order->shipping_status_id, [5])) {
                    $orderStatus = "PROCESSING";
                    $ordItemStatus = "DELIVERED";
                }

                if(in_array($order->shipping_status_id, [6,7,8])) {
                    $orderStatus = "PROCESSING";
                    $ordItemStatus = "PROCESSING";
                }

                if(in_array($order->shipping_status_id, [9,10])) {
                    $orderStatus = "PROCESSING";
                    $ordItemStatus = "RETURN_REQUESTED";
                }

                if(in_array($order->shipping_status_id, [11])) {
                    $orderStatus = "PROCESSING";
                    $ordItemStatus = "COURIER_RETURN";
                }

                if(in_array($order->shipping_status_id, [12])) {
                    $orderStatus = "PROCESSING";
                    $ordItemStatus = "RETURNED";
                }

                $reponse[$index] = [
                    'id' => $order->unique_id,
                    'orderDate' => date('Y-m-d\TH:i:s', strtotime($order->created_at)),
                    'orderStatus' => $orderStatus,
                    'sla' => date("Y-m-d H:i:s", strtotime('+'.$order->min_ship_hours.' hours', strtotime($order->created_at))),
                    'priority' => 0,
                    'paymentType' => ($order->payment_type=='o')?'PREPAID':'COD',
                    'orderPrice' => [
                        'currency' => 'INR',
                        'totalCashOnDeliveryCharges' => $order->cod_charge,
                        'totalDiscount' => $order->total_seller_discount,
                        'totalGiftCharges' => 0,
                        'totalPrepaidAmount' => ($order->payment_type=='o')?$order->total:0,
                        'totalShippingCharges' => ($order->shipping_charge + $order->shipping_charge_sgst + $order->shipping_charge_cgst),
                    ],
                    'orderItems' => [
                        [
                            'orderItemId' => $order->unique_id,
                            'status' => $ordItemStatus,
                            'productId' => $order->p_unique_id,
                            'variantId' => $order->pv_unique_id,
                            'sku' => $order->sku,
                            'returnReason' => '',
                            'returnDate' => '',
                            'returnAWB' => '',
                            'returnShippingProvider' => '',
                            'title' => $order->name,
                            'shippingMethodCode' => 'STD',
                            'orderItemPrice' => [
                                'cashOnDeliveryCharges' => $order->cod_charge,
                                'sellingPrice' => $order->price,
                                'shippingCharges' => ($order->shipping_charge + $order->shipping_charge_sgst + $order->shipping_charge_cgst),
                                'discount' => $order->total_seller_discount,
                                'totalPrice' => $order->total,
                                'transferPrice' => 0,
                                'currency' => 'INR'
                            ],
                            'quantity' => $order->quantity,
                            'giftWrap' => [
                                'giftWrapMessage' => '',
                                'giftWrapCharges' => 0
                            ],
                            'onHold' => false,
                            'packetNumber' => 0,
                        ]
                    ],
                    'taxExempted' => false,
                    'cFormProvided' => false,
                    'thirdPartyShipping' => false,
                    'shippingAddress' => [
                        'addressLine1' => $order->shipping_address1,
                        'addressLine2' => $order->shipping_address2,
                        'city' => $order->shipping_city,
                        'country' => 'India',
                        'email' => $order->shipping_email,
                        'name' => $order->shipping_name,
                        'phone' => $order->shipping_mobile,
                        'pincode' => $order->shipping_pincode,
                        'state' => $order->shipping_state,
                    ],
                    'billingAddress' => [
                        'addressLine1' => $order->billing_address1,
                        'addressLine2' => $order->billing_address2,
                        'city' => $order->billing_city,
                        'country' => 'India',
                        'email' => $order->billing_email,
                        'name' => $order->billing_name,
                        'phone' => $order->billing_mobile,
                        'pincode' => $order->billing_pincode,
                        'state' => $order->billing_state,
                    ],
                    'additionalInfo' => $order->description
                ];

                ++$index;
            }
        }

        return response(['orders'=>$reponse], 200);
    }

    //Post Order Acceptance
    public function acknowledge(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        $validator = Validator::make($request->all(), [
            'orderId' => 'required'
        ]);

        if ($validator->fails())
            return response()->json(['status'=>'FAILED', 'errorMessages' => $validator->messages()->toArray()], 400);

        $order = OrderDetail::where('child_order_id',$request->orderId)
                    ->where('seller_id', $this->loggedInUser->ref_id)
                    ->exists();

        if(!$order) return response()->json(['status'=>'FAILED', 'errorMessage'=>'Order Id not available'], 200);

        $status = 'SUCCESS';

        // check if order is already accepted
        $exist = OrderHistory::where(['order_detail_id'=>$request->orderId,'order_status_id'=>4])->exists();
        if(!$exist) {
            $response = json_decode(__changeStatus(4,[$request->orderId],null));
            $status = ($response->status == 1)?'SUCCESS':'FAILED';
        }

        return response()->json(['status'=>$status], 200);
    }

    //Post Inventory Update
    public function updateInventory(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        $validator = Validator::make($request->all(), [
            'inventoryList' => 'required|array',
            'inventoryList.*.productId' => 'required|string',
            'inventoryList.*.variantId' => 'required|string',
            'inventoryList.*.inventory' => 'required|integer',
            'inventoryList.*.hsnCode' => 'string',
        ]);

        if ($validator->fails())
            return response()->json(['status'=>'FAILED', 'errorMessages'=>$validator->messages()->toArray()], 400);

        $allResponses = [];
        $failedProductList = [];
        $index = 0;
        foreach($request->inventoryList as $inventory) {
            // check product id exist
            $product = Product::where('unique_id',$inventory['productId'])
                    ->where('seller_id', $this->loggedInUser->ref_id)
                    ->exists();

            if(!$product) {
                $failedProductList[$index] = [
                    'productId' => $inventory['productId'],
                    'variantId' => $inventory['variantId'],
                    'message' => 'Product Id not available',
                ];
            }

            // check product variant id exist
            $prdVariant = ProductVariant::selectRaw("products.unique_id as prd_unique_id, product_variants.unique_id as prdv_unique_id")
                    ->join("products", "product_variants.product_id", "=", "products.id")
                    ->where('product_variants.unique_id',$inventory['variantId'])
                    ->where('products.seller_id', $this->loggedInUser->ref_id)
                    ->first();

            if(!$prdVariant || ($prdVariant->prd_unique_id != $inventory['productId'])) {
                $failedProductList[$index] = [
                    'productId' => $inventory['productId'],
                    'variantId' => $inventory['variantId'],
                    'message' => 'Variant Id not available',
                ];
            }

            ProductVariant::where('unique_id', $inventory['variantId'])->update([
                'stock' => $inventory['inventory'],
            ]);

            ++$index;
        }

        return response()->json(['status'=>'SUCCESS','failedProductList'=>$failedProductList], 200);
    }

    // Get Invoice Details
    public function invoiceDetails(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        $validator = Validator::make($request->all(), [
            'orderItemIds' => 'required'
        ]);

        if ($validator->fails())
            return response()->json(['status'=>'FAILED', 'errorMessages'=>$validator->messages()->toArray()], 400);

        $orderItemIds = explode(",",$request->orderItemIds);

        $response = [];
        foreach ($orderItemIds as $orderItemId) {
            $order = OrderDetail::selectRaw('order_details.*, c.tax')->join('products as p', 'p.id', '=', 'order_details.product_id')
                                ->join('categories as c', 'c.id', '=', 'p.primary_category')
                                ->where('child_order_id',$orderItemId)
                                ->where('seller_id', $this->loggedInUser->ref_id)
                                ->first();
            if(!$order) {
                $response[$orderItemId] = [
                    'errorMessage' => 'Order Item Id not available'
                ];
            } else {
                $response[$orderItemId] = [
                    "code" => $order->seller_invoice_prefix.$order->seller_invoice_no,
                    "productTaxes" => [
                        "additionalInfo" => null,
                        "productId" => $order->product_id,
                        "variantId" => $order->product_variant_id,
                        "taxPercentage" => $order->tax,
                        "centralGstPercentage" => $order->tax/2,
                        "stateGstPercentage" => $order->tax/2,
                        "unionTerritoryGstPercentage" => 0,
                        "integratedGstPercentage" => 0,
                        "compensationCessPercentage" => 0
                    ],
                    "invoiceDate" => date("Y-m-d\TH:i:s", strtotime($order->created_at))
                ];
            }
        }

        return response()->json($response, 200);
    }

    // Get Courier Details
    public function courierDetails(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        if(!$request->orderItemIds)
            return response(['status' => 'FAILED', 'error' => 'Order Item ID missing'], 400);

        $orderItemIds = explode(",", $request->orderItemIds);

        $response = [];
        foreach ($orderItemIds as $orderItemId) {
            $tracking_no = $courier_id = $courier_name = $status = '';

            $order = OrderDetail::selectRaw('tracking_no, courier_id, courier_name, shipping_model')
                                ->where('child_order_id',$orderItemId)
                                ->where('seller_id', $this->loggedInUser->ref_id)
                                ->first();

            if($order) {
                if($order->shipping_model=='s') $status = 'SELLER_SHIPPING';
                else {
                    if(!$order->courier_id) $status = 'COURIER_NOT_ASSIGNED';
                    if(!empty($order->tracking_no) && !empty($order->courier_id)) $status = 'AVAILABLE';
                }

                $tracking_no = $order->tracking_no;
                $courier_id = $order->courier_id;
                $courier_name = $order->courier_name;
            }

            $response[$orderItemId] = [
                "awbNo" => $tracking_no,
                "status" => $status,
                "courierCode" => $courier_id,
                "courierName" => $courier_name,
                "additionalInfo" => ""
            ];
        }

        return response()->json($response, 200);
    }

    // Post Shipment Details
    public function postShipment(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        $validator = Validator::make($request->all(), [
            'boxHeight' => 'required|integer',
            'boxLength' => 'required|integer',
            'boxWidth' => 'required|integer',
            'weight' => 'required|integer',
            'orderItems' => 'required|array',
            'orderItems.*.orderItemId' => 'required|string',
            'orderItems.*.invoiceNumber' => 'required|string',
            'orderItems.*.invoiceDate' => 'required|date_format:Y-m-d',
        ]);

        if(count($request->orderItems)>1) {
            $validator->after(function ($validator) {
                $validator->errors()->add(
                    'orderItems', 'Only 1 item allowed.'
                );
            });
        }

        if ($validator->fails())
            return response(['status' => 'FAILED', 'errors' => $validator->messages()->toArray()], 400);

        $orderItemId = $request->orderItems[0]['orderItemId'];

        // check order exists or not
        $order = OrderDetail::where(['seller_id'=>$this->loggedInUser->ref_id,'child_order_id'=>$orderItemId])->first();

        if($order) {
            // save whole post request data as JSON in order detail table
            $postData = $request->only('boxHeight','boxLength','boxWidth','weight','orderItems');

            $order->post_shipment_details = json_encode($postData);
            $order->save();

            return response()->json(["status"=>"SUCCESS"], 200);
        } else {
            return response()->json(['status'=>'SUCCESS', 'orderItems'=>['orderItemId'=>$orderItemId,'errorMessage'=>'Invalid order item ID']], 200);
        }
    }

    // Get Labels
    public function labels(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        if(!$request->orderItemIds)
            return response(['status'=>'FAILED', 'error'=>'Order Item ID missing'], 400);

        $orderItemIds = explode(",", $request->orderItemIds);

        $response = [];
        foreach ($orderItemIds as $orderItemId) {
            $label_url = '';

            $order = OrderDetail::selectRaw('label_url')
                                ->where('child_order_id',$orderItemId)
                                ->where('seller_id', $this->loggedInUser->ref_id)
                                ->first();
            if($order) $label_url = $order->label_url;

            $response[$orderItemId] = [
                "label_url" => $label_url
            ];
        }

        return response($response, 200);
    }

    // Post Manifest Details
    public function fetchManifest(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        $validator = Validator::make($request->all(), [
            'courierCode' => 'required|string',
            'manifestItems' => 'required|array',
            'manifestItems.*.orderItemId' => 'required|string|',
            'manifestItems.*.status' => [
                                    'required',
                                    Rule::in(['CANCELLED', 'CREATED', 'PROCESSING', 'PACKED', 'READY_TO_DISPATCH', 'DISPATCHED', 'DELIVERED', 'REPLACED', 'RETURN_REQUESTED', 'RETURNED']),
                                ],
            'manifestItems.*.productId' => 'required',
            'manifestItems.*.variantId' => 'required',
            'manifestItems.*.quantity' => 'required|integer',
        ]);

        if(count($request->manifestItems)>1) {
            $validator->after(function ($validator) {
                $validator->errors()->add(
                    'manifestItems', 'Only 1 item allowed.'
                );
            });
        }

        if ($validator->fails())
            return response()->json(['status'=>'FAILED', 'errors'=>$validator->messages()->toArray()], 400);

        $orderItemId = $request->manifestItems[0]['orderItemId'];

        $order = OrderDetail::selectRaw('manifest_url')
                                ->where('child_order_id',$orderItemId)
                                ->where('seller_id', $this->loggedInUser->ref_id)
                                ->first();
        if(!$order)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Order Item Id not available'], 200);

        return response()->json(['status'=>'SUCCESS', 'manifestUrl'=>$order->manifest_url], 200);
    }

    // Post Order Dispatch
    public function orderDispatch(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        $validator = Validator::make($request->all(), [
            'orderItems' => 'required|array',
            'orderItems.*.orderItemId' => 'required|string',
            'orderItems.*.quantity' => 'required|integer',
            'selfShipping' => 'required|array'
        ]);

        if ($validator->fails())
            return response()->json(['status'=>'FAILED', 'errors' => $validator->messages()->toArray()], 400);

        $orderItems = $request->orderItems;
        $selfShipping = $request->selfShipping;

        $response = [];
        $errors = 0;
        $itemArr = [];
        $status = 'SUCCESS';

        DB::beginTransaction();

        try{
            foreach ($orderItems as $orderItem) {
                $order = OrderDetail::where('child_order_id',$orderItem['orderItemId'])
                                    ->where('seller_id', $this->loggedInUser->ref_id)
                                    ->first();
                $errorMessage = '';
                if(!$order) {
                    ++$errors;
                    $errorMessage = 'Order Item Id not available';
                } else {
                    // check if dispatch/shipped status is already exist or not
                    $exist = OrderHistory::where(['order_detail_id'=>$order->child_order_id,'shipping_status_id'=>4])->exists();
                    if(!$exist) {
                        $response = json_decode(__changeSellerShippingStatus(4,[$order->child_order_id],null));
                        if($response->status != 1) {
                            ++$errors;
                            $errorMessage = 'Some error occurred while updating order status';
                        }
                    } else {
                        ++$errors;
                        $errorMessage = 'Order Item is already dispached';
                    }
                    /*
                    // check if dispatch status is already exist or not
                    $isExist = OrderHistory::where(['order_detail_id'=>$order->child_order_id,'shipping_status_id'=>4])->exists();
                    if(!$isExist) {
                        // update order shipping data
                        $order->shipping_status_id = 4;
                        $order->courier_name = $selfShipping['deliveryPartner'];
                        $order->invoice_no = $selfShipping['invoiceNumber'];
                        $order->tracking_no = $selfShipping['trackingId'];
                        $order->excepted_delivery_date = date('Y-m-d H:i:s',strtotime($selfShipping['tentativeDeliveryDate']));
                        $order->save();

                        // add data to order history table
                        OrderHistory::insertGetId([
                            'order_id' => $order->order_id,
                            'order_detail_id' => $order->child_order_id,
                            'shipping_status_id' => 4
                        ]);
                    } else {
                        ++$errors;
                        $errorMessage = 'Order Item is already dispatched';
                    }*/
                }

                $itemArr[] = [
                    'orderItemId' => $orderItem['orderItemId'],
                    'errorMessage' => $errorMessage,
                ];
            }

            if(count($orderItems) == $errors) $status = 'FAILED';
            if($errors >= 1) $status = 'PARTIAL_SUCCESS';

            DB::commit();
            return response()->json(['status'=>$status, 'orderItems'=>$itemArr], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status'=>'FAILED'], 400);
        }
    }

    // Post Order Cancel
    public function orderCancel(Request $request)
    {
        $this->__getUserByApiKey($request);

        if(!$this->loggedInUser)
            return response()->json(['status'=>'FAILED', 'errorMessage'=>'Invalid Apikey!'], 400);

        $validator = Validator::make($request->all(), [
            'orderId' => 'required',
            'orderItems' => 'required|array',
            'orderItems.*.orderItemId' => 'required|string',
            'orderItems.*.productId' => 'required|string',
            'orderItems.*.variantId' => 'required|string',
            'orderItems.*.quantity' => 'required|integer',
        ]);

        if ($validator->fails())
            return response()->json(['status'=>'FAILED', 'errors'=>$validator->messages()->toArray()], 400);

        $orderItems = $request->orderItems;

        $response = [];
        $errors = 0;
        $itemArr = [];
        $status = 'SUCCESS';

        DB::beginTransaction();

        try {
            foreach ($orderItems as $orderItem) {
                $order = OrderDetail::where('child_order_id',$orderItem['orderItemId'])
                                    ->where('seller_id', $this->loggedInUser->ref_id)
                                    ->first();
                $errorMessage = '';
                if(!$order) {
                    ++$errors;
                    $errorMessage = 'Order Item Id not available';
                } else {
                    // check if cancel status is already exist or not
                    $exist = OrderHistory::where(['order_detail_id'=>$order->child_order_id,'order_status_id'=>2])->exists();
                    if(!$exist) {
                        $response = json_decode(__changeStatus(2,[$order->child_order_id],null));
                        if($response->status != 1) {
                            ++$errors;
                            $errorMessage = 'Some error occurred while updating order status';
                        }
                    } else {
                        ++$errors;
                        $errorMessage = 'Order Item is already cancelled';
                    }

                    /*
                    // check if cancel status is already exist or not
                    $isExist = OrderHistory::where(['order_detail_id'=>$order->child_order_id,'order_status_id'=>2])->exists();
                    if(!$isExist) {
                        // update order shipping data
                        $order->order_status_id = 2;
                        $order->save();

                        // add data to order history table
                        OrderHistory::insertGetId([
                            'order_id' => $order->order_id,
                            'order_detail_id' => $order->child_order_id,
                            'order_status_id' => 2
                        ]);
                    } else {
                        ++$errors;
                        $errorMessage = 'Order Item is already cancelled';
                    } */
                }

                $itemArr[] = [
                    'orderItemId' => $orderItem['orderItemId'],
                    'errorMessage' => $errorMessage,
                ];
            }

            if(count($orderItems) == $errors) $status = 'FAILED';
            if($errors >= 1) $status = 'PARTIAL_SUCCESS';

            DB::commit();
            return response()->json(['status'=>$status, 'orderItems'=>$itemArr], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status'=>'FAILED'], 400);
        }
    }
}
<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\ProductVariant;
use App\Models\AttributeOption;
use App\Models\ProductVariantOption;
use App\Models\Attribute;
use App\Models\Seller;
use App\Models\Coupon;
use Illuminate\Support\Facades\Session;
use DB;

// Models

class GuestCartController extends ApiController
{
  /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct() {}

    public function cart(){
        try{
            $finalCart = $this->finalGuestCart();
            return $this->respond(['success'=>true,'finalCart'=>$finalCart]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }


    public function finalGuestCart($session="no"){
        if($session == "no"){
            session_start();
        }
        $cod = 0; 
        $cod_charge = 0;
        $cart = isset($_SESSION['cart']) ? $_SESSION['cart']:null;
        if(!$cart){
            return ['cart'=>[],'msg'=>'Cart Empty!'];
        }
        $checkDealProductAlreadyExists = false;
        foreach ($cart as $key => $cartItem) {
             if($cartItem["is_deal"] == 'y'){
                $checkDealProductAlreadyExists = true;
                break;
            }
        }
        $products = [];
        // dd($cart);
        $keys = array_column($cart, 'created_at');      
        array_multisort($keys, SORT_ASC, $cart);
        foreach ($cart as $cartItem) {
            $productVariantid = $cartItem["product_variant_id"];
            $productDetails = ProductVariant::selectRaw("product_variants.*,deals.deal_price,deals.start_time,deals.end_time,deals.max_orders,CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_price ELSE product_variants.price END AS final_price,CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_discount ELSE product_variants.discount END AS final_discount")
            ->join('products as p','p.id','=','product_variants.product_id')
            ->leftjoin('deals', function($join){
                $join->on('deals.product_variant_id', '=', 'product_variants.id')
                    ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time');
              })
              ->whereRaw("product_variants.id = $productVariantid AND deals.deleted_at IS NULL")
              ->first();
            $cartItem["deal_price"] = $productDetails->deal_price;
            $cartItem["max_orders"] = $productDetails->max_orders;
            $cartItem["fabpik_seller_price"] = $productDetails->fabpik_seller_price;
            $cartItem["total_seller_discount"] = $productDetails->total_seller_discount;
            $cartItem["final_price"] = $productDetails->final_price;
            $cartItem["final_discount"] = $productDetails->final_discount;
            array_push($products,$cartItem);
        }
        
        if(!$checkDealProductAlreadyExists){
            $tempCart = [];
            foreach ($products as $product) {
                if($product["deal_price"] != null && $product["max_orders"] > 0){
                    $product['quantity'] = 1;
                    $product['price'] = $product["deal_price"];
                    $product['total_price'] = $product["deal_price"];
                    $product['seller_discount'] =  $product["mrp"] - $product["deal_price"];
                    $product['total_seller_discount'] = $product["mrp"] - $product["deal_price"];
                    $product['is_deal'] = 'y';
                    $updatedProduct = $product;
                    break;
                }
            }
            if(isset($updatedProduct)){
                foreach ($cart as $cartItem) {
                    if($updatedProduct["product_variant_id"] == $cartItem["product_variant_id"]){
                        array_push($tempCart,$updatedProduct);
                    }else{
                        array_push($tempCart,$cartItem);
                    }
                }
                $_SESSION['cart'] = $tempCart;
            }
        }else{
            $tempCart = [];
            foreach ($products as $product) {
                if($product["is_deal"] == 'y' && $product["max_orders"] <= 0){
                    $product['quantity'] = 1;
                    $product['price'] = $product["final_price"];
                    $product['total_price'] = $product["final_price"];
                    $product['seller_discount'] =  $product["final_discount"];
                    $product['total_seller_discount'] = $product["final_discount"];
                    $product['is_deal'] = 'n';
                    $updatedProduct = $product;
                    break;
                }
            }
            if(isset($updatedProduct)){
                foreach ($cart as $cartItem) {
                    if($updatedProduct["product_variant_id"] == $cartItem["product_variant_id"]){
                        array_push($tempCart,$updatedProduct);
                    }else{
                        array_push($tempCart,$cartItem);
                    }
                }
                $_SESSION['cart'] = $tempCart;
            }
        }
        $tempCart = [];
        foreach ($products as $product) {
            if(($product["price"] < $product["final_price"] || $product["price"] > $product["final_price"]) && $product["is_deal"] == 'n' ){
                $product['quantity'] = 1;
                $product['price'] = $product["final_price"];
                $product['total_price'] = $product["final_price"];
                $product['seller_discount'] =  $product["final_discount"];
                $product['total_seller_discount'] = $product["final_discount"];
                $newUpdatedProduct = $product;
                break;
            }
        }
        if(isset($newUpdatedProduct)){
            foreach ($cart as $cartItem) {
                if($newUpdatedProduct["product_variant_id"] == $cartItem["product_variant_id"]){
                    array_push($tempCart,$newUpdatedProduct);
                }else{
                    array_push($tempCart,$cartItem);
                }
            }
            $_SESSION['cart'] = $tempCart;
        }
        $cart = $_SESSION['cart'];
        $totalMrp = 0;
        $mrpDiscount = 0;
        $totalSellingPrice = 0;
        $totalOrderAmount = 0;
        $coupon = null;
        $convenience_fee = 0;
        $finalCart = [];
        $sellerIds = [];
        $stockAvailablity = true;
        foreach ($cart as $cartItem) {
            $totalMrp += ($cartItem["mrp"] * $cartItem["quantity"]);
            $totalSellingPrice += $cartItem["total_price"];
            $totalOrderAmount = $totalSellingPrice;
            $mrpDiscount += $cartItem["total_seller_discount"];
            $totalSellerOriginalPrice = 0;
            $groupItems = [];
            if($cartItem["is_deal"] == "y"){
                $data = ['groupDesign'=>false,'convenience_fee'=>null,'seller_name'=>null,'products'=>[$cartItem]];
                array_push($finalCart,$data);
            }else{
                foreach ($cart as $product) {
                    if($cartItem["seller_id"] == $product["seller_id"] && $product["min_order_amount"] != null && $product["is_deal"] == "n" && !in_array($product["seller_id"], $sellerIds)){
                        $totalSellerOriginalPrice += $product["seller_original_price"] * $product["quantity"];
                        $groupItems[] = $product;
                    }
                }
                if($totalSellerOriginalPrice < $cartItem["min_order_amount"] && count($groupItems) > 0){
                    $convenience_fee += $cartItem["convenience_fee"];
                    $data = ['groupDesign'=>true,'convenience_fee'=>$cartItem["convenience_fee"],'seller_name'=>$cartItem["company_name"],'products'=>$groupItems];
                    array_push($finalCart,$data);
                    $sellerIds[] = $cartItem["seller_id"];
                }else{
                    if(!in_array($cartItem["seller_id"], $sellerIds)){
                        $data = ['groupDesign'=>false,'convenience_fee'=>null,'seller_name'=>null,'products'=>[$cartItem]];
                        array_push($finalCart,$data);
                    }
                }
            }           
            if($cartItem["stock"] <= 0){
                $stockAvailablity = false;
            }
        }
        if($totalSellingPrice >= COD_MIN_AMOUNT){
            $cod = 1;
            $cod_charge = count($_SESSION['cart']) * COD_CHARGE;
        }
        $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $totalOrderAmount AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
        $totalOrderAmount +=$handling_charges[0]->charge_amount;
        $handlingCharge = $handling_charges[0]->charge_amount;
        return ['cart'=>$finalCart,'totalMrp'=>$totalMrp,'totalSellingPrice'=>$totalSellingPrice,'totalOrderAmount'=>$totalOrderAmount,'coupon'=>$coupon,'handling_charges'=>$handlingCharge,'cod'=>$cod,'cod_charge'=>$cod_charge,'imagePrefixUrl'=>url(UPLOAD_PATH.'/'),'mrpDiscount'=>$mrpDiscount,"stockAvailablity"=>$stockAvailablity,'convenience_fee'=>$convenience_fee];
    }

    function guestAddToCart(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'product_variant_id' => 'required|integer',
                'quantity' => 'required|integer'
            ]);
            if($validator->fails()){
                return response(['success' => false,'msg'=>"Something went wrong!","errors"=>$validator->errors()->toJson()]);
            } 
            $cartCount = 0;
            $productVariantid = $request->product_variant_id;
            $productDetails = ProductVariant::selectRaw("product_variants.*,deals.deal_price,deals.start_time,deals.end_time,deals.max_orders,p.seller_id,variation_images.thumbnail, variation_images.images,categories.primary_attribute,categories.secondary_attribute,p.primary_category,CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_price ELSE product_variants.price END AS final_price,CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount,1)  ELSE TRUNCATE(product_variants.discount,1) END AS final_discount,b.name as brand_name")
                                            ->join('products as p','p.id','=','product_variants.product_id')
                                            ->leftjoin('deals', function($join){
                                                $join->on('deals.product_variant_id', '=', 'product_variants.id')
                                                    ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time');
                                                })
                                            ->join('brands AS b','b.id','p.brand_id')
                                            ->join('categories','categories.id','=','p.primary_category')
                                            ->leftjoin('view_product_images as variation_images','variation_images.product_variant_id','=','product_variants.id')
                                            ->whereRaw("product_variants.id = $productVariantid AND deals.deleted_at IS NULL")
                                            ->first();
            if($productDetails){
                $product_variant_options = ProductVariantOption::where('product_variant_id','=',$productVariantid)->get();
                $attributes = [];
                foreach ($product_variant_options as $options) {
                    if($options->attribute_id == $productDetails->primary_attribute){
                        $attribute = Attribute::find($options->attribute_id);
                        $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                            ->where('id','=',$options->attribute_option_id)
                                                            ->first();
                        if($attribute && $attributeOption){
                            $primaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'p');
                            array_push($attributes,$primaryAttribute);
                        }
                    }else if($options->attribute_id == $productDetails->secondary_attribute ){
                        $attribute = Attribute::find($options->attribute_id);
                        $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                            ->where('id','=',$options->attribute_option_id)
                                                            ->first();
                        if($attribute && $attributeOption){
                            $secondaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'s');
                            array_push($attributes,$secondaryAttribute);
                        }
                    }
                }
                $images = json_decode($productDetails->images);
                if($images == null){
                    $productDetails->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                }else{
                    $productDetails->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($productDetails->thumbnail-1)]);
                }
               if($productDetails->stock < $request->total_quantity ){
                    return response(['success' => false,'msg'=>"No Stock!"]);
                } 
                
                session_start();
                // unset($_SESSION['cart']);
                // die;
                // dd($_SESSION['cart']);
                $cart = isset($_SESSION['cart'])?$_SESSION['cart']:null;
                // dd($cart);
                if(!$cart) {
                    $checkDealProductAlreadyExists = false;
                    $seller = Seller::where('id','=', $productDetails->seller_id)->first();
                    $cartItem = [
                        ["product_id"=>$productDetails->product_id,
                         "product_variant_id"=>$productDetails->id,
                         "seller_id"=>$productDetails->seller_id,
                         "primary_category"=>$productDetails->primary_category,
                         "quantity"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? 1 : $request->quantity,
                         "mrp"=>$productDetails->mrp,
                         "total_seller_discount"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? ($productDetails->mrp - $productDetails->deal_price)  : $productDetails->final_discount * $request->quantity,
                         "price"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? $productDetails->deal_price : $productDetails->final_price,
                         "seller_discount"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? ($productDetails->mrp - $productDetails->deal_price)  : $productDetails->final_discount,
                         "total_price"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? $productDetails->deal_price  : $productDetails->final_price * $request->quantity,
                         "is_deal"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? 'y' : 'n',
                         "seller_pincode"=>$seller->company_pincode,
                         "coupon_id"=>null,
                         "coupon_discount"=>0,
                         "coupon_applied"=>null,
                         "coupon_type"=>null,
                         "customer_pincode"=>null,
                         "name"=>$productDetails->name,
                         "store_name"=>$seller->store_name,
                         "unique_id"=>$productDetails->unique_id,
                         "slug"=>$productDetails->slug,
                         "cod_enable"=>$seller->cod_enable,
                         "seller_original_price"=>$productDetails->price,
                         "thumbnail_path"=>$productDetails->thumbnail_path,
                         "min_order_amount"=>$seller->min_order_amount,
                         "convenience_fee"=>$seller->convenience_fee,
                         "company_name"=>$seller->company_name,
                         "brand_name"=>$productDetails->brand_name,
                         "stock"=>$productDetails->stock,
                         "attributes"=>$attributes,
                         "created_at"=>date('Y-m-d H:i:s'),
                         "final_discount"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? ((($productDetails->mrp *  $request->quantity) - $productDetails->deal_price)  * 100) /
                            ($productDetails->mrp * $request->quantity) : (($productDetails->final_discount * $request->quantity) * 100) / ($productDetails->mrp * $request->quantity)
                        ]
                    ];
                    $_SESSION['cart'] = $cartItem;
                    return response(['success' => true,'cartCount'=>count($_SESSION['cart'])]);
                }else{
                    // unset($_SESSION['cart']);
                    // die;
                    $cart = $_SESSION['cart'];
                    // $checkProductExistenceInCart = array_search($request->product_variant_id, array_column($cart, 'product_variant_id'));
                    $checkProductExistenceInCart = false;
                    $checkDealProductAlreadyExists = false;
                    foreach ($cart as $key => $cartItem) {
                        if($cartItem["product_variant_id"] == $request->product_variant_id){
                            $checkProductExistenceInCart = true;
                            if($cartItem["is_deal"]== 'y'){
                                $checkProductIsDealProduct = true;
                            }else{
                                $checkProductIsDealProduct = false;
                            }
                        }else if($cartItem["is_deal"] == 'y'){
                            $checkDealProductAlreadyExists = true;
                        }
                        if($checkDealProductAlreadyExists && $checkProductExistenceInCart){
                            break;
                        }
                    }
                    $tempCart = [];
                    if($checkProductExistenceInCart){
                        foreach ($cart as $key => $cartItem) {
                            if($cartItem["product_variant_id"] == $request->product_variant_id){
                                $quantity = $request->quantity + $cartItem["quantity"];
                                if($quantity <= 0 ){
                                   continue;
                                }
        
                                $cartItem["quantity"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? 1 : $quantity;
        
                                $cartItem["price"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? $productDetails->deal_price : $productDetails->final_price;
        
                                $cartItem["total_price"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? $productDetails->deal_price  : ($productDetails->final_price * $quantity);
        
                                $cartItem["seller_discount"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? ($productDetails->mrp - $productDetails->deal_price)  : $productDetails->final_discount;
        
                                $cartItem["total_seller_discount"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? ($productDetails->mrp - $productDetails->deal_price)  : ($productDetails->final_discount * $quantity);

                                $cartItem["final_discount"] = $productDetails->deal_price != null && !$checkDealProductAlreadyExists ? ((($productDetails->mrp *  $quantity) - $productDetails->deal_price)  * 100) /
                                ($productDetails->mrp *  $quantity) : (($productDetails->final_discount * $quantity) * 100) / ($productDetails->mrp *  $quantity);
        
                                $cartItem["is_deal"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? 'y' : 'n';
                            }
                            array_push($tempCart,$cartItem);
                        }
                        $_SESSION['cart'] = $tempCart;
                        $finalCart = $this->finalGuestCart('started');
                        return response(['success' => true,'cart'=>$finalCart,'cartCount'=>count($_SESSION['cart'])]);
                    }else{
                        $seller = Seller::where('id','=', $productDetails->seller_id)->first();
                        $cartItem = 
                            ["product_id"=>$productDetails->product_id,
                            "product_variant_id"=>$productDetails->id,
                            "seller_id"=>$productDetails->seller_id,
                            "primary_category"=>$productDetails->primary_category,
                            "quantity"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? 1 : $request->quantity,
                            "mrp"=>$productDetails->mrp,
                            "total_seller_discount"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? ($productDetails->mrp - $productDetails->deal_price)  : $productDetails->final_discount * $request->quantity,
                            "price"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? $productDetails->deal_price : $productDetails->final_price,
                            "seller_discount"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? ($productDetails->mrp - $productDetails->deal_price)  : $productDetails->final_discount,
                            "total_price"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? $productDetails->deal_price  : $productDetails->final_price * $request->quantity,
                            "seller_pincode"=>$seller->company_pincode,
                            "is_deal"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? 'y' : 'n',
                            "seller_pincode"=>$seller->company_pincode,
                            "coupon_id"=>null,
                            "coupon_discount"=>0,
                            "coupon_applied"=>null,
                            "coupon_type"=>null,
                            "customer_pincode"=>null,
                            "name"=>$productDetails->name,
                            "store_name"=>$seller->store_name,
                            "unique_id"=>$productDetails->unique_id,
                            "slug"=>$productDetails->slug,
                            "cod_enable"=>$seller->cod_enable,
                            "seller_original_price"=>$productDetails->price,
                            "thumbnail_path"=>$productDetails->thumbnail_path,
                            "min_order_amount"=>$seller->min_order_amount,
                            "convenience_fee"=>$seller->convenience_fee,
                            "company_name"=>$seller->company_name,
                            "brand_name"=>$productDetails->brand_name,
                            "stock"=>$productDetails->stock,
                            "attributes"=>$attributes,
                            "created_at"=>date('Y-m-d H:i:s'),
                            "final_discount"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? ((($productDetails->mrp * $request->quantity) - $productDetails->deal_price)  * 100) /
                            (($productDetails->mrp * $request->quantity)) : (($productDetails->final_discount * $request->quantity) * 100) / (($productDetails->mrp * $request->quantity))
                    ];
    
                        $cart = $_SESSION['cart'];
                        array_push($cart,$cartItem);
                        $_SESSION['cart'] = $cart;
                    }
                    
                    return response(['success' => true,'cartCount'=>count($_SESSION['cart'])]);
                }
            }else{
                return response(['success' => false,'msg'=>"Something went wrong!"]);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }


    public function removeItemGuestCart(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'product_variant_id' => 'required|integer'
            ]);
    
            if($validator->fails()){
                return response(['success' => false,'msg'=>"Something went wrong!","errors"=>$validator->errors()->toJson()]);
            }
            session_start();
            $cart = $_SESSION['cart'];
            $tempCart = [];
            foreach ($cart as $key => $cartItem) {
                if($cartItem["product_variant_id"] == $request->product_variant_id){
                    continue;
                }
                array_push($tempCart,$cartItem);
            }
            $_SESSION['cart'] = $tempCart;
            $finalCart = $this->finalGuestCart("started");
            return response(['success' => true,'cartCount'=>count($_SESSION['cart']),'finalCart'=>$finalCart]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }


    public function guestCartCount(){
        try{
            session_start();
            $count = isset($_SESSION['cart']) ? count($_SESSION['cart']):0;
            return $this->respond(['success'=>true,'count'=>$count]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function guestCoupons(){
        try{
            session_start();
            $currentDate = date('Y-m-d H:i:s');
            $coupons = Coupon::where('status','=', '1')
                               ->where('start_date', '<=', (string)date('Y-m-d H:i:s'))
                               ->where('expiry_date', '>=', (string)date('Y-m-d H:i:s'))
                               ->where('deleted_at','=',NULL)
                               ->get();
            $cart = isset($_SESSION['cart']) ? $_SESSION['cart'] : null;
            if($cart){
            $couponsTemp2 = [];
            foreach ($cart as $cartItem) {
              foreach ($coupons as $coupon) {
                $id = $coupon->id;
                $seller_id = (string)$cartItem["seller_id"];
                $primary_category = (string)$cartItem["primary_category"];
                $p_variant = (string)$cartItem["product_variant_id"];
                // $cptemp = Coupon::whereRaw("((seller_ids is null AND category_ids is null AND product_varient_ids is null) OR (json_contains(seller_ids,'\"$seller_id\"') OR json_contains(category_ids,'\"$primary_category\"') OR json_contains(product_varient_ids,'\"$p_variant\"'))) AND id = $id")->first();
                $cptemp = Coupon::selectRaw("coupons.*,coupon_groups.product_varient_ids")
                                    ->leftjoin("coupon_groups","coupon_groups.id","=","coupons.coupon_group_id")
                                    ->whereRaw("((coupons.seller_ids is null AND coupons.category_ids is null AND coupons.product_varient_ids is null AND coupon_groups.product_varient_ids is null) OR (json_contains(coupons.seller_ids,'\"$seller_id\"') OR json_contains(coupons.category_ids,'\"$primary_category\"') OR json_contains(coupons.product_varient_ids,'\"$p_variant\"') OR json_contains(coupon_groups.product_varient_ids,'\"$p_variant\"'))) AND coupons.id = $id")
                                    ->first();
                if($cptemp){
                    $found = 0;
                    foreach ($couponsTemp2 as $coupon) {    
                        if($coupon->id == $cptemp->id){
                            $found = 1;
                        }
                    }
                    if($found == 0){
                        array_push($couponsTemp2, $cptemp);
                    } 
                  }
              }
            }
    
            $couponsTemp= [];
            foreach ($couponsTemp2 as $coupon) {
                if($coupon->dis_type == 'f' && $coupon->sub_type == 's'){
                    $totalSaving = 0;
                    $discountMRPBeforecoupon = 0;
                    foreach ($cart as $cartItem) {
                        $totalSaving += $cartItem["total_seller_discount"];
                        $discountMRPBeforecoupon += $cartItem["total_price"];
                    }
                    $totalSaving += $coupon->amount;
                    $discountMrpAfterCoupon = $discountMRPBeforecoupon - $coupon->amount;
                    $coupon->savings = $totalSaving;
                    $coupon->discountAfterCoupon = $discountMrpAfterCoupon;
                    array_push($couponsTemp,$coupon);
                }else if($coupon->dis_type == 'p' && $coupon->sub_type == 's'){
                    $totalSaving = 0;
                    $currentDiscountPercentage = 0;
                    $productsDiscounts = [];
                    $couponDiscountPercentage = 0;
                    $couponDiscountAmount = 0;
                    $totalDiscountOfaProductInPercentage = 0;
                    $totalDiscountOfaProductInRupees = 0;
                    foreach ($cart as $cartItem){
                        $currentDiscountPercentage = ($cartItem["total_seller_discount"] * 100)/($cartItem["mrp"] * $cartItem["quantity"]);
                        $couponDiscountAmount = ($coupon->amount/100) * $cartItem["total_price"];
                        $couponDiscountPercentage = ($couponDiscountAmount * 100) / $cartItem["total_price"];
                        $totalDiscountOfaProductInPercentage = (($couponDiscountAmount + $cartItem["total_seller_discount"]) * 100)/($cartItem["mrp"] * $cartItem["quantity"]);
                        $totalDiscountOfaProductInRupees = $couponDiscountAmount + $cartItem["total_seller_discount"];
                        $totalSaving += $totalDiscountOfaProductInRupees;
                        array_push($productsDiscounts,['product_variant_id'=>$cartItem["product_variant_id"],
                                                        'currentDiscountPercentage'=>$currentDiscountPercentage,
                                                        'couponDiscountAmount'=>$couponDiscountAmount,
                                                        'totalDiscountOfaProductInPercentage'=>$totalDiscountOfaProductInPercentage,
                                                        'totalDiscountOfaProductInRupees'=>$totalDiscountOfaProductInRupees]);
                    }
                    $coupon->savings = $totalSaving;
                    $coupon->productsDiscounts = $productsDiscounts;
                    array_push($couponsTemp,$coupon);
                }else if($coupon->dis_type == 'p' && $coupon->sub_type == 'm'){
                    $totalSaving = 0;
                    $productsDiscounts = [];
                    $totalDiscountOfaProductInPercentage = 0;
                    $totalDiscountOfaProductInRupees = 0;
                    foreach ($cart as $cartItem){
                        $couponDiscountAmount = ($coupon->amount/100) * ($cartItem["mrp"] * $cartItem["quantity"]);
                        if($couponDiscountAmount > $cartItem["total_seller_discount"]){
                            $totalSaving += $couponDiscountAmount;
                            array_push($productsDiscounts,['product_variant_id'=>$cartItem["product_variant_id"],
                                                           'couponDiscountAmount'=>$couponDiscountAmount,
                                                           'sellerDiscountAmount'=>$cartItem["total_seller_discount"]]);
                        }else{
                            $totalSaving += $cartItem["total_seller_discount"];
                            array_push($productsDiscounts,['product_variant_id'=>$cartItem["product_variant_id"],
                                                           'couponDiscountAmount'=>0,
                                                           'sellerDiscountAmount'=>$cartItem["total_seller_discount"]]);
                        }
                    }
                    $coupon->savings = $totalSaving;
                    $coupon->productsDiscounts = $productsDiscounts;
                    array_push($couponsTemp,$coupon);
                }
            }
            return response(['success' => true,'coupons'=>$couponsTemp]); 
            }else{
                return response(['success' => false,'msg'=>"No Cart Items!"]);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function guestVirtualCart(Request $request){
        try{
            session_start();
            $cart = isset($_SESSION['cart']) ? $_SESSION['cart'] : null;
            if($cart){
                $coupon = Coupon::find($request->cp_id);
                $totalMrp = 0;
                $totalSellingPrice = 0;
                $totalOrderAmount = 0;
                $tempCart = [];
                $path = url(UPLOAD_PATH);
                if($coupon->dis_type == 'f' && $coupon->sub_type == 's'){
                    foreach ($cart as $cartItem) {
                        $totalMrp += ($cartItem["mrp"] * $cartItem["quantity"]);
                        $totalSellingPrice += $cartItem["total_price"];
                        $totalOrderAmount = $totalSellingPrice;
                        $cartItem["thumbnail_image_path"] = $cartItem["thumbnail_path"];
                        $cartItem["final_price"] = $cartItem["total_price"];
                        $cartItem["final_discount"] = number_format($cartItem["total_seller_discount"] * 100 / ($cartItem["mrp"] * $cartItem["quantity"]),1);
                        array_push($tempCart,$cartItem);
                    }
                    $totalSellingPrice -= $coupon->amount;
                    $totalOrderAmount = $totalSellingPrice;
                    $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $totalOrderAmount AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
                    $totalOrderAmount +=$handling_charges[0]->charge_amount;
                    $handlingCharge = $handling_charges[0]->charge_amount;
                    if($totalOrderAmount >= $coupon->min_cart_amount){
                        return response(['success'=>true,'cart'=>$tempCart,'totalMrp'=>$totalMrp,'totalSellingPrice'=>$totalSellingPrice,'totalOrderAmount'=>$totalOrderAmount,'handling_charges'=>$handlingCharge,'imgUrl'=>url(UPLOAD_PATH)]);
                    }else{
                        $msg = "For this coupon to be applied minimum cart amount must be atlease ".$coupon->min_cart_amount;
                        return response(['success'=>false,'msg'=>$msg]);
                    }
                }else if($coupon->dis_type == 'p' && $coupon->sub_type == 's'){
                    foreach ($cart as $cartItem) {
                        $totalMrp += ($cartItem["mrp"] * $cartItem["quantity"]);
                        $totalSellingPrice += $cartItem["total_price"];
                        $couponDiscountAmount = ($coupon->amount / 100) * $cartItem["total_price"];
                        $totalSellingPrice -= $couponDiscountAmount;
                        $cartItem["thumbnail_image_path"] = $cartItem["thumbnail_path"];
                        $totalOrderAmount = $totalSellingPrice;
                        $cartItem["final_price"] = $cartItem["total_price"] - $couponDiscountAmount;
                        $cartItem["final_discount"] = number_format(($cartItem["total_seller_discount"] + $couponDiscountAmount) * 100 / ($cartItem["mrp"] * $cartItem["quantity"]),1);
                        array_push($tempCart,$cartItem);
                    }
                    $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $totalOrderAmount AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
                    $totalOrderAmount +=$handling_charges[0]->charge_amount;
                    $handlingCharge = $handling_charges[0]->charge_amount;
                    if($totalOrderAmount >= $coupon->min_cart_amount){
                        return response(['success'=>true,'cart'=>$tempCart,'totalMrp'=>$totalMrp,'totalSellingPrice'=>$totalSellingPrice,'totalOrderAmount'=>$totalOrderAmount,'handling_charges'=>$handlingCharge,'imgUrl'=>url(UPLOAD_PATH)]);
                    }else{
                        $msg = "For this coupon to be applied minimum cart amount must be atlease ".$coupon->min_cart_amount;
                        return response(['success'=>false,'msg'=>$msg]);
                    }
                }else if($coupon->dis_type == 'p' && $coupon->sub_type == 'm'){
                    foreach ($cart as $cartItem) {
                        $totalMrp += ($cartItem["mrp"] * $cartItem["quantity"]);
                        $couponDiscountAmount = ($coupon->amount / 100) * ($cartItem["mrp"] * $cartItem["quantity"]);
                        if($couponDiscountAmount > $cartItem["total_seller_discount"]){
                            $cartItem["final_price"] = ($cartItem["mrp"] * $cartItem["quantity"]) - $couponDiscountAmount;
                            $cartItem["final_discount"] = $coupon->amount;
                            $cartItem["thumbnail_image_path"] = $cartItem["thumbnail_path"];
                            $totalSellingPrice += $cartItem["final_price"];
                            $totalOrderAmount = $totalSellingPrice;
                        }else{
                            $cartItem["final_price"] = $cartItem["total_price"];
                            $cartItem["final_discount"] = number_format($cartItem["total_seller_discount"] * 100 / ($cartItem["mrp"] * $cartItem["quantity"]),1);
                            $cartItem["thumbnail_image_path"] = $cartItem["thumbnail_path"];
                            $totalSellingPrice += $cartItem["final_price"];
                            $totalOrderAmount = $totalSellingPrice;
                        }
                        array_push($tempCart,$cartItem);
                    }
                    $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $totalOrderAmount AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
                    $totalOrderAmount +=$handling_charges[0]->charge_amount;
                    $handlingCharge = $handling_charges[0]->charge_amount;
                    if($totalOrderAmount >= $coupon->min_cart_amount){
                        return response(['success'=>true,'cart'=>$tempCart,'totalMrp'=>$totalMrp,'totalSellingPrice'=>$totalSellingPrice,'totalOrderAmount'=>$totalOrderAmount,'handling_charges'=>$handlingCharge,'imgUrl'=>url(UPLOAD_PATH)]);
                    }else{
                        $msg = "For this coupon to be applied minimum cart amount must be atlease ".$coupon->min_cart_amount;
                        return response(['success'=>false,'msg'=>$msg]);
                    }
                }
            }else{
                return response(['success' => false,'msg'=>"No Products in Cart."]);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }
}
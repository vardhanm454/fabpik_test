<?php

namespace App\Http\Controllers\Api\Customer;

use DB;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use DateTime;
use PDF;

// Models
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\Review;
use App\Models\OrderStatus;
use App\Models\Coupon;
use App\Models\OrderHistory;
use App\Models\ProductVariantOption;
use App\Models\Attribute;

class PurchaseHistoryController extends ApiController
{
    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     *
     *
     * get list of orders list
     */
    public function OrderList(Request $request)
    {
        try{
            if (auth('api')->user()) {
                $filter = $request->id;
                $user_id = auth('api')->user()->ref_id;
                $orders = Order::select("order_details.id AS order_detail_id","order_details.quantity","order_details.child_order_id","order_details.name","order_details.product_id","order_details.product_variant_id","order_details.order_id","order_details.created_at","orders.payment_type","order_statuses.user_text AS order_status","shipping_statuses.name AS shipping_status","order_details.images","order_details.thumbnail","order_details.shipping_status_id",'order_details.order_status_id',DB::raw("DATE_FORMAT(order_details.created_at, '%d %b %Y %l:%i %p') AS created_date"),DB::raw("IF(order_details.coupon_discount > 0, order_details.total + order_details.coupon_discount , order_details.total) AS total"),"order_details.cod_charge","orders.parent_order_id")
                                         ->join('order_details','order_details.order_id','=','orders.id')
                                         ->join('order_statuses','order_details.order_status_id','=','order_statuses.id')
                                         ->join('shipping_statuses','order_details.shipping_status_id','=','shipping_statuses.id')
                                         ->whereRaw("orders.customer_id = $user_id");
                if($request->sortBy == "Returned"){
                    $orders->whereIn('shipping_statuses.id', [11,12]);
                }else if($request->sortBy == "Order in Process"){
                    $orders->whereIn('order_statuses.id', [1]);
                }else if($request->sortBy == "Delivered"){
                    $orders->whereIn('order_statuses.id', [8]);
                }else if($request->sortBy == "Cancelled"){
                    $orders->whereIn('order_statuses.id', [2]);
                }
                $orders = $orders->orderBy("order_details.created_at","DESC")->get();
                $returns = [];
                if ($orders) {
                    foreach ($orders as $order) {
                        // $product->discount = number_format(($product->discount * 100) / $product->mrp, 1) + 0;
                        $images = json_decode($order->images);
                        // $order->created_date = date_format($order->created_at, "Y-m-d");
                        if($order->shipping_status_id == 11){
                            $order->color = 'red';
                        }
                        else if($order->order_status_id == 2){
                            $order->color = 'orange';
                        }
                        else if($order->order_status_id == 1 || $order->order_status_id == 4 || $order->shipping_status_id == 1 || $order->shipping_status_id == 2 || $order->shipping_status_id == 3 || $order->shipping_status_id == 4 ){
                            $order->color = 'blue';
                        }else if($order->order_status_id == 8 || $order->shipping_status_id == 5){
                            $order->color = 'green';
                        }
                        if ($images) {
                            if ($order->thumbnail != null) {
                                $order->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($order->thumbnail-1)]);
                            } else {
                                $order->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                            }
                            array_push($returns, $order);
                        } else {
                            $order->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                            array_push($returns, $order);
                        }
                    }
                }
                return $this->respond(['success' => true,'orders'=>$orders]);
            } else {
                return response(['success' => false], 401);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
      *
      *
      * get order status list
      */
    public function OrderStatusList()
    {
        if (auth('api')->user()) {
            $final_status_list=[];
            $order_status_list = OrderStatus::whereIn('id', [1,2,5,8])->select('id', 'name')->get();
            foreach ($order_status_list as $statusItem) {
                if ($statusItem->id == 5) {
                    $data = array('id'=>$statusItem->id,'type' => 'Returned','color'=>'#ffbdbd');
                    array_push($final_status_list, $data);
                } elseif ($statusItem->id == 1) {
                    $data = array('id'=>$statusItem->id,'type' => 'Under Process','color'=>'#8ca5ff');
                    array_push($final_status_list, $data);
                } elseif ($statusItem->id == 8) {
                    $data = array('id'=>$statusItem->id,'type' => 'Delivered','color'=>'#a7ffbb');
                    array_push($final_status_list, $data);
                } elseif ($statusItem->id == 2) {
                    $data = array('id'=>$statusItem->id,'type' => 'Cancelled','color'=>'#ffbd86');
                    array_push($final_status_list, $data);
                }
            }
            return $this->respond($final_status_list);
        } else {
            return response(['success' => false], 401);
        }
    }
    /**
     *
     *
     * get  product details
     */
    public function getproductdetailsById($child_order_id)
    {
        try{
            $user_id = auth('api')->user()->ref_id;
            $order = Order::select("order_details.id AS order_detail_id", "order_details.child_order_id", "order_details.name", "order_details.product_id", "order_details.product_variant_id", "order_details.order_id", "order_details.created_at", "orders.payment_type", "order_statuses.user_text AS order_status", "shipping_statuses.name AS shipping_status", "order_details.images", "order_details.thumbnail", "order_details.tracking_no", "order_details.shipment_id", "coupons.code", "orders.shipping_first_name", "orders.shipping_last_name", "orders.shipping_email", "orders.shipping_mobile", "orders.shipping_address1", "orders.shipping_address2", "orders.shipping_city", "orders.shipping_state", "orders.shipping_pincode", "sellers.company_name", "order_details.quantity","order_details.price","order_details.deal_price", "orders.handling_charge","order_details.cod_charge","order_details.dis_type","order_details.sub_type","order_details.coupon_id","order_details.coupon_discount","order_details.shipping_model",DB::raw("DATE_FORMAT(order_details.created_at, '%d %b %Y %l:%i %p') AS created_date"),DB::raw("(order_details.mrp * order_details.quantity) AS mrp"),DB::raw(" IF(order_details.coupon_discount > 0, order_details.total + order_details.coupon_discount , order_details.total) AS total"),"order_details.total_seller_discount",DB::raw("IF(order_details.dis_type = 'p' AND order_details.sub_type = 'm' AND order_details.total_seller_discount < order_details.coupon_discount, 0 , order_details.total_seller_discount) AS total_seller_discount"))
                                         ->join('order_details', 'order_details.order_id', '=', 'orders.id')
                                         ->join('order_statuses', 'order_details.order_status_id', '=', 'order_statuses.id')
                                         ->leftjoin('coupons', 'coupons.id', '=', 'order_details.coupon_id')
                                         ->join('sellers', 'sellers.id', '=', 'order_details.seller_id')
                                         ->join('shipping_statuses', 'order_details.shipping_status_id', '=', 'shipping_statuses.id')
                                         ->whereRaw("orders.customer_id = $user_id AND order_details.child_order_id = $child_order_id")
                                         ->first();
            
            $returns = [];
            $showReturnOption = false;      
            $order_statuses = [];
            $shipping_statuses = [];
            if ($order) {
                // $discount = ((($order->mrp * $order->quantity) - ($order->price * $order->quantity)) * 100)/($order->mrp * $order->quantity);
                $discount = ($order->total_seller_discount * 100)/($order->mrp * $order->quantity);
    
                // if($order->coupon_id != null){
                //     if($order->dis_type == 'f' && $order->sub_type == 's'){
                //         $order->price = $order->price * $order->quantity;
                //         $order->total = ($order->total + ($order->payment_type == 'c' ? $order->cod_charge : 0 )) - $order->coupon_discount;
                //     }else if($order->dis_type == 'p' && $order->sub_type == 's'){
                //         $order->price = ($order->price * $order->quantity) - $order->coupon_discount;
                //         $order->total = ($order->total + ($order->payment_type == 'c' ? $order->cod_charge : 0 )) - $order->coupon_discount;
                //     }else if($order->dis_type == 'p' && $order->sub_type == 'm'){
                //         if($order->coupon_discount > 0){
                //             $order->price = ($order->mrp * $order->quantity) - $order->coupon_discount;
                //             $order->total = (($order->mrp * $order->quantity) + ($order->payment_type == 'c' ? $order->cod_charge : 0 )) - $order->coupon_discount;
                //         }else{
                //             $order->price = ($order->price * $order->quantity);
                //             $order->total = ($order->total+ ($order->payment_type == 'c' ? $order->cod_charge : 0 )) - $order->coupon_discount;
                //         }
                //     }
                // }else{
                //     $order->price = ($order->price * $order->quantity);
                //     $order->total = ($order->total + ($order->payment_type == 'c' ? $order->cod_charge : 0 ));
                // }
    
                $order_statuses = OrderHistory::where('order_detail_id', '=', $order->order_detail_id)
                                                    ->pluck('order_status_id');
                $shipping_statuses = OrderHistory::where('order_detail_id', '=', $order->order_detail_id)
                                                      ->pluck('shipping_status_id');
                $deliveryStatus = OrderHistory::where('order_detail_id', '=', $order->order_detail_id)
                                                ->whereRaw("order_status_id = 8 AND shipping_status_id = 5")
                                                ->first();
                if ($deliveryStatus) {
                    $currentDate = new DateTime(date('Y-m-d H:i:s'));
                    //    $currentDate = new DateTime("2021-01-26 11:50:48");
                    $deliveryDate7DaysAddition = new DateTime(date('Y-m-d H:i:s', strtotime($deliveryStatus->created_at. ' + 7 days')));
                    if ($currentDate < $deliveryDate7DaysAddition) {
                        $showReturnOption = true;
                    } else {
                        $showReturnOption = false;
                    }
                }
                     
                // $product->discount = number_format(($product->discount * 100) / $product->mrp, 1) + 0;
    
                $images = json_decode($order->images);
                // $order->created_date = date_format($order->created_at, "Y-m-d");
                if ($images) {
                    if ($order->thumbnail != null) {
                        $order->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($order->thumbnail-1)]);
                    } else {
                        $order->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                    }
                } else {
                    $order->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                }
                return $this->respond(['success'=>true,'order'=>$order,'discount'=>$discount,'showReturnOption'=>$showReturnOption,'order_statuses'=>$order_statuses,'shipping_statuses'=>$shipping_statuses]);
    
            }else{
                return $this->respond(['success'=>false,'msg'=>'No Such Order!']);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function customerParentOrders(){
        try{
            $user_id = auth('api')->user()->ref_id;
            $orders = Order::select("orders.*",DB::raw("DATE_FORMAT(orders.created_at, '%d %b %Y %l:%i %p') AS created_date"),DB::raw("SUM(od.mrp * od.quantity) AS totalMrp"),DB::raw(" IF(od.dis_type = 'p' AND od.sub_type = 'm' AND od.total_seller_discount < od.coupon_discount, orders.coupon_discount , SUM(od.total_seller_discount) + orders.coupon_discount) AS discounts"),DB::raw("orders.total_convenience_fee + orders.cod_charge + orders.handling_charge AS charges"))
                             ->join("order_details as od","od.order_id", "=","orders.id")
                             ->whereRaw("customer_id = $user_id")
                             ->orderBy("orders.created_at","DESC")
                             ->groupBy("od.order_id")
                             ->get();
            return $this->respond(['success'=>true,'orders'=>$orders]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }

    /**
     ** Customer Invoice Download
    */

    public function customerInvoice($child_order_id)
    {
        try{
            $this->data['orderDetails'] = OrderDetail::with('order', 'seller', 'productVarient', 'orderHistory')->where('id', $child_order_id)->first();
            $productVarientOptions = ProductVariantOption::where('product_variant_id', $this->data['orderDetails']->productVarient->id)->get();
            $attributes = [];
            foreach ($productVarientOptions as $i=>$options) {
                $getAttributes = Attribute::with(['attributeOptions' => function ($q) use ($options) {
                    $q->where('id', '=', $options->attribute_option_id);
                }])->where('id', $options->attribute_id)->first();
    
                $attributes[$i]['atrributeName'] = $getAttributes->display_name;
                $attributes[$i]['atrributeValue'] = $getAttributes->attributeOptions[0]->option_name;
            }
            $this->data['attributeOptions'] = $attributes;
    
            $pdf = PDF::loadView('invoices.customerinvoice', $this->data);
            $path = UPLOAD_PATH.'/'.'customerinvoice/';
            // $path = 'uploads/customerinvoice/';
            if (!file_exists($path)) { 
                // path does not exist
                mkdir($path, 0777);
                chmod($path, 0777);
            }
    
            $filename = $path.$this->data['orderDetails']->child_order_id.'.pdf';
            if($pdf->save($filename)){
                return response()->json(['success'=>'true', 'url'=>url($filename)]);
            }else{
                return response()->json(['success'=>'false', 'url'=>'']);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }
    /**
     *
     *
     * get  product details
     */
    public function getShippingAddressById($id)
    {
        if (auth('api')->user()) {
            $order_id = OrderDetail::where('id', $id)->select('order_id')->first();
            $shipping_address= Order::where('id', $order_id->order_id)->where('customer_id', auth('api')->user()->ref_id)->get();
            return $this->respond($shipping_address);
        } else {
            return response(['success' => false], 401);
        }
    }

    /**
     *
     *
     * cancel order
     */
    public function cancelOrder(Request $request)
    {
        try{
            $result = __changeStatus('2', [$request->id]);
            $result = json_decode($result);
            if($result->status == 1){
                return response(['result'=>$result,'success'=>true]);
            }else{
                return response(['result'=>$result,'success'=>false]);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function returnOrder(Request $request)
    {
        try{
            $result = __changeStatus('5', [$request->id]);
            return response(['result'=>$result,'success'=>true]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }
    /**
     *
     *
     *  order summary
     */
    public function OrdersummaryById(Request $request)
    {
        if (auth('api')->user()) {
            $order_summary=[];
            $final_order_summary=[];
            $Ordersummary =  Order::select("order_details.id as order_detail_id", "order_details.created_at", "orders.payment_type", "order_statuses.user_text")
            ->join('order_details', 'order_details.order_id', '=', 'orders.id')
            ->join('order_statuses', 'order_details.order_status_id', '=', 'order_statuses.id')
            ->where('orders.customer_id', auth('api')->user()->ref_id)
            ->where('order_details.id', $request->id)->get();
            foreach ($Ordersummary as $summary) {
                $summary->date = date('M d Y', strtotime($summary->created_at));
            }
        
            $orderhistory = OrderHistory::select("order_histories.order_status_id as orderstatusId", "order_statuses.user_text as orderstatus", "order_histories.shipping_status_id as shippingstatusId", "shipping_statuses.user_text as shippingstatus")
            ->join('order_details', 'order_details.id', '=', 'order_histories.order_detail_id')
            ->join('order_statuses', 'order_histories.order_status_id', '=', 'order_statuses.id')
            ->join('shipping_statuses', 'order_histories.shipping_status_id', '=', 'shipping_statuses.id')
            ->where('order_details.id', $request->id)
            ->get();

            //   print_r($orderhistory);exit;

            foreach ($orderhistory as $Item) {
                if ($Item->orderstatusId == 1 && $Item->shippingstatusId == 1) {
                    $status = "Ordered";
                } elseif ($Item->orderstatusId == 1 && $Item->shippingstatusId == 2) {
                    $status = "Ordered";
                } elseif ($Item->orderstatusId == 4 && $Item->shippingstatusId == 2) {
                    $status = "Packed";
                } elseif ($Item->orderstatusId == 4 && $Item->shippingstatusId == 3) {
                    $status = "Packed";
                } elseif ($Item->orderstatusId == 4 && $Item->shippingstatusId == 4) {
                    $status = "Shipped";
                } elseif ($Item->orderstatusId == 8 && $Item->shippingstatusId == 5) {
                    $status = "Delivered";
                } elseif ($Item->orderstatusId == 9 && $Item->shippingstatusId == 5) {
                    $status = "Delivered";
                }
                array_push($order_summary, $status);
            }

            $value = array_values(array_unique($order_summary));
            $status=[];
            foreach ($value as $item) {
                $status[$item] = $item;
            }
            $finalstatus[]=$status;

            return ['order_summary'=>$Ordersummary,'status'=>$finalstatus];
        } else {
            return response(['success' => false], 401);
        }
    }

    /**
     *
     *
     *   Order Inprocess Count
     */

    public function userOrdersInfoCount(){
        try{
            $user_id = auth('api')->user()->ref_id;
    
            $OrderInprocess =  Order::join('order_details','order_details.order_id','=','orders.id')
                                    ->where('order_details.order_status_id','=',1)
                                    ->where('order_details.shipping_status_id','=',1)
                                    ->where('customer_id','=',auth('api')->user()->ref_id)
                                    ->count();
    
            $NextDelivery =  Order::join('order_details','order_details.order_id','=','orders.id')
                                     ->whereRaw("orders.customer_id = $user_id AND (order_details.order_status_id = 4 OR order_details.order_status_id = 10 OR order_details.shipping_status_id = 2 OR order_details.shipping_status_id = 3 OR order_details.shipping_status_id = 4)")
                                     ->count();
    
            return response(['success' => true,'ordersInProcess'=>$OrderInprocess,'nextDelivery'=>$NextDelivery]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }

    public function OrderInprocessCount()
    {
        $OrderInprocess =  Order::join('order_details','order_details.order_id','=','orders.id')
                                    ->where('order_details.order_status_id','=',1)
                                    ->where('order_details.shipping_status_id','=',1)
                                    ->where('customer_id','=',auth('api')->user()->ref_id)
                                    ->count();
        return $this->respond($OrderInprocess);
    }

    /**
     *
     *
     *   Order Inprocess Count 
     */
    public function NextDeliveryCount()
    {
        $user_id = auth('api')->user()->ref_id;
        $NextDelivery =  Order::join('order_details','order_details.order_id','=','orders.id')
                                 ->whereRaw("orders.customer_id = $user_id AND (order_details.order_status_id = 4 OR order_details.order_status_id = 10 OR order_details.shipping_status_id = 2 OR order_details.shipping_status_id = 3 OR order_details.shipping_status_id = 4)")
                                 ->count();

        $NextDeliveryCount = count($NextDelivery);
        return $this->respond($NextDeliveryCount);
    }
}

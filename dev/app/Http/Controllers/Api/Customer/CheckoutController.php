<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Resources\WishlistCollection;
// Models
use App\Models\Wishlist;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\CustomerAddress;
use App\Models\Cart;
use App\Http\Controllers\Api\CartController;
use Seshac\Shiprocket\Shiprocket;

class CheckoutController extends ApiController
{
    protected $cartController;
    public function __construct(CartController $cartController) {
        $this->cartController = $cartController;
        $this->middleware('auth:api');
    }

    public function getCheckoutDetails(Request $request){
        try{
            $defaultAddress = CustomerAddress::select("states.name AS state","customer_addresses.*")
                                                ->join("states","customer_addresses.state_id",'=','states.id')
                                                ->where('customer_id','=',auth('api')->user()->ref_id)
                                                ->where('is_default','=',1)
                                                ->first();
            $isUserDetailsSet = isset(auth('api')->user()->email) && isset(auth('api')->user()->name) ? true : false;
            if($defaultAddress){
                $finalCart = $this->cartController->finalCart();
                $token =  Shiprocket::getToken();
                // return $this->respond($shiprocketDelivery);
                $updatedCart =[];
                foreach ($finalCart['cart'] as $cartItem) {
                    $pincodeDetails = [
                        'pickup_postcode' => $cartItem->seller_pincode,
                        'delivery_postcode' => $defaultAddress->pincode,
                        'weight' => $cartItem->shipping_weight,
                        'cod' => 0,
                        'couriers_type'=>true
                    ];
                    $response = Shiprocket::courier($token)->checkServiceability($pincodeDetails);
                    $shiprocketDelivery = json_decode($response);
                    // $data = ['success'=>true,'address'=>$shiprocketDelivery];
    
                    if($shiprocketDelivery->status == 404){
                        $cartItem->pincodeError = true;
                        $cartItem->estimatedDelivery = null;
                        // return $this->respond(['success'=>2,'errType'=>'pincode','address'=>$defaultAddress,'msg'=>$shiprocketDelivery->message,'finalCart'=>$finalCart,'isUserDetailsSet'=>$isUserDetailsSet]);
                    }else{
                        $cartItem->pincodeError = false;
                        $etd_hours = $shiprocketDelivery->data->available_courier_companies[0]->etd_hours;
                        $totalEtd = $cartItem->min_ship_hours + $etd_hours;
                        $hoursToDays = ceil($totalEtd/24);
                        $currentDate = date("d M Y");
                        $currentDateToStoreInCart = date("Y-m-d");
                        $addedDays = date('d M Y', strtotime($currentDate. ' + '."$hoursToDays".' days'));
                        $addedDaysToStoreInCart = date('Y-m-d', strtotime($currentDateToStoreInCart. ' + '."$hoursToDays".' days'));
                        $cartItem->estimatedDelivery = $addedDays;
                        Cart::where('id','=',$cartItem->id)->update(['etd'=>$addedDaysToStoreInCart]);
                    }
                    array_push($updatedCart,$cartItem);
                }
                $finalCart['cart'] = $updatedCart; 
                $data = ['success'=>true,'address'=>$defaultAddress,'finalCart'=>$finalCart,'isUserDetailsSet'=>$isUserDetailsSet,'cod_min_amount'=>COD_MIN_AMOUNT,'use_my_location_button'=>USE_MY_LOCATION_BUTTON,'google_auto_suggestions'=>GOOGLE_AUTOSUGGESTIONS];
                return $this->respond($data); 
            }else{
                $finalCart = $this->cartController->finalCart();
                $data = ['success'=>2,'errType'=>'addr','finalCart'=>$finalCart,'isUserDetailsSet'=>$isUserDetailsSet,'msg'=>'Add ADDRESS to continue further!','cod_min_amount'=>COD_MIN_AMOUNT,'use_my_location_button'=>USE_MY_LOCATION_BUTTON,'google_auto_suggestions'=>GOOGLE_AUTOSUGGESTIONS];
                return $this->respond($data); 
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }
}
<?php

namespace App\Http\Controllers\Api\Customer;
use DB;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
// Models
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\Review;

class ReviewController extends ApiController
{
    
    public function __construct() {
        $this->middleware('auth:api',['except' => ['getReviewedProductReadonly','getReviews']]);
    }

    public function getReviews(Request $request,$pid){
        try{
            $reviews = Review::selectRaw("reviews.review_title,reviews.rating,reviews.product_variant_id,reviews.review_comment,reviews.published,customers.name,customers.email,customers.mobile,vm.images,vm.thumbnail,DATE_FORMAT(reviews.updated_at, '%D %b, %Y') as date")
                                ->join("customers","customers.id",'=',"reviews.customer_id")
                                ->join('product_variants','product_variants.id','=','reviews.product_variant_id')
                                ->join('products as p','p.id','=','reviews.product_id')
                                ->join('categories as c','c.id','=','p.primary_category')
                                ->join('product_variant_options as pvo','pvo.product_variant_id','=','product_variants.id')
                                ->leftjoin('variation_images as vm', function($join){
                                    $join->on('vm.attribute_option_id', '=', 'pvo.attribute_option_id');
                                    $join->on('vm.product_id','=','reviews.product_id');
                                })
                                ->whereRaw("pvo.attribute_id = c.primary_attribute AND reviews.product_id = $pid AND pvo.deleted_at IS NULL AND product_variants.deleted_at IS NULL AND p.deleted_at IS NULL AND p.status = 1 AND c.status = 1 AND c.deleted_at IS NULL AND reviews.published = 1");
                                
            switch($request->sortBy) {
                case 'recent':
                    $reviews = $reviews->orderBy('reviews.updated_at', 'DESC');
                    break;
                case 'positive':
                    $reviews = $reviews->orderBy('reviews.rating', 'DESC');
                    break;
                case 'negative':
                    $reviews = $reviews->orderBy('reviews.rating', 'ASC');
                    break;
                default:
                    $reviews = $reviews->orderBy('reviews.updated_at', 'DESC');
            }
            $reviews = $reviews->get();
            $avgRating = Review::selectRaw("AVG(reviews.rating) AS avg_rating")->whereRaw("reviews.product_id = $pid AND reviews.published = 1")->get();
            $reviewStats = Review::reviewStats($pid);  
    
            return $this->respond(['success'=>true,'reviews'=>$reviews,'stats'=>$reviewStats,'avgRating'=> $avgRating[0]->avg_rating]);       
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function getCustomerReviews(){
        try{
            $customer_id = auth('api')->user()->ref_id;
            $reviews = Order::selectRaw("reviews.id,reviews.review_title,reviews.rating,reviews.review_comment,reviews.published,customers.name,customers.email,customers.mobile,vm.images,vm.thumbnail,DATE_FORMAT(reviews.updated_at, '%D %b, %Y') as published_date,order_details.product_variant_id,order_details.product_id,product_variants.name as pv_name,b.name as brand_name,product_variants.unique_id,product_variants.slug,order_details.child_order_id")
                                    ->join('order_details','order_details.order_id','=','orders.id')
                                    ->leftjoin("reviews","reviews.child_order_id",'=','order_details.child_order_id')
                                    ->join("customers",'customers.id','=','orders.customer_id')
                                    ->join('product_variants','product_variants.id','=','order_details.product_variant_id')
                                    ->join('products as p','p.id','=','order_details.product_id')
                                    ->join('brands as b','b.id','=','p.brand_id')
                                    ->join('categories as c','c.id','=','p.primary_category')
                                    ->join('product_variant_options as pvo','pvo.product_variant_id','=','product_variants.id')
                                    ->leftjoin('variation_images as vm', function($join){     
                                        $join->on('vm.attribute_option_id', '=', 'pvo.attribute_option_id');
                                        $join->on('vm.product_id','=','order_details.product_id');
                                    })
                                    ->whereRaw("pvo.attribute_id = c.primary_attribute AND pvo.deleted_at IS NULL AND product_variants.deleted_at IS NULL AND p.deleted_at IS NULL AND p.status = 1 AND c.status = 1 AND c.deleted_at IS NULL AND orders.customer_id = $customer_id AND (order_details.order_status_id = 8 OR order_details.order_status_id = 7 OR order_details.order_status_id = 5 OR order_details.order_status_id = 6 OR order_details.order_status_id = 12)")
                                    ->get();
            $reviewsTemp = [];
            if($reviews) {
                foreach ($reviews as $review) {
                    $images = json_decode($review->images);
                    if($images){
                        if($review->thumbnail != null){
                            $review->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($review->thumbnail-1)]);
                        }else{
                            $review->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                        }
                        array_push($reviewsTemp, $review);
                    }else{
                        $review->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                        array_push($reviewsTemp, $review);
                    }
                }
            }
            return $this->respond(['success'=>true,'reviews'=>$reviews]);       
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }


    }
   
    /**
     * 
     * 
     * add review
     */
    public function addreview(Request $request){
        try{
            if(auth('api')->user()){
                $validator = Validator::make($request->all(), [
                    'review_comment' => 'required|string',
                    'product_id'=>'required',
                    'product_variant_id'=>'required',
                    'rating'=>'required',
                    'child_order_id'=>'required'
                ]);
                if($validator->fails()){
                    return $this->respond($validator->errors()->toJson(), 400);
                }
                $review = new Review();
                $review->customer_id = auth('api')->user()->ref_id;
                $review->product_id = $request->product_id;
                $review->product_variant_id = $request->product_variant_id;
                $review->rating = $request->rating;
                $review->review_comment = $request->review_comment; 
                $review->review_title = $request->review_title; 
                $review->child_order_id = $request->child_order_id; 
                $review->status = 1;
                $review->published = 0;
                $review->save();
                return $this->respond(['success'=>true,'review'=>$review]);
            }else{
                return response(['success' => false],401);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function editReview(Request $request){
        try{
            if(auth('api')->user()){
                $validator = Validator::make($request->all(), [
                    'review_comment' => 'required|string',
                    'review_title'=>'required',
                    'rating'=>'required',
                    'review_id'=>'required'
                ]);
                if($validator->fails()){
                    return $this->respond($validator->errors()->toJson(), 400);
                }
                $review = Review::find($request->review_id);
                $review->rating = $request->rating;
                $review->review_comment = $request->review_comment; 
                $review->review_title = $request->review_title; 
                $review->status = 1;
                $review->published = 0;
                $review->save();
                return $this->respond(['success'=>true,'review'=>$review]);
            }else{
                return response(['success' => false],401);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }
   
}
<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Resources\WishlistCollection;
use App\Models\Wishlist;
use App\Models\CustomerAddress;
use App\Models\Cart;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Deals;
use App\Models\Payment;
use Mail;
use App\Http\Controllers\Api\CartController;
use Razorpay\Api\Api;
use DB;
use App\Http\Controllers\Api\Customer\OrderController;
use App\Mail\SendPaymentSuccessEmail;
use Seshac\Shiprocket\Shiprocket;
use File;


class PaymentController extends ApiController
{
  

    protected $cartController;
    protected $orderController;
    public function __construct(CartController $cartController,OrderController $orderController) {
        $this->cartController = $cartController;
        $this->orderController = $orderController;
        $this->middleware('auth:api');

    }

    public function generateOrderId(Request $request){
        // $cart = Cart::
        try{
            $user_id = auth('api')->user()->ref_id;
            $cartProducts = Cart::selectRaw("carts.*,deals.deal_price,deals.max_orders,product_variants.stock")
                                ->join("product_variants","product_variants.id","carts.product_variant_id")
                                ->leftjoin('deals', function($join){
                                    $join->on('deals.product_variant_id', '=', 'carts.product_variant_id')
                                        ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time');
                                })
                                ->whereRaw("carts.customer_id = $user_id AND deals.deleted_at IS NULL")
                                ->get();
            $success = false;
            DB::beginTransaction();
            foreach ($cartProducts as $cartItem) {
                if($cartItem->is_deal == 'y'){
                    if($cartItem->max_orders > 0 && $cartItem->stock > 0){
                        Deals::whereRaw("product_variant_id = $cartItem->product_variant_id AND NOW( ) BETWEEN deals.start_time AND deals.end_time")
                               ->update(['max_orders'=>DB::raw("max_orders - ".$cartItem->quantity."" )]);
                        ProductVariant::where('id','=',$cartItem->product_variant_id)
                                        ->update(['stock'=>DB::raw("stock - ".$cartItem->quantity."" )]);
                        $success = true;
                    }else{
                        $success = false;
                        break;
                    }
                }else{
                    if($cartItem->stock > 0){
                        ProductVariant::where('id','=',$cartItem->product_variant_id)
                                        ->update(['stock'=>DB::raw("stock - ".$cartItem->quantity."" )]);
                        $success = true;
                    }else{
                        $success = false;
                        break;
                    }
                }
            }
            if(!$success){
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'msg'=>'No Stock!',
                ]);
            }else{
                DB::commit();
            }
            $username=RAZORPAY_KEY_ID;
            $password=RAZORPAY_KEY_SECRET;
            $URL='https://api.razorpay.com/v1/orders';
            $post = [
                'amount' => $request->amount *100,
                'currency' => $request->currency,
                'receipt'   => 1,
                'payment_capture'=> 1,
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$URL);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            $result=curl_exec($ch);
            $resultToStore = json_decode($result);
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
            curl_close ($ch);
            $new_payment = new Payment();
            $new_payment->customer_id = auth('api')->user()->ref_id;
            $new_payment->email = auth('api')->user()->email;
            $new_payment->mobile = auth('api')->user()->mobile;
            $new_payment->name = auth('api')->user()->name;
            $new_payment->amount = $request->amount;
            $new_payment->receipt_id = $resultToStore->id;
            $new_payment->currency = $request->currency;
            $new_payment->payment_status_id = 2;
            $new_payment->save();
    
            return response()->json([
                'success' => true,
                'orderRes'=>$result,
            ]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function reStock(){
        try{
            $user_id = auth('api')->user()->ref_id;
            $cartProducts = Cart::selectRaw("carts.*,deals.deal_price,deals.max_orders,product_variants.stock")
                                ->join("product_variants","product_variants.id","carts.product_variant_id")
                                ->leftjoin('deals', function($join){
                                    $join->on('deals.product_variant_id', '=', 'carts.product_variant_id')
                                        ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time');
                                })
                                ->whereRaw("carts.customer_id = $user_id AND deals.deleted_at IS NULL")
                                ->get();
            foreach ($cartProducts as $cartItem) {
                if($cartItem->is_deal == 'y'){
                        Deals::whereRaw("product_variant_id = $cartItem->product_variant_id AND NOW( ) BETWEEN deals.start_time AND deals.end_time")
                               ->update(['max_orders'=>DB::raw("max_orders + ".$cartItem->quantity."" )]);
                        ProductVariant::where('id','=',$cartItem->product_variant_id)
                                        ->update(['stock'=>DB::raw("stock + ".$cartItem->quantity."" )]);
                  
                }else{
                        ProductVariant::where('id','=',$cartItem->product_variant_id)
                                        ->update(['stock'=>DB::raw("stock + ".$cartItem->quantity."" )]);
                   
                }
            }
            return response()->json([
                'success' => true
            ]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

   public function getOrderSummary(){
        try{
            $finalCart = $this->cartController->finalCart();
            $token =  Shiprocket::getToken();
            $defaultAddress = CustomerAddress::where('customer_id','=',auth('api')->user()->ref_id)
                                               ->where('is_default','=',1)
                                               ->first();
            $pincodeError = false;
            $updatedCart =[];
            foreach ($finalCart['cart'] as $cartItem) {
                $pincodeDetails = [
                    'pickup_postcode' => $cartItem->seller_pincode,
                    'delivery_postcode' => $defaultAddress->pincode,
                    'weight' =>$cartItem->shipping_weight,
                    'cod' => 0,
                    'couriers_type'=>true
                ];
                $response = Shiprocket::courier($token)->checkServiceability($pincodeDetails);
                $shiprocketDelivery = json_decode($response);
                if($shiprocketDelivery->status == 404){
                    $pincodeError = true;
                }
            }
            $data = ['success'=>true,'finalCart'=>$finalCart,'user'=>auth('api')->user(),'pincodeError'=>$pincodeError,'cod_min_amount'=>COD_MIN_AMOUNT];
            return $this->respond($data); 
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function verifyPaymentSignature(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'razorpay_order_id' => 'required',
                'razorpay_payment_id' => 'required',
                'razorpay_signature' => 'required'
            ]);
    
            if($validator->fails()){
                return $this->respond($validator->errors()->toJson(), 422);
            }
            $attributes = array('razorpay_signature' => $request->razorpay_signature,  'razorpay_payment_id'=> $request->razorpay_payment_id ,'razorpay_order_id' => $request->razorpay_order_id);
            $razorPay = new Api(RAZORPAY_KEY_ID, RAZORPAY_KEY_SECRET);
            $razorPayOrder  = $razorPay->utility->verifyPaymentSignature($attributes);
            if($razorPayOrder == null){
                DB::beginTransaction();
                try {
                    $checkIfPaymentIdExists = Payment::where('razorpay_payment_id','=',$request->razorpay_payment_id)
                                                       ->exists();
                    if(!$checkIfPaymentIdExists){                        
                    // The reason for writing the below update is because we need to know either js response or webhook callback ran first
                    Payment::where('receipt_id','=',$request->razorpay_order_id)
                                ->update(['razorpay_payment_id'=>$request->razorpay_payment_id,'razorpay_signature'=>$request->razorpay_signature,'payment_status_id'=>1,'payment_from'=>'js']);
                    $generateOrder = $this->orderController->createOrder('Prepaid',auth('api')->user()->ref_id);
                    Payment::where('receipt_id','=',$request->razorpay_order_id)
                                ->update(['order_id'=>$generateOrder['parent_order']->parent_order_id,'payment_from'=>'js']);
                    // $paymentDetails = ['amount'=>$generateOrder['parent_order']->total,'userName'=>auth('api')->user()->name];
                    // Razorpay capture Payment starts
                    // if(!empty($request->razorpay_payment_id)) {​​​​​​​
                        // $razorPay->payment->fetch($request->razorpay_payment_id)->capture(['amount'=>$generateOrder['parent_order']->total * 100, 'currency' => 'INR']);
                    // }​​​​​​​
                    DB::commit();
                    return $this->respond(['success'=>true,'order_id'=>$generateOrder['parent_order']->parent_order_id]);
                }else{
                    DB::rollback();
                    return $this->respond(['success'=>true,'order_id'=>$request->razorpay_order_id]);
                }
                } catch (Exception $e) {
                    DB::rollback();
                    return $this->respond(['success'=>false]);
                }
                
                
            }else{
                return $this->respond(['success'=>false]);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function handleWebhook(Request $request){
        file_put_contents(UPLOAD_PATH."/webhook.txt","test\n", FILE_APPEND);
    }
  
}
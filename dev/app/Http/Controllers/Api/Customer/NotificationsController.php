<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\User;
use App\Models\Notifications;
use App\Models\NotificationLogs;
use App\Models\FCMToken;
use App\Models\StockNotify;
use App\Http\Controllers\Api\CartController;
use File;
use DB;
use DateTime;

class NotificationsController extends ApiController
{

    public function __construct() {
        $this->middleware('auth:api',['except' => ['updateOrInsertFCMTokenPublic']]);
    }

    public function getNotificationsCount(){
        try{
            $user_id = auth('api')->user()->id;
            $sql = "SELECT
                    COUNT(*) AS notifications_count
                    FROM
                        `notifications` AS `t1`
                    JOIN `notification_logs` AS `t2`
                    ON
                        JSON_SEARCH(t1.users, 'one', t2.user_id)
                    WHERE
                        (t2.visited_on < t1.notify_on OR t2.visited_on IS NULL) AND t1.notify_on < NOW() AND t1.notified_to = 'c'  AND `t2`.`user_id` = $user_id AND `t1`.`deleted_at` IS NULL";
            $notifications_count = DB::select($sql);
            return response(['success'=>true,'notifications_count'=>$notifications_count[0]->notifications_count]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
 
    }

    public function updateVisitedTimeAndDate(){
        try{
            $user_id = auth('api')->user()->id;
            NotificationLogs::where('user_id','=',$user_id)
                              ->update(['visited_on'=> date("Y-m-d H:i:s")]);
            return response(['success'=>true]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }

    public function getNotifications(){
        try{
            $user_id = auth('api')->user()->id;
            $currentDate = date('Y-m-d H:i:s');
            $notifications = Notifications::whereRaw("json_contains(users,'\"$user_id\"') AND '$currentDate' > notify_on AND notified_to = 'c'")
                                            ->get();
            return response(['success'=>true,'notifications'=>$notifications]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function stockNotify(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'product_variant_id' => 'required'
            ]);   
            $stock_notify = new StockNotify();
            $stock_notify->user_id = auth('api')->user()->ref_id;
            $stock_notify->product_variant_id = $request->product_variant_id;
            $stock_notify->save();
            return response()->json(['success'=>true]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function checkNotificationEnabled(){
        $user_id = auth('api')->user()->ref_id;
        $notification_check = User::whereRaw("ref_id=$user_id AND get_notified = 'y'")->exists();
        return response()->json(['notification_check'=>$notification_check]);
    }

    public function enableNotifications(){
        $user_id = auth('api')->user()->ref_id;
        User::whereRaw("ref_id=$user_id")
             ->update(['get_notified'=>'y']);
        return response()->json(['success'=>true]);
    }

    public function checkUserNotifiedForTheProduct(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'product_variant_id' => 'required'
            ]);   
            $user_id = auth('api')->user()->ref_id;
            $p_v_id = $request->product_variant_id;
            $userNotified = StockNotify::whereRaw("user_id = $user_id AND product_variant_id = $p_v_id")->exists();
            return response()->json(['success'=>true,'userNotified'=>$userNotified,'get_notified'=>auth('api')->user()->get_notified]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function updateOrInsertFCMTokenPublic(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'fcm_token' => 'required'
            ]);   
    
            FCMToken::updateOrCreate(
                ['fcm_token' => $request->fcm_token],
                ['fcm_token' => $request->fcm_token]
            );
            return response()->json(['success'=>true]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }

    public function updateOrInsertFCMTokenLoggedInUsers(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'fcm_token' => 'required'
            ]);   
            $token = $request->fcm_token;
            $user_id = auth('api')->user()->id;
            $tokenExists = FCMToken::where("fcm_token",'=',$token)->exists();
            if($tokenExists){
                FCMToken::updateOrInsert(
                    ['fcm_token' => $request->fcm_token],
                    ['fcm_token' => $request->fcm_token,'user_id'=>auth('api')->user()->id]
                );
            }else{
                // FCMToken::whereRaw("user_id = $user_id")->delete();
                $newFcmToken = new FCMToken();
                $newFcmToken->fcm_token = $token;
                $newFcmToken->user_id = $user_id;
                $newFcmToken->save();
            }
            return response()->json(['success'=>true]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }

}

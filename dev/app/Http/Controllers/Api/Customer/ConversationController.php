<?php

namespace App\Http\Controllers\Api\Customer;
use DB;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
// Models
use App\Models\Seller;
use App\Models\User;
use App\Models\Conversation;
use App\Models\SellerWarehouses;
use App\Models\CustomerFeedback;
class ConversationController extends ApiController
{
    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     * 
     * 
     * seller list
     */
    public function conversationList()
    {
        if(auth('api')->user()){
            $user = auth('api')->user();
             $seller_list = Conversation::select('sellers.*','conversations.created_at as condate')
                ->join('sellers','sellers.id','conversations.seller_id')
                ->where('sellers.approval_status',1)->where('sellers.status',1)
                ->whereIn('conversations.user_id', function($query)
                {
                    $query->select(DB::raw('id'))
                            ->from('users')
                            ->where('users.ref_id',auth('api')->user()->ref_id)
                            ->get();
                        
                })
                ->groupBy('conversations.seller_id')
                ->orderBy('conversations.created_at', 'DESC')
                ->get();

                $i=0;
                foreach($seller_list as $seller){
                    $firstletter = substr($seller->name,0,1);
                    $seller_list[$i]->firstletter = $firstletter; 
                    $date = date( 'd,M', strtotime($seller->condate));
                    $seller_list[$i]->date = $date;
                    $images = User::select("users.avatar")->join('sellers','sellers.id','users.ref_id')->where('sellers.id',$seller->id)->get();
                    $sellerdata = Conversation::getSellerAvatar($images);
                    foreach($sellerdata as $d){
                         $seller_list[$i]->seller_images = $d->seller_images;
                     }
                 $i++;
                }
        return $this->respond($seller_list);

        }else{
            return response(['success' => false],401);
        }
    }

    /**
     * 
     * 
     * send message
     */
    public function sendMessage(Request $request)
    {
        if(auth('api')->user()){

            $conversation = new Conversation;
            $conversation->seller_id = $request->seller_id;
            $conversation->user_id = $request->user_id;
            $conversation->from_user = $request->user_id;
            $conversation->to_user = $request->seller_id;
            $conversation->message = $request->message;
            $conversation->save();

           // $conversationlist = Conversation::where('seller_id',$request->seller_id)->where('user_id',$request->user_id)->get();
           $conversationlist = Conversation::select('conversations.*','users.avatar')
           ->join('users' ,'users.ref_id' ,'conversations.seller_id')
           ->where('conversations.seller_id',$request->seller_id)->where('conversations.user_id',$request->user_id)->get();
             $sellerdata = Conversation::getSellerAvatar($conversationlist);
            foreach($conversationlist as $convo){
                $convo->date = date( 'h:i a', strtotime($convo->created_at));
            }
            return $this->respond($conversationlist);
           
        }else{
            return response(['success' => false],401);
        }

    }

    /**
     * 
     * 
     * get message
     */
    public function getMessage(Request $request)
    {  
        if(auth('api')->user()){
            $conversationlist = Conversation::select('conversations.*','users.avatar')
                ->join('users' ,'users.ref_id' ,'conversations.seller_id')
                ->where('conversations.seller_id',$request->seller_id)->where('conversations.user_id',$request->user_id)->orderBy('conversations.created_at', 'ASC')->get();
            $sellerdata = Conversation::getSellerAvatar($conversationlist);

            foreach($conversationlist as $convo){
                $convo->date = date( 'h:i a', strtotime($convo->created_at));
            }

            $seller = Seller::select('sellers.*','users.avatar')
            ->join('users','users.ref_id','sellers.id')
            ->where('sellers.id',$request->seller_id)->where('sellers.approval_status',1)->where('sellers.status',1)->get();
          
            $sellerdata = Conversation::getSellerAvatar($seller);
             return ['seller'=> $seller,'conversationlist'=>$conversationlist];

        }else{
            return response(['success' => false],401);
        }

    }

     /**
     * 
     * 
     * feedback message
     */
    public function feedback(Request $request)
    { 
        if(auth('api')->user()){

            $user = auth('api')->user();
            $feedback = new Customerfeedback;
            $feedback->customer_id = $user->ref_id;
            $feedback->feedback = $request->feedback;
            $feedback->save();
            return $this->respond(['success' => true]);
            
        }else{
            return response(['success' => false],401);
        }
    }

}
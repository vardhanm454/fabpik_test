<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Validator;

// Models
use App\Models\User;
use App\Models\State;
use App\Models\CustomerAddress;

class AddressController extends ApiController
{
    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     *get state.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getState()
    {
        try{
            $states = State::all();
            return $this->respond(['success'=>true,'states'=>$states]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     *Get Customer Address list.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        try{
            $addresses = CustomerAddress::where('customer_id', auth('api')->user()->ref_id)
                                         ->where('deleted_at', NULL)
                                         ->orderBy('id', 'DESC')
                                         ->get(); 
            if(count($addresses) == 0){
                $addresses = [];
            }
            return $this->respond(['success'=>true,'addresses'=>$addresses]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     *get Customer Address.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        try{
            $address = CustomerAddress::find($id);
            return $this->respond(['success'=>true,'data'=>$address]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function sendGoogleAddressSuggestion(){
        return $this->respond(['use_my_location_button'=>USE_MY_LOCATION_BUTTON,'google_auto_suggestions'=>GOOGLE_AUTOSUGGESTIONS]); 
    }

    /**
     * Add multiple Customer Address.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'mobile' => 'required|integer|digits:10',
                'addr_type' => 'required|string',
                'address' => 'required|string', 
                'locality'=> 'required|string',    
                'city' => 'required|string',
                'state_id' => 'required|integer',
                'pincode' => 'required|integer|digits:6'
            ]);
    
            if($validator->fails()){
               // return $this->respond($validator->errors()->toJson(), 400);
               return $this->respond(['success' => false,'msg'=>"Something went wrong!",'error'=>$validator->messages()->toArray()], 210); 
            }
    
            $address = new CustomerAddress();
            $address->customer_id = auth('api')->user()->ref_id;
            $address->name = $request->name;
            $address->mobile = $request->mobile;
            $address->is_default = $request->is_default;
            $address->addr_type = $request->addr_type;
            $address->addr_other = $request->addr_other;
            $address->address = $request->address;
            $address->locality = $request->locality;
            $address->landmark = $request->landmark;
            $address->city = $request->city;
            $address->state_id = $request->state_id;
            $address->pincode = $request->pincode;
            $address->is_default = $request->is_default;
            $address->save();
    
            if($address->is_default == 1){
                $address = CustomerAddress::where('id' ,'!=', $address->id)->where('customer_id' ,auth('api')->user()->ref_id)->update(array('is_default' => 0));
            }
            return $this->respond(['success'=>true]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     *Update Customer Address.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id,Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'mobile' => 'required|integer|digits:10',
                'addr_type' => 'required|string',
                'address' => 'required|string', 
                'locality'=> 'required|string',    
                'city' => 'required|string',
                'state_id' => 'required|integer',
                'pincode' => 'required|integer|digits:6',
            ]);
    
            if($validator->fails()){
                // return $this->respond($validator->errors()->toJson(), 400);
                return response(['success' => false,'msg'=>"Something went wrong!",'error'=>$validator->messages()->toArray()], 210); 
            }
    // dd($request->all());
            $address = CustomerAddress::find($id);
            $address->name = $request->name;
            $address->mobile = $request->mobile;
            $address->is_default = $request->is_default;
            $address->addr_type = $request->addr_type;
            $address->addr_other = $request->addr_other; 
            $address->address = $request->address;
            $address->landmark = $request->landmark;
            $address->locality = $request->locality;
            $address->city = $request->city;
            $address->state_id = $request->state_id;
            $address->pincode = $request->pincode;
            $address->save();
    
            if($address){
                $address = CustomerAddress::where('id' ,'!=', $id)->where('customer_id' ,auth('api')->user()->ref_id)->update(array('is_default' => '0'));
            }
            
            return $this->respond(['success'=>true]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

     /**
     *Update Customer default Address.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatedefaultAddress($id,Request $request)
    {
        try{
            // dd($request->is_default);
            $validator = Validator::make($request->all(), [
                'is_default' => 'required|integer',
            ]);
    
            if($validator->fails()){
              //  return $this->respond($validator->errors()->toJson(), 400);
                return response(['success' => false,'msg'=>"Something went wrong!",'error'=>$validator->messages()->toArray()], 210);  
            }
    
            $address = CustomerAddress::find($id);
            $address->is_default = $request->is_default;
            $address->save();
    
            if($address){
                $address = CustomerAddress::where('id' ,'!=', $id)->where('customer_id' ,auth('api')->user()->ref_id)->update(array('is_default' => '0'));
            }
            
            return $this->respond(['success'=>true,'address'=>$address]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * soft delete Customer Address.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove($id)
    {
        try{
            $delete = CustomerAddress::where(['customer_id'=>auth('api')->user()->ref_id,'id'=>$id])->delete();
            if($delete) {
                return $this->respond(['success'=>true]);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }
}

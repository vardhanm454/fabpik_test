<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Resources\WishlistCollection;
use App\Models\Wishlist;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Seller;
use DB;

class WishlistController extends ApiController
{
    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     * Get Wishlist.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        try{
            $final_wishlist = [];
            $user_id = auth('api')->user()->ref_id;
            $wishlists = Wishlist::select("wishlists.*","product_variants.id AS product_variant_id","product_variants.name","product_variants.mrp","product_variants.price","product_variants.fabpik_seller_price","product_variants.fabpik_seller_discount","product_variants.slug","vm.images","vm.thumbnail","vm.thumbnail_img_name","s.company_name","product_variants.unique_id","product_variants.stock","deals.start_time","deals.end_time","deals.deal_price",DB::raw("IF(
                deals.deal_price,
                TRUNCATE((
                    product_variants.mrp - deals.deal_price 
                ) / product_variants.mrp * 100,1),
                0
            ) AS deal_discount"),DB::raw("TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) AS discount"),DB::raw("CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_price ELSE product_variants.price END AS final_price"),DB::raw("CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1)  ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) END AS final_discount"))
                                ->join('product_variants','product_variants.id','=','wishlists.product_variant_id')
                                ->join('products as p','p.id','=','wishlists.product_id')
                                ->join('sellers as s','s.id','=','p.seller_id')
                                ->join('categories as c','c.id','=','p.primary_category')
                                ->join('product_variant_options as pvo','pvo.product_variant_id','=','product_variants.id')
                                ->leftjoin('variation_images as vm', function($join){
                                    $join->on('vm.attribute_option_id', '=', 'pvo.attribute_option_id');
                                    $join->on('vm.product_id','=','wishlists.product_id');
                                })
                                ->leftjoin('deals', function($join){
                                    $join->on('deals.product_variant_id', '=', 'product_variants.id')
                                         ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time AND deals.max_orders > 0');
                                })
                                ->whereRaw("customer_id = $user_id AND pvo.attribute_id = c.primary_attribute AND pvo.deleted_at IS NULL AND product_variants.deleted_at IS NULL AND p.deleted_at IS NULL AND p.status = 1 AND c.status = 1 AND c.deleted_at IS NULL AND s.status = 1 AND s.deleted_at IS NULL AND deals.deleted_at IS NULL")
                                ->get(); 
              return $this->respond(['success'=>true,'products'=>$wishlists,'imgUrl'=>url(UPLOAD_PATH)]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function variantIds()
    {
        try{
            $ids = Wishlist::where('customer_id', auth('api')->user()->ref_id)->pluck('product_variant_id');
            return $this->respond(['success'=>true,'wishlistIds'=>$ids]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function checkProductExistInWishlist(Request $request){
        try{
            $wishlist = Wishlist::where('customer_id', auth('api')->user()->ref_id)
                                  ->where('product_variant_id','=',$request->id)
                                  ->exists();
            return $this->respond(['success'=>true,'wishlistExists'=>$wishlist]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }

     /**
     * Get Wishlist count.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function count()
    {
        try{
            $user_id = auth('api')->user()->ref_id;
            $wishlistcount = Wishlist::join("products","products.id",'=','wishlists.product_id')
                                        ->join("product_variants","product_variants.id",'=',"wishlists.product_variant_id")
                                        ->join("sellers","sellers.id",'=',"products.seller_id") 
                                        ->whereRaw("customer_id = $user_id AND products.deleted_at IS NULL AND products.status = 1 AND product_variants.deleted_at IS NULL AND sellers.deleted_at IS NULL AND sellers.status = 1 AND sellers.approval_status = 1")
                                        ->count();
            return $this->respond(['success'=>true,'count'=>$wishlistcount]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Add Wishlist.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'product_variant_id' => 'required|integer',
                'product_id' => 'required|integer',
            ]);
            $user_id = auth('api')->user()->ref_id;
            $wishlistCount = 0;
            if($validator->fails()) return $this->respond($validator->errors()->toJson(), 400);
    
            $checkProductExistenceInWishList = Wishlist::where('product_variant_id',$request->product_variant_id)
                                                        ->where('customer_id',auth('api')->user()->ref_id)
                                                        ->exists();
            if($checkProductExistenceInWishList) {
                Wishlist::where('customer_id','=',auth('api')->user()->ref_id)
                          ->where('product_variant_id','=',$request->product_variant_id)
                          ->delete();
                $wishlistCount = Wishlist::join("products","products.id",'=','wishlists.product_id')
                ->join("product_variants","product_variants.id",'=',"wishlists.product_variant_id")
                ->join("sellers","sellers.id",'=',"products.seller_id") 
                ->whereRaw("customer_id = $user_id AND products.deleted_at IS NULL AND products.status = 1 AND product_variants.deleted_at IS NULL AND sellers.deleted_at IS NULL AND sellers.status = 1 AND sellers.approval_status = 1")
                ->count();
                return response(['success' => true,'msg'=>'Product removed from wishlist!','wishlistCount'=>$wishlistCount]);
            }else{
                $wishlist = new Wishlist();
                $wishlist->customer_id = auth('api')->user()->ref_id;
                $wishlist->product_variant_id = $request->product_variant_id;
                $wishlist->product_id = $request->product_id;
                $wishlist->save();
    
                $wishlistCount = Wishlist::join("products","products.id",'=','wishlists.product_id')
                ->join("product_variants","product_variants.id",'=',"wishlists.product_variant_id")
                ->join("sellers","sellers.id",'=',"products.seller_id") 
                ->whereRaw("customer_id = $user_id AND products.deleted_at IS NULL AND products.status = 1 AND product_variants.deleted_at IS NULL AND sellers.deleted_at IS NULL AND sellers.status = 1 AND sellers.approval_status = 1")
                ->count();
    
                return response(['success' => true,'msg'=>'Product added to your wishlist!', 'wishlist'=>$wishlist,'wishlistCount'=>$wishlistCount]);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * remove Wishlist.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove($id)
    {
        try{
            $delete = Wishlist::where(['customer_id'=>auth('api')->user()->ref_id,'product_variant_id'=>$id])->delete();
            $user_id = auth('api')->user()->ref_id;
            $wishlistCount = Wishlist::join("products","products.id",'=','wishlists.product_id')
            ->join("product_variants","product_variants.id",'=',"wishlists.product_variant_id")
            ->join("sellers","sellers.id",'=',"products.seller_id") 
            ->whereRaw("customer_id = $user_id AND products.deleted_at IS NULL AND products.status = 1 AND product_variants.deleted_at IS NULL AND sellers.deleted_at IS NULL AND sellers.status = 1 AND sellers.approval_status = 1")
            ->count();
            if($delete){
                 return $this->respond(['success'=>true,'wishlistCount'=>$wishlistCount]);
            }else{
                return $this->respond(['success'=>false,'wishlistCount'=>$wishlistCount]);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }
}

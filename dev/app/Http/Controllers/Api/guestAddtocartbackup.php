
    public function finalGuestCart(){
        session_start();
        $cod = 0; 
        $cod_charge = 0;
        $cart = isset($_SESSION['cart']) ? $_SESSION['cart']:null;
        if(!$cart){
            return ["success"=>false,'cart'=>[]];
        }
        $checkDealProductAlreadyExists = false;
        foreach ($cart as $key => $cartItem) {
             if($cartItem["is_deal"] == 'y'){
                $checkDealProductAlreadyExists = true;
                break;
            }
        }
        $products = [];
        // dd($cart);
        $keys = array_column($cart, 'created_at');      
        array_multisort($keys, SORT_ASC, $cart);
        foreach ($cart as $cartItem) {
            $productVariantid = $cartItem["product_variant_id"];
            $productDetails = ProductVariant::selectRaw("product_variants.*,deals.deal_price,deals.start_time,deals.end_time,deals.max_orders")
            ->join('products as p','p.id','=','product_variants.product_id')
            ->leftjoin('deals', function($join){
                $join->on('deals.product_variant_id', '=', 'product_variants.id')
                    ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time');
              })
              ->whereRaw("product_variants.id = $productVariantid AND deals.deleted_at IS NULL")
              ->first();
            $cartItem["deal_price"] = $productDetails->deal_price;
            $cartItem["max_orders"] = $productDetails->max_orders;
            $cartItem["fabpik_seller_price"] = $productDetails->fabpik_seller_price;
            $cartItem["total_seller_discount"] = $productDetails->total_seller_discount;
            array_push($products,$cartItem);
        }
        
        if(!$checkDealProductAlreadyExists){
            $tempCart = [];
            foreach ($products as $product) {
                if($product["deal_price"] != null && $product["max_orders"] > 0){
                    $product['quantity'] = 1;
                    $product['price'] = $product["deal_price"];
                    $product['total_price'] = $product["deal_price"];
                    $product['seller_discount'] =  $product["mrp"] - $product["deal_price"];
                    $product['total_seller_discount'] = $product["mrp"] - $product["deal_price"];
                    $product['is_deal'] = 'y';
                    $updatedProduct = $product;
                    break;
                }
            }
            if(isset($updatedProduct)){
                foreach ($cart as $cartItem) {
                    if($updatedProduct["product_variant_id"] == $cartItem["product_variant_id"]){
                        array_push($tempCart,$updatedProduct);
                    }else{
                        array_push($tempCart,$cartItem);
                    }
                }
                $_SESSION['cart'] = $tempCart;
            }
        }else{
            $tempCart = [];
            foreach ($products as $product) {
                if($product["is_deal"] == 'y' && $product["max_orders"] <= 0){
                    $product['quantity'] = 1;
                    $product['price'] = $product->fabpik_seller_price;
                    $product['total_price'] = $product->fabpik_seller_price;
                    $product['seller_discount'] =  $product->fabpik_seller_discount;
                    $product['total_seller_discount'] = $product->fabpik_seller_discount;
                    $product['is_deal'] = 'n';
                    $updatedProduct = $product;
                    break;
                }
            }
            if(isset($updatedProduct)){
                foreach ($cart as $cartItem) {
                    if($updatedProduct["product_variant_id"] == $cartItem["product_variant_id"]){
                        array_push($tempCart,$updatedProduct);
                    }else{
                        array_push($tempCart,$cartItem);
                    }
                }
                $_SESSION['cart'] = $tempCart;
            }
        }

        $cart = $_SESSION['cart'];
        $totalMrp = 0;
        $totalSellingPrice = 0;
        $totalOrderAmount = 0;
        $coupon = null;

        foreach ($cart as $cartItem) {
            $totalMrp += ($cartItem["mrp"] * $cartItem["quantity"]);
            $totalSellingPrice += $cartItem["total_price"];
            $totalOrderAmount = $totalSellingPrice;
        }
        if($totalSellingPrice >= COD_MIN_AMOUNT){
            $cod = 1;
            $cod_charge = count($_SESSION['cart']) * COD_CHARGE;
        }
        $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $totalOrderAmount AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
        $totalOrderAmount +=$handling_charges[0]->charge_amount;
        $handlingCharge = $handling_charges[0]->charge_amount;
        return ['success'=>true,'cart'=>$_SESSION['cart'],'totalMrp'=>$totalMrp,'totalSellingPrice'=>$totalSellingPrice,'totalOrderAmount'=>$totalOrderAmount,'coupon'=>$coupon,'handling_charges'=>$handlingCharge,'cod'=>$cod,'cod_charge'=>$cod_charge,'imagePrefixUrl'=>url(UPLOAD_PATH.'/')];
    }

    function guestAddToCart(Request $request){
        $validator = Validator::make($request->all(), [
            'product_variant_id' => 'required|integer',
            'quantity' => 'required|integer'
        ]);
        if($validator->fails()){
            return $this->respond($validator->errors()->toJson(), 400);
        } 
        $cartCount = 0;
        $productVariantid = $request->product_variant_id;
        $productDetails = ProductVariant::selectRaw("product_variants.*,deals.deal_price,deals.start_time,deals.end_time,deals.max_orders,p.seller_id,variation_images.thumbnail, variation_images.images,categories.primary_attribute,categories.secondary_attribute")
                                        ->join('products as p','p.id','=','product_variants.product_id')
                                        ->leftjoin('deals', function($join){
                                            $join->on('deals.product_variant_id', '=', 'product_variants.id')
                                                ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time');
                                            })
                                        ->join('categories','categories.id','=','p.primary_category')
                                        ->leftjoin('view_product_images as variation_images','variation_images.product_variant_id','=','product_variants.id')
                                        ->whereRaw("product_variants.id = $productVariantid AND deals.deleted_at IS NULL")
                                        ->first();
        if($productDetails){
            $product_variant_options = ProductVariantOption::where('product_variant_id','=',$productVariantid)->get();
            $attributes = [];
            foreach ($product_variant_options as $options) {
                if($options->attribute_id == $productDetails->primary_attribute){
                    $attribute = Attribute::find($options->attribute_id);
                    $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                        ->where('id','=',$options->attribute_option_id)
                                                        ->first();
                    if($attribute && $attributeOption){
                        $primaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'p');
                        array_push($attributes,$primaryAttribute);
                    }
                }else if($options->attribute_id == $productDetails->secondary_attribute ){
                    $attribute = Attribute::find($options->attribute_id);
                    $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                        ->where('id','=',$options->attribute_option_id)
                                                        ->first();
                    if($attribute && $attributeOption){
                        $secondaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'s');
                        array_push($attributes,$secondaryAttribute);
                    }
                }
            }
            $images = json_decode($productDetails->images);
            if($images == null){
                $productDetails->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
            }else{
                $productDetails->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($productDetails->thumbnail-1)]);
            }
           if($request->quantity > $productDetails->stock){
                return response(['success' => false,'msg'=>"Stock not Available!"]);
            } 
            
            session_start();
            // unset($_SESSION['cart']);
            // die;
            // dd($_SESSION['cart']);
            $cart = isset($_SESSION['cart'])?$_SESSION['cart']:null;
            // dd($cart);
            if(!$cart) {
                $checkDealProductAlreadyExists = false;
                $seller = Seller::where('id','=', $productDetails->seller_id)->first();
                $cartItem = [
                    ["product_id"=>$productDetails->product_id,
                     "product_variant_id"=>$productDetails->id,
                     "seller_id"=>$productDetails->seller_id,
                     "quantity"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? 1 : $request->quantity,
                     "mrp"=>$productDetails->mrp,
                     "total_seller_discount"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? ($productDetails->mrp - $productDetails->deal_price)  : $productDetails->fabpik_seller_discount * $request->quantity,
                     "price"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? $productDetails->deal_price : $productDetails->fabpik_seller_price,
                     "seller_discount"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? ($productDetails->mrp - $productDetails->deal_price)  : $productDetails->fabpik_seller_discount,
                     "total_price"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? $productDetails->deal_price  : $productDetails->fabpik_seller_price * $request->quantity,
                     "seller_pincode"=>$seller->company_pincode,
                     "is_deal"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? 'y' : 'n',
                     "seller_pincode"=>$seller->company_pincode,
                     "coupon_id"=>null,
                     "coupon_discount"=>0,
                     "coupon_applied"=>null,
                     "coupon_type"=>null,
                     "customer_pincode"=>null,
                     "name"=>$productDetails->name,
                     "store_name"=>$seller->store_name,
                     "unique_id"=>$productDetails->unique_id,
                     "slug"=>$productDetails->slug,
                     "cod_enable"=>$seller->cod_enable,
                     "seller_original_price"=>$productDetails->price,
                     "thumbnail_path"=>$productDetails->thumbnail_path,
                     "attributes"=>$attributes,
                     "created_at"=>date('Y-m-d H:i:s')
                    ]
                ];
                $_SESSION['cart'] = $cartItem;
                return response(['success' => true,'cartCount'=>count($_SESSION['cart'])]);
            }else{
                // unset($_SESSION['cart']);
                // die;
                $cart = $_SESSION['cart'];
                // $checkProductExistenceInCart = array_search($request->product_variant_id, array_column($cart, 'product_variant_id'));
                $checkProductExistenceInCart = false;
                $checkDealProductAlreadyExists = false;
                foreach ($cart as $key => $cartItem) {
                    if($cartItem["product_variant_id"] == $request->product_variant_id){
                        $checkProductExistenceInCart = true;
                        if($cartItem["is_deal"]== 'y'){
                            $checkProductIsDealProduct = true;
                        }else{
                            $checkProductIsDealProduct = false;
                        }
                    }else if($cartItem["is_deal"] == 'y'){
                        $checkDealProductAlreadyExists = true;
                    }
                    if($checkDealProductAlreadyExists && $checkProductExistenceInCart){
                        break;
                    }
                }
                $tempCart = [];
                if($checkProductExistenceInCart){
                    foreach ($cart as $key => $cartItem) {
                        if($cartItem["product_variant_id"] == $request->product_variant_id){
                            $quantity = $request->quantity + $cartItem["quantity"];
                            if($quantity <= 0 ){
                               continue;
                            }
    
                            $cartItem["quantity"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? 1 : $quantity;
    
                            $cartItem["price"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? $productDetails->deal_price : $productDetails->fabpik_seller_price;
    
                            $cartItem["total_price"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? $productDetails->deal_price  : ($productDetails->fabpik_seller_price * $quantity);
    
                            $cartItem["seller_discount"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? ($productDetails->mrp - $productDetails->deal_price)  : $productDetails->fabpik_seller_discount;
    
                            $cartItem["total_seller_discount"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? ($productDetails->mrp - $productDetails->deal_price)  : ($productDetails->fabpik_seller_discount * $quantity);
    
                            $cartItem["is_deal"] = $productDetails->deal_price != null && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? 'y' : 'n';
                        }
                        array_push($tempCart,$cartItem);
                    }
                    $_SESSION['cart'] = $tempCart;
                    $finalCart = $this->finalGuestCart;
                    return response(['success' => true,'cart'=>$finalCart,'cartCount'=>count($_SESSION['cart'])]);
                }else{
                    $seller = Seller::where('id','=', $productDetails->seller_id)->first();
                    $cartItem = 
                        ["product_id"=>$productDetails->product_id,
                        "product_variant_id"=>$productDetails->id,
                        "seller_id"=>$productDetails->seller_id,
                        "quantity"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? 1 : $request->quantity,
                        "mrp"=>$productDetails->mrp,
                        "total_seller_discount"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? ($productDetails->mrp - $productDetails->deal_price)  : $productDetails->fabpik_seller_discount * $request->quantity,
                        "price"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? $productDetails->deal_price : $productDetails->fabpik_seller_price,
                        "seller_discount"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? ($productDetails->mrp - $productDetails->deal_price)  : $productDetails->fabpik_seller_discount,
                        "total_price"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? $productDetails->deal_price  : $productDetails->fabpik_seller_price * $request->quantity,
                        "seller_pincode"=>$seller->company_pincode,
                        "is_deal"=>$productDetails->deal_price != null && !$checkDealProductAlreadyExists ? 'y' : 'n',
                        "seller_pincode"=>$seller->company_pincode,
                        "coupon_id"=>null,
                        "coupon_discount"=>0,
                        "coupon_applied"=>null,
                        "coupon_type"=>null,
                        "customer_pincode"=>null,
                        "name"=>$productDetails->name,
                        "store_name"=>$seller->store_name,
                        "unique_id"=>$productDetails->unique_id,
                        "slug"=>$productDetails->slug,
                        "cod_enable"=>$seller->cod_enable,
                        "seller_original_price"=>$productDetails->price,
                        "thumbnail_path"=>$productDetails->thumbnail_path,
                        "attributes"=>$attributes,
                        "created_at"=>date('Y-m-d H:i:s')
                ];

                    $cart = $_SESSION['cart'];
                    array_push($cart,$cartItem);
                    $_SESSION['cart'] = $cart;
                }
                
                return response(['success' => true,'cartCount'=>count($_SESSION['cart'])]);
            }
        }else{
            return response(['success' => false,'msg'=>"Something went wrong!"]);
        }
    }


    public function removeItemGuestCart(Request $request){
        $validator = Validator::make($request->all(), [
            'product_variant_id' => 'required|integer'
        ]);

        if($validator->fails()){
            return response(['success' => false,'errors'=>$validator->messages()->toArray()], 422);  
        }
        session_start();
        $cart = $_SESSION['cart'];
        $tempCart = [];
        foreach ($cart as $key => $cartItem) {
            if($cartItem["product_variant_id"] == $request->product_variant_id){
                continue;
            }
            array_push($tempCart,$cartItem);
        }
        $_SESSION['cart'] = $tempCart;
        $finalCart = $this->finalGuestCart();
        return response(['success' => true,'cartCount'=>count($_SESSION['cart']),'finalCart'=>$finalCart]);
    }


    public function guestCartCount(){
        session_start();
        return $this->respond(count($_SESSION['cart']));
    }
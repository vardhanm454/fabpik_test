<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Resources\WishlistCollection;
use App\Models\Wishlist;
use App\Models\CustomerAddress;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Payment;
use App\Models\Cart;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Shipingcharge;
use App\Models\OrderHistory;
use App\Models\OrderHandlingCharge;
use App\Models\SellerWarehouses;
use App\Models\Deals;
use App\Models\Customer;
use Mail;
use DB;
use App\Http\Controllers\Api\CartController;
use Razorpay\Api\Api;
use App\Http\Controllers\Api\Customer\OrderController;
use App\Mail\SendPaymentSuccessEmail;
use Seshac\Shiprocket\Shiprocket;
use File;


class WebhookController extends ApiController
{

    public function __construct() {}

    public function handleWebhook(Request $request){
        $razorPay = new Api(RAZORPAY_KEY_ID, RAZORPAY_KEY_SECRET);
        $webhookBody = $request->getContent();
        $webhookSecret = WEBHOOK_SECRET;
        $webhookSignature = $request->header('X-Razorpay-Signature');

        $result = $razorPay->utility->verifyWebhookSignature($webhookBody, $webhookSignature, $webhookSecret);
        if($result == null){
            $checkIfPaymentIdExists = Payment::where('razorpay_payment_id','=',$request->payload["payment"]["entity"]["id"])
                                            ->exists();
            if(!$checkIfPaymentIdExists){
                Payment::where('receipt_id','=',$request->payload["payment"]["entity"]["order_id"])
                    ->update(['razorpay_payment_id'=>$request->payload["payment"]["entity"]["id"],             'razorpay_signature'=>$webhookSignature]);
                $generateOrder = $this->createOrder('Prepaid',$request->payload["payment"]["entity"]["notes"]["customer_id"]);
                Payment::where('receipt_id','=',$request->payload["payment"]["entity"]["order_id"])
                     ->update(['order_id'=>$generateOrder['parent_order']->parent_order_id,'payment_status_id'=>1]);
                // $result2 = ["result"=>$request->payload["payment"]["entity"]["order_id"]];
                // $data = json_encode($result2);
                // file_put_contents(UPLOAD_PATH."/webhook.json",$data, FILE_APPEND);
                return response()->json(['status' => 'ok'], 200);
            }else{
                return response()->json(['status' => 'ok'], 200);
            }
        }else{
            return response()->json(['status' => 'ok'], 200);
        }
        $result2 = ["result"=>$request->payload["payment"]["entity"]["order_id"]];
        $data = json_encode($result2);
        file_put_contents(UPLOAD_PATH."/webhook.json",$data, FILE_APPEND);
        
    }
  
    public function createOrder($payment_type,$customer_id){
            
        $finalCart = $this->finalCart($customer_id);
        DB::beginTransaction();
        try{
            $defaultAddress = CustomerAddress::select("states.name AS state","customer_addresses.*")
                                            ->join("states","customer_addresses.state_id",'=','states.id')
                                            ->where('customer_id','=',$customer_id)
                                            ->where('is_default','=',1)
                                            ->first();
            if(!$defaultAddress){
                return ['success'=>false,'error' => 'Please set the address in checkout!'];
            }
            $customer = Customer::find($customer_id);
            $new_order = new Order();
            $new_order->parent_order_id = __generateParentOrderId();
            $new_order->invoice_prefix = "FAB";
            $new_order->customer_id =$customer_id;
            $new_order->email = $customer->email;
            $new_order->mobile = $customer->mobile;
            $new_order->first_name = $customer->name;
            $new_order->last_name = $customer->name; 
            $new_order->billing_first_name = $defaultAddress->name;
            $new_order->billing_last_name = $defaultAddress->name;
            $new_order->billing_email = $customer->email;
            $new_order->billing_mobile = $defaultAddress->mobile;
            $new_order->billing_address1 = $defaultAddress->address;
            $new_order->billing_address2 = $defaultAddress->landmark;
            $new_order->billing_city = $defaultAddress->city;
            $new_order->billing_state = $defaultAddress->state;
            $new_order->billing_state_id = $defaultAddress->state_id;
            $new_order->billing_pincode = $defaultAddress->pincode;
            $new_order->payment_status_id = $payment_type == 'COD' ? 2 : 1;
            $new_order->shipping_first_name = $defaultAddress->name;
            $new_order->shipping_last_name = $defaultAddress->name;
            $new_order->shipping_email = $customer->email;
            $new_order->shipping_mobile = $defaultAddress->mobile;
            $new_order->shipping_address1 = $defaultAddress->address;
            $new_order->shipping_address2 = $defaultAddress->landmark;
            $new_order->shipping_city = $defaultAddress->city;
            $new_order->shipping_state = $defaultAddress->state;
            $new_order->shipping_state_id  = $defaultAddress->state_id;
            $new_order->shipping_pincode  = $defaultAddress->pincode;
            $new_order->order_status_id = 1;
            $new_order->shipping_state_id = 1;
            $new_order->total  = $payment_type == 'COD' ? ($finalCart['totalOrderAmount'] - $finalCart['handling_charges']) + count($finalCart['cart']) * COD_CHARGE : $finalCart['totalOrderAmount'];
            $new_order->coupon_id = $finalCart['cart'][0]->coupon_id;
            $new_order->coupon_code  = $finalCart['coupon'] != null ? $finalCart['coupon']->code : null;
            if($finalCart['coupon']){
                $tltCouponDiscount=0;
                foreach ($finalCart['cart'] as $cartItem) {
                    if($finalCart['coupon']['dis_type'] == 'f' && $finalCart['coupon']['sub_type'] == 's'){
                            $tltCouponDiscount = $cartItem->coupon_discount;
                    }else{
                        $tltCouponDiscount += $cartItem->coupon_discount;
                    }
                }
                $new_order->coupon_discount = $tltCouponDiscount;
            }else{
                $new_order->coupon_discount = 0;
            }
            $new_order->handling_charge = $payment_type == 'COD' ? count($finalCart['cart']) * COD_CHARGE   : $finalCart['handling_charges'];
            $new_order->payment_type = $payment_type == 'COD' ? 'c' : 'o';
            $new_order->save();
            $order_data = [];
            $dealProductsExists = Cart::where('customer_id','=',$customer_id)
                                        ->where('is_deal','=','y')
                                        ->exists();
            foreach ($finalCart['cart'] as $cartItem) {
                $totalShippingWeight = $cartItem->quantity * $cartItem->shipping_weight * 1000;
                
                $sub_order = new OrderDetail();
                $sub_order->order_id  = $new_order->id;
                $childOrderId = __generateChildOrderId();
                $sub_order->child_order_id = $childOrderId;
                $sub_order->tracking_no  = null;
                $sub_order->seller_id = $cartItem->seller_id;
                $sub_order->images = $cartItem->images;
                $sub_order->thumbnail = $cartItem->thumbnail;
                $sub_order->payment_status_id  = $payment_type == 'COD' ? 2 : 1;
                $sub_order->product_id  = $cartItem->product_id;
                $sub_order->product_variant_id   = $cartItem->product_variant_id;
                // storing company pincode as seller warehouse pincode in order details table because there shouldn't be dependency of seller warehouse while order creation
                $sub_order->seller_warehouse_pincode  = $cartItem->seller_pincode;
                $sub_order->name  = $cartItem->name;
                $sub_order->shipping_weight  = $cartItem->shipping_weight;
                $sub_order->shipping_length  = $cartItem->shipping_length;
                $sub_order->shipping_breadth  = $cartItem->shipping_breadth;
                $sub_order->shipping_height  = $cartItem->shipping_height;
                $sub_order->quantity = $cartItem->quantity;
    
                if($cartItem->is_deal == 'y'){
                    $sub_order->price = $cartItem->seller_original_price;
                    $sub_order->deal_price = $cartItem->price;
                }else{
                    $sub_order->price = $cartItem->price;
                }
                $sub_order->mrp = $cartItem->mrp;
                $total_price = null;
                if($cartItem->coupon_id != null){
                   $sub_order->coupon_id  = $cartItem->coupon_id; 
                   $coupon = Coupon::where('id','=',$cartItem->coupon_id)->first();
                   $sub_order->dis_type = $coupon->dis_type;
                   $sub_order->sub_type = $coupon->sub_type;
                   $sub_order->coupon_code = $coupon->code;
                   if($coupon->dis_type == 'f' && $coupon->sub_type == 's'){
                       if($dealProductsExists){
                           if($cartItem->is_deal == 'y'){
                               $sub_order->coupon_discount = 0;     
                               $total_price = $cartItem->total_price;
                           }else{
                                $sub_order->coupon_discount = ($cartItem->coupon_discount/(count($finalCart['cart'])-1));
                                $total_price = $cartItem->total_price - ($cartItem->coupon_discount/(count($finalCart['cart'])-1));
                           }
                       }else{
                        $sub_order->coupon_discount  = ($cartItem->coupon_discount/count($finalCart['cart']));
                        $total_price = $cartItem->total_price - ($cartItem->coupon_discount/count($finalCart['cart']));
                       }
                   }else{
                       $sub_order->coupon_discount  = $cartItem->coupon_discount;
                   }
                }else{
                    $sub_order->coupon_id  = null;
                    $sub_order->coupon_discount  = 0;
                }
                $sub_order->discount  = $cartItem->seller_discount;
                $sellerDiscountPercentage = ($cartItem->seller_discount *100)/$cartItem->mrp;
                $sub_order->total_seller_discount  = $cartItem->total_seller_discount;
                $sub_order->cod_charge = $payment_type == 'COD' ? COD_CHARGE : 0;
    
                if($total_price == null){
                    $sub_order->total = $cartItem->total_price;
                }else{
                    $sub_order->total = $total_price;
                }
                /*
                Calculating Tax:
                Getting tax from the products table as percentage
                Converting the tax percentage into rupees from mrp
                */
    
                $sub_order->tax  = ($cartItem->tax/100)*($cartItem->seller_original_price * $cartItem->quantity);
                $sub_order->sgst  = (($cartItem->tax/100)*($cartItem->seller_original_price * $cartItem->quantity))/2;
                $sub_order->cgst  = (($cartItem->tax/100)*($cartItem->seller_original_price * $cartItem->quantity))/2;
    
                $actualCommission = $cartItem->commission;
                $commission_model = CommissionModel::selectRaw("commission_type,commission_on,vary_with_mrp_discount,shipping_charges,handling_charges")
                                        ->where("id","=",$cartItem->commission_id)
                                        ->first();
                if($sellerDiscountPercentage > 0){
                    $percentage = $cartItem->commission - (($sellerDiscountPercentage/100) * $cartItem->commission);
                    $actualCommission = $percentage >= 8 ? $percentage : 8;
                }
                if($commission_model){
                    //commission on is mrp
                    if($commission_model->commission_on == 'm'){
                        //if seller has given the discount
                        if($commission_model->vary_with_mrp_discount == 'y'){ 
                            $commission =($actualCommission/100) * ($cartItem->mrp * $cartItem->quantity);
                            $commission_sgst = $commission * (COMMISION_SGST/100);
                            $commission_cgst = $commission * (COMMISION_CGST/100);
                        }else{           
                            //if seller has not given the discount
                            $commission =($actualCommission/100) * ($cartItem->mrp * $cartItem->quantity);
                            $commission_sgst = $commission * (COMMISION_SGST/100);
                            $commission_cgst = $commission * (COMMISION_CGST/100);
                        }
                    }else{
                        //commission on is selling price
                        if($commission_model->vary_with_mrp_discount == 'y'){
                            //if seller has given the discount
                            $commission = ($actualCommission/100)* ($cartItem->seller_original_price * $cartItem->quantity);
                            $commission_sgst = (($actualCommission/100)* ($cartItem->seller_original_price * $cartItem->quantity)) * (COMMISION_SGST/100);
                            $commission_cgst = (($actualCommission/100)* ($cartItem->seller_original_price * $cartItem->quantity))* (COMMISION_CGST/100);
                        }else{
                            //if seller has not given the discount
                            $commission = ($actualCommission/100)* ($cartItem->seller_original_price * $cartItem->quantity);
                            $commission_sgst = (($actualCommission/100)* ($cartItem->seller_original_price * $cartItem->quantity)) * (COMMISION_SGST/100);
                            $commission_cgst = (($actualCommission/100)* ($cartItem->seller_original_price * $cartItem->quantity))* (COMMISION_CGST/100);
                        }
                    }
                }
                
                $sub_order->commission = $commission;
                $sub_order->commission_sgst = $commission_sgst;
                $sub_order->commission_cgst = $commission_cgst;
                
                $sub_order->shipping_status_id = 1;
    
                if($cartItem->shipping_model == 's') {
                    $shipping_charge = 0;
                    $shipping_charge_sgst = 0;
                    $shipping_charge_cgst = 0;
                } else {
                    $shippingCharge =  Shipingcharge::select("charge_amount")
                                                        ->whereRaw("min_amount <= ".$totalShippingWeight. " AND status = 1")
                                                        ->orderBy("min_amount",'DESC')
                                                        ->first();
                    $shipping_charge = $shippingCharge->charge_amount;
                    $shipping_charge_sgst = (SHIPPING_SGST/100)*$shippingCharge->charge_amount;
                    $shipping_charge_cgst = (SHIPPING_CGST/100)*$shippingCharge->charge_amount;
                }
    
                $sub_order->shipping_charge = $shipping_charge;
                $sub_order->shipping_charge_sgst = $shipping_charge_sgst;
                $sub_order->shipping_charge_cgst = $shipping_charge_cgst;
    
                // get order handeling charge 
                
                $orderHandlingCharge =  OrderHandlingCharge::select("charge_amount")
                                                              ->whereRaw("min_amount <= ".$totalShippingWeight. " AND status = 1")
                                                              ->orderBy("min_amount",'DESC')
                                                              ->first();
                
                $order_handling_charge = $orderHandlingCharge->charge_amount;
                $order_handling_charge_sgst = (ORDER_HANDLING_SGST/100)*$orderHandlingCharge->charge_amount;
                $order_handling_charge_cgst = (ORDER_HANDLING_CGST/100)*$orderHandlingCharge->charge_amount;
    
                $sub_order->order_handling_charge = $order_handling_charge;
                $sub_order->order_handling_charge_sgst = $order_handling_charge_sgst;
                $sub_order->order_handling_charge_cgst = $order_handling_charge_cgst;
    
                if($commission_model){  
                    if($commission_model->shipping_charges == 'y' && $commission_model->handling_charges == 'y'){
                        if($cartItem->is_deal == 'y'){
                            $sub_order->final_amount_to_pay_seller  = $cartItem->seller_original_price - ($commission+$commission_sgst+$commission_cgst+$shipping_charge+$shipping_charge_sgst+$shipping_charge_cgst + $order_handling_charge + $order_handling_charge_sgst + $order_handling_charge_cgst);
                        }else{
                            $sub_order->final_amount_to_pay_seller  = ($cartItem->seller_original_price * $cartItem->quantity) - ($commission+$commission_sgst+$commission_cgst+$shipping_charge+$shipping_charge_sgst+$shipping_charge_cgst + $order_handling_charge + $order_handling_charge_sgst + $order_handling_charge_cgst);
                        }
                    }
                    else if($commission_model->shipping_charges == 'n' && $commission_model->handling_charges == 'n'){
                        if($cartItem->is_deal == 'y'){
                            $sub_order->final_amount_to_pay_seller  = $cartItem->seller_original_price - ($commission+$commission_sgst+$commission_cgst);
                        }else{
                            $sub_order->final_amount_to_pay_seller  = ($cartItem->seller_original_price * $cartItem->quantity) - ($commission+$commission_sgst+$commission_cgst);
                        }
                    }
                    else if($commission_model->shipping_charges == 'y' && $commission_model->handling_charges == 'n'){
                        if($cartItem->is_deal == 'y'){
                            $sub_order->final_amount_to_pay_seller  = $cartItem->seller_original_price - ($commission+$commission_sgst+$commission_cgst+$shipping_charge+$shipping_charge_sgst+$shipping_charge_cgst);
                        }else{
                            $sub_order->final_amount_to_pay_seller  = ($cartItem->seller_original_price * $cartItem->quantity) - ($commission+$commission_sgst+$commission_cgst+$shipping_charge+$shipping_charge_sgst+$shipping_charge_cgst);
                        }
                    }
                    else if($commission_model->shipping_charges == 'n' && $commission_model->handling_charges == 'y'){
                        if($cartItem->is_deal == 'y'){
                            $sub_order->final_amount_to_pay_seller  = $cartItem->seller_original_price - ($commission+$commission_sgst+$commission_cgst+ $order_handling_charge + $order_handling_charge_sgst + $order_handling_charge_cgst);
                        }else{
                            $sub_order->final_amount_to_pay_seller  = ($cartItem->seller_original_price * $cartItem->quantity) - ($commission+$commission_sgst+$commission_cgst + $order_handling_charge + $order_handling_charge_sgst + $order_handling_charge_cgst);
                        }
                    }
                }
    
                $sub_order->order_status_id = 1;          
                $sub_order->excepted_delivery_date = $cartItem->etd;   
                $sub_order->shipping_model = $cartItem->shipping_model;   
                $sub_order->save();
                
                $order_history = new OrderHistory();    
                $order_history->order_id = $new_order->id;
                $order_history->order_detail_id = $sub_order->id;
                $order_history->order_status_id  = 1;
                $order_history->shipping_status_id = 1;
                $order_history->save();
    
                $seller_details = SellerWarehouses::where('seller_id','=',$cartItem->seller_id)
                                                    ->where('status','=',1)
                                                    ->where('is_default','=',1)
                                                    ->first();
    
                if($cartItem->is_deal == 'y'){
                    Deals::whereRaw("product_variant_id = $cartItem->product_variant_id AND NOW( ) BETWEEN deals.start_time AND deals.end_time")
                           ->update(['max_orders'=>DB::raw("max_orders - ".$cartItem->quantity."" )]);
                }
                
                ProductVariant::where('id','=',$cartItem->product_variant_id)
                                ->update(['stock'=>DB::raw("stock - ".$cartItem->quantity."" )]);
            }
           DB::commit();
           return ['success'=>true,'parent_order'=>$new_order];
        }catch (Exception $ex) {
            DB::rollback();
            return ['success'=>false,'error' => $ex->getMessage()];
        }
    }

    public function finalCart($customer_id){
        $cod = 0; 
        $cod_charge = 0;
        $user_id = $customer_id;
        $checkDealProductAlreadyExists = Cart::where('customer_id','=',$user_id)
                                               ->where('is_deal','=','y')
                                               ->exists();
        $cartProducts = Cart::selectRaw("carts.*,deals.deal_price,deals.max_orders,product_variants.fabpik_seller_price,product_variants.fabpik_seller_discount")
                                ->join("product_variants","product_variants.id","carts.product_variant_id")
                                ->leftjoin('deals', function($join){
                                    $join->on('deals.product_variant_id', '=', 'carts.product_variant_id')
                                         ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time');
                                })
                                ->whereRaw("customer_id = $user_id AND deals.deleted_at IS NULL")
                                ->orderBy('created_at', 'ASC')
                                ->get();
        if(!$checkDealProductAlreadyExists){
            foreach ($cartProducts as $cartItem) {
                if($cartItem->deal_price != null && $cartItem->max_orders > 0){
                    Cart::where('product_variant_id','=',$cartItem->product_variant_id)
                          ->where('customer_id','=',$user_id)
                          ->update(['quantity' => 1,
                                    'price' =>$cartItem->deal_price,
                                    'total_price'=>$cartItem->deal_price,
                                    'seller_discount'=> $cartItem->mrp - $cartItem->deal_price,
                                    'total_seller_discount'=>$cartItem->mrp - $cartItem->deal_price,
                                    'is_deal'=>'y']);
                    break;
                }
            }
        }else{
            foreach ($cartProducts as $cartItem) {
                if($cartItem->is_deal == 'y' && $cartItem->max_orders <= 0){
                        Cart::where('product_variant_id','=',$cartItem->product_variant_id)
                          ->where('customer_id','=',$user_id)
                          ->update(['quantity' => 1,
                                    'price' =>$cartItem->fabpik_seller_price,
                                    'total_price'=>$cartItem->fabpik_seller_price,
                                    'seller_discount'=> $cartItem->fabpik_seller_discount,
                                    'total_seller_discount'=>$cartItem->fabpik_seller_discount,
                                    'is_deal'=>'n']);
                    break;
                }
                if(count($cartProducts) == 1 && $cartItem->is_deal == 'y' && $cartItem->coupon_id != null ){
                    Cart::where('product_variant_id','=',$cartItem->product_variant_id)
                          ->where('customer_id','=',$user_id)
                          ->update(['coupon_discount' => 0,
                                    'coupon_id'=>null,
                                    'coupon_applied'=>null]);
                }
            }
        }
        // "seller_warehouses.name AS seller_warehouse_name" removed from the below select
        $cart = Cart::select("sellers.commission","sellers.commission_id","product_variants.shipping_weight","product_variants.shipping_length","product_variants.shipping_breadth","product_variants.shipping_height","sellers.shipping_model","sellers.store_name","products.images","products.tax","product_variants.name","product_variants.shipping_weight","product_variants.sku","product_variants.min_ship_hours","product_variants.description","carts.*","categories.primary_attribute","categories.secondary_attribute","vm.images","vm.thumbnail","product_variants.slug","product_variants.unique_id","sellers.commission_type","sellers.cod_enable","product_variants.price as seller_original_price","b.name as brand_name")
                        ->join("products","products.id",'=',"carts.product_id")
                        ->join("sellers","sellers.id",'=',"carts.seller_id")
                        ->join('brands AS b','b.id','products.brand_id')
                        ->join("product_variants","product_variants.id","carts.product_variant_id")
                        // ->join("seller_warehouses","seller_warehouses.seller_id",'=',"carts.seller_id")
                        ->join('categories','categories.id','=','products.primary_category')
                        ->join('product_variant_options AS pvo','pvo.product_variant_id','carts.product_variant_id')
                        ->leftjoin('variation_images as vm', function($join){
                            $join->on('vm.attribute_option_id', '=', 'pvo.attribute_option_id');
                            $join->on('vm.product_id','=','carts.product_id');
                        })
                        ->whereRaw("carts.customer_id = $user_id AND pvo.attribute_id = categories.primary_attribute AND pvo.deleted_at IS NULL AND product_variants.deleted_at IS NULL AND products.deleted_at IS NULL AND sellers.status = 1")
                        ->get();
        // $cart->join('variation_images as vm','vm.attribute_option_id','=','pvo.attribute_option_id')->toSql();
        //  $cart->groupBy('product_variants.product_id', 'pvo.attribute_id');
        $finalCart = [];
        $totalMrp = 0;
        $totalSellingPrice = 0;
        $totalOrderAmount = 0;
        $coupon = null;
        $totalOrderAmountForCouponCheck =0;
        
        foreach ($cart as $cartItem) {
            $totalOrderAmountForCouponCheck += $cartItem->total_price;
        }
        foreach ($cart as $key => $cartItem) {
            $totalMrp += ($cartItem->mrp * $cartItem->quantity);
            $totalSellingPrice += $cartItem->total_price;
            $totalOrderAmount = $totalSellingPrice;
            
            if($cartItem->coupon_id){
                $coupon = Coupon::where('id','=',$cartItem->coupon_id)
                                  ->where('status','=', '1')
                                  ->where('start_date', '<=', (string)date('Y-m-d H:i:s'))
                                  ->where('expiry_date', '>=', (string)date('Y-m-d H:i:s'))
                                  ->where('min_cart_amount', '<', $totalOrderAmountForCouponCheck)
                                  ->first();
                if($coupon){
                    if($coupon->dis_type == 'f' && $coupon->sub_type == 's' && $key == (count($cart)-1)){
                        $totalSellingPrice -= $cartItem->coupon_discount;
                        $totalOrderAmount  = $totalSellingPrice;
                    }else if($coupon->dis_type == 'p' && $coupon->sub_type == 's'){
                        $cartItem->total_price -= $cartItem->coupon_discount;
                        $totalSellingPrice -=$cartItem->coupon_discount;
                        $totalOrderAmount -=$cartItem->coupon_discount;
                    }else if($coupon->dis_type == 'p' && $coupon->sub_type == 'm'){
                        if($cartItem->coupon_discount > $cartItem->total_seller_discount){
                            $cartItem->total_price = ($cartItem->mrp * $cartItem->quantity) - $cartItem->coupon_discount;
                            $totalSellingPrice +=$cartItem->total_seller_discount;
                            $totalSellingPrice -=$cartItem->coupon_discount;
                            $totalOrderAmount +=$cartItem->total_seller_discount;
                            $totalOrderAmount -=$cartItem->coupon_discount;
                        }
                    }
                }else{
                    Cart::where('id', $cartItem->id)
                            ->update(['coupon_id' => null,'coupon_discount'=>0]);
                }
            }
            
            array_push($finalCart,$cartItem);
        }
        if($totalSellingPrice >= COD_MIN_AMOUNT){
            $cod = 1;
            $cod_charge = count($finalCart) * COD_CHARGE;
        }
      
        $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $totalOrderAmount AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
        $totalOrderAmount +=$handling_charges[0]->charge_amount;
        $handlingCharge = $handling_charges[0]->charge_amount;
        
        return ['cart'=>$finalCart,'totalMrp'=>$totalMrp,'totalSellingPrice'=>$totalSellingPrice,'totalOrderAmount'=>$totalOrderAmount,'coupon'=>$coupon,'handling_charges'=>$handlingCharge,'cod'=>$cod,'cod_charge'=>$cod_charge,'imagePrefixUrl'=>url(UPLOAD_PATH.'/')];
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

// Models
use App\Models\User;
use App\Models\Cart;
use App\Models\ProductVariantOption;
use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\CustomerAddress;
use App\Models\SellerWarehouses;
use App\Models\Seller;
use DB;
class CartController extends ApiController
{
    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api',['except' => ['getAttributes']]);
    }

    /**
     * Get all cart products.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        try{
            $finalCart = $this->finalCart();
            return $this->respond(['success'=>true,'finalCart'=>$finalCart]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function finalCart(){
        $cod = 0; 
        $dealExists = false;
        $cod_charge = 0;
        $user_id = auth('api')->user()->ref_id;
        if (date('Y-m-d') == date('Y-m-d', strtotime(auth('api')->user()->deal_claimed_date))) {
            $userDealsLimitExpired = true;
        }else{
            $userDealsLimitExpired = false;
        }
        $checkDealProductAlreadyExists = Cart::where('customer_id','=',$user_id)
                                               ->where('is_deal','=','y')
                                               ->exists();
       $cartProducts = Cart::selectRaw("carts.*,deals.deal_price,deals.max_orders,CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_price ELSE product_variants.price END AS final_price,CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_discount ELSE product_variants.discount END AS final_discount")
                                ->join("product_variants","product_variants.id","carts.product_variant_id")
                                ->leftjoin('deals', function($join){
                                    $join->on('deals.product_variant_id', '=', 'carts.product_variant_id')
                                         ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time');
                                })
                                ->whereRaw("carts.customer_id = $user_id AND deals.deleted_at IS NULL")
                                ->orderBy('created_at', 'ASC')
                                ->get();
        
        if(!$checkDealProductAlreadyExists && !$userDealsLimitExpired){
            foreach ($cartProducts as $cartItem) {
                if($cartItem->deal_price != null && $cartItem->max_orders > 0){
                    
                    Cart::where('product_variant_id','=',$cartItem->product_variant_id)
                          ->where('customer_id','=',auth('api')->user()->ref_id)
                          ->update(['quantity' => 1,
                                    'price' =>$cartItem->deal_price,
                                    'total_price'=>$cartItem->deal_price,
                                    'seller_discount'=> $cartItem->mrp - $cartItem->deal_price,
                                    'total_seller_discount'=>$cartItem->mrp - $cartItem->deal_price,
                                    'is_deal'=>'y',
                                    'coupon_discount'=>0,
                                    'coupon_applied'=>$cartItem->coupon_id == null ? null : 'n']);
                    break;
                }
            }
        }else{
            foreach ($cartProducts as $cartItem) {
                if(($cartItem->is_deal == 'y' && $cartItem->max_orders <= 0) ||($userDealsLimitExpired && $cartItem->is_deal == 'y')){
                        Cart::where('product_variant_id','=',$cartItem->product_variant_id)
                                ->where('customer_id','=',auth('api')->user()->ref_id)
                                ->update(['quantity' => 1,
                                            'price' =>$cartItem->final_price,
                                            'total_price'=>$cartItem->final_price,
                                            'seller_discount'=> $cartItem->final_discount,
                                            'total_seller_discount'=>$cartItem->final_discount,
                                            'is_deal'=>'n']);
                    break;
                }
            }
            if(count($cartProducts) == 1 && $cartProducts[0]["is_deal"] == 'y' && $cartProducts[0]["coupon_id"] != null ){
                Cart::where('product_variant_id','=',$cartItem->product_variant_id)
                      ->where('customer_id','=',auth('api')->user()->ref_id)
                      ->update(['coupon_discount' => 0,
                                'coupon_id'=>null,
                                'coupon_applied'=>null]);
            }
        }

        foreach ($cartProducts as $cartItem) {
            if($cartItem->deal_price != null){
                $dealExists = true;
            }
            if(($cartItem->price < $cartItem->final_price || $cartItem->price > $cartItem->final_price) && $cartItem->is_deal == 'n' ){
                Cart::where('product_variant_id','=',$cartItem->product_variant_id)
                          ->where('customer_id','=',auth('api')->user()->ref_id)
                          ->update(['price' =>$cartItem->final_price,
                                    'total_price'=>$cartItem->final_price,
                                    'seller_discount'=> $cartItem->final_discount,
                                    'total_seller_discount'=>$cartItem->final_discount]);
            }
        }
        
        // "seller_warehouses.name AS seller_warehouse_name" removed from the below select
        $cart = Cart::select("sellers.commission","product_variants.shipping_weight","product_variants.shipping_length","product_variants.shipping_breadth","product_variants.shipping_height","sellers.shipping_model","sellers.store_name","products.images","products.tax","product_variants.name","product_variants.shipping_weight","product_variants.sku","product_variants.min_ship_hours","product_variants.description","product_variants.stock","carts.*","categories.primary_attribute","categories.secondary_attribute","vm.images","vm.thumbnail","product_variants.slug","product_variants.unique_id","sellers.commission_type","sellers.commission_id","sellers.cod_enable","product_variants.price as seller_original_price","b.name as brand_name","sellers.min_order_amount","sellers.convenience_fee","sellers.company_name","products.is_customized")
                        ->join("products","products.id",'=',"carts.product_id")
                        ->join("sellers","sellers.id",'=',"carts.seller_id")
                        ->join('brands AS b','b.id','products.brand_id')
                        ->join("product_variants","product_variants.id","carts.product_variant_id")
                        // ->join("seller_warehouses","seller_warehouses.seller_id",'=',"carts.seller_id")
                        ->join('categories','categories.id','=','products.primary_category')
                        ->join('product_variant_options AS pvo','pvo.product_variant_id','carts.product_variant_id')
                        ->leftjoin('variation_images as vm', function($join){
                            $join->on('vm.attribute_option_id', '=', 'pvo.attribute_option_id');
                            $join->on('vm.product_id','=','carts.product_id');
                        })
                        ->whereRaw("carts.customer_id = $user_id AND pvo.attribute_id = categories.primary_attribute AND pvo.deleted_at IS NULL AND product_variants.deleted_at IS NULL AND products.deleted_at IS NULL AND sellers.status = 1")
                        ->get();
        // $cart->join('variation_images as vm','vm.attribute_option_id','=','pvo.attribute_option_id')->toSql();
        //  $cart->groupBy('product_variants.product_id', 'pvo.attribute_id');
        $finalCart = [];
        $groupedCart = [];
        $totalMrp = 0;
        $totalSellingPrice = 0;
        $totalOrderAmount = 0;
        $convenience_fee = 0;
        $sellerIds = [];
        $coupon = null;
        $totalSellingPriceWithoutCoupon = 0;
        $couponDiscount = 0;
        // $isCustomizable = false;
        $mrpDiscount = 0;
        $stockAvailablity = true;
        $totalOrderAmountForCouponCheck =0;
        // $customizableProducts = [];
        
        foreach ($cart as $cartItem) {
            $totalOrderAmountForCouponCheck += $cartItem->total_price;
        }
        foreach ($cart as $key => $cartItem) {
            $attributes = [];
            $totalMrp += ($cartItem->mrp * $cartItem->quantity);
            $totalSellingPrice += $cartItem->total_price;
            $totalOrderAmount = $totalSellingPrice;
            // if($cartItem->is_customized == 1){
                
            // }
            /* get the attributes of the product */
            $product_variant_options = ProductVariantOption::where('product_variant_id','=',$cartItem->product_variant_id)->get();
            foreach ($product_variant_options as $options) {
                if($options->attribute_id == $cartItem->primary_attribute){
                    $attribute = Attribute::find($options->attribute_id);
                    $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                        ->where('id','=',$options->attribute_option_id)
                                                        ->first();
                    if($attribute && $attributeOption){
                        $primaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'p');
                        array_push($attributes,$primaryAttribute);
                    }
                }else if($options->attribute_id == $cartItem->secondary_attribute ){
                    $attribute = Attribute::find($options->attribute_id);
                    $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                        ->where('id','=',$options->attribute_option_id)
                                                        ->first();
                    if($attribute && $attributeOption){
                        $secondaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'s');
                        array_push($attributes,$secondaryAttribute);
                    }
                }
            }
            $cartItem->attributes = $attributes;
            /* get the attributes of the product */

            /* rechecking the coupon expiry if coupon applied */
            if($cartItem->coupon_id){
                $coupon = Coupon::where('id','=',$cartItem->coupon_id)
                                  ->where('status','=', '1')
                                  ->where('start_date', '<=', (string)date('Y-m-d H:i:s'))
                                  ->where('expiry_date', '>=', (string)date('Y-m-d H:i:s'))
                                  ->where('min_cart_amount', '<', $totalOrderAmountForCouponCheck)
                                  ->first();
                if($coupon){
                    if($coupon->dis_type == 'f' && $coupon->sub_type == 's'){
                        $cartItem->final_discount = ($cartItem->total_seller_discount * 100) /
                        ($cartItem->mrp * $cartItem->quantity);
                        $mrpDiscount += $cartItem->total_seller_discount;
                    }
                    if($coupon->dis_type == 'f' && $coupon->sub_type == 's' && $key == (count($cart)-1)){
                        $couponDiscount += $cartItem->coupon_discount;
                        $totalSellingPrice -= $cartItem->coupon_discount;
                        $totalOrderAmount  = $totalSellingPrice;
                    }else if($coupon->dis_type == 'p' && $coupon->sub_type == 's'){
                        $cartItem->final_discount = (($cartItem->total_seller_discount +
                        $cartItem->coupon_discount) *
                        100) /
                        ($cartItem->mrp * $cartItem->quantity);
                        $couponDiscount += $cartItem->coupon_discount;
                        $cartItem->total_price -= $cartItem->coupon_discount;
                        $totalSellingPrice -=$cartItem->coupon_discount;
                        $totalOrderAmount -=$cartItem->coupon_discount;
                        $mrpDiscount += $cartItem->total_seller_discount;
                    }else if($coupon->dis_type == 'p' && $coupon->sub_type == 'm'){
                        if($cartItem->coupon_discount > $cartItem->total_seller_discount){
                            $cartItem->final_discount = ($cartItem->coupon_discount * 100) /
                            ($cartItem->mrp * $cartItem->quantity);
                            $couponDiscount += $cartItem->coupon_discount;
                            $cartItem->total_price = ($cartItem->mrp * $cartItem->quantity) - $cartItem->coupon_discount;
                            $totalSellingPrice +=$cartItem->total_seller_discount;
                            $totalSellingPrice -=$cartItem->coupon_discount;
                            $totalOrderAmount +=$cartItem->total_seller_discount;
                            $totalOrderAmount -=$cartItem->coupon_discount;
                        }else{
                            $cartItem->final_discount = ($cartItem->total_seller_discount * 100) /
                            ($cartItem->mrp * $cartItem->quantity);
                            $mrpDiscount += $cartItem->total_seller_discount;
                        }
                    }
                }else{
                    $mrpDiscount += $cartItem->total_seller_discount;
                    $cartItem->final_discount = ($cartItem->total_seller_discount * 100) /
                    ($cartItem->mrp * $cartItem->quantity);
                    Cart::where('id', $cartItem->id)
                            ->update(['coupon_id' => null,'coupon_discount'=>0]);
                }
            }else{
                $cartItem->final_discount = ($cartItem->total_seller_discount * 100) /
                    ($cartItem->mrp * $cartItem->quantity);
                $mrpDiscount += $cartItem->total_seller_discount;
            }
            /* rechecking the coupon expiry if coupon applied */

            /* appending thumbnail image path to the cart item */
            $images = json_decode($cartItem->images);
            if($images){
                if($cartItem->thumbnail != null){
                    $cartItem->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($cartItem->thumbnail-1)]);
                }else{
                        $cartItem->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                }
            }else{
                $cartItem->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
            }
             /* appending thumbnail image path to the cart item */

            /* grouping the cart for convenience fee */
            $totalSellerOriginalPrice = 0;
            $groupItems = [];
            if($cartItem->is_deal == "y"){
                $data = ['groupDesign'=>false,'convenience_fee'=>null,'seller_name'=>null,'products'=>[$cartItem]];
                    array_push($groupedCart,$data);
            }else{
                foreach ($cart as $product) {
                    if($cartItem->seller_id == $product->seller_id && $product->min_order_amount != null && $product->is_deal == "n" && !in_array($product->seller_id, $sellerIds)){
                        $totalSellerOriginalPrice += ($product->total_price - $product->coupon_discount);
                        $groupItems[] = $product;
                    }
                }
                if($totalSellerOriginalPrice < $cartItem->min_order_amount && count($groupItems) > 0){
                    $convenience_fee += $cartItem->convenience_fee;
                    $data = ['groupDesign'=>true,'convenience_fee'=>$cartItem->convenience_fee,'seller_name'=>$cartItem->company_name,'products'=>$groupItems];
                    array_push($groupedCart,$data);
                    $sellerIds[] = $cartItem->seller_id;
                }else{
                    if(!in_array($cartItem->seller_id, $sellerIds)){
                        $data = ['groupDesign'=>false,'convenience_fee'=>null,'seller_name'=>null,'products'=>[$cartItem]];
                        array_push($groupedCart,$data);
                    }
                }
            }
            /* grouping the cart for convenience fee */
            
            if($cartItem->stock < $cartItem->quantity){
                $stockAvailablity = false;
            }
            array_push($finalCart,$cartItem);
        }
        $totalOrderAmount += $convenience_fee;
        $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $totalOrderAmount AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
        $totalOrderAmount +=$handling_charges[0]->charge_amount;
        $handlingCharge = $handling_charges[0]->charge_amount;
        if($totalOrderAmount >= COD_MIN_AMOUNT){   
            $cod = 1;
            $cod_charge = count($finalCart) * COD_CHARGE;
        }
        
        return ['cart'=>$finalCart,'groupedCart'=>$groupedCart,'totalMrp'=>$totalMrp,'totalSellingPrice'=>$totalSellingPrice,'totalOrderAmount'=>round($totalOrderAmount,2),'coupon'=>$coupon,'handling_charges'=>$handlingCharge,'cod'=>$cod,'cod_charge'=>$cod_charge,'imagePrefixUrl'=>url(UPLOAD_PATH.'/'),'showDealsExpiredMsg'=>$userDealsLimitExpired && $dealExists ? true :false,'couponDiscount'=>$couponDiscount,'convenience_fee'=>$convenience_fee,'cod_min_amount'=>COD_MIN_AMOUNT,'mrpDiscount'=>$mrpDiscount,'stockAvailablity'=>$stockAvailablity];
    }
      /**
     * Get cart count.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function count()
    {
        try{
            $user_id = auth('api')->user()->ref_id;
            $cart = Cart::join("products","products.id",'=','carts.product_id')
                          ->join("product_variants","product_variants.id",'=',"carts.product_variant_id")
                          ->join("sellers","sellers.id",'=',"carts.seller_id") 
                        //   ->join("seller_warehouses","seller_warehouses.seller_id",'=',"carts.seller_id")
                          ->whereRaw("customer_id = $user_id AND products.deleted_at IS NULL AND products.status = 1 AND product_variants.deleted_at IS NULL AND sellers.deleted_at IS NULL AND sellers.status = 1 AND sellers.approval_status = 1")
                          ->count();  
            return $this->respond(['success'=>true,'count'=>$cart]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     *Get Cart.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $cart = Cart::find($id);

        return $this->respond($cart);
    }

    /**
     * Add product to cart.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'product_variant_id' => 'required|integer',
                'quantity' => 'required|integer'
            ]);
            $user_id = auth('api')->user()->ref_id;
            $cartCount = 0;
            if($validator->fails()){
                return $this->respond($validator->errors()->toJson(), 400);
            } 
            if (date('Y-m-d') == date('Y-m-d', strtotime(auth('api')->user()->deal_claimed_date))) {
                $userDealsLimitExpired = true;
            }else{
                $userDealsLimitExpired = false;
            }
    
            $checkProductExistenceInCart = Cart::where('product_variant_id','=',$request->product_variant_id)
                                                 ->where('customer_id','=',auth('api')->user()->ref_id)
                                                 ->exists();
            $checkDealProductAlreadyExists = Cart::where('customer_id','=',auth('api')->user()->ref_id)
                                                   ->where('is_deal','=','y')
                                                   ->exists();
            $productVariant = Cart::getProductVariantDetails($request->product_variant_id);
    
            if($request->quantity > $productVariant->stock){
                return response(['success' => false,'msg'=>"Stock not Available!"]);
            }
    
            $product = Cart::getProductDetails($productVariant->product_id);     
    
            if($checkProductExistenceInCart){
                $checkProductIsDealProduct = Cart::where('product_variant_id','=',$request->product_variant_id)
                                                   ->where('customer_id','=',auth('api')->user()->ref_id) 
                                                   ->where('is_deal','=','y')
                                                   ->exists(); 
                $cart = Cart::where('product_variant_id','=',$request->product_variant_id)
                              ->where('customer_id','=',auth('api')->user()->ref_id)
                              ->update(['quantity' => $productVariant->deal_price != null && !$userDealsLimitExpired && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? 1 : $request->quantity,
                                        'price' =>$productVariant->deal_price != null && !$userDealsLimitExpired &&  (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? $productVariant->deal_price : $productVariant->final_price,
                                        'total_price'=>$productVariant->deal_price != null && !$userDealsLimitExpired && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? $productVariant->deal_price  : $productVariant->final_price * $request->quantity,
                                        'seller_discount'=> $productVariant->deal_price != null && !$userDealsLimitExpired && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? ($productVariant->mrp - $productVariant->deal_price)  : $productVariant->final_discount,
                                        'total_seller_discount'=>$productVariant->deal_price != null && !$userDealsLimitExpired && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? ($productVariant->mrp - $productVariant->deal_price)  : $productVariant->final_discount * $request->quantity,
                                        'is_deal'=>$productVariant->deal_price != null && !$userDealsLimitExpired && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? 'y' : 'n']);
                $cartCount = Cart::join("products","products.id",'=','carts.product_id')
                                    ->join("product_variants","product_variants.id",'=',"carts.product_variant_id")
                                    ->join("sellers","sellers.id",'=',"carts.seller_id") 
                                    // ->join("seller_warehouses","seller_warehouses.seller_id",'=',"carts.seller_id")
                                    ->whereRaw("customer_id = $user_id AND products.deleted_at IS NULL AND products.status = 1 AND product_variants.deleted_at IS NULL AND sellers.deleted_at IS NULL AND sellers.status = 1 AND sellers.approval_status = 1")
                                    ->count(); 
                return response(['success' => true,'cartCount'=>$cartCount]); 
    
            }else{
                $seller = Seller::where('id','=', $product->seller_id)->first();
                $cart = new Cart();
                $cart->customer_id = auth('api')->user()->ref_id;
                $cart->product_id = $product->id;
                $cart->product_variant_id = $productVariant->id;
                $cart->seller_id = $product->seller_id;
                $cart->quantity = $productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired ? 1 : $request->quantity;
                $cart->mrp = $productVariant->mrp;
                $cart->total_seller_discount =$productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired ? ($productVariant->mrp - $productVariant->deal_price)  : $productVariant->final_discount * $request->quantity;
    
                $cart->price = $productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired ? $productVariant->deal_price : $productVariant->final_price;
    
                $cart->seller_discount = $productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired ? ($productVariant->mrp - $productVariant->deal_price)  : $productVariant->final_discount;
    
                $cart->total_price = $productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired ? $productVariant->deal_price  : $productVariant->final_price * $request->quantity;
                if($productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired){
                    $cart->is_deal = 'y';
                }
                $cart->seller_pincode = $seller->company_pincode;
                $address =  CustomerAddress::where('customer_id','=',auth('api')->user()->ref_id)
                                            ->where('is_default','=',1)
                                            ->first();
    
                if($address){
                    $cart->customer_pincode = $address->pincode;
                }
                $findCoupon = Cart::where('customer_id','=',auth('api')->user()->ref_id)->first();
                if(isset($findCoupon->coupon_id)){
                    $cart->coupon_id = $findCoupon->coupon_id;
                    $coupon = Coupon::where('id','=',$findCoupon->coupon_id)->first();
                    if($coupon){
                        if($coupon->dis_type == 'f' && $coupon->sub_type == 's' && ($productVariant->deal_price == null || $checkDealProductAlreadyExists)){
                            $cart->coupon_discount = $coupon->amount;
                            
                        }else if($coupon->dis_type == 'p' && $coupon->sub_type == 's' && ($productVariant->deal_price == null || $checkDealProductAlreadyExists)){
                            $cart->coupon_discount = ($coupon->amount/100) * ($productVariant->final_price * $request->quantity);
                        }else if($coupon->dis_type == 'p' && $coupon->sub_type == 'm' && ($productVariant->deal_price == null || $checkDealProductAlreadyExists)){
                            $couponDiscountAmount = ($coupon->amount/100) * ($productVariant->mrp * $request->quantity);
                            $cart->coupon_discount = $couponDiscountAmount;
                        }
                        if($productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired){
                            $cart->coupon_applied = 'n';
                        }else{
                            $cart->coupon_applied = 'y';
                        }
                    }
                }
                $cart->save(); 
                $cartCount = Cart::join("products","products.id",'=','carts.product_id')
                                    ->join("product_variants","product_variants.id",'=',"carts.product_variant_id")
                                    ->join("sellers","sellers.id",'=',"carts.seller_id") 
                                    // ->join("seller_warehouses","seller_warehouses.seller_id",'=',"carts.seller_id")
                                    ->whereRaw("customer_id = $user_id AND products.deleted_at IS NULL AND products.status = 1 AND product_variants.deleted_at IS NULL AND sellers.deleted_at IS NULL AND sellers.status = 1 AND sellers.approval_status = 1")
                                    ->count();
                
            }
            
            return $this->respond(['success'=>true,'cart'=>$cart,'cartCount'=>$cartCount]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Update cart.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCart(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'product_variant_id' => 'required|integer',
                'quantity' => 'required|numeric|gt:0'
            ]);
    
            if($validator->fails()){
                return response(['success' => false,'errors'=>$validator->messages()->toArray()], 422);  
            }
    
            $productVariant = Cart::getProductVariantDetails($request->product_variant_id);
            $product = Cart::getProductDetails($productVariant->product_id);
            if($productVariant->stock < $request->quantity){
                return response(['success'=>false,'msg'=>'No Stock!']); 
            }
            $cart = Cart::where(['customer_id'=>auth('api')->user()->ref_id, 'product_variant_id'=>$request->product_variant_id])->first();
            $cart->product_id = $product->id;
            $cart->product_variant_id = $productVariant->id;
            $cart->seller_id = $product->seller_id;
            $cart->quantity = $request->quantity;
            $cart->price = $productVariant->final_price;
            if($cart->coupon_id){
                $coupon = Coupon::where('id','=',$cart->coupon_id)->first();
                if($coupon->dis_type == 'p' && $coupon->sub_type == 's'){
                    $cart->coupon_discount = ($coupon->amount/100)* ($productVariant->final_price * $request->quantity);
                }else if($coupon->dis_type == 'p' && $coupon->sub_type == 'm'){
                    $couponDiscountAmount = ($coupon->amount/100)* ($productVariant->mrp * $request->quantity); 
                    $sellerDiscountAmount =  $productVariant->final_discount * $request->quantity;
                    if($couponDiscountAmount > $sellerDiscountAmount){
                        $cart->coupon_discount =$couponDiscountAmount;
                    }else{
                        $cart->coupon_discount = 0;
                    }
                }
            }
            // $cart->seller_discount = $product->seller_discount * $request->quantity;
            $cart->total_seller_discount = $productVariant->final_discount * $request->quantity;
            $cart->total_price = $productVariant->final_price * $request->quantity;
            $cart->save();
            $finalCart = $this->finalCart();
            return response(['success'=>true,'finalCart'=>$finalCart]); 
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Remove product from cart.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove($id)
    {
        try{
            $user_id = auth('api')->user()->ref_id;
            $delete = Cart::where(['customer_id'=>$user_id, 'product_variant_id'=>$id])->delete();
            $finalCart = $this->finalCart();
            $cartCount =Cart::join("products","products.id",'=','carts.product_id')
            ->join("product_variants","product_variants.id",'=',"carts.product_variant_id")
            ->join("sellers","sellers.id",'=',"carts.seller_id") 
            // ->join("seller_warehouses","seller_warehouses.seller_id",'=',"carts.seller_id")
            ->whereRaw("customer_id = $user_id AND products.deleted_at IS NULL AND products.status = 1 AND product_variants.deleted_at IS NULL AND sellers.deleted_at IS NULL AND sellers.status = 1 AND sellers.approval_status = 1")
            ->count();
            if($delete) {
                return response(['success' => true,'finalCart'=>$finalCart,'cartCount'=>$cartCount]); 
            }else{
                return response(['success' => false,'cartCount'=>$cartCount]); 
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function getAttributes(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'product_variant_id' => 'required',
                'primary_attribute' => 'required',
                'secondary_attribute' => 'required'
            ]);
            if($validator->fails()){
                return $this->respond($validator->errors()->toJson(), 400);
            } 
    
            $product_variant_options = ProductVariantOption::where('product_variant_id','=',$request->product_variant_id)->get();
            $attributes = [];
            foreach ($product_variant_options as $options) {
                if($options->attribute_id == $request->primary_attribute){
                    $attribute = Attribute::find($options->attribute_id);
                    $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                        ->where('id','=',$options->attribute_option_id)
                                                        ->first();
                    if($attribute && $attributeOption){
                        $primaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'p');
                        array_push($attributes,$primaryAttribute);
                    }
                }else if($options->attribute_id == $request->secondary_attribute ){
                    $attribute = Attribute::find($options->attribute_id);
                    $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                        ->where('id','=',$options->attribute_option_id)
                                                        ->first();
                    if($attribute && $attributeOption){
                        $secondaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'s');
                        array_push($attributes,$secondaryAttribute);
                    }
                }
            }
            return response(['success' => true,'attributes'=>$attributes]); 
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }
}

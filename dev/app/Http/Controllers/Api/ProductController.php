<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Api\Customer;
use DB;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Seshac\Shiprocket\Shiprocket;
use App\Http\Resources\SearchCollection;
// Models
use App\Models\Product;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\ChildCategory;
use App\Models\ProductCategory;
use App\Models\ProductVariant;
use App\Models\ProductVariantOption;
use App\Models\Brand;
use App\Models\ProductTag;
use App\Models\AttributeOption;
use App\Models\Seller;
use App\Models\Slider;
use App\Models\CustomerAddress;
use App\Models\OrderDetail;
use App\Models\Coupon;
use App\Models\Deals;
use DateTime;
use Carbon\Carbon;
use Facades\App\Repository\SuggestionRepo;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Str;

class ProductController extends ApiController
{   
    /**
     * Get product list and sort by different parameters
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function __construct() {
        date_default_timezone_set("Asia/Kolkata");
    }

    

    public function productList(Request $request)
    {
        try{
            $filters = [
                'categories' => $request->cids, 
                'subcategories' => $request->scids,
                'childcategories' => $request->ccids,
                'brands' => $request->brands,
                'discount' => $request->discount,
                'ediscount' => $request->ediscount,
                'udiscount' => $request->udiscount,
                'attrs' => $request->attrs,
                'minpr' => $request->minpr,
                'maxpr' => $request->maxpr,
                'sort' => $request->sortBy?:'popularity',
                'page' => $request->page?:1,
                'q'=>$request->q,
                'tag'=>$request->tag,
                'coupon'=>$request->coupon,
            ];
            $products = Product::getListingProducts($filters);
            return $this->respond(['success'=>true,'data'=>$products]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }

    /**
     * header search autosuggestion
     * 
     */

    public function clean($search_param) {  // removing white spaces and special chars
        $search_param = str_replace(' ', '-', $search_param); // Replaces all spaces with hyphens.
     
        return preg_replace('/[^A-Za-z0-9\-]/', '', $search_param); // Removes special chars.
    }

    public function autoSuggestionSearch(Request $request)
    {
        try{
            $search_param = $request->name;  // search param
            $clean_search_param = $this->clean($search_param);  // clean string  
            $split_string = str_split($clean_search_param, 4); // split string
            
            $categories = [];
            $products = [];
            if(isset($search_param)){ 
                
                // 1. category seacrh
                
                $categories =  DB::table('view_categories')->where(function ($c) use ($split_string) {
                                foreach ($split_string as $value) {
                                    $c->orWhere('child_slug', 'like', "%{$value}%");
                                    $c->orWhere('subcategory_title', 'like', "%{$value}%");
                                    $c->orWhere('category_title', 'like', "%{$value}%");
                                }
                                })->limit(5)->orderBy('child_id', 'ASC')->get(); 
    
                // 2. brand search
    
                $brands = Brand::where(function ($b) use ($split_string) {
                                foreach ($split_string as $value) {
                                    $b->orWhere('name', 'like', "%{$value}%");
                                }
                                })->select('name','id')->limit(5)->get();        
    
                // 3. tags serach
    
                $tags =  DB::table('view_product_tags')->where(function ($t) use ($split_string) {
                                foreach ($split_string as $value) {
                                    $t->orWhere('name', 'like', "%{$value}%");
                                }
                                })->limit(5)->get(); 
                                
                                
                // 4. tags ids serach
    
                $tag_ids = ProductTag::join("assigned_product_tags as apt",'product_tags.id','=','apt.tag_id',)
                                        ->where('product_tags.name', 'LIKE', '%'.$clean_search_param.'%')
                                        ->pluck('apt.product_id');
                                                                         
    
    
                if(count($tag_ids) <= 0){
                    $tag_ids = [0];
                }
                else
                {
                    $tag_ids = $tag_ids; 
                }
                
                // 5. products search
                
                $products = DB::table("view_product_variants")->where(function ($q) use ($split_string,  $tag_ids) {
                        
                        foreach ($split_string as $value) {
                            
                            $q->orWhere('slug', 'like', "%{$value}%");
                            $q->orWhere('sku', 'like', "%{$value}%");
                            $q->orWhere('brand_name', 'like', "%{$value}%");
                            $q->orWhereIn('id', $tag_ids);
                            
                        }
                        })->limit((20 - count($categories)))->orderBy('id','asc')->get();
                         
                            
                            
                // $products =  DB::select( DB::raw("SELECT * FROM products WHERE name SOUNDS LIKE '%$search_param%' ") );
            }
            // return $this->respond($categories);
            return $this->respond(['success'=>true,'categories'=>$categories,'products'=>$products,'brands'=>$brands,'tags'=>$tags,'imgUrl'=>url(UPLOAD_PATH)]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    
    public function deals(){
        try{
            $products =  Deals::selectRaw("product_variants.id,product_variants.name,product_variants.slug,product_variants.sku,product_variants.mrp,product_variants.discount,product_variants.price,product_variants.fabpik_seller_price,product_variants.fabpik_seller_discount,brands.name AS brand_name,product_variants.unique_id,'product' AS 'type',(product_variants.mrp - deals.deal_price ) / product_variants.mrp * 100 AS discount_percent,imgs.images,imgs.thumbnail,deals.start_time,deals.end_time,deals.deal_price,deals.total_max_orders,deals.max_orders")
                                            ->join('products','products.id','=','deals.product_id')
                                            ->join('product_variants','product_variants.id','=','deals.product_variant_id')
                                            ->join('brands','brands.id','=','products.brand_id')
                                            ->join('sellers','sellers.id','products.seller_id')
                                            ->join('view_product_images as imgs','imgs.product_variant_id','=','deals.product_variant_id')
                                            ->whereRaw("NOW( ) BETWEEN deals.start_time AND deals.end_time AND deals.deleted_at IS NULL")
                                            ->groupBy('deals.product_id')
                                            ->orderBy('deals.deal_price','ASC')
                                            ->get();
            
            $productsTemp = [];
            if($products) {
                foreach ($products as $product) {   
                    // $start_time  = strtotime($product->start_time);
                    // $end_time = strtotime($product->end_time);
                    // $differenceInSeconds = $end_time - $start_time;
                    $start_time = new DateTime(date('Y-m-d H:i:s'));
                    $end_time = new DateTime($product->end_time);
                    $interval = $end_time->diff($start_time);
                    $diffInSeconds = $interval->s; //45
                    $diffInMinutesToSec = $interval->i * 60; //23
                    $diffInHoursToSec   = $interval->h * 3600; //8
                    $diffInDaysToSec    = $interval->d * 86400; //21
                    $diffInMonthsToSec  = $interval->m * 2629800; //4
                    $diffInYearsToSec   = $interval->y * 31557600; //1
    
                    $totalSeconds = $diffInSeconds + $diffInMinutesToSec + $diffInHoursToSec + $diffInDaysToSec + $diffInMonthsToSec + $diffInYearsToSec;
                    if($product->total_max_orders == 0 && $product->max_orders == 0){
                        $product->claimed = 100;
                    }else{
                        $product->claimed = number_format(($product->total_max_orders - $product->max_orders)/$product->total_max_orders * 100,1)+0;
                    }
                    $product->timeLeft = $totalSeconds;
                    $product->interval = $interval;
                    $images = json_decode($product->images);
                    if($images){
                        if($product->thumbnail != null){
                            $product->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($product->thumbnail-1)]);
                        }else{
                            $product->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                        }
                        array_push($productsTemp, $product);
                    }else{
                        $product->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                        array_push($productsTemp, $product);
                    }
                }
            }
       
            $upcomingDeals = Deals::selectRaw("product_variants.id,product_variants.name,product_variants.slug,product_variants.sku,product_variants.mrp,product_variants.discount,product_variants.price,product_variants.fabpik_seller_price,product_variants.fabpik_seller_discount,brands.name AS brand_name,product_variants.unique_id,'product' AS 'type',(product_variants.mrp - deals.deal_price ) / product_variants.mrp * 100 AS discount_percent,imgs.images,imgs.thumbnail,deals.start_time,deals.end_time,deals.deal_price,deals.total_max_orders,deals.max_orders")
                                            ->join('products','products.id','=','deals.product_id')
                                            ->join('product_variants','product_variants.id','=','deals.product_variant_id')
                                            ->join('brands','brands.id','=','products.brand_id')
                                            ->join('sellers','sellers.id','products.seller_id')
                                            ->join('view_product_images as imgs','imgs.product_variant_id','=','deals.product_variant_id')
                                            // ->whereRaw("deals.publish_date < NOW( ) AND deals.start_time < NOW( )")
                                            ->whereRaw("NOW( ) BETWEEN deals.publish_date AND deals.start_time")
                                            ->groupBy('deals.product_id')
                                            ->orderBy('deals.deal_price','ASC')
                                            ->get();
            $upcomingDealsTemp = [];
            if($upcomingDeals){
                foreach ($upcomingDeals as $product) {
                    $current_time  = strtotime(date('Y-m-d H:i:s'));
                    $end_time = strtotime($product->start_time);
                    $startsInSeconds = $end_time - $current_time;
                    $deal_start_time  = strtotime($product->start_time);
                    $deal_end_time = strtotime($product->end_time);
                    $dealTimeInSeconds = $deal_end_time - $deal_start_time;
                    // $start_time = new DateTime(date('Y-m-d H:i:s'));
                    // $end_time = new DateTime($product->start_time);
                    // $interval = $end_time->diff($start_time);
                    // $diffInSeconds = $interval->s; //45
                    // $diffInMinutesToSec = $interval->i * 60; //23
                    // $diffInHoursToSec   = $interval->h * 3600; //8
                    // $diffInDaysToSec    = $interval->d * 86400; //21
                    // $diffInMonthsToSec  = $interval->m * 2629800; //4
                    // $diffInYearsToSec   = $interval->y * 31557600; //1
    
                    // $totalSeconds = $diffInSeconds + $diffInMinutesToSec + $diffInHoursToSec + $diffInDaysToSec + $diffInMonthsToSec + $diffInYearsToSec;
                    if($product->total_max_orders == 0 && $product->max_orders == 0){
                        $product->claimed = 100;
                    }else{
                        $product->claimed = number_format(($product->total_max_orders - $product->max_orders)/$product->total_max_orders * 100,1)+0;
                    }
                    $product->startsIn = $startsInSeconds;
                    $product->dealTimeInSeconds = $dealTimeInSeconds;
                    // $product->interval = $interval;
                    $images = json_decode($product->images);
                    if($images){
                        if($product->thumbnail != null){
                            $product->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($product->thumbnail-1)]);
                        }else{
                            $product->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                        }
                        array_push($upcomingDealsTemp, $product);
                    }else{
                        $product->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                        array_push($upcomingDealsTemp, $product);
                    }
                }
            }
            return $this->respond(['success'=>true,'products'=>$productsTemp,'upcomingDeals'=>$upcomingDealsTemp]); 
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function nextVariantNavigation(Request $request){
        try{
            $product_id = $request->product_id;
            $product = ProductVariant::select("slug","unique_id","id")->whereRaw("product_id = $product_id AND stock > 0")->first();
            if($product){
                return $this->respond(['success'=>true,'data'=>$product]); 
            }else{
                return $this->respond(['success'=>false]); 
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
                                   
    }

    public function sliderImages($view){
        try{
            $images = null;
            if($view == 'd'){
                $images = Slider::whereRaw("view = 0 AND NOW( ) BETWEEN start_date AND end_date AND status = 1 AND deleted_at IS NULL")
                                  ->orderBy('display_order','ASC')
                                  ->get();
            }else if($view == 'm'){
                $images = Slider::whereRaw("view = 1 AND NOW( ) BETWEEN start_date AND end_date AND status = 1 AND deleted_at IS NULL")
                                  ->orderBy('display_order','ASC')
                                  ->get();
          
            }
            return $this->respond(['success'=>true,'images'=>$images,'imgUrl'=>url(UPLOAD_PATH)]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
        }
       /**
     * 
     * popolar categories
     */
    public function popularCategory(Request $request)
    {
        try{
            $final_category = [];
            $sql = "SELECT
                        t1.*,
                        t2.title AS subcat_title,
                        t2.slug AS subcat_slug,
                        t3.slug AS cat_slug
                    FROM
                        (
                        SELECT
                            id,
                            category_id,
                            subcategory_id,
                            title,
                            slug
                        FROM
                            `childcategories`
                        WHERE
                            subcategory_id IN(
                            SELECT
                                id
                            FROM
                                subcategories
                            ORDER BY
                                RAND())
                        ) AS t1
                    JOIN subcategories AS t2
                    ON
                        t2.id = t1.subcategory_id
                    JOIN categories AS t3
                    ON
                        t3.id = t2.category_id
                    ORDER BY
                        RAND()
                    LIMIT 5";
            $categories = DB::select($sql);
            return $this->respond(['success'=>true,'categories'=>$categories]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }
    /**
     * search functioanlity
     * 
     */

     public function topSellingProducts(){
         try{
             $topSellingProducts = OrderDetail::selectRaw("order_details.product_variant_id,order_details.product_id,order_details.seller_id,s.company_name,pv.mrp,pv.discount,pv.price,pv.name,pv.fabpik_seller_price,pv.fabpik_seller_discount,pv.unique_id,pv.slug,vm.images,vm.thumbnail,vm.thumbnail_img_name,SUM(order_details.quantity) as total_sold,deals.start_time,deals.end_time,deals.deal_price,pv.stock,TRUNCATE((pv.discount * 100)/pv.mrp,1) AS discount,IF(
                deals.deal_price,
                TRUNCATE((
                    pv.mrp - deals.deal_price 
                ) / pv.mrp * 100,1),
                0
            ) AS deal_discount,CASE WHEN NOW( ) BETWEEN pv.special_price_start_date AND pv.special_price_end_date THEN pv.fabpik_seller_price ELSE pv.price END AS final_price,
            CASE WHEN NOW( ) BETWEEN pv.special_price_start_date AND pv.special_price_end_date THEN TRUNCATE(pv.fabpik_seller_discount_percentage,1)  ELSE TRUNCATE((pv.discount * 100)/pv.mrp,1) END AS final_discount")
                        ->join("products as p", function($join) {
                            $join->on("p.id", "order_details.product_id");
                            })  
                        ->join("product_variants as pv", function($join) {
                            $join->on("pv.id", "order_details.product_variant_id");
                            })
                        ->join("categories as c", function($join) {
                            $join->on("c.id", "p.primary_category");
                            })
                        ->join("product_variant_options as pvo", function($join) {
                            $join->on("pvo.product_variant_id", 'order_details.product_variant_id');
                            })
                        ->join("sellers as s", function($join) {
                            $join->on("s.id", 'order_details.seller_id');
                            })
                        ->join("variation_images as vm", function($join) {
                            $join->on("vm.attribute_option_id", "pvo.attribute_option_id")
                                ->on("vm.product_id", "order_details.product_id");
                            })
                        ->leftjoin('deals', function($join){
                                    $join->on('deals.product_variant_id', '=', 'pv.id')
                                         ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time AND deals.max_orders > 0');
                            })
                        ->whereRaw("pvo.attribute_id = c.primary_attribute AND pvo.deleted_at IS NULL AND pv.deleted_at IS NULL AND p.deleted_at IS NULL AND deals.deleted_at IS NULL AND s.status = 1")
                        ->groupBy("seller_id")
                        ->groupBy("product_variant_id")
                        ->orderByRaw("total_sold DESC")
                        ->limit(10)
                        ->get();    
            return $this->respond(['products'=>$topSellingProducts,'imgUrl'=>url(UPLOAD_PATH)]); 
         }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
     }

    public function search(Request $request){

        $productsearch = Product::getSearchData($request);
        
        $i = 0;
        foreach($productsearch as $key => $product){
          
                $data	= DB::table('product_variant_options AS pvo') 
                            ->select([DB::raw('group_concat(DISTINCT ao.option_name ) as option_name'), DB::raw('group_concat(DISTINCT ao.colour_code ) as color_code') ,'pvo.product_variant_id','a.display_name'])
                            ->join('product_variants AS pv','pv.id','pvo.product_variant_id')
                            ->join('attribute_options AS ao','ao.id','pvo.attribute_option_id')
                            ->join('attributes AS a','a.id','ao.attribute_id')
                            ->where('pv.product_id','=' , $product->product_id)
                            ->groupBy('a.display_name')
                            ->get(); 
                         foreach($data as $key => $d){    
                                              
                            if($d->display_name == 'Size')
                            { $options = explode(",",$d->option_name);
                               
                                  $d->option_name = $options; 
                                $productsearch[$i]->variantsize = $d;
                            }
                            if($d->display_name == 'Color'){
                                $options = explode(",",$d->option_name);
                            $options = explode(",",$d->color_code);
                                $d->color_code = $options; 
                                $productsearch[$i]->variantcolor = $d;
                            }
                             
                        } 
                       
                $seller = Seller::with('product')->select('company_name')->where('id',$product->seller_id)->first();  
                $productsearch[$i]->seller = $seller->toArray(); 
                $i++;
                     
        }

    $productdata = Product::getProductimagespath($productsearch);
    return $productdata;

    }
    /**
     * Get flash deal product list.
     *
     * @return \Illuminate\Http\JsonResponse
    */

    public function similarProducts($ccid,$unique_id){
        try{
            $product = ProductVariant::selectRaw("pvo.product_variant_id, product_variants.unique_id, product_variants.name, product_variants.slug,product_variants.unique_id, product_variants.sku, product_variants.stock, product_variants.mrp, product_variants.discount, product_variants.price, p.id AS product_id, p.seller_id, vm.images, vm.thumbnail,vm.thumbnail_img_name, s.company_name as seller_company_name,product_variants.fabpik_seller_price,product_variants.fabpik_seller_discount,deals.start_time,deals.end_time,deals.deal_price,CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_price ELSE product_variants.price END AS final_price,CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1)  ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) END AS final_discount,IF(
                deals.deal_price,
                TRUNCATE((
                    product_variants.mrp - deals.deal_price 
                ) / product_variants.mrp * 100,1),
                0
            ) AS deal_discount")
                            ->join('product_variant_options AS pvo','pvo.product_variant_id','product_variants.id')
                            ->join('products AS p','p.id','product_variants.product_id')
                            ->join('categories AS c','c.id','p.primary_category')
                            ->join('brands AS b','b.id','p.brand_id')
                            ->leftjoin('variation_images as vm', function($join){
                                $join->on('vm.attribute_option_id', '=', 'pvo.attribute_option_id');
                                $join->on('vm.product_id','=','product_variants.product_id');
                            })
                            ->leftjoin('deals', function($join){
                                $join->on('deals.product_variant_id', '=', 'product_variants.id')
                                     ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time AND deals.max_orders > 0');
                            })
                            ->join('product_categories AS pc','pc.product_id','product_variants.product_id')
                            ->join('sellers AS s','s.id','p.seller_id')
                            ->whereRaw("pvo.attribute_id = c.primary_attribute AND product_variants.unique_id != $unique_id  AND p.status=1 AND p.deleted_at IS null AND product_variants.deleted_at IS null AND c.status=1 AND c.deleted_at is null AND s.status=1 AND s.approval_status=1 AND s.deleted_at is null AND deals.deleted_at IS NULL AND pvo.deleted_at IS NULL");
    
            $product->whereIn('pc.childcategory_id', [$ccid]);
            $product->groupBy('product_variants.product_id', 'pvo.attribute_id');
            $products = $product->limit(10)->get();
    
            return $this->respond(['success'=>true,'products'=>$products,'imgUrl'=>url(UPLOAD_PATH)]); 
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }

    public function flashDeals()
    {
        try{
            $products = DB::select("SELECT
                                    `products`.`seller_id`,
                                    `pv`.`name`,
                                    `pv`.`description` AS `pv_desc`,
                                    `pv`.`product_id`,
                                    `pv`.`unique_id`,
                                    `pv`.`id` AS `product_variant_id`,
                                    `pv`.`slug`,
                                    `pv`.`stock`,
                                    `pv`.`mrp` AS `mrp`,
                                    `pv`.`price` AS `price`,
                                    `pv`.`fabpik_seller_price`,
                                    `pv`.`fabpik_seller_discount`,
                                    `vm`.`images`,
                                    `vm`.`thumbnail`,
                                    `vm`.`thumbnail_img_name`,
                                    `sellers`.`company_name`,
                                    `deals`.`start_time`,
                                    `deals`.`end_time`,
                                    `deals`.`deal_price`,
                                    CASE WHEN NOW( ) BETWEEN pv.special_price_start_date AND pv.special_price_end_date THEN pv.fabpik_seller_price ELSE pv.price END AS final_price,
                                    CASE WHEN NOW( ) BETWEEN pv.special_price_start_date AND pv.special_price_end_date THEN TRUNCATE(pv.fabpik_seller_discount_percentage,1)  ELSE TRUNCATE((pv.discount * 100)/pv.mrp,1) END AS final_discount,
                                    IF(
                                        deals.deal_price,
                                        TRUNCATE((
                                            pv.mrp - deals.deal_price 
                                        ) / pv.mrp * 100,1),
                                        0
                                    ) AS deal_discount
    
                                    FROM
                                        `products`
                                    INNER JOIN `product_variants` AS `pv`
                                    ON
                                        `pv`.`product_id` = `products`.`id`
                                    INNER JOIN `sellers`
                                    ON
                                        `sellers`.`id` = `products`.`seller_id`
                                    INNER JOIN `categories` AS `c`
                                    ON
                                        `c`.`id` = `products`.`primary_category`
                                    INNER JOIN `product_variant_options` AS `pvo`
                                    ON
                                        `pvo`.`product_variant_id` = `pv`.`id`
                                    LEFT JOIN `deals` 
                                    ON 
                                        `deals`.`product_variant_id` = `pv`.`id` AND NOW() BETWEEN deals.start_time AND deals.end_time AND deals.max_orders > 0
                                    INNER JOIN `variation_images` AS `vm`
                                    ON
                                        `vm`.`attribute_option_id` = `pvo`.`attribute_option_id`
                                    WHERE
                                        products.fabpik_featured = 1 AND pvo.attribute_id = c.primary_attribute AND products.status = 1 AND `vm`.`product_id` = `products`.`id` AND products.deleted_at IS NULL AND `products`.`deleted_at` IS NULL AND deals.deleted_at  IS NULL AND sellers.status = 1
                                    GROUP BY
                                        products.id
                                    ORDER BY
                                        `products`.`no_of_items`
                                    DESC"
                                );
            return $this->respond(['success'=>true,'products'=>$products,'imgUrl'=>url(UPLOAD_PATH)]);   
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Get category wise product list.
     * 
     * @return \Illuminate\Http\JsonResponse
    */
    public function category($id)
    {
        try{
            $products =  Product::with('productcategory','productvariant','seller')
                            ->whereHas('productcategory', function ($query) use($id){
                                return $query->where('category_id', $id);
                            })
                            ->orderBy('no_of_items', 'DESC')
                            ->get();
                
            // $productdata = Product::getProductimagespath($products);
            return $this->respond($products);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Related product list.
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function related($id)
    {
        try{
            $product = Product::find($id);
            $products =  Product::with('productcategory','productvariant','seller')
                                 ->whereHas('productcategory', function ($query) use($product){
                                        return $query->where('subcategory_id', $product->id);
                                        })
                                 ->where('id', '!=', $id)
                                 ->orderBy('no_of_items', 'DESC')
                                 ->get();
            // $productdata = Product::getProductimagespath($products);
            return $this->respond($products);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }


    public function getProductsFilters(Request $request)	
    {	
        try{
            $allSubCats = [];
            $allChildCats = [];
            if($request->subCategorySlug == null &&  $request->currentChildCategorySlug == null){
                $parentCategory = Category::where('slug','=',$request->parentCategorySlug)->first();
                $allSubCats = Subcategory::selectRaw("subcategories.*")
                                           ->whereRaw("subcategories.category_id",'=',$parentCategory->id)
                                           ->get();
                $allChildCats = ChildCategory::selectRaw("childcategories.*")
                                               ->whereRaw("childcategories.category_id",'=',$parentCategory->id)
                                               ->get();
            }else if($request->currentChildCategorySlug == null || $request->currentChildCategorySlug != null){
                $parentCategory = Category::where('slug','=',$request->parentCategorySlug)->first();
                $subCategory = Subcategory::where('category_id','=',$parentCategory->id)
                                            ->where('slug','=',$request->subCategorySlug)
                                            ->first();
                $childCategories = ChildCategory::where('category_id','=',$parentCategory->id)
                                                 ->where('subcategory_id','=',$subCategory->id)
                                                 ->get();
                $categories = ChildCategory::getChildCategoryFilters($childCategories,$request->currentChildCategorySlug);
            }else{
                $allSubCategories = Subcategory::where('category_id','=',$parentCategory->id)
                                             ->get();
                $allParentCategories = Category::all();
                // $categories = 
            }
            $getProductBrands = Product::getProductBrands($parentCategory,$subCategory,$childCategories);
            $minMaxfilters = Product::getMinMaxPrice($parentCategory,$subCategory,$childCategories);	
            $attributes = Product::getProductAttributes($parentCategory);
            $returns = ['success'=>true,'categories'=>$categories,'brands'=>$getProductBrands,'minMaxPrices'=>$minMaxfilters,'attributes'=>$attributes];
            return $this->respond($returns);	
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function getAttributeFilters(Request $request){
        try{
            $attributes = Product::getProductAttributes($request->parentCategory);
            return $this->respond(['success'=>true,'attributes'=>$attributes]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function getGlobalSearchFilters(){
        try{
            $parentCategories = Category::all();
            $brands = Brand::all();
            return $this->respond(['parentCategories'=>$parentCategories,'brands'=>$brands]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function getParentCategoires(){
        try{
            $parentCategories = Category::all();
            return $this->respond(['success'=>true,'parentCategories'=>$parentCategories]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }

    public function getSubCategories(Request $request){
        try{
            $parentCategory = Category::where('id','=',$request->parentCategoryId)->first();
            $subCategories = Subcategory::where('category_id','=',$request->parentCategoryId)->get();
            return $this->respond(['success'=>true,'subCategories'=>$subCategories,'parentCategory'=>$parentCategory]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }

    public function getSubCategoriesBySlugs(Request $request){
        try{
            $parentCategory = Category::where('slug','=',$request->parentCategorySlug)->first();
            if($parentCategory){
                $subCategories = Subcategory::where('category_id','=',$parentCategory->id)->get();
                if($subCategories){
                    return $this->respond(['success'=>true,'subCategories'=>$subCategories,'parentCategory'=>$parentCategory]);
                }else{
                    return $this->respond(['success'=>false]);
                }
            }else{
                return $this->respond(['success'=>false]);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function getChildCategories(Request $request){
        try{
            $parentCategory = Category::where('id','=',$request->parentCategoryId)->first();
            $subCategory = Subcategory::where('id','=',$request->subCategoryId)->first();
            $childCategories = ChildCategory::where('subcategory_id','=',$request->subCategoryId)->get();
            return $this->respond(['success'=>true,
                                   'parentCategory'=>$parentCategory,
                                   'subCategory'=>$subCategory,
                                   'childCategories'=>$childCategories]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function getChildCategoriesBySlug(Request $request){
        try{
            $parentCategory = Category::where('slug','=',$request->parentCategorySlug)->first();
            if($parentCategory){
                $subCategory = Subcategory::where('slug','=',$request->subCategorySlug)
                                            ->where('category_id','=',$parentCategory->id)
                                            ->first();
                if($subCategory){
                    $childCategories = ChildCategory::where('subcategory_id','=',$subCategory->id)->get();
                    if($childCategories){
                        return $this->respond(['success'=>true,
                                               'parentCategory'=>$parentCategory,
                                               'subCategory'=>$subCategory,
                                               'childCategories'=>$childCategories]);
                    }else{
                        return $this->respond(['success'=>false]);
                    }
                }else{
                    return $this->respond(['success'=>false]);
                }
            }else{
                return $this->respond(['success'=>false]);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }

    public function getAllBrands(Request $request){
        try{
            $category = Category::find($request->cid);
            if(isset($category->brands)){
                $brands = Brand::where('status','=',1)
                                ->whereIn('id',json_decode($category->brands))
                                ->get();
                if(!$brands || count($brands) == 0){
                    $brands = Brand::where('status','=',1)
                                    ->get();
                }
            }else{
                $brands = Brand::where('status','=',1)
                                ->get();
            }
            return $this->respond(['success'=>true,'brands'=>$brands]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function getFilteredData(Request $request)
    {   
       
        $params=$request->params;
        $i=0;
        $index=0;
        $new_elements_array=[];
        $new_brands_array=[];
        $new_category_array=[];
        if(isset($params['varId']) && $params['varVal']){
            foreach ($params['varId'] as $key => $value) {
                if(isset($array) && in_array($value,$array)){
                    if(isset($params['varVal'][$index]) && !in_array($params['varVal'][$index],$new_elements_array[$i]['varVal']))
                    $new_elements_array[$i]["varVal"]=$params['varVal'][$index]['varVal'];
                }
                else{
                    $i++;
                    $array[]=$value;
                    $new_elements_array[$i]["varId"]=$value;
                    $new_elements_array[$i]["varVal"]=$params['varVal'][$index]['varVal'];
                }
                $index++;
            }
        }

     
        if(isset($params['varBrandId'])){
                $new_brands_array=$params['varBrandId'];
        }
        if(isset($params['filterCId'])){
            $new_category_array=$params['filterCId'];
        }
        $filterdata = [
            'variants'=> $new_elements_array,
            'brands'=> $new_brands_array,
            'childcategory'=>$new_category_array
        ];
        // print_r($filterdata);
        $productfilters = Product::getFrontendFilteredData($params['minBug'],$params['maxBug'],$params,$filterdata);
     
        // $data['data']=$productfilters;  
        return $this->respond($productfilters); 
    }
    public function productDetails($product_variant_slug,$unique_id)	
    {	
        try{
            $productdetails = Product::productDetails($product_variant_slug,$unique_id);
            if($productdetails == null){
                return $this->respond(['msg'=>'Product not found','success'=>false]); 
            }
            return $this->respond(['success'=>true,'data'=>$productdetails]); 
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
     
    }

    public function checkPincode($unique_id,Request $request)
	{
        try{
            $defaultAddress =  CustomerAddress::where('customer_id','=',auth('api')->user()->ref_id)
                                                ->where('is_default','=',1)
                                                ->first();
            if($defaultAddress){
                $seller_pincode = ProductVariant::selectRaw("s.company_pincode,product_variants.min_ship_hours,product_variants.shipping_weight")
                                                    ->join("products as p","p.id","product_variants.product_id")
                                                    ->join("sellers as s","s.id","p.seller_id")
                                                    ->whereRaw("product_variants.unique_id = $unique_id")
                                                    ->first();
                $pincodeDetails = [
                    'pickup_postcode' => $seller_pincode !=null ? $seller_pincode->company_pincode : '500081',
                    'delivery_postcode' => $defaultAddress->pincode,
                    'weight' => $seller_pincode->shipping_weight ? $seller_pincode->shipping_weight : '0.1',
                    'cod' => 0,
                    'couriers_type'=>true
                ];
                $token =  Shiprocket::getToken(); 
                $response = Shiprocket::courier($token)->checkServiceability($pincodeDetails);
                $shiprocketDelivery = json_decode($response); 
                if($shiprocketDelivery->status == 404){
                    return $this->respond(['success'=>2,'msg'=>$shiprocketDelivery->message,'address'=>$defaultAddress]);
                }
                $etd_hours = $shiprocketDelivery->data->available_courier_companies[0]->etd_hours;
                $totalEtd = $seller_pincode->min_ship_hours + $etd_hours;
                $hoursToDays = ceil($totalEtd/24);
                $currentDate = date("d M Y");
                $addedDays = date('d M Y', strtotime($currentDate. ' + '."$hoursToDays".' days'));
                // $etd = date("M d, Y", strtotime($shiprocketDelivery->data->available_courier_companies[0]->etd));
                $data = ['address'=>$defaultAddress, 'estimateddelivery'=>$addedDays];
                return $this->respond($data);   
            }
            else{
                $this->respond(['success'=>false]);
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }  
    
    
    public function getCouponsForProductDetails(){
        try{
            $currentDate = date('Y-m-d H:i:s');
            $coupons = Coupon::where('status','=', 1)
                               ->whereDate('start_date', '<=', $currentDate) 
                               ->where('expiry_date', '>=', $currentDate)    
                               ->get();
            return response(['coupons'=>$coupons]); 
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function checkenterPincode(Request $request) 
    {
        try{
            $validator = Validator::make($request->all(), [
                'pincode' => 'required|integer|digits:6',
                // 'weight' => 'required'
            ]);
    
            if($validator->fails()){
                return $this->respond($validator->errors()->toJson(), 400);
            }
    
            $unique_id = $request->unique_id;
            $seller_pincode = ProductVariant::selectRaw("s.company_pincode,product_variants.min_ship_hours,product_variants.shipping_weight")
                                                ->join("products as p","p.id","product_variants.product_id")
                                                ->join("sellers as s","s.id","p.seller_id")
                                                ->whereRaw("product_variants.unique_id = $unique_id")
                                                ->first();
            $pincodeDetails = [
                'pickup_postcode' => $seller_pincode !=null ? $seller_pincode->company_pincode : '500081',
                'delivery_postcode' => $request->pincode,
                'weight' => $seller_pincode->shipping_weight ? $seller_pincode->shipping_weight : '0.1',
                'cod' => 0,
                'couriers_type'=>true
            ];
            $token =  Shiprocket::getToken();
            $response = Shiprocket::courier($token)->checkServiceability($pincodeDetails);
            $shiprocketDelivery = json_decode($response);
            if($shiprocketDelivery->status == 404){
                return $this->respond(['success'=>false,'msg'=>$shiprocketDelivery->message]);
            }
            $etd_hours = $shiprocketDelivery->data->available_courier_companies[0]->etd_hours;
            $totalEtd = $seller_pincode->min_ship_hours + $etd_hours;
            $hoursToDays = ceil($totalEtd/24);
            $currentDate = date("d M Y");
            $addedDays = date('d M Y', strtotime($currentDate. ' + '."$hoursToDays".' days'));
            // dd($shiprocketDelivery->data->available_courier_companies[0]);
    
            // $etdHours = $shiprocketDelivery->data->available_courier_companies[0]->etd_hours;
    
            $etd = date("M d, Y", strtotime($shiprocketDelivery->data->available_courier_companies[0]->etd));
          
            return $this->respond(['success'=>true,'etd'=>$addedDays]); 
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }
   
}
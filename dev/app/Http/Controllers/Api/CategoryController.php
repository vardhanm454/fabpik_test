<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Models
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\ChildCategory;

// Repositories
use Facades\App\Repository\CategoriesRepo;

class CategoryController extends ApiController
{
    /**
     * Get category list.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function categoryList()
    {
        // $categories = Category::with('subcategories.childcategories')->active()->get();
        try{
            $categories = CategoriesRepo::allActive('id','asc');
            return $this->respond(['success'=>true,'categories'=>$categories]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function onlyCategories(){
        try{
            $categories = CategoriesRepo::onlyCategories('id','asc');
            return $this->respond($categories);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Get five category list.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fivecategory()
    {
        try{
            $categories = Category::active()->limit(5)->get();
            return $this->respond($categories);  
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }
    
    /**
     * Get category and subcategory list.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function categorySubCategoryList($id)
    {
        try{
            $catsubList = Category::with('subcategory')->where('id', $id)->get();
            return $this->respond($catsubList);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Get category and subcategory listfor mobile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function categorySubCategoryMobileList($cid)
    {
        try{
            $return = [];
            $return['category'] = Category::select('id','slug','title')->find($cid);
            $return['subCategories'] = Subcategory::select('subcategories.*')
                                                    ->join('view_subcat_product_count AS sc','sc.subcategory_id','=','subcategories.id')
                                                    ->where('subcategories.category_id', $cid)
                                                    ->where('sc.total_products','>',0)
                                                    ->active()
                                                    ->get();
            return $this->respond(['success'=>true,'categories'=>$return]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Get sub and child category list.
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function subChildCategoryList($sid)
    {
        try{
            $childCategories = ChildCategory::where('subcategory_id', $sid)->active()->get();
            return $this->respond($childCategories);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Get sub and child category list for mobile
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function subChildCategoryMobileList($sid)
    {
        try{
            $return = [];
            $return['category'] = Category::select('categories.slug')
            ->join('subcategories','subcategories.category_id','categories.id')->where('subcategories.id', $sid)->get();
            $return['subCategory'] = Subcategory::select('id','slug','title','category_id')->find($sid);
            $return['childCategories'] = ChildCategory::select('childcategories.*')
                                                        ->join('view_childcat_product_count as vcc','vcc.childcategory_id','=','childcategories.id')
                                                        ->where('subcategory_id', $sid)
                                                        ->active()
                                                        ->get();
            return $this->respond(['success'=>true,'categories'=>$return]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }  

    /**
     * Get category and sub categoryslug from chidid
     *
     * @return \Illuminate\Http\JsonResponse
    */
    public function catsubSlug($ccid)
    {
        try{
            $return = [];
            $return['category'] = Category::select('categories.slug')
                ->join('childcategories','childcategories.category_id','categories.id')->where('childcategories.id', $ccid)->get();
            $return['subCategory'] =  SubCategory::select('subcategories.slug')
                ->join('childcategories','childcategories.subcategory_id','subcategories.id')->where('childcategories.id', $ccid)->get();
            // $return['childCategories'] = ChildCategory::where('subcategory_id', $sid)->active()->get();
            return $this->respond($return);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }  
}
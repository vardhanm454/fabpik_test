<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Seshac\Shiprocket\Shiprocket;
use App\Http\Controllers\Api\CartController;
use App\Models\MobileAppSliders;

// Models
use App\Models\TermsAndCondtion;

class CommonController extends ApiController
{
	/**
	 * get get est delivery date from Shiprocket Api
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */

	public function checkPincode(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'pincode' => 'required|integer|digits:6',
            // 'weight' => 'required'
        ]);

        if($validator->fails()){
            return $this->respond($validator->errors()->toJson(), 400);
		}


		$token =  Shiprocket::getToken();

		$pincodeDetails = [
		    'pickup_postcode' => '500081',
		    'delivery_postcode' => $request->pincode,
		    'weight' => $request->weight?$request->weight:'0.1',
		    'cod' => $request->cod?$request->cod:1,
		];

		$response = Shiprocket::courier($token)->checkServiceability($pincodeDetails);
		$shiprocketDelivery = json_decode($response);
		// dd($shiprocketDelivery->data->available_courier_companies[0]);

		// $etdHours = $shiprocketDelivery->data->available_courier_companies[0]->etd_hours;

		$etd = date("M d, Y", strtotime($shiprocketDelivery->data->available_courier_companies[0]->etd ));
		return $this->respond($etd);
	}

	/**
	 * get all the policies based on NAME
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 * privacypolicy, cancellation-refund-policy,
	 * terms-and-conditions, return-policy, shipping-delivery-policy
	 */

	public function policy(Request $request, $name)
	{
		try{
			$policies = TermsAndCondtion::where('name', $name)->first();
			return $this->respond(['success'=>true,'policies'=>$policies]);
		}catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
	}

	##HOME MOBILE APP SLIDERS HERE
	public function mobileSliders(Request $request)
	{
		$page = ($request->page)?:'h';

		$imgBaseUrl = env('APP_URL').UPLOAD_PATH.DIRECTORY_SEPARATOR.'mobileAppSliders';

        $sliders = MobileAppSliders::selectRaw("page, panel, title, CONCAT('".$imgBaseUrl."/',image) as image, filter_params")
								->where('page',$page)
								->orderBy('id')
								->get();
		if(count($sliders) > 0)
			return response()->json([
				'success' => true,
				'message' => 'Home Mobile App Sliders',
				'data' => $sliders,
			]);

		return response()->json([
			'success' => false,
			'message' => 'No Data Found',
			'data' => []
		]);
	}

}
<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

// Models
use App\Models\User;

use App\Models\Cart;
use App\Models\Coupon;
use App\Models\Category;
use App\Models\ProductVariantOption;
use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Http\Controllers\Api\CartController;
use DB;
use App\Models\Order;
use Carbon\Carbon;
class CouponsController extends ApiController
{
    public function __construct(CartController $cartController) {
        $this->middleware('auth:api',['except' => ['getAllOffers']]);
        date_default_timezone_set('Asia/Kolkata');
        $this->cartController = $cartController;

    }

    public function getAllCoupons(){
        try{
            $currentDate = date('Y-m-d H:i:s');
            $coupons = Coupon::where('status','=', '1')
                               ->where('start_date', '<=', (string)date('Y-m-d H:i:s'))
                               ->where('expiry_date', '>=', (string)date('Y-m-d H:i:s'))
                               ->where('deleted_at','=',NULL)
                               ->get();
            // $coupons = Coupon::whereRaw("status = 1 AND start_date <= '$currentDate' AND expiry_date >= '$currentDate' AND ((seller_ids is null AND category_ids is null AND product_varient_ids is null) OR (json_contains(seller_ids,$seller_id) OR json_contains(category_ids,$primary_category) OR json_contains(product_varient_ids,$p_variant) ))")->get();
            $carts = Cart::select("carts.*","product_variants.mrp","p.primary_category")
                           ->join("products as p","p.id","=","carts.product_id")
                           ->join("product_variants","carts.product_variant_id",'=',"product_variants.id")
                           ->where('customer_id','=',auth('api')->user()->ref_id)
                           ->get();
    
    
            $couponsTemp2 = [];
            foreach ($carts as $cartItem) {
              foreach ($coupons as $coupon) {
                $id = $coupon->id;
                $seller_id = (string)$cartItem->seller_id;
                $primary_category = (string)$cartItem->primary_category;
                $p_variant = (string)$cartItem->product_variant_id;
                $mobile_number = auth('api')->user()->mobile;
                $cptemp = Coupon::selectRaw("coupons.*,coupon_groups.product_varient_ids")
                                  ->leftjoin("coupon_groups","coupon_groups.id","=","coupons.coupon_group_id")
                                  ->whereRaw("((coupons.seller_ids is null AND coupons.category_ids is null AND coupons.product_varient_ids is null AND coupons.mobile_numbers IS NULL AND coupon_groups.product_varient_ids is null) OR (json_contains(coupons.seller_ids,'\"$seller_id\"') OR json_contains(coupons.category_ids,'\"$primary_category\"') OR json_contains(coupons.product_varient_ids,'\"$p_variant\"') OR json_contains(coupons.mobile_numbers,'\"$mobile_number\"') OR json_contains(coupon_groups.product_varient_ids,'\"$p_variant\"'))) AND coupons.id = $id")
                                  ->first();
                if($cptemp){
                    $found = 0;
                    foreach ($couponsTemp2 as $coupon) {    
                        if($coupon->id == $cptemp->id){
                            $found = 1;
                        }
                    }
                    if($found == 0){
                        array_push($couponsTemp2, $cptemp);
                    } 
                  }
              }
            }
    
            $couponsTemp= [];
            foreach ($couponsTemp2 as $coupon) {
                if($coupon->dis_type == 'f' && $coupon->sub_type == 's'){
                    $totalSaving = 0;
                    $discountMRPBeforecoupon = 0;
                    foreach ($carts as $cartItem) {
                        $totalSaving += $cartItem->total_seller_discount;
                        $discountMRPBeforecoupon += $cartItem->total_price;
                    }
                    $totalSaving += $coupon->amount;
                    $discountMrpAfterCoupon = $discountMRPBeforecoupon - $coupon->amount;
                    $coupon->savings = $totalSaving;
                    $coupon->discountAfterCoupon = $discountMrpAfterCoupon;
                    array_push($couponsTemp,$coupon);
                }else if($coupon->dis_type == 'p' && $coupon->sub_type == 's'){
                    $totalSaving = 0;
                    $currentDiscountPercentage = 0;
                    $productsDiscounts = [];
                    $couponDiscountPercentage = 0;
                    $couponDiscountAmount = 0;
                    $totalDiscountOfaProductInPercentage = 0;
                    $totalDiscountOfaProductInRupees = 0;
                    foreach ($carts as $cartItem) {
                        $currentDiscountPercentage = ($cartItem->total_seller_discount * 100)/($cartItem->mrp * $cartItem->quantity);
                        $couponDiscountAmount = ($coupon->amount/100) * $cartItem->total_price;
                        $couponDiscountPercentage = ($couponDiscountAmount * 100) / $cartItem->total_price;
                        $totalDiscountOfaProductInPercentage = (($couponDiscountAmount + $cartItem->total_seller_discount) * 100)/($cartItem->mrp * $cartItem->quantity);
                        $totalDiscountOfaProductInRupees = $couponDiscountAmount + $cartItem->total_seller_discount;
                        $totalSaving += $totalDiscountOfaProductInRupees;
                        array_push($productsDiscounts,['product_variant_id'=>$cartItem->product_variant_id,
                                                        'currentDiscountPercentage'=>$currentDiscountPercentage,
                                                        'couponDiscountAmount'=>$couponDiscountAmount,
                                                        'totalDiscountOfaProductInPercentage'=>$totalDiscountOfaProductInPercentage,
                                                        'totalDiscountOfaProductInRupees'=>$totalDiscountOfaProductInRupees]);
                    }
                    $coupon->savings = $totalSaving;
                    $coupon->productsDiscounts = $productsDiscounts;
                    array_push($couponsTemp,$coupon);
                }else if($coupon->dis_type == 'p' && $coupon->sub_type == 'm'){
                    $totalSaving = 0;
                    $productsDiscounts = [];
                    $totalDiscountOfaProductInPercentage = 0;
                    $totalDiscountOfaProductInRupees = 0;
                    foreach ($carts as $cartItem) {
                        $couponDiscountAmount = ($coupon->amount/100) * ($cartItem->mrp * $cartItem->quantity);
                        if($couponDiscountAmount > $cartItem->total_seller_discount){
                            $totalSaving += $couponDiscountAmount;
                            array_push($productsDiscounts,['product_variant_id'=>$cartItem->product_variant_id,
                                                           'couponDiscountAmount'=>$couponDiscountAmount,
                                                           'sellerDiscountAmount'=>$cartItem->total_seller_discount]);
                        }else{
                            $totalSaving += $cartItem->total_seller_discount;
                            array_push($productsDiscounts,['product_variant_id'=>$cartItem->product_variant_id,
                                                           'couponDiscountAmount'=>0,
                                                           'sellerDiscountAmount'=>$cartItem->total_seller_discount]);
                        }
                    }
                    $coupon->savings = $totalSaving;
                    $coupon->productsDiscounts = $productsDiscounts;
                    array_push($couponsTemp,$coupon);
                }
            }
            return response(['success'=>true,'coupons'=>$couponsTemp]); 
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function applyCoupon(Request $request){ 
        try{
            $coupon = $request->coupon;
            $coupons_used = Order::selectRaw("COUNT(coupon_id) AS coupons_used")
                                    ->where('coupon_id','=',$coupon['id'])
                                    ->where('customer_id','=',auth('api')->user()->ref_id)
                                    ->first();
            if($coupon['max_usage'] == 'm' ){
               return response(['success'=>true,'finalCart'=>$this->applyCouponAuxiliaryFun($coupon)]);
            }else if($coupons_used->coupons_used == 'o' && $coupons_used->coupons_used == 0){
                return response(['success'=>true,'finalCart'=>$this->applyCouponAuxiliaryFun($coupon)]);
            }else{
                return response(['success'=>false,'msg'=>"Coupon is already used!"]); 
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function applyCouponAuxiliaryFun($coupon){
        $cart = $this->finalCart(); 
        if($cart['totalSellingPrice'] < $coupon["min_cart_amount"]){
            return ['success'=>false,'msg'=>"SubTotal(Sale Price) should be greater than ".$coupon["min_cart_amount"]."!"];
        }
        // return response(['coupon'=>$coupon]);
        if($coupon['dis_type'] == 'f' && $coupon['sub_type'] == 's'){
            if(count($cart['cart']) == 1 && $cart['cart'][0]['is_deal'] == 'y'){
               return ['success'=>false,'msg'=>"Coupon Cannot be Applied for Deal Product!"];
            }
            // $orderTotal = $cart['totalOrderAmount'];
            // $couponDiscount = 0;
            // $mrpDiscount = 0;
            // $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $orderTotal AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
            // $cart['totalSellingPrice'] = $coupon['discountAfterCoupon'];
            // $cart['totalOrderAmount'] = $coupon['discountAfterCoupon'] + $handling_charges[0]->charge_amount;
            // $cart['handling_charges'] = $handling_charges[0]->charge_amount;
            foreach($cart['cart'] as $key => $cartItem) { 
                
                // $mrpDiscount += $cartItem['total_seller_discount'];
                if($cartItem['is_deal'] == 'n'){
                    Cart::where('customer_id','=',auth('api')->user()->ref_id)
                        ->where('id','=',$cartItem['id'])
                        ->update(['coupon_id'=>$coupon['id'],'coupon_discount'=>$coupon['amount'],'coupon_applied'=>'y']);  //ask ranjan for coupon type
                    // $cart['cart'][$key]['coupon_applied'] = 'y'; 
                    // $couponDiscount += $coupon['amount'];
                    
                }else{
                    Cart::where('customer_id','=',auth('api')->user()->ref_id)
                        ->where('id','=',$cartItem['id'])
                        ->update(['coupon_id'=>$coupon['id'],'coupon_discount'=>$coupon['amount'],'coupon_applied'=>'n']);
                    // $cart['cart'][$key]['coupon_applied'] = 'n'; 
                }
            }
            // $cart['couponDiscount'] = $coupon['amount'];
            // $cart['mrpDiscount'] = $mrpDiscount;

            // return $cart;
            return $this->cartController->finalCart();
        }else if($coupon['dis_type'] == 'p' && $coupon['sub_type'] == 's'){
            if(count($cart['cart']) == 1 && $cart['cart'][0]['is_deal'] == 'y'){
                return ['success'=>false,'msg'=>"Coupon Cannot be Applied for Deal Product!"];
             }
            // $finalCartAfterDiscount = ['cart'=>[]];
            // $totalSellingPrice = 0;
            // $orderTotal = 0 ;
            // $totalMrp = 0;
            // $couponDiscount = 0;
            // $mrpDiscount = 0;
            foreach ($cart['cart'] as $cartItem) {
                $p_variant = $cartItem['product_variant_id'];
                $seller_id = $cartItem['seller_id'];
                $primary_category = $cartItem['primary_category'];
                $id = $coupon['id'];
                // $cptemp = Coupon::whereRaw("((seller_ids is null AND category_ids is null AND product_varient_ids is null) OR (json_contains(seller_ids,'\"$seller_id\"') OR json_contains(category_ids,'\"$primary_category\"') OR json_contains(product_varient_ids,'\"$p_variant\"'))) AND id = $id")->first();
                // $mrpDiscount += $cartItem['total_seller_discount'];
                $p_variant = (string)$cartItem->product_variant_id;
                $mobile_number = auth('api')->user()->mobile;
                $cptemp = Coupon::selectRaw("coupons.*,coupon_groups.product_varient_ids")
                                  ->leftjoin("coupon_groups","coupon_groups.id","=","coupons.coupon_group_id")
                                  ->whereRaw("((coupons.seller_ids is null AND coupons.category_ids is null AND coupons.product_varient_ids is null AND coupons.mobile_numbers IS NULL AND coupon_groups.product_varient_ids is null) OR (json_contains(coupons.seller_ids,'\"$seller_id\"') OR json_contains(coupons.category_ids,'\"$primary_category\"') OR json_contains(coupons.product_varient_ids,'\"$p_variant\"') OR json_contains(coupons.mobile_numbers,'\"$mobile_number\"') OR json_contains(coupon_groups.product_varient_ids,'\"$p_variant\"'))) AND coupons.id = $id")
                                  ->first();
                if($cptemp && $cartItem['is_deal'] == 'n'){
                    foreach ($coupon['productsDiscounts'] as $productDiscount) {
                        if($productDiscount['product_variant_id'] == $cartItem['product_variant_id']){
                            //  $cartItem['total_price'] -= $productDiscount['couponDiscountAmount'];
                            //  $cartItem['coupon_discount'] = $productDiscount['couponDiscountAmount'];
                             Cart::where('customer_id','=',auth('api')->user()->ref_id)
                                   ->where('id','=',$cartItem['id'])
                                   ->update(['coupon_id'=>$coupon['id'],'coupon_discount'=>$productDiscount['couponDiscountAmount'],'coupon_applied'=>'y']);
                            // $cartItem['coupon_applied']='y';
                            // $couponDiscount += $productDiscount['couponDiscountAmount'];
                            //  array_push($finalCartAfterDiscount['cart'],$cartItem);
                        }
                    }
                }else{
                    // $cartItem['coupon_discount'] = 0;
                    Cart::where('customer_id','=',auth('api')->user()->ref_id)
                                   ->where('id','=',$cartItem['id'])
                                   ->update(['coupon_id'=>$coupon['id'],'coupon_discount'=>0,'coupon_applied'=>'n']);
                    // $cartItem['coupon_applied']='n';
                    // array_push($finalCartAfterDiscount['cart'],$cartItem);
                }
                
                // $totalSellingPrice += $cartItem['total_price'];
                // $orderTotal = $totalSellingPrice;
                // $totalMrp += ($cartItem['mrp'] * $cartItem['quantity']);
            }
            // $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $orderTotal AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
            // $orderTotal = $orderTotal + $handling_charges[0]->charge_amount;
            // $finalCartAfterDiscount['totalSellingPrice'] = $totalSellingPrice;
            // $finalCartAfterDiscount['totalOrderAmount'] = $orderTotal;
            // $finalCartAfterDiscount['totalMrp'] = $totalMrp;
            // $finalCartAfterDiscount['mrpDiscount'] = $mrpDiscount;
            // $finalCartAfterDiscount['handling_charges'] = $handling_charges[0]->charge_amount;
            // $finalCartAfterDiscount['couponDiscount'] = $couponDiscount;
            // return $finalCartAfterDiscount;
             return $this->cartController->finalCart();
        }else if($coupon['dis_type'] == 'p' && $coupon['sub_type'] == 'm'){
            if(count($cart['cart']) == 1 && $cart['cart'][0]['is_deal'] == 'y'){
                return ['success'=>false,'msg'=>"Coupon Cannot be Applied for Deal Product!"];
            }
            // $finalCartAfterDiscount = ['cart'=>[]];
            // $totalSellingPrice = 0;
            // $orderTotal = 0 ;
            // $totalMrp = 0;
            // $mrpDiscount = 0;
            // $couponDiscount = 0;
            foreach ($cart['cart'] as $cartItem) {
                $p_variant = $cartItem['product_variant_id'];
                $seller_id = $cartItem['seller_id'];
                $primary_category = $cartItem['primary_category'];
                $id = $coupon['id'];
                // $cptemp = Coupon::whereRaw("((seller_ids is null AND category_ids is null AND product_varient_ids is null) OR (json_contains(seller_ids,'\"$seller_id\"') OR json_contains(category_ids,'\"$primary_category\"') OR json_contains(product_varient_ids,'\"$p_variant\"'))) AND id = $id")->first();
                $p_variant = (string)$cartItem->product_variant_id;
                $mobile_number = auth('api')->user()->mobile;
                $cptemp = Coupon::selectRaw("coupons.*,coupon_groups.product_varient_ids")
                                  ->leftjoin("coupon_groups","coupon_groups.id","=","coupons.coupon_group_id")
                                  ->whereRaw("((coupons.seller_ids is null AND coupons.category_ids is null AND coupons.product_varient_ids is null AND coupons.mobile_numbers IS NULL AND coupon_groups.product_varient_ids is null) OR (json_contains(coupons.seller_ids,'\"$seller_id\"') OR json_contains(coupons.category_ids,'\"$primary_category\"') OR json_contains(coupons.product_varient_ids,'\"$p_variant\"') OR json_contains(coupons.mobile_numbers,'\"$mobile_number\"') OR json_contains(coupon_groups.product_varient_ids,'\"$p_variant\"'))) AND coupons.id = $id")
                                  ->first();
                if($cptemp && $cartItem['is_deal'] == 'n'){
                    foreach ($coupon['productsDiscounts'] as $productDiscount) {    
                        if($productDiscount['product_variant_id'] == $cartItem['product_variant_id']){
                            if($productDiscount['couponDiscountAmount'] > $productDiscount['sellerDiscountAmount']){
                                // $couponDiscount +=  $productDiscount['couponDiscountAmount'];
                                // $cartItem['total_price'] = ($cartItem['mrp'] * $cartItem['quantity']) - $productDiscount['couponDiscountAmount'];
                                // $cartItem['coupon_discount'] = $productDiscount['couponDiscountAmount'];
                                Cart::where('customer_id','=',auth('api')->user()->ref_id)
                                   ->where('id','=',$cartItem['id'])
                                   ->update(['coupon_id'=>$coupon['id'],'coupon_discount'=>$productDiscount['couponDiscountAmount'],'coupon_applied'=>'y']);
                                // $cartItem['coupon_applied']='y';
                                // array_push($finalCartAfterDiscount['cart'],$cartItem);
                            }else{
                                // $mrpDiscount += $cartItem['total_seller_discount'];
                                // $couponDiscount +=  $productDiscount['couponDiscountAmount'];
                                // $cartItem['coupon_discount'] = $productDiscount['couponDiscountAmount'];
                                Cart::where('customer_id','=',auth('api')->user()->ref_id)
                                   ->where('id','=',$cartItem['id'])
                                   ->update(['coupon_id'=>$coupon['id'],'coupon_discount'=>$productDiscount['couponDiscountAmount'],'coupon_applied'=>'y']);
                                // $cartItem['coupon_applied']='y';
                                // array_push($finalCartAfterDiscount['cart'],$cartItem);
                            }
                        }
                    }
                }else{
                    // $mrpDiscount += $cartItem['total_seller_discount'];
                    // $cartItem['coupon_discount'] = 0;
                    Cart::where('customer_id','=',auth('api')->user()->ref_id)
                        ->where('id','=',$cartItem['id'])
                        ->update(['coupon_id'=>$coupon['id'],'coupon_discount'=>0,'coupon_applied'=>'n']);
                    // $cartItem['coupon_applied']='n';
                    // array_push($finalCartAfterDiscount['cart'],$cartItem);
                }
                
                // $totalSellingPrice += $cartItem['total_price'];
                // $orderTotal = $totalSellingPrice;
                // $totalMrp += ($cartItem['mrp'] * $cartItem['quantity']);
            }
            
            // if(in_array(auth('api')->user()->mobile,MOBILE_NUMBERS)){   
            //     $handlingCharge = 0;
            // }else{
            //     $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $orderTotal AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");;
            //     $orderTotal = $orderTotal + $handling_charges[0]->charge_amount;
            //     $handlingCharge = $handling_charges[0]->charge_amount;
            // }   
            // $finalCartAfterDiscount['totalSellingPrice'] = $totalSellingPrice;
            // $finalCartAfterDiscount['totalOrderAmount'] = $orderTotal;
            // $finalCartAfterDiscount['totalMrp'] = $totalMrp;
            // $finalCartAfterDiscount['couponDiscount'] = $couponDiscount;
            // $finalCartAfterDiscount['handling_charges'] = $handlingCharge;
            // $finalCartAfterDiscount['mrpDiscount'] = $mrpDiscount;

            // return $finalCartAfterDiscount;
            return $this->cartController->finalCart();
        }
    }

    public function removeCoupon(Request $request){
        $coupon = $request->coupon;
        Cart::where('carts.customer_id', auth('api')->user()->ref_id)
              ->update(['coupon_id'=>null,'coupon_discount'=>0,'coupon_applied'=>null]);
        $finalCart = $this->cartController->finalCart();
        return response($finalCart);

    }
    public function finalCart(){
        // $cart = Cart::select("products.images","products.tax","product_variants.mrp","product_variants.name","product_variants.description","product_variants.discount","product_variants.price","carts.*","seller_warehouses.name AS store_name","product_categories.category_id")
        //                 ->join("products","products.id",'=',"carts.product_id")
        //                 ->join("product_variants","carts.product_variant_id",'=',"product_variants.id")
        //                 ->join("seller_warehouses","seller_warehouses.seller_id",'=',"carts.seller_id")
        //                 ->join("product_categories","product_categories.product_id",'=','carts.product_id')
        //                 ->where('carts.customer_id', auth('api')->user()->customer_id)
        //                 ->get(); 
        $user_id = auth('api')->user()->ref_id;
        $cart = Cart::select("sellers.commission","product_variants.shipping_weight","product_variants.shipping_length","product_variants.shipping_breadth","product_variants.shipping_height","sellers.shipping_model","sellers.store_name","products.images","products.tax","product_variants.name","product_variants.shipping_weight","product_variants.sku","product_variants.min_ship_hours","product_variants.description","carts.*","categories.primary_attribute","categories.secondary_attribute","vm.images","vm.thumbnail","product_variants.slug","product_variants.unique_id","sellers.cod_enable","sellers.commission_type","sellers.commission_id","sellers.min_order_amount","sellers.convenience_fee","product_variants.price as seller_original_price","b.name as brand_name")
                        ->join("products","products.id",'=',"carts.product_id")
                        ->join("sellers","sellers.id",'=',"carts.seller_id")
                        ->join("product_variants","product_variants.id","carts.product_variant_id")
                        ->join('brands AS b','b.id','products.brand_id')
                        // ->join("seller_warehouses","seller_warehouses.seller_id",'=',"carts.seller_id")
                        ->join('categories','categories.id','=','products.primary_category')
                        ->join('product_variant_options AS pvo','pvo.product_variant_id','carts.product_variant_id')
                        ->leftjoin('variation_images as vm', function($join){
                            $join->on('vm.attribute_option_id', '=', 'pvo.attribute_option_id');
                            $join->on('vm.product_id','=','carts.product_id');
                        })
                        ->whereRaw("carts.customer_id = $user_id AND pvo.attribute_id = categories.primary_attribute AND pvo.deleted_at IS NULL AND product_variants.deleted_at IS NULL AND products.deleted_at IS NULL AND sellers.status = 1")
                        ->get();       
        $finalCart = [];
        $totalMrp = 0;
        $totalSellingPrice = 0;
        $totalOrderAmount = 0;
        foreach ($cart as $cartItem) {
            $attributes = [];
            $totalMrp += ($cartItem->mrp * $cartItem->quantity);
            $totalSellingPrice += $cartItem->total_price;
            $totalOrderAmount = $totalSellingPrice;
            $product_variant_options = ProductVariantOption::where('product_variant_id','=',$cartItem->product_variant_id)->get();

            foreach ($product_variant_options as $options) {
                if($options->attribute_id == $cartItem->primary_attribute){
                    $attribute = Attribute::find($options->attribute_id);
                    $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                        ->where('id','=',$options->attribute_option_id)
                                                        ->first();
                    if($attribute && $attributeOption){
                        $primaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'p');
                        array_push($attributes,$primaryAttribute);
                    }
                }else if($options->attribute_id == $cartItem->secondary_attribute ){
                    $attribute = Attribute::find($options->attribute_id);
                    $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                        ->where('id','=',$options->attribute_option_id)
                                                        ->first();
                    if($attribute && $attributeOption){
                        $secondaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'s');
                        array_push($attributes,$secondaryAttribute);
                    }
                }
            }
            $images = json_decode($cartItem->images);
            if($images){
                if($cartItem->thumbnail != null){
                    $cartItem->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($cartItem->thumbnail-1)]);
                }else{
                        $cartItem->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                }
            }else{
                $cartItem->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
            }
            $cartItem->attributes = $attributes;
            array_push($finalCart,$cartItem);
        }
        $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $totalOrderAmount AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
        $totalOrderAmount +=$handling_charges[0]->charge_amount;
        return ['cart'=>$finalCart,'totalMrp'=>$totalMrp,'totalSellingPrice'=>$totalSellingPrice,'totalOrderAmount'=>$totalOrderAmount,'handling_charges'=>$handling_charges[0]->charge_amount];
    }

    public function getAllOffers(){
        try{
            $currentDate = date('Y-m-d H:i:s');
            $coupons = Coupon::select("coupons.*", DB::raw("DATE_FORMAT(coupons.expiry_date, '%d-%b-%Y') as formatted_expiry"))
                               ->where('status','=', '1')
                               ->where('start_date', '<=', (string)date('Y-m-d H:i:s'))
                               ->where('expiry_date', '>=', (string)date('Y-m-d H:i:s'))
                               ->get();
            return response(['success'=>true,'offers'=>$coupons]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }


    public function getVirtualCart(Request $request){
        try{
            $coupon = Coupon::find($request->cp_id);
           
            $totalMrp = 0;
            $totalSellingPrice = 0;
            $totalOrderAmount = 0;
            $tempCart = [];
            $path = url(UPLOAD_PATH);
            $cart = Cart::selectRaw("carts.*,product_variants.name,CONCAT('$path','/',variation_images.thumbnail_img_name) AS thumbnail_image_path")
                              ->join("product_variants","product_variants.id","carts.product_variant_id")
                              ->leftjoin('view_product_images as variation_images','variation_images.product_variant_id','=','product_variants.id')
                              ->where("customer_id",'=',auth('api')->user()->ref_id)
                              ->get();
            if(count($cart) == 0){
                return response(['success'=>false,'msg'=>'No products in the Cart.']);
            }
             if($coupon->max_usage == 'o'){
                $cp_id = $request->cp_id;
                $user_id = auth('api')->user()->ref_id;
                $checkUserUsedCoupon = Order::whereRaw("coupon_id = $cp_id AND customer_id = $user_id")->exists();
                if($checkUserUsedCoupon){
                    return response(['success'=>false,'msg'=>'You have already used this coupon.']);
                }
            }
            if($coupon->dis_type == 'f' && $coupon->sub_type == 's'){
                foreach ($cart as $cartItem) {
                    $totalMrp += ($cartItem->mrp * $cartItem->quantity);
                    $totalSellingPrice += $cartItem->total_price;
                    $totalOrderAmount = $totalSellingPrice;
                    $cartItem->final_price = $cartItem->total_price;
                    $cartItem->final_discount = number_format($cartItem->total_seller_discount * 100 / ($cartItem->mrp * $cartItem->quantity),1);
                    array_push($tempCart,$cartItem);
                }
                $totalSellingPrice -= $coupon->amount;
                $totalOrderAmount = $totalSellingPrice;
                $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $totalOrderAmount AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
                $totalOrderAmount +=$handling_charges[0]->charge_amount;
                $handlingCharge = $handling_charges[0]->charge_amount;
                if($totalOrderAmount >= $coupon->min_cart_amount){
                    return response(['success'=>true,'cart'=>$tempCart,'totalMrp'=>$totalMrp,'totalSellingPrice'=>$totalSellingPrice,'totalOrderAmount'=>$totalOrderAmount,'handling_charges'=>$handlingCharge,'imgUrl'=>url(UPLOAD_PATH)]);
                }else{
                    $msg = "For this coupon to be applied minimum cart amount must be atlease ".$coupon->min_cart_amount;
                    return response(['success'=>false,'msg'=>$msg]);
                }
            }else if($coupon->dis_type == 'p' && $coupon->sub_type == 's'){
                foreach ($cart as $cartItem) {
                    $totalMrp += ($cartItem->mrp * $cartItem->quantity);
                    $totalSellingPrice += $cartItem->total_price;
                    $couponDiscountAmount = ($coupon->amount / 100) * $cartItem->total_price;
                    $totalSellingPrice -= $couponDiscountAmount;
                    $totalOrderAmount = $totalSellingPrice;
                    $cartItem->final_price = $cartItem->total_price - $couponDiscountAmount;
                    $cartItem->final_discount = number_format(($cartItem->total_seller_discount + $couponDiscountAmount) * 100 / ($cartItem->mrp * $cartItem->quantity),1);
                    array_push($tempCart,$cartItem);
                }
                $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $totalOrderAmount AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
                $totalOrderAmount +=$handling_charges[0]->charge_amount;
                $handlingCharge = $handling_charges[0]->charge_amount;
                if($totalOrderAmount >= $coupon->min_cart_amount){
                    return response(['success'=>true,'cart'=>$tempCart,'totalMrp'=>$totalMrp,'totalSellingPrice'=>$totalSellingPrice,'totalOrderAmount'=>$totalOrderAmount,'handling_charges'=>$handlingCharge,'imgUrl'=>url(UPLOAD_PATH)]);
                }else{
                    $msg = "For this coupon to be applied minimum cart amount must be atlease ".$coupon->min_cart_amount;
                    return response(['success'=>false,'msg'=>$msg]);
                }
            }else if($coupon->dis_type == 'p' && $coupon->sub_type == 'm'){
                foreach ($cart as $cartItem) {
                    $totalMrp += ($cartItem->mrp * $cartItem->quantity);
                    $couponDiscountAmount = ($coupon->amount / 100) * ($cartItem->mrp * $cartItem->quantity);
                    if($couponDiscountAmount > $cartItem->total_seller_discount){
                        $cartItem->final_price = ($cartItem->mrp*$cartItem->quantity) - $couponDiscountAmount;
                        $cartItem->final_discount = $coupon->amount;
                        $totalSellingPrice += $cartItem->final_price;
                        $totalOrderAmount = $totalSellingPrice;
                    }else{
                        $cartItem->final_price = $cartItem->total_price;
                        $cartItem->final_discount = number_format($cartItem->total_seller_discount * 100 / ($cartItem->mrp * $cartItem->quantity),1);
                        $totalSellingPrice += $cartItem->final_price;
                        $totalOrderAmount = $totalSellingPrice;
                    }
                    array_push($tempCart,$cartItem);
                }
                $handling_charges = DB::select("SELECT charge_amount FROM `handeling_charges` WHERE min_amount <= $totalOrderAmount AND deleted_at IS NULL ORDER BY min_amount DESC LIMIT 1");
                $totalOrderAmount +=$handling_charges[0]->charge_amount;
                $handlingCharge = $handling_charges[0]->charge_amount;
                if($totalOrderAmount >= $coupon->min_cart_amount){
                    return response(['success'=>true,'cart'=>$tempCart,'totalMrp'=>$totalMrp,'totalSellingPrice'=>$totalSellingPrice,'totalOrderAmount'=>$totalOrderAmount,'handling_charges'=>$handlingCharge,'imgUrl'=>url(UPLOAD_PATH)]);
                }else{
                    $msg = "For this coupon to be applied minimum cart amount must be atlease ".$coupon->min_cart_amount;
                    return response(['success'=>false,'msg'=>$msg]);
                }
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

    }
}

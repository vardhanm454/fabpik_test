<?php

namespace App\Http\Controllers\Api\Seller;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Seller;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\ShippingStatus;
use App\Models\ProductVariantOption;
use App\Models\OrderStatus;
use App\Models\OrderHistory;
use App\Models\Attribute;
// use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Models\PaymentStatus;
use App\Models\Review;

class OrdersController extends ApiController 
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function ordersGroupByStatus(){
        if(!$user = auth()->user()){
            return response(['success' => false, 'message'=>'Invalid request', 'error'=>'login error' ],422);
        }else{
            $order_details = Order::select("order_details.id AS order_detail_id","order_details.quantity AS quantity","order_details.child_order_id","product_variants.name","products.images","product_variants.product_id","product_variants.id AS product_variant_id","product_variants.sku AS product_variant_sku","order_details.order_id","order_details.created_at","order_details.updated_at","order_details.total","orders.payment_type","order_statuses.name AS order_status","shipping_statuses.name AS shipping_status","categories.primary_attribute","categories.secondary_attribute","vm.images","vm.thumbnail")
                                ->join('order_details','order_details.order_id','=','orders.id')
                                ->join('product_variants','product_variants.id','=','order_details.product_variant_id')
                                ->join('products','order_details.product_id','=','products.id')
                                ->join('order_statuses','order_details.order_status_id','=','order_statuses.id')
                                ->join('shipping_statuses','order_details.shipping_status_id','=','shipping_statuses.id')
                                ->join('categories','categories.id','=','products.primary_category')
                                ->join('view_product_images as vm', 'vm.product_variant_id','=','product_variants.id')
                                ->whereRaw("order_details.seller_id = $user->ref_id")
                                ->orderBy('order_details.updated_at', 'desc')
                                ->get();
            $new_orders = [];
            
            foreach ($order_details as $order) {
                $attributes = [];
                $product_variant_options = ProductVariantOption::where('product_variant_id','=',$order->product_variant_id)->get();
                foreach ($product_variant_options as $options) {
                   if($options->attribute_id == $order->primary_attribute){
                        $attribute = Attribute::find($options->attribute_id);
                        $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                            ->where('id','=',$options->attribute_option_id)
                                                            ->first();
                        if($attribute && $attributeOption){
                            $primaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'p');
                            array_push($attributes,$primaryAttribute);
                        }

                   }else if($options->attribute_id == $order->secondary_attribute){
                        $attribute = Attribute::find($options->attribute_id);
                        $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                            ->where('id','=',$options->attribute_option_id)
                                                            ->first();
                        if($attribute && $attributeOption){
                            $secondaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'s');
                            array_push($attributes,$secondaryAttribute);
                        }
                   }
                }
                $order->attributes = $attributes;
                $images = json_decode($order->images);
                if($images){
                    if($order->thumbnail != null){
                        $order->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($order->thumbnail-1)]);
                    }else{
                        $order->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                    }
                }else{
                    $order->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                }
                array_push($new_orders,$order);
            }
            $orders = array("orders"=>array(),"confirmedOrder"=>array(),"shippedOrder"=>array(),"deliveredOrder"=>array(),
                            "cancelledOrder"=>array(),"returnedOrder"=>array(), "returnApprovedbyAdmin"=>array(), "readyForPickupOrders"=>array(), "pickupScheduledOrders"=>array());    
            foreach ($new_orders as $value) {
                if($value->order_status == "Pending/Active Orders" && $value->shipping_status == "Pending"){
                    array_push($orders['orders'],$value);  
                }
                else if( ($value->order_status == "Order Confirmed" && ($value->shipping_status == "Pending")) ){
                    array_push($orders['confirmedOrder'],$value);
                }else if( $value->shipping_status == "Ready For Pickup" || ($value->order_status == "Generate AWB" && $value->shipping_status == "Pickup Scheduled")){
                    array_push($orders['readyForPickupOrders'],$value);
                }else if(($value->order_status == "Delivered" || $value->order_status == "Completed") && $value->shipping_status == "Delivered"){
                    array_push($orders['deliveredOrder'],$value);
                }else if($value->order_status == "Cancelled" || $value->order_status == "Rejected by Seller"){
                    array_push($orders['cancelledOrder'],$value);
                }else if($value->shipping_status == "Shipped" || $value->shipping_status == "Undelivered Product Return Confirmed"){
                    array_push($orders['shippedOrder'],$value);
                }else if($value->shipping_status == "Delivery Unsuccessful"){
                    array_push($orders['shippedOrder'],$value);
                }else if($value->shipping_status == "Product Return confirmed" || $value->shipping_status == "Undelivered Product Returned"){
                    array_push($orders['returnedOrder'],$value);
                }else if($value->order_status == "Return Approved by Admin" || $value->shipping_status == "Return Pickup Initiated"){
                    array_push($orders['returnApprovedbyAdmin'],$value);
                }
            }
            return response(['success' => true,'data'=>$orders]);
        }
       
    }

    public function getOrderByStatus(Request $request){
        $validator = Validator::make($request->all(), [
            'orderStatusFlag' => 'required'
        ]);
        if($validator->fails()){
            return response(['success' => false,'errors'=>$validator->messages()->toArray()], 422);  
        }else{
            $user = auth()->user();
           
            $order_details = Order::select("order_details.id AS order_detail_id","order_details.child_order_id","product_variants.name","products.images","product_variants.product_id","product_variants.id AS product_variant_id","order_details.order_id","order_details.created_at","order_details.updated_at","order_details.total","orders.payment_type","order_statuses.name AS order_status","shipping_statuses.name AS shipping_status","categories.primary_attribute","categories.secondary_attribute","vm.images","vm.thumbnail")
                                     ->join('order_details','order_details.order_id','=','orders.id')
                                     ->join('product_variants','product_variants.id','=','order_details.product_variant_id')
                                     ->join('products','order_details.product_id','=','products.id')
                                     ->join('order_statuses','order_details.order_status_id','=','order_statuses.id')
                                     ->join('shipping_statuses','order_details.shipping_status_id','=','shipping_statuses.id')
                                     ->join('categories','categories.id','=','products.primary_category')
                                     ->join('product_variant_options AS pvo','pvo.product_variant_id','order_details.product_variant_id')
                                    //  ->leftjoin('variation_images as vm', function($join){
                                    //      $join->on('vm.attribute_option_id', '=', 'pvo.attribute_option_id');
                                    //      $join->on('vm.product_id','=','order_details.product_id');
                                    //  })
                                    ->join('view_product_images as vm', 'vm.product_variant_id','=','product_variants.id')
                                     ->whereRaw("order_details.seller_id = $user->ref_id AND pvo.attribute_id = categories.primary_attribute")
                                     ->orderBy('updated_at', 'DESC')
                                     ->get();
            $new_orders = [];
            foreach ($order_details as $order) {
                $attributes = [];
                $product_variant_options = ProductVariantOption::where('product_variant_id','=',$order->product_variant_id)->get();
                foreach ($product_variant_options as $options) {
                   if($options->attribute_id == $order->primary_attribute){
                        $attribute = Attribute::find($options->attribute_id);
                        $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                            ->where('id','=',$options->attribute_option_id)
                                                            ->first();
                        if($attribute && $attributeOption){
                            $primaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'p');
                            array_push($attributes,$primaryAttribute);
                        }

                   }else if($options->attribute_id == $order->secondary_attribute){
                        $attribute = Attribute::find($options->attribute_id);
                        $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                            ->where('id','=',$options->attribute_option_id)
                                                            ->first();
                        if($attribute && $attributeOption){
                            $secondaryAttribute = array("attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'s');
                            array_push($attributes,$secondaryAttribute);
                        }
                   }
                }
                $order->attributes = $attributes;
                $images = json_decode($order->images);
                if($images){
                    if($order->thumbnail != null){
                        $order->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($order->thumbnail-1)]);
                    }else{
                        $order->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                    }
                }else{
                    $order->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                }
                array_push($new_orders,$order);
            }
            $orders = [];
            //based on orderstatus
            switch ($request->orderStatusFlag) {
                case 0:
                    foreach ($new_orders as $value) {
                        if($value->order_status == "Pending/Active Orders" && $value->shipping_status == "Pending") {
                            array_push($orders,$value);
                        }
                    }
                    break;
                case 1:
                    foreach ($new_orders as $value) {
                        if($value->order_status == "Order Confirmed" && ($value->shipping_status == "Pending" || $value->shipping_status == "Ready For Pickup" )){
                            array_push($orders,$value);
                        }
                    }
                    break;
                case 2:
                    foreach ($new_orders as $value) {
                        if($value->shipping_status == "Shipped"){
                            array_push($orders,$value);
                        }
                    }
                    break;
                case 3:
                    foreach ($new_orders as $value) {
                        if($value->order_status == "Delivered" && $value->shipping_status == "Delivered"){
                            array_push($orders,$value);
                        }
                    }
                    break;
                case 4:
                    foreach ($new_orders as $value) {
                        if($value->order_status == "Cancelled" || $value->order_status == "Rejected by Seller" ){
                            array_push($orders,$value);
                        }
                    }
                    break;
                case 5:
                    foreach ($new_orders as $value) {
                        if($value->shipping_status == "Product Return confirmed"){
                            array_push($orders,$value);
                        }
                    }
                    break;
                default:
                    return response(['success' => false,'msg'=>'Order Status Not Found!']);
                    break;
            }
            return response(['success' => true,'orders'=>$orders]); 
        }   
    }

    public function updateOrderStatus(Request $request){
        $validator = Validator::make($request->all(), [
            'orderDetailId' => 'required',
            'setOrderStatusFlag' => 'required',
        ]);
        if($validator->fails()){
            return response(['success' => false,'errors'=>$validator->messages()->toArray()], 422);  
        }else{
            $user = auth()->user();
            
            if($request->setOrderStatusFlag == 'pickup'){
                // When seller change the shipping status to ready to pickup
                __changeSellerShippingStatus('2',[$request->orderDetailId]);
            }
            else if($request->setOrderStatusFlag == 'cancel'){
                 // When user cancel the order
                __changeStatus('3',[$request->orderDetailId]);
            }else if($request->setOrderStatusFlag == 'confirm'){
                //when seller confirm the order
                __changeStatus('4',[$request->orderDetailId]);
            }else if($request->setOrderStatusFlag == 'shipped'){
                //when seller change the shipping status to shipped
                __changeSellerShippingStatus('4',[$request->orderDetailId]);
            }
            else if($request->setOrderStatusFlag == 'delivered'){
                //when seller change the shipping status to delivered
                __changeSellerShippingStatus('5',[$request->orderDetailId]);
            }
            else if($request->setOrderStatusFlag == 'deliveredunsuccessful'){
                //when seller change the shipping status to delivery unsucess
                __changeSellerShippingStatus('6',[$request->orderDetailId]);
            }
            else if($request->setOrderStatusFlag == 'undeliveredproductreturnconfirmed'){
                //when seller change the shipping status to undelivery product returnedd confirmed  
                __changeSellerShippingStatus('7',[$request->orderDetailId]);
            }
            else if($request->setOrderStatusFlag == 'returnpickupinitiated'){
                //when seller change the shipping status to return pickup intiated
                __changeSellerShippingStatus('10',[$request->orderDetailId]);
            }
            else if($request->setOrderStatusFlag == 'productreturnconfirmed'){
                //when seller change the shipping status to product return confirmed
                __changeSellerShippingStatus('11',[$request->orderDetailId]);
            }
            return response(['success'=>true]);
        }
    }

    /**
     * Get the order full details
     */
    public function getOrderDetails($orderId){
        if (!$user = auth()->user()) {
            return response(['success' => false, 'message'=>'Invalid request', 'error'=>true ], 422);
        } else {
            $paymentStatus = PaymentStatus::get();
            $orderStatus = OrderStatus::get();
            $shippingStatus = ShippingStatus::get();
            $orderDetails = OrderDetail::with('order','seller', 'productVarient', 'orderHistory')->where('id', $orderId)->first();
            $orderHistories = orderHistory::where('order_detail_id', $orderId)->whereNotNull('order_status_id')->get();
            $attributeOptions = __getVariantOptionValues($orderDetails->product_variant_id);
            $imageDetails = url('/').'/'.UPLOAD_PATH.'/'.__getVariantImages($orderDetails->product_variant_id);

            return response(['success' => true, 'paymentStatus'=>$paymentStatus, 'orderStatus'=>$orderStatus, 'shippingStatus'=>$shippingStatus, 'orderDetails'=>$orderDetails, 'orderHistories'=>$orderHistories, 'attributeOptions'=>$attributeOptions, 'imageDetails'=>$imageDetails, 'message'=>'', 'error'=>false], 200); 

        }
    }

    public function reviews(Request $request)
    {
        $iPage = intval($request->page);

        $criteria = (object)[
            'length' => 10,
            'fstatus' => (!is_null($request->fstatus))?$request->fstatus:null,
            'fcustomers' => (!is_null($request->fcustomers))?$request->fcustomers:null,
            'ffromdate' => (!is_null($request->ffromdate))?date('Y-m-d', strtotime($request->ffromdate)):null,
            'ftodate' => (!is_null($request->ftodate))?date('Y-m-d', strtotime($request->ftodate)):null,
            'fprovariants' => (!is_null($request->fprovariants))?$request->fprovariants:null,
            'fseller' => auth('api')->user()->ref_id
        ];

        $reviews = Review::getAjaxListData($criteria, $iPage, 'id', 'desc');

        $featuredList = [
            ["danger" => "No"],
            ["success" => "Yes"]
        ];

        $statusList = [
            ["danger" => "Inactive"],
            ["success" => "Active"]
        ];
        
        $records = [];
        $records['total_reviews'] = $reviews->total();
        $records['overall_ratings'] = ($records['total_reviews'] > 0)?(Review::getRatingSum(auth('api')->user()->ref_id)/$records['total_reviews']):0;
        foreach ($reviews as $review) {
            $status = $statusList[$review->status];
            $records['reviews'][] = [
                'child_order_id'=>$review->child_order_id,
                'customer_name'=>$review->customer_name,
                'customer_email'=>$review->customer_email,
                'name'=>$review->name,
                'review_title'=>$review->review_title,
                'review_comment'=>$review->review_comment,
                'rating'=>$review->rating,
                'reviewed_at'=>date("d-m-Y", strtotime($review->updated_at)),
                'published'=>$review->published == 1 ? 'Yes' : 'No',
            ];
        }

        return response()->json($records);
    }

}
<?php

namespace App\Http\Controllers\Api\Seller;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use DB;

use App\Models\User;
use App\Models\Seller;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\ProductVariantOption;
use App\Models\OrderStatus;
use App\Models\Attribute;
use App\Models\AttributeOption;

class DashboardController extends ApiController 
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function ordersStats()
    {
        $data['ordersToday'] = OrderDetail::where('seller_id', auth()->user()->ref_id)
                                            ->whereIn('shipping_status_id', [1,2,3,4,5])
                                            ->whereIn('order_status_id', [1,4,8,9,10])
                                            ->whereRaw("DATE(`created_at`) = CURDATE()")
                                            ->count();

        $data['ordersToShip'] = OrderDetail::where('seller_id', auth()->user()->ref_id)
                                            ->whereIn('shipping_status_id', [2,3])
                                            ->whereIn('order_status_id', [1,4,8,9,10])
                                            ->count();

        $data['totalSales'] = OrderDetail::where('seller_id', auth()->user()->ref_id)
                                        ->whereIn('shipping_status_id', [2,3,4,5])
                                        ->whereIn('order_status_id', [1,4,8,9,10])
                                        // ->whereRaw("DATE(`created_at`) = CURDATE()")
                                        ->value(DB::raw("SUM(`price`*`quantity`)"));

        $data['totalSalesToday'] = OrderDetail::where('seller_id', auth()->user()->ref_id)
                                                ->whereIn('shipping_status_id', [2,3,4,5])
                                                ->whereIn('order_status_id', [1,4,8,9,10])
                                                ->whereRaw("DATE(`created_at`) = CURDATE()")
                                                ->value(DB::raw("SUM(`price`*`quantity`)"));

        // get last 7 days total orders and sales
        $data['ordersStats'] = [];
        $data['salesStats'] = [];
        for ($i = 6; $i >= 0; $i--) {
            $totalOrders = 0;
            $totalSale = 0;
            $day = date('d-m-Y', strtotime("-$i days"));

            $order = DB::select("SELECT DATE_FORMAT(created_at, '%d-%m-%Y') AS day, count(*) as total FROM order_details WHERE DATE_FORMAT(created_at, '%d-%m-%Y')='".$day."' AND seller_id = ".auth('api')->user()->ref_id." GROUP BY DATE_FORMAT(created_at, '%d-%m-%Y')");
            if(count($order) >= 1) {
                $totalOrders = $order[0]->total;
            }

            $data['ordersStats'][] = [
                'day' => $day,
                'orders' => $totalOrders,
            ];

            // echo "SELECT DATE_FORMAT(created_at, '%m-%Y') AS month, SUM(pay_amount) as total FROM orders WHERE DATE_FORMAT(created_at, '%m-%Y')='".$month."' AND status <> 7 GROUP BY DATE_FORMAT(created_at, '%m-%Y')"; die;
            $sale = DB::select("SELECT DATE_FORMAT(created_at, '%d-%m-%Y') AS day, SUM(final_amount_to_pay_seller) as total FROM order_details WHERE DATE_FORMAT(created_at, '%d-%m-%Y')='".$day."' AND order_status_id IN(4,8,9,10) AND shipping_status_id IN(2,3,4,5) AND seller_id = ".auth('api')->user()->ref_id." GROUP BY DATE_FORMAT(created_at, '%d-%m-%Y')");
            if(count($sale) >= 1) {
                $totalSale = $sale[0]->total;
            }

            $data['salesStats'][] = [
                'day' => $day,
                'sales' => $totalSale,
            ];
        }

        return $this->respond($data);
    }

    public function orderDetails(Request $request) 
    {
        $order_details = Order::select("order_details.id AS order_detail_id","order_details.invoice_no","product_variants.name","products.images","product_variants.product_id","product_variants.id AS product_variant_id","order_details.order_id","order_details.created_at","order_details.updated_at","order_details.total","orders.payment_type","order_statuses.name AS order_status","shipping_statuses.name AS shipping_status","categories.primary_attribute","categories.secondary_attribute")
                            ->join('order_details','order_details.order_id','=','orders.id')
                            ->join('product_variants','product_variants.id','=','order_details.product_variant_id')
                            ->join('products','order_details.product_id','=','products.id')
                            ->join('order_statuses','order_details.order_status_id','=','order_statuses.id')
                            ->join('shipping_statuses','order_details.shipping_status_id','=','shipping_statuses.id')
                            ->join('categories','categories.id','=','products.primary_category')
                            ->where('order_details.seller_id','=',auth('api')->user()->ref_id)
                            ->get();
            $new_orders = [];
            
            foreach ($order_details as $order) {
                $attributes = [];
                $product_variant_options = ProductVariantOption::where('product_variant_id','=',$order->product_variant_id)->get();
                foreach ($product_variant_options as $options) {
                   if($options->attribute_id == $order->primary_attribute){
                        $attribute = Attribute::find($options->attribute_id);
                        $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                            ->where('id','=',$options->attribute_option_id)
                                            ->first();
                        if($attribute && $attributeOption){
                            $primaryAttribute = ["attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'p'];
                            array_push($attributes,$primaryAttribute);
                        }

                   }else if($options->attribute_id == $order->secondary_attribute){
                        $attribute = Attribute::find($options->attribute_id);
                        $attributeOption = AttributeOption::where('attribute_id','=',$options->attribute_id)
                                                ->where('id','=',$options->attribute_option_id)
                                                ->first();
                        if($attribute && $attributeOption){
                            $secondaryAttribute = ["attribute_name"=>$attribute->display_name,"attribute_option"=>$attributeOption->option_name,'type'=>'s'];
                            array_push($attributes,$secondaryAttribute);
                        }
                   }
                }
                $order->attributes = $attributes;
                array_push($new_orders,$order);
            }
                                      
        return $this->respond(['success' => true, 'data' => $new_orders]);
    }

    public function settlementStats()
    {
        $totalAmount = OrderDetail::where('seller_id', auth()->user()->ref_id)
                                    ->whereIn('shipping_status_id', [2,3,4,5])
                                    ->whereIn('order_status_id', [1,4,8,9,10])
                                    ->value(DB::raw("SUM(`price`*`quantity`)"));

        $totalPyoutAmount = OrderDetail::where('seller_id', auth()->user()->ref_id)
                                        ->where('payout_status', 1)
                                        ->whereIn('shipping_status_id', [2,3,4,5])
                                        ->whereIn('order_status_id', [1,4,8,9,10])
                                        ->sum('final_amount_to_pay_seller');
        
        $pendingpayoutAmount = OrderDetail::where('seller_id', auth()->user()->ref_id)
                                            ->whereIn('shipping_status_id', [2,3,4,5])
                                            ->whereIn('order_status_id', [1,4,8,9,10])
                                            ->sum('final_amount_to_pay_seller');

        $totalPendingAmount = $pendingpayoutAmount - $totalPyoutAmount;

        return $this->respond(['totalAmount'=>$totalAmount, 'totalPyoutAmount'=>$totalPyoutAmount, 'totalPendingAmount'=>$totalPendingAmount]);
    }

    public function settlementLogs(Request $request)
    {
        $page = isset($request->page)?$request->page:1;

        $payouts = OrderDetail::selectRaw("name, quantity, final_amount_to_pay_seller, DATE_FORMAT(`payout_date_from_admin` ,'%d-%m-%Y') as payout_date_from_admin")
                            ->where('seller_id', auth('api')->user()->ref_id)
                            ->where('payout_status', 1)
                            // ->whereIn('shipping_status_id', [2,3,4,5])
                            // ->whereIn('order_status_id', [4,8,9,10])
                            ->paginate(10, ['name','quantity', 'final_amount_to_pay_seller','payout_date_from_admin'], 'page', $page);

        return $this->respond(['payouts'=>$payouts]);

    }
}
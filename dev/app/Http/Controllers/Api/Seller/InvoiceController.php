<?php

namespace App\Http\Controllers\Api\Seller;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Seller;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\ProductVariantOption;
use App\Models\productVarient;
use App\Models\Attribute;
use DB;
use PDF;

class InvoiceController extends ApiController 
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function userInvoice(){
        if(!auth('api')->user()->ref_id){
            return response(['success' => false],401);
        }else{
            $orders = OrderDetail::select("order_details.created_at AS date","order_details.payout_status","order_details.payout_date_from_admin","order_details.child_order_id","order_details.order_id","product_variants.name AS product_name")
                                    ->join('product_variants','product_variants.id','=','order_details.product_variant_id')
                                    ->where('order_details.seller_id','=',auth('api')->user()->ref_id)
                                    ->get();
            $user_invoices = array();

            foreach ($orders as $order) {
                $order_history = DB::table('order_histories')
                                     ->where('order_detail_id','=',$order->child_order_id)
                                     ->where('order_id','=',$order->order_id)
                                     ->first();
                if($order_history){
                    if($order_history->order_status_id == 8){
                        $order->DeliveredDate = $order_history->created_at;
                        $order->amountCleared = true;
                    }
                    array_push($user_invoices,$order);
                }
                
            }
            return response(['userInvoices'=>$user_invoices]);


        }
    }

    public function fabpikInvoice(){
        if(!$user = auth()->user()){
            return response(['success' => false],401);
        }else{
            $orders = Order::select("orders.created_at AS date","orders.parent_order_id","order_details.payout_status","order_details.payout_date_from_admin","order_details.child_order_id","order_details.tax","order_details.total","order_details.order_id","product_variants.name AS product_name")
                                    ->join('order_details','order_details.order_id','=','orders.id')
                                    ->join('product_variants','product_variants.id','=','order_details.product_variant_id')
                                    ->where('order_details.seller_id','=',auth('api')->user()->ref_id)
                                    ->get();
            $fabpik_invoices = array();

            foreach ($orders as $order) {
                $invoce_store = 0;
                $order_history = DB::table('order_histories')
                                     ->where('order_detail_id','=',$order->child_order_id)
                                     ->where('order_id','=',$order->order_id)
                                     ->first();
                if($order_history){
                    if($order_history->order_status_id == 13){
                        $order->DeliveredDate = $order_history->created_at;
                    }
                    array_push($fabpik_invoices,$order);
                }
                
            }                        
            return response(['fabpikInvoices'=>$fabpik_invoices]);
        }
    }

    public function userAndFabpikInvoices(Request $request)
    {
        if(auth('api')->user()->ref_id == '' || auth('api')->user()->ref_id == null){ 
            return response(['success' => false, 'message'=>'Invalid Details', 'error'=>'Invalid Details'],401);
        }

        //fabpik invoices
        $ordersForFabpikInvoice = Order::select("orders.created_at AS date","order_details.invoice_no","order_details.id", "order_details.delivered_date","order_details.payout_status","order_details.payout_date_from_admin","orders.parent_order_id","order_details.child_order_id","order_details.tax","order_details.cgst","order_details.sgst","order_details.total","order_details.order_id","product_variants.name AS product_name")
                ->join('order_details','order_details.order_id','=','orders.id')
                ->join('product_variants','product_variants.id','=','order_details.product_variant_id')
                ->where('order_details.seller_id','=',auth('api')->user()->ref_id)
                ->whereIn('order_details.order_status_id', [8,9])
                ->where('order_details.shipping_status_id', 5)
                ->where('order_details.payment_status_id', 1)
                ->where('order_details.payout_status', 1);

        if($request->search_type=='fabpik') {
            if(isset($request->inv_no))  $ordersForFabpikInvoice = $ordersForFabpikInvoice->where('order_details.invoice_no', 'LIKE', '%'.$request->inv_no.'%');
            if(isset($request->fdt) && isset($request->tdt))  $ordersForFabpikInvoice = $ordersForFabpikInvoice->whereRaw("order_details.created_at BETWEEN '".date('Y-m-d 00:00:00', strtotime($request->fdt))."' AND '".date('Y-m-d 23:59:59', strtotime($request->tdt))."'");
        }
        $ordersForFabpikInvoice = $ordersForFabpikInvoice->get();
        // $fabpik_invoices = array();

        // foreach ($ordersForFabpikInvoice as $order) {
        //     $invoce_store = 0;
        //     $order_history_fabpik_invoice = DB::table('order_histories')
        //                          ->where('order_detail_id','=',$order->child_order_id)
        //                          ->where('order_id','=',$order->order_id)                                     
        //                          ->first();
        //     if($order_history_fabpik_invoice){
        //         if($order_history_fabpik_invoice->order_status_id == 13){
        //             $order->DeliveredDate = $order_history_fabpik_invoice->created_at;
        //         }
        //         array_push($fabpik_invoices,$order);
        //     }

        // }   

        //user invoices                      
        $ordersForUserInvoice = OrderDetail::select("order_details.created_at AS date","order_details.id","order_details.invoice_no","order_details.delivered_date","order_details.payout_status","order_details.payout_status","order_details.payout_date_from_admin","order_details.cgst","order_details.sgst","order_details.child_order_id","order_details.order_id","product_variants.name AS product_name")
                ->join('product_variants','product_variants.id','=','order_details.product_variant_id')
                ->where('order_details.seller_id','=',auth('api')->user()->ref_id)
                ->whereIn('order_details.order_status_id', [8,9])
                ->where('order_details.shipping_status_id', 5);
        
        if($request->search_type=='user') {
            if(isset($request->inv_no))  $ordersForUserInvoice = $ordersForUserInvoice->where('order_details.invoice_no', 'LIKE', '%'.$request->inv_no.'%');
            if(isset($request->fdt) && isset($request->tdt))  $ordersForUserInvoice = $ordersForUserInvoice->whereRaw("order_details.created_at BETWEEN '".date('Y-m-d 00:00:00', strtotime($request->fdt))."' AND '".date('Y-m-d 23:59:59', strtotime($request->tdt))."'");
        }
        $ordersForUserInvoice = $ordersForUserInvoice->get();
        // $user_invoices = array();

        // foreach ($ordersForUserInvoice as $order) {
        //     $invoce_store = 0;
        //     $order_history_user_invoice = DB::table('order_histories')
        //                          ->where('order_detail_id','=',$order->child_order_id)
        //                          ->where('order_id','=',$order->order_id)
        //                          ->first();
        //     if($order_history_user_invoice){
        //         if($order_history_user_invoice->order_status_id == 13){
        //             $order->DeliveredDate = $order_history_user_invoice->created_at;
        //             $order->amountCleared = true;
        //         }
        //         array_push($user_invoices,$order);
        //     }

        // }
        return response(['success'=>true,'data'=>array('userInvoices'=>$ordersForUserInvoice,'fabpik_invoices'=>$ordersForFabpikInvoice)]);
    }

    /**
     ** Seller(Payment and Commission) Invoice Download
    */
    public function sellerInvoice($child_order_id)
    {
        if(auth('api')->user()->ref_id == '' || auth('api')->user()->ref_id == null){ 
            return response(['success' => false, 'message'=>'Invalid Details', 'error'=>'Invalid Details'],401);
        }else{
            $this->data['orderDetails'] = OrderDetail::with('order', 'seller', 'productVarient', 'orderHistory')->where('id', $child_order_id)->first();
            $productVarientOptions = ProductVariantOption::where('product_variant_id', $this->data['orderDetails']->product_variant_id)->get();
            
            $attributes = [];
            foreach ($productVarientOptions as $i=>$options) {
                $getAttributes = Attribute::with(['attributeOptions' => function ($q) use ($options) {
                    $q->where('id', '=', $options->attribute_option_id);
                }])->where('id', $options->attribute_id)->first();

                $attributes[$i]['atrributeName'] = $getAttributes->display_name;
                $attributes[$i]['atrributeValue'] = $getAttributes->attributeOptions[0]->option_name;
            }
            $this->data['attributeOptions'] = $attributes;

            $pdf = PDF::loadView('seller.payments.sellerinvoicedownload', $this->data);

            $path = 'uploads/sellerinvoice/';
            if (!file_exists($path)) {
                // path does not exist
                mkdir($path, 0777);
                chmod($path, 0777);
            }

            $filename = $path.'seller_invoice_'.$this->data['orderDetails']->child_order_id.'.pdf';
            if ($pdf->save($filename)) {
                return response()->json(['success'=>'true', 'url'=>url($filename)]);
            } else {
                return response()->json(['success'=>'false', 'url'=>'']);
            }
        }

    }

    /**
     ** Customer Invoice Download
    */

    public function customerInvoice($child_order_id)
    {
        if(auth('api')->user()->ref_id == '' || auth('api')->user()->ref_id == null){ 
            return response(['success' => false, 'message'=>'Invalid Details', 'error'=>'Invalid Details'],401);
        }else{
            $this->data['orderDetails'] = OrderDetail::with('order', 'seller', 'productVarient', 'orderHistory')->where('id', $child_order_id)->first();
            $productVarientOptions = ProductVariantOption::where('product_variant_id', $this->data['orderDetails']->product_variant_id)->get();
            $attributes = [];
        
            foreach ($productVarientOptions as $i=>$options) {
                $getAttributes = Attribute::with(['attributeOptions' => function ($q) use ($options) {
                    $q->where('id', '=', $options->attribute_option_id);
                }])->where('id', $options->attribute_id)->first();
                $attributes[$i]['atrributeName'] = $getAttributes->display_name;
                $attributes[$i]['atrributeValue'] = $getAttributes->attributeOptions[0]->option_name;
            }
         
            $this->data['attributeOptions'] = $attributes;
        
            // $pdf = PDF::loadView('customer.orders.customerinvoice', $this->data);
            $pdf = PDF::loadView('invoices.customerinvoice', $this->data);
            $path = UPLOAD_PATH.'/'.'customerinvoice/';
            // $path = 'uploads/customerinvoice/';
            if (!file_exists($path)) {
                // path does not exist
                mkdir($path, 0777);
                chmod($path, 0777);
            }

            $filename = $path.$this->data['orderDetails']->child_order_id.'.pdf';
            if ($pdf->save($filename)) {
                return response()->json(['success'=>'true', 'url'=>url($filename)]);
            } else {
                return response()->json(['success'=>'false', 'url'=>'']);
            }
        }
    }

}
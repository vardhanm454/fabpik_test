<?php

namespace App\Http\Controllers\Api\Seller;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Seller;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductVariant;

class SellerShoplandingController extends ApiController 
{
   
    public function FeturedNewCollection(Request $request){
        $seller = Seller::where('id',$request->sellerid)->orwhere('url_slug',$request->sellerslug)->where('status',1)->get();

        $featured =  Product::select('products.name','products.sell_price','products.images','products.thumbnail')
            ->join('sellers','sellers.id','products.seller_id')
            ->where('products.seller_id',$request->sellerid)
            ->orwhere('sellers.url_slug',$request->sellerslug)
            ->where('products.seller_featured',1)
            ->orderBy('products.created_at', 'DESC')->limit(3)->get();
            $productdata = Product::getProductimagespath($featured);
        
        $new_collection = ProductVariant::selectRaw("pvo.product_variant_id, product_variants.unique_id, product_variants.name, product_variants.slug, product_variants.sku, product_variants.stock, product_variants.mrp, product_variants.discount, product_variants.price, p.id AS product_id, p.seller_id, vm.images, vm.thumbnail")
            ->join('product_variant_options AS pvo','pvo.product_variant_id','product_variants.id')
            ->join('products AS p','p.id','product_variants.product_id')
            ->join('sellers','sellers.id','p.seller_id')
            ->join('categories AS c','c.id','p.primary_category')
            ->join('variation_images as vm','vm.attribute_option_id','=','pvo.attribute_option_id')
            ->join('product_categories AS pc','pc.product_id','product_variants.product_id')
            ->whereRaw('pvo.attribute_id = c.primary_attribute')
            ->where('p.seller_id',$request->sellerid)
            ->orwhere('sellers.url_slug',$request->sellerslug)
            ->groupBy('pvo.product_variant_id')->orderBy('product_variants.created_at', 'DESC')->get();
            $productimage = Product::getProductimagespath($new_collection);

            return ['seller'=> $seller,'featured'=>$featured,'new_collection'=>$new_collection];
     
        
    }
}

<?php

namespace App\Http\Controllers\Api\Seller;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Mail;

//Models
use App\Models\User;
use App\Models\Seller;
use App\Models\OrderDetail;
use App\Models\State;
use App\Models\SellerSettingChangeLog;
use App\Models\SellerWarehouses;

//Mail
use App\Mail\SellerPasswordUpdate;

class AccountController extends ApiController 
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function companyTypes(){
        $companyTypes = array(array("type"=>"Proprietary","id"=>2),
                              array("type"=>"LLP","id"=>3),
                              array("type"=>"Private Limited","id"=>4),
                              array("type"=>"Public Limited","id"=>5));


        return response($companyTypes);
    }

    public function companyDetailsStore(Request $request){
        
        $validator = Validator::make($request->all(), [
            'companyType' => 'nullable',
            'companyName' => 'required',
            'companyIdentificationNumber' => 'string|nullable',
            'idDocument' => 'nullable',
            'GSTRegistered' => 'required',
            'GSTNumber' => 'nullable',
            'GSTDocument' => 'nullable',
            'companyAddresss' => 'required',
            'state' => 'required',
            'pincode' => 'required',
            'OfficePhone'=>'required',
            'legalEntity'=>'required',
        ]);
        //
        if($validator->fails()){
            return response(['success' => false,'errors'=>$validator->messages()->toArray()], 422);  
        }
        if(!$user = auth()->user()){
            return response(['success' => false],401);
        }else{
             $seller = Seller::find($user->ref_id);
            if($request->idDocument != 0 && $request->GSTDocument != 0  ){
                $extensionIdDocument = explode('/', mime_content_type($request->idDocument))[1];
                $extensionGSTDocument = explode('/', mime_content_type($request->GSTDocument))[1];
                $base64_str_gst_doc = substr($request->GSTDocument, strpos($request->GSTDocument, ",")+1);
                $base64_str_id_doc = substr($request->idDocument, strpos($request->idDocument, ",")+1);
                $gst_doc_base64_decode = base64_decode($base64_str_gst_doc);
                $id_doc_base64_decode = base64_decode($base64_str_id_doc);
                $gst_file_name =  uniqid() . '.'.$extensionGSTDocument;
                $id_file_name = uniqid() . '.'.$extensionIdDocument;;
                $gst_file_path_and_name = './uploads/seller/gst_certificate/' .$gst_file_name;
                $id_file_path_and_name = './uploads/seller/incorporation_certificate/' . $id_file_name;
                file_put_contents($gst_file_path_and_name, $gst_doc_base64_decode);
                file_put_contents($id_file_path_and_name, $id_doc_base64_decode);
                $seller->gst_certificate = $gst_file_name;
                $seller->incorporation_certificate = $id_file_name;
             }

             
             $seller->company_type = $request->companyType;
             $seller->company_name = $request->companyName;
             $seller->company_reg_no = $request->companyIdentificationNumber;
             $seller->gst_registered = $request->GSTRegistered;
             $seller->gst_no = $request->GSTNumber;
             $seller->company_address = $request->companyAddresss;
             $seller->company_state = $request->state;
             $seller->company_pincode = $request->pincode;
             $seller->legal_entity = $request->legalEntity;
             $seller->office_phone_no  = $request->OfficePhone;
             $seller->save();
             return response(['success' => true]);
        }
       
    }

    public function pocDetailsStore(Request $request){
        $validator = Validator::make($request->all(), [
            'POCName' => 'required',
            'designation' => 'required',
            'POCPhoneNumber' => 'required',
            'POCEmail' => 'required',
            'pocDocument'=>'required'
        ]);
        if($validator->fails()){
            return response(['success' => false,'errors'=>$validator->messages()->toArray()], 422);  
        }
        if(!$user = auth()->user()){
            return response(['success' => false],401);
        }else{
            $seller = Seller::find($user->ref_id);
            $seller->poc_name = $request->POCName;
            $seller->poc_designation = $request->designation;
            $seller->poc_phone = $request->POCPhoneNumber;
            $seller->poc_email = $request->POCEmail;
            $extensionpocDocument = explode('/', mime_content_type($request->pocDocument))[1];
            $base64_str_pocDocument = substr($request->pocDocument, strpos($request->pocDocument, ",")+1);
            $pocDocument_base64_decode = base64_decode($base64_str_pocDocument);
            $pocDocument_file_name =  uniqid() . '.'.$extensionpocDocument;
            $pocDocument_file_path_and_name = './uploads/seller/poc_documents/' .$pocDocument_file_name;
            file_put_contents($pocDocument_file_path_and_name, $pocDocument_base64_decode);
            $seller->poc_doc = $pocDocument_file_name;
            $seller->save();
            return response(['success' => true]);
        }
    }

    public function bankDetails(Request $request){
        $validator = Validator::make($request->all(), [
            'accountHolderName' => 'required',
            'accountNumber' => 'required',
            'bankName' => 'required',
            'IFSCcode' => 'required',
            'cancelledChequeDocument' => 'required',
            'branchName' => 'required',
        ]); 
        if($validator->fails()){
            return response(['success' => false,'errors'=>$validator->messages()->toArray()], 422);  
        }
        if(!$user = auth()->user()){
            return response(['success' => false],401);
        }else{
            $extensioncancelledChequeDocument = explode('/', mime_content_type($request->cancelledChequeDocument))[1];
            $base64_str_cancelledChequeDocument = substr($request->cancelledChequeDocument, strpos($request->cancelledChequeDocument, ",")+1);
            $cancelledChequeDocument_base64_decode = base64_decode($base64_str_cancelledChequeDocument);
            $cancelledChequeDocument_file_name =  uniqid() . '.'.$extensioncancelledChequeDocument;
            $cancelledChequeDocument_file_path_and_name = './uploads/seller/cancelled_cheque_files/' .$cancelledChequeDocument_file_name;
            file_put_contents($cancelledChequeDocument_file_path_and_name, $cancelledChequeDocument_base64_decode);
            $seller = Seller::find($user->ref_id);
            $seller->bank_account_name = $request->accountHolderName;
            $seller->bank_account_no = $request->accountNumber;
            $seller->bank_name = $request->bankName;
            $seller->ifsc_code = $request->IFSCcode;
            $seller->branch_name = $request->branchName;
            $seller->cancelled_cheque_file = $cancelledChequeDocument_file_name;
            $seller->save();
            return response(['success' => true]);
        }
    }

    public function wareHouseDetails(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'landmark' => 'required',
            'city' => 'required',
            'state_id' => 'required',
            'pincode' => 'required',
        ]);
        if($validator->fails()){
            return response(['success' => false, 'message'=>'Invalid details', 'errors'=>$validator->messages()->toArray(), 'error'=>true], 422);  
        }

        if(!$user = auth()->user()){
            return response(['success' => false, 'message'=>'Invalid details', 'error'=>true], 422);
        }else{
            $sellerWarehouses = SellerWarehouses::where('seller_id', auth()->user()->ref_id)->where('is_default', $request->is_default)->first();
            if (!empty($sellerWarehouses)) {
                $defaultAddress = SellerWarehouses::find($sellerWarehouses->id);
                $defaultAddress->is_default = '0';
                $defaultAddress->save();
            }
            if (!empty($request->warehouse_id)) {
                $sellerWarehouseUpdate = SellerWarehouses::where('id', $request->warehouse_id)->where('seller_id', auth()->user()->ref_id)->update([
                    'name' => trim($request->warehouse_name),
                    'is_default' => $request->is_default,
                    'address' => $request->warehouse_address,
                    'landmark' => $request->warehouse_landmark,
                    'city' => $request->warehouse_city,
                    'state_id' => $request->warehouse_state,
                    'pincode' => $request->warehouse_pincode,
                ]);

                return response(['success' => true, 'message'=>'warehouse Details has been Updated Successfuly!', 'error'=>false], 200);
            } else {
                $seller_id = auth()->user()->ref_id;
                $newWarehouseDetails = [
                                        'seller_id' => $seller_id,
                                        'name' => trim($request->warehouse_name),
                                        'is_default' => $request->is_default,
                                        'address' => $request->warehouse_address,
                                        'landmark' => $request->warehouse_landmark,
                                        'city' => $request->warehouse_city,
                                        'state_id' => $request->warehouse_state,
                                        'pincode' => $request->warehouse_pincode,
                                    ];
                $sellerWarehouseUpdate = SellerWarehouses::create($newWarehouseDetails);
                return response(['success' => true, 'message'=>'warehouse Details Added Successfuly!', 'error'=>false], 200);
            }
        }
    }

    public function states(){
        $states = State::all();
        return response(['success' => true,'states'=>$states]);
    }

    public function getDetails($id=null)
    {
        $id = ($id)?$id:auth('api')->user()->ref_id;

        $details = Seller::find($id);

        // $this->respond($details);
        return response(['success' => true, 'details'=>$details]);
    }

    /**
     * [Show Seller Request Settings]
     * @return [type] [description]
    */
    public function sellerSettings(Request $request)
    {
        $sellerId = auth('api')->user()->ref_id;
        $sellerDetails = Seller::where('id',$sellerId)->select('commission', 'commission_accept','commission_type', 'cod_enable', 'shipping_model')->first();

        if($sellerId == '' || $sellerId == null){
            return response(['success' => false, 'staus_code'=>422, 'message'=>'Invalid Details', 'errors'=>'Invalid Details'], 422);
        }else{
            $sellerDetails = ['commission'=>$sellerDetails->commission, 'commission_type'=>$sellerDetails->commission_type, 'commission_accept'=>$sellerDetails->commission_accept, 'shipping_model'=>	$sellerDetails->shipping_model, 'cod_enable'=>$sellerDetails->cod_enable];

            $settingLogs = SellerSettingChangeLog::where('created_by', auth('api')->user()->ref_id)->whereNull('approved_by')->whereNull('approved_at')->whereNull('is_deleted')->get();

            return response(['success' => true, 'staus_code'=>200, 'message'=>'Raised requests', 'settingslog'=>$settingLogs, 'sellerDetails'=>$sellerDetails, 'errors'=>''], 200);
        }
    }

    /**
     * [Change Seller Request Model]
     * @return [type] [description]
    */
    public function changeRequest(Request $request)
    {
        $sellerId = auth('api')->user()->ref_id;
        if($sellerId == '' || $sellerId == null){
            return response(['success' => false, 'staus_code'=>422, 'message'=>'Invalid Details', 'errors'=>'Invalid Details'], 422);
        }else{
            //validate the required details
            $values='';
            if($request->column_name == 'cod_enable'){
                $values = 'y,n';
            }else if($request->column_name == 'shipping_model'){
                $values = 's,f';
            }
            $validator = Validator::make($request->all(), [
                                            'column_name' => 'required|in:cod_enable,shipping_model',
                                            'old_value' => 'required|in:'.$values,
                                            'new_value' => 'required|in:'.$values,
                                        ],[
                                            'column_name.in' => 'column should be cod_enable or shipping_model'
                                        ]);

            if ($validator->fails()) {
                return response(['success' => false, 'staus_code'=>422, 'message'=>'errors', 'errors'=>$validator->messages()->toArray()], 422);
            }

            //check the request is already raised or not.
            if ($request->old_value != $request->new_value) {
                $isRequestExist = SellerSettingChangeLog::where('created_by', $sellerId)
                                    ->where('settings_column_name', $request->column_name)
                                    ->whereNull('approved_by')
                                    ->whereNull('approved_at')
                                    ->whereNull('is_deleted')->exists();

                //if already request raised throw and error
                if ($isRequestExist) {
                    return response(['success' => false, 'staus_code'=>422, 'message'=>'You already raise the request, it is under review, Please wait', 'errors'=>'You already raise the request'], 422);
                }

                $changeRequest = new SellerSettingChangeLog();
                $changeRequest->settings_column_name = $request->column_name;
                $changeRequest->old_value  = $request->old_value;
                $changeRequest->new_value  = $request->new_value;
                $changeRequest->created_by  = $sellerId;

                if ($changeRequest->save()) {
                    //send mail to admin and seller your request is raised
                    $seller_details = Seller::find($sellerId);
                    $to=['seller_email' => auth('api')->user()->email,];
                    if($request->column_name == 'cod_enable'){
                        $details = [
                            'codDetails' => $changeRequest,
                            'sellerDetails' => $seller_details,
                            'req_type' => 'codChange',
                            ];            
                        __sendEmails($to, 'seller_cod_change_request', $details);
                    }else if($request->column_name == 'shipping_model'){
                        $details = [
                            'shippingDetails' => $changeRequest,
                            'sellerDetails' => $seller_details,
                            'req_type' => 'codChange',
                        ];        
                        __sendEmails($to, 'seller_shipping_change_request', $details);
                    }
                    
                    $settingLogs = SellerSettingChangeLog::where('created_by', auth('api')->user()->ref_id)->whereNull('approved_by')->whereNull('approved_at')->whereNull('is_deleted')->get();
                
                    return response(['success' => true, 'staus_code'=>200, 'message'=>'Your request has sent to fabpik team for review', 'settingslog'=>$settingLogs, 'errors'=>''], 200);
                } else {
                    return response(['success' => false, 'staus_code'=>422, 'message'=>'Some Thing went Wrong, Try After Some Time!', 'errors'=>'Request Unprocessable'], 422);
                }
            } else {
                return response(['success' => false, 'staus_code'=>422, 'message'=>'The new value should not be equal to old value', 'errors'=>'Please check once'], 422);
            }
        }
    }
    
    /**
     * [Undo Seller Changes]
     * @return [type] [description]
    */
    public function undoChanges(Request $request, $request_id=null){

        if($request_id!=null){
            if (auth('api')->user()->ref_id == '' || auth('api')->user()->ref_id == null) {
                return response(['success' => false, 'message'=>'Invalid Details', 'error'=>'Invalid Details'], 401);
            } else {
                $validator = Validator::make($request->all(), [
                    'column_name' => 'required|in:cod_enable,shipping_model'
                ],[
                    'column_name.in' => 'column should be cod_enable or shipping_model'
                ]);

                if ($validator->fails()) {
                    return response(['success' => false, 'staus_code'=>422, 'message'=>'errors', 'errors'=>$validator->messages()->toArray()], 422);
                }

                $undoRequest = SellerSettingChangeLog::where('created_by', auth('api')->user()->ref_id)->where('id', $request_id)
                                ->where('settings_column_name', $request->column_name)
                                ->whereNull('approved_by')
                                ->whereNull('approved_at')
                                ->update(['is_deleted'=>date('Y-m-d H:i:s')]);
                return response(['success' => true, 'staus_code'=>200, 'message'=>'Request undo successfully', 'errors'=>''], 200);
            }
        }else{
            return response(['success' => false, 'staus_code'=>422, 'message'=>'requestId Required', 'error'=>'requestId Required'], 422);
        }
    }

    /**
     * [Accept the commission details]
     * @return [type] [description]
    */
    public function acceptCommissionDetails(Request $request)
    {
        $sellerId = auth('api')->user()->ref_id;
        if($sellerId == '' || $sellerId == null){
            return response(['success' => false, 'staus_code'=>422, 'message'=>'token expired', 'errors'=>true], 422);
        }else{
            $sellerAccept = Seller::where('id',$sellerId)->update(['commission_accept'=>1]);
            if($sellerAccept){
                return response(['success' => true, 'staus_code'=>200, 'message'=>'', 'errors'=>false], 200);
            }else{
                return response(['success' => false, 'staus_code'=>200, 'message'=>'Internal error', 'errors'=>true], 200);
            }
            
        }
    }

        /**
     * [Change Seller Password]
     * @return [type] [description]
     */
    public function changePassword(Request $request)
    {
            $rules = [
                'current_password' => 'required',
                'password' => 'required|regex:/^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\W).*$/',
                'password_confirmation'=>'required',
            ];

            $validator = Validator::make($request->all(), $rules, [
                'password.regex' => 'Please Enter Valid Password, Password must be 8 charaters, 1 uppercase and 1 special charater',
            ]);
            
            if ($validator->fails()) {
                return response(['success' => false, 'staus_code'=>422, 'message'=>'Invalid data!', 'errors'=>true, 'err'=>$validator->messages()->toArray()], 422);
            }

            //check the user entered current password with DB Old password
            if (!(Hash::check($request->current_password, auth()->user()->password))) {
                return response(['success' => false, 'staus_code'=>422, 'message'=>'Current Password does not match!', 'errors'=>true], 422);
            }

            //check the password and old password, both can't be same
            if ($request->current_password==$request->password) {
                return response(['success' => false, 'staus_code'=>422, 'message'=>'Current Password and New Password cannot be same!', 'errors'=>true], 422);
            }
            
            //checking the new and confirm password, if both not equal throw error
            if ($request->password!=$request->password_confirmation) {
                return response(['success' => false, 'staus_code'=>422, 'message'=>'Confirm Password and New Password does not match!', 'errors'=>true], 422);
            }
    
            // if all chcekings are done insert the new password into DB
            $userUpdate = User::where('ref_id', auth()->user()->ref_id)->update([
                'password' => Hash::make($request->password),
            ]);


            $seller_details = Seller::with('user')->where('id', auth()->user()->ref_id)->first();
            
            //once the password is updated we are send an email to seller
            if ($userUpdate) {
                Mail::to($seller_details->email)->send(new SellerPasswordUpdate($seller_details));
                return response(['success' => true, 'staus_code'=>200, 'message'=>'Password Changed Successfuly!', 'errors'=>false], 200);
            }else{
                return response(['success' => false, 'staus_code'=>422, 'message'=>'Something went wrong, please try after sometime.', 'errors'=>true], 200);
            }        
    }
}
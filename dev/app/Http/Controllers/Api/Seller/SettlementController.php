<?php

namespace App\Http\Controllers\Api\Seller;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Seller;
use App\Models\OrderDetail;
use App\Models\Order;

class SettlementController extends ApiController 
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function fundsSettlement(){
        if(!$user = auth()->user()){
            return response(['success' => false],401);
        }else{
            $total_sale_value = OrderDetail::where('seller_id','=',$user->ref_id)->sum('total');
            $total_settled_amount = OrderDetail::where('seller_id','=',$user->ref_id)->where('payout_status','=',1)->sum('final_amount_to_pay_seller');
            $total_amount_to_be_settled = OrderDetail::where('seller_id','=',$user->ref_id)->where('payout_status','=',0)->sum('final_amount_to_pay_seller');
            $amountSettled = OrderDetail::select("final_amount_to_pay_seller AS amountSettled","payout_date_from_admin AS Date")
                                          ->where('payout_status','=',1)
                                          ->where('seller_id','=',$user->ref_id)
                                          ->get();
            return response(['success' => true,
                            'totalSalesValue'=>$total_sale_value,
                            'totalSettledAmount'=>$total_settled_amount,
                            'AmountToBeSettled'=>$total_amount_to_be_settled,
                            'amountSettledData'=>$amountSettled]);
        }
    }
}
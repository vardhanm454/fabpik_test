<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App;
use Hash;
use Validator;
use Carbon\Carbon;
use Avatar;
use Mail;
use DB;

// Models
use App\Models\User;
use App\Models\Cart;
use App\Models\VerifyOtp;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Seller;
use App\Models\Coupon;
use App\Models\FCMToken;
use App\Models\NotificationLogs;
use App\Mail\forgotPasswordEmail;
use App\Mail\ForgotPasswordUserEmail;
use App\Mail\PasswordResetEmail;
use App\Mail\SendOtpUserEmail;
use App\Mail\SendOtpUserRegisterEmail;
use App\Mail\SendRegisterSuccessEmail;
use App\Mail\ChangeMobileNumberOtpEmail;

class AuthController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login','forgot_password','sendMailLoginOtp','loginWithPassword','verifyOtpLoginDetails','verifyOtpRegister','verifyOtp','resendOtp','register','forgotPassword','resetPassword','changePassword','verifyOtpLogin']]);
    }

    protected function credentials(Request $request)
    {
        if(is_numeric($request->mobile))
            return ['mobile'=>$request->mobile,'password'=>'1234'];
        // if (filter_var($request->username, FILTER_VALIDATE_EMAIL))
        //     return ['email' => $request->username, 'password'=>$request->password];
    }

    protected function getUserName(Request $request)
    {

    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'user' => 'required'
            ]);        
            // $mobileExistsForOtherTypeAccounts = User::where('mobile',$request->user)->whereNotNull('seller_id')->exists();
            // $emailExistForOtherTypeAccounts = User::where('email',$request->user)->whereNotNull('seller_id')->exists();
            // if($mobileExistsForOtherTypeAccounts || $emailExistForOtherTypeAccounts){
            //     return $this->respond(['success'=>false,'msg'=>'The phone number is registered for different account type, please use other mobile number to register.']);
            // }
            $user = $request->user;
            // $mobileCheck = User::where('mobile','=',$request->user)
            //                     ->where('user_type','=','customer')
            //                     ->exists();
            // $emailCheck = User::where('email','=',$request->user)
            //                     ->where('user_type','=','customer')
            //                     ->exists();
            $checkUserExists = User::whereRaw("(email = '$user' OR mobile = '$user') AND user_type = 'customer'")
                                     ->exists();
            if(!$checkUserExists){
                return $this->respond(['success'=>false,'msg'=>'New User!']);
            }else{
                $checkUserActiveStatus = Customer::whereRaw("(email = '$user' OR mobile = '$user') AND status = 1")->exists();
                if(!$checkUserActiveStatus){
                    return $this->respond(['success'=>false,'msg'=>'Try again after sometime. If it occurs again, reach out to Fabpik Customer care.']);
                }else{
                    $otp = __generateOtp();
                    $userDetails = User::select("users.mobile","users.email")
                                        ->whereRaw("(mobile = '$user' OR email = '$user') AND user_type = 'customer'")
                                        ->first(); 
                    if($userDetails){
                        // Mail::to($userDetails->email)->send(new SendOtpUserEmail(['otp'=>$otp]));
                        $this->sendOtp($userDetails->mobile,$otp,'login_otp');
                        $this->saveOTP($userDetails->mobile,$otp);  
                    }
                    // else{
                    //     $userDetails = User::select("users.mobile","users.email")->where('email','=',$request->user)->first();
                    //     // Mail::to($userDetails->email)->send(new SendOtpUserEmail(['otp'=>$otp]));
                    //     $this->sendOtp($userDetails->mobile,$otp,'login_otp');
                    //     $this->saveOTP($userDetails->mobile,$otp); 
                    // }
                }
                return $this->respond(['success'=>true]);
            }
        } catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }

       
    }

    public function sendMailLoginOtp(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'user' => 'required'
            ]);
            $userDetails = User::select("users.mobile","users.email")->where('mobile','=',$request->user)->first(); 
            
            if($userDetails){
                $otpData = VerifyOtp::where('mobile', $userDetails->mobile)->first();
                Mail::to($userDetails->email)->send(new SendOtpUserEmail(['otp'=>$otpData->otp]));
            }else{
                $userDetails = User::select("users.mobile","users.email")->where('email','=',$request->user)->first();
                $otpData = VerifyOtp::where('mobile', $userDetails->mobile)->first(); 
                Mail::to($userDetails->email)->send(new SendOtpUserEmail(['otp'=>$otpData->otp]));
            }
            return $this->respond(['success'=>true]); 
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Register a new User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) 
    {   try{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'mobile'=>'required|unique:customers',
                'email'=>'required|unique:customers',
                'password'=>'required',
            ]);
            if($validator->fails()){
                return $this->respond(['success'=>false,'errors'=>$validator->errors()], 200);
            }

            $otp = __generateOtp();
            $this->sendOtp($request->mobile,$otp,'verify_mobile');
            // Mail::to($request->email)->send(new SendOtpUserRegisterEmail(['otp'=>$otp]));
            $this->saveOTP($request->mobile,$otp);

            return $this->respond(['success'=>true]); 
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
        //  $token = auth('api')->attempt(['mobile'=>$user->mobile,'password'=>'1234']);
    }

    public function verifyOtpRegister(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'otp' => 'required',
                'mobile'=>'required',
                'email'=>'required',
                'password'=>'required',
                'name'=>'required'
            ]);
            if($validator->fails()){
                return $this->respond(['success'=>false,'errors'=>$validator->errors()], 200);
            }

            DB::beginTransaction();
            $otpData = VerifyOtp::where('mobile', $request->mobile)->first();
    
            if(!$otpData || ($otpData->otp != $request->otp)) return $this->respond(['success'=>false,'msg'=>'Otp Verification Failed!']);
            
            $customer = new Customer();
            $customer->mobile = $request->mobile;
            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->save();
    
            // Create a new Customer User
            $user = new User();
            $user->ref_id = $customer->id;
            $user->user_type = 'customer';
            $user->mobile = $customer->mobile;
            $user->name = $customer->name;
            $user->email = $customer->email;
            $user->password = Hash::make($request->password);
            $user->save();
    
            $notification_log = new NotificationLogs();
            $notification_log->user_id = $user->id;
            $notification_log->save();

    
            $user->assignRole('Customer');
    
            $credentials = [
                'mobile'=>$request->mobile,
                'password'=>$request->password, 
                'user_type'=>'customer'
            ];
    
            if(!$token = auth('api')->attempt($credentials)) {
                return response(['success' => false,'msg'=>'Cannot Login,Try after sometime'], 200); 
            }
            $this->guestCartAddToUser($user->ref_id,null);
            $otpData->delete();

            if($user && $customer){
                DB::commit();
                Mail::to($request->email)->send(new SendRegisterSuccessEmail(['userDetails'=>$customer]));
                return $this->respond([ 
                    'success'=>true,
                    'access_token' => $token,
                    'token_type' => 'bearer',
                    'user' => auth('api')->user()
                ]);
            }else{
                DB::rollback();
                return $this->respond(['success'=>false,'msg'=>"Something went wrong!"]);
            }
            // $flashDeals = $this->getFlashDeals();
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function getFlashDeals(){
        $products = DB::select("SELECT
                        `products`.`seller_id`,
                        `pv`.`name`,
                        `pv`.`description` AS `pv_desc`,
                        `pv`.`product_id`,
                        `pv`.`unique_id`,
                        `pv`.`id` AS `product_variant_id`,
                        `pv`.`slug`,
                        `pv`.`mrp` AS `mrp`,
                        `pv`.`discount`,
                        `pv`.`price` AS `price`,
                        `vm`.`images`,
                        `vm`.`thumbnail`,
                        `sellers`.`company_name`
                        FROM
                            `products`
                        INNER JOIN `product_variants` AS `pv`
                        ON
                            `pv`.`product_id` = `products`.`id`
                        INNER JOIN `sellers`
                        ON
                            `sellers`.`id` = `products`.`seller_id`
                        INNER JOIN `categories` AS `c`
                        ON
                            `c`.`id` = `products`.`primary_category`
                        INNER JOIN `product_variant_options` AS `pvo`
                        ON
                            `pvo`.`product_variant_id` = `pv`.`id`
                        INNER JOIN `variation_images` AS `vm`
                        ON
                            `vm`.`attribute_option_id` = `pvo`.`attribute_option_id`
                        WHERE
                            products.fabpik_featured = 1 AND pvo.attribute_id = c.primary_attribute AND products.status = 1 AND `vm`.`product_id` = `products`.`id` AND products.deleted_at IS NULL AND `products`.`deleted_at` IS NULL
                        GROUP BY
                            products.id
                        ORDER BY
                            `products`.`no_of_items`
                        DESC
                        LIMIT 3"
                    );
        $returns = [];
        if($products) {
            foreach ($products as $product) {
                // get discount percentage
                $product->discount = number_format(($product->discount * 100) / $product->mrp, 1) + 0;
                $images = json_decode($product->images);
                if($images){
                    if($product->thumbnail != null){
                        $product->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($product->thumbnail-1)]);
                    }else{
                        $product->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                    }
                    array_push($returns, $product);
                }else{
                    $product->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                    array_push($returns, $product);
                }
            }
        }
        return $returns;
    }

    public function verifyOtpLoginDetails(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'user'=>'required'
            ]);
            if($validator->fails()){
                return $this->respond(['success'=>false,'errors'=>$validator->errors()], 200);
            }
            
            $userDetails = User::select("users.mobile","users.email")
                                ->where('user_type','customer'); 
            if($userDetails = $userDetails->where('mobile',$request->user)->first()){
                return $this->respond(['success'=>true,'user'=>$userDetails,'type'=>'mobile']);
            }else{
                // $userDetails = $userDetails->where('email',$request->user)->first();
                $userDetailsEmail = User::select("users.mobile","users.email")
                                ->where('user_type','customer')
                                ->where('email',$request->user)->first();
                return $this->respond(['success'=>true,'user'=>$userDetailsEmail,'type'=>'email']);
            }
            return $this->respond(['success'=>false]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function verifyOtpLogin(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'user'=>'required',
                'otp'=>'required'
            ]);
            
            if($validator->fails()){
                return $this->respond(['success'=>false,'errors'=>$validator->errors()], 200);
            }
    
            $userDetails = User::where('mobile',$request->user)->where('user_type', 'customer')->first();
            if($userDetails){
                $otpData = VerifyOtp::where('mobile', $userDetails->mobile)->first();
                if($otpData && $otpData->otp == $request->otp) {    
                    if(!$token = auth('api')->login($userDetails)){
                        return response(['success' => false,'msg'=>'Cannot Login,Try after sometime'], 200); 
                    }else{
                        $this->guestCartAddToUser($userDetails->ref_id,$userDetails->deal_claimed_date);
                        if($request->fcm_token != null){
                            $this->addFCMToken($userDetails->ref_id,$request->fcm_token);
                        }
                        $otpData->delete();
                        return $this->respond([ 
                            'success'=>true,
                            'access_token' => $token,
                            'token_type' => 'bearer',
                            'user' => auth('api')->user()
                        ]);
                    }
                }else{
                    return $this->respond(['success'=>false,'msg'=>"Otp Verification Failed!"]);
                }            
            } else {
                $userDetails = User::where('email',$request->user)->where('user_type', 'customer')->first();
                $otpData = VerifyOtp::where('mobile', $userDetails->mobile)->first();
    
                if(!$otpData || ($otpData->otp != $request->otp)) return $this->respond(['success'=>false,'msg'=>"Otp Verification Failed!"]);
    
                if(!$token = auth('api')->login($userDetails)){
                    return response(['success' => false,'msg'=>'Can not login, Try after sometime.'], 200); 
                } else {
                    $this->guestCartAddToUser($userDetails->ref_id,$userDetails->deal_claimed_date);
                    if($request->fcm_token != null){
                        $this->addFCMToken($userDetails->ref_id,$request->fcm_token);
                    }
                    $otpData->delete();
                    return $this->respond([
                        'success'=>true,
                        'access_token' => $token,
                        'token_type' => 'bearer',
                        'user' => auth('api')->user()
                    ]);
                }
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function guestCartAddToUser($customer_id,$deal_claimed_date){
        session_start();
        $guestCart = isset($_SESSION['cart']) ? $_SESSION['cart']:null;
        if($guestCart){
            foreach ($guestCart as $cartItem) {
                $this->addProductToCart($cartItem['product_variant_id'],$cartItem['quantity'],$customer_id,$deal_claimed_date);
            }
            unset($_SESSION['cart']);
        }else{
            return false;
        }
    }

    public function addFCMToken($customer_id,$fcm_token){
        FCMToken::updateOrInsert(
            ['fcm_token' => $fcm_token],
            ['fcm_token' => $fcm_token,'user_id' =>$customer_id]
        );
    }

    public function addProductToCart($productVariantId,$quantity,$customer_id,$deal_claimed_date)
    {
        $cartCount = 0;
        if (date('Y-m-d') == date('Y-m-d', strtotime(auth('api')->user()->deal_claimed_date))) {
            $userDealsLimitExpired = true;
        }else{
            $userDealsLimitExpired = false;
        }
        $checkProductExistenceInCart = Cart::where('product_variant_id','=',$productVariantId)
                                             ->where('customer_id','=',$customer_id)
                                             ->exists();
        $checkDealProductAlreadyExists = Cart::where('customer_id','=',$customer_id)
                                               ->where('is_deal','=','y')
                                               ->exists();
        $productVariant = Cart::getProductVariantDetails($productVariantId);

        if($quantity > $productVariant->stock){
            return response(['success' => false,'msg'=>"Stock not Available!"]);
        }

        $product = Cart::getProductDetails($productVariant->product_id);

        if($checkProductExistenceInCart){
            $checkProductIsDealProduct = Cart::where('product_variant_id','=',$productVariantId)
                                               ->where('customer_id','=',$customer_id)
                                               ->where('is_deal','=','y')
                                               ->exists();
            $cart = Cart::where('product_variant_id','=',$productVariantId)
                          ->where('customer_id','=',$customer_id)
                          ->update(['quantity' => $productVariant->deal_price != null && !$userDealsLimitExpired &&  (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? 1 : $quantity,
                                    'price' =>$productVariant->deal_price != null && !$userDealsLimitExpired && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? $productVariant->deal_price : $productVariant->fabpik_seller_price,
                                    'total_price'=>$productVariant->deal_price != null && !$userDealsLimitExpired && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? $productVariant->deal_price  : $productVariant->fabpik_seller_price * $quantity,
                                    'seller_discount'=> $productVariant->deal_price != null && !$userDealsLimitExpired && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? ($productVariant->mrp - $productVariant->deal_price)  : $productVariant->fabpik_seller_discount,
                                    'total_seller_discount'=>$productVariant->deal_price != null && !$userDealsLimitExpired && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? ($productVariant->mrp - $productVariant->deal_price)  : $productVariant->fabpik_seller_discount * $quantity,
                                    'is_deal'=>$productVariant->deal_price != null && !$userDealsLimitExpired && (!$checkDealProductAlreadyExists || $checkProductIsDealProduct) ? 'y' : 'n']);

        }else{
            $seller = Seller::where('id','=', $product->seller_id)->first();
            $cart = new Cart();
            $cart->customer_id = $customer_id;
            $cart->product_id = $product->id;
            $cart->product_variant_id = $productVariant->id;
            $cart->seller_id = $product->seller_id;
            $cart->quantity = $productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired ? 1 : $quantity;
            $cart->mrp = $productVariant->mrp;
            $cart->total_seller_discount =$productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired ? ($productVariant->mrp - $productVariant->deal_price)  : $productVariant->fabpik_seller_discount * $quantity;

            $cart->price = $productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired ? $productVariant->deal_price : $productVariant->fabpik_seller_price;

            $cart->seller_discount = $productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired ? ($productVariant->mrp - $productVariant->deal_price)  : $productVariant->fabpik_seller_discount;

            $cart->total_price = $productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired ? $productVariant->deal_price  : $productVariant->fabpik_seller_price * $quantity;
            if($productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired){
                $cart->is_deal = 'y';
            }
            $cart->seller_pincode = $seller->company_pincode;
            $address =  CustomerAddress::where('customer_id','=',$customer_id)
                                        ->where('is_default','=',1)
                                        ->first();
            if($address){
                $cart->customer_pincode = $address->pincode;
            }
            $findCoupon = Cart::where('customer_id','=',$customer_id)->first();
            if(isset($findCoupon->coupon_id)){
                $cart->coupon_id = $findCoupon->coupon_id;
                $coupon = Coupon::where('id','=',$findCoupon->coupon_id)->first();
                if($coupon){
                    if($coupon->dis_type == 'f' && $coupon->sub_type == 's' && ($productVariant->deal_price == null || $checkDealProductAlreadyExists)){
                        $cart->coupon_discount = $coupon->amount;
                    }else if($coupon->dis_type == 'p' && $coupon->sub_type == 's' && ($productVariant->deal_price == null || $checkDealProductAlreadyExists)){
                        $cart->coupon_discount = ($coupon->amount/100) * ($productVariant->fabpik_seller_price * $quantity);
                    }else if($coupon->dis_type == 'p' && $coupon->sub_type == 'm' && ($productVariant->deal_price == null || $checkDealProductAlreadyExists)){
                        $couponDiscountAmount = ($coupon->amount/100) * ($productVariant->mrp * $quantity);
                        // if($couponDiscountAmount > ($productVariant->discount * $request->quantity)){
                            $cart->coupon_discount = $couponDiscountAmount;
                        // }
                        // else{
                        //     $cart->coupon_discount = $couponDiscountAmount;
                        // }
                    }
                    if($productVariant->deal_price != null && !$checkDealProductAlreadyExists && !$userDealsLimitExpired){
                        $cart->coupon_applied = 'n';
                    }else{
                        $cart->coupon_applied = 'y';
                    }
                }
            }
            $cart->save();

        }
    }

    public function loginWithPassword(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'user'=>'required',
                'password'=>'required'
            ]);
            
            if($validator->fails()){
                return $this->respond(['success'=>false,'errors'=>$validator->errors()], 200);
            }
    
            $userDetails = User::where('mobile',$request->user)->where('user_type', 'customer')->first();
            if($userDetails){
                if(!$token = auth('api')->attempt(['mobile'=>$request->user,'password'=>$request->password,'user_type'=>'customer'])){
                    return response(['success' => false,'msg'=>'Wrong Credentials!'], 200); 
                } else {
                    $this->guestCartAddToUser($userDetails->ref_id,$userDetails->deal_claimed_date);
                    return $this->respond([ 
                        'success'=>true,
                        'access_token' => $token,
                        'token_type' => 'bearer',
                        'user' => auth('api')->user()
                    ]);
                }
            } else {
                if(!$token = auth('api')->attempt(['email'=>$request->user,'password'=>$request->password,'user_type'=>'customer'])){
                    return response(['success' => false,'msg'=>'Wrong Credentials!'], 200); 
                } else {
                    $userDetailsWithEmail = User::where('email',$request->user)->where('user_type', 'customer')->first();
                    $this->guestCartAddToUser($userDetailsWithEmail->ref_id,$userDetailsWithEmail->deal_claimed_date);
                    return $this->respond([ 
                        'success'=>true,
                        'access_token' => $token,
                        'token_type' => 'bearer',
                        'user' => auth('api')->user()
                    ]);
                }
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Resend Otp.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendOtp(Request $request)
    {   
        try{
            $validator = Validator::make($request->all(), [
                'mobile'=>'required|integer|digits:10',
                'type'=>'required'
            ]);
    
            if($validator->fails()){
                return $this->respond($validator->errors()->toJson(), 400);
            }
            
            $otp = __generateOtp();
            $this->saveOTP($request->mobile, $otp);
            if($request->type == "login_otp"){
                $userDetails = User::select("users.mobile","users.email")->where(['mobile'=>$request->mobile, 'user_type'=>'customer'])->first(); 
                Mail::to($userDetails->email)->send(new SendOtpUserEmail(['otp'=>$otp]));
                // return $this->respond(['success' =>true]);
            }
            if($this->sendOtp($request->mobile, $otp, $request->type)) {
                // if($request->email){
                //     Mail::to($request->email)->send(new SendOtpUserRegisterEmail(['otp'=>$otp]));
                // }
                return $this->respond(['success'=>true]);
            }else{
                return $this->respond(['success'=>false]);
            }  
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Forgot Password.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword(Request $request)
    {

    }

    /**
     * Reset Password.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(Request $request)
    {   try{
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required|string',
                'password' => 'required|string|confirmed|min:6'
            ]);

            if($validator->fails()){
                return $this->respond($validator->errors()->toJson(), 400);
            }

            $user = User::where('remember_token', $request->remember_token)->first();
            if($user){
                // update user password
                User::where(['email'=>$user->email, 'user_type'=>'customer'])->update(['password'=>Hash::make($request->password)]);

                //send change password success mail to user
                $updatedDate = date('d M, Y h:i:A', strtotime($user->updated_at)); 
                Mail::to($user->email)->send(new PasswordResetEmail($user,$request->password,$updatedDate));
            
                return $this->respondSuccess();
            }       
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Change Password.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request)
    {   
        try{
            $validator = Validator::make($request->all(), [
                'old_password' => 'required|string|min:6',
                'password' => 'required|string|confirmed|min:6'
            ]);
    
            if($validator->fails()){
                return $this->respond($validator->errors()->toJson(), 400);
            } 
    
            //login user password match with old password
            if ((Hash::check($request->old_password, auth('api')->user()->password)) == false) {
                return $this->respond(['error' => 'Incorrect old password entered.'], 400);
            } else if ((Hash::check($request->password, Auth::user()->password)) == true) {
                return $this->respond(['error' => 'Please enter a password which is not similar then current password.'], 400);
            } else {
                $email = auth('api')->user()->email;
    
                // update use password
                User::where(['email'=>$email,'user_type'=>'customer'])->update(['password'=>Hash::make($request->password)]);
    
                $updatedDate = date('d M, Y h:i:A', strtotime(auth('api')->user()->updated_at));
               
                //send change password success mail to user
                Mail::to($email)->send(new PasswordResetEmail(auth('api')->user(),$request->password,$updatedDate));
               
                return $this->respondSuccess();
            }        
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile()
    {
        $customer = Customer::where('id',auth('api')->user()->ref_id)->get();
        $customerdata = Customer::getCustomerAvatar($customer);
       // return $this->respond(auth('api')->user());
        return $this->respond($customerdata);
    }

    /**
     * update profile authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(Request $request)
    {
        try{
            if(count($request->all()) > 2){
                $validator = Validator::make($request->all(), [
                    'name' => 'required|string|between:2,100',
                    'email' => 'required|string|email|max:100',
                    'mobile' => 'required|integer|digits:10',
                    'gender' => 'required|string',
                    'dob' => 'required|string'
                ]);
            }else{
                $validator = Validator::make($request->all(), [
                    'mobile' => 'required|integer|digits:10'
                ]);
            }
           
            if($validator->fails()){
                return response(['success' => false,'error'=>$validator->messages()->toArray()], 210);  
            }
    
            $user = auth('api')->user();
    
            if(($user->mobile == $request->mobile) && (count($request->all()) <= 2)){
                if(!empty($request->otp)) {
                    $otpData = VerifyOtp::where('mobile', $request->mobile)->first();                
        
                    if($otpData && $otpData->otp == $request->otp) { // OTP verified, now update user
                        // Delete OTP
                        $otpData->delete();
                        return $this->respondSuccess();
                    }
        
                    return $this->respond(['error'=>'OTP verification failed.'], 422);
                } else {
                    $otp = __generateOtp();
                    $this->saveOTP($request->mobile, $otp);
                    
                    if($this->sendOtp($request->mobile, $otp, 'login_otp')) {
                        Mail::to($user->email)->send(new ChangeMobileNumberOtpEmail(['otp'=>$otp]));
                        return $this->respondSuccess();
                    }
                }
    
            } else if($user->mobile !== $request->mobile) {
                
                if(!empty($request->otp)) {
                    $otpData = VerifyOtp::where('mobile', $request->mobile)->first();                
        
                    if($otpData && $otpData->otp == $request->otp) { // OTP verified, now update user
                        // update a Customer 
                        $checkUniqueNumber = User::where(['mobile'=>$request->mobile,'user_type'=>'customer'])->exists();
                        if($checkUniqueNumber){
                            return $this->respond(['error'=>'The phone number is registered for different account, please use other mobile number to register.']);
                        }
    
                        Customer::where('id',$user->ref_id)->update([
                            'mobile' => $request->mobile
                        ]);
    
                        // update User
                        $user->mobile = $request->mobile;
                        $user->save();
    
                        // Delete OTP
                        $otpData->delete();
        
                        return $this->respondSuccess();
                    }
        
                    return $this->respond(['error'=>'OTP verification failed.'], 422);
                } else {
                    $otp = __generateOtp();
                    $this->saveOTP($request->mobile, $otp);
                    
                    if($this->sendOtp($request->mobile, $otp, 'login_otp')) {
                        return $this->respondSuccess();
                    }
                }
            } else {
                if($request->gstDoc){
                    $extensionGstDoc = explode('/', mime_content_type($request->gstDoc))[1];
                    $base64_str_gstDoc = substr($request->gstDoc, strpos($request->gstDoc, ",")+1);
                    $gstDoct_base64_decode = base64_decode($base64_str_gstDoc);
                    $gstDoc_file_name =  uniqid() . '.'.$extensionGstDoc;
                    $gstDoc_file_path_and_name = './uploads/gst_doc/' .$gstDoc_file_name;
                    file_put_contents($gstDoc_file_path_and_name, $gstDoct_base64_decode);
                    $gst_certificate = $gstDoc_file_name;
                } else {
                    $gst_certificate = $request->gst_certificate;
                }
    
                // create Avatar image for user
                $avatar = md5(uniqid().uniqid()).'.jpg';
                Avatar::create($request->name)->save('uploads'.DIRECTORY_SEPARATOR.'avatar'.$avatar, 100);
    
                // update a Customer
                Customer::where('id',$user->ref_id)->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'mobile' => $request->mobile,
                    'image'  => $avatar,
                    'alternate_mobile' => $request->alternate_mobile,
                    'gender' => $request->gender,
                    'dob' => $request->dob,
                    'gst' => $request->gst,
                    'gst_no' => $request->gst_no,
                   'gst_certificate' =>  $gst_certificate   
                ]);
                
                // update User
                $user->name = $request->name;
                $user->email = $request->email;
                $user->mobile = $request->mobile;
                $user->avatar = $avatar;
                $user->save();
    
                return $this->respondSuccess('User Updated successfully');
            }
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try{
            auth('api')->logout(true);
            return $this->respondSuccess();
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return $this->respond([
            'success'=>true,
            'access_token' => $token,
            'token_type' => 'bearer',
            // 'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]);
    }

    public function saveOTP($mobile, $otp)
    {
        return VerifyOtp::updateOrInsert([
            'mobile' => $mobile
        ],
        [
            'mobile' => $mobile,
            'otp' => $otp,
            'otp_expiry' => Carbon::now()->addMinutes(3)
        ]);
    }

    public function sendOtp($mobile, $otp, $type='login_otp')
    {
        __sendSms($mobile,$type,['otp'=>$otp]);
        return true;
    }

    public function passwordGeneration($n)
    {
        $generator = "abcmhkghijk";
        $result = "";
        for ($i = 1; $i <= $n; $i++) {
            $result .= substr($generator, (rand()%(strlen($generator))), 1);
        }
        return $result;
    }

    public function forgot_password(Request $request)
    {
        try{
            $user = User::where(['email'=>$request->email, 'user_type'=>'customer'])->first();
            if(!$user){
                return response()->json([
                    'success' => false,
                    'msg'=>'User doesnt exist',
                ]);
            }
    
            $password= $this->passwordGeneration(8);
    
            Mail::to($request->email)->send(new ForgotPasswordUserEmail($password));
    
            $user->password = Hash::make($password);
            $user->save();
    
            return response()->json([
                'success' => true,
                'msg'=>'Password has been sent to your mail',
            ]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }

    public function change_password(Request $request)
    {
        try{
            $user = auth('api')->user();
    
            if (!Hash::check($request->oldpassword, $user->password)) {
                return response()->json([
                    'success' => false,
                    'msg' => "Your old password doesn't match",
                ]);          
            }
            
            if(Hash::check($request->newpassword, $user->password)){
                return response()->json([
                    'success' => false,
                    'msg' => "You are keeping the same password again",
                ]);  
            }
    
            $user->password = Hash::make($request->newpassword);
            $user->save();
            return response()->json([
                'success' => true,
                'msg' => "Successfully changed the password",
            ]);
        }catch (\Throwable $e) {
            return $this->respond(['success'=>false,'msg'=>"Something went wrong!",'error_msg'=>$e->getMessage()]);
        }
    }


}
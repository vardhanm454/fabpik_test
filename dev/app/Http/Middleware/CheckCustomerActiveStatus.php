<?php

namespace App\Http\Middleware;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Closure;
use Illuminate\Http\Request;
use App\Models\Customer;


class CheckCustomerActiveStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Customer::find(auth('api')->user()->ref_id);
        if($user->status == 0){
            auth('api')->logout(true);
            throw new UnauthorizedHttpException('jwt-auth', 'You are banned!');
        }
        return $next($request);
    }
}

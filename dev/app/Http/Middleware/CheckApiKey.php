<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('apikey');
        if (!$token)
        {
            return response()->json(['error'=>'Unauthorised Access!'], 403);
        }

        // set Authorization token
        $request->headers->set('Authorization', 'Bearer '.$token);

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        "http://localhost/fabpikall/dev/api/handleWebhook",
        "https://1f6223576d7f.ngrok.io/fabpik-latest/dev/api/handleWebhook"
    ];
}

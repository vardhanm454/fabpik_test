<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserApiVersion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $api_version = $request->header('api-version');
        // if (!$request->api_version || $request->api_version != '1.0.0')
        if (!$api_version || $api_version != '1.0.0')
        {
            return response()->json(['error'=>'Please update your App!'], 422);
        }

        return $next($request);
    }
}
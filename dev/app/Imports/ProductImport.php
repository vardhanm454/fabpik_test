<?php

namespace App\Imports;

use Illuminate\Support\Collection; 
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Validators\Failure;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
// use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

// Models
use App\Models\Brand;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\ChildCategory;
use App\Models\AttributeOption;
use App\Models\IronType;
use App\Models\WashingType;
use App\Models\Country;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\VariationImage;
use App\Models\ProductVariant;
use App\Models\ProductVariantOption;
use App\Models\Sizechart;
use App\Models\Seller;

class ProductImport implements ToCollection, WithHeadingRow, WithChunkReading, WithColumnFormatting
{
	use Importable;

    private $seller_id;
    private $errors = []; // array to accumulate errors
    
    public function __construct($seller_id)
    {
        $this->seller_id = $seller_id;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        // dd($rows);
        $productId = 0;

        $yesNo = ['yes'=>1,'no'=>0];

        // get all brands
        $brands = Brand::active()->whereNull('created_by')->orWhere('created_by', $this->seller_id)->pluck('id','name')->toArray();

        //get all categories 
        $categories = Category::active()->pluck('id','title')->toArray();

        //get all category wise primary attribute
        $catPrimaryAttr = Category::active()->pluck('primary_attribute','title')->toArray();

        //get all category wise secondary attribute
        $catSecondaryAttr = Category::active()->pluck('secondary_attribute','title')->toArray();

        //get all categories 
        $childCategories = DB::table('view_categories')->selectRaw("path_id, REPLACE(path_title, ' > ', '>>') as path_title")->pluck('path_id','path_title')->toArray();

        // get all attribute options
        $attrOptions = AttributeOption::selectRaw("id, CONCAT(attribute_id,'-',option_name) as name")->pluck('id','name')->toArray();

        // get all attribute options
        $attributeOptions = AttributeOption::selectRaw("id, option_name")->pluck('id','option_name')->toArray();

        // get all iron types
        $ironTypes = IronType::pluck('id','name')->toArray();

        // get all washing types
        $washingTypes = WashingType::pluck('id','name')->toArray();

        // get all countries
        $countries = Country::pluck('id','name')->toArray();

        // get all size charts
        $sizeChart = Sizechart::where('seller_id', $this->seller_id)->pluck('id','name')->toArray();
        
        //get Seller Code
        $sellerDetails = Seller::select('seller_code', 'commission_type')->where('id', $this->seller_id)->first();
        $seller_code = '';
        if($sellerDetails != null){
            if ($sellerDetails->seller_code != null) {
                $seller_code = $sellerDetails->seller_code;
            }
        }

        $rows = $rows->toArray();

        // iterating each row and validating it:
        foreach ($rows as $key=>$row) {            
            $rules = [
                'type' => [
                            'required',
                            Rule::in(['configurable', 'variant']),
                        ],

                
                'sku' => ($row['type'] == 'configurable') ? (['required', function ($attribute, $value, $fail) {
                                            
                        $prdExists = Product::where('seller_id', $this->seller_id)->where('sku', $value)->whereNull('deleted_at')->exists();
                               if($prdExists) {
                                    $fail("Product SKU <strong>".$value."</strong> is exist.");
                                }
                            
                        },]) : ( ($row['type'] == 'variant') ? (['required', function ($attribute, $value, $fail) {

                            $prdVrnExists = ProductVariant::with(['product' => function($q){
                                    $q->where('seller_id', $this->seller_id);
                                }])->where('sku', $value)->whereNull('deleted_at')->first();

                            if($prdVrnExists != null) {
                                $fail("Variant SKU <strong>".$value."</strong> is exist.");
                            }

                            },]) :'' ),

                // 'thumbnail' => ($row['type']=='variant')?(['required', 'mimes:jpeg,png,jpg,gif']):'',
                // 'images' => ($row['type']=='variant')?(['required', 'mimes:jpeg,png,jpg,gif']):'',
                
                // 'hsn' => ($row['type']=='configurable')?(['required',function ($attribute, $value, $fail) {
                //                                     // Check HSN exist or not
                //                                     $hsnExists = Product::where('hsn_code', $value)->whereNull('deleted_at')->exists();
                //                                     if($hsnExists) {
                //                                         $fail("HSN <strong>".$value."</strong> is exist.");
                //                                     }
                //                                 }]):'',

                'name' => ['required'/*, 'unique:products,name'*/],
                'description' => ['required'],
                'minimumstockquantity' => ['required', 'gte:1'],
                'brand' => [
                                'required',
                                function ($attribute, $value, $fail) use($brands) {
                                    // Check brand exist or not
                                    if(!array_key_exists($value, $brands)) {
                                        $fail("Brand name as <strong>".$value."</strong> not exist.");
                                    }
                                },
                            ],
                'primarycategory' => [
                                        'required',
                                        function ($attribute, $value, $fail) use($categories) {
                                            // Check category title valid or not
                                            if(!array_key_exists($value, $categories)) {
                                                $fail("Primary category name as <strong>".$value."</strong> not exist.");
                                            }
                                        },
                                    ],

                /**
                 * Valiation for clothing Primarty Attribute and Secondary Attributes are Required
                 * Color and ClothingSize.
                 
                 * Valiation for Personal Care Primarty Attribute and Secondary Attributes are Required
                 * Quantity, Age.
                
                 * Valiation for Organic Primarty Attribute is Required
                 * Health Size.
                 
                 * Valiation for Toys Primarty Attribute and secondary Attributes are Required
                 * Color, Age.
                
                 * Valiation for Footwear Primarty Attribute and Secondary Attributes are Required
                 * Color, FootWearSize
                 
                 * Valiation for Sports Primarty Attribute is Required
                 * sportstype
                 
                 * Valiation for Health & Safety Primarty Attribute is Required
                 * HealthSize
                 
                 * Valiation for Food & Nutrition Primarty Attribute is Required
                 * Quantity
                 
                 * Valiation for Books & Art Primarty Attribute and Secondary Attributes are Required
                 * Types, Age
                 
                 * Valiation for Accessories Primarty Attribute and secondary Attributes are Required
                 * Color, Age.
                 * 
                */

                //all primary atttibutes must give the value
                'color' => (
                    (($row['primarycategory']== 'Clothing') || ($row['primarycategory']==  'Toys') || 
                    ($row['primarycategory']== 'Footwear') || ($row['primarycategory']== 'Accessories')) 
                    && $row['type']=='variant') ?
                                        [
                                            'required',
                                            function ($attribute, $value, $fail) use($attributeOptions) {
                                                    if($value == 'N/A'){
                                                        $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                    }
                                                    else if(($value != 'N/A') && !array_key_exists($value, $attributeOptions)) {
                                                        $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                    }
                                                },
                                        ]:'',

                'sportstype' => ( $row['primarycategory']== 'Sports' && $row['type']=='variant')?[
                                                                        'required',
                                                                        function ($attribute, $value, $fail) use($attributeOptions) {
                                                                                if($value == 'N/A'){
                                                                                    $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                                                }
                                                                                else if(($value != 'N/A') && !array_key_exists($value, $attributeOptions)) {
                                                                                    $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                                                }
                                                                            },
                                                                    ]:'',
                'quantity' => (
                    (($row['primarycategory']== "Personal Care") || ($row['primarycategory']== "Food & Nutrition"))
                    && $row['type']=='variant')?[
                                                                        'required',
                                                                        function ($attribute, $value, $fail) use($attributeOptions) {
                                                                                if($value == 'N/A'){
                                                                                    $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                                                }
                                                                                else if(($value != 'N/A') && !array_key_exists($value, $attributeOptions)) {
                                                                                    $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                                                }
                                                                            },
                                                                    ]:'',

                'healthsize' => (
                    (($row['primarycategory']== 'Organic') || ($row['primarycategory']=='Health & Safety'))
                    && $row['type']=='variant')?[
                                                                'required',
                                                                function ($attribute, $value, $fail) use($attributeOptions) {
                                                                    if($value == 'N/A'){
                                                                        $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                                    }
                                                                    else if(($value != 'N/A') && !array_key_exists($value, $attributeOptions)) {
                                                                        $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                                    }
                                                                },     
                                                        ]:'',

                'types' => ($row['primarycategory']== 'Books & Art' && $row['type']=='variant')?[
                                                                        'required',
                                                                        function ($attribute, $value, $fail) use($attributeOptions) {
                                                                            if($value == 'N/A'){
                                                                                $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                                            }
                                                                            else if(($value != 'N/A') && !array_key_exists($value, $attributeOptions)) {
                                                                                $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                                            }
                                                                        },     
                                                                ]:'',
                //Secondary Attribute validation Started
                'clothingsize' => ($row['primarycategory']=='Clothing')?[
                                                                    'required',
                                                                    function ($attribute, $value, $fail) use($attributeOptions) {
                                                                        if(($value != 'N/A') && !array_key_exists($value, $attributeOptions)) {
                                                                            $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                                        }
                                                                    },
                                                                ]:'',

                'age' => (($row['primarycategory']== 'Accessories') || ($row['primarycategory']== 'Toys') || 
                    ($row['primarycategory']== 'Personal Care') || ($row['primarycategory']== 'Books & Art'))?[
                                                                                'required',
                                                                                function ($attribute, $value, $fail) use($attributeOptions) {
                                                                                    if(($value != 'N/A') && !array_key_exists($value, $attributeOptions)) {
                                                                                        $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                                                    }
                                                                                },     
                                                                        ]:'',
    
                'footwearsize' => ($row['primarycategory']== ('Footwear'))?['required',
                                                                            function ($attribute, $value, $fail) use($attributeOptions) {
                                                                                if(($value != 'N/A') && !array_key_exists($value, $attributeOptions)) {
                                                                                    $fail("Attribute value as <strong>".$value."</strong> not exist.");
                                                                                }
                                                                            },     
                                                                        ]:'',
               
                'isbn' => ($row['type']=='variant' && $row['primarycategory']=='Books & Art')?['required', 'regex:/^(?=(?:\D*\d){10}(?:(?:\D*\d){3})?$)[\d-]+$/', 'unique:product_variants,isbn_code']:'',
                'categories' => [
                                    'required',
                                    function ($attribute, $value, $fail) use($childCategories) {
                                        $categories = explode(';', $value);
                                        foreach($categories as $category) {
                                            // Check category title valid or not
                                            if(!array_key_exists($category, $childCategories)) {
                                                $fail("Category as <strong>".$category."</strong> not exist.");
                                            }
                                        }
                                    },
                                ],
                'countryoforigin' => [
                                'required',
                                function ($attribute, $value, $fail) use($countries) {
                                    // Check country exist or not
                                    if(!array_key_exists($value, $countries)) {
                                        $fail("Country name as <strong>".$value."</strong> not exist.");
                                    }
                                },
                            ],
                'clothingmaterial' => ['max:255'],
                'washcareinstructions' => ['max:500',
                                // function ($attribute, $value, $fail) use($washingTypes) {
                                //     $washTypes = explode(',', $value);
                                //     // Check washingType exist or not
                                //     foreach ($washTypes as $wTypes) {
                                //         if (!empty($value) && !array_key_exists($wTypes, $washingTypes)) {
                                //             $fail("Washing Type as <strong>".$wTypes."</strong> not exist.");
                                //         }
                                //     }
                                // },
                            ],
                'ironinstructions' => [ 'max:500',
                                // function ($attribute, $value, $fail) use($ironTypes) {
                                //     // Check ironType exist or not
                                //     if(!empty($value) && !array_key_exists($value, $ironTypes)) {
                                //         $fail("Iron Type as <strong>".$value."</strong> not exist.");
                                //     }
                                // },
                            ],
                'sizechart' => ($row['primarycategory']=='Clothing')?[
                                function ($attribute, $value, $fail) use($sizeChart) {
                                    // Check sizechart exist or not
                                    if(!empty($value) && !array_key_exists($value, $sizeChart)) {
                                        $fail("Size Chart as <strong>".$value."</strong> not exist.");
                                    }
                                },
                            ]:'',
                'mrp' => 'required|numeric',
                'noofitemsinpackage' => 'required|numeric',
                'itemdetailsinpackage' => 'required',
                'sellingprice' => 'required|regex:/^\d+(\.\d{1,2})?$/|lte:mrp',
                'tax' => 'required|numeric|between:0,18',
                'unit' => ($row['type'] == 'configurable') ? ['required', 'alpha'] :'',
                'featured' => ['required', Rule::in(['yes', 'no'])],
                'stock' => ($row['type'] == 'variant') ? ['required', 'numeric'] :'',
                'minimumshippingdays' => 'required|numeric',      
                'shippingweight' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'shippinglength' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'shippingbreadth' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'shippingheight' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                // 'primaryattributevalue' => [
                //                             'required',
                //                             function ($attribute, $value, $fail) use($attributeOptions) {
                //                                 if(($value != 'N/A') && !array_key_exists($value, $attributeOptions)) {
                //                                     $fail("Attribute value as <strong>".$value."</strong> not exist.");
                //                                 }
                //                             },
                //                         ],
                // 'secondaryattributevalue' => [
                //                             'required',
                //                             function ($attribute, $value, $fail) use($attributeOptions) {
                //                                 if(($value != 'N/A') && !array_key_exists($value, $attributeOptions)) {
                //                                     $fail("Attribute value as <strong>".$value."</strong> not exist.");
                //                                 }
                //                             },
                //                         ],
            ];

            $validator = Validator::make($row, $rules, []);

            // $validator->after(function ($validator) {
            //     if ($this->somethingElseIsInvalid()) {
            //         $validator->errors()->add(
            //             'field', 'Something is wrong with this field!'
            //         );
            //     }
            // });

            if ($validator->fails()) {
                $rowIndex = $key+1;
                // $this->errors[$key+1] = $validator->errors();
                // dd($validator->errors());
                foreach ($validator->errors()->messages() as $field=>$messages) {
                    // $this->errors[$rowIndex][$field] = $messages;
                    $this->errors[] = (object) [
                        'row' => $rowIndex,
                        'column' => $field,
                        'errors' => $messages
                    ];
                    // foreach ($messages as $error) {
                    //     // accumulating errors:
                    //     // $this->errors[$key+1][$field][] = $error;
                    //     $this->errors[$key+1][$field] = $error;
                    // }
                }
            }
        }

        // if($this->getErrors()) {
        //     throw new \Exception($e, $this->errors);
        //     // dd($this->errors);
        //     // return redirect()->route('seller.products.import')->with('failures', $this->errors);
        // }

        if(empty($this->getErrors())) {
            foreach ($rows as $row) {
                // Primary attribute for current category
                $primaryAttrId = $catPrimaryAttr[$row['primarycategory']];

                // Secondary attribute for current category
                $secondaryAttrId = $catSecondaryAttr[$row['primarycategory']];
                
                /*
                    ** Primary Attribute Value checking
                */
                $primAttrOptId = '';
                $secAttrOptId = '';
                if($row['primarycategory'] == 'Clothing' || 
                    $row['primarycategory'] == 'Toys' ||
                    $row['primarycategory'] == 'Footwear' ||
                    $row['primarycategory'] == 'Accessories'){

                    if(!is_null($row['color']) && $row['color']!='N/A') {
                        // get primary attribute option id
                        $primAttrOptId = $attrOptions[$primaryAttrId.'-'.$row['color']];
                    }
                }
                if($row['primarycategory'] == 'Sports'){

                    if(!is_null($row['sportstype']) && $row['sportstype']!='N/A') {
                        // get primary attribute option id
                        $primAttrOptId = $attrOptions[$primaryAttrId.'-'.$row['sportstype']];
                    }
                }

                if($row['primarycategory'] == 'Personal Care' || $row['primarycategory'] == 'Food & Nutrition'){

                    if(!is_null($row['quantity']) && $row['quantity']!='N/A') {
                        // get primary attribute option id
                        $primAttrOptId = $attrOptions[$primaryAttrId.'-'.$row['quantity']];
                    }
                }

                if($row['primarycategory'] == 'Organic' || $row['primarycategory'] == 'Health & Safety'){

                    if(!is_null($row['healthsize']) && $row['healthsize']!='N/A') {
                        // get primary attribute option id
                        $primAttrOptId = $attrOptions[$primaryAttrId.'-'.$row['healthsize']];
                    }
                }

                if($row['primarycategory'] == 'Books & Art'){
                    if(!is_null($row['types']) && $row['types']!='N/A') {
                        // get primary attribute option id
                        $primAttrOptId = $attrOptions[$primaryAttrId.'-'.$row['types']];
                    }
                }

                /*
                    ** Secondary Attribute Value checking
                */

                if ($row['primarycategory'] == 'Clothing') { 
                    if (!is_null($row['clothingsize']) && $row['clothingsize']!='N/A') {
                        // get secondary attribute option id
                        $secAttrOptId = $attrOptions[$secondaryAttrId.'-'.$row['clothingsize']];
                    }
                }

                if($row['primarycategory'] == 'Books & Art' || $row['primarycategory'] == 'Accessories'
                || $row['primarycategory'] == 'Toys' || $row['primarycategory'] ==  'Personal Care'){

                    if (!is_null($row['age']) && $row['age']!='N/A') {
                        // get secondary attribute option id
                        $secAttrOptId = $attrOptions[$secondaryAttrId.'-'.$row['age']];
                    }

                }

                if($row['primarycategory'] == 'Footwear'){

                    if (!is_null($row['footwearsize']) && $row['footwearsize']!='N/A') {
                        // get secondary attribute option id
                        $secAttrOptId = $attrOptions[$secondaryAttrId.'-'.$row['footwearsize']];
                    }
                    
                }

                if($row['type'] == 'configurable') {
                    $product = new Product();
                    $product->sku = $row['sku'];
                    $product->name = $row['name'];
                    $product->slug = __generateSlug($row['name'].$row['sku']);
                    $product->unique_id = __generateProductSerial();
                    $product->description = str_replace("\n","<br>",$row['description']);
                    $product->brand_id = $brands[$row['brand']];
                    $product->primary_category = $categories[$row['primarycategory']];
                    $product->hsn_code = $row['hsn'];
                    $product->min_stock_quantity = $row['minimumstockquantity'];

                    if ($row['primarycategory'] == 'Clothing') {
                        $product->iron_type = $row['ironinstructions'];
                        $product->washing_type = $row['washcareinstructions'];
                        $product->dress_material = $row['clothingmaterial'];
                        if ($row['sizechart'] != "N/A" && !empty($row['sizechart']) ) {
                            $product->size_chart_id = $sizeChart[$row['sizechart']]; 
                        }
                    } else if($row['primarycategory'] == 'Footwear') {
                        $product->iron_type = null;
                        $product->washing_type = null;
                        $product->dress_material = null;
                        if ($row['sizechart'] != "N/A" && !empty($row['sizechart']) ) {
                            $product->size_chart_id = $sizeChart[$row['sizechart']]; 
                        }
                    } else {
                        $product->iron_type = null;
                        $product->washing_type = null;
                        $product->dress_material = null;
                        $product->size_chart_id = null;
                    }

                    $product->seller_id = $this->seller_id;
                    // $product->weight = $row['weight'];
                    // $product->length = $row['length'];
                    // $product->breadth = $row['breadth'];
                    // $product->height = $row['height'];
                    $product->weight = $row['shippingweight'];
                    $product->length = $row['shippinglength'];
                    $product->breadth = $row['shippingbreadth'];
                    $product->height = $row['shippingheight'];
                    $product->unit = $row['unit'];
                    $product->min_ship_hours = $row['minimumshippingdays'];
                    // $product->min_ship_hours = $row['mininimumshippinghours'];
                    // $product->return_avbl = $yesNo[$row['returnable']];
                    // $product->cancel_avl = $yesNo[$row['cancelable']];
                    // $product->refund_avl = $yesNo[$row['refundable']];
                    $product->return_avbl = 1;
                    $product->cancel_avl = 1;
                    $product->refund_avl = 1;
                    $product->seller_featured = $yesNo[$row['featured']];
                    // $product->fabpik_featured = intval($request->featured_fabpik);
                    $product->no_of_items = $row['noofitemsinpackage'];
                    // $product->items_in_package = $row['itemsinpackage'];
                    $product->items_in_package = $row['itemdetailsinpackage'];
                    $product->country_of_origin = $countries[$row['countryoforigin']];
                    $product->description = $row['description'];
                    $product->mrp = $row['mrp'];
                    $product->sell_price = $row['sellingprice'];
                    $product->seller_discount = ($row['mrp'] - $row['sellingprice']);

                    $product->fabpik_seller_price = $row['sellingprice'];
                    $product->fabpik_seller_discount = ($row['mrp'] - $row['sellingprice']);
                    $product->fabpik_seller_discount_percentage = 100*($row['mrp'] - $row['sellingprice'])/$row['mrp'];
                    
                    // if($sellerDetails != null){
                    //     if ($sellerDetails->commission_type == 'f') {
                    //         $product->seller_discount = 0;
                    //         $product->sell_price = $row['mrp'];
                    //     }else{
                    //         $product->seller_discount = ($row['mrp'] - $row['sellingprice']);
                    //         $product->sell_price = $row['sellingprice'];
                    //     }
                    // }
                   
                    $product->tax = $row['tax'];
                    // $product->meta_title = $request->product_meta_title;
                    // $product->meta_keywords = $request->meta_keywords;
                    // $product->meta_description = $request->product_meta_description;
                    
                    // product default images
                    // $images = explode(',', $row['images']);
                    $newNames = str_replace(" ","-",$row['images']);
                    $images = explode(',', $newNames);
                    foreach($images as $i=>$img){
                        // $images[$i] = $seller_code.'/'.$images[$i];
                        $images[$i] = $seller_code.'/products'.DIRECTORY_SEPARATOR.$images[$i];
                    }
                    $product->images = json_encode($images);
                    $product->thumbnail = array_search($row['thumbnail'], $images) + 1;

                    $product->save();

                    $productId = $product->id;
                    $productUniqueId = $product->unique_id;

                    $prdCategories = explode(';', $row['categories']); 
                    foreach($prdCategories as $category) {
                        @list($cId, $scId, $ccId) = explode('-', $childCategories[$category]);

                        $prdCategory = new ProductCategory();
                        $prdCategory->product_id = $productId;
                        $prdCategory->category_id = $cId;
                        $prdCategory->subcategory_id = $scId;
                        $prdCategory->childcategory_id = $ccId;
                        $prdCategory->save();
                    }
                }

                if($row['type'] == 'variant') {
                    // check images are there for this product'sprimary attribute
                    $isExist = VariationImage::where(['product_id'=>$productId, 'attribute_option_id'=>$primAttrOptId])->exists();
                    if(!$isExist) {
                        // $images = explode(',', $row['images']);
                        $newNames = str_replace(" ","-",$row['images']);
                        $images = explode(',', $newNames);
                        $thumbnail_img_number = (array_search($row['thumbnail'], $images)+1);
                        
                        foreach($images as $i=>$img){
                            // $images[$i] = $seller_code.'/'.$images[$i];
                            $images[$i] = $seller_code.'/products'.DIRECTORY_SEPARATOR.$images[$i];
                        }
                        if(count($images) < 6) {
                            $addMore = 6 - count($images);

                            for ($i=0; $i < $addMore; $i++) { 
                                array_push($images, null);
                            }
                        }

                        $variantImages = new VariationImage();
                        $variantImages->product_id = $productId;
                        $variantImages->attribute_option_id = $primAttrOptId;
                        $variantImages->thumbnail = $thumbnail_img_number;
                        $variantImages->thumbnail_img_name =  $images[$thumbnail_img_number-1];
                        $variantImages->images = json_encode($images);
                        $variantImages->save();
                    }

                    // insert variation data
                    $prdVariant = new ProductVariant();
                    $prdVariant->product_id = $productId;
                    $prdVariant->sku = $row['sku'];
                    // $prdVariant->hsn_code = $row['hsn'];
                    if ($row['primarycategory'] == 'Books & Art') {
                        $prdVariant->isbn_code = $row['isbn'];
                    }
                    // $prdVariant->unique_id = __generateProductVariantSerial();
                    $prdVariant->unique_id = __generateNewProductVariantSerial($productId, $productUniqueId);
                    $prdVariant->name = $row['name'];
                    $prdVariant->slug = __generateSlug($row['name'].$row['sku']);
                    // $prdVariant->description = $row['description'];
                    $prdVariant->description = nl2br($row['description']);
                    $prdVariant->mrp = $row['mrp'];
                    // if($sellerDetails != null){
                    //     if ($sellerDetails->commission_type == 'f') {
                    //         $product->price = $row['mrp'];
                    //         $product->discount = 0;
                    //     }else{
                    //         $product->price = $row['sellingprice'];
                    //         $product->discount = ($row['mrp'] - $row['sellingprice']);
                    //     }
                    // }
                    $prdVariant->price = $row['sellingprice'];
                    $prdVariant->discount = ($row['mrp'] - $row['sellingprice']);

                    $prdVariant->fabpik_seller_price = $row['sellingprice'];
                    $prdVariant->fabpik_seller_discount = ($row['mrp'] - $row['sellingprice']);
                    $prdVariant->fabpik_seller_discount_percentage = 100*($row['mrp'] - $row['sellingprice'])/$row['mrp'];

                    $prdVariant->stock = $row['stock'];
                    $prdVariant->min_stock_quantity = $row['minimumstockquantity'];
                    // $prdVariant->min_order_qty = $request->variant_min_quantity;
                    // $prdVariant->min_ship_hours = $row['mininimumshippinghours'];
                    $prdVariant->min_ship_hours = $row['minimumshippingdays'];
                    $prdVariant->shipping_weight = $row['shippingweight'];
                    $prdVariant->shipping_length = $row['shippinglength'];
                    $prdVariant->shipping_breadth = $row['shippingbreadth'];
                    $prdVariant->shipping_height = $row['shippingheight'];
                    // $prdVariant->is_default = intval($request->is_default);
                    $prdVariant->save();

                    $tblData = []; 
                    // insert variation attribute data
                    $tblData[] = [
                        'product_variant_id' => $prdVariant->id,
                        'attribute_id' => $primaryAttrId,
                        'attribute_option_id' => $primAttrOptId,
                    ];

                    /*
                        *Store Secodary Attribute Options
                    */

                    if ($row['primarycategory'] == 'Clothing') {
                        if ( (!is_null($row['clothingsize']) && $row['clothingsize']!='N/A') ) {

                            $tblData[] = [
                                'product_variant_id' => $prdVariant->id,
                                'attribute_id' => $secondaryAttrId,
                                'attribute_option_id' => $secAttrOptId,
                            ];

                        }
                    }

                    if ($row['primarycategory'] == 'Footwear') {
                        if ( (!is_null($row['footwearsize']) && $row['footwearsize']!='N/A') ) {
                            $tblData[] = [
                                'product_variant_id' => $prdVariant->id,
                                'attribute_id' => $secondaryAttrId,
                                'attribute_option_id' => $secAttrOptId,
                            ];

                        }
                    }

                    if($row['primarycategory'] == 'Books & Art' || $row['primarycategory'] == 'Accessories'
                        || $row['primarycategory'] == 'Toys' || $row['primarycategory'] ==  'Personal Care'){

                        if ( (!is_null($row['age']) && $row['age']!='N/A') ) {
                            $tblData[] = [
                                'product_variant_id' => $prdVariant->id,
                                'attribute_id' => $secondaryAttrId,
                                'attribute_option_id' => $secAttrOptId,
                            ];

                        }
                    }

                    ProductVariantOption::insert($tblData);
                }
            }
        }
    }

    // this function returns all validation errors after import:
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function customValidationMessages()
    {
        return [
            '*.type.in' => "Type value needs to be either <strong>configurable or variant</strong>.",
            '*.sku.unique' => "SKU is already exist in database.",
        ];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            '*.sku' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
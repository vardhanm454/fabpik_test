<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Payment;
use DateTime;
use Razorpay\Api\Api;

class PaymentRefund extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'paymentrefund:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Payment refund if order not got created.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // get all payment where order id is null and payment id is not null
        // $date = new DateTime();
        // $date->modify('-24 hours');
        // $formatted_date = $date->format('Y-m-d H:i:s');
        // $payments = Payment::whereRaw("order_id IS NULL AND razorpay_payment_id IS NOT NULL AND (created_at BETWEEN '$date' AND '$formatted_date') AND payment_status_id = 1")
        //                     ->get();
        $payments = Payment::whereRaw("order_id IS NULL AND razorpay_payment_id IS NOT NULL AND (created_at > now() - interval 24 hour) AND payment_status_id = 1")
                            ->get();
        $api = new Api(RAZORPAY_KEY_ID, RAZORPAY_KEY_SECRET);
        foreach ($payments as $payment) {
            $refund = $api->refund->create(array('payment_id' => $payment->razorpay_payment_id));
            if($refund['id']){
                Payment::where('razorpay_payment_id','=',$payment->razorpay_payment_id)
                         ->update(['payment_status_id'=>4]);
            }
        }

    }
} 
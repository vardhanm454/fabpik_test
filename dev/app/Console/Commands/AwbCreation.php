<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\OrderDetail;

class AwbCreation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'awbcreation:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'AWB Creation Automatically based on the created Date and No.of Shipping dates.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = time();

        $orders = OrderDetail::with('productVarient')
                            ->whereNotNull('shipment_id')
                            ->whereNull('tracking_no')
                            ->where('order_status_id', 4)
                            ->where('shipping_status_id', 1)
                            ->get();

        foreach ($orders as $order) {
            $shipReadyTime = strtotime($order->created_at. ' + '.$order->productVarient->min_ship_hours.' days');

            \Log::info($now .' - '. $shipReadyTime);
            if($shipReadyTime <= $now){
                $shippingStatusResult = __changeSellerShippingStatus(2, [$order->id]);
                \Log::info('AWB Creattion Result');
                \Log::info('====================');
                \Log::info($shippingStatusResult);

                // $changeStatusResult = _changeStatus(10, [$order->id]);
                // \Log::info('shippingStatusResult');
                // \Log::info('====================');
                // \Log::info($shippingStatusResult);
            }
        }        
    }
}
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ConvertToWebp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convertto:webp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert images(jpeg/jpg/png) to webp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $folder = '100020';
        $this->listFolderFiles('uploads/'.$folder.'/products');
    }

    public function listFolderFiles($dir)
    {
        $ffs = scandir($dir);

        unset($ffs[array_search('.', $ffs, true)]);
        unset($ffs[array_search('..', $ffs, true)]);
        unset($ffs[array_search('.DS_Store', $ffs, true)]);
        unset($ffs[array_search('sizechart', $ffs, true)]);

        // prevent empty ordered elements
        if (count($ffs) < 1)
            return;

        echo '<ol>';
        foreach($ffs as $ff){
            if(is_dir($dir.'/'.$ff)) {
                $this->listFolderFiles($dir.'/'.$ff);
            } else {
                echo '<li>'.$dir.'/'.$ff.'</li>';
                echo '<li>'.$this->convert($dir.'/'.$ff).'</li>'; die;
            }
        }
        echo '</ol>';
    }

    public function convert($file)
    {
        $fileInfo = pathinfo($file);

        switch (strtolower($fileInfo['extension'])) {
            case 'jpg':
                $image =  imagecreatefromjpeg($file);
                break;

            case 'jpeg':
                $image =  imagecreatefromjpeg($file);
                break;

            case 'png':
                $image =  imagecreatefrompng($file);
                break;

            default:
                # code...
                break;
        }

        ob_start();
        imagejpeg($image,NULL,80);
        $cont =  ob_get_contents();
        ob_end_clean();
        imagedestroy($image);
        $content =  imagecreatefromstring($cont);

        $webpFile = $fileInfo['dirname'].'/'.$fileInfo['filename'].'.webp';
        imagewebp($content,$webpFile);
        imagedestroy($content);

        return $webpFile;
    }
}

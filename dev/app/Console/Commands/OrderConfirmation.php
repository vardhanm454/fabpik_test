<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\OrderDetail;
use DateTime;

class OrderConfirmation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orderconfirmation:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Confirmations: Order Confirmation from Seller Corn Job.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //get present date and time i.e., today at 3:00 PM
        $today = new DateTime();
        $today->modify('-15 hours');
        $formatted_date = $today->format('Y-m-d H:i:s');
        // \Log::info($formatted_date);

        //get all orders which are all pending state
        $getOrderDetails = OrderDetail::whereRaw("created_at < '$formatted_date' AND order_status_id=1")
                                        ->whereNull('shipment_id')
                                        ->whereNull('tracking_no')
                                        ->where('shipping_status_id', 1)
                                        ->get();
        // \Log::info($getOrderDetails);
        foreach($getOrderDetails as $orderDetail){
            // \Log::info($orderDetail);
            \Log::info($now .' - Order Confirmation Corn Log');

            $changeStatusResult = __changeStatus(4, [$orderDetail->id]);
            \Log::info('changeStatusResult');
            \Log::info('====================');
            \Log::info($changeStatusResult);

            $shippingStatusResult = __changeSellerShippingStatus(2, [$orderDetail->id]);
            \Log::info('shippingStatusResult');
            \Log::info('====================');
            \Log::info($shippingStatusResult);
        }
    }
}
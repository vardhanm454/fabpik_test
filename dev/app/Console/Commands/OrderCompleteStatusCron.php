<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\OrderDetail;
use DateTime;

class OrderCompleteStatusCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ordercompletestatus:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Order Complete Status Corn Job.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /*Get the all the order details, having the tracking details when,
        **shipping Status ID is Delivered, Order Status ID Delivered and
        **Payment Status ID is Paid
        */


        $orderDetails = OrderDetail::select('delivered_date', 'id')
                                        ->where('tracking_no', '<>',  null)
                                        ->whereIn('shipping_status_id', [5])
                                        ->whereIn('order_status_id', [8])
                                        ->whereIn('payment_status_id', [1])
                                        ->get();
        $today = date("Y-m-d");
        \Log::info($orderDetails);
        \Log::info("============");
        foreach($orderDetails as $details) {
            //get Date diff as intervals 
            $d1 = new DateTime($details->delivered_date);
            $d2 = new DateTime($today);
            $interval = $d1->diff($d2);
            $diffInDays = $interval->d;
            if($diffInDays >= 7){
                $ChangeStatus = __changeStatus($status = 9, [$details->id]);
                \Log::info('Order Completion tatus');
                \Log::info('======================');
                \Log::info($ChangeStatus);
            }
        }
    }
}
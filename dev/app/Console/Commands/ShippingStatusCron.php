<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\OrderDetail;

class ShippingStatusCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shippingstatus:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Shipping Status Corn Job.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Example AWB Tracking Numbers : 1904014350301 (Delivered), 781707597070 (In-Transit)
        // \Log::info(__getShipRocketOrderStatus(781707597070));
        // $shippingStatus = __changeShipRocketShippingStatus(__getShipRocketOrderStatus(781707597070), [1]);
        //     \Log::info($shippingStatus);

        //Get the all the order details, having the tracking details when shipping Status Id  

        $orderDetails = OrderDetail::select('tracking_no', 'id')
                                        ->where('tracking_no', '<>',  null)
                                        ->whereIn('shipping_status_id', [2, 3, 4, 6])->get();
        \Log::info('orderDetails');
        \Log::info('==================');
        \Log::info($orderDetails);

        foreach($orderDetails as $details) {
            \Log::info($details->tracking_no);

            $shiprocketResponse = __getShipRocketOrderStatus($details->tracking_no);

            \Log::info('shiprocketResponse');
            \Log::info('==================');
            \Log::info($shiprocketResponse);
            \Log::info('==================');

                        
            $shippingStatus = __changeShipRocketShippingStatus($shiprocketResponse, [$details->id]);
            \Log::info("shippingStatusResult");
            \Log::info('==================');
            \Log::info($shippingStatus);
            \Log::info('==================');
        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\FCMToken;
use App\Models\Notifications;
use App\Models\User;
use DB;

class TriggerNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:trigger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger the notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $notifications = Notifications::selectRaw('id, title, content')->whereRaw("notify_on < NOW() AND notification_sent_web_push = 'n'")->get();
        foreach ($notifications as $notification) {
            if($notification->users == null && $notification->notified_to == 'c'){
                $fcm_tokens = User::join("fcm_tokens as ft","ft.user_id",'=','users.id')
                                    ->whereRaw("users.user_type = 'customer'")
                                    ->pluck("ft.fcm_token");
                $this->triggerFCMNotificationApi($fcm_tokens,$notification->title,$notification->content);
                Notifications::where("id","=",$notification->id)
                               ->update(["notification_sent_web_push"=>'y']);
                
            }else if($notification->users == null && $notification->notified_to == 's'){
                $fcm_tokens = User::join("fcm_tokens as ft","ft.user_id",'=','users.id')
                                    ->whereRaw("users.user_type = 'seller'")
                                    ->pluck("ft.fcm_token");
                $this->triggerFCMNotificationApi($fcm_tokens,$notification->title,$notification->content);
                Notifications::where("id","=",$notification->id)
                               ->update(["notification_sent_web_push"=>'y']);
            }else{
                $notification_id = $notification->id;
                $sql = "SELECT
                        t2.fcm_token
                        FROM
                            `notifications` AS `t1`
                        JOIN `fcm_tokens` AS `t2`
                        ON
                            JSON_SEARCH(t1.users, 'one', t2.user_id)
                        WHERE t1.notify_on < NOW() AND t1.deleted_at IS null AND t1.id = $notification_id";
                $tokens = DB::select($sql);
                
                $fcm_tokens = [];
                foreach($tokens as $token){
                    $fcm_tokens[] = $token->fcm_token;
                }
                $this->triggerFCMNotificationApi($fcm_tokens,$notification->title,$notification->content);
                Notifications::where("id","=",$notification->id)
                               ->update(["notification_sent_web_push"=>'y']);
            }
        }
        // $sql = "SELECT
        //             t2.fcm_token, t1.id
        //         FROM
        //             `notifications` AS `t1`
        //         JOIN `fcm_tokens` AS `t2`
        //         ON
        //             JSON_SEARCH(t1.users, 'one', t2.user_id)
        //         WHERE t1.notify_on < NOW() AND t1.deleted_at IS null";

        // $tokens = DB::select($sql);

        // //Notification
       

        // // $tokens = Notifications::join(DB::raw("`fcm_tokens` AS `t2` ON JSON_SEARCH(t1.users, 'one', t2.user_id)"))
        // //                         ->whereRaw("notify_on < NOW() AND deleted_at IS null")->pluck('t2.fcm_token');

        // $toTokens = array(); 
        // $tempArray = array();
        // foreach($notifications as $notify){
        //     foreach ($tokens as $token) {
        //         if ($notify->id == $token->id) {
        //             $toTokens[] = $token->fcm_token;
        //         }
        //     }
        //     $notify->fcm_tokens = $toTokens;
        //     $toTokens = array();
        //     $tempArray[] = $notify;
        // }

        // \Log::info("Retrived Tokens");
        // \Log::info("==============");
        // \Log::info($tempArray);
        // \Log::info("==============");

        // if($tokens){
        //     foreach($tempArray as $notifyTo){
        //         $data = [ 
        //             "notification" => [
        //                 "title" => $notifyTo->title, /* The notification's title */
        //                 "body" => $notifyTo->content, /* The notification's body text */
        //                 "sound" => "default", /* The sound to play when the device receives the notification */
        //                 "click_action"=>"FCM_PLUGIN_ACTIVITY",
        //                 "icon"=>"fcm_push_icon"
        //             ],
        //             "data" => [
        //                 "landing_page" => "login"
        //             ],
        //             // "to" =>'cdceymfC06UiJQvF4Df0CW:APA91bFh4ohjPqAOaq7-ALakgHRnEqKG334N8nzdY8NlktXhXwc8UaVrnhpGFXK2GGICQXMT2mYna_vgl7AdWI85lH3r-gLAqVzhliv2w_slG3qJhrHE6xgY7AfPUFSc99GZxnKGi9M4',
        //             "registration_ids"=>$notifyTo->fcm_tokens,
        //             "priority" => "high",
        //             "restricted_package_name"=>""
        //         ];
            
        //         $dataString = json_encode($data);
            
        //         $headers = [
        //             'Authorization: key=' . "AAAAiiTOZZo:APA91bFjFNbnC82eQjseTewsDHXIiFtVu0TzpnMryLr2uQffbfBGDn4UiuA17M6AK69ddnfc4M40mwLo3X5jGR3_87n3HT2E-RptJbd5xQdQjUgtO0BDWtTRXtKzDw41F1XVMz0KGCvM",
        //             'Content-Type: application/json',
        //         ];
        
        //         $ch = curl_init();
        
        //         curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        //         curl_setopt($ch, CURLOPT_POST, true);
        //         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //         curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        
        //         $result = curl_exec($ch);
        //     }
           
        // }

        
    }

    public function triggerFCMNotificationApi($tokens,$title,$content){
        \Log::info($tokens);
        $data = [ 
            "notification" => [
                "title" => $title, /* The notification's title */
                "body" => $content, /* The notification's body text */
                "sound" => "default", /* The sound to play when the device receives the notification */
                "click_action"=>"FCM_PLUGIN_ACTIVITY",
                "icon"=>"fcm_push_icon"
            ],
            "data" => [
                "landing_page" => "login"
            ],
            // "to" =>'fKXJatAaJVBAFCanHG4PbU:APA91bH87nwgWbpW3-jhoT4HI7cVYXsfyJL1bAmdSSPOwh9l6wyXUYtjMf-YeUjS2UD8tVNtQdR0qudE_UHVq4W8lh8XSKG8WC41uC_JFTMPGotXMKCx6y_MG-7T-HS4vIuPtwRlmKCR',
            "registration_ids"=>$tokens,
            "priority" => "high",
            "restricted_package_name"=>""
        ];
    
        $dataString = json_encode($data);
    
        $headers = [
            'Authorization: key=AAAAkBJ9yKg:APA91bFiJlNGDkFdLOCK7FX5c6TKAJZndNe0TqYV93kYvNS2IRI7AtpEiaw4-c1DpHxvazxTmuCttMemMwrGdjg2YAQtK1VniuNHN_qhAgoMgeRrQPMkmZun7SKYI7HEbCdZhQA9loPV',
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   
        curl_exec($ch);
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DeleteProductImportFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'productimport:deletefiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all those import files whos\' creation time is older than 1 hour';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("Delete Product Import Files work started!");

        $files = \File::files('uploads/imports');
        foreach ($files as $file) {
            if(time() - filemtime($file) > 24 * 3600 
                && $file->getFilename() != 'sample-product-import.xlsx'
                && $file->getFilename() != 'sample-product-accessories-import.xlsx'
                && $file->getFilename() != 'sample-product-books-art-import.xlsx'
                && $file->getFilename() != 'sample-product-clothing-import.xlsx'
                && $file->getFilename() != 'sample-product-food-nutrition-import.xlsx'
                && $file->getFilename() != 'sample-product-footware-import.xlsx'
                && $file->getFilename() != 'sample-product-health-safety-import.xlsx'
                && $file->getFilename() != 'sample-product-organic-import.xlsx'
                && $file->getFilename() != 'sample-product-personal-care-import.xlsx'
                && $file->getFilename() != 'sample-product-sports-import    .xlsx'
                && $file->getFilename() != 'sample-product-toys-import' ) {
                @unlink($file);
            }
        }

        $files = \File::files('storage/framework/laravel-excel');
        foreach ($files as $file) {
            if(time() - filemtime($file) > 24 * 3600) {
                @unlink($file);
            }
        }

        \Log::info("Delete Product Import Files work finished!");
    }
}

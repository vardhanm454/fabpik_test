<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\TestCron::class,
        Commands\DeleteProductImportFiles::class,
        Commands\ShippingStatusCron::class,
        Commands\OrderCompleteStatusCron::class,
        Commands\PaymentRefund::class,
        // Commands\OrderConfirmation::class,
        // Commands\AwbCreation::class,
        Commands\TriggerNotifications::class,
        Commands\ConvertToWebp::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->everyMinute();
        // $schedule->command('cron:test')->everyMinute();
        $schedule->command('productimport:deletefiles')->dailyAt('2:00');
        $schedule->command('shippingstatus:cron')->everyFifteenMinutes();
        $schedule->command('ordercompletestatus:cron')->hourly();
        $schedule->command('paymentrefund:cron')->daily();
        // $schedule->command('orderconfirmation:cron')->dailyAt('3:00');
        // $schedule->command('awbcreation:cron')->dailyAt('3:00');
        $schedule->command('notifications:trigger')->hourly();
        // $schedule->command('convertto:webp')->dailyAt('1:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

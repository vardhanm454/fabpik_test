<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class VariationImage extends Model
{
    use SoftDeletes;

    protected $table        = 'variation_images';
    protected $primaryKey   = 'id';

    protected $fillable = ['product_id', 'attribute_option_id', 'thumbnail', 'images'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TermsAndCondtion extends Model
{
    use SoftDeletes;

	protected $table        = 'terms_conditions';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='desc')
    {
    	$termsandcondtions = TermsAndCondtion::orderBy($orderColumn, $orderDir);
        return $termsandcondtions->paginate(intval($criteria->length), ['id','name','display_name','status'], 'page', $page);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommissionUpdateLog extends Model
{
    use SoftDeletes;

    protected $table        = 'commission_update_logs';
    protected $primaryKey   = 'id';
}
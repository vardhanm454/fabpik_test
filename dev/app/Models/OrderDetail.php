<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    // use SoftDeletes;

	protected $table        = 'order_details';
    protected $primaryKey   = 'id';

    public function seller()
    {
        return $this->belongsTo('App\Models\Seller', 'seller_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

    public function productVarient()
    {
        return $this->belongsTo('App\Models\ProductVariant', 'product_variant_id', 'id');
    }

    public function orderHistory()
    {
        return $this->hasMany('App\Models\OrderHistory', 'order_detail_id', 'id');
    }

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
        $orderDetails = OrderDetail::with('seller', 'order', 'productVarient')->orderBy($orderColumn, $orderDir);
        
        if (isset($criteria->seller))  
            $orderDetails = $orderDetails->where('seller_id', $criteria->seller);
        if (isset($criteria->fchildid)) 
            $orderDetails = $orderDetails->where('child_order_id', 'LIKE', "%{$criteria->fchildid}%");
        if (isset($criteria->fparentid)) 
            $orderDetails = $orderDetails->whereHas('order', function($q) use($criteria){
                                                                $q->where('parent_order_id', 'LIKE', "%{$criteria->fparentid}%");
                                                            });
        if (isset($criteria->fcustomername)) 
            $orderDetails = $orderDetails->whereHas('order', function($q) use($criteria){
                                                $q->where('first_name', 'LIKE', "%{$criteria->fcustomername}%")
                                                    ->where('last_name', 'LIKE', "%{$criteria->fcustomername}%");
                                            });

        if (isset($criteria->fcustomermobile)) 
                $orderDetails = $orderDetails->whereHas('order', function($q) use($criteria){
                                                            $q->where('mobile', 'LIKE', "%{$criteria->fcustomermobile}%");
                                                        });

        if (isset($criteria->fcustomeremail)) 
            $orderDetails = $orderDetails->whereHas('order', function($q) use($criteria){
                                                        $q->where('email', 'LIKE', "%{$criteria->fcustomeremail}%");
                                                    });

        if (isset($criteria->fproductsku)) 
                $orderDetails = $orderDetails->whereHas('productVarient', function($q) use($criteria){
                                                            $q->where('sku', 'LIKE', "%{$criteria->fproductsku}%");
                                                        });

        if(!is_null($criteria->ffromdate) && !is_null($criteria->ftodate)){
            if($criteria->ffromdate == $criteria->ftodate){
                $orderDetails = $orderDetails->where('created_at','>=', $criteria->ffromdate.' 00:00:00');
            }else{
                $orderDetails = $orderDetails->whereBetween('created_at',[ $criteria->ffromdate.' 00:00:00', $criteria->ftodate.' 23:59:59' ]);
            }
        } else if(!is_null($criteria->ffromdate)){
            $orderDetails = $orderDetails->where('created_at','>=', $criteria->ffromdate.' 00:00:00');
        } else if(!is_null($criteria->ftodate)){
            $orderDetails = $orderDetails->where('created_at','<=', $criteria->ftodate.' 23:59:59');
        }
        
        if(!is_null($criteria->fostatus)) $orderDetails = $orderDetails->where('order_status_id', $criteria->fostatus);
        if(isset($criteria->fseller) && !is_null($criteria->fseller)) $orderDetails = $orderDetails->where('seller_id', $criteria->fseller);

        /**
         * the below code is checking in OrderDetails table
         */
        
        if (isset($criteria->fmodel))
            $orderDetails = $orderDetails->where('shipping_model', $criteria->fmodel);
        
        if (isset($criteria->fproductname))
            $orderDetails = $orderDetails->where('name', 'LIKE', "%{$criteria->fproductname}%");

        if(isset($criteria->fminsellprice) && isset($criteria->fmaxsellprice)){
            if($criteria->fminsellprice == $criteria->fmaxsellprice){
                $orderDetails = $orderDetails->where('price','>=', $criteria->fminsellprice);
            }else{
                $orderDetails = $orderDetails->whereBetween('price',[ $criteria->fminsellprice, $criteria->fmaxsellprice ]);
            }
        } else if(isset($criteria->fminsellprice)){
            $orderDetails = $orderDetails->where('price','>=', $criteria->fminsellprice);
        } else if(isset($criteria->fmaxsellprice)){
            $orderDetails = $orderDetails->where('price','<=', $criteria->fmaxsellprice);
        }

        return $orderDetails->paginate(intval($criteria->length), ['id', 'created_at', 'seller_price', 'shipping_model', 'shipping_weight', 'shipping_length', 'shipping_breadth', 'shipping_height', 'commission', 'payout_status', 'shipping_charge', 'order_handling_charge', 'name', 'child_order_id', 'order_id', 'mrp', 'seller_id', 'price', 'discount', 'quantity', 'total', 'order_status_id', 'product_id', 'product_variant_id', 'shipping_status_id', 'payment_status_id', 'total_seller_discount', 'discount', 'deal_price', 'user_customization_text'], 'page', $page);
    }

    /*
        *
        *Shipping Status DataTable List
        *
    */
    public static function getAjaxShippingListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
        $orderDetails = OrderDetail::with('seller', 'order', 'productVarient')->where('order_status_id','>=', 4)->orderBy($orderColumn, $orderDir);
        if (isset($criteria->seller))  { $orderDetails = $orderDetails->where('seller_id', $criteria->seller); }
        if(!is_null($criteria->ffromdate)) $orderDetails = $orderDetails->where('created_at','>=', $criteria->ffromdate);
        if(!is_null($criteria->ftodate)) $orderDetails = $orderDetails->where('created_at','<=', $criteria->ftodate);
        if(!is_null($criteria->fsstatus)) $orderDetails = $orderDetails->where('shipping_status_id', $criteria->fsstatus);
        if(isset($criteria->fseller) && !is_null($criteria->fseller)) $orderDetails = $orderDetails->where('seller_id', $criteria->fseller);
        if(isset($criteria->fnumber) && !is_null($criteria->fnumber) ) $orderDetails = $orderDetails->where('tracking_no', 'LIKE', "%{$criteria->fnumber}%");
        if(isset($criteria->fcorderid) && !is_null($criteria->fcorderid) ) $orderDetails = $orderDetails->where('child_order_id', 'LIKE', "%{$criteria->fcorderid}%");

        if(isset($criteria->fmobile) && !is_null($criteria->fmobile)) $orderDetails = $orderDetails->whereHas('order', function($query) use($criteria){
                                                                                                                        $query->where('mobile', '=', $criteria->fmobile);
                                                                                                                    });
        if(isset($criteria->fsku) && !is_null($criteria->fsku)) $orderDetails = $orderDetails->whereHas('productVarient', function($query) use($criteria){
                                                                                                            $query->where('sku', '=', $criteria->fsku);
                                                                                                        });
        if(isset($criteria->fostatus) && !is_null($criteria->fostatus)) $orderDetails = $orderDetails->where('order_status_id', $criteria->fostatus);
        if(isset($criteria->ftrackid) && !is_null($criteria->ftrackid)) $orderDetails = $orderDetails->where('tracking_no', $criteria->ftrackid);

        if(isset($criteria->fproductname) && !is_null($criteria->fproductname)) $orderDetails = $orderDetails->where('name', 'LIKE', "%{$criteria->fproductname}%");
        if (isset($criteria->fproductsku) && !is_null($criteria->fproductsku)) {
            $orderDetails = $orderDetails->whereHas('productVarient', function($q) use($criteria) {
                                            $q->where('sku', 'LIKE', "%{$criteria->fproductsku}%");
            });
        }
        
        if(isset($criteria->fpincode) && !is_null($criteria->fpincode)){
            $orderDetails = $orderDetails->whereHas('order', function($q) use($criteria) {
                $q->where('shipping_pincode', 'LIKE', "%{$criteria->fpincode}%");
            });
        } 

        if(isset($criteria->fexcepteddeliverydate) && !is_null($criteria->fexcepteddeliverydate)) $orderDetails = $orderDetails->where('excepted_delivery_date', 'LIKE', "%{$criteria->fexcepteddeliverydate}%");
        if(isset($criteria->fdelivereddate) && !is_null($criteria->fdelivereddate)) $orderDetails = $orderDetails->where('delivered_date', 'LIKE', "%{$criteria->fdelivereddate}%");

        return $orderDetails->paginate(intval($criteria->length), ['id', 'order_id', 'commission', 'seller_id', 'price', 'child_order_id', 'manifest_url', 'mrp', 'quantity','delivered_date', 'child_order_id','total', 'order_status_id', 'product_id', 'shipment_id', 'product_variant_id', 'created_at', 'tracking_no','excepted_delivery_date', 'shipping_status_id', 'shipping_model',], 'page', $page);
    }

    /*
        *
        *Payment Status DataTable List
        *
    */
    public static function getAjaxPaymentsListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
        $orderDetails = OrderDetail::with('seller', 'order', 'productVarient')->orderBy($orderColumn, $orderDir);
        if (isset($criteria->seller))  { $orderDetails = $orderDetails->where('seller_id', $criteria->seller); }
        if(!is_null($criteria->ffromdate)) $orderDetails = $orderDetails->where('created_at','>=', $criteria->ffromdate);
        if(!is_null($criteria->ftodate)) $orderDetails = $orderDetails->where('created_at','<=', $criteria->ftodate);
        if(!is_null($criteria->fostatus)) $orderDetails = $orderDetails->where('order_status_id', $criteria->fostatus);
        if(isset($criteria->fseller) && !is_null($criteria->fseller)) $orderDetails = $orderDetails->where('seller_id', $criteria->fseller);
        // if(isset($criteria->fshippingstatus) && !is_null($criteria->fshippingstatus)) $orderDetails = $orderDetails->where('shipping_status_id', $criteria->fshippingstatus);
        // if(isset($criteria->fpaymentstatus) && !is_null($criteria->fpaymentstatus)) $orderDetails = $orderDetails->where('payment_status_id', $criteria->fpaymentstatus);
        // if(isset($criteria->forderstatus) && !is_null($criteria->forderstatus)) $orderDetails = $orderDetails->where('order_status_id', $criteria->forderstatus);
        if(isset($criteria->fpayoutdate) && !is_null($criteria->fpayoutdate)) $orderDetails = $orderDetails->where('payout_date_from_admin', $criteria->fpayoutdate);
        if(isset($criteria->forderid) && !is_null($criteria->forderid)) $orderDetails = $orderDetails->where('child_order_id', 'LIKE', "%{$criteria->forderid}%");
        if(isset($criteria->fpstatus) && !is_null($criteria->fpstatus)) $orderDetails = $orderDetails->where('payout_status', $criteria->fpstatus);
        
        if (isset($criteria->fpaymentstatus) && !is_null($criteria->fpaymentstatus)) {
            $orderDetails = $orderDetails->where('payment_status_id', $criteria->fpaymentstatus) ->where(function ($q) {
                $q->where('order_status_id', 8)->orWhere('order_status_id', 9);
            })->where('shipping_status_id', 5);
        }

        return $orderDetails->paginate(intval($criteria->length), ['id', 'order_id', 'commission', 'mrp', 'seller_price', 'seller_id', 'child_order_id', 'manifest_url', 'quantity','price','discount', 'total', 'total_seller_discount', 'order_status_id', 'created_at','product_id', 'product_variant_id', 'tracking_no','excepted_delivery_date', 'shipping_status_id','shipping_charge', 'shipping_charge_sgst','shipping_charge_cgst','sgst', 'cgst','commission','commission_sgst', 'commission_cgst', 'final_amount_to_pay_seller', 'payout_status', 'payout_date_from_admin' ], 'page', $page);
    }

    /*
        *
        *Self Shipping DataTable List
        *
    */
    public static function getAjaxSelfShippingListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
        
        $orders = OrderDetail::with('seller', 'order', 'productVarient')->whereHas('order',function($q){
            $q->where('payment_type','c');})->where('shipping_model','s')->orderBy($orderColumn, $orderDir);
            
        if (isset($criteria->seller))  { $orders = $orders->where('seller_id', $criteria->seller); }
        if(!is_null($criteria->ffromdate)) $orders = $orders->where('created_at','>=', $criteria->ffromdate);
        if(!is_null($criteria->ftodate)) $orders = $orders->where('created_at','<=', $criteria->ftodate);
        if(!is_null($criteria->fostatus)) $orders = $orders->where('order_status_id', $criteria->fostatus);
        if(isset($criteria->fseller) && !is_null($criteria->fseller)) $orders = $orders->where('seller_id', $criteria->fseller);
        // if(isset($criteria->fshippingstatus) && !is_null($criteria->fshippingstatus)) $orders = $orders->where('shipping_status_id', $criteria->fshippingstatus);
        // if(isset($criteria->fpaymentstatus) && !is_null($criteria->fpaymentstatus)) $orders = $orders->where('payment_status_id', $criteria->fpaymentstatus);
        // if(isset($criteria->forderstatus) && !is_null($criteria->forderstatus)) $orders = $orders->where('order_status_id', $criteria->forderstatus);
        if(isset($criteria->fpayoutdate) && !is_null($criteria->fpayoutdate)) $orders = $orders->where('payout_date_from_seller', $criteria->fpayoutdate);
        if(isset($criteria->forderid) && !is_null($criteria->forderid)) $orders = $orders->where('child_order_id', 'LIKE', "%{$criteria->forderid}%");
        if(isset($criteria->fpstatus) && !is_null($criteria->fpstatus)) $orders = $orders->where('payout_status', $criteria->fpstatus);
        // $orders = $orders->where('payment_status_id', 1) ->where(function($q) {
        //     $q->where('order_status_id', 8)->orWhere('order_status_id', 9);
        // })->where('shipping_status_id', 5);
        return $orders->paginate(intval($criteria->length), ['id', 'order_id', 'commission', 'mrp', 'seller_id', 'child_order_id', 'manifest_url', 'quantity','price','discount', 'total', 'total_seller_discount', 'order_status_id', 'created_at','product_id', 'product_variant_id', 'tracking_no','excepted_delivery_date', 'shipping_status_id','shipping_charge', 'shipping_charge_sgst','shipping_charge_cgst','sgst', 'cgst','commission','commission_sgst', 'commission_cgst','commission_igst', 'final_amount_to_pay_seller', 'payout_status', 'payout_date_from_admin', 'payout_date_from_seller', 'order_handling_charge','order_handling_charge_sgst', 'order_handling_charge_cgst', 'order_handling_charge_igst' ], 'page', $page);
    }

    public static function getShippingData($id)
    {
        $statusName = ShippingStatus::where('id', $id)->first();
        return $statusName;
    }

    public static function getOrderStatusData($id)
    {
        $orderStatusName = OrderStatus::where('id', $id)->first();
        return $orderStatusName;
    }

    public static function getPaymentStatusData($id)
    {
        $paymentStatusName = PaymentStatus::where('id', $id)->first();
        return $paymentStatusName;
    }

    /**
     * 
     * 
     * get attributes like size and color
     */
    public static function getAttributedetails($orderlist)
    {
        $i = 0;
            foreach($orderlist as $key => $product)
            {
                $product->date = date( 'M d Y', strtotime($product->created_at));
                $data = DB::table('product_variant_options AS pvo')
                            ->select([DB::raw('group_concat(DISTINCT ao.option_name ) as option_name'), DB::raw('group_concat(DISTINCT ao.colour_code ) as color_code') ,'pvo.product_variant_id','a.display_name'])
                            ->join('attribute_options AS ao','ao.id','pvo.attribute_option_id')
                            ->join('attributes AS a','a.id','ao.attribute_id')
                            ->where('pvo.product_variant_id','=' , $product->product_variant_id)
                            ->groupBy('a.display_name')
                            ->get();  
                            
                foreach($data as $key => $d) {
    
                    if($d->display_name == 'Size')
                    { 
                        $options = explode(",",$d->option_name);
                        $d->option_name = $options; 
                        $orderlist[$i]->variantsize = [$d];
                    }
    
                    if($d->display_name == 'Color')
                    {
                        $options = explode(",",$d->color_code);
                        $d->color_code = $options; 
                        $orderlist[$i]->variantcolor = [$d];
                    }
                }

                
                $i++;                     
            }
            return $orderlist;
    }
}

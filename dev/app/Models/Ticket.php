<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    //use SoftDeletes;

    protected $table        = 'help_desks';
    protected $primaryKey   = 'id';

     public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function comments()
    {
        return $this->hasMany('App\Models\TicketComment', 'help_desk_id', 'id');
    }

    public function seller()
    {
        return $this->belongsTo('App\Models\Seller', 'seller_id', 'id');
    }

    public static function getAjaxListData(
        $criteria=[], 
        $page=1, 
        $orderColumn='id', 
        $orderDir='asc')
    {
        $tickets = Ticket::with('seller')->orderBy($orderColumn, $orderDir);
        if (isset($criteria->seller))  { $tickets = $tickets->where('seller_id', $criteria->seller); }
        if($criteria->fname) $tickets = $tickets->where('issue_title', 'LIKE', "%{$criteria->fname}%");
        if($criteria->fstatus) $tickets = $tickets->where('status', $criteria->fstatus);
        // $tickets->where('seller_id',auth()->user()->ref_id);
        return $tickets->paginate(intval($criteria->length), ['id', 'ticket_no', 'created_at', 'updated_at', 'issue_title', 'seller_id', 'category', 'attachment', 'issue_status'], 'page', $page);
    }
}

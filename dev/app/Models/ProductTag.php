<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductTag extends Model
{
    use SoftDeletes;

	protected $table        = 'product_tags';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by','id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo('App\Models\User', 'modified_by','id');
    }

    public function assignedProductTags()
    {
        return $this->hasMany('App\Models\AssignedProductTag', 'tag_id','id');
    }

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='desc')
    {
        $tags = ProductTag::with('user', 'modifiedUser', 'assignedProductTags')->orderBy($orderColumn, $orderDir);
        
        if(!is_null($criteria->fname)) $tags = $tags->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $tags = $tags->where('status', $criteria->fstatus);
        
        return $tags->paginate(intval($criteria->length), ['id', 'name', 'created_by', 'modified_by', 'status', 'created_at', 'updated_at'], 'page', $page);
    }
}

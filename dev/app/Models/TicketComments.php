<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketComments extends Model
{
    //use SoftDeletes;

    protected $table        = 'help_desk_comments';
    protected $primaryKey   = 'id';

}

<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductVariant extends Model
{
    use SoftDeletes, HasFactory;

    protected $table        = 'product_variants';
    protected $primaryKey   = 'id';

    
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }

    public function wishlists(){
        return $this->belongsTo('App\Models\Wishlist','product_variant_id','id');
    }
    public function product_variant_options()
    {
        return $this->hasMany('App\Models\ProductVariantOption' ,'product_variant_id','id');
    }

    public static function getAjaxListVarientsData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='asc',
        $productId=0
    )
    {
        $productVariants = ProductVariant::with(['product'=> function($q){
            $q->whereNull('deleted_at');
        },'product.category', 'product.seller', 'product.productvariant','product.productcategory' => function($q){
            $q->where('is_active','=', 1);
        }])->where('product_id', $productId)->orderBy($orderColumn, $orderDir);
        if ($criteria->fname) {
            $productVariants = $productVariants->where('title', 'LIKE', "%{$criteria->fname}%");
        }
        return $productVariants->paginate(intval($criteria->length), ['id','unique_id','name','mrp','sku','discount','price','min_order_qty','stock'], 'page', $page);
    }

    public static function getAjaxChangePriceListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    )
    {
        $products = ProductVariant::with(['product'=> function($q){
            $q->whereNull('deleted_at');
        },'product.category', 'product.seller', 'product.productvariant','product.productcategory' => function($q){
            $q->where('is_active','=', 1);
        }])->orderBy($orderColumn, $orderDir);

        if (isset($criteria->fseller)) { $products = $products->whereHas('product', function($q) use ($criteria){
                                                            $q->where('seller_id', $criteria->fseller); 
                                                        });
        }
 
        if (isset($criteria->fbrand)) { $products = $products->whereHas('product', function($q) use ($criteria){
                                                            $q->where('brand_id', $criteria->fbrand); 
                                                        });
        }

        if (isset($criteria->fbrand)) { $products = $products->whereHas('product', function($q) use ($criteria){
                                                            $q->where('brand_id', $criteria->fbrand); 
                                                        });
        }

        if (isset($criteria->fcompanyname)) { $products = $products->whereHas('product.seller', function($q) use ($criteria){
                                                            $q->where('company_name', 'LIKE', "%{$criteria->fcompanyname}%");
                                                        });
        }

        if (isset($criteria->fsfeatured)) { $products = $products->whereHas('product', function($q) use ($criteria){
                                                            $q->where('seller_featured', $criteria->fsfeatured);
                                                        });
        }

        if (isset($criteria->ffeatured)) { $products = $products->whereHas('product', function($q) use ($criteria){
                                                            $q->where('fabpik_featured', $criteria->ffeatured);
                                                        });
        }

        if(isset($criteria->fdiscount)) $products = $products->where('discount', 'LIKE', "%{$criteria->fdiscount}%")
                                                        ->orWhere('discount', $criteria->fdiscount);
        if(isset($criteria->ffdiscount)) $products = $products->where('fabpik_seller_discount', 'LIKE', "%{$criteria->ffdiscount}%")
                                                        ->orWhere('fabpik_seller_discount', $criteria->ffdiscount);

        if(isset($criteria->fname)) $products = $products->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $products = $products->whereHas('product', function($q) use ($criteria){
                                                                $q->where('status', $criteria->fstatus); 
                                                            });
        
        if(isset($criteria->fpricefrom)) $products = $products->where('price','>=', $criteria->fpricefrom);
        if(isset($criteria->fpriceto)) $products = $products->where('price','<=', $criteria->fpriceto);

        if(isset($criteria->ffromdate)) $products = $products->where('created_at','>=', $criteria->ffromdate);
        if(isset($criteria->ftodate)) $products = $products->where('created_at','<=', $criteria->ftodate);

        if(isset($criteria->spsfromdate)) $products = $products->where('special_price_start_date','>=', $criteria->spsfromdate);
        if(isset($criteria->spstodate)) $products = $products->where('special_price_start_date','<=', $criteria->spstodate);

        if(isset($criteria->spefromdate)) $products = $products->where('special_price_end_date','>=', $criteria->spefromdate);
        if(isset($criteria->spetodate)) $products = $products->where('special_price_end_date','<=', $criteria->spetodate);

        if(isset($criteria->fsku)) $products = $products->where('sku', 'LIKE', "%{$criteria->fsku}%");
        if(isset($criteria->funiqueid)) $products = $products->where('unique_id', 'LIKE', "%{$criteria->funiqueid}%");
        
        if(!is_null($criteria->fcategory)) {
            @list($categoryId,$subCategoryId,$childCategoryId) = explode('-', $criteria->fcategory);
            $products =  $products->whereHas('product.productcategory', function ($q) use ($childCategoryId){
                $q->where('childcategory_id','=', $childCategoryId);
            });
        }

        // if(isset($criteria->fcommissiontype) && !is_null($criteria->fcommissiontype)) {
        //     $commissionType = $criteria->fcommissiontype;
        //     $products =  $products->whereHas('product.seller', function ($q) use ($commissionType){
        //         $q->where('commission_type','=', $commissionType);
        //     });
        // }

        if(isset($criteria->fcommissiontype) && !is_null($criteria->fcommissiontype)) {
            $commissionType = $criteria->fcommissiontype;
            $products =  $products->whereHas('product.seller', function ($q) use ($commissionType){
                $q->where('commission_id','=', $commissionType);
            });
        }

        return $products->paginate(intval($criteria->length), ['id','product_id', 'name','sku','mrp','price','discount', 'unique_id', 'slug', 'fabpik_seller_price','fabpik_seller_discount','fabpik_seller_discount_percentage', 'fabpik_addon_discount',  'special_price_start_date','special_price_end_date','created_at','updated_at'], 'page', $page);
    }
   
    public static function getSellerProductStockAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    ){
        $prodVariants = DB::table('sellers as t1')
                        ->where('t1.status',1)
                        ->where('t1.approval_status',1)
                        ->selectRaw('t1.id,t1.seller_code,t1.name')
                        ->orderBy($orderColumn, $orderDir);

        if (isset($criteria->fseller)) { $prodVariants = $prodVariants->where('id', $criteria->fseller);}
        if (isset($criteria->fcode)) { $prodVariants = $prodVariants->where('seller_code', $criteria->fcode);}

        return $prodVariants->paginate(intval($criteria->length), ['id','name','seller_code'], 'page', $page);
    }
    public static function getSellerProductStockExportData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    ){
        $prodVariants = DB::table('sellers as t1')
                        ->where('t1.status',1)
                        ->where('t1.approval_status',1)
                        ->selectRaw('t1.id,t1.seller_code,t1.name')
                        ->orderBy($orderColumn, $orderDir);

        if (isset($criteria->fseller)) { $prodVariants = $prodVariants->where('id', $criteria->fseller);}
        if (isset($criteria->fcode)) { $prodVariants = $prodVariants->where('seller_code', $criteria->fcode);}

        return $prodVariants->get();
    }

    public static function getLowStockReportAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='pv_unique_id',
        $orderDir='asc'
    )
    {
        $prodVariants = DB::table('view_stock_report')
                        ->whereRaw('stock<=min_stock')
                        ->select('p_unique_id','pv_unique_id','name','sku','stock','c_name','seller_code','category_id','s_name','brand_name')
                        ->orderBy($orderColumn, $orderDir);

        if (isset($criteria->fid)) {$prodVariants = $prodVariants->where('pv_unique_id',$criteria->fid);}
        if (isset($criteria->fpid)) {$prodVariants = $prodVariants->where('p_unique_id',$criteria->fpid);}
        if (isset($criteria->fsku)) {$prodVariants = $prodVariants->where('sku',$criteria->fsku);}
        if (isset($criteria->fseller)) {$prodVariants = $prodVariants->where('seller_id',$criteria->fseller);}
        if(isset($criteria->pname)) $prodVariants = $prodVariants->where('name',$criteria->pname);
        // dd($criteria->fcategory);
        if(isset($criteria->fcategory)) {
            @list($categoryId,$subCategoryId,$childCategoryId) = explode('-', $criteria->fcategory);
            // dd($categoryId);
            $prodVariants =  $prodVariants->where('category_id', $categoryId);}
        if (isset($criteria->fbrand)) { $prodVariants = $prodVariants->where('brand_id', $criteria->fbrand);}

        return $prodVariants->paginate(intval($criteria->length), ['p_unique_id','pv_unique_id','name','sku','seller_code','stock','c_name','s_name','brand_name'], 'page', $page);
    }

    public static function getLowStockReportExportData(
        $criteria=[],
        $page=1,
        $orderColumn='pv_unique_id',
        $orderDir='desc'
    )
    {
        $prodVariants = DB::table('view_stock_report')
                        ->whereRaw('stock<5')
                        ->select('p_unique_id','pv_unique_id','name','sku','stock','c_name','seller_code','category_id','s_name','brand_name')
                        ->orderBy($orderColumn, $orderDir);

        if (isset($criteria->fid)) {$prodVariants = $prodVariants->where('pv_unique_id',$criteria->fid);}
        if (isset($criteria->fpid)) {$prodVariants = $prodVariants->where('p_unique_id',$criteria->fpid);}
        if (isset($criteria->fsku)) {$prodVariants = $prodVariants->where('sku',$criteria->fsku);}
        if (isset($criteria->fseller)) {$prodVariants = $prodVariants->where('seller_id',$criteria->fseller);}
        if(isset($criteria->pname)) $prodVariants = $prodVariants->where('name',$criteria->pname);
        // dd($criteria->fcategory);
        if(isset($criteria->fcategory)) {
            @list($categoryId,$subCategoryId,$childCategoryId) = explode('-', $criteria->fcategory);
            // dd($categoryId);
            $prodVariants =  $prodVariants->where('category_id', $categoryId);}
        if (isset($criteria->fbrand)) { $prodVariants = $prodVariants->where('brand_id', $criteria->fbrand);}

        return $prodVariants->get();
    }

    public static function getModifiedStockReportAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='pv_unique_id',
        $orderDir='asc'
    )
    {
        $prodVariants = DB::table('view_stock_report')
                        ->select('pv_unique_id','p_unique_id','name','sku','s_name','seller_code','brand_name','updated_at')
                        ->orderBy('pv_unique_id', 'asc');

        if (isset($criteria->fid)) {$prodVariants = $prodVariants->where('pv_unique_id',$criteria->fid);}
        if (isset($criteria->fpid)) {$prodVariants = $prodVariants->where('p_unique_id',$criteria->fpid);}
        if (isset($criteria->fsku)) {$prodVariants = $prodVariants->where('sku',$criteria->fsku);}
        if (isset($criteria->fseller)) { $prodVariants = $prodVariants->where('seller_id',$criteria->fseller);}
        if(isset($criteria->pname)) $prodVariants = $prodVariants->where('name',$criteria->pname);
        if (isset($criteria->fbrand)) { $prodVariants = $prodVariants->where('brand_id', $criteria->fbrand);}
        if(isset($criteria->reportrange)) {
            @list($rangefrom,$rangeto) = explode('-', $criteria->reportrange);
            $modfrom= date('Y-m-d H:i:s', strtotime($rangefrom));
            $modto= date('Y-m-d H:i:s', strtotime($rangeto));
            // dd($modto);
                $prodVariants = $prodVariants->whereNotBetween('updated_at',[ $modfrom, $modto ]);
        } 

        return $prodVariants->paginate(intval($criteria->length), ['p_unique_id','pv_unique_id','name','sku','seller_code','s_name','brand_name','updated_at'], 'page', $page);
    }
    public static function getModifiedStockReportExportData(
        $criteria=[],
        $page=1,
        $orderColumn='pv_unique_id',
        $orderDir='desc'
    )
    {
        $prodVariants = DB::table('view_stock_report')
                        ->select('pv_unique_id','p_unique_id','name','sku','s_name','seller_code','brand_name','updated_at')
                        ->orderBy('pv_unique_id', 'asc');

        if (isset($criteria->fid)) {$prodVariants = $prodVariants->where('pv_unique_id',$criteria->fid);}
        if (isset($criteria->fpid)) {$prodVariants = $prodVariants->where('p_unique_id',$criteria->fpid);}
        if (isset($criteria->fsku)) {$prodVariants = $prodVariants->where('sku',$criteria->fsku);}
        if (isset($criteria->fseller)) { $prodVariants = $prodVariants->where('seller_id',$criteria->fseller);}
        if(isset($criteria->pname)) $prodVariants = $prodVariants->where('name',$criteria->pname);
        if (isset($criteria->fbrand)) { $prodVariants = $prodVariants->where('brand_id', $criteria->fbrand);}
        if(isset($criteria->reportrange)) {
            @list($rangefrom,$rangeto) = explode('-', $criteria->reportrange);
            $modfrom= date('Y-m-d H:i:s', strtotime($rangefrom));
            $modto= date('Y-m-d H:i:s', strtotime($rangeto));
            // dd($modto);
                $prodVariants = $prodVariants->whereNotBetween('updated_at',[ $modfrom, $modto ]);
        } 

        return $prodVariants->get();
    }
    public static function getExpiredSpecialPricesAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    )
    {
        $products = ProductVariant::with(['product'=> function($q){
            $q->whereNull('deleted_at');
        },'product.category', 'product.seller', 'product.productvariant','product.productcategory' => function($q){
            $q->where('is_active','=', 1);
        }])->whereRaw('special_price_end_date > NOW()')->orderBy($orderColumn, $orderDir);

        if (isset($criteria->fseller)) { $products = $products->whereHas('product', function($q) use ($criteria){
                                                            $q->where('seller_id', $criteria->fseller); 
                                                        });
        }

        return $products->paginate(intval($criteria->length), ['id','product_id', 'name','sku','mrp','price','discount', 'unique_id', 'slug', 'fabpik_seller_price','fabpik_seller_discount','fabpik_seller_discount_percentage','special_price_start_date','special_price_end_date','created_at','updated_at'], 'page', $page);
    }

    public static function expiredspecialpricesexport(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    )
    {
        $products = ProductVariant::with(['product'=> function($q){
            $q->whereNull('deleted_at');
        },'product.category', 'product.seller', 'product.productvariant','product.productcategory' => function($q){
            $q->where('is_active','=', 1);
        }])->whereRaw('special_price_end_date > NOW()')->orderBy($orderColumn, $orderDir);

        if (isset($criteria->fseller)) { $products = $products->whereHas('product', function($q) use ($criteria){
                                                            $q->where('seller_id', $criteria->fseller); 
                                                        });
        }

        return $products->get();
    }

    //Coupon Group Get the all the Product Variant List
    public static function getAjaxCouponGroupProductVariantListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    )
    {
        $productVariantData = ProductVariant::with(['product'=> function($q){
            $q->whereNull('deleted_at')->where('status', 1);
        }, 'product.category', 'product.seller', 'product.brand', 'product.productvariant','product.productcategory' => function($q){
            $q->where('is_active','=', 1);
        }])->orderBy($orderColumn, $orderDir);

        // dd($criteria->fselectedprodvariant);
        if(!is_null($criteria->fselectedprodvariant))
            $productVariantData = $productVariantData->whereNotIn('id', $criteria->fselectedprodvariant);

        if (isset($criteria->fseller)) { 
            $productVariantData = $productVariantData->whereHas('product', function($q) use ($criteria){
                                                                $q->where('seller_id', $criteria->fseller); 
            });
        }

        if (isset($criteria->fbrand)) { 
            $productVariantData = $productVariantData->whereHas('product', function($q) use ($criteria){
                                            $q->where('brand_id', $criteria->fbrand); 
            });
        }

        if (isset($criteria->fname))
            $productVariantData->where('name','>=', $criteria->fname);

        if (isset($criteria->fpricefrom))
            $productVariantData->where('fabpik_seller_price','>=', $criteria->fpricefrom);

        if (isset($criteria->fpriceto))
            $productVariantData->where('fabpik_seller_price','<=', $criteria->fpriceto);

        if (isset($criteria->fdiscountfrom))
            $productVariantData->where('fabpik_seller_discount','>=', $criteria->fdiscountfrom);

        if (isset($criteria->fdiscountto))
            $productVariantData->where('fabpik_seller_discount','<=', $criteria->fdiscountto);

        if (isset($criteria->fpdiscountfrom))
            $productVariantData->where('fabpik_seller_discount_percentage','>=', $criteria->fpdiscountfrom);

        if (isset($criteria->fpdiscountto))
            $productVariantData->where('fabpik_seller_discount_percentage','<=', $criteria->fpdiscountto);

        if (isset($criteria->fspricefrom))
            $productVariantData->where('price','>=', $criteria->fspricefrom);

        if (isset($criteria->fspriceto))
            $productVariantData->where('price','<=', $criteria->fspriceto);

        if (isset($criteria->fsdiscountfrom))
            $productVariantData->where('discount','>=', $criteria->fsdiscountfrom);

        if (isset($criteria->fsdiscountto))
            $productVariantData->where('discount','<=', $criteria->fsdiscountto);

        if (isset($criteria->fspdiscountfrom))
            $productVariantData->where('discount','>=', DB::raw('`mrp` * '.(($criteria->fspdiscountfrom/100))));

        if (isset($criteria->fspdiscountto))
            $productVariantData->where('discount','<=', DB::raw('`mrp` * '.(($criteria->fspdiscountto/100))));

        
        if (isset($criteria->fcategory)) { 
            $productVariantData = $productVariantData->whereHas('product.productcategory', function($q) use ($criteria){
                                                                $q->where('category_id', $criteria->fcategory); 
            });
        }

        if (isset($criteria->fsubcategory)) { 
            $productVariantData = $productVariantData->whereHas('product.productcategory', function($q) use ($criteria){
                                                                $q->where('subcategory_id', $criteria->fsubcategory); 
            });
        }

        if (isset($criteria->fchildcategory)) { 
            $productVariantData = $productVariantData->whereHas('product.productcategory', function($q) use ($criteria){
                                                                $q->whereIn('childcategory_id', array($criteria->fchildcategory)); 
            });
        }   
    

        return $productVariantData->paginate(intval($criteria->length), ['id','product_id', 'name','sku','mrp','price','discount', 'unique_id', 'slug', 'fabpik_seller_price','fabpik_seller_discount','fabpik_seller_discount_percentage','special_price_start_date','special_price_end_date','created_at','updated_at'], 'page', $page);
    }

    //filter the groups in step 2
    public static function getSelectAllProductVariantListData(
        $criteria=[]
    )
    {
        $productVariantData = ProductVariant::with(['product'=> function($q){
            $q->whereNull('deleted_at')->where('status', 1);
        },'product.category', 'product.seller', 'product.productvariant','product.productcategory' => function($q){
            $q->where('is_active','=', 1);
        }]);

        if(!is_null($criteria->prodIds))
            $productVariantData =   $productVariantData->whereNotIn('id', $criteria->prodIds);

        if (isset($criteria->fseller)) { 
            $productVariantData = $productVariantData->whereHas('product', function($q) use ($criteria){
                                                                $q->where('seller_id', $criteria->fseller); 
            });
        }

        if (isset($criteria->fbrand)) { 
            $productVariantData = $productVariantData->whereHas('product', function($q) use ($criteria){
                                            $q->where('brand_id', $criteria->fbrand); 
            });
        }

        if (isset($criteria->fname))
            $productVariantData->where('name','>=', $criteria->fname);

        if (isset($criteria->fpricefrom))
            $productVariantData->where('fabpik_seller_price','>=', $criteria->fpricefrom);

        if (isset($criteria->fpriceto))
            $productVariantData->where('fabpik_seller_price','<=', $criteria->fpriceto);

        if (isset($criteria->fdiscountfrom))
            $productVariantData->where('fabpik_seller_discount','>=', $criteria->fdiscountfrom);

        if (isset($criteria->fdiscountto))
            $productVariantData->where('fabpik_seller_discount','<=', $criteria->fdiscountto);

        if (isset($criteria->fpdiscountfrom))
            $productVariantData->where('fabpik_seller_discount_percentage','>=', $criteria->fpdiscountfrom);

        if (isset($criteria->fpdiscountto))
            $productVariantData->where('fabpik_seller_discount_percentage','<=', $criteria->fpdiscountto);
        
        if (isset($criteria->fspricefrom))
            $productVariantData->where('price','>=', $criteria->fspricefrom);

        if (isset($criteria->fspriceto))
            $productVariantData->where('price','<=', $criteria->fspriceto);

        if (isset($criteria->fsdiscountfrom))
            $productVariantData->where('discount','>=', $criteria->fsdiscountfrom);

        if (isset($criteria->fsdiscountto))
            $productVariantData->where('discount','<=', $criteria->fsdiscountto);

        if (isset($criteria->fspdiscountfrom))
            $productVariantData->where('discount','>=', DB::raw('`mrp` * '.(($criteria->fspdiscountfrom/100))));

        if (isset($criteria->fspdiscountto))
            $productVariantData->where('discount','<=', DB::raw('`mrp` * '.(($criteria->fspdiscountto/100))));

        if (isset($criteria->fcategory)) { 
                $productVariantData = $productVariantData->whereHas('product.productcategory', function($q) use ($criteria){
                                                                    $q->where('category_id', $criteria->fcategory); 
                });
        }
    
        if (isset($criteria->fsubcategory)) { 
                $productVariantData = $productVariantData->whereHas('product.productcategory', function($q) use ($criteria){
                                                                    $q->where('subcategory_id', $criteria->fsubcategory); 
                });
        }
    
        if (isset($criteria->fchildcategory)) { 
                $productVariantData = $productVariantData->whereHas('product.productcategory', function($q) use ($criteria){
                                                                    $q->whereIn('childcategory_id', array($criteria->fchildcategory)); 
                });
        }

        $productVariantData = $productVariantData->select('id')->get();
        
        return $productVariantData;
    }


    //Coupon Group Get the all the Product Variant Final List
    public static function getAjaxCouponGroupProductVariantFinalListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    )
    {
        $productVariantData = ProductVariant::with(['product'=> function($q){
            $q->whereNull('deleted_at')->where('status', 1);
        },'product.category', 'product.seller', 'product.brand', 'product.productvariant','product.productcategory' => function($q){
            $q->where('is_active','=', 1);
        }])->orderBy($orderColumn, $orderDir);

        
        if(!is_null($criteria->fselectedprodvariant))
            $productVariantData = $productVariantData->whereIn('id', $criteria->fselectedprodvariant);


        if (isset($criteria->fseller)) { 
            $productVariantData = $productVariantData->whereHas('product', function($q) use ($criteria){
                                                                $q->where('seller_id', $criteria->fseller); 
            });
        }

        if (isset($criteria->fbrand)) { 
            $productVariantData = $productVariantData->whereHas('product', function($q) use ($criteria){
                                            $q->where('brand_id', $criteria->fbrand); 
            });
        }

        if (isset($criteria->fname))
            $productVariantData->where('name','>=', $criteria->fname);

        if (isset($criteria->fpricefrom))
            $productVariantData->where('fabpik_seller_price','>=', $criteria->fpricefrom);

        if (isset($criteria->fpriceto))
            $productVariantData->where('fabpik_seller_price','<=', $criteria->fpriceto);

        if (isset($criteria->fdiscountfrom))
            $productVariantData->where('fabpik_seller_discount','>=', $criteria->fdiscountfrom);

        if (isset($criteria->fdiscountto))
            $productVariantData->where('fabpik_seller_discount','<=', $criteria->fdiscountto);

        if (isset($criteria->fpdiscountfrom))
            $productVariantData->where('fabpik_seller_discount_percentage','>=', $criteria->fpdiscountfrom);

        if (isset($criteria->fpdiscountto))
            $productVariantData->where('fabpik_seller_discount_percentage','<=', $criteria->fpdiscountto);
     
        if (isset($criteria->fspricefrom))
            $productVariantData->where('price','>=', $criteria->fspricefrom);

        if (isset($criteria->fspriceto))
            $productVariantData->where('price','<=', $criteria->fspriceto);

        if (isset($criteria->fsdiscountfrom))
            $productVariantData->where('discount','>=', $criteria->fsdiscountfrom);

        if (isset($criteria->fsdiscountto))
            $productVariantData->where('discount','<=', $criteria->fsdiscountto);

        if (isset($criteria->fspdiscountfrom))
            $productVariantData->where('discount','>=', DB::raw('`mrp` * '.(($criteria->fspdiscountfrom/100))));

        if (isset($criteria->fspdiscountto))
            $productVariantData->where('discount','<=', DB::raw('`mrp` * '.(($criteria->fspdiscountto/100))));

            
        if (isset($criteria->fcategory)) { 
                $productVariantData = $productVariantData->whereHas('product.productcategory', function($q) use ($criteria){
                                                                    $q->where('category_id', $criteria->fcategory); 
                });
        }
    
        if (isset($criteria->fsubcategory)) { 
                $productVariantData = $productVariantData->whereHas('product.productcategory', function($q) use ($criteria){
                                                                    $q->where('subcategory_id', $criteria->fsubcategory); 
                });
        }
    
        if (isset($criteria->fchildcategory)) { 
                $productVariantData = $productVariantData->whereHas('product.productcategory', function($q) use ($criteria){
                                                                    $q->whereIn('childcategory_id', array($criteria->fchildcategory)); 
                });
        }   

        return $productVariantData->paginate(intval($criteria->length), ['id','product_id', 'name','sku','mrp','price','discount', 'unique_id', 'slug', 'fabpik_seller_price','fabpik_seller_discount','fabpik_seller_discount_percentage','special_price_start_date','special_price_end_date','created_at','updated_at'], 'page', $page);
    }
   

}

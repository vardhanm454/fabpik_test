<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Conversation extends Model
{

    public static function getSellerAvatar($seller) { 
        
        $i=0;
        foreach($seller as $s){
            $seller[$i]->seller_images = url(UPLOAD_PATH.'/avatar',$s->avatar);   
            $i++;
        }
       return $seller;
    }
}

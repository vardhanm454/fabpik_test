<?php

namespace App\Models;

class Role extends \Spatie\Permission\Models\Role
{
    protected $table = 'roles';
    protected $primaryKey   = 'id';
    protected $fillable = ['name','guard_name'];

    public static function getAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='asc')
    {
        $roles = Role::orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->name)) $roles = $roles->where('name', $criteria->name);
        $roles = $roles->where('seller_id', $criteria->seller);

        return $roles->paginate(intval($criteria->length), ['id','name','created_at'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='asc')
    {
        $roles = Role::orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->name)) $roles = $roles->where('name', $criteria->name);
        $roles = $roles->where('seller_id', $criteria->seller);

        return $roles->paginate(intval($criteria->length), ['id','name','guard_name','created_at'], 'page', $page);
    }
}

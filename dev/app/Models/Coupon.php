<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;

	protected $table        = 'coupons';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 'y');
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
    	$coupons = Coupon::orderBy($orderColumn, $orderDir);
        
        if(!is_null($criteria->fname)) $coupons = $coupons->where('code', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $coupons = $coupons->where('status', $criteria->fstatus);
        if(!is_null($criteria->freference)) $coupons = $coupons->where('coupon_group_id', $criteria->freference);

        return $coupons->paginate(intval($criteria->length), ['id','code','amount','max_usage','sub_type','dis_type','status'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[], 
        $orderColumn='id', 
        $orderDir='desc')
    {
        $coupons = Coupon::orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->fname)) $coupons = $coupons->where('code', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $coupons = $coupons->where('status', $criteria->fstatus);
        return $coupons->get();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SellerSettingChangeLog extends Model
{
    protected $table        = 'seller_setting_change_logs';
    protected $primaryKey   = 'id';

    public function seller()
    {
        return $this->belongsTo('App\Models\Seller', 'created_by','id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'approved_by','id');
    }

    public static function getAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    )
    {
        $logs = SellerSettingChangeLog::with('seller','user')->whereNull('is_deleted')->orderBy($orderColumn, $orderDir);
                       
        return $logs->paginate(intval($criteria->length), ['id','settings_column_name','old_value','new_value','created_by','created_at','approved_by','approved_at','updated_at', 'is_deleted'], 'page', $page);
    }
}

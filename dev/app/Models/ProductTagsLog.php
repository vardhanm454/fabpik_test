<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ProductTagsLog extends Model
{
    // use SoftDeletes;

	protected $table        = 'product_tags_logs';
    protected $primaryKey   = 'id';

    public function modifiedUser()
    {
        return $this->belongsTo('App\Models\User', 'modified_by','id');
    }
}

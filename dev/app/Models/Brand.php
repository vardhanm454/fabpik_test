<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;

	protected $table        = 'brands';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function seller()
    {
        return $this->belongsTo('App\Models\Seller', 'created_by', 'id');
    }

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='desc')
    {
    	//$brands = Brand::orderBy($orderColumn, $orderDir);

        $brands = Brand::with('seller')->orderBy('brands.'.$orderColumn, $orderDir);


        if(!is_null($criteria->fname)) $brands = $brands->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $brands = $brands->where('status', $criteria->fstatus);

        return $brands->paginate(intval($criteria->length), ['id','name','image','created_by','status'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[], 
        $orderColumn='id', 
        $orderDir='desc')
    {
        $brands = Brand::orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->fname)) $brands = $brands->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $brands = $brands->where('status', $criteria->fstatus);
        return $brands->get();
    }
}

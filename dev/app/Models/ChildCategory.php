<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ChildCategory extends Model
{
    use SoftDeletes;

    protected $table        = 'childcategories';
    protected $primaryKey   = 'id';

    protected $appends = ['icon_path','image_path'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Models\Subcategory', 'subcategory_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function getIconPathAttribute() { 
        return url(UPLOAD_PATH.'/'.$this->icon); 
    }

    public function getImagePathAttribute() { 
        return url(UPLOAD_PATH.'/'.$this->image); 
    }

    public static function getAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    ) {
        
        $childcategories = ChildCategory::with(['category', 'subcategory'])->orderBy('childcategories.'.$orderColumn, $orderDir);

        if (!is_null($criteria->fname)) {$childcategories = $childcategories->where('childcategories.title', 'LIKE', "%{$criteria->fname}%"); }
        if (!is_null($criteria->fstatus)) { $childcategories = $childcategories->where('childcategories.status', $criteria->fstatus); }
        
        return $childcategories->paginate(intval($criteria->length), ['childcategories.id','childcategories.image','childcategories.icon','childcategories.category_id', 'childcategories.subcategory_id', 'childcategories.title','childcategories.status'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[],
        $orderColumn='id',
        $orderDir='desc'
    ) {
        $childcategories = ChildCategory::with('category', 'subcategory')->orderBy('childcategories.'.$orderColumn, $orderDir);
        if (!is_null($criteria->fname)) {$childcategories = $childcategories->where('childcategories.title', 'LIKE', "%{$criteria->fname}%"); }
        if (!is_null($criteria->fstatus)) { $childcategories = $childcategories->where('childcategories.status', $criteria->fstatus); }
        return $childcategories->get();
    }

    public static function getCategoriesExportData() {
        $childcategories = ChildCategory::with('category', 'subcategory')->where('childcategories.status', 1)->orderBy('childcategories.id', 'desc');
        return $childcategories->get();
    }


    public static function getChildCategoryFilters($childCategories,$currentChildCategorySlug)
    {
  
        $childCategoriesReturn = [];
        foreach ($childCategories as $category) {
            if($category->slug == $currentChildCategorySlug){
                $category->is_checked = true;
                array_push($childCategoriesReturn,$category);
            }else{
                $category->is_checked = false;
                array_push($childCategoriesReturn,$category);
            }
        }
        return $childCategoriesReturn;
        // $childcategories = DB::table('categories as t1')
        //                         ->join('subcategories as t2', 't2.category_id', '=', 't1.id')
        //                         ->join('childcategories as t3', 't3.subcategory_id', '=', 't2.id')
        //                         ->where('t2.id', $productdata['subCategoryId'])
        //                         ->where('t1.status', 1)
        //                         ->where('t2.status', 1)
        //                         ->where('t3.status', 1)
        //                         ->selectRaw("t3.title, t3.id ,'false' as is_checked, t2.title as subtitle")
        //                         ->get();
        
    //    $tblPrjCatPrdInpData=[];
    //     foreach ($childcategories as $input_key => $input_value) {
    //         $tblPrjCatPrdInpData = [
    //         'subcategory_name' =>$input_value->subtitle,
    //         'childcategorieslist'=>$childcategories
    //         ];
    //     }
    //     return $tblPrjCatPrdInpData;
    // manual 
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Model
{
    use SoftDeletes;

	protected $table        = 'attributes';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    // public function attributeCategory()
    // {
    //   return $this->hasMany('App\Models\AttributeCategory', 'attribute_id', 'id');
    // }

    public function attributeOptions()
    {
      return $this->hasMany('App\Models\AttributeOption', 'attribute_id', 'id');
    }

    public function primaryCategory()
    {
      return $this->belongsTo('App\Models\Category', 'id', 'primary_attribute');
    }

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
    	$attribute = Attribute::with( 'attributeOptions', 'primaryCategory')->orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->fname)) $attribute = $attribute->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $attribute = $attribute->where('status', $criteria->fstatus);
        return $attribute->paginate(intval($criteria->length), ['id','name','display_name','attr_type','status', 'created_at','updated_at'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[], 
        $orderColumn='id', 
        $orderDir='desc')
    {
        $attribute = Attribute::with('attributeCategory', 'attributeOptions')->orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->fname)) $attribute = $attribute->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $attribute = $attribute->where('status', $criteria->fstatus);
        return $attribute->get();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Database\Eloquent\Factories\HasFactory;

class WashingType extends Model
{
    // use SoftDeletes, HasFactory;

    protected $table        = 'washing_types';
    protected $primaryKey   = 'id';
}

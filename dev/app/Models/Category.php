<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

	protected $table        = 'categories';
    protected $primaryKey   = 'id';

    protected $appends = ['icon_path','image_path'];

    public function subcategory()
    {
      return $this->hasMany('App\Models\Subcategory', 'category_id', 'id');
    }

    public function subcategories()
    {
      return $this->hasMany('App\Models\Subcategory', 'category_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function getIconPathAttribute() { 
        return url(UPLOAD_PATH.'/'.$this->icon); 
    }

    public function getImagePathAttribute() { 
        return url(UPLOAD_PATH.'/'.$this->image); 
    }

    /*
    ** delete Child categories
    */
    public static function deleteChildCategory($ids=[])
    {
        ChildCategory::whereIn('category_id', $ids)->delete();
    }

    /*
    ** delete Sub categories
    */
    public static function deleteSubCategory($ids=[])
    {
        Subcategory::whereIn('category_id', $ids)->delete();
    }
    
    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='desc')
    {
    	$categories = Category::orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->fname)) $categories = $categories->where('title', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $categories = $categories->where('status', $criteria->fstatus);
        return $categories->paginate(intval($criteria->length), ['id','title','image','icon', 'featured', 'commission','tax', 'status'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[], 
        $orderColumn='id', 
        $orderDir='desc')
    {
        $categories = Category::orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->fname)) $categories = $categories->where('title', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $categories = $categories->where('status', $criteria->fstatus);
        return $categories->get();
    }

    public static function getCategories(){
        return Category::where('status', 1)->get();
    }
}
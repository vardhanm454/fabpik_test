<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class TaxCodes extends Model
{
    // use SoftDeletes;
    
    protected $table        = 'taxcodes';
    // protected $primaryKey   = 'id';
    // public $timestamps = false;

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
        
    	$taxcodes = TaxCodes::select('id','code','igst','cgst','sgst')->orderBy($orderColumn, $orderDir);
        // dd($criteria);
        if(!is_null($criteria->fcode)) $taxcodes = $taxcodes->where('code', 'LIKE', "%{$criteria->fcode}%");
        return $taxcodes->paginate(intval($criteria->length), ['id','code','igst','cgst','sgst'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[], 
        $orderColumn='id', 
        $orderDir='desc')
    {
        $taxcodes = TaxCodes::select('id','code','igst','cgst','sgst')->orderBy($orderColumn, $orderDir);
        // dd($criteria);
        if(!is_null($criteria->fcode)) $taxcodes = $taxcodes->where('code', 'LIKE', "%{$criteria->fcode}%");
        return $taxcodes->get();
    }


}

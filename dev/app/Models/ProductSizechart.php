<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSizechart extends Model
{
    use SoftDeletes;

    protected $table        = 'product_size_charts';
    protected $primaryKey   = 'id';

}

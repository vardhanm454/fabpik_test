<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponGroup extends Model
{
    use SoftDeletes;

	protected $table        = 'coupon_groups';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 'y');
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by','id');
    }

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
    	$coupons = CouponGroup::with('user')->orderBy($orderColumn, $orderDir);
        
        if(!is_null($criteria->fname)) $coupons = $coupons->where('group_name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $coupons = $coupons->where('status', $criteria->fstatus);

        return $coupons->paginate(intval($criteria->length), ['id', 'group_name', 'description', 'created_by', 'created_at', 'updated_at', 'status'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[], 
        $orderColumn='id', 
        $orderDir='desc')
    {
       
    }
}

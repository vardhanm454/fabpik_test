<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission
{
    protected $table = 'permissions';
    protected $primaryKey   = 'id';

    public static function getAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='asc')
    {
        $permissions = Permission::where('for_seller', 0)->orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->module_name)) $permissions = $permissions->where('module_name', $criteria->module_name);
        if(!is_null($criteria->name)) $permissions = $permissions->where('name', 'LIKE', "%{$criteria->name}%");

        return $permissions->paginate(intval($criteria->length), ['id','module_name','name'], 'page', $page);
	}

	/**
	 *
	 */
    public static function getModuleWisePermissions($criterias = [])
    {
		$permissions = new Permission();
		if(isset($criterias['module']) && !is_null($criterias['module'])) $permissions = $permissions->where('module_name', $criterias->panel);
		if(isset($criterias['panel']) && !is_null($criterias['panel'])) $permissions = $permissions->where('for_seller', $criterias['panel']);

		$permissions = $permissions->selectRaw("id, name, display_name, module_name")
									->orderBy('module_name')
									->orderBy('display_name')
									->get();

		$response = [];
		foreach($permissions as $key => $permission) {
			$response[$permission->module_name][] = $permission;
		}

		return $response;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockNotify extends Model
{
    use SoftDeletes;

	protected $table        = 'stock_notify';
    protected $primaryKey   = 'id';

}

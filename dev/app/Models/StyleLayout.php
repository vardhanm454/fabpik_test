<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StyleLayout extends Model
{
    use SoftDeletes;

    protected $table        = 'stylelayouts';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id','id');
    }

    public function createdUser()
    {
        return $this->belongsTo('App\Models\User', 'created_by','id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo('App\Models\User', 'updated_by','id');
    }

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='desc')
    {
        $stylelayouts = StyleLayout::with('createdUser', 'modifiedUser', 'category')->orderBy($orderColumn, $orderDir);

        if(!is_null($criteria->fname))
            $stylelayouts =  $stylelayouts->where('title', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fcategory))  
            $stylelayouts =  $stylelayouts->where('category_id', $criteria->fcategory);
        if(!is_null($criteria->freference))  
             $stylelayouts =  $stylelayouts->whereJsonContains("columns", [$criteria->freference]);
            

        return $stylelayouts->paginate(intval($criteria->length), ['id','title','category_id', 'columns','created_by', 'created_at', 'updated_by', 'updated_at'], 'page', $page);
    }
}

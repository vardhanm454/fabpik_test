<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImageSizechart extends Model
{
    use SoftDeletes;

    protected $table        = 'sizechart_image_master';
    protected $primaryKey   = 'id';
}

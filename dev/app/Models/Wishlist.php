<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class Wishlist extends Model
{
     use HasFactory;
    //  use SoftDeletes;
     protected $fillable = [
        'customer_id',
        'product_id',
        'product_variant_id'
    ];

    public function product()
      {
          return $this->belongsTo('App\Models\Product','product_id','id');
      }

      public function productvariant()
      {
          return $this->belongsTo('App\Models\ProductVariant','product_variant_id','id');
      }

    /**
     * Return a image path.ss
     *
     * 
    */
    public static function getProductimagespath($products)
    {
        foreach($products as $product)
        {   
            $thumbnail = $product['thumbnail'];           
            $images = explode(',', trim($product['images'],"[]"));
            $imgarr=array();
            foreach($images as $key => $image)
            {
              $imgarr[ $key+1 ] = $image;
               
            }

            $thumbnail = isset($imgarr[$thumbnail])?trim($imgarr[$thumbnail], '"'):'';
            $product['images'] = url(UPLOAD_PATH.'/'.$thumbnail);  
        
        }

       return $products;
    }
    
}

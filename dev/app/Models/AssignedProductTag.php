<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedProductTag extends Model
{
    use SoftDeletes;

	protected $table        = 'assigned_product_tags';
    protected $primaryKey   = 'id';


    public function productTagDetails()
    {
        return $this->belongsTo('App\Models\ProductTag','tag_id','id');
    }
}

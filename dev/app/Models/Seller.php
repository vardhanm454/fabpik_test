<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seller extends Model
{
    use SoftDeletes;

    protected $table        = 'sellers';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 'y');
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function product()
    {
        return $this->hasMany('App\Models\Product','seller_id','id');
    }

    public function sellerWarehouses()
    {
        return $this->hasMany('App\Models\SellerWarehouses','seller_id','id');
    }

    public function user()
    {
        return $this->hasMany('App\Models\User','ref_id','id');
    }
    
    public function commissions()
    {
        return $this->belongsTo('App\Models\CommissionModel','commission_id','id');
    }

    public static function getAjaxListData(
        $criteria=[], 
        $page=1, 
        $orderColumn='id', 
        $orderDir='asc')
    {
        $sellers = Seller::with(["commissions", "user" => function($q){
                                    $q->where('email_verified_at', "!=" , null);
                                }])->orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->fstatus)) $sellers = $sellers->where('status', $criteria->fstatus);
        if(!is_null($criteria->fcategory)) $sellers = $sellers->where('categories', $criteria->fcategory);
        if(!is_null($criteria->fapproval)) $sellers = $sellers->where('approval_status', (( $criteria->fapproval !='null' )?$criteria->fapproval:null) );
        if(!is_null($criteria->ffromdate) && !is_null($criteria->ftodate)){
            if($criteria->ffromdate == $criteria->ftodate){
                $sellers = $sellers->where('created_at','>=', $criteria->ffromdate);
            }else{
                $sellers = $sellers->whereBetween('created_at',[ $criteria->ffromdate, $criteria->ftodate ]);
            }
        } else if(!is_null($criteria->ffromdate)){
            $sellers = $sellers->where('created_at','>=', $criteria->ffromdate);
        } else if(!is_null($criteria->ftodate)){
            $sellers = $sellers->where('created_at','<=', $criteria->ftodate);
        }
        if(!is_null($criteria->fname)) $sellers = $sellers->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->femail)) $sellers = $sellers->where('email', 'LIKE', "%{$criteria->femail}%");
        if(!is_null($criteria->fcompanyname)) $sellers = $sellers->where('company_name', 'LIKE', "%{$criteria->fcompanyname}%");
        if(!is_null($criteria->fmobile)) $sellers = $sellers->where('mobile', 'LIKE', "%{$criteria->fmobile}%");
        if(!is_null($criteria->freference)) $sellers = $sellers->where('commission_id', $criteria->freference);
        
        if(!is_null($criteria->fsellercode)) $sellers = $sellers->where('seller_code', 'LIKE', "%{$criteria->fsellercode}%");

        if(!is_null($criteria->fcommission)) $sellers = $sellers->whereHas('commissions', function($q) use($criteria){
                                                                    $q->where('id', $criteria->fcommission);
                                                                });

        return $sellers->paginate(intval($criteria->length), ['id','name','company_name','company_type', 'commission_id', 'email','mobile','categories','created_at','approved_on','approval_status', 'status', 'seller_code'], 'page', $page);
    }
}

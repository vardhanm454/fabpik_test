<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    // use SoftDeletes;

	protected $table        = 'reviews';
    protected $primaryKey   = 'id';

    public function getColor($rating){
        if($rating == 5){
            $color = "darkgreen";
        }
        else if($rating == 4){
            $color = "lightgreen";
        }
        else if($rating == 3){
            $color = "yellow";
        }
        else if($rating == 2){
            $color = "orange";
        }
        else if($rating == 1){
            $color = "red";
        }
        return $color;
    }

    public static function reviewStats($pid)
    {
        $stats =  DB::table("product_review_stats")->where('product_id','=',$pid)->orderBy('rating','DESC')->get();
        $total =  DB::table("product_review_stats")->selectRaw("SUM(total) AS total")->where('product_id','=',$pid)->first()->total;
        $statsTemp = [];
        $found = 0;
        for ($i=5; $i >= 1 ; $i--) {
            foreach ($stats as $stat) {
                $found = 0;
                if($stat->rating == $i){
                    $found = 1;
                    break;
                }
            }
            if($found == 1){
                if($i == 5){
                    $color = "darkgreen";
                }
                else if($i == 4){
                    $color = "lightgreen";
                }
                else if($i == 3){
                    $color = "yellow";
                }
                else if($i == 2){
                    $color = "orange";
                }
                else if($i == 1){
                    $color = "red";
                }
                $stat->color = $color;
                $stat->percent = ($stat->total/(int)$total)*100;
                array_push($statsTemp,$stat);
            }else{
                if($i == 5){
                    $color = "darkgreen";
                }
                else if($i == 4){
                    $color = "lightgreen";
                }
                else if($i == 3){
                    $color = "yellow";
                }
                else if($i == 2){
                    $color = "orange";
                }
                else if($i == 1){
                    $color = "red";
                }
                $stat = ['product_id'=>$pid,'rating'=>$i,'total'=>0,'color'=>$color,'percent'=>0];
                array_push($statsTemp,$stat);
            }
        }
        return $statsTemp;
        // if(count($stats) < 5){
        //     $statsTemp = [];
        //     $rating = 5;
        //     for ($i=0; $i < 5 ; $i++) {
        //         if(isset($stats[$i])){
        //             array_push($statsTemp,$stats[$i]);
        //         }else{
        //             $stat = ['product_id'=>$pid,'rating'=>$rating,'total'=>0];
        //             array_push($statsTemp,$stat);
        //         }
        //         $rating--;
        //     }
        //     return $statsTemp;
        // }else{
        //     return $stats;
        // }
        
    }


    public static function getAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    )
    {
        $reviews = Review::selectRaw("reviews.id,reviews.review_title,reviews.review_comment,reviews.product_variant_id,reviews.rating,reviews.published,reviews.status,reviews.created_at,reviews.child_order_id,product_variants.name,product_variants.unique_id,sellers.id as seller_id,sellers.seller_code,reviews.updated_at,customers.name as customer_name,customers.email as customer_email")
                            ->join("product_variants","product_variants.id","=","reviews.product_variant_id")
                            ->join("products","products.id","=","reviews.product_id")
                            ->join("customers","customers.id",'=',"reviews.customer_id")
                            ->join("sellers","sellers.id","=","products.seller_id");
        if(!is_null($criteria->fstatus)) $reviews = $reviews->where('published', $criteria->fstatus);
        if(!is_null($criteria->fcustomers)) $reviews = $reviews->where('reviews.customer_id', $criteria->fcustomers);
        if(!is_null($criteria->ffromdate)) $reviews = $reviews->where('reviews.created_at','>=', $criteria->ffromdate);
        if(!is_null($criteria->ftodate)) $reviews = $reviews->where('reviews.created_at','<=', $criteria->ftodate);
        if(!is_null($criteria->fprovariants)) $reviews = $reviews->where('reviews.product_variant_id',json_decode($criteria->fprovariants));
        if(!is_null($criteria->fseller)) $reviews = $reviews->where('products.seller_id', $criteria->fseller);

        return $reviews->paginate(intval($criteria->length), ['id','name','seller_id','unique_id','created_at','updated_at', 'status'], 'page', $page);
    }

    public static function getRatingSum($seller = null)
    {
        $ratingSum = Review::join("product_variants","product_variants.id","=","reviews.product_variant_id")
                        ->join("products","products.id","=","reviews.product_id")
                        ->join("customers","customers.id",'=',"reviews.customer_id")
                        ->join("sellers","sellers.id","=","products.seller_id")
                        ->where('products.seller_id', $seller)
                        ->sum('reviews.rating');
        
        return $ratingSum;
    }


}

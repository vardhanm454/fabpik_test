<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourierPartner extends Model
{
    use SoftDeletes;
    
    protected $table        = 'courier_partners';
    protected $primaryKey   = 'id';
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class LayoutColumnLog extends Model
{
    // use SoftDeletes;

	protected $table        = 'layout_column_logs';
    protected $primaryKey   = 'id';

    public function modifiedUser()
    {
        return $this->belongsTo('App\Models\User', 'modified_by','id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    // use SoftDeletes;

	protected $table        = 'orders';
    protected $primaryKey   = 'id';

    public function orderDetail(){
        return $this->hasMany('App\Models\OrderDetail', 'order_id');
    }
    
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');
    }

    public function customerAddress()
    {
        return $this->belongsTo('App\Models\CustomerAddress', 'customer_id', 'customer_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'order_details');
    }

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
        $orders = Order::with('customer','orderDetail')->orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->ffromdate)) $orders = $orders->where('created_at','>=', $criteria->ffromdate);
        if(!is_null($criteria->ftodate)) $orders = $orders->where('created_at','<=', $criteria->ftodate);
        if(!is_null($criteria->fostatus)) $orders = $orders->where('order_status_id', $criteria->fostatus);
        if(!is_null($criteria->fptype)) $orders = $orders->where('payment_type', $criteria->fptype);
        if(!is_null($criteria->fpstatus)) $orders = $orders->where('payment_status_id', $criteria->fpstatus);

        if(!is_null($criteria->fparentid)) $orders = $orders->where('parent_order_id', 'LIKE', "%{$criteria->fparentid}%");
        if(!is_null($criteria->fcustomername)) $orders = $orders->where('first_name', 'LIKE', "%{$criteria->fcustomername}%")->where('last_name', 'LIKE', "%{$criteria->fcustomername}%");
        if(!is_null($criteria->fcustomermobile)) $orders = $orders->where('mobile', 'LIKE', "%{$criteria->fcustomermobile}%");

        return $orders->paginate(intval($criteria->length), ['id', 'customer_id', 'coupon_code', 'handling_charge', 'shipping_charge','parent_order_id', 'coupon_id', 'total', 'payment_type','payment_status_id', 'order_status_id','created_at','first_name', 'mobile' ], 'page', $page);
    }
}

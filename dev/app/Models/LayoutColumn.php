<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LayoutColumn extends Model
{
    use SoftDeletes;

	protected $table        = 'layout_columns';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by','id');
    }

    public function modifiedUser()
    {
        return $this->belongsTo('App\Models\User', 'modified_by','id');
    }

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='desc')
    {
        $columns = LayoutColumn::with('user', 'modifiedUser')->orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->fname)) $columns = $columns->where('name', 'LIKE', "%{$criteria->fname}%");

        return $columns->paginate(intval($criteria->length), ['id', 'name', 'created_by', 'modified_by', 'status', 'created_at', 'updated_at'], 'page', $page);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use DB;
use App\Models\ProductVariantOption;
use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Http\Resources\ProductCollection;

class Product extends Model
{
    use SoftDeletes, HasFactory;

    protected $table        = 'products';
    protected $primaryKey   = 'id';

    protected $appends = ['image_paths','thumbnail_path'];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category','primary_category','id');
    }

    public function wishlists(){
        return $this->hasMany('App\Models\Wishlist','id','product_id');
    }

    public function productcategory()
    {
        return $this->hasMany('App\Models\ProductCategory','product_id','id');
    }

    public function productvariant()
    {
        return $this->hasMany('App\Models\ProductVariant' ,'product_id','id');
    }

    public function variantimages()
    {
        return $this->hasMany('App\Models\VariationImage' ,'product_id','id');
    }

    public function seller()
    {
        return $this->belongsTo('App\Models\Seller','seller_id','id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Brand','brands_id','id');
    }

    public function productTags()
    {
        return $this->hasMany('App\Models\AssignedProductTag','product_id','id');
    }

    public function getImagePathsAttribute() {
        $images = json_decode($this->image);
        $return = [];
        if(!empty($images)) {
           foreach ($images as $key => $image) {
                $return[] = url(UPLOAD_PATH.'/'.$image);
            }
        }

        return $return;
    }

    public function getThumbnailPathAttribute() {
        $images = json_decode($this->image);

        return (isset($images[$this->thumbnail-1]))?url(UPLOAD_PATH.'/'.$images[$this->thumbnail-1]):null;
    }


    // public static function getListingProducts($filters=[])
    // {
    //     /* SELECT
    //         t2.product_variant_id,
    //         t1.unique_id,
    //         t1.name,
    //         t1.slug,
    //         t1.sku,
    //         t1.stock,
    //         t1.mrp,
    //         t1.discount,
    //         t1.price,
    //         t3.id AS product_id,
    //         t3.seller_id,
    //         t5.images,
    //         t5.thumbnail
    //     FROM
    //         `product_variants` AS t1
    //     JOIN product_variant_options AS t2
    //     ON
    //         t2.product_variant_id = t1.id
    //     JOIN products AS t3
    //     ON
    //         t3.id = t1.product_id
    //     JOIN categories AS t4
    //     ON
    //         t4.id = t3.primary_category
    //     JOIN variation_images AS t5
    //     ON
    //         t5.attribute_option_id = t2.attribute_option_id
    //     JOIN `product_categories` AS `pc` ON `pc`.`product_id` = `t1`.`product_id`
    //     WHERE
    //         t3.status = 1 AND t3.deleted_at IS NULL AND t1.deleted_at IS NULL AND t4.primary_attribute = t2.attribute_id AND t4.status = 1 AND t4.deleted_at IS NULL
    //     GROUP BY
    //         t1.product_id,
    //         t2.attribute_id*/

    //         $product = ProductVariant::selectRaw("pvo.product_variant_id, product_variants.unique_id, product_variants.name, product_variants.slug,product_variants.unique_id, product_variants.sku, product_variants.stock, product_variants.mrp, product_variants.discount, product_variants.price, p.id AS product_id, p.seller_id, vm.images, vm.thumbnail, s.company_name as seller_company_name")
    //         ->join('product_variant_options AS pvo','pvo.product_variant_id','product_variants.id')
    //         ->join('products AS p','p.id','product_variants.product_id')
    //         ->join('categories AS c','c.id','p.primary_category')
    //         ->join('brands AS b','b.id','p.brand_id')
    //         ->leftjoin('variation_images as vm', function($join){
    //             // $join->on('vm.attribute_id', '=', 'c.primary_attribute');
    //             $join->on('vm.attribute_option_id', '=', 'pvo.attribute_option_id');
    //             $join->on('vm.product_id','=','product_variants.product_id');
    //         })
    //         ->join('product_categories AS pc','pc.product_id','product_variants.product_id')
    //         ->join('sellers AS s','s.id','p.seller_id')
    //         ->whereRaw('pvo.attribute_id = c.primary_attribute AND p.status=1 AND p.deleted_at IS null AND c.status=1 AND c.deleted_at is null AND s.status=1 AND s.approval_status=1 AND s.deleted_at is null AND b.deleted_at IS NULL AND b.status = 1');


    //                     // ->whereRaw('p.status=1 AND p.deleted_at IS null AND product_variants.deleted_at IS null AND c.status=1 AND c.deleted_at is null AND s.status=1 AND s.approval_status=1 AND s.deleted_at is null AND b.deleted_at IS NULL AND b.status = 1');

    //     if(isset($filters['categories']) && !empty($filters['categories'])){
    //         $product->whereIn('pc.category_id', $filters['categories']);
    //     }
    //     if(isset($filters['subcategories']) && !empty($filters['subcategories'])){
    //         $product->whereIn('pc.subcategory_id', $filters['subcategories']);
    //     }
    //     if(isset($filters['childcategories']) && !empty($filters['childcategories'])){
    //         $product->whereIn('pc.childcategory_id', $filters['childcategories']);
    //     }
    //     if(isset($filters['brands']) && !empty($filters['brands'])){
    //         $product->whereIn('b.id', $filters['brands']);
    //     }
    //     if(isset($filters['attrs']) && !empty($filters['attrs'])){
    //         $product->whereIn('pvo.attribute_option_id', $filters['attrs']);
    //     }
    //     // else{
    //     //     $product->whereRaw("pvo.attribute_id = c.primary_attribute");
    //     // }
    //     if(isset($filters['minpr']) && !empty($filters['minpr'])  && isset($filters['maxpr']) && !empty($filters['maxpr'])){
    //         $product->whereBetween('product_variants.price', [$filters['minpr'],$filters['maxpr']]);
    //     }

    //     $product->groupBy('product_variants.product_id', 'pvo.attribute_id');

    //     //sorting through different parameters
    //     $sort = $filters['sort'];
    //     switch($sort) {
    //         case 'popularity':
    //         $products = $product->orderBy('product_variants.stock', 'DESC');
    //         break;

    //         case 'latest':
    //         $products = $product->orderBy('product_variants.created_at', 'DESC');
    //         break;

    //         case 'discount':
    //         $products = $product->orderBy('product_variants.discount', 'DESC');
    //         break;

    //         case 'price_high_to_low':
    //         $products = $product->orderBy('product_variants.price', 'DESC');
    //         break;

    //         case 'price_low_to_high':
    //         $products = $product->orderBy('product_variants.price', 'ASC');
    //         break;

    //         default:
    //         $products = $product->orderBy('product_variants.stock', 'DESC');
    //     }

    //     $offset = ($filters['page'] - 1) * 20;
    //     // return $product->offset($offset)->limit(20)->toSql();
    //     // return $product->offset($offset)->limit(20)->toSql();
    //     $products = $product->offset($offset)->limit(20)->get();

    //     $returns = [];
    //     if($products) {
    //         foreach ($products as $product) {
    //             // get discount percentage
    //             $product->discount = number_format(($product->discount * 100) / $product->mrp, 1) + 0;
    //             $images = json_decode($product->images);
    //             if($images){
    //                 if($product->thumbnail != null){
    //                     $product->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($product->thumbnail-1)]);
    //                 }else{
    //                     $product->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
    //                 }
    //                 array_push($returns, $product);
    //             }else{
    //                 $product->thumbnail_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
    //                 array_push($returns, $product);
    //             }
    //         }
    //     }
    //     return $returns;
    // }

    public static function getListingProducts($filters = [])
    {
        $products = ProductVariant::join('products AS p','p.id','product_variants.product_id')
                        ->join('categories AS c','c.id','p.primary_category')
                        ->join('product_variant_options AS pvo','pvo.product_variant_id','product_variants.id')
                        ->join('product_categories AS pc','pc.product_id','product_variants.product_id')
                        ->join('brands AS b','b.id','p.brand_id')
                        ->join('sellers AS s','s.id','p.seller_id')
                        ->join('view_categories AS vc','vc.child_id','pc.childcategory_id')
                        ->leftJoin(DB::raw("(SELECT a.product_variant_id, b.thumbnail, b.images,b.thumbnail_img_name FROM ( SELECT t1.product_id, t4.product_variant_id, t4.attribute_id, t4.attribute_option_id FROM `product_variants` AS t1 JOIN products AS t2 ON t2.id = t1.product_id JOIN categories AS t3 ON t3.id = t2.primary_category JOIN product_variant_options AS t4 ON t4.product_variant_id = t1.id WHERE t4.attribute_id = t3.primary_attribute AND t4.deleted_at IS NULL ) AS a LEFT JOIN variation_images AS b ON b.product_id = a.product_id AND b.attribute_option_id = a.attribute_option_id) AS img"),'img.product_variant_id','product_variants.id')
                        ->leftjoin('deals', function($join){
                            $join->on('deals.product_variant_id', '=', 'product_variants.id')
                                ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time AND deals.max_orders > 0');
                        })
                        ->whereRaw('product_variants.deleted_at IS NULL AND p.status=1 AND p.deleted_at IS NULL AND s.status=1 AND s.approval_status=1 AND s.deleted_at IS NULL AND b.deleted_at IS NULL AND b.status = 1 AND deals.deleted_at IS NULL');
        if(isset($filters['categories']) && !empty($filters['categories'])){
            $categories = $filters['categories'];
            $products = $products->whereIn('pc.category_id', $filters['categories']);
        }
        if(isset($filters['subcategories']) && !empty($filters['subcategories'])){
            $subcategories = $filters['subcategories'];
            $products = $products->whereIn('pc.subcategory_id', $filters['subcategories']);
        }
        if(isset($filters['childcategories']) && !empty($filters['childcategories'])){
            $childcategories = $filters['childcategories'];
            $products = $products->whereIn('pc.childcategory_id', $filters['childcategories']);
        }
        if(isset($filters['brands']) && !empty($filters['brands'])){
            $brands = $filters['brands'];
            $products = $products->whereIn('b.id', $filters['brands']);
        }
        if(isset($filters['attrs']) && !empty($filters['attrs'])){
            $attributes = $filters['attrs'];
            $products = $products->whereIn('pvo.attribute_option_id', $filters['attrs']);
        }
        if(isset($filters['minpr']) && !empty($filters['minpr'])  || isset($filters['maxpr']) && !empty($filters['maxpr'])){
            $min = $filters['minpr'];
            $max =$filters['maxpr'];
            $products = $products->whereBetween('product_variants.fabpik_seller_price', [$filters['minpr'],$filters['maxpr']]);
        }
        if(isset($filters['discount']) && !empty($filters['discount'])){
            $discount = $filters['discount'];
            $products = $products->whereRaw("CASE WHEN deals.deal_price IS NOT NULL THEN TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1) >= $discount WHEN NOW() BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1) >= $discount ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) >= $discount END");
        }
        if(isset($filters['ediscount']) && !empty($filters['ediscount'])){
            $ediscount = $filters['ediscount'];
            $products = $products->whereRaw("CASE WHEN deals.deal_price IS NOT NULL THEN TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1) = $ediscount WHEN NOW() BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1) = $ediscount ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) = $ediscount END" );
        }
        if(isset($filters['udiscount']) && !empty($filters['udiscount'])){
            $udiscount = $filters['udiscount'];
            $products = $products->whereRaw("CASE WHEN deals.deal_price IS NOT NULL THEN TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1) <= $udiscount WHEN NOW() BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_discount_percentage <= $udiscount ELSE ((product_variants.discount * 100)/product_variants.mrp) <= $udiscount END");
        }
        if($filters['q'] != null){
            $term = $filters['q'];
            $tagPIds = ProductTag::distinct('apt.product_id')
                                ->join("assigned_product_tags as apt",'apt.tag_id','=','product_tags.id')
                                ->where('product_tags.name', 'LIKE', '%'.$term.'%')
                                ->pluck('apt.product_id')
                                ->toArray();
            $tagPIds = (count($tagPIds)>0)?$tagPIds:[0];

            $products = $products->where(function($q) use($term,$tag_ids){
                    $q->where('product_variants.name', 'LIKE', '%'.$term.'%')
                        ->orWhere('product_variants.sku', 'LIKE', '%'.$term.'%')
                        ->orWhere('p.name', 'LIKE', '%'.$term.'%')
                        ->orWhere('s.company_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('vc.category_title', 'LIKE', '%'.$term.'%')
                        ->orWhere('vc.subcategory_title', 'LIKE', '%'.$term.'%')
                        ->orWhere('vc.child_title', 'LIKE', '%'.$term.'%')
                        ->orWhere('b.name', 'LIKE', '%'.$term.'%')
                        ->orWhereIn("product_variants.product_id", $tagPIds);
            });
        }
        if(!is_null($filters['tag'])){
            $pIds = AssignedProductTag::where('tag_id', $filters['tag'])
                                    ->pluck('product_id')
                                    ->toArray();
            $pIds = (count($pIds)>0)?$pIds:[0];
            $products = $products->whereIn('product_variants.product_id', $pIds);
        }
        if(!is_null($filters['coupon'])){
            $coupon = Coupon::find($filters['coupon']);
            $product_variants = [0];
            if($coupon){
                $coupon_grp = CouponGroup::find($coupon->coupon_group_id);
                if($coupon_grp){
                    $product_variants_temp = json_decode($coupon_grp->product_varient_ids);

                    $product_variants = (count($coupon_grp->product_varient_ids)>0)?$coupon_grp->product_varient_ids:[0];
                }

            }
            $products = $products->whereIn('product_variants.id', $product_variants);
        }

        //sorting through different parameters
        $sort = $filters['sort'];
        switch($sort) {
            case 'popularity':
                $products = $products->orderBy('product_variants.stock', 'DESC');
                break;
            case 'latest':
                $products = $products->orderBy('product_variants.created_at', 'DESC');
                break;
            case 'discount':
                $products = $products->orderBy('final_discount', 'DESC');
                break;
            case 'price_high_to_low':
                $products = $products->orderBy('final_price', 'DESC');
                break;
            case 'price_low_to_high':
                $products = $products->orderBy('final_price', 'ASC');
                break;
            default:
                $products = $products->orderBy('product_variants.stock', 'DESC');
        }

        if((isset($filters['udiscount']) && !empty($filters['udiscount'])) || (isset($filters['discount']) && !empty($filters['discount']))){
            $products = $products->orderBy('final_discount', 'DESC');
        }

        $products = $products->selectRaw("
                            pvo.product_variant_id,
                            product_variants.unique_id,
                            product_variants.name,
                            product_variants.slug,
                            product_variants.unique_id,
                            product_variants.sku,
                            product_variants.stock,
                            product_variants.mrp,
                            product_variants.discount,
                            product_variants.price,
                            product_variants.fabpik_seller_price,
                            product_variants.fabpik_seller_discount,
                            p.id AS product_id,
                            img.images,
                            img.thumbnail,
                            img.thumbnail_img_name,
                            p.seller_id,
                            s.company_name as seller_company_name,
                            s.store_name as seller_store_name,
                            pvo.attribute_option_id,
                            b.name as brand_name,
                            deals.start_time,
                            deals.end_time,
                            deals.deal_price,
                            c.primary_attribute,
                            c.secondary_attribute,
                            CASE WHEN deals.deal_price IS NOT NULL THEN deals.deal_price WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_price ELSE product_variants.price END AS final_price,CASE WHEN deals.deal_price IS NOT NULL THEN TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1) WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1)  ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) END AS final_discount,IF(deals.deal_price,TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1),0) AS deal_discount,TRUNCATE((product_variants.fabpik_seller_discount * 100)/product_variants.mrp,1) AS discount");

        $products = $products->groupBy(
                                'product_variants.product_id',
                                'c.primary_attribute'
                            )
                            ->paginate(20, ['*'], 'page', $filters['page']?:1);

        return ['totalResults'=>$products->total(),'products'=>$products->items(),'imgUrl'=>url(UPLOAD_PATH)];
    }

    public static function getListingProducts_old($filters=[])
    {
        $countQuery = "SELECT
                            pvo.product_variant_id
                        FROM
                            `product_variants`
                        INNER JOIN `products` AS `p`
                        ON
                            `p`.`id` = `product_variants`.`product_id`
                        INNER JOIN `categories` AS `c`
                        ON
                            `c`.`id` = `p`.`primary_category`
                        INNER JOIN `product_variant_options` AS `pvo`
                        ON
                            `pvo`.`product_variant_id` = `product_variants`.`id`
                        INNER JOIN `product_categories` AS `pc`
                        ON
                            `pc`.`product_id` = `product_variants`.`product_id`
                        INNER JOIN `brands` AS `b`
                        ON
                            `b`.`id` = `p`.`brand_id`
                        INNER JOIN `view_categories` AS `vc`
                        ON
                            `vc`.`child_id` = `pc`.`childcategory_id`
                        LEFT JOIN(
                            SELECT
                                a.product_variant_id,
                                b.thumbnail,
                                b.images
                            FROM
                                (
                                SELECT
                                    t1.product_id,
                                    t4.product_variant_id,
                                    t4.attribute_id,
                                    t4.attribute_option_id
                                FROM
                                    `product_variants` AS t1
                                JOIN products AS t2
                                ON
                                    t2.id = t1.product_id
                                JOIN categories AS t3
                                ON
                                    t3.id = t2.primary_category
                                JOIN product_variant_options AS t4
                                ON
                                    t4.product_variant_id = t1.id
                                WHERE
                                    t4.attribute_id = t3.primary_attribute AND t4.deleted_at IS NULL
                            ) AS a
                        LEFT JOIN variation_images AS b
                        ON
                            b.product_id = a.product_id AND b.attribute_option_id = a.attribute_option_id
                        ) AS img
                        ON
                            `img`.`product_variant_id` = `product_variants`.`id`
                        LEFT JOIN `sellers` AS `s`
                        ON
                            `s`.`id` = `p`.`seller_id`
                        LEFT JOIN `deals`
                         ON `deals`.`product_variant_id` = `product_variants`.`id` AND NOW() BETWEEN deals.start_time AND deals.end_time AND deals.max_orders > 0
                        WHERE
                            product_variants.deleted_at IS NULL AND p.status = 1 AND p.deleted_at IS NULL AND s.status = 1 AND s.approval_status = 1 AND s.deleted_at IS NULL AND b.deleted_at IS NULL AND b.status = 1  AND `product_variants`.`deleted_at` IS NULL";

        $products = ProductVariant::join('products AS p','p.id','product_variants.product_id')
                        ->join('categories AS c','c.id','p.primary_category')
                        ->join('product_variant_options AS pvo','pvo.product_variant_id','product_variants.id')
                        ->join('product_categories AS pc','pc.product_id','product_variants.product_id')
                        ->join('brands AS b','b.id','p.brand_id')
                        ->join('view_categories AS vc','vc.child_id','pc.childcategory_id')
                        ->leftJoin(DB::raw("(SELECT a.product_variant_id, b.thumbnail, b.images,b.thumbnail_img_name FROM ( SELECT t1.product_id, t4.product_variant_id, t4.attribute_id, t4.attribute_option_id FROM `product_variants` AS t1 JOIN products AS t2 ON t2.id = t1.product_id JOIN categories AS t3 ON t3.id = t2.primary_category JOIN product_variant_options AS t4 ON t4.product_variant_id = t1.id WHERE t4.attribute_id = t3.primary_attribute AND t4.deleted_at IS NULL ) AS a LEFT JOIN variation_images AS b ON b.product_id = a.product_id AND b.attribute_option_id = a.attribute_option_id) AS img"),'img.product_variant_id','product_variants.id')
                        ->leftjoin('deals', function($join){
                            $join->on('deals.product_variant_id', '=', 'product_variants.id')
                                 ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time AND deals.max_orders > 0');
                        })
                        ->leftJoin('sellers AS s','s.id','p.seller_id')
                        ->whereRaw('product_variants.deleted_at IS NULL AND p.status=1 AND p.deleted_at IS NULL AND s.status=1 AND s.approval_status=1 AND s.deleted_at IS NULL AND b.deleted_at IS NULL AND b.status = 1 AND deals.deleted_at IS NULL');

        if(isset($filters['categories']) && !empty($filters['categories'])){
            $categories = $filters['categories'];
            $countQuery .= " AND pc.category_id IN (".implode(",", $categories).")";
            $products->whereIn('pc.category_id', $filters['categories']);
        }
        if(isset($filters['subcategories']) && !empty($filters['subcategories'])){
            $subcategories = $filters['subcategories'];
            $countQuery .= " AND pc.subcategory_id IN (".implode(",", $subcategories).")";
            $products->whereIn('pc.subcategory_id', $filters['subcategories']);
        }
        if(isset($filters['childcategories']) && !empty($filters['childcategories'])){
            $childcategories = $filters['childcategories'];
            $countQuery .= " AND pc.childcategory_id IN (".implode(",", $childcategories).")";
            $products->whereIn('pc.childcategory_id', $filters['childcategories']);
        }
        if(isset($filters['brands']) && !empty($filters['brands'])){
            $brands = $filters['brands'];
            $countQuery .= " AND b.id IN (".implode(",", $brands).")";
            $products->whereIn('b.id', $filters['brands']);
        }
        if(isset($filters['attrs']) && !empty($filters['attrs'])){
            $attributes = $filters['attrs'];
            $countQuery .= " AND pvo.attribute_option_id IN (".implode(",", $attributes).")";

            $products->whereIn('pvo.attribute_option_id', $filters['attrs']);
        }
        if(isset($filters['minpr']) && !empty($filters['minpr'])  || isset($filters['maxpr']) && !empty($filters['maxpr'])){
            $min = $filters['minpr'];
            $max =$filters['maxpr'];
            $countQuery .= " AND product_variants.fabpik_seller_price BETWEEN $min AND $max";
            $products->whereBetween('product_variants.fabpik_seller_price', [$filters['minpr'],$filters['maxpr']]);
        }
        if(isset($filters['discount']) && !empty($filters['discount'])){
            $discount = $filters['discount'];
            // $products->whereRaw("concat(round(((product_variants.fabpik_seller_discount / product_variants.mrp) * 100 ),2), '%') >= ".$filters['discount']);
            $products->whereRaw("CASE WHEN deals.deal_price IS NOT NULL THEN TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1) >= $discount WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1) >= $discount ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) >= $discount END");
            $countQuery .= " AND CASE WHEN deals.deal_price IS NOT NULL THEN TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1) >= $discount WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1) >= $discount ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) >= $discount END";
            // $products->where("final_discount",">=",$filters['discount']);
        }
        if(isset($filters['ediscount']) && !empty($filters['ediscount'])){
            $ediscount = $filters['ediscount'];
            // $countQuery .= " AND concat(round(((product_variants.fabpik_seller_discount / product_variants.mrp) * 100 ),2), '%') = $ediscount";
            $countQuery .= " AND CASE WHEN deals.deal_price IS NOT NULL THEN TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1) = $ediscount WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1) = $ediscount  ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) = $ediscount END";
            // $products->whereRaw("concat(round(((product_variants.fabpik_seller_discount / product_variants.mrp) * 100 ),2), '%') = ".$filters['ediscount']);
            $products->whereRaw("CASE WHEN deals.deal_price IS NOT NULL THEN TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1) = $ediscount WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1) = $ediscount ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) = $ediscount END" );
        }
        if(isset($filters['udiscount']) && !empty($filters['udiscount'])){
            $udiscount = $filters['udiscount'];
            // $countQuery .= " AND concat(round(((product_variants.fabpik_seller_discount / product_variants.mrp) * 100 ),2), '%') <= $udiscount";
            $countQuery .= " AND CASE WHEN deals.deal_price IS NOT NULL THEN TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1) <= $udiscount WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1) <= $udiscount ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) <= $udiscount END";
            // $products->whereRaw("concat(round(((product_variants.fabpik_seller_discount / product_variants.mrp) * 100 ),2), '%') <= ".$filters['udiscount']);
            $products->whereRaw("CASE WHEN deals.deal_price IS NOT NULL THEN TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1) <= $udiscount WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_discount_percentage <= $udiscount ELSE ((product_variants.discount * 100)/product_variants.mrp) <= $udiscount END");

            // $products->where("final_discount","<=",$filters['udiscount']);
        }
        if($filters['q'] != null){
            $term = $filters['q'];
            $tag_ids = ProductTag::join("assigned_product_tags as apt",'apt.tag_id','=','product_tags.id')
                                    ->where('product_tags.name', 'LIKE', '%'.$term.'%')
                                    ->pluck('apt.product_id');
            if(count($tag_ids) <= 0){
                $tag_ids = "[0]";
            }
            $countQuery .= " AND (product_variants.name LIKE '%{$term}%' OR product_variants.sku LIKE '%{$term}%' OR p.name LIKE '%{$term}%' OR s.company_name LIKE '%{$term}%' OR vc.category_title LIKE '%{$term}%' OR vc.subcategory_title LIKE '%{$term}%' OR vc.child_title LIKE '%{$term}%' OR b.name LIKE '%{$term}%' OR product_variants.product_id IN (".implode(",",json_decode($tag_ids))."))";
            $products->where(function($q) use($term,$tag_ids){
                    $q->where('product_variants.name', 'LIKE', '%'.$term.'%')
                        ->orWhere('product_variants.sku', 'LIKE', '%'.$term.'%')
                        ->orWhere('p.name', 'LIKE', '%'.$term.'%')
                        ->orWhere('s.company_name', 'LIKE', '%'.$term.'%')
                        ->orWhere('vc.category_title', 'LIKE', '%'.$term.'%')
                        ->orWhere('vc.subcategory_title', 'LIKE', '%'.$term.'%')
                        ->orWhere('vc.child_title', 'LIKE', '%'.$term.'%')
                        ->orWhere('b.name', 'LIKE', '%'.$term.'%')
                        ->orWhereIn("product_variants.product_id",json_decode($tag_ids));
            });
        }
        if($filters['tag'] != null){
            $tag_id = $filters['tag'];
            $p_ids = AssignedProductTag::where('tag_id','=',$tag_id)->pluck('product_id');
            if(count($p_ids) <= 0){
                $p_ids = "[0]";
            }
            $countQuery .= " AND product_variants.product_id IN (".implode(",", json_decode($p_ids)).")";
            $products->whereIn('product_variants.product_id', json_decode($p_ids));
        }
        if($filters['coupon'] != null){
            $coupon_id = $filters['coupon'];
            $coupon = Coupon::find($coupon_id);
            if($coupon){
                $coupon_grp = CouponGroup::find($coupon->coupon_group_id);
                if($coupon_grp){
                    $product_variants_temp = json_decode($coupon_grp->product_varient_ids);
                    $product_variants = $coupon_grp->product_varient_ids;
                    if(count($product_variants_temp) <= 0){
                        $product_variants = "[0]";
                    }
                }else{
                    $product_variants = "[0]";
                }
                $countQuery .= " AND product_variants.id IN (".implode(",",json_decode($product_variants)).")";
                $products->whereIn('product_variants.id', json_decode($product_variants));
            }else{
                $product_variants = "[0]";
                $countQuery .= " AND product_variants.id IN (".implode(",",json_decode($product_variants)).")";
                $products->whereIn('product_variants.id', json_decode($product_variants));
            }
        }

        $countQuery .= "
                        GROUP BY
                            `product_variants`.`product_id`,
                            `c`.`primary_attribute`";

        $products->groupBy('product_variants.product_id', 'c.primary_attribute');
        $returns = ['totalResults'=>null,'products'=>[],'imgUrl'=>url(UPLOAD_PATH)];

        $returns['totalResults'] = DB::select("SELECT COUNT(t1.product_variant_id) AS totalCount FROM (".$countQuery.") AS t1")[0]->totalCount;
        //sorting through different parameters
        $sort = $filters['sort'];
        switch($sort) {
            case 'popularity':
                $products = $products->orderBy('product_variants.stock', 'DESC');
                break;
            case 'latest':
                $products = $products->orderBy('product_variants.created_at', 'DESC');
                break;
            case 'discount':
                $products = $products->orderBy('final_discount', 'DESC');
                break;
            case 'price_high_to_low':
                $products = $products->orderBy('final_price', 'DESC');
                break;
            case 'price_low_to_high':
                $products = $products->orderBy('final_price', 'ASC');
                break;
            default:
                $products = $products->orderBy('product_variants.stock', 'DESC');
        }

        if((isset($filters['udiscount']) && !empty($filters['udiscount'])) || (isset($filters['discount']) && !empty($filters['discount']))){
            $products = $products->orderBy('final_discount', 'DESC');
        }

        $products = $products->selectRaw("pvo.product_variant_id, product_variants.unique_id, product_variants.name, product_variants.slug,product_variants.unique_id, product_variants.sku, product_variants.stock, product_variants.mrp, product_variants.discount, product_variants.price,product_variants.fabpik_seller_price, product_variants.fabpik_seller_discount, p.id AS product_id, img.images, img.thumbnail,img.thumbnail_img_name, p.seller_id, s.company_name as seller_company_name,s.store_name as seller_store_name,pvo.attribute_option_id,b.name as brand_name,deals.start_time,deals.end_time,deals.deal_price,c.primary_attribute,c.secondary_attribute,CASE WHEN deals.deal_price IS NOT NULL THEN deals.deal_price WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_price ELSE product_variants.price END AS final_price,CASE WHEN deals.deal_price IS NOT NULL THEN TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1) WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1)  ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) END AS final_discount,IF(deals.deal_price,TRUNCATE((product_variants.mrp - deals.deal_price) / product_variants.mrp * 100,1),0) AS deal_discount,TRUNCATE((product_variants.fabpik_seller_discount * 100)/product_variants.mrp,1) AS discount");

        $offset = ($filters['page'] - 1) * 20;
        // return $products->offset($offset)->limit(20)->toSql();
        $products = $products->offset($offset)->limit(20)->get();

        $returns['products'] = $products;

        return $returns;
    }

    /**
     * Return a image path.
     *
     *
     */
    public static function getProductimagespath($products)
    {
        /*foreach($products as $product)
        {
            $thumbnail = $product->thumbnail;
            $images = explode(',', trim($product->images,"[]"));
            $imgarr=array();
            foreach($images as $key => $image)
            {
              $imgarr[ $key+1 ] = $image;

            }

            $thumbnail = isset($imgarr[$thumbnail])?trim($imgarr[$thumbnail], '"'):'';
            $product->images = url(UPLOAD_PATH.'/'.$thumbnail);
        }*/
        $returns = [];
        if($products) {
            foreach ($products as $product) {
                $images = json_decode($product->images);
                $product->thumbnail_path = url(UPLOAD_PATH.'/'.$images[($product->thumbnail-1)]);
                array_push($returns, $product);
            }
        }

       return $returns;
    }

    /**
     * Return a product details image path.
     *
     *
     */
    public static function getProductdetailimagespath($productdetails)
    {
        foreach($productdetails as $product)
        {
            $store_name =  Seller::select('company_name')->where('id', $product['seller_id'])->get();
            $thumbnail = $product['thumbnail'];
            $images = explode(',', trim($product['images'],"[]"));
            $imgarr=array();
            foreach($images as $key => $image)
            {
              $imgarr[ $key+1 ] = $image;

            }

            $thumbnail = isset($imgarr[$thumbnail])?trim($imgarr[$thumbnail], '"'):'';
            $product['images'] = url(PRODUCT_IMAGE_UPLOAD_PATH.'/'.$thumbnail);
            $product['seller'] = $store_name;

        }
       return $productdetails;
    }

     /**
     * Return a product details with search parameters
     *
     *
     */
    public static function getSearchData($params)
    {
        $product	= DB::table('product_variants AS pv')
                        ->select ([ 'p.id AS product_id', 'pvo.product_variant_id' , 'pv.name' , 'pv.mrp' , 'pv.discount' , 'pv.price' , 'pv.slug' , 'p.id' , 'p.seller_id' , 'vm.images' , 'vm.thumbnail' ])
                            ->join('product_variant_options AS pvo','pv.id','pvo.product_variant_id')
                            ->join('products AS p','pv.product_id','p.id')
                            ->join('categories AS c','p.primary_category','c.id')
                            ->join('variation_images as vm','vm.attribute_option_id','=','pvo.attribute_option_id')
                            ->join('product_categories AS pc','pv.product_id','pc.product_id')
                            ->whereRaw('pvo.attribute_id = c.primary_attribute')
                            ->whereIn('pc.childcategory_id', function($query) use ($params)
                            {
                                $query->select(DB::raw('id'))
                                    ->from('childcategories as cc');
                                    if($params->name && $params->product_cat){
                                        $query->whereRaw('lower(cc.title) like (?)',["{$params->name}%"])->whereRaw('cc.category_id = '.$params->product_cat);
                                    }
                                    else if($params->name){
                                        $query->whereRaw('lower(cc.title) like (?)',["{$params->name}%"]);
                                    }
                                    else if($params->product_cat){
                                        $query->whereRaw('cc.category_id = '.$params->product_cat);
                                    }

                            })
                            ->groupBy('pvo.product_variant_id')
                            ->get();
        return $product;

    }

    public static function getAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    )
    {
        $products = Product::with(['category', 'seller', 'productTags', 'productcategory' => function($q){
            $q->where('is_active','=', 1);
        }] )->orderBy($orderColumn, $orderDir);

        if (isset($criteria->seller)) { $products = $products->where('seller_id', $criteria->seller); }
        if($criteria->fname) $products = $products->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $products = $products->where('status', $criteria->fstatus);
        if(!is_null($criteria->fseller)) $products = $products->where('seller_id', $criteria->fseller);
        if(!is_null($criteria->fpricefrom)) $products = $products->where('sell_price','>=', $criteria->fpricefrom);
        if(!is_null($criteria->fpriceto)) $products = $products->where('sell_price','<=', $criteria->fpriceto);
        if(!is_null($criteria->ffromdate)) $products = $products->where('created_at','>=', $criteria->ffromdate);
        if(!is_null($criteria->ftodate)) $products = $products->where('created_at','<=', $criteria->ftodate);
        if(!is_null($criteria->fsku)) $products = $products->where('sku', 'LIKE', "%{$criteria->fsku}%");
        if(!is_null($criteria->funiqueid)) $products = $products->where('unique_id', 'LIKE', "%{$criteria->funiqueid}%");
        if(isset($criteria->fbrand) && !is_null($criteria->fbrand)) $products = $products->where('brand_id', $criteria->fbrand);
        if(!is_null($criteria->fcategory)) {
            @list($categoryId,$subCategoryId,$childCategoryId) = explode('-', $criteria->fcategory);
            $products->whereHas('productcategory', function ($q) use ($childCategoryId){
                $q->where('childcategory_id','=', $childCategoryId);
            });
        }

        if(!is_null($criteria->fpcategory)) $products = $products->whereHas('productcategory', function($q) use ($criteria){
            $q->where('category_id', $criteria->fpcategory);
        });

        if(isset($criteria->freference) && !is_null($criteria->freference)) $products = $products->whereHas('productTags', function($q) use ($criteria){
            $q->where('tag_id', $criteria->freference);
        });
        // dd($criteria->fpcategory);

        return $products->paginate(intval($criteria->length), ['id','name','thumbnail','sku','seller_id','unique_id','primary_category','created_at','updated_at', 'status'], 'page', $page);
    }

    /**
     * get all primary attribute options
     */
    public static function getPrimaryAttrOptions($pid)
    {
        return Product::join('categories as t2','t2.id','=','products.primary_category')
                    ->join('attributes as t3','t3.id','=','t2.primary_attribute')
                    ->join('attribute_options as t4','t4.attribute_id','=','t3.id')
                    ->where('products.id',$pid)
                    ->where('t4.deleted_at',null)
                    ->selectRaw("products.id as product_id, t3.name as attr_name, t3.display_name as attr_display_name, t3.attr_type, t4.id as option_id, t4.attribute_id, t4.option_name, t4.colour_code")
                    ->get();
    }

    /**
     * get all secondary attribute options
     */
    public static function getSecondaryAttrOptions($pid)
    {
        return Product::join('categories as t2','t2.id','=','products.primary_category')
                    ->join('attributes as t3','t3.id','=','t2.secondary_attribute')
                    ->join('attribute_options as t4','t4.attribute_id','=','t3.id')
                    ->where('products.id',$pid)
                    ->where('t4.deleted_at',null)
                    ->selectRaw("products.id as product_id, t3.name as attr_name, t3.display_name as attr_display_name, t3.attr_type, t4.id as option_id, t4.attribute_id, t4.option_name, t4.colour_code")
                    ->get();
    }

    /**
     * Export Products
    */
    public static function getExportData(
        $criteria=[],
        $orderColumn='id',
        $orderDir='desc')
    {
        $products = Product::with('category','seller')->orderBy($orderColumn, $orderDir);
        if (isset($criteria->seller))  { $products = $products->where('seller_id', $criteria->seller); }
        if($criteria->fname) $products = $products->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $products = $products->where('status', $criteria->fstatus);
        return $products->get();
    }

    public static function getProductFilters($productdata = [])
    {
        $variants = DB::table('categories as t1')
                    ->join('subcategories as t2', 't2.category_id', '=', 't1.id')
                    ->join('childcategories as t3', 't3.subcategory_id', '=', 't2.id')
                    ->join('product_categories as t4', 't4.childcategory_id', '=', 't3.id')
                    ->join('products as t5', 't5.id', '=', 't4.product_id')
                    ->join('product_variants as t6', 't6.product_id', '=', 't5.id')
                    ->join('product_variant_options as t7', 't7.product_variant_id', '=', 't6.id')
                    ->join('attributes as t8','t8.id','=','t7.attribute_id')
                    ->join('attribute_options as t9','t9.id','=','t7.attribute_option_id')
                    ->where('t2.id', $productdata['subCategoryId'])
                    ->where('t1.status', 1)
                    ->where('t2.status', 1)
                    ->where('t3.status', 1)
                    ->where('t5.status', 1)
                    ->where('t8.status', 1)
                    ->where('t9.status', 1);
        $variants->selectRaw("distinct t8.display_name ,t8.id,t8.attr_type, t9.colour_code,t9.option_name,t7.attribute_id");
     return $variants->get();
    }

    public static function getProductBrands($parentCategory,$subCategory,$childCategories){

        $brands = ProductCategory::selectRaw("distinct brands.name,brands.id")
                                    ->join("products","products.id",'=','product_categories.product_id')
                                    ->join('brands','brands.id','=','products.brand_id')
                                    ->where('category_id','=',$parentCategory->id)
                                    ->where('subcategory_id','=',$subCategory->id)
                                    ->get();
        return $brands;
    }

    public static function getMinMaxPrice($parentCategory,$subCategory,$childCategories)
    {
        $price = ProductCategory::selectRaw("min(product_variants.mrp) AS min, max(product_variants.mrp) AS max")
                                    ->join("products","products.id",'=','product_categories.product_id')
                                    ->join('product_variants','product_variants.product_id','=','products.id')
                                    ->where('product_categories.category_id','=',$parentCategory->id)
                                    ->where('product_categories.subcategory_id','=',$subCategory->id)
                                    ->first();
        if(!is_null($price->max) && !is_null($price->min)){
            return $price;
        }else{
            $price = ['min'=>0,'max'=>10000];
            return $price;
        }
    }

    public static function getProductAttributes($pcategoryid){
        $parentCategory = Category::where('id',$pcategoryid)->first();
        $primaryAttributeName = Attribute::where('id','=',$parentCategory->primary_attribute)->first();
        $primaryAttributeOptions = AttributeOption::where('attribute_id','=',$parentCategory->primary_attribute)->get();
        $secondaryAttributeOptions = [];
        $secondaryAttributeName =null;
        if(!is_null($parentCategory->secondary_attribute)) {
            $secondaryAttributeName = Attribute::where('id','=',$parentCategory->secondary_attribute)->first();
            $secondaryAttributeOptions = AttributeOption::where('attribute_id','=',$parentCategory->secondary_attribute)->get();
        }
        $attributes = ['primaryAttributeName'=>$primaryAttributeName->display_name,
                       'primary_attribute_type'=>$primaryAttributeName->attr_type,
                       'secondaryAttributeName'=>(isset($secondaryAttributeName)) ? $secondaryAttributeName->display_name:'',
                       'secondary_attribute_type'=>(isset($secondaryAttributeName)) ? $secondaryAttributeName->attr_type:'',
                       'primaryAttributeOptions'=>$primaryAttributeOptions,
                       'secondaryAttributeOptions'=>$secondaryAttributeOptions,
                    ];
        return $attributes;
    }

    public static function getFrontendFilteredData($min,$max,$params,$filters = [])
    {


        $products = DB::table('product_variants as t1')
                    ->join('product_variant_options as t2','t2.product_variant_id','=','t1.id')
                    ->join('products as t3','t3.id','=','t1.product_id')
                    // ->join('categories as c', function ($join) {
                    //     $join->onor( 't2.t_mst_vendor_id', '=', 't7.t_mst_vendor_id')
                    //          ->on( 't6.t_mst_state_id', '=', 't7.t_mst_state_id');
                    // })
                    ->join('product_categories as t4','t4.product_id','=','t3.id')
                    ->join('categories as t5','t5.id','=','t4.category_id')
                    ->join('attributes as t6','t6.id','=','t2.attribute_id')
                    ->join('attribute_options as t7','t7.id','=','t2.attribute_option_id')
                    ->join('brands as t8','t8.id','=','t3.brand_id');
       $count=count($filters['variants']);
       $count2=count($filters['brands']);
       $count3=count($filters);

        if($filters){
            $entered=0;
            if($filters['variants']){
                foreach ($filters['variants'] as $key => $value) {
                    if(isset($value['varId']) && !is_null($value['varId'])) {
                        foreach ($value['varVal'] as $key2 => $value2) {
                            if($key==1 && $entered==0){
                                if($key==count($filters['variants']) && $key2==count($value['varVal'])-1){
                                    $products->orwhereRaw("( (t7.attribute_id =".$value['varId']." AND t7.option_name='".$value2."') )");
                                }
                                else
                                {
                                    $products->orwhereRaw("( (t7.attribute_id =".$value['varId']." AND t7.option_name='".$value2."') ");
                                }
                                $entered=1;
                            }
                            else if($key==count($filters['variants']) && $key2==count($value['varVal'])-1){
                                $products->orwhereRaw("(t7.attribute_id =".$value['varId']." AND t7.option_name='".$value2."') )");
                            }
                            else{
                                $products->orwhereRaw("(t7.attribute_id =".$value['varId']." AND t7.option_name='".$value2."')");
                            }
                        }
                    }
                }
            }

            $entered=0;
            if(isset($filters['brands'])){
                foreach($filters['brands'] as $key4 => $value4) {
                    if($key4==0 && $entered==0){
                        if($key4==count($filters['brands'])-1){
                            $products->whereRaw("( (t3.brand_id ='".$value4."') )");
                        }
                        else
                        {
                             $products->whereRaw("( (t3.brand_id ='".$value4."') ");
                        }
                        $entered=1;
                    }
                    else if($key4==count($filters['brands'])-1){
                        $products->orwhereRaw(" (t3.brand_id ='".$value4."')  )");
                    }
                    else{
                        $products->orwhereRaw("(t3.brand_id ='".$value4."')");
                    }
                }
           }

           $entered=0;
           if(isset($filters['childcategory'])){
            foreach($filters['childcategory'] as $key4 => $value4) {
                if($key4==0 && $entered==0){
                    if($key4==count($filters['childcategory'])-1){
                        $products->whereRaw("( (t4.childcategory_id='".$value4."') )");
                    }
                    else
                    {
                         $products->whereRaw("( (t4.childcategory_id='".$value4."') ");
                    }
                    $entered=1;
                }
                else if($key4==count($filters['childcategory'])-1)
                {
                    $products->orwhereRaw("(t4.childcategory_id='".$value4."')  )");
                }
                else{
                    $products->orwhereRaw("(t4.childcategory_id='".$value4."')");
                }

                  }

       }


            // $products->where('t2.status', 1);
            // $products->where('t6.status', 1);
            // $products->where('t7.status', 1);
            $products->whereBetween('t1.price',[$min-1,$max+1]);
            $products->selectRaw("t2.product_variant_id,t1.name,t3.id, t3.thumbnail,t7.option_name, t3.images ,t1.mrp ,t1.discount, t3.sell_price,t1.price ,t1.slug ,t1.name, t3.seller_id ");
            $products=$products->get();
            // $products=$products->tosql();
            // print_r($products);

        }

        $new_products=[];
        $old_array=[];
        $new_products_arr=[];

        foreach ($products as $key11 => $value11) {
            //    print_r($value11);
            $old_array[]=$value11->product_variant_id;


         }
         $new_array=array_count_values($old_array);

         $fabcount=0;

        //  foreach ($filters as $key15 => $value15)
        //   {
        //     if(count($value15)!=0 && $key15!='variants')
        //     {
        //       $fabcount+=1;
        //     }
        //   }

        //   if($fabcount>0)
        //   {
        //       if(count($filters['childcategory'])>0 && $fabcount==1)
        //           $fabcount+=$count;
        //       else{
        //           $fabcount+=1;
        //       }
        //   }

         foreach ($products as $key12 => $value12) {
            if($filters){
                if($fabcount == $new_array[$value12->product_variant_id]
                && array_key_exists($value12->product_variant_id,$new_array)
                && !in_array($value12->product_variant_id,$new_products_arr)){
                    $new_products[]=$value12;
                    // print_r($new_array[$value12->product_variant_id]);
                    $new_products_arr[]=$value12->product_variant_id;
                }

                else{
                    if(array_key_exists($value12->product_variant_id,$new_array)
                     && !in_array($value12->product_variant_id,$new_products_arr)){

                       $new_products[]=$value12;
                        $new_products_arr[]=$value12->product_variant_id;
                    }
                }
            //    }

            }
        }
         $i=0;

         foreach ($new_products as $key13 => $value13)
         {
         $data	= DB::table('product_variant_options AS pvo')
         ->select([DB::raw('group_concat(DISTINCT ao.option_name ) as option_name'), DB::raw('group_concat(DISTINCT ao.colour_code ) as color_code') ,'pvo.product_variant_id','a.display_name'])
         ->join('attribute_options AS ao','ao.id','pvo.attribute_option_id')
         ->join('attributes AS a','a.id','ao.attribute_id')
         ->where('pvo.product_variant_id','=' , $value13->product_variant_id)
         ->groupBy('a.display_name')->get();
              foreach($data as $key => $d){
                 if($d->display_name == 'Size')
                 { $options = explode(",",$d->option_name);

                       $d->option_name = $options;
                     $new_products[$i]->variantsize = $d;
                 }
                 if($d->display_name == 'Color'){
                     $options = explode(",",$d->option_name);
                 $options = explode(",",$d->color_code);
                     $d->color_code = $options;
                     $new_products[$i]->variantcolor = $d;
                 }
             }
     $seller = Seller::with('product')->select('company_name')->where('id',$value13->seller_id)->first();
     $new_products[$i]->seller = $seller->toArray();
     $i++;
     }
        $productsdata = Product::getProductimagespath($new_products);
        return new ProductCollection($productsdata);
        // return $new_products;

    }
    public static function productDetails($productVariantSlug,$uniqueId)
    {
       $product = ProductVariant::select("product_variants.name as pv_name","product_variants.description as pv_desc","product_variants.product_id","product_variants.id as product_variant_id","product_variants.slug","product_variants.mrp as pv_mrp","product_variants.discount","product_variants.fabpik_seller_price","product_variants.fabpik_seller_discount","product_variants.price as pv_price","product_variants.stock","product_variants.min_order_qty","product_variants.unique_id","p.dress_material","p.seller_discount","p.sell_price","p.tax","p.description as p_desc","vm.images","vm.thumbnail","c.title as cname","c.primary_attribute","c.secondary_attribute","sc.title as scname","cc.title as ccname","cc.id as cc_id","p.washing_type as wash_name","p.iron_type as iron_name","size_charts.image as sizechart_image",'p.items_in_package',"p.no_of_items",'s.url_slug AS seller_url_slug','s.id AS seller_id','s.store_name','product_variants.shipping_weight','product_variants.shipping_length',"product_variants.shipping_breadth","product_variants.shipping_height",'wishlists.product_variant_id AS wishlist','p.primary_category','p.meta_title','p.meta_keywords','p.meta_description',"deals.start_time","deals.end_time","deals.deal_price","deals.id as deal_id","p.unit","size_charts.image as sizechart_image","b.name as brand_name",DB::raw("(CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_price ELSE product_variants.price END) AS final_price"),DB::raw("(CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount_percentage,1)  ELSE TRUNCATE((product_variants.discount * 100)/product_variants.mrp,1) END) AS final_discount"),DB::raw("IF(
        deals.deal_price,
        TRUNCATE((
            product_variants.mrp - deals.deal_price
        ) / product_variants.mrp * 100,1),
        0
    ) AS deal_discount"))

    //    "psc.sizes","psc.columns","psc.size_chart_data","psc.notes","psc.title as product_size_chart_title","sim.images as size_chart_images","sim.title as size_chart_image_title","psc.id as size_chart_id",
                                    ->join('products as p', 'p.id', '=', 'product_variants.product_id')
                                    ->join('sellers as s', 's.id', '=', 'p.seller_id')
                                    ->join('brands AS b','b.id','p.brand_id')
                                    ->join('product_categories as pc', 'pc.product_id', '=', 'product_variants.product_id')
                                    ->join('categories AS c','c.id','p.primary_category')
                                    ->join('subcategories as sc','sc.id','pc.subcategory_id')
                                    ->join('childcategories as cc', 'cc.id', '=', 'pc.childcategory_id')
                                    ->join('product_variant_options AS pvo','pvo.product_variant_id','product_variants.id')
                                    ->leftjoin('variation_images as vm', function($join){
                                        $join->on('vm.attribute_option_id', '=', 'pvo.attribute_option_id');
                                        $join->on('vm.product_id','=','p.id');
                                    })
                                    ->leftjoin('deals', function($join){
                                        $join->on('deals.product_variant_id', '=', 'product_variants.id')
                                            ->whereRaw('NOW() BETWEEN deals.start_time AND deals.end_time AND deals.max_orders > 0');
                                    })
                                    // ->leftjoin('product_size_charts as psc','psc.product_id','=','p.id')
                                    // ->leftjoin('sizechart_image_master as sim','sim.id','=','psc.images')
                                    ->leftjoin('wishlists', 'wishlists.product_variant_id', '=', 'product_variants.id')
                                    ->leftjoin('size_charts', 'size_charts.id', '=', 'p.size_chart_id')
                                    ->groupBy('pc.product_id')
                                    ->whereRaw("product_variants.unique_id = '$uniqueId'  AND pvo.attribute_id = c.primary_attribute AND p.status=1 AND p.deleted_at IS NULL AND product_variants.deleted_at IS NULL AND c.status=1 AND c.deleted_at IS NULL AND wishlists.deleted_at IS NULL AND deals.deleted_at IS NULL AND s.status = 1 AND pvo.deleted_at IS NULL")
                                    ->first();
        // return ['product'=>$product];
        if($product == null){
            return null;
        }
        // $coupons = Coupon::where('status','=', '1')
        //                    ->where('start_date', '<=', (string)date('Y-m-d H:i:s'))
        //                    ->where('expiry_date', '>=', (string)date('Y-m-d H:i:s'))
        //                 //    ->whereJsonContains('')
        //                    ->get();
        if(auth('api')->user()){
            $mobile_number = auth('api')->user()->mobile;
        }else{
            $mobile_number = null;
        }
        $currentDate = (string)date('Y-m-d H:i:s');
        $seller_id = $product->seller_id;
        $primary_category = (string)$product->primary_category;
        $p_variant = (string)$product->product_variant_id;
        $coupons = Coupon::leftjoin("coupon_groups","coupon_groups.id","=","coupons.coupon_group_id")
                            ->whereRaw("coupons.status = 1 AND coupons.start_date <= '$currentDate' AND coupons.expiry_date >= '$currentDate' AND ((coupons.seller_ids is null AND coupons.category_ids is null AND coupons.product_varient_ids is null AND coupon_groups.product_varient_ids is null AND coupons.mobile_numbers IS NULL) OR (json_contains(coupons.seller_ids, '\"$seller_id\"') OR json_contains(coupons.category_ids,'\"$primary_category\"') OR json_contains(coupons.product_varient_ids,'\"$p_variant\"') OR json_contains(coupons.mobile_numbers,'\"$mobile_number\"') OR json_contains(coupon_groups.product_varient_ids,'\"$p_variant\"') ))")
                            ->get();
        $couponsTemp= [];
        foreach ($coupons as $coupon) {
            if($coupon->dis_type == 'f' && $coupon->sub_type == 's'){
                $totalSaving = 0;
                $totalSaving += $product->discount;
                $totalSaving += $coupon->amount;
                $coupon->savings = $totalSaving;
                array_push($couponsTemp,$coupon);
            }else if($coupon->dis_type == 'p' && $coupon->sub_type == 's'){
                $totalSaving = 0;
                $couponDiscountAmount = 0;
                $totalDiscountOfaProductInRupees = 0;
                $couponDiscountAmount = ($coupon->amount/100) * $product->pv_price;
                $totalDiscountOfaProductInRupees = $couponDiscountAmount + $product->discount;
                $totalSaving += $totalDiscountOfaProductInRupees;
                $coupon->savings = $totalSaving;
                array_push($couponsTemp,$coupon);
            }else if($coupon->dis_type == 'p' && $coupon->sub_type == 'm'){
                $totalSaving = 0;
                $productsDiscounts = [];
                $couponDiscountAmount = ($coupon->amount/100) * ($product->pv_mrp * 1);
                if($couponDiscountAmount > $product->discount){
                    $totalSaving += $couponDiscountAmount;
                }else{
                    $totalSaving += $product->discount;
                }
                $coupon->savings = $totalSaving;
                array_push($couponsTemp,$coupon);
            }
        }
        if($product) {
                $imagesWithPath = [];
                if($product->deal_id != null){
                    $deal_start_time  = strtotime(date('Y-m-d H:i:s'));
                    $deal_end_time = strtotime($product->end_time);
                    $dealTimeInSeconds = $deal_end_time - $deal_start_time;
                    $product->deal_time_left = $dealTimeInSeconds;
                }else{
                    $product->deal_time_left = null;
                }
                $product->discount = number_format(($product->fabpik_seller_discount * 100) / $product->pv_mrp, 1) + 0;
                // if($product->deal_price != null){
                //     $product->deal_discount = number_format(($product->pv_mrp - $product->deal_price ) / $product->pv_mrp * 100, 1) + 0;
                // }
                $images = json_decode($product->images);
                $product->sizechart_image_url = url(UPLOAD_PATH.'/'.$product->sizechart_image);
                if($images == null){
                    $image_path = url(UPLOAD_PATH.'/'.'no-image.jpg');
                    $imagePath = $image_path;
                    array_push($imagesWithPath,$imagePath);
                    $product->images = $imagesWithPath;
                }else{
                    foreach ($images as $image) {
                        if($image != null){
                            $image_path = url(UPLOAD_PATH.'/'.$image);
                            $imagePath= $image_path;
                            array_push($imagesWithPath,$imagePath);
                        }
                    }
                    $product->images = $imagesWithPath;
                }

        }
        $returns = ['product'=>$product,'variantOptions'=>__getVariantOptions($product->product_variant_id),'coupons'=>$couponsTemp,'url'=>url(UPLOAD_PATH."/sizechart/"),'user'=>auth('api')->user()];
        return $returns;
    }

    public static function getAjaxSpecialPriceListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    )
    {
        $products = Product::with(['category', 'seller', 'productvariant', 'productcategory' => function($q){
            $q->where('is_active','=', 1);
        }] )->orderBy($orderColumn, $orderDir);
        if (isset($criteria->seller)) { $products = $products->where('seller_id', $criteria->seller); }
        if($criteria->fname) $products = $products->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $products = $products->where('status', $criteria->fstatus);
        if(!is_null($criteria->fseller)) $products = $products->where('seller_id', $criteria->fseller);
        if(!is_null($criteria->fpricefrom)) $products = $products->whereHas('productvariant', function($q) use ($criteria){
                    $q->where('sell_price','>=', $criteria->fpricefrom);
                });
        if(!is_null($criteria->fpriceto)) $products = $products->whereHas('productvariant', function($q) use ($criteria){
                    $q->where('sell_price','<=', $criteria->fpriceto);
                });
        if(!is_null($criteria->ffromdate)) $products = $products->whereHas('productvariant', function($q) use ($criteria){
                    $q->where('created_at','>=', $criteria->ffromdate);
                });
        if(!is_null($criteria->ftodate)) $products = $products->whereHas('productvariant', function($q) use ($criteria){
                    $q->where('created_at','<=', $criteria->ftodate);
                });
        if(!is_null($criteria->fsku)) $products = $products->whereHas('productvariant', function($q) use ($criteria){
                $q->where('sku', 'LIKE', "%{$criteria->fsku}%");
                });
        if(!is_null($criteria->funiqueid)) $products = $products->whereHas('productvariant', function($q) use ($criteria){
                    $q->where('unique_id', 'LIKE', "%{$criteria->funiqueid}%");
                });
        if(!is_null($criteria->fcategory)) {
            @list($categoryId,$subCategoryId,$childCategoryId) = explode('-', $criteria->fcategory);
            $products =  $products->whereHas('productcategory', function ($q) use ($childCategoryId){
                $q->where('childcategory_id','=', $childCategoryId);
            });
        }

        if(isset($criteria->fcommissiontype) && !is_null($criteria->fcommissiontype)) {
            $commissionType = $criteria->fcommissiontype;
            $products =  $products->whereHas('seller', function ($q) use ($commissionType){
                $q->where('commission_type','=', $commissionType);
            });
        }

        return $products->paginate(intval($criteria->length), ['id','name','thumbnail','sku','seller_id','mrp','sell_price','unique_id','primary_category','fabpik_seller_price','fabpik_seller_discount','created_at','updated_at', 'status'], 'page', $page);
    }

    /**
     * Export Full Products Data
    */
    public static function getProductExportData(
        $criteria=[],
        $orderColumn='id',
        $orderDir='desc')
    {
        $products = Product::with(['category', 'seller', 'productvariant', 'productcategory' => function($q){
            $q->where('is_active','=', 1);
        }])->orderBy($orderColumn, $orderDir);
        if (isset($criteria->fseller))  { $products = $products->where('seller_id', $criteria->fseller); }
        if(!is_null($criteria->fpcategory)) $products = $products->whereHas('productcategory', function($q) use ($criteria){
            $q->where('category_id', $criteria->fpcategory);
        });

        if($criteria->fids) $products = $products->whereRaw("id in (".$criteria->fids.")");
        if($criteria->fname) $products = $products->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $products = $products->where('status', $criteria->fstatus);
        if(!is_null($criteria->fseller)) $products = $products->where('seller_id', $criteria->fseller);
        if(!is_null($criteria->fpricefrom)) $products = $products->where('sell_price','>=', $criteria->fpricefrom);
        if(!is_null($criteria->fpriceto)) $products = $products->where('sell_price','<=', $criteria->fpriceto);
        if(!is_null($criteria->ffromdate)) $products = $products->where('created_at','>=', $criteria->ffromdate);
        if(!is_null($criteria->ftodate)) $products = $products->where('created_at','<=', $criteria->ftodate);
        if(!is_null($criteria->fsku)) $products = $products->where('sku', 'LIKE', "%{$criteria->fsku}%");
        if(!is_null($criteria->funiqueid)) $products = $products->where('unique_id', 'LIKE', "%{$criteria->funiqueid}%");
        if(!is_null($criteria->fcategory)) {
            @list($categoryId,$subCategoryId,$childCategoryId) = explode('-', $criteria->fcategory);
            $products->whereHas('productcategory', function ($q) use ($childCategoryId){
                $q->where('childcategory_id','=', $childCategoryId);
            });
        }

        if(isset($criteria->fcommissiontype) && !is_null($criteria->fcommissiontype)) {
            $commissionType = $criteria->fcommissiontype;
            $products =  $products->whereHas('seller', function ($q) use ($commissionType){
                $q->where('commission_type','=', $commissionType);
            });
        }

        return $products->get();
    }

    public static function getMarketPlaceProductCount($seller_id)
    {
        $countQuery = "SELECT
                            pvo.product_variant_id
                        FROM
                            `product_variants`
                        INNER JOIN `products` AS `p`
                        ON
                            `p`.`id` = `product_variants`.`product_id`
                        INNER JOIN `categories` AS `c`
                        ON
                            `c`.`id` = `p`.`primary_category`
                        INNER JOIN `product_variant_options` AS `pvo`
                        ON
                            `pvo`.`product_variant_id` = `product_variants`.`id`
                        INNER JOIN `product_categories` AS `pc`
                        ON
                            `pc`.`product_id` = `product_variants`.`product_id`
                        INNER JOIN `brands` AS `b`
                        ON
                            `b`.`id` = `p`.`brand_id`
                        INNER JOIN `view_categories` AS `vc`
                        ON
                            `vc`.`child_id` = `pc`.`childcategory_id`
                        LEFT JOIN(
                            SELECT
                                a.product_variant_id,
                                b.thumbnail,
                                b.images
                            FROM
                                (
                                SELECT
                                    t1.product_id,
                                    t4.product_variant_id,
                                    t4.attribute_id,
                                    t4.attribute_option_id
                                FROM
                                    `product_variants` AS t1
                                JOIN products AS t2
                                ON
                                    t2.id = t1.product_id
                                JOIN categories AS t3
                                ON
                                    t3.id = t2.primary_category
                                JOIN product_variant_options AS t4
                                ON
                                    t4.product_variant_id = t1.id
                                WHERE
                                    t4.attribute_id = t3.primary_attribute AND t4.deleted_at IS NULL
                            ) AS a
                        LEFT JOIN variation_images AS b
                        ON
                            b.product_id = a.product_id AND b.attribute_option_id = a.attribute_option_id
                        ) AS img
                        ON
                            `img`.`product_variant_id` = `product_variants`.`id`
                        LEFT JOIN `sellers` AS `s`
                        ON
                            `s`.`id` = `p`.`seller_id`
                        WHERE
                            s.id=".$seller_id." AND product_variants.deleted_at IS NULL AND p.status = 1 AND p.deleted_at IS NULL AND s.status = 1 AND s.approval_status = 1 AND s.deleted_at IS NULL AND b.deleted_at IS NULL AND b.status = 1  AND `product_variants`.`deleted_at` IS NULL";

        $countQuery .= "
                        GROUP BY
                            `product_variants`.`product_id`,
                            `c`.`primary_attribute`";

        $returns = ['totalResults'=>null];

        $returns['totalResults'] = DB::select("SELECT COUNT(t1.product_variant_id) AS totalCount FROM (".$countQuery.") AS t1")[0]->totalCount;

        return $returns;
    }

    public static function getMarketPlaceProducts($filters=[],$seller_id)
    {
        $offset = ($filters['pageNumber'] - 1) * $filters['pageSize'];

        $skus = [];
        if(!empty($filters['skus'])) {
            $skus = explode(',', $filters['skus']);
        }

        $products = Product::join('product_variants AS pv','pv.product_id','products.id')
                        ->join('categories AS c','c.id','products.primary_category')
                        ->join('brands AS b','b.id','products.brand_id')
                        ->whereRaw('products.status = 1 AND c.deleted_at IS NULL AND c.status = 1 AND b.deleted_at IS NULL AND b.status = 1')
                        ->where('products.seller_id', $seller_id);

        if(count($skus) > 0) $products = $products->whereIn('pv.sku',$skus);

        $products = $products->selectRaw("products.id, products.unique_id, products.name as parentTitle, products.description as additionalInfo, products.created_at, b.name as brand, c.primary_attribute, c.secondary_attribute")
                        ->groupBy('pv.product_id')
                        ->offset($offset)
                        ->limit($filters['pageSize'])
                        ->get();

        $response = [];
        $parent = 0;
        if($products) {
            foreach($products as $product) {
                $response[$parent] = [
                    'id' => $product->unique_id,
                    'parentTitle' => $product->parentTitle,
                    'brand' => $product->brand,
                    'commissionPercentage' => 0,
                    'paymentGatewayCharge' => 0,
                    'logisticsCost' => 0,
                    'additionalInfo' => $product->additionalInfo,
                    'created' => date('Y-m-d\TH:i:s', strtotime($product->created_at)),
                ];

                $primaryAttr = Attribute::select('id','name')->where('id',$product->primary_attribute)->first();
                if($product->secondary_attribute)
                    $secondaryAttr = Attribute::select('id','name')->where('id',$product->secondary_attribute)->first();

                $prdVariants = [];
                $child = 0;

                // get all variants of this parent product
                $variants = ProductVariant::selectRaw("
                                            product_variants.id,
                                            product_variants.product_id,
                                            product_variants.unique_id,
                                            product_variants.name,
                                            product_variants.slug,
                                            product_variants.sku,
                                            product_variants.description,
                                            product_variants.stock,
                                            product_variants.mrp,
                                            product_variants.discount,
                                            product_variants.price")
                                        ->where('product_variants.product_id', $product->id);

                if(count($skus) > 0) $variants = $variants->whereIn('sku',$skus);
                $variants = $variants->get();
                if($variants) {
                    foreach ($variants as $variant) {
                        // SELECT vi.thumbnail_img_name FROM `product_variant_options` as pvo JOIN variation_images as vi on vi.attribute_option_id = pvo.attribute_option_id WHERE pvo.product_variant_id = 1 AND pvo.attribute_id = 1 AND vi.product_id = 1

                        // get product thumbnail image
                        $image = DB::table('product_variant_options as pvo')
                                    ->join('variation_images as vi', 'vi.attribute_option_id', '=', 'pvo.attribute_option_id')
                                    ->where('pvo.product_variant_id', $variant->id)
                                    ->where('vi.product_id', $variant->product_id)
                                    ->where('pvo.attribute_id', $product->primary_attribute)
                                    ->select('vi.thumbnail_img_name')
                                    ->first();

                        $prdVariants[$child] = [
                            'imageUrl' => ($image)?env('APP_URL').UPLOAD_PATH.DIRECTORY_SEPARATOR.$image->thumbnail_img_name:'',
                            'productUrl' => 'https://fabpik.in/products/'.$variant->slug.'/'.$variant->unique_id,
                            'variantId' => $variant->unique_id,
                            'title' => $variant->name,
                            'sku' => $variant->sku,
                            'live' => true,
                            'productDescription' => $variant->description,
                            'itemPrice' => [
                                'currency' => 'INR',
                                'listingPrice' => $variant->price,
                                'mrp' => $variant->mrp,
                                'msp' => $variant->mrp,
                                'netSellerPayable' => $variant->price,
                            ],
                            'inventory' => $variant->stock,
                            'blockedInventory' => 0,
                            'pendency' => 0
                        ];

                        // get primary and secondary attribute option values
                        // SELECT ao.option_name FROM `product_variant_options` as pvo JOIN attribute_options as ao on ao.id = pvo.attribute_option_id WHERE pvo.product_variant_id = 1 AND pvo.attribute_id = 1
                        $primaryAttrValue = DB::table('product_variant_options as pvo')
                                                ->join('attribute_options as ao', 'ao.id', '=', 'pvo.attribute_option_id')
                                                ->where('pvo.product_variant_id', $variant->id)
                                                ->where('pvo.attribute_id', $product->primary_attribute)
                                                ->select('ao.option_name')
                                                ->first();

                        $prdVariants[$child][$primaryAttr->name] = ($primaryAttrValue)?$primaryAttrValue->option_name:'';

                        if($product->secondary_attribute) {
                            $secondaryAttrValue = DB::table('product_variant_options as pvo')
                                                ->join('attribute_options as ao', 'ao.id', '=', 'pvo.attribute_option_id')
                                                ->where('pvo.product_variant_id', $variant->id)
                                                ->where('pvo.attribute_id', $product->secondary_attribute)
                                                ->select('ao.option_name')
                                                ->first();

                            $prdVariants[$child][$secondaryAttr->name] = ($secondaryAttrValue)?$secondaryAttrValue->option_name:'';
                        }

                        ++$child;
                    }
                }

                $response[$parent]['variants'] = $prdVariants;

                ++$parent;
            }
        }

        return $response;
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class CommissionModel extends Model
{
    use SoftDeletes;

    protected $table        = 'commission_models';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function createdUser()
    {
        return $this->belongsTo('App\Models\User', 'created_by','id');
    }


    public static function getAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    ) {
        
        $commissionmodels = CommissionModel::with('createdUser')->orderBy($orderColumn, $orderDir);

        if(!is_null($criteria->fname)) $commissionmodels = $commissionmodels->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->fstatus)) $commissionmodels = $commissionmodels->where('status', $criteria->fstatus);
        if(!is_null($criteria->fcommissiontype)) $commissionmodels = $commissionmodels->where('commission_type', $criteria->fcommissiontype);
        if(!is_null($criteria->foncommission)) $commissionmodels = $commissionmodels->where('commission_on', $criteria->foncommission);
        if(!is_null($criteria->fshippingcharges)) $commissionmodels = $commissionmodels->where('shipping_charges', $criteria->fshippingcharges);
        if(!is_null($criteria->fhandlingcharges)) $commissionmodels = $commissionmodels->where('handling_charges', $criteria->fhandlingcharges);
       
        return $commissionmodels->paginate(intval($criteria->length), ['id', 'name', 'commission_type', 'commission_on', 'vary_with_mrp_discount', 'shipping_charges', 'handling_charges', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at', 'deleted_by', 'deleted_at'], 'page', $page);
    }

    //get the commission models based on ID
    public static function getCommission($id = null)
    {
        return CommissionModel::where('id', $id)->first();
    }
}

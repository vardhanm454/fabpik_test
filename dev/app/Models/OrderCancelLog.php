<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class OrderCancelLog extends Model
{
    // use SoftDeletes;

	protected $table        = 'order_cancel_log';
    protected $primaryKey   = 'id';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SellerWarehouses extends Model
{
    use SoftDeletes;

    protected $table        = 'seller_warehouses';
    protected $primaryKey   = 'id';
    protected $fillable = ['seller_id','name','is_default','phone', 'address','address_two', 'landmark','city','state_id','pincode','shiprocket_pickup_nickname'];

    public function scopeActive($query)
    {
        return $query->where('status', 'y');
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }
}

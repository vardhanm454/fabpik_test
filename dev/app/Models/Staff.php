<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model
{
    use SoftDeletes;

    protected $table = 'staffs';
    protected $primaryKey   = 'id';
    protected $fillable = [
        'name',
        'email',
        'designation',
        'mobile_no'
    ];

    public static function getAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc')
    {
        $staffs = Staff::orderBy($orderColumn, $orderDir);

        if(!is_null($criteria->name)) $staffs = $staffs->where('name', 'LIKE', "%{$criteria->name}%");
        if(!is_null($criteria->email)) $staffs = $staffs->where('email', 'LIKE',"%{$criteria->email}%");
        if(!is_null($criteria->mobile)) $staffs = $staffs->where('mobile_no', 'LIKE',"%{$criteria->mobile}%");
        if(!is_null($criteria->status)) $staffs = $staffs->where('status', $criteria->status);
        if(!is_null($criteria->seller)) $staffs = $staffs->where('seller_id', $criteria->seller);

        return $staffs->paginate(intval($criteria->length), ['id','name','email','status', 'mobile_no'], 'page', $page);
    }


    public static function getExportData(
        $criteria=[],
        $orderColumn='id',
        $orderDir='desc')
    {

        $staffs = Staff::orderBy($orderColumn, $orderDir);

        if(!is_null($criteria->name)) $staffs = $staffs->where('name', 'LIKE', "%{$criteria->name}%");
        if(!is_null($criteria->email)) $staffs = $staffs->where('email', 'LIKE',"%{$criteria->email}%");
        if(!is_null($criteria->mobile)) $staffs = $staffs->where('mobile_no', 'LIKE',"%{$criteria->mobile}%");
        if(!is_null($criteria->seller)) $staffs = $staffs->where('seller_id', $criteria->seller);

        return $staffs->get();
    }


}

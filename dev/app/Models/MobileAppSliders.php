<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MobileAppSliders extends Model
{
    use SoftDeletes;

    protected $table        = 'mobile_app_sliders';
    protected $primaryKey   = 'id';
    protected $fillable = [ 
        'page',
        'panel',
        'title',
        'image',
        'filter_params',
    ];
}

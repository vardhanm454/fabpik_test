<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    
    protected $table        = 'customers';
    protected $primaryKey   = 'id';
    
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function user()
    {
        // return $this->hasOne(User::class, 'customer_id' ,'id');
        return $this->hasOne(User::class, 'ref_id' ,'id');
    }

    public static function getAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='asc'
    )
    {
        $customer = Customer::orderBy($orderColumn, $orderDir);
        // if ($criteria->fname) { $customer = $customer->where('name', 'LIKE', "%{$criteria->fname}%"); }
        // if (!is_null($criteria->fstatus)) { $customer = $customer->where('status', $criteria->fstatus); }
        if(!is_null($criteria->fstatus)) $customer = $customer->where('status', $criteria->fstatus);
        if(!is_null($criteria->ffromdate) && !is_null($criteria->ftodate)){
            if($criteria->ffromdate == $criteria->ftodate){
                $customer = $customer->where('created_at','>=', $criteria->ffromdate);
            }else{
                $customer = $customer->whereBetween('created_at',[ $criteria->ffromdate, $criteria->ftodate ]);
            }
        } else if(!is_null($criteria->ffromdate)){
            $customer = $customer->where('created_at','>=', $criteria->ffromdate);
        } else if(!is_null($criteria->ftodate)){
            $customer = $customer->where('created_at','<=', $criteria->ftodate);
        }
        if(!is_null($criteria->fname)) $customer = $customer->where('name', 'LIKE', "%{$criteria->fname}%");
        if(!is_null($criteria->femail)) $customer = $customer->where('email', 'LIKE', "%{$criteria->femail}%");
        if(!is_null($criteria->fmobile)) $customer = $customer->where('mobile', 'LIKE', "%{$criteria->fmobile}%");
        return $customer->paginate(intval($criteria->length), ['id','name','email','mobile', 'created_at', 'updated_at', 'status'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[], 
        $orderColumn='id', 
        $orderDir='desc')
    {
        $customer = Customer::orderBy($orderColumn, $orderDir);
        if ($criteria->fname) { $customer = $customer->where('name', 'LIKE', "%{$criteria->fname}%"); }
        if (!is_null($criteria->fstatus)) { $customer = $customer->where('status', $criteria->fstatus); }
        return $customer->get();
    }

    public static function getCustomerAvatar($customer) { 
        
        $i=0;
        foreach($customer as $s){
            $customer[$i]->avatar =  url(UPLOAD_PATH.'/avatar',$s->image); 
            $i++;
        }
       return $customer;
    }
}

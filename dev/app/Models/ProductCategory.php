<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductCategory extends Model
{
    // use SoftDeletes, HasFactory;
    public $timestamps = false;

    protected $table        = 'product_categories';
    protected $primaryKey   = 'id';

    // public function product()
    // {
    //     return $this->belongsTo('App\Models\Product' ,'id','product_id');
    // }
}
<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FCMToken extends Model
{
    
    protected $table = 'fcm_tokens';
    protected $primaryKey   = 'id';
    protected $fillable = [ 
        'fcm_token'
    ];
}

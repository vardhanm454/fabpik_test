<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderHandlingCharge extends Model
{
    use SoftDeletes;

	protected $table        = 'order_handling_charge';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }
    
}

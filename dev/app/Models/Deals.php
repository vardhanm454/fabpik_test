<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deals extends Model
{
    use SoftDeletes;

	protected $table        = 'deals';
    protected $primaryKey   = 'id';

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
    	// $deals = Deals::orderBy($orderColumn, $orderDir);
    	$deals = Deals::selectRaw("deals.*,pv.name,users.name as created_by_user,pv.fabpik_seller_price as price,pv.mrp")
                        ->join('product_variants as pv','pv.id','=','deals.product_variant_id')
                        ->join('users','users.id','=','deals.created_by')
                        ->orderBy("deals.created_at","DESC");
        
        if(!is_null($criteria->fprovariants)) $deals = $deals->whereIn('deals.product_variant_id',json_decode($criteria->fprovariants));
        // if(!is_null($criteria->fstatus)) $deals = $deals->where('status', $criteria->fstatus);
        return $deals->paginate(intval($criteria->length), ['id'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[], 
        $orderColumn='id', 
        $orderDir='desc')
    {
        $deals = Deals::selectRaw("deals.start_time,deals.end_time,pv.name")
                            ->join('product_variants as pv','pv.id','=','deals.product_variant_id');
        // if(!is_null($criteria->fname)) $coupons = $coupons->where('code', 'LIKE', "%{$criteria->fname}%");
        // if(!is_null($criteria->fstatus)) $coupons = $coupons->where('status', $criteria->fstatus);
        return $deals->get();
    }
}

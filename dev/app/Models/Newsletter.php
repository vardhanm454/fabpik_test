<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Newsletter extends Model
{
    use SoftDeletes;

	protected $table        = 'newsletters';
    protected $primaryKey   = 'id';

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
    	$newsletters = Newsletter::orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->ffromdate) && !is_null($criteria->ftodate)){
            if($criteria->ffromdate == $criteria->ftodate){
                $newsletters = $newsletters->where('notify_on','>=', $criteria->ffromdate);
            }else{
                $newsletters = $newsletters->whereBetween('notify_on',[ $criteria->ffromdate, $criteria->ftodate ]);
            }
        } else if(!is_null($criteria->ffromdate)){
            $newsletters = $newsletters->where('notify_on','>=', $criteria->ffromdate);
        } else if(!is_null($criteria->ftodate)){
            $newsletters = $newsletters->where('notify_on','<=', $criteria->ftodate);
        }

        return $newsletters->paginate(intval($criteria->length), ['id','subject','content','notify_on','created_by','created_at','updated_by','updated_at'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[], 
        $orderColumn='id', 
        $orderDir='desc')
    {
        $newsletters = Newsletter::orderBy($orderColumn, $orderDir);
        if(!is_null($criteria->ffromdate) && !is_null($criteria->ftodate)){
            if($criteria->ffromdate == $criteria->ftodate){
                $newsletters = $newsletters->where('notify_on','>=', $criteria->ffromdate);
            }else{
                $newsletters = $newsletters->whereBetween('notify_on',[ $criteria->ffromdate, $criteria->ftodate ]);
            }
        } else if(!is_null($criteria->ffromdate)){
            $newsletters = $newsletters->where('notify_on','>=', $criteria->ffromdate);
        } else if(!is_null($criteria->ftodate)){
            $newsletters = $newsletters->where('notify_on','<=', $criteria->ftodate);
        }
        
        return $newsletters->get();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class OrderAddressModifyLog extends Model
{
    // use SoftDeletes;

	protected $table        = 'order_address_modify_logs';
    protected $primaryKey   = 'id';
}

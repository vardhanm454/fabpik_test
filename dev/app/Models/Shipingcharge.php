<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shipingcharge extends Model
{
    use SoftDeletes;

	protected $table        = 'shiping_charges';
    protected $primaryKey   = 'id';

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='asc')
    {
    	$shippingchargs = Shipingcharge::orderBy($orderColumn, $orderDir);
        if($criteria->fstatus) $shippingchargs = $shippingchargs->where('status', $criteria->fstatus);
        return $shippingchargs->paginate(intval($criteria->length), ['id','min_amount','max_amount','charge_amount','status'], 'page', $page);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Subcategory extends Model
{
    use SoftDeletes;

    protected $table        = 'subcategories';
    protected $primaryKey   = 'id';

    protected $appends = ['icon_path','image_path'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id','id');
    }
    
    public function childcategories()
    {
      return $this->hasMany('App\Models\ChildCategory', 'subcategory_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }
    
    /*
    ** delete Child categories
    **
    */
    public static function deleteChildCategory($ids=[])
    {
        ChildCategory::whereIn('subcategory_id', $ids)->delete();
    }


    public function getIconPathAttribute() { 
        return url(UPLOAD_PATH.'/'.$this->icon); 
    }

    public function getImagePathAttribute() { 
        return url(UPLOAD_PATH.'/'.$this->image); 
    }

    public static function getAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='desc'
    )
    {
        $subcategories = Subcategory::with('category')->orderBy('subcategories.'.$orderColumn, $orderDir);
        if (!is_null($criteria->fname)) { $subcategories = $subcategories->where('subcategories.title', 'LIKE', "%{$criteria->fname}%"); }
        if (!is_null($criteria->fstatus)) { $subcategories = $subcategories->where('subcategories.status', $criteria->fstatus); }
        
        return $subcategories->paginate(intval($criteria->length), ['subcategories.id','subcategories.image','subcategories.icon','subcategories.category_id','subcategories.title','subcategories.status'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[], 
        $orderColumn='id', 
        $orderDir='desc')
    {
        $subcategories = Subcategory::with('category')->orderBy('subcategories.'.$orderColumn, $orderDir);
        if (!is_null($criteria->fname)) { $subcategories = $subcategories->where('subcategories.title', 'LIKE', "%{$criteria->fname}%"); }
        if (!is_null($criteria->fstatus)) { $subcategories = $subcategories->where('subcategories.status', $criteria->fstatus); }
         
        return $subcategories->get();
    }
}

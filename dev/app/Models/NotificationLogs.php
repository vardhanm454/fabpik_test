<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationLogs extends Model
{

	protected $table        = 'notification_logs';
    protected $primaryKey   = 'id';
    protected $fillable = ['user_id','visited_on'];


}

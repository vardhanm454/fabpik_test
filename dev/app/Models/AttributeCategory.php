<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class AttributeCategory extends Model
{
    // use SoftDeletes;
    
    protected $table        = 'attribute_category';
    // protected $primaryKey   = 'id';
    public $timestamps = false;

    public function category()
    {
      return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }
}

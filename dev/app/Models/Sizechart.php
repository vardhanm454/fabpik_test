<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sizechart extends Model
{
    use SoftDeletes;

    protected $table        = 'size_charts';
    protected $primaryKey   = 'id';

    public function seller()
    {
        return $this->belongsTo('App\Models\Seller', 'seller_id', 'id');
    }

    public static function getAjaxListData(
        $criteria=[],
        $page=1,
        $orderColumn='id',
        $orderDir='asc'
    ) {
        $sizechart = Sizechart::with('seller')->orderBy($orderColumn, $orderDir);
        if ($criteria->fname) { $sizechart = $sizechart->where('name', 'LIKE', "%{$criteria->fname}%"); }
        // if ($criteria->fstatus) { $sizechart = $sizechart->where('status', $criteria->fstatus); }
        if(isset($criteria->fseller) && !is_null($criteria->fseller)) { $sizechart = $sizechart->where('seller_id', $criteria->fseller); }
        if (isset($criteria->seller))  { $sizechart = $sizechart->where('seller_id', $criteria->seller); }
        return $sizechart->paginate(intval($criteria->length), ['id','name','image', 'created_at','updated_at', 'seller_id', 'description'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[],
        $orderColumn='id',
        $orderDir='desc'
    ) {
        $sizechart = Sizechart::with('seller')->orderBy($orderColumn, $orderDir);
        if ($criteria->fname) { $sizechart = $sizechart->where('name', 'LIKE', "%{$criteria->fname}%"); }
        if (isset($criteria->seller))  { $sizechart = $sizechart->where('seller_id', $criteria->seller); }
        // if ($criteria->fstatus) { $sizechart = $sizechart->where('status', $criteria->fstatus); }
        return $sizechart->get();
    }
}

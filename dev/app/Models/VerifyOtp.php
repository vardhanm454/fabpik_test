<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VerifyOtp extends Model
{
	protected $table        = 'verify_otp';
    protected $primaryKey   = 'id';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function productvariant()
    {
        return $this->belongsTo(ProductVariant::class);
    }
    
    /**
     * Return a key value array,get product_id.
     *
     * 
     */
    public static function getProductVariantDetails($productVariantid)
    {
       return ProductVariant::selectRaw("product_variants.*,deals.deal_price,deals.start_time,deals.end_time,deals.max_orders,CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN product_variants.fabpik_seller_price ELSE product_variants.price END AS final_price,CASE WHEN NOW( ) BETWEEN product_variants.special_price_start_date AND product_variants.special_price_end_date THEN TRUNCATE(product_variants.fabpik_seller_discount,1)  ELSE TRUNCATE(product_variants.discount,1) END AS final_discount")
                            ->leftjoin('deals', function($join){
                                $join->on('deals.product_variant_id', '=', 'product_variants.id')
                                    ->whereRaw('NOW( ) BETWEEN deals.start_time AND deals.end_time');
                              })
                              ->whereRaw("product_variants.id = $productVariantid AND deals.deleted_at IS NULL")
                              ->first();
    }

     /**
     * Return a key value array,get seller_id.
     *
     * 
     */
    public static function getProductDetails($product)
    {
       return Product::where('id', $product)->first();
    }
 
  
}

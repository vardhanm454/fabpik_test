<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductVariantOption extends Model
{
    use SoftDeletes, HasFactory;

    protected $table        = 'product_variant_options';
    protected $primaryKey   = 'id';

    public function productvariant()
    {
        return $this->belongsTo('App\Models\ProductVariant','product_variant_id' ,'id');
    }
    
    public function attributes()
    {
      return $this->belongsTo('App\Models\Attribute', 'attribute_id', 'id');
    }
}

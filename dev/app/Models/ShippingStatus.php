<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingStatus extends Model
{
    // use SoftDeletes;

	protected $table        = 'shipping_statuses';
    protected $primaryKey   = 'id';

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tickets extends Model
{
    //use SoftDeletes;

    protected $table        = 'help_desks';
    protected $primaryKey   = 'id';

     public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function statusLabel()
    {
        return ($this->status=='1')?'Active':'Inactive';
    }

    public function comments()
    {
        return $this->hasMany('App\Models\TicketComments', 'help_desk_id', 'id');
    }

    public static function getAjaxListData(
        $criteria=[], 
        $page=1, 
        $orderColumn='id', 
        $orderDir='asc')
    {
        $tickets = Tickets::orderBy($orderColumn, $orderDir);
        if($criteria->fname) $tickets = $tickets->where('issue_title', 'LIKE', "%{$criteria->fname}%");
        if($criteria->fstatus) $tickets = $tickets->where('status', $criteria->fstatus);
        // $tickets->where('seller_id',auth()->user()->ref_id);
        return $tickets->paginate(intval($criteria->length), ['id','ticket_no','created_at','updated_at','issue_title','seller_id','category','issue_status'], 'page', $page);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderRefundLogs extends Model
{

	protected $table        = 'order_refund_log';
    protected $primaryKey   = 'id';
    protected $fillable = ['child_order_id','amount','reason','created_by'];


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    use SoftDeletes;

	protected $table        = 'sliders';
    protected $primaryKey   = 'id';
    protected $fillable = [ 
        'title',
        'image_path',
        'slider_url',
        'status',
        'view',
        'display_order',
        'start_date',
        'end_date'
    ];

    public static function getAjaxListData(
    	$criteria=[], 
    	$page=1, 
    	$orderColumn='id', 
    	$orderDir='desc')
    {
    	$sliders = Slider::orderBy($orderColumn, $orderDir);
        
        if(!is_null($criteria->title)) $sliders = $sliders->where('title', 'LIKE', "%{$criteria->title}%");
        if(!is_null($criteria->status)) $sliders = $sliders->where('status', $criteria->status);

        return $sliders->paginate(intval($criteria->length), ['id','title','image_path','slider_url','display_order','status','view','start_date','end_date'], 'page', $page);
    }

    public static function getExportData(
        $criteria=[], 
        $orderColumn='id', 
        $orderDir='desc')
    {        
        $sliders = Slider::orderBy($orderColumn, $orderDir);

        if(!is_null($criteria->title)) $sliders = $sliders->where('title', 'LIKE', "%{$criteria->title}%");
        if(!is_null($criteria->status)) $sliders = $sliders->where('status', $criteria->status);

        return $sliders->get();
    }  
}

<?php 

namespace App\Repository;

use Illuminate\Support\Facades\Cache;
use App\Repository\BaseRepo;

// Models
use App\Models\Slider;

class SuggestionRepo extends BaseRepo
{
	const CACHE_KEY = 'SUGGESTIONS';
	const CACHE_TAG = 'SUGGESTIONS';

    function __construct()
    {
        parent::__construct(self::CACHE_TAG, self::CACHE_KEY);
    }
	
	public function results($orderColumn='id', $orderDir='asc', $criterias=[])
	{
		$key = "allPaginated.{$orderColumn}.{$orderDir}";
		foreach ($criterias as $index => $value) {
			$key .= (!is_null($value))?".{$index}.{$value}":"";
		}
		$key = parent::getCacheKey($key);

        // if (Cache::tags(self::CACHE_TAG)->has($key)) {
		//     echo "Cache Hit"; die;
		// }

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function () use($orderColumn, $orderDir, $criterias, $page) {
			$categories = DB::table('view_categories')
                           ->selectRaw("view_categories.*, 'category' AS 'type'")
                           ->where('child_title','LIKE','%'.$search_param.'%')
                           ->orWhere('subcategory_title','LIKE','%'.$search_param.'%')
                           ->orWhere('category_title','LIKE','%'.$search_param.'%')
                           ->limit(10)
                           ->get();
            $brands = Brand::selectRaw("name,id")
                            ->where('name','LIKE','%'.$search_param.'%')
                            ->where('status','=',1)
                            ->limit(5)
                            ->get();
			$products = DB::table("view_product_variants")
                            ->selectRaw("view_product_variants.*")
                            ->whereRaw("(view_product_variants.name LIKE '%".$search_param."%' OR view_product_variants.sku LIKE '%".$search_param."%' OR view_product_variants.brand_name LIKE '%".$search_param."%'  OR view_product_variants.product_tags LIKE '%".$search_param."%')")
                            ->limit((20 - count($categories)))
                            ->get();
		    return $this->respond(['categories'=>$categories,'products'=>$products,'brands'=>$brands,'imgUrl'=>url(UPLOAD_PATH)]);

		});	
	}

	public function allExport($criterias=[])
	{
		$key = "export";
		foreach ($criterias as $index => $value) {
			$key .= (!is_null($value))?".{$index}.{$value}":"";
		}
		$key = parent::getCacheKey($key);

		$orderColumn = 'id';
		$orderDir = 'desc';

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function () use($orderColumn, $orderDir, $criterias) {
            return Slider::getExportData($criterias, $orderColumn, $orderDir);
		});
	}

	public function get($id)
	{
		$key = parent::getCacheKey("GET.{$id}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($id) {
			return Slider::find($id);
		});
	}
}
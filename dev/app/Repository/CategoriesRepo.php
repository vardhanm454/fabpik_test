<?php 

namespace App\Repository;
use Illuminate\Support\Facades\Cache;

// Models
use App\Models\Category;

class CategoriesRepo
{
	const CACHE_KEY = 'CATEGORIES';
	const CACHE_TAG = 'CATEGORIES';
	
	public function all($orderColumn, $orderDir)
	{
		$key = $this->getCacheKey("all.{$orderColumn}.{$orderDir}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($orderColumn, $orderDir) {
			return Category::with('subcategories.childcategories')
							->orderBy('categories.priority','ASC')
							->get();
			// return DB::table('')orderBy($orderColumn, $orderDir)->get();
		});
	}

	public function onlyCategories($orderColumn, $orderDir)
	{
		$key = $this->getCacheKey("onlyCategories.{$orderColumn}.{$orderDir}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($orderColumn, $orderDir) {
			return Category::selectRaw("id,title,slug,icon")
							->orderBy('priority','ASC')
							->get();
			// return DB::table('')orderBy($orderColumn, $orderDir)->get();
		});
	}

	public function allActive($orderColumn, $orderDir)	
	{
		$key = $this->getCacheKey("allActive.{$orderColumn}.{$orderDir}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($orderColumn, $orderDir) {
			$categories = Category::with('subcategories.childcategories')
									->orderBy('categories.priority','ASC')
									->active()
									->get();
			$categories = json_decode($categories);
			$allCategories = array_merge($categories,STATIC_CATEGORIES);
			return $allCategories;
			// return DB::table('')orderBy($orderColumn, $orderDir)->get();
		});
	}

	public function flush($key=null)
	{
		return ($key) ? Cache::forget($key) : Cache::tags(self::CACHE_TAG)->flush();
	}

	public function getCacheKey($key)
	{
		return strtoupper(self::CACHE_KEY . ".{$key}");
	}
}
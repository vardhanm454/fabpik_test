<?php 

namespace App\Repository;

use Illuminate\Support\Facades\Cache;

class BaseRepo
{
	private $cache_tag = '';
    private $cache_key = '';

    function __construct($tag=null, $key=null)
    {
        $this->cache_tag = $tag;
        $this->cache_key = $key;
    }

    public function keys($tag=null, $key=null)
    {
        return ['tag'=>$this->cache_tag, 'key'=>$this->cache_key];
    }

	public function flush($key=null)
	{
		return ($key) ? Cache::forget($key) : Cache::tags($this->cache_tag)->flush();
	}

	public function getCacheKey($key)
	{
		return strtoupper($this->cache_key . ".{$key}");
	}
}
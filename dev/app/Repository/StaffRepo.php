<?php 

namespace App\Repository;

use Illuminate\Support\Facades\Cache;
use App\Repository\BaseRepo;

// Models
use App\Models\Staff;

class StaffRepo extends BaseRepo
{
	const CACHE_KEY = 'STAFFS';
	const CACHE_TAG = 'STAFFS';

    function __construct()
    {
        parent::__construct(self::CACHE_TAG, self::CACHE_KEY);
    }
	
	public function all($orderColumn='id', $orderDir='asc')
	{
		$key = parent::getCacheKey("all.{$orderColumn}.{$orderDir}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($orderColumn, $orderDir) {
			return Staff::orderBy($orderColumn, $orderDir)->get();
		});
	}

	public function allActive($orderColumn='id', $orderDir='asc')
	{
		$key = parent::getCacheKey("allActive.{$orderColumn}.{$orderDir}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($orderColumn, $orderDir) {
			return Staff::active()->orderBy($orderColumn, $orderDir)->get();
		});
	}

	public function allPaginated($orderColumn='id', $orderDir='asc', $criterias=[], $page=1)
	{
		$key = "allPaginated.{$orderColumn}.{$orderDir}";
		foreach ($criterias as $index => $value) {
			$key .= (!is_null($value))?".{$index}.{$value}":"";
		}
		$key = parent::getCacheKey($key.".{$page}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function () use($orderColumn, $orderDir, $criterias, $page) {
		    return Staff::getAjaxListData($criterias, $page, $orderColumn, $orderDir);
		});	
	}

	public function allExport($criterias=[])
	{
		$key = "export";
		foreach ($criterias as $index => $value) {
			$key .= (!is_null($value))?".{$index}.{$value}":"";
		}
		$key = parent::getCacheKey($key);

		$orderColumn = 'id';
		$orderDir = 'desc';

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function () use($orderColumn, $orderDir, $criterias) {
            return Staff::getExportData($criterias, $orderColumn, $orderDir);
		});
	}

	public function get($id)
	{
		$key = parent::getCacheKey("GET.{$id}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($id) {
			return Staff::find($id);
		});
	}
}
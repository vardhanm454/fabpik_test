<?php 

namespace App\Repository;

use Illuminate\Support\Facades\Cache;
use App\Repository\BaseRepo;

// Models
use Spatie\Permission\Models\Permission;

class PermissionRepo extends BaseRepo
{
	const CACHE_KEY = 'PERMISSIONS';
	const CACHE_TAG = 'PERMISSIONS';

    function __construct()
    {
        parent::__construct(self::CACHE_TAG, self::CACHE_KEY);
    }
	
	public function all($orderColumn='id', $orderDir='asc')
	{
		$key = parent::getCacheKey("all.{$orderColumn}.{$orderDir}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($orderColumn, $orderDir) {
			return Permission::select('id','module_name','name')->orderBy($orderColumn, $orderDir)->get();
		});
	}
    
    public function modulewise($orderColumn='id', $orderDir='asc')
	{
		$key = parent::getCacheKey("modulewise.{$orderColumn}.{$orderDir}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($orderColumn, $orderDir) {
            $moduleWisePermissions = [];
            $permissions = Permission::select('id','module_name','name')->orderBy($orderColumn, $orderDir)->get();
            if($permissions) {
                foreach($permissions as $permission) {
                    $moduleWisePermissions[$permission->module_name][] = [
                        'id' => $permission->id,
                        'name' => $permission->name
                    ];
                }
            }
			return json_decode(json_encode($moduleWisePermissions), FALSE);
		});
	}

	public function get($id)
	{
		$key = parent::getCacheKey("GET.{$id}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($id) {
			return Permission::find($id);
		});
	}
}
<div id="filemanager" class="modal-dialog modal-full">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h4 class="modal-title">Image Manager</h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-sm-3">
					<div class="input-group">
						<input type="text" name="search" value="{{ $filter_name }}" placeholder="Search" class="form-control">
						<span class="input-group-btn">
							<button type="button" data-toggle="tooltip" title="Search" id="button-search" class="btn btn-default"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</div>
				<div class="col-sm-9 text-right">
					<a href="{{ $parent }}" data-toggle="tooltip" title="Parent Folder" id="button-parent" class="btn btn-default btn-sm"><span aria-hidden="true" class="icon-arrow-up font-yellow-soft"></span> Up-One level</a> 

					<a href="{{ $refresh }}" data-toggle="tooltip" title="Refresh" id="button-refresh" class="btn btn-default btn-sm"><span aria-hidden="true" class="icon-refresh font-purple-seance"></span> Refresh</a>

					<button type="button" data-toggle="tooltip" title="Upload" id="button-upload" class="btn btn-default btn-sm"><span aria-hidden="true" class="icon-cloud-upload font-green-seagreen"></span> Upload</button>

					<button type="button" data-toggle="tooltip" title="New Folder" id="button-folder" class="btn btn-default btn-sm"><span aria-hidden="true" class="icon-folder font-blue-steel"></span> New Folder</button>

					<button type="button" data-toggle="tooltip" title="Delete" id="button-delete" class="btn btn-default btn-sm"><span aria-hidden="true" class="icon-trash font-red-thunderbird"></span> Delete</button>
				</div>
			</div>
			<hr>
			@foreach($imageRows as $images)
			<div class="row">
				@foreach($images as $image)
				<div class="col-md-2">
					@if($image['type'] == 'directory')
					<div class="panel panel-default text-center pd-20 bg-grey-cararra bg-font-grey-cararra">
						<a href="{{ $image['href'] }}" class="directory" style="display:block;"><i class="fa fa-folder-open fa-3x font-blue-soft"></i></a>
						<p class="margin-top-10"><strong><input type="checkbox" name="path[]" value="{{ $image['path'] }}" style="/*display: none;*/">&nbsp;{{ $image['name'] }}</strong></p>
					</div>					
					@endif

					@if($image['type'] == 'image')
					<div class="panel panel-default text-center pd-20">
						<a href="{{ $image['href'] }}" class="thumbnail"><img src="{{ $image['thumb'] }}" alt="{{ $image['name'] }}" title="{{ $image['name'] }}" width="70" height="70" style="display:block;"></a>
						<p><input type="checkbox" name="path[]" value="{{ $image['path'] }}" style="/*display: none;*/">&nbsp;{{ $image['name'] }}</p>
					</div>
					@endif
				</div>
				@endforeach
			</div>
			<br>
			@endforeach
		</div>
		<div class="modal-footer"></div>
		{{--<div class="modal-footer">{{ $images->links() }}</div>--}}
	</div>
</div>

<script src="{{ __common_asset('global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	@if($target)
	$('a.thumbnail').click(function(e) {
		// $('body')fin.on('click', 'a.thumbnail', function(e) {
		e.preventDefault();

		@if($thumb)
		$('#{{ $thumb }}').find('img').attr('src', $(this).find('img').attr('src'));
		@endif

		$('#{{ $target }}').val($(this).parent().find('input').val());

		$('#modal-image').modal('hide');
	});
	@endif

	$('a.directory').click(function(e) {
		// $('body').on('click', 'a.directory', function(e) {
		e.preventDefault();

		$('#modal-image').load($(this).attr('href'));
	});

	$('.pagination a').click(function(e) {
		// $('body').on('click', '.pagination a', function(e) {
		e.preventDefault();

		$('#modal-image').load($(this).attr('href'));
	});

	$('#button-parent').click(function(e) {
		// $('body').on('click', '#button-parent', function(e) {
		e.preventDefault();

		$('#modal-image').load($(this).attr('href'));
	});

	$('#button-refresh').click(function(e) {
		// $('body').on('click', '#button-refresh', function(e) {
		e.preventDefault();

		$('#modal-image').load($(this).attr('href'));
	});

	$('input[name=\'search\']').on('keydown', function(e) {
		if (e.which == 13) {
			$('#button-search').trigger('click');
		}
	});

	$('#button-search').click(function(e) {
		// $('body').on('click', '#button-search', function(e) {
		var url = "{{route('filemanager', ['directory'=>$directory])}}";

		var filter_name = $('input[name=\'search\']').val();

		if (filter_name) {
			url += '&filter_name=' + encodeURIComponent(filter_name);
		}

		@if($thumb)
		url += '&thumb=' + '{{ $thumb }}';
		@endif

		@if($target)
		url += '&target=' + '{{ $target }}';
		@endif

		$('#modal-image').load(url);
	});

	$('#button-upload').on('click', function() {
		$('#form-upload').remove();

		$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file[]" value="" multiple="multiple" /></form>');

		$('#form-upload input[name=\'file[]\']').trigger('click');

		if (typeof timer != 'undefined') {
	    	clearInterval(timer);
		}

		timer = setInterval(function() {
			if ($('#form-upload input[name=\'file[]\']').val() != '') {
				// alert("{{$directory}}");
				clearInterval(timer);
				$.ajax({
					url: "{{route('filemanager.upload', ['directory'=>$directory])}}",
					type: 'post',
					dataType: 'json',
					data: new FormData($('#form-upload')[0]),
					cache: false,
					contentType: false,
					processData: false,
					beforeSend: function() {
						$('#button-upload i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
						$('#button-upload').prop('disabled', true);
					},
					complete: function() {
						$('#button-upload i').replaceWith('<i class="fa fa-upload"></i>');
						$('#button-upload').prop('disabled', false);
					},
					success: function(json) {
						if (json['error']) {
							alert(json['error']);
						}

						if (json['success']) {
							alert(json['success']);

							$('#button-refresh').trigger('click');
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			}
		}, 500);
	});

	$('#button-folder').popover({
		html: true,
		placement: 'bottom',
		trigger: 'click',
		title: 'New Folder',
		content: function() {
			html  = '<div class="input-group">';
			html += '  <input type="text" name="folder" value="" placeholder="Folder Name" class="form-control">';
			html += '  <span class="input-group-btn"><button type="button" title="New Folder" id="button-create" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></span>';
			html += '</div>';

			return html;
		}
	});

	$('#button-folder').on('shown.bs.popover', function() {
		$('#button-create').on('click', function() {
			$.ajax({
				url: "{{route('filemanager.folder', ['directory'=>$directory])}}",
				type: 'post',
				dataType: 'json',
				data: 'folder=' + encodeURIComponent($('input[name=\'folder\']').val()),
				beforeSend: function() {
					$('#button-create').prop('disabled', true);
				},
				complete: function() {
					$('#button-create').prop('disabled', false);
				},
				success: function(json) {
					if (json['error']) {
						alert(json['error']);
					}

					if (json['success']) {
						alert(json['success']);

						$('#button-refresh').trigger('click');
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});
	});

	$('#modal-image #button-delete').on('click', function(e) {
		console.log($('input[name^=\'path\']:checked'));
		if (confirm('Are you sure?')) {
			$.ajax({
				url: "{{route('filemanager.delete')}}",
				type: 'post',
				dataType: 'json',
				data: $('input[name^=\'path\']:checked'),
				beforeSend: function() {
					$('#button-delete').prop('disabled', true);
				},
				complete: function() {
					$('#button-delete').prop('disabled', false);
				},
				success: function(json) {
					if (json['error']) {
						alert(json['error']);
					}

					if (json['success']) {
						alert(json['success']);

						$('#button-refresh').trigger('click');
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	});
</script>
<!doctype html>
<html>
    <head>
        <!-- <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> -->
        <title></title>
        
        <link rel="stylesheet" href="template.css">
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
    </head>
    <body>
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" class="body">
            <tr>
                <td class="container">      
                    <div class="content">
                        <div class="header">
                            <hr class="header_hr">
                            <b class="header_title">Fabpik</b>
                        </div>   

                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="main">
                            <tr>    
                                <td>
                                    <div class="content_main">
                                        <b>Hii  Harry,</b>
                                        <p style="font-size:22px;color:#04060f;font-weight:500;text-align:center;margin-bottom: 0px;">Your Order has been Refunded !</p>

                                       <p class="basic_content">This is a Notification to confirm that we have successfully processed a Rs.1550 refund for your purchase <b>Frock DeepPink-9M-Cotton </b> This refund applies to order <b>20201105-20532313</b>.</p>
                                       <p class="basic_content">We will automatically credit the funds to your <b> MasterCard ending 1234.</b> Please note it might take 5 bussiness days for the money to appear back in your account.</p>
                                   
                                    </div>
                                    <hr style="margin :25px;color:grey;">
                                    <div class="footer basic_content">
                                        <span>FOLLOW US </span>
                                        <span>
                                            <img src="public/twitter.png" class="item_img"></img> 
                                            <img src="public/facebook.png" class="item_img"></img> 
                                            <img src="public/instagram.png" class="item_img"></img> 
                                            </span>   
                                    </div>
                                </td>    
                            </tr>
                        </table>

                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>

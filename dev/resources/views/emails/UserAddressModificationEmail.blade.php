<html>

<head>
    <!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="date=no">
    <meta name="format-detection" content="address=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="x-apple-disable-message-reformatting">
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500,500i,700,700i&amp;display=swap"
        rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />
    <!--<![endif]-->
    <title>Email Template</title>
    <!--[if gte mso 9]>
	<style type="text/css" media="all">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->
    <style type="text/css" media="screen">
    /* Linked Styles */

    body {
        padding: 0 !important;
        margin: 0 auto !important;
        display: block !important;
        min-width: 100% !important;
        width: 100% !important;
        background: #f0f0f0;
        -webkit-text-size-adjust: none
    }

    a {
        color: #58595b;
        text-decoration: none
    }

    p {
        padding: 0 !important;
        margin: 0 !important
    }

    img {
        -ms-interpolation-mode: bicubic;
        /* Allow smoother rendering of resized image in Internet Explorer */
    }

    .mcnPreviewText {
        display: none !important;
    }

    .text-btn a {
        display: block;
    }

    /* Mobile styles */

    @media only screen and (max-device-width: 480px),
    only screen and (max-width: 480px) {
        .mpy-10 {
            padding-top: 10px !important;
            padding-bottom: 10px !important;
        }

        .mpy-30 {
            padding-top: 30px !important;
            padding-bottom: 30px !important;
        }

        .mpx-20 {
            padding-left: 20px !important;
            padding-right: 20px !important;
        }

        .mpb-40 {
            padding-bottom: 40px !important;
        }

        .mpt-30 {
            padding-top: 30px !important;
        }

        .mpb-0 {
            padding-bottom: 0px !important;
        }

        .mpb-20 {
            padding-bottom: 20px !important;
        }

        u+.body .gwfw {
            width: 100% !important;
            width: 100vw !important;
        }

        .td,
        .m-shell {
            width: 100% !important;
            min-width: 100% !important;
        }

        .mt-center {
            text-align: center !important;
        }

        .center {
            margin: 0 auto !important;
        }

        .m-left {
            margin-right: auto !important;
        }

        .m-td,
        .m-hide {
            display: none !important;
            width: 0 !important;
            height: 0 !important;
            font-size: 0 !important;
            line-height: 0 !important;
            min-height: 0 !important;
        }

        .m-block {
            display: block !important;
        }

        .fluid-img img {
            width: 100% !important;
            max-width: 100% !important;
            height: auto !important;
        }

        .column,
        .column-top,
        .column-dir,
        .column-bottom,
        .column-dir-top {
            float: left !important;
            width: 100% !important;
            display: block !important;
        }

        .content-spacing {
            width: 15px !important;
        }
    }
    </style>

    <style class="esdev-css">
    /* CONFIG STYLES Please do not delete and edit CSS styles below */
    /* IMPORTANT THIS STYLES MUST BE ON FINAL EMAIL */
    #outlook a {
        padding: 0;
    }

    .ExternalClass {
        width: 100%;
    }

    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
        line-height: 100%;
    }

    .es-button {
        mso-style-priority: 100 !important;
        text-decoration: none !important;
    }

    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }

    .es-desk-hidden {
        display: none;
        float: left;
        overflow: hidden;
        width: 0;
        max-height: 0;
        line-height: 0;
        mso-hide: all;
    }

    td .es-button-border:hover a.es-button-1556804085234 {
        background: #e75e83 !important;
        border-color: #e75e83 !important;
    }

    td .es-button-border-1556804085253:hover {
        background: #e75e83 !important;
    }

    .es-button-border:hover {
        background: #e75e83 !important;
        border-color: #e75e83 #e75e83 #e75e83 #e75e83 !important;
    }

    td .es-button-border:hover a.es-button-1556806949166 {
        background: #e75e83 !important;
        border-color: #e75e83 !important;
    }

    td .es-button-border-1556806949166:hover {
        background: #e75e83 !important;
    }

    /*
END OF IMPORTANT
*/
    s {
        text-decoration: line-through;
    }

    html,
    body {
        width: 100%;
        font-family: arial, 'helvetica neue', helvetica, sans-serif;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
    }

    table {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        border-collapse: collapse;
        border-spacing: 0px;
    }

    table td,
    html,
    body,
    .es-wrapper {
        padding: 0;
        Margin: 0;
    }

    .es-content,
    .es-header,
    .es-footer {
        table-layout: fixed !important;
        width: 100%;
    }

    img {
        display: block;
        border: 0;
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
    }

    table tr {
        border-collapse: collapse;
    }

    p,
    hr {
        Margin: 0;
    }

    h1,
    h2,
    h3,
    h4,
    h5 {
        Margin: 0;
        line-height: 120%;
        mso-line-height-rule: exactly;
        font-family: arial, 'helvetica neue', helvetica, sans-serif;
    }

    p,
    ul li,
    ol li,
    a {
        -webkit-text-size-adjust: none;
        -ms-text-size-adjust: none;
        mso-line-height-rule: exactly;
    }

    .es-left {
        float: left;
    }

    .es-right {
        float: right;
    }

    .es-p5 {
        padding: 5px;
    }

    .es-p5t {
        padding-top: 5px;
    }

    .es-p5b {
        padding-bottom: 5px;
    }

    .es-p5l {
        padding-left: 5px;
    }

    .es-p5r {
        padding-right: 5px;
    }

    .es-p10 {
        padding: 10px;
    }

    .es-p10t {
        padding-top: 10px;
    }

    .es-p10b {
        padding-bottom: 10px;
    }

    .es-p10l {
        padding-left: 10px;
    }

    .es-p10r {
        padding-right: 10px;
    }

    .es-p15 {
        padding: 15px;
    }

    .es-p15t {
        padding-top: 15px;
    }

    .es-p15b {
        padding-bottom: 15px;
    }

    .es-p15l {
        padding-left: 15px;
    }

    .es-p15r {
        padding-right: 15px;
    }

    .es-p20 {
        padding: 20px;
    }

    .es-p20t {
        padding-top: 20px;
    }

    .es-p20b {
        padding-bottom: 20px;
    }

    .es-p20l {
        padding-left: 20px;
    }

    .es-p20r {
        padding-right: 20px;
    }

    .es-p25 {
        padding: 25px;
    }

    .es-p25t {
        padding-top: 25px;
    }

    .es-p25b {
        padding-bottom: 25px;
    }

    .es-p25l {
        padding-left: 25px;
    }

    .es-p25r {
        padding-right: 25px;
    }

    .es-p30 {
        padding: 30px;
    }

    .es-p30t {
        padding-top: 30px;
    }

    .es-p30b {
        padding-bottom: 30px;
    }

    .es-p30l {
        padding-left: 30px;
    }

    .es-p30r {
        padding-right: 30px;
    }

    .es-p35 {
        padding: 35px;
    }

    .es-p35t {
        padding-top: 35px;
    }

    .es-p35b {
        padding-bottom: 35px;
    }

    .es-p35l {
        padding-left: 35px;
    }

    .es-p35r {
        padding-right: 35px;
    }

    .es-p40 {
        padding: 40px;
    }

    .es-p40t {
        padding-top: 40px;
    }

    .es-p40b {
        padding-bottom: 40px;
    }

    .es-p40l {
        padding-left: 40px;
    }

    .es-p40r {
        padding-right: 40px;
    }

    .es-menu td {
        border: 0;
    }

    .es-menu td a img {
        display: inline-block !important;
    }

    /* END CONFIG STYLES */
    a {
        font-family: arial, 'helvetica neue', helvetica, sans-serif;
        font-size: 14px;
        text-decoration: none;
    }

    h1 {
        font-size: 30px;
        font-style: normal;
        font-weight: normal;
        color: #e75e83;
    }

    h1 a {
        font-size: 30px;
    }

    h2 {
        font-size: 26px;
        font-style: normal;
        font-weight: bold;
        color: #e75e83;
    }

    h2 a {
        font-size: 26px;
    }

    h3 {
        font-size: 22px;
        font-style: normal;
        font-weight: normal;
        color: #e75e83;
    }

    h3 a {
        font-size: 22px;
    }

    p,
    ul li,
    ol li {
        font-size: 14px;
        font-family: arial, 'helvetica neue', helvetica, sans-serif;
        line-height: 150%;
    }

    ul li,
    ol li {
        Margin-bottom: 15px;
    }

    .es-menu td a {
        text-decoration: none;
        display: block;
    }

    .es-wrapper {
        width: 100%;
        height: 100%;
        background-repeat: repeat;
        background-position: center top;
    }

    .es-wrapper-color {
        background-color: #f6f6f6;
    }

    .es-content-body {
        background-color: #ffffff;
    }

    .es-content-body p,
    .es-content-body ul li,
    .es-content-body ol li {
        color: #333333;
    }

    .es-content-body a {
        color: #e75e83;
    }

    .es-header {
        background-color: transparent;
        background-repeat: repeat;
        background-position: center top;
    }

    .es-header-body {
        background-color: #ffffff;
    }

    .es-header-body p,
    .es-header-body ul li,
    .es-header-body ol li {
        color: #e75e83;
        font-size: 16px;
    }

    .es-header-body a {
        color: #e75e83;
        font-size: 16px;
    }

    .es-footer {
        background-color: transparent;
        background-repeat: repeat;
        background-position: center top;
    }

    .es-footer-body {
        background-color: transparent;
    }

    .es-footer-body p,
    .es-footer-body ul li,
    .es-footer-body ol li {
        color: #ffffff;
        font-size: 14px;
    }

    .es-footer-body a {
        color: #ffffff;
        font-size: 14px;
    }

    .es-infoblock,
    .es-infoblock p,
    .es-infoblock ul li,
    .es-infoblock ol li {
        line-height: 120%;
        font-size: 12px;
        color: #cccccc;
    }

    .es-infoblock a {
        font-size: 12px;
        color: #cccccc;
    }

    .es-button-border {
        border-style: solid solid solid solid;
        border-color: #e75e83 #e75e83 #e75e83 #e75e83;
        background: #e75e83;
        border-width: 0px 0px 0px 0px;
        display: inline-block;
        border-radius: 0px;
        width: auto;
    }

    /* RESPONSIVE STYLES Please do not delete and edit CSS styles below. If you don't need responsive layout, please delete this section. */
    @media only screen and (max-width: 600px) {

        p,
        ul li,
        ol li,
        a {
            font-size: 25px !important;
            line-height: 150% !important;
        }

        h1 {
            font-size: 30px !important;
            text-align: center;
            line-height: 120% !important;
        }

        strong {
            font-size: 25px;
        }

        h2 {
            font-size: 22px !important;
            text-align: center;
            line-height: 120% !important;
        }

        h3 {
            font-size: 20px !important;
            text-align: center;
            line-height: 120% !important;
        }

        h1 a {
            font-size: 30px !important;
        }

        h2 a {
            font-size: 22px !important;
        }

        h3 a {
            font-size: 20px !important;
        }

        h4 {
            font-size: 25px;
        }

        .rs {
            padding-left: 20px;
            padding-right: 20px;
        }

        .es-menu td a {
            font-size: 16px !important;
        }

        .es-header-body p,
        .es-header-body ul li,
        .es-header-body ol li,
        .es-header-body a {
            font-size: 16px !important;
        }

        .font-md-20 {
            font-size: 20px !important;
        }

        .font-md-14 {
            font-size: 14px !important;
        }

        .font-md-25 {
            font-size: 25px !important;
        }

        .font-md-23 {
            font-size: 23px !important;
        }

        .lh-55 {
            line-height: 55px !important;
        }

        .es-footer-body p,
        .es-footer-body ul li,
        .es-footer-body ol li,
        .es-footer-body a {
            font-size: 14px !important;
        }

        .es-infoblock p,
        .es-infoblock ul li,
        .es-infoblock ol li,
        .es-infoblock a {
            font-size: 12px !important;
        }

        *[class="gmail-fix"] {
            display: none !important;
        }

        .es-m-txt-c,
        .es-m-txt-c h1,
        .es-m-txt-c h2,
        .es-m-txt-c h3 {
            text-align: center !important;
        }

        .es-m-txt-r,
        .es-m-txt-r h1,
        .es-m-txt-r h2,
        .es-m-txt-r h3 {
            text-align: right !important;
        }

        .es-m-txt-l,
        .es-m-txt-l h1,
        .es-m-txt-l h2,
        .es-m-txt-l h3 {
            text-align: left !important;
        }

        .es-m-txt-r img,
        .es-m-txt-c img,
        .es-m-txt-l img {
            display: inline !important;
        }

        .es-button-border {
            display: block !important;
        }

        .es-btn-fw {
            border-width: 10px 0px !important;
            text-align: center !important;
        }

        .es-adaptive table,
        .es-btn-fw,
        .es-btn-fw-brdr,
        .es-left,
        .es-right {
            width: 100% !important;
        }

        .es-content table,
        .es-header table,
        .es-footer table,
        .es-content,
        .es-footer,
        .es-header {
            width: 100% !important;
            max-width: 600px !important;
        }

        .es-adapt-td {
            display: block !important;
            width: 100% !important;
        }

        .adapt-img {
            width: 100% !important;
            height: auto !important;
        }

        .es-m-p0 {
            padding: 0px !important;
        }

        .es-m-p0r {
            padding-right: 0px !important;
        }

        .es-m-p0l {
            padding-left: 0px !important;
        }

        .es-m-p0t {
            padding-top: 0px !important;
        }

        .es-m-p0b {
            padding-bottom: 0 !important;
        }

        .es-m-p20b {
            padding-bottom: 20px !important;
        }

        .es-mobile-hidden,
        .es-hidden {
            display: none !important;
        }

        tr.es-desk-hidden,
        td.es-desk-hidden,
        table.es-desk-hidden {
            width: auto !important;
            overflow: visible !important;
            float: none !important;
            max-height: inherit !important;
            line-height: inherit !important;
        }

        tr.es-desk-hidden {
            display: table-row !important;
        }

        table.es-desk-hidden {
            display: table !important;
        }

        td.es-desk-menu-hidden {
            display: table-cell !important;
        }

        .es-menu td {
            width: 1% !important;
        }

        table.es-table-not-adapt,
        .esd-block-html table {
            width: auto !important;
        }

        table.es-social {
            display: inline-block !important;
        }

        table.es-social td {
            display: inline-block !important;
        }

        a.es-button,
        button.es-button {
            font-size: 20px !important;
            display: block !important;
            border-left-width: 0px !important;
            border-right-width: 0px !important;
        }
    }

    /* END RESPONSIVE STYLES */
    a.es-button,
    button.es-button {
        border-style: solid;
        border-color: #e75e83;
        border-width: 10px 20px 10px 20px;
        display: inline-block;
        background: #e75e83;
        border-radius: 0px;
        font-size: 18px;
        font-family: arial, 'helvetica neue', helvetica, sans-serif;
        font-weight: normal;
        font-style: normal;
        line-height: 120%;
        color: #ffffff;
        text-decoration: none;
        width: auto;
        text-align: center;
    }

    .es-button-border:hover a.es-button,
    .es-button-border:hover button.es-button {
        background: #e75e83 !important;
        border-color: #e75e83 !important;
    }

    .overlay {
        background-color: #FFFFFF;
        border: 1px solid #00000054;
        color: #000000;
        padding: 0.5em;
    }
    </style>
</head>

<body class="body">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" class="gwfw">
        <tbody>
            <tr>
                <td align="center" valign="top" class="py-50 mpy-10 px-10"
                    style="padding-top: 50px; padding-left: 10px; padding-right: 10px;">
                    <table width="750" border="0" cellspacing="0" cellpadding="0" class="m-shell" style="">
                        <tbody>
                            <tr>
                                <td class="td overlay"
                                    style="width:600px; min-width:600px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">

                                    <!-- Header -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                        class="background-body" style="background:#e75e83;">
                                        <tbody>
                                            <tr>
                                                <td class="pb-2" style="padding-bottom: 2px;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                        bgcolor="#ffffff" style="border-radius: 4px 4px 0px 0px;">
                                                        <tbody>
                                                            <tr>
                                                                <td class="py-25 px-50 mpx-20"
                                                                    style="padding-top: 25px; padding-bottom: 25px; padding-left: 50px; padding-right: 50px;">
                                                                    <table width="100%" border="0" cellspacing="0"
                                                                        cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th class="column" width="190"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="img mt-center"
                                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                                    <a href="#"
                                                                                                        target="_blank"><img
                                                                                                            src="https://fabpik.in/assets/images/logo.png"
                                                                                                            width="190"
                                                                                                            border="0"
                                                                                                            alt=""
                                                                                                            style="margin: 0 auto;"></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </th>
                                                                                <th class="column mpb-20" width="20"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                                                                </th>
                                                                                <th class="column"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td align="right">
                                                                                                    <table border="0"
                                                                                                        cellspacing="0"
                                                                                                        cellpadding="0"
                                                                                                        class="center">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="text-14 nav fw-b tt-u"
                                                                                                                    style="color:#333333; font-family:'Montserrat', Arial, sans-serif; font-size:14px; line-height:24px; text-align:left; min-width:auto !important; font-weight:bold; text-transform:uppercase;">

                                                                                                                    <a href="https://fabpik.in/category/clothing"
                                                                                                                        target="_blank"
                                                                                                                        class="link c-dgrey"
                                                                                                                        style="text-decoration:none; color:#333333;"><span
                                                                                                                            class="link c-dgrey"
                                                                                                                            style="text-decoration:none; color:#333333;">Clothing</span></a>
                                                                                                                    <span
                                                                                                                        class="c-grey"
                                                                                                                        style="color:#555555;">&nbsp;
                                                                                                                        |
                                                                                                                        &nbsp;</span>
                                                                                                                    <a href="https://fabpik.in/category/toys"
                                                                                                                        target="_blank"
                                                                                                                        class="link c-dgrey"
                                                                                                                        style="text-decoration:none; color:#333333;"><span
                                                                                                                            class="link c-dgrey"
                                                                                                                            style="text-decoration:none; color:#333333;">TOYS</span></a>
                                                                                                                    <span
                                                                                                                        class="c-grey"
                                                                                                                        style="color:#555555;">&nbsp;
                                                                                                                        |
                                                                                                                        &nbsp;</span>
                                                                                                                    <a href="https://fabpik.in/"
                                                                                                                        target="_blank"
                                                                                                                        class="link c-dgrey"
                                                                                                                        style="text-decoration:none; color:#333333;"><span
                                                                                                                            class="link c-dgrey"
                                                                                                                            style="text-decoration:none; color:#333333;">More</span></a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </th>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                        class="background-body" style="background:#ffff;">
                                        <tbody>
                                            <tr>
                                                <td class="pb-2" style="padding-bottom: 2px;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                        bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td class="py-50 px-50 mpy-30 mpx-20"
                                                                    style="padding-top: 10px; padding-bottom: 10px; padding-left: 50px; padding-right: 50px;">
                                                                    <table width="100%" border="0" cellspacing="0"
                                                                        cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u es-p30t es-p30b fw-b"
                                                                                    style="font-family:'Montserrat', Arial, sans-serif; font-size:14px; line-height:24px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; text-transform:uppercase; font-weight:bold;">
                                                                                    <strong>Hi,</strong>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 a-center pb-22 lh-28"
                                                                                    style="color:#333333; font-family:'Montserrat', Arial, sans-serif; font-size:14px; min-width:auto !important; text-align:center; padding-bottom: 22px; line-height:28px;">
                                                                                    <p
                                                                                        style="color: #333333;font-family:'Montserrat', Arial, sans-serif;">
                                                                                        As per your request your
                                                                                        delivery address is modified.
                                                                                        Please
                                                                                        find
                                                                                        your details below.</p>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END Title + Copy + Price + Button -->
                                    <!-- Title + Three Properties -->

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                        class="background-body" style="background:#fff;">
                                        <tbody>
                                            <tr>
                                                <td class="pb-2" style="padding-bottom: 2px;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                        class="t-bg" style="background:#f0f4f7;">
                                                        <tbody>
                                                            <tr>
                                                                <td class="py-50 px-50 mpy-30 mpx-20"
                                                                    style="padding-top: 50px; padding-bottom: 50px; padding-left: 50px; padding-right: 50px;">
                                                                    <table width="100%" border="0" cellspacing="0"
                                                                        cellpadding="0">
                                                                        <tbody>
                                                                                <tr>
                                                                                    <td class="text-20 c-white fw-b pb-18"
                                                                                        style="font-family:'Montserrat', Arial, sans-serif; font-size:20px; line-height:40px; text-align:left; min-width:auto !important; color:#000; font-weight:bold; padding-bottom: 18px;">
                                                                                        <b>SHIPPING ADDRESS:</b></br>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="text-14 c-white lh-28 font-md-20"
                                                                                        style="font-family:'Montserrat', Arial, sans-serif; font-size:14px; text-align:left; min-width:auto !important; color:#000; line-height:28px;">
                                                                                        <b>{{$data['current_shipping_address1']}}, {{$data['current_shipping_address2']}},</br>
                                                                                        {{$data['current_shipping_city']}}, {{$data['current_shipping_state']}}, {{$data['current_shipping_pincode']}}</br>
                                                                                    </td>
                                                                                </tr>                                                                          
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- Footer -->
                    <table width="100%" border="0" bgcolor="#ffffff" cellspacing="0" cellpadding="0"
                        style="margin-top: 40px;">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                                        <tbody>
                                            <tr>
                                                <td class="plr-15" style="padding: 40px 30px;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" style="padding-bottom: 35px;">
                                                                    <!-- Socials -->
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="img" width="11"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                    <a href="https://www.facebook.com/fabpik.in"
                                                                                        target="_blank">
                                                                                        <img src="https://portal.fabpik.in/PORTAL/uploads/facebook.png"
                                                                                            width="25px" alt=""
                                                                                            srcset="">

                                                                                    </a>
                                                                                </td>
                                                                                <td class="img" width="40"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                </td>
                                                                                <td class="img" width="23"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                    <a href="https://twitter.com/FabpikI"
                                                                                        target="_blank">
                                                                                        <img src="https://portal.fabpik.in/PORTAL/uploads/twitter.png"
                                                                                            width="25px" alt=""
                                                                                            srcset="">
                                                                                    </a>
                                                                                </td>
                                                                                <td class="img" width="36"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                </td>
                                                                                <td class="img" width="22"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                    <a href="https://www.instagram.com/fabpik.in/"
                                                                                        target="_blank">
                                                                                        <img src="https://portal.fabpik.in/PORTAL/uploads/instagram.png"
                                                                                            width="25px" alt=""
                                                                                            srcset="">
                                                                                    </a>
                                                                                </td>
                                                                                <td class="img" width="36"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                </td>
                                                                                <td class="img" width="26"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                    <a href="https://www.youtube.com/channel/UCDZDbmBT380YUB-KBSkds4Q?"
                                                                                        target="_blank">
                                                                                        <img src="https://portal.fabpik.in/PORTAL/uploads/youtube-logotype.png"
                                                                                            width="25px" alt=""
                                                                                            srcset="">
                                                                                    </a>
                                                                                </td>
                                                                                <td class="img" width="36"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                </td>
                                                                                <td class="img" width="26"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                    <a href="https://in.pinterest.com/FabPikIndia/"
                                                                                        target="_blank">
                                                                                        <img src="https://portal.fabpik.in/PORTAL/uploads/pinterest-logo.png"
                                                                                            width="25px" alt=""
                                                                                            srcset="">
                                                                                    </a>
                                                                                </td>
                                                                                <td class="img" width="36"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                </td>
                                                                                <td class="img" width="26"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                    <a href="https://www.snapchat.com/add/fabpikindia"
                                                                                        target="_blank">
                                                                                        <img src="https://portal.fabpik.in/PORTAL/uploads/snapchat.png"
                                                                                            width="25px" alt=""
                                                                                            srcset="">
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <!-- END Socials -->
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="padding-bottom: 35px;">
                                                                    <!-- Socials -->
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="img" width="11"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                    <a href="https://www.facebook.com/fabpik.in"
                                                                                        target="_blank">
                                                                                        <img src="https://fabpik.in/assets/images/icons/app-store-button.png"
                                                                                            width="110px" alt="">

                                                                                    </a>
                                                                                </td>
                                                                                <td class="img" width="15"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                </td>
                                                                                <td class="img" width="23"
                                                                                    style="font-size:0pt; line-height:0pt; text-align:left;">
                                                                                    <a href="https://twitter.com/FabpikI"
                                                                                        target="_blank">
                                                                                        <img src="https://fabpik.in/assets/images/icons/google-play-button.png"
                                                                                            width="110px" alt="">

                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <!-- END Socials -->
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" border="0" cellspacing="0"
                                                                        cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th class="column mpb-20" width="4%"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                                                                </th>
                                                                                <th class="column-bottom" width="42%"
                                                                                    style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:bottom;">
                                                                                    <table width="100%" border="0"
                                                                                        cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="text-14 lh-28 c-grey mt-center"
                                                                                                    style="font-family:'Montserrat', Arial, sans-serif; font-size:14px; text-align:center; min-width:auto !important; line-height:28px; color:#555555;">
                                                                                                    <p>
                                                                                                        <b>In case of
                                                                                                            any queries,
                                                                                                            please feel
                                                                                                            free to
                                                                                                            contact our
                                                                                                            Support
                                                                                                            Team</b>
                                                                                                        <br>•
                                                                                                        {{CUSTOMER_SUPPORT_MOBILE_NUMBER}}
                                                                                                        <br>•
                                                                                                        {{CUSTOMER_SUPPORT_EMAIL}}
                                                                                                    </p>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </th>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- END Footer -->
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
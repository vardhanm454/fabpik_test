<!doctype html>
<html>
    <head>
        <!-- <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> -->
        <title></title>
        
        <link rel="stylesheet" href="template.css">
    </head>
    <body>
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" class="body">
            <tr>
                <td class="container">      
                    <div class="content">
                        <div class="header">
                            <hr class="header_hr">
                            <b class="header_title">Fabpik</b>
                        </div>   
                        <!-- <table role="presentation" class="main">

                        </table> -->
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="main">
                            <tr>    
                                <td>
                                    <!-- font-family: Whitney,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica,Arial,sans-serif; -->
                                    <div class="content_main">
                                        <b>Hi  {{$data->name}},</b></br>
                                        <p class="basic_content">    
                                           <b>Thank you for registering with us.</b> 
                                        </br> FabPik - One stop portal for your little ones.
                                            Clothing, Toys, Accessories, Footwear, Personal Care, Books, Sports needs and many more.
                                        </p>
                                    </div>
                                    <hr style="margin :25px;color:grey;">
                                    <div class="content_main">
                                        <p class="basic_content"> 
                                            <b>Your Account Details:</b></br>
                                            Username : {{$data->email}}
                                        </p>
                                        <p class="basic_content">     
                                            Login and Start exploring at <a href="{{URL::to('/')}}" style="color:#ef808b;">www.fabpik.in</a></br>
                                         </p>
                                         <img src="public/saleoffer5.jpg" class="content_img"></img>
                                         <div>
                                            <img src="public/saleoffer7.jpg" class="" style="width:150px"></img> 
                                            <img src="public/saleoffer8.jpg" class="" style="width:150px"></img> 
                                            <img src="public/saleoffer9.jpg" class="" style="width:150px"></img> 
                                         </div>
                                    </div>
                                    <hr style="margin :25px;color:grey;">
                                    <div class="content_main basic_content" >     
                                            You are receiving this email because
                                            <ul>
                                                <li>You're an awesome customer of "FabPik"</li>
                                                <li>You subscribed via our website</li>
                                            </ul>
                                            <div class="footer">
                                                <span>For any help, reach out to us at: 040-418-91487;</span> <span> Mail Us: wecare@fabpik.in</span>
                                                <span>FOLLOW US </span>
                                                <span>
                                                    <img src="public/twitter.png" class="item_img"></img> 
                                                    <img src="public/facebook.png" class="item_img"></img> 
                                                    <img src="public/instagram.png" class="item_img"></img> 
                                                    </span>   
                                            </div>   

                                    </div>
                                </td>    
                            </tr>
                        </table>

                        <!-- <div class="footer" style="background-color:white;margin-top: 3px;">
                            For any help, reach out to us at: 7898675436; Mail Us: wecare@fabpik.in
                            <center>Fabpik</center>
                        </div> -->

                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>

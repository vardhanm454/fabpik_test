<!doctype html>
<html>
    <head>
        <!-- <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> -->
        <title></title>
        
<style>
    body {
  background-color: #f6f6f6;
  font-family: Whitney,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica,Arial,sans-serif;
  -webkit-font-smoothing: antialiased;
  font-size: 16px;
  line-height: 1.4;
  margin: 0;
  padding: 0;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%; 
  }
  table {
    border-collapse: separate;
    mso-table-lspace: 0pt;
    mso-table-rspace: 0pt;
    width: 100%; }
    table td {
    /* font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"; */
    font-size: 15px;
    vertical-align: top; 
    }
   /* -------------------------------------
    BODY & CONTAINER
    ------------------------------------- */
    .body {
    background-color: #f6f6f6;
    width: 100%; 
    }
    /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
    .container {
    display: block;
    margin: 0 auto !important;
    /* makes it centered */
    max-width: 580px;
    padding: 10px;
    width: 580px; 
    }
     /* This should also be a block element, so that it will fill 100% of the .container */
    .content {
    box-sizing: border-box;
    display: block;
    margin: 0 auto;
    max-width: 580px;
    padding: 10px; 
    }
    .header{
        background-color:white;margin-bottom: 3px;
    }
    .header_hr{
        border-bottom: 5px solid #f45454;
    }
    .header_title{
        font-size:35px;color:#f45454;margin-left:14px;
    }
    .main {
        background: #fff;
        border-radius: 3px;
        width: 100%; 
        }
    .content_main{
        margin : 25px;
    }
    .content_img{
        display: block;
        margin: 0 auto;
        height: 85px;
        width:100%;
    }
    .content_a{
        font-weight: 500; font-size:15px; margin-top:20px;color: green; text-align:center;
    }
    .content_b{
        font-size:24px;text-align:center;font-weight: 500;color:black
    }
    .content_user{
        margin-left:30px;
    }
    .btn{
        display: inline-block;
    font-weight: 400;
    color: #212529;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .btn-danger {
        color: #fff;
        background-color: #dc3545;
        border-color: #dc3545;
    }
    .content_main2{
        font-size:13px;
    }
    .item_img{
        width:20px !important;
    }
    .basic_content{
        color:#747876;
        text-align:left;
        font-size:13px;
        margin-bottom:25px;
    }
    .heading{
        font-size:15px;
        margin-top:10px;
        margin-bottom:3px;
        text-align:left;
    }
    .footer{
        margin:25px;
    }
    .grid-container{
        display :grid;
        grid-template-columns: auto auto auto auto auto;
        margin-top:10px;
    }
    /* .grid-item{
        border-radius:10px solid grey;
        border:1px solid grey;
    } */
</style>
    </head>
    <body>
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" class="body">
            <tr>
                <td class="container">      
                    <div class="content">
                        <div class="header">
                            <hr class="header_hr">
                            <b class="header_title">Fabpik</b>
                        </div>   

                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="main">
                            <tr>    
                                <td>
                                    <div class="content_main">
                                        <b>Hi {{$data->order->first_name}},</b>
                                        <p style="font-size:22px;color:#04060f;font-weight:500;text-align:center;margin-bottom: 0px;">Your Return has been Proceed !</p>
                                        <img style="height: 98px;margin: 0 auto;display: block;" src="https://ayala.com.ph/sustainability/sites/default/files/images/icon/Ayala-Sustainability-May-2019-AnimatedIcons-Truck-GIF.gif"></img>

                                       <p class="basic_content">Your Return for order <b>{{$data->child_order_id}}</b> has been proceed - you will receive a full refund for your items by 15 november, 2020 to the original payment method used.</p>
                                       <p class="basic_content">If you have any questions,or wish to discuss this with a member of team,please visit our <a href="{{URL::to('/ContactUs')}}" style="color:#ef808b;">Contact Us</a> page for futher information.</p>
                                   
                                    </div>
                                    <hr style="margin :25px;color:grey;">
                                    <div class="footer basic_content">
                                        <span>FOLLOW US </span>
                                        <span>
                                            <img src="public/twitter.png" class="item_img"></img> 
                                            <img src="public/facebook.png" class="item_img"></img> 
                                            <img src="public/instagram.png" class="item_img"></img> 
                                            </span>   
                                    </div>
                                </td>    
                            </tr>
                        </table>

                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>

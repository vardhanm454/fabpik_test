
<!DOCTYPE html>
<html lang="en" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

  <head>
    <meta charset="utf-8">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no, date=no, address=no, email=no">
    <!--[if mso]>
    <xml><o:OfficeDocumentSettings><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml>
    <style>
      td,th,div,p,a,h1,h2,h3,h4,h5,h6 {font-family: "Segoe UI", sans-serif; mso-line-height-rule: exactly;}
    </style>
  <![endif]-->
    <title>Feedback Form</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet" media="screen">
    <style>
      .hover-underline:hover {
        text-decoration: underline !important;
      }

      @keyframes spin {
        to {
          transform: rotate(360deg);
        }
      }

      @keyframes ping {

        75%,
        100% {
          transform: scale(2);
          opacity: 0;
        }
      }

      @keyframes pulse {
        50% {
          opacity: .5;
        }
      }

      @keyframes bounce {

        0%,
        100% {
          transform: translateY(-25%);
          animation-timing-function: cubic-bezier(0.8, 0, 1, 1);
        }

        50% {
          transform: none;
          animation-timing-function: cubic-bezier(0, 0, 0.2, 1);
        }
      }

      @media (max-width: 600px) {
        .sm-leading-32 {
          line-height: 32px !important;
        }

        .sm-px-24 {
          padding-left: 24px !important;
          padding-right: 24px !important;
        }

        .sm-py-32 {
          padding-top: 32px !important;
          padding-bottom: 32px !important;
        }

        .sm-w-full {
          width: 100% !important;
        }
      }
    </style>
  </head>

  <body style="margin: 0; padding: 0; width: 100%; word-break: break-word; -webkit-font-smoothing: antialiased; --bg-opacity: 1; background-color: #eceff1; background-color: rgba(236, 239, 241, var(--bg-opacity));">
    <div style="display: none;">Please verify your email address</div>
    <div role="article" aria-roledescription="email" aria-label="Verify Email Address" lang="en">
      <table style="font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; width: 100%;" width="100%" cellpadding="0" cellspacing="0" role="presentation">
        <tr>
          <td align="center" style="--bg-opacity: 1; background-color: #eceff1;margin-top:20px; background-color: rgba(236, 239, 241, var(--bg-opacity)); font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif;" bgcolor="rgba(236, 239, 241, var(--bg-opacity))">
            <table class="sm-w-full" style="font-family:  'Lato', sans-serif ; width: 600px;" width="600" cellpadding="0" cellspacing="0" role="presentation">
             
              <tr>
                <td align="center" class="sm-px-24" style="font-family:  'Lato', sans-serif ;">
                  <table style="font-family:  'Lato', sans-serif ; width: 100%;margin-top: 30px;" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                     
                  
                    <tr>
                      
                      <td class="sm-px-24" style="--bg-opacity: 1; background-color: #ffffff; background-color: rgba(255, 255, 255, var(--bg-opacity)); border-radius: 4px; font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif; font-size: 14px; line-height: 24px; padding: 48px; text-align: left; --text-opacity: 1; color: #626262; color: rgba(98, 98, 98, var(--text-opacity));" bgcolor="rgba(255, 255, 255, var(--bg-opacity))" align="left">
                        <a>
                            <img src="http://13.126.160.234/fabpikseller/app-assets/images/logo/logo.png" width="155" alt="Fabpik Admin" style="border: 0;
                            max-width: 100%;
                            line-height: 100%;
                            text-align: center;
                            vertical-align: middle;
                            margin: 0 auto;
                            /* width: 100%; */
                            align-items: center;
                            justify-content: center;
                            display: block;">
                          </a>
                        <p style="    font-weight: 600;
                        font-size: 18px;
                        margin-bottom: 0;
                        margin-top: 0;
                        text-align: center;">Give feedback for you order in Fabpik</p>
                        <p style="font-weight: 600; font-size: 15px; margin-top: 0; text-align: center; --text-opacity: 1; color: #ff5850; color: rgba(255, 88, 80, var(--text-opacity));">Order ID :  10001041</p>
                       
                        <p style="margin: 0 0 2px;">
                        Hi,👋
                        </p>
                  
                        <p style="    margin: 0 0 10px;
                        line-height: 19px;">
                            Please take a minute to feedback your experience with the following product
                            </p>
                            <div  class="row shopping-block ng-star-inserted" style="    background-color: #fff;
                            border: 1px solid transparent;
                            margin: 0;
                            padding: 10px 15px 10px;
                            align-items: center;
                            margin-bottom: 7px;
                            position: relative;display: flex;
        flex-wrap: wrap;
        border: 1px solid #cac7f6 !important;
                            box-shadow: 0 2px 18px 1px rgb(49 53 72 / 10%);">
                                <div  class="col-lg-2 col-sm-4 col-xs-4"><a   href="/products/boys-polo-t-shirt-skate-themed-all-over401034423/10000004"><img  src="http://localhost/fabpik-latest/dev/uploads/100000/products/01022021/401034423-FRONT.jpg" width="55"></a></div>
                                <div  class="col-lg-10 col-sm-8 col-xs-8 ">
                                    <p style="font-size: 9px;
                                    width: 100%;
                                    color: #e75d81;
                                    text-overflow: ellipsis;
                                    overflow: hidden;
                                    white-space: nowrap;
                                    font-weight: 600;
                                    text-transform: uppercase;
                                    margin-bottom: 0;
                                    margin-top: 0;
                                    padding-left:10px;
                                    line-height: 15px;">Meenaakshi overseas</p><a   href="/products/boys-polo-t-shirt-skate-themed-all-over401034423/10000004">
                                        <h4 style="font-size: 14px !important;
                                        margin-top: 0px;
                                        line-height: 13px !important;
                                        color: #261e1e;
                                        text-overflow: ellipsis;
                                        overflow: hidden;
                                        padding-left:10px;
                                        margin-bottom: 0;
                                        font-weight: 500;
                                        width: 100%;">Boys Polo T Shirt skate Themed All Over</h4>
                                    </a>
                                    <p style="    list-style-type: none;
                                    display: inline-block;
                                    font-size: 11px;
                                    margin-right: 7px;
                                    font-weight: 700;
                                    margin: 0;
                                    padding-left: 10px;">Size:<span>38</span></p>
                                  
                                </div>
                            </div>
                            <p class="sm-leading-32" style="font-weight: 600; line-height: 19px; font-size: 15px; margin: 17px 0 11px; --text-opacity: 1; color: #263238; color: rgba(38, 50, 56, var(--text-opacity));">
                                Please share your feedback with us & you can win cashback for writing feedback
                              </p>
                        
                        <a href="https://fabpik.in/feedback_url" style="display: block; font-size: 14px; line-height: 100%; margin-bottom: 24px; --text-opacity: 1;    line-height: 100%;
                        margin-bottom: 11px;
                     color: #e75d81; text-decoration: none;">https://fabpik.in/feedback_url</a>
                        <table style="font-family:  'Lato', sans-serif ;" cellpadding="0" cellspacing="0" role="presentation">
                          <tr>
                            <td style="mso-padding-alt: 16px 24px; --bg-opacity: 1; background-color: #e75d81; border-radius: 4px; font-family: Montserrat, -apple-system, 'Segoe UI', sans-serif;" bgcolor="rgba(115, 103, 240, var(--bg-opacity))">
                              <a href="https://fabpik.in/feedback_url" style="display: block; font-weight: 600; font-size: 14px; line-height: 100%;     padding: 7px 24px; --text-opacity: 1; color: #ffffff; color: rgba(255, 255, 255, var(--text-opacity)); text-decoration: none;">Give Your Feedback &rarr;</a>
                            </td>
                          </tr>
                        </table>
                      
                        
                       
                      </td>
                    </tr>
                    <tr>
                      <td style="font-family:  'Lato', sans-serif ; height: 20px;" height="20"></td>
                    </tr>
                   
                    <tr>
                      <td style="font-family:  'Lato', sans-serif ; height: 16px;" height="16"></td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <th class="column-top" width="54%" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td class="text-14 lh-28 c-grey mt-center" style="font-family:'Montserrat', Arial, sans-serif; font-size:14px; text-align:left; min-width:auto !important; line-height:28px; color:#555555;">
                                                            <p> <b>You are
                                                                    receiving
                                                                    this email
                                                                    because</b>
                                                                <br>• You're an awesome customer of "FabPik"
                                                                <br>• You subscribed via our website </p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </th>
                                        <th class="column mpb-20" width="4%" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"> </th>
                                        <th class="column-bottom" width="42%" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:bottom;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td class="text-14 lh-28 c-grey mt-center" style="font-family:'Montserrat', Arial, sans-serif; font-size:14px; text-align:left; min-width:auto !important; line-height:28px; color:#555555;">
                                                            <p> <b>For any help,
                                                                    reach out to
                                                                    us
                                                                    at:</b>
                                                                    <br>• 040-418-91487
                                                                <br>• wecare@fabpik.in
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </body>

</html>
<!doctype html>
<html>
    <head>
        <!-- <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> -->
        <title></title>
<style>
body {
  background-color: #f6f6f6;
  font-family: Whitney,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica,Arial,sans-serif;
  -webkit-font-smoothing: antialiased;
  font-size: 16px;
  line-height: 1.4;
  margin: 0;
  padding: 0;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%; 
  }
  table {
    border-collapse: separate;
    mso-table-lspace: 0pt;
    mso-table-rspace: 0pt;
    width: 100%; }
    table td {
    /* font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"; */
    font-size: 15px;
    vertical-align: top; 
    }
   /* -------------------------------------
    BODY & CONTAINER
    ------------------------------------- */
    .body {
    background-color: #f6f6f6;
    width: 100%; 
    }
    /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
    .container {
    display: block;
    margin: 0 auto !important;
    /* makes it centered */
    max-width: 580px;
    padding: 10px;
    width: 580px; 
    }
     /* This should also be a block element, so that it will fill 100% of the .container */
    .content {
    box-sizing: border-box;
    display: block;
    margin: 0 auto;
    max-width: 580px;
    padding: 10px; 
    }
    .header{
        background-color:white;margin-bottom: 3px;
    }
    .header_hr{
        border-bottom: 5px solid #f45454;
    }
    .header_title{
        font-size:35px;color:#f45454;margin-left:14px;
    }
    .main {
        background: #fff;
        border-radius: 3px;
        width: 100%; 
        }
    .content_main{
        margin : 25px;
    }
    .content_img{
        display: block;
        margin: 0 auto;
        height: 85px;
        width:100%;
    }
    .content_a{
        font-weight: 500; font-size:15px; margin-top:20px;color: green; text-align:center;
    }
    .content_b{
        font-size:24px;text-align:center;font-weight: 500;color:black
    }
    .content_user{
        margin-left:30px;
    }
    .btn{
        display: inline-block;
    font-weight: 400;
    color: #212529;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .btn-danger {
        color: #fff;
        background-color: #dc3545;
        border-color: #dc3545;
    }
    .content_main2{
        font-size:13px;
    }
    .item_img{
        width:20px !important;
    }
    .basic_content{
        color:#747876;
        text-align:left;
        font-size:13px;
        margin-bottom:25px;
    }
    .heading{
        font-size:15px;
        margin-top:10px;
        margin-bottom:3px;
        text-align:left;
    }
    .footer{
        margin:25px;
    }
    .grid-container{
        display :grid;
        grid-template-columns: auto auto auto auto auto;
        margin-top:10px;
    }
    /* .grid-item{
        border-radius:10px solid grey;
        border:1px solid grey;
    } */
</style>
    </head>
    <body>
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" class="body">
            <tr>
                <td style=" display: block;margin: 0 auto !important;max-width: 750px;padding: 10px;width: 750px; ">      
                    <div style="box-sizing: border-box;display: block;margin: 0 auto; max-width: 750px;padding: 10px; ">
                        <div class="header">
                            <hr class="header_hr">
                            <b class="header_title">Fabpik</b>
                        </div>   
                        <!-- <table role="presentation" class="main">

                        </table> -->
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="main">
                            <tr>    
                                <td>
                                    <!-- font-family: Whitney,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica,Arial,sans-serif; -->
                                    <div class="content_main">
                                        <b>Hi {{$data->order->first_name}},</b>
                                        <h3 class="basic_content">
                                            Thanks for Shooping with us,Your Order Successfully Delivered, Please find Your Order details below:
                                        </h3>
                                    </div>
                                    <hr style="margin :25px;color:grey;">
                                <table class="content_main basic_content">
                                    <tr>
                                        <td>
                                           
                                            <div style="margin-top:12px;">
                                                <ul style=" list-style-type: none;">
                                                    <li> <span style="color:#747876">{{$data->order->billing_first_name}} {{$data->order->billing_last_name}}</span></li>
                                                    <li> <span style="color:#747876">{{$data->order->billing_address1}}, {{$data->order->billing_address2}}</span></li>
                                                    <li> <span style="color:#747876">{{$data->order->billing_city}}, {{$data->order->billing_state}}</span></li>
                                                    <li><span>PinCode : </span> <span style="color:#747876">{{$data->order->billing_pincode}}</span></li>
                                                    <li><span>Phone no : </span> <span style="color:#747876">{{$data->order->billing_mobile}}</span></li>
                                                  </ul>
                                                </div>
                                            
                                            </td>
                                        <td>
                                            <ul style=" list-style-type: none;">
                                                <li><span>Order Date :</span> <span style="color:#747876">{{date('m-d-Y', strtotime($data->order->created_at))}}</span></li>
                                                <li><span>Order ID : </span> <span style="color:#747876">{{$data->child_order_id}}</span></li>
                                                <li><span>Payment Type : </span> <span style="color:#747876">{{($data->payment_status_id == 1)?'Online':'Cash On Delivery'}}</span></li>
                                                <li><span>Total:</span> <span style="color:#747876">{{$data->total}}</span></li>
                                              </ul>
                                        </td>
                                    </tr>\
                                </table>
                                <hr style="margin :25px;color:grey;">
                                
                                <center style="background-color: #f8fafc;margin:25px;">
                                    <table class="body main basic_content" cellpadding="0" cellspacing="0" border="0" class="wrapper">
                                        <tr>
                                            <th style="width:43px;color:rgb(216, 142, 142);border:1px solid #dddddd;text-align:left;padding:8px">S No.</th>
                                            <th style="width:120px;color:rgb(216, 142, 142);border:1px solid #dddddd;text-align:left;padding:8px">Product Details</th>
                                            <th style="width:61px;color:rgb(216, 142, 142);border:1px solid #dddddd;text-align:left;padding:8px">MRP</th>
                                            <th style="width:53px;color:rgb(216, 142, 142);border:1px solid #dddddd;text-align:left;padding:8px">Selling Price</th>
                                            <th style="width:61px;color:rgb(216, 142, 142);border:1px solid #dddddd;text-align:left;padding:8px">Discount</th>
                                            <th style="width:53px;color:rgb(216, 142, 142);border:1px solid #dddddd;text-align:left;padding:8px">Quantity</th>
                                            <th style="width:53px;color:rgb(216, 142, 142);border:1px solid #dddddd;text-align:left;padding:8px">Final Price</th>
                                        </tr>
                                       
                                        <tr>
                        
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;font-weight: 400;"> 1 </th>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;font-weight: 400;">{{$data->productVarient->name}}</th>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;font-weight: 400;text-align:right;">Rs {{$data->mrp}}</th>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;font-weight: 400;text-align:right;"> Rs {{$data->price}}</th>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;font-weight: 400;text-align:right;"> Rs {{$data->discount}}</th>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;font-weight: 400;text-align:right;">  {{$data->quantity}} </th>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;font-weight: 400;text-align:right;"> Rs {{$data->total}}</th>
                                        </tr>
                                         
                                        <tr>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;text-align:right;font-weight:500;color:#4e4f58;" colspan="5">Sub-Total </th>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;text-align:right;font-weight: 400;" colspan="2"> Rs {{$data->total}}</th>
                                        </tr>
                                       
                                        <tr>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;text-align:right;font-weight:500;color:#4e4f58;" colspan="5">Additional Discount</th>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;text-align:right;font-weight: 400;" colspan="2">Rs {{($data->discount)?$data->discount:0}}</th>
                                        </tr>
                                        <tr>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;text-align:right;font-weight:500;color:#4e4f58;" colspan="5">Handling Charges </th>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;text-align:right;font-weight: 400;" colspan="2"> Rs {{($data->shipping_charge)?$data->shipping_charge:0}}</th>
                                        </tr>
                                        <tr>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;text-align:right;font-weight:500;color:#4e4f58;" colspan="5">Total</th>
                                            <th style="border:1px solid #dddddd;text-align:left;padding:8px;text-align:right;font-weight: 400;" colspan="2">Rs {{$data->total+$data->shipping_charg}}</th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </center>

                                <div class="content_main basic_content">
                                   <h5>Disclaimer: </h5> 
                                 The amount is inclusive of all taxes. Invoices will be shared with you soon. Your usage of FabPik Website/app is governed by our Privacy policy and Terms and Conditions. For any query with respect to your payment, kindly connect with FabPik Support at <a href="{{URL::to('/')}}" style="color:#ef808b;">www.fabpik.in.</a> 

                                </div>
                            </td> 
                                
                               
                            </tr>
                        </table>


                        <!-- <div class="footer" style="background-color:white;margin-top: 3px;">
                            For any help, reach out to us at: 7898675436; Mail Us: wecare@fabpik.in
                            <center>Fabpik</center>
                        </div> -->

                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>

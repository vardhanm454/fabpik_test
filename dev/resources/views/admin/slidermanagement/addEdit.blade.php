@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content') 

<div class="row categpries-wrap">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    <form class="form-horizontal form-row-seperated" action="{{(isset($slider))?route('admin.slidermgmt.edit',['id'=>$id]):route('admin.slidermgmt.create')}}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('title'))?'has-error':''}}">
                                                <label class="control-label">Title:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="title" maxlength="100" placeholder="" value="{{old('title',(isset($slider))?$slider->title:'')}}">
                                                    <span class="help-block">{{$errors->first('title')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('slider_url'))?'has-error':''}}">
                                                <label class="control-label">Slider URL:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="slider_url" maxlength="100" placeholder="" value="{{old('slider_url',(isset($slider))?$slider->slider_url:'')}}">
                                                    <span class="help-block">{{$errors->first('slider_url')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('start_date'))?'has-error':''}}">
                                                <label class="control-label">Start Date:<span class="required"> * </span></label>
                                                <input type="text" class="form-control date-picker" name="start_date" data-date-start-date="0d"
                                                    value="{{old('start_date', isset($slider)?date('d-m-Y H:i',strtotime($slider->start_date)):'')}}"
                                                    readonly>
                                                    <span class="help-block">{{$errors->first('start_date')}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('end_date'))?'has-error':''}}">
                                                <label class="control-label">End Date:<span class="required"> * </span></label>
                                                <input type="text" class="form-control date-picker" name="end_date" data-date-start-date="0d"
                                                    placeholder="" value="{{old('end_date', isset($slider)?date('d-m-Y H:i',strtotime($slider->end_date)):'')}}" readonly>
                                                    <span class="help-block">{{$errors->first('end_date')}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('display_order'))?'has-error':''}}">
                                                <label class="control-label">Display Order:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="number" class="form-control" name="display_order" step="1" value="{{old('display_order',(isset($slider))?$slider->display_order:'')}}">
                                                    <span class="help-block">{{$errors->first('display_order')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Status:<span class="required"> * </span></label>
                                            <div class="">
                                                <select name="status" id="status" class="form-control">
                                                    @foreach([1=>'Active',0=>'Inactive'] as $val=>$label)
                                                    <option value="{{$val}}" {{(old('status', (isset($slider))?$slider->status:'')==$val)?'selected="selected"':''}}>{{$label}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label">View:<span class="required"> * </span></label>
                                                <div class="">
                                                    <select name="view" id="view" class="form-control">
                                                        @foreach([0=>'Desktop',1=>'Mobile'] as $val=>$label)
                                                        <option value="{{$val}}" {{(old('status', (isset($slider))?$slider->view:'')==$val)?'selected="selected"':''}}>{{$label}}</option>
                                                        @endforeach
                                                        <!-- <option value="">Select</option>
                                                        <option value="0" {{request()->status=='1'?'selected="selected"':''}}>Desktop</option>
                                                        <option value="1" {{request()->status=='0'?'selected="selected"':''}}>Mobile</option> -->
                                                    </select>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="{{($errors->first('image'))?'has-error':''}}">
                                                <label class="control-label" for="input-image">Image:<span class="required"> * </span></label>
                                                <div class="">
                                                    <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                                                        <img src="{{(isset($slider))?route('ajax.previewImage',['image'=>$slider->image_path,'type'=>'slider']):url('uploads/no-image.png') }}" alt="" title="" data-placeholder="{{ url('uploads/no-image.png') }}" height="150" width="150"/>
                                                    </a>
                                                    <input type="hidden" name="image" value="{{ (isset($slider))?$slider->image_path:'' }}" id="input-image" />
                                                    <span class="help-block">{{$errors->first('image')}}</span>
                                                </div>
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                            @if(isset($slider))
                            <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
                            @endif
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var SliderAddEdit = function () {

    return {

        //main function to initiate the module
        init: function () {
            if (jQuery().datetimepicker) {
                var start_date = new Date(); 
                
                $('.date-picker').datetimepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true,
                    startDate: start_date,
                    format: 'dd-mm-yyyy hh:ii',
                    locale: 'en'
                });
            }


            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.slidermgmt')}}";
            });


            $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                // alert(url);
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                swal('Category', 'Record Deleted Successfully', "success");
                                location.reload();
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });

            $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
                var $element = $(this);
                // alert($element);
                var $popover = $element.data('bs.popover');

                e.preventDefault();
                $('a[data-toggle="image"]').popover('destroy');
                if ($popover) {
                    return;
                }
                $element.popover({
                    html: true,
                    placement: 'bottom',
                    trigger: 'manual',
                    content: function() {
                        return '<button type="button" id="button-image" class="btn btn-outline btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-outline btn-danger"><i class="fa fa-trash-o"></i></button>';
                    }
                });
                $element.popover('show');

                $('#button-image').on('click', function() {
                    var $button = $(this);
                    var $icon = $button.find('> i');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element
                            .parent().find('input').attr('id') + "&thumb=" + $element
                            .attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-pencil');
                            }
                        },
                        success: function(html) {
                            $('body').append(
                                '<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' +
                                html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });

                $('#button-clear').on('click', function() {
                    $element.find('img').attr('src', $element.find('img').attr(
                        'data-placeholder'));
                    $element.parent().find('input').val('');
                    $element.popover('destroy');
                });
            });
        }
    };
}();

jQuery(document).ready(function() {    
    SliderAddEdit.init();
});

</script>
@endpush
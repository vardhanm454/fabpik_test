@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')

  

<div class="row categpries-wrap">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    @if(isset($slider))
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.slidermgmt.edit',['id'=>$id])}}" method="post"
                        enctype="multipart/form-data">
                    @else
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.slidermgmt.create')}}" method="post"
                        enctype="multipart/form-data">
                    @endif
                        {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        
 
     <div id="signupbox" style=" margin-top:10px" class="mainbox col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-1">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title" style="height: 20px"></div>
                <div style="float:right; font-size: 85%; position: relative; top:-10px"></div>
            </div>  
            <div class="panel-body" >

 
                         <div class="{{($errors->first('title'))?'has-error':''}}">
                                            
                        <div id="div_id_email" class="form-group required">
                            <label for="id_title" class="control-label col-md-4  requiredField">Title<span class="asteriskField">*</span> </label>
                            <div class="controls col-md-6 ">
                            <input type="text" class="form-control maxlength-handler" name="title"maxlength="100" placeholder="" value="{{old('title',isset($slider)?$slider->title:'')}}">
                            <span class="help-block">{{$errors->first('title')}}</span>

                            </div>     
                        </div>
                    </div>
                        
                        <div class="{{($errors->first('slider_url'))?'has-error':''}}">
                                            
                        <div id="div_id_name" class="form-group required"> 
                            <label for="id_name" class="control-label col-md-4  requiredField">Slider Url<span class="asteriskField">*</span> </label> 
                            <div class="controls col-md-6 "> 
<input type="text" class="form-control maxlength-handler" name="slider_url"
    maxlength="100" placeholder="" value="{{old('slider_url',isset($slider)?$slider->slider_url:'')}}">
                                    <span class="help-block">{{$errors->first('slider_url')}}</span>
                    
                            </div>
                        </div>
                    </div>
                    
                     <div class="{{($errors->first('display_order'))?'has-error':''}}">
                                            
                    
                        <div id="div_id_number" class="form-group required">
                             <label for="id_number" class="control-label col-md-4  requiredField">Display Order<span class="asteriskField">*</span> </label>
                             <div class="controls col-md-6">
                                 
                <input type="text" class="form-control maxlength-handler" name="display_order" maxlength="100" placeholder="" value="{{old('display_order', isset($slider)?$slider->display_order:'')}}">
                                 <span class="help-block">{{$errors->first('display_order')}}</span>
                            </div> 
                        </div> 
                    </div>
            
    <div class="{{($errors->first('slider_image'))?'has-error':''}}">
        <div id="div_id_number" class="form-group required">
            <label class="control-label col-md-4" for="input-image">Slider <small>(200x300)</small>:<span class="required"> * </span></label>
        <div class="controls col-md-6">
        <a href="" id="thumb-image-banner" data-toggle="image" class="img-thumbnail">
            
            <img src="{{ (isset($slider))?route('ajax.previewImage',['image'=>$slider->image_path,'type'=>'slider']):url('uploads/no-image.png') }}"alt="" title="" data-placeholder="{{ url('uploads/no-image.png') }}"width="100" height="100" />
  
        </a>

<input type="hidden" name="slider_image" value="{{isset($slider)?$slider->image_path:'' }}" id="input-image" />
        <span class="help-block">{{$errors->first('slider_image')}}</span>
        </div>
        </div>
        </div>



@if(isset($slider))
        <div class="{{($errors->first('slider_image'))?'has-error':''}}">
        <div id="div_id_number" class="form-group required">
            <label class="control-label col-md-4" for="input-image">Status:
            <span class="required"> * </span></label>
    
        <div class="controls col-md-6">
        <select class="form-control input-sm" name="status">
            @foreach(['1'=>'Active','0'=>'Inactive'] as $val=>$label)
            <option value="{{$val}}" @if(old('status', isset($slider)?$slider->status:1)==$val) selected @endif>
                {{$label}}</option>
                @endforeach
                </select>
                <span class="help-block">{{$errors->first('status')}}</span>
        </div>
        </div>
        </div>
        @endif
                
</div>
</div>
</div>                                 
</div>

</div>


            <div class="col-md-2">
            <div class="row">


            </div>
            </div>
                                
            </div>
            </div>
            <div class="form-actions text-right">
            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>

            @if(isset($slider))
            <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
            @endif
            </div>
            </form>


                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">

$('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.slidermgmt')}}";
            });


$('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
               // alert(url);
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                swal('Category', 'Record Deleted Successfully', "success");
                                location.reload();
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });

$(document).on('click', 'a[data-toggle=\'image\']', function(e) {
                var $element = $(this);
               // alert($element);
                var $popover = $element.data('bs.popover');

                e.preventDefault();
                $('a[data-toggle="image"]').popover('destroy');
                if ($popover) {
                    return;
                }
                $element.popover({
                    html: true,
                    placement: 'bottom',
                    trigger: 'manual',
                    content: function() {
                        return '<button type="button" id="button-image" class="btn btn-outline btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-outline btn-danger"><i class="fa fa-trash-o"></i></button>';
                    }
                });
                $element.popover('show');

                $('#button-image').on('click', function() {
                    var $button = $(this);
                    var $icon = $button.find('> i');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element
                            .parent().find('input').attr('id') + "&thumb=" + $element
                            .attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-pencil');
                            }
                        },
                        success: function(html) {
                            $('body').append(
                                '<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' +
                                html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });

                $('#button-clear').on('click', function() {
                    $element.find('img').attr('src', $element.find('img').attr(
                        'data-placeholder'));
                    $element.parent().find('input').val('');
                    $element.popover('destroy');
                });
            });

</script>
@endpush
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->

<head>
    <meta charset="utf-8" />
    <title>Sliders</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">
                <table class="tabletable-bordered">
                    <thead>
                        <tr style="background:#c2c2c2;">
                            <th> # </th>
                            <th> Title </th>
                            <th> Url Path </th>
                            <th> Display Order</th>
                            <th> Start Date</th>
                            <th> End Date</th>
                            <th> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($sliders))
                        @foreach($sliders as $index => $slider)
                        <tr>
                            <td>{{++$index}}</td>
                            <td>{{$slider->title}}</td>
                            <td>{{$slider->slider_url}}</td>
                            <td> {{$slider->display_order}}</td>
                            <td> {{date('d-m-Y H:i', strtotime($slider->start_date))}}</td>
                            <td> {{date('d-m-Y H:i', strtotime($slider->end_date))}}</td>
                            <td><?php echo ($slider->status == 1)?'Active':'Inactive'; ?></td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>
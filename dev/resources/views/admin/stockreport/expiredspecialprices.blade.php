@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                    <form action="#" method="get" class="filter-form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Special Price End Date Range:</strong> </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm" name="fpricefrom"
                                                id="fpricefrom" placeholder="Min" value="{{request()->fpricefrom}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control input-sm" name="fpriceto"
                                                id="fpriceto" placeholder="Max" value="{{request()->fpriceto}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>ID:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fid" id="fid"
                                            value="{{request()->fid}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Name:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fname" id="fname"
                                            value="{{request()->fname}}" autocomplete="off">
                                    </div>
                                </div>
                            </div>     
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Action:</strong> </label>
                                    <div class="col-md-9">                                    
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('admin.stockreport.expiredspecialprices')}}" title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span></a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                    <a href="javascript:;" id="btn_table_export" class="btn btn-sm btn-outline btn-default text-warning tooltips"
                            data-container="body" data-placement="top" data-original-title="Export to Excel"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a>
                    
                    </div>
                    <table class="table dt-responsive nowrap product-list-table text-center"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="4%" class="">&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="10%" class="text-center"> ID </th>
                                <th width="17%" class="text-center"> Name </th>
                                <th width="10%" class="text-center"> Image </th>
                                <th width="13%" class="none"> Category </th>
                                <th class="none"> SKU </th>
                                <th width="7%" class="text-center"> MRP </th>
                                <th width="10%" class="text-center"> Sale/<br>Selling Price </th>
                                <th width="10%" class="text-center"> Seller <br>Discount (INR) </th>
                                <th width="10%" class="text-center"> Fabpik <br>Selling Price </th>
                                <th width="10%" class="text-center"> Fabpik <br>Discount (INR) </th>
                                <th width="20%" class="text-center"> Fabpik <br>Discount (%) </th>
                                <th width="20%" class="none"> Special Price Start Date </th>
                                <th width="20%" class="none"> Special Price End Date </th>
                                <th width="" class="none"> Primary Variant: </th>
                                <th width="" class="none"> Secondary Variant: </th>
                                <th width="" class="none"> Created at: </th>
                                <th width="" class="none"> Modified at: </th>
                                <th width="10%" class="text-center"> Status </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="changePriceModal" tabindex="-1" role="dialog" aria-labelledby="changePriceModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changePriceModalLabel">Change Prices</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="">
                    <label class="control-label">Discount <span class="required"> * </span></label>
                    <form>
                        <div class="form-group">
                            <select class="form-control" name="discount_type" id="discount_type">
                                <option value="">Select</option>
                                @foreach(['f'=>'Fixed', 'p'=>'Percentage'] as $val=>$label)
                                <option value="{{$val}}"> {{$label}} </option>
                                @endforeach
                            </select>
                            <span style="color:#e73d4a" id="discount_type_result"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Amount (On MRP) <span class="required"> * </span></label>
                            <input type="text" class="form-control" name="discount_amount" id="discount_amount" autocomplete="off">
                            <span style="color:#e73d4a" id="discount_amount_result"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Start Date:<span class="required"> * </span></label>
                            <input type="text" class="form-control special_date_picker" name="start_date" id="start_date" data-date-start-date="0d" readonly>
                            <span style="color:#e73d4a" id="start_date_result"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">End Date:<span class="required"> * </span></label>
                            <input type="text" class="form-control special_date_picker" name="end_date" id="end_date" data-date-start-date="0d" readonly>
                            <span style="color:#e73d4a" id="end_date_result"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var SpecialPrices = function() {
    
    $('#changePriceModal').on('hidden.bs.modal', function () {
        $('#changePriceModal form')[0].reset();
    });
    
    return {

        //main function to initiate the module
        init: function() {

            $('#fcategory').select2({ 
                allowClear:true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.childcategories.autocomplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (categoryArr) {
                    return categoryArr.text;
                },
            });

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }
            $('.special_date_picker').datetimepicker({
                    todayHighlight:true,
                    todayBtn:true,
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true,
                    format: 'dd-mm-yyyy hh:ii',
                    locale: 'en'
                }).datetimepicker("setDate","0");


            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.stockreport.expiredspecialprices')}}?search=1";

                if ($('#fname').val() != '') url += "&fname=" + encodeURIComponent($('#fname').val());
                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fstatus').val() != '') url += "&fstatus=" + encodeURIComponent($('#fstatus').val());
                if ($('#fpricefrom').val() != '') url += "&fpricefrom=" + encodeURIComponent($('#fpricefrom').val());
                if ($('#fpriceto').val() != '') url += "&fpriceto=" + encodeURIComponent($('#fpriceto').val());
                // if ($('#ffeatured').val() != '') url += "&ffeatured=" + encodeURIComponent($('#ffeatured').val());
                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#spsfromdate').val() != '') url += "&spsfromdate=" + encodeURIComponent($('#spsfromdate').val());
                if ($('#spstodate').val() != '') url += "&spstodate=" + encodeURIComponent($('#spstodate').val());
                if ($('#spefromdate').val() != '') url += "&spefromdate=" + encodeURIComponent($('#spefromdate').val());
                if ($('#spetodate').val() != '') url += "&spetodate=" + encodeURIComponent($('#spetodate').val());
                if ($('#fcategory').val()) url += "&fcategory=" + encodeURIComponent($('#fcategory').val());
                if ($('#fsku').val()) url += "&fsku=" + encodeURIComponent($('#fsku').val());
                if ($('#funiqueid').val()) url += "&funiqueid=" + encodeURIComponent($('#funiqueid').val());
                if ($('#fcommissiontype').val()) url += "&fcommissiontype=" + encodeURIComponent($('#fcommissiontype').val());
                if ($('#fbrand').val()) url += "&fbrand=" + encodeURIComponent($('#fbrand').val());
                if ($('#fcompanyname').val()) url += "&fcompanyname=" + encodeURIComponent($('#fcompanyname').val());
                if ($('#fsfeatured').val()) url += "&fsfeatured=" + encodeURIComponent($('#fsfeatured').val());
                if ($('#ffeatured').val()) url += "&ffeatured=" + encodeURIComponent($('#ffeatured').val());
                if ($('#fdiscount').val()) url += "&fdiscount=" + encodeURIComponent($('#fdiscount').val());
                if ($('#ffdiscount').val()) url += "&ffdiscount=" + encodeURIComponent($('#ffdiscount').val());

                window.location.href = url;
            });
        }

    };

}();

jQuery(document).ready(function() {
    SpecialPrices.init();
});
</script>
@endpush
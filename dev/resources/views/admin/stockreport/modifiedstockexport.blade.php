<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title>Modified Stock Report</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">                        
                <table class="tabletable-bordered">
                    <thead>
                        <tr style="background:#c2c2c2;">
                            <th > Variant Id </th>
                            <th > Parent Id </th>
                            <th > SKU</th>
                            <th > Product Name </th>
                            <th > Seller Name </th>
                            <th > Seller Id </th>
                            <th > Brand </th>
                            <th > Last Modified </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($prodVariants))
                        @foreach($prodVariants as $prodVariant)
                        <tr>
                            <td>{{$prodVariant->pv_unique_id}}</td>
                            <td>{{$prodVariant->p_unique_id}}</td>
                            <td>{{$prodVariant->sku}}</td>
                            <td>{{$prodVariant->name}}</td>
                            <td>{{$prodVariant->s_name}}</td>
                            <td>{{$prodVariant->seller_code}}</td>
                            <td>{{$prodVariant->brand_name}}</td>
                            <td>{{date('m/d/Y',strtotime($prodVariant->updated_at))}}</td>

                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>

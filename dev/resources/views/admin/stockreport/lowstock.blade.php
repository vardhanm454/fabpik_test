@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
    
    <style>
    .display-none{
        display:none;
    }
    </style>
@endpush

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                    <form action="#" method="get" class="filter-form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Variant Id:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fid" id="fid"
                                            value="{{request()->fid}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Parent Id:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fpid" id="fpid"
                                            value="{{request()->fpid}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>SKU:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fsku" id="fsku"
                                            value="{{request()->fsku}}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Product Name:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="pname" id="pname"
                                            value="{{request()->pname}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Seller:</strong> </label>
                                    <div class="col-md-9">
                                        <select name="fseller" id="fseller"
                                            class="form-control form-filter input-sm select2">
                                            <option value="">Select Seller</option>
                                            @if(isset($sellers))
                                            @foreach($sellers as $seller)
                                            <option value="{{$seller->id}}"
                                            {{request()->fseller==$seller->id?'selected="selected"':''}}>
                                                {{$seller->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>  
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Brand:</strong> </label>
                                    <div class="col-md-9">
                                        <select name="fbrand" id="fbrand"
                                            class="form-control form-filter input-sm select2">
                                            <option value="">Select Brand</option>
                                            @if(isset($brands))
                                            @foreach($brands as $brand)
                                            <option value="{{$brand->id}}"
                                            {{request()->fbrand==$brand->id?'selected="selected"':''}}>
                                                {{$brand->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>      
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Category:</strong> </label>
                                    <div class="col-md-9">
                                        <select class="form-control input-sm" name="fcategory" id="fcategory">
                                        @if(request()->fcategory)
                                        @php $categories = \DB::table('view_categories')->where('category_id', request()->fcategory)->first(); @endphp
                                       
                                        <option value="{{$categories->category_id}}" selected="selected">{{$categories->category_title}}</option>
                                        @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Action:</strong> </label>
                                    <div class="col-md-9">                                    
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('admin.stockreport.lowstock')}}" title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                       <a href="javascript:;" id="btn_table_export" class="btn btn-sm btn-outline btn-default text-warning tooltips"
                            data-container="body" data-placement="top" data-original-title="Export to Excel"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a>
                    </div>
                    <table class="table dt-responsive nowrap product-list-table text-center"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="10%" class="text-center"> Variant Id </th>
                                <th width="10%" class="text-center"> Parent Id </th>
                                <th width="10%" class="text-center"> SKU</th>
                                <th width="18%" class="text-center"> Product Name </th>
                                <th width="10%" class="text-center"> Stock</th>
                                <th width="18%" class="text-center"> Seller</th>
                                <th width="17%" class="text-center"> Category</th>
                                <th width="17%" class="text-center"> Brand</th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var SpecialPrices = function() {
    
    $('#changePriceModal').on('hidden.bs.modal', function () {
        $('#changePriceModal form')[0].reset();
    });
    
    return {

        //main function to initiate the module
        init: function() {

            $('#fcategory').select2({ 
                allowClear:true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.categoryAutoComplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (categoryArr) {
                    return categoryArr.text;
                },
            });

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }
            $('.special_date_picker').datetimepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true,
                    format: 'dd-mm-yyyy hh:ii:ss',
                    locale: 'en'
                });

                // Export all Stock data
                $('#btn_table_export').on('click', function(e) {
                e.preventDefault();

                var url = "{{route('admin.stockreport.lowstockexport')}}?export=1";

                if ($('#fid').val() != '') url += "&fid=" + encodeURIComponent($('#fid').val());
                if ($('#fpid').val() != '') url += "&fpid=" + encodeURIComponent($('#fpid').val());
                if ($('#fsku').val() != '') url += "&fsku=" + encodeURIComponent($('#fsku').val());
                if ($('#pname').val() != '') url += "&pname=" + encodeURIComponent($('#pname').val());
                if ($('#fseller').val()) url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fcategory').val()) url += "&fcategory=" + encodeURIComponent($('#fcategory').val());
                if ($('#fbrand').val()) url += "&fbrand=" + encodeURIComponent($('#fbrand').val());

                window.location.href = url;
            });

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.stockreport.lowstock')}}?search=1";

                if ($('#fid').val() != '') url += "&fid=" + encodeURIComponent($('#fid').val());
                if ($('#fpid').val() != '') url += "&fpid=" + encodeURIComponent($('#fpid').val());
                if ($('#fsku').val() != '') url += "&fsku=" + encodeURIComponent($('#fsku').val());
                if ($('#pname').val() != '') url += "&pname=" + encodeURIComponent($('#pname').val());
                if ($('#fseller').val()) url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fcategory').val()) url += "&fcategory=" + encodeURIComponent($('#fcategory').val());
                if ($('#fbrand').val()) url += "&fbrand=" + encodeURIComponent($('#fbrand').val());
                
                window.location.href = url;
            });
        }

    };

}();

jQuery(document).ready(function() {
    SpecialPrices.init();
});
</script>
@endpush
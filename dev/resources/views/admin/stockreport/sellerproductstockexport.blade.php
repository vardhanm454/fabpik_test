<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title>Seller Products Report</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">                        
                <table class="tabletable-bordered">
                    <thead>
                        <tr style="background:#c2c2c2;">
                            <th > Seller Id </th>
                            <th > Seller Name </th>
                            <th > Parent Product Count </th>
                            <th > Variant Product Count </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($prodVariants))
                        @foreach($prodVariants as $prodVariant)

                        @php 
                        $productCount = \DB::table('products')->where('seller_id',$prodVariant->id)->whereNull('deleted_at')->count();
                        $VariantproductCount = \DB::table('product_variants as t1')->join('products as t2','t2.id','t1.product_id')->whereNull('t1.deleted_at')->where('t2.seller_id',$prodVariant->id)->count();
                        @endphp
                        <tr>    
                            <td>{{$prodVariant->seller_code}}</td>
                            <td>{{$prodVariant->name}}</td>
                            <td>{{$productCount}}</td>
                            <td>{{$VariantproductCount}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>

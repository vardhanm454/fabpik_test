<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title>Low Stock Report</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">                        
                <table class="tabletable-bordered">
                    <thead>
                        <tr style="background:#c2c2c2;">
                                <th> ID </th>
                                <th> Name </th>
                                <th> Image </th>
                                <th> Category </th>
                                <th> SKU </th>
                                <th> MRP </th>
                                <th> Sale/<br>Selling Price </th>
                                <th> Seller <br>Discount (INR) </th>
                                <th> Fabpik <br>Selling Price </th>
                                <th> Fabpik <br>Discount (INR) </th>
                                <th> Fabpik <br>Discount (%) </th>
                                <th> Special Price Start Date </th>
                                <th> Special Price End Date </th>
                                <th> Primary Variant: </th>
                                <th> Secondary Variant: </th>
                                <th> Created at: </th>
                                <th> Modified at: </th>
                                <th> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($prodVariants))
                        @foreach($prodVariants as $prodVariant)
                        <tr>
                            <td>{{$prodVariant->pv_unique_id}}</td>
                            <td>{{$prodVariant->p_unique_id}}</td>
                            <td>{{$prodVariant->sku}}</td>
                            <td>{{$prodVariant->name}}</td>
                            <td>{{$prodVariant->stock}}</td>
                            <td>{{$prodVariant->s_name}}</td>
                            <td>{{$prodVariant->seller_code}}</td>
                            <td>{{$prodVariant->c_name}}</td>
                            <td>{{$prodVariant->brand_name}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>

@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content')
<style>
.modal {
    text-align: center;
    padding: 0 !important;
}

.modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
}

.modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
}

.modal-body {
    position: relative;
    overflow-y: auto;
    max-height: 400px;
    padding: 15px;
}
</style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                       <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class=""><strong>Date:</strong> </label><br>
                                    <div class="">
                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control datepicker input-sm" name="ffromdate" id="ffromdate"
                                                placeholder="From" value="{{request()->ffromdate}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control input-sm" name="ftodate" id="ftodate" placeholder="To" value="{{request()->ftodate}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class=""><strong>Order Status:</strong> </label><br>
                                    <div class="">
                                        <select name="fostatus" id="fostatus"
                                            class="form-control select2 form-filter input-sm margin-bottom-5" data-allow-clear="true">
                                            <option value="">Select</option>
                                            @if(isset($orderStatus))
                                            @foreach($orderStatus as $status)
                                            <option value="{{$status->id}}"
                                            {{request()->fostatus==$status->id?'selected="selected"':''}}>
                                                {{$status->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class=""><strong>Seller:</strong> </label><br>
                                    <div class="">
                                           <select name="fseller" id="fseller"
                                            class="form-control select2 form-filter input-sm margin-bottom-5" data-allow-clear="true">
                                            <option value="">Select</option>
                                            @if(isset($sellers))
                                            @foreach($sellers as $seller)
                                            <option value="{{$seller->id}}" {{request()->fseller==$seller->id?'selected="selected"':''}}>
                                            {{$seller->company_name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class=""><strong>Shipping Model:</strong> </label><br>
                                    <div class="">
                                        <select name="fmodel" id="fmodel" class="form-control form-filter input-sm margin-bottom-5 select2" data-allow-clear="true">
                                            <option value="">Select</option>

                                            @foreach(['f'=>'FabShip', 's'=>'Self'] as $val=>$label)
                                            <option value="{{$val}}"
                                            {{request()->fmodel==$val?'selected="selected"':''}}>
                                                {{$label}}
                                            </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class=""><strong>Parent Order ID:</strong> </label><br>
                                    <div class="">
                                        <input type="text" name="fparentid" id="fparentid" value="{{request()->fparentid}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class=""><strong>Child Order ID:</strong> </label><br>
                                    <div class="">
                                        <input type="text" name="fchildid" id="fchildid" value="{{request()->fchildid}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class=""><strong>Customer Name:</strong> </label><br>
                                    <div class="">
                                        <input type="text" name="fcustomername" id="fcustomername" value="{{request()->fcustomername}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class=""><strong>Customer Mobile Number:</strong> </label><br>
                                    <div class="">
                                        <input type="text" name="fcustomermobile" id="fcustomermobile" value="{{request()->fcustomermobile}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class=""><strong>Product SKU:</strong> </label><br>
                                    <div class="">
                                        <input type="text" name="fproductsku" id="fproductsku" value="{{request()->fproductsku}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <label class=""><strong>Product Name:</strong> </label><br>
                                    <div class="">
                                        <input type="text" name="fproductname" id="fproductname" value="{{request()->fproductname}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>                                    
                                </div>
                                
                                <div class="col-md-4">
                                </div>

                                <div class="col-md-4">
                                    <label class=""><strong>Action:</strong> </label><br>
                                    <div class="">
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('admin.childorders')}}" title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                    <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-placement="top" data-original-title="Change Order Status" data-toggle="tooltip" data-action="change-status">
                            <strong><span aria-hidden="true" id="btn-confirm" class="text-danger"></span>&nbsp;Change Status</strong></a>

                            <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Refund Initiate" data-action="refund-initiate"><strong><span aria-hidden="true" class="text-danger"></span>&nbsp;Refund Initiate</strong></a>

                            <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Refund Done" data-action="change-payment-status"><strong><span aria-hidden="true" class="text-danger"></span>&nbsp;Refund Done</strong></a>

                            <!-- <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Modify Address" data-action="modify-address"><strong><span aria-hidden="true" class="fa fa-address-card text-primary"></span>&nbsp;Modify Address</strong></a> -->

                            <!-- Button trigger modal -->
                            <!-- <button type="button" class="btn btn-default btn-sm"  data-toggle="modal" data-action="modify-address" data-target="#modifyAddressModal"><strong><span aria-hidden="true" class="fa fa-address-card text-primary"></span>&nbsp;Modify Address</strong>
                            </button> -->

                            <!-- Button trigger modal -->
                            <!-- <button type="button" class="btn btn-default btn-sm"  data-toggle="modal" data-action="cancel-order" data-target="#cancelOrderModal"><strong><span aria-hidden="true" class="fa fa-address-card text-primary"></span>&nbsp;Cancel Order</strong>
                            </button> -->


                    </div>
                    <table class="table dt-responsive table-checkable nowrap"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="100" class="text-center"> Date </th>
                                <th width="130" class="text-center"> Parent Order ID </th>
                                <th width="130" class="text-center"> Child Order ID </th>
                                <th width="100" class="none"> Seller ID: </th>
                                <th width="100" class="none"> Seller Name: </th>
                                <th width="100" class="none"> Customer Name: </th>
                                <th width="100" class="none"> Mobile Number: </th>
                                <th width="130" class="none"> Product Name </th>
                                <th width="100" class="none"> SKU: </th>
                                <th width="100" class="text-center"> Quantity </th>
                                <th width="100" class="text-center"> Final Amount </th>
                                <th width="100" class="text-center"> Order Status </th>
                                <th width="130" class="none"> Payment Status: </th>
                                <th width="130" class="none"> shipping Status: </th>
                                <th width="130" class="none"> User Custimization Text: </th>
                                <th width="130" class="text-center"> Actions </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="changeStatusModal" tabindex="-1" role="dialog" aria-labelledby="changeStatusModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changeStatusModalLabel">Change Order Status!
                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </h5>

            </div>
            <div class="modal-body">
                <div class="">
                    <label class="control-label">Status</label>
                    <div class="">
                        <select class="form-control" name="status" id="status">
                            <option value="">Select</option>
                            @foreach(['6'=>'Return Approved by Admin' , '7'=>'Rejection Approved by Admin', '10'=>'Generate AWB', '8'=>'Delivered', '9'=>'Completed'] as $val=>$label)
                            <option value="{{$val}}"> {{$label}} </option>
                            @endforeach
                        </select>
                        <span style="color:#e73d4a" id="result"></span>
                    </div>
                </div>
                <div class="">
                    <label class="control-label">Remark</label>
                    <div class="">
                        <textarea class="form-control" name="remark" id="remark" rows="3"></textarea>
                        <span style="color:#e73d4a" id="remark-result"></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>


<!-- Change Address Modal -->
<div class="modal fade" id="modifyAddressModal" tabindex="-1" role="dialog" aria-labelledby="modifyAddressModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modifyAddressModalLabel"><strong>Modify Order Delivery Address</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form>
        <div class="modal-body">
        

            <input type="hidden" class="form-control" name="shipping_address_hidden" id="shipping_address_hidden">
            <input type="hidden" class="form-control" name="shipping_address_2_hidden" id="shipping_address_2_hidden">
            <input type="hidden" class="form-control" name="shipping_pincode_hidden" id="shipping_pincode_hidden">
            <input type="hidden" class="form-control" name="shipping_city_hidden" id="shipping_city_hidden">
            <input type="hidden" class="form-control" name="shipping_state_id_hidden" id="shipping_state_id_hidden">
            <input type="hidden" class="form-control" name="shipping_state_hidden" id="shipping_state_hidden">

            <input type="hidden" class="form-control" name="shipping_order_id" id="shipping_order_id">
            <input type="hidden" class="form-control" name="current_order_status" id="current_order_status">
            <input type="hidden" class="form-control" name="child_order_id" id="child_order_id">
            
            <!-- <input type="hidden" class="form-control" name="useremail" id="useremail">
            <input type="hidden" class="form-control" name="usermobile" id="usermobile"> -->

            <input type="hidden" class="form-control" name="otpcheck" id="otpcheck">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="shipping-name" class="col-form-label">Name<span class="required"> *</span></label>
                        <input type="text" class="form-control" id="shipping_name" readonly>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="shipping-mobile" class="col-form-label">Mobile<span class="required"> *</span></label>
                        <input type="text" class="form-control"  id="shipping_mobile" readonly>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="shipping_email" class="col-form-label">Email<span class="required"> *</span></label>
                        <input type="text" class="form-control"  id="shipping_email" readonly>
                    </div>
                </div>
            </div>
        
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="shipping-address" class="col-form-label">Address <span class="required"> *</span></label>
                        <textarea class="form-control" name="shipping_address" id="shipping_address"></textarea>
                        <span style="color:#e73d4a" id="shipping_address_result"></span>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="shipping-address" class="col-form-label">Address 2</label>
                        <input type="text" class="form-control" name="shipping_address_2" id="shipping_address_2">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="shipping-pincode" class="col-form-label">Pincode<span class="required"> *</span></label>
                        <input type="text" class="form-control" name="shipping_pincode" id="shipping_pincode">
                        <span style="color:#e73d4a" id="shipping_pincode_result"></span>

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="shipping-city" class="col-form-label">City<span class="required"> *</span></label>
                        <input type="text" class="form-control" name="shipping_city" id="shipping_city">
                        <span style="color:#e73d4a" id="shipping_city_result"></span>


                    </div>
                </div>
            
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="shipping-state" class="col-form-label">State<span class="required"> *</span></label>
                        <select class="form-control" name="shipping_state_id" id="shipping_state_id">
                            <option value="">Select</option>
                            @php $states = \App\Models\State::get(); @endphp
                            @foreach($states as $state)
                                <option value="{{$state->id}}">{{$state->name}}</option>
                            @endforeach
                        </select>
                        <span style="color:#e73d4a" id="shipping_state_id_result"></span>

                    </div>
                </div>
            </div>

            <div class="row" id="verifyblock" style="display:none">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="verify-otp" class="col-form-label">Enter OTP<span class="required"> *</span></label>
                        <input type="text" class="form-control" id="verify_otp" name="verify_otp">
                        <a href="#" class="send_otp">Resend OTP</a>
                    </div>
                </div>
            </div>

            <span style="color:#e73d4a" id="address_change_result"></span>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary send_otp" id="send_otp" name="send_otp">Send OTP</button>
            <button type="button" class="btn btn-primary" id="verifyfinish" name="verifyfinish" style="display:none">Verify & Finish</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Address Change Modal -->
<!-- Cancel Order Modal -->
<div class="modal fade" id="cancelOrderModal" tabindex="-1" role="dialog" aria-labelledby="cancelOrderModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">    
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cancelOrderModalLabel"><strong>Cancel Order</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <input type="hidden" class="form-control" name="cancel_order_id" id="cancel_order_id">
        <input type="hidden" class="form-control" name="cancel_child_order_id" id="cancel_child_order_id">
        <input type="hidden" class="form-control" name="cancel_usermobile" id="cancel_usermobile">
        <input type="hidden" class="form-control" name="cancel_useremail" id="cancel_useremail">
        <input type="hidden" class="form-control" name="cancel_check_verify_otp" id="cancel_check_verify_otp">
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="cancel_reason" class="col-form-label">Reason <span class="required"> *</span></label>
                    <textarea type="text" class="form-control" id="order_cancel_reason" name="order_cancel_reason"></textarea>
                    <span style="color:#e73d4a" id="order_cancel_reason_result"></span>                   
                </div>
            </div>
        </div>

        <div class="row" id="cancel_verify_block" style="display:none" >
            <div class="col-md-6">
                <div class="form-group">
                    <label for="verify-otp" class="col-form-label">Enter OTP<span class="required"> *</span></label>
                    <input type="text" class="form-control" id="cancel_verify_otp" name="cancel_verify_otp">
                    <a href="#" class="cancel_send_otp">Resend OTP</a>
                </div>
            </div>
        </div>
        <span style="color:#e73d4a" id="cancel_order_result"></span>        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary cancel_send_otp" id="cancel_send_otp">Send OTP</button>
        <button type="button" class="btn btn-primary cancel_verify_otp_btn" id="cancel_verify_otp_btn" style="display:none">Verify & Finish</button>
      </div>
    </div>
  </div>
</div>
<!-- Cancel Order Modal -->
<!-- Refund Initiate Modal -->
<div class="modal fade" id="refundInitiateModal" tabindex="-1" role="dialog" aria-labelledby="refundInitiateModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">    
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="refundInitiateModalLabel"><strong>Refund Initiate</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <input type="hidden" class="form-control" name="refund_order_id" id="refund_order_id">
        <input type="hidden" class="form-control" name="refund_child_order_id" id="refund_child_order_id">
        <input type="hidden" class="form-control" name="refund_usermobile" id="refund_usermobile">
        <input type="hidden" class="form-control" name="refund_useremail" id="refund_useremail">
        <input type="hidden" class="form-control" name="refund_payment_type" id="refund_payment_type">

        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="refund_initiate_reason" class="col-form-label">Reason <span class="required"> *</span></label>
                    <textarea type="text" class="form-control" id="refund_initiate_reason" name="refund_initiate_reason"></textarea>
                    <span style="color:#e73d4a" id="refund_initiate_reason_result"></span>                   
                </div>
            </div>
        </div>   
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary finish_btn" id="finish_btn">Submit</button>
      </div>
    </div>
  </div>
</div>
<!-- Cancel Order Modal -->

<!-- Modify Commission -->
<div class="modal fade" id="commissionDetailsModal" tabindex="-1" role="dialog" aria-labelledby="commissionDetailsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">    
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="commissionDetailsModalLabel"><strong>Modify Commission</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <input type="hidden" id="commission_child_order_id" name="commission_child_order_id">
      
      <div class="row">           
            <div class="col-md-6">
                <div class="form-group">
                    <Strong> MRP (INR) - </Strong> <span id="order_mrp"></span> 
                </div>
                <div class="form-group">
                    <Strong> Price (Per Unit) - </Strong> <span id="order_price"></span> 
                </div>
                <div class="form-group">
                    <Strong> Quantity - </Strong> <span id="order_quantity"></span> 
                </div>
                <div class="form-group">
                    <Strong> L*B*H - </Strong> <span id="order_volume"></span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <Strong> Commission Model - </Strong> <a href="#" target="_blank" id="commission_link"> <span id="commission_model_name"></span> </a> 
                </div>
                <div class="form-group">
                    <Strong> Commission Type - </Strong> <span id="commission_model_type"></span>
                </div>
                <div class="form-group">
                    <Strong> Shipping Model - </Strong> <span id="order_shipping_model"></span>
                </div>
                <div class="form-group">
                    <Strong> Weight (In Grams) - </Strong> <span id="order_weight"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="commission_value" class="col-form-label">Commission (INR): <span class="required"> *</span></label>
                    <input type="text" class="form-control input-sm" id="commission_value" name="commission_value" autocomplete="off" readonly>
                    <!-- <span style="" id="commission_value_gst"> <i> <small>Commission GST( SGST + CGST ): 18/-</small> </i> </span>                   -->
                    <span style="color:#e73d4a" id="commission_value_result"></span>                   
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="shipping_charges" class="col-form-label">Shipping Charges (INR): <span class="required"> *</span></label>
                    <input type="text" class="form-control input-sm" id="shipping_charges" name="shipping_charges" autocomplete="off" readonly>
                    <!-- <span style="" id="shipping_charge_gst"> <i> <small>Shipping Charge GST( SGST + CGST ): 18/-</small> </i> </span>      -->
                    <span style="color:#e73d4a" id="shipping_charges_result"></span>                   
                </div>
            </div>
        </div>  

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="handling_charges" class="col-form-label">Handling Charges (INR): <span class="required"> *</span></label>
                    <input type="text" class="form-control input-sm" id="handling_charges" name="handling_charges" autocomplete="off" readonly>
                    <!-- <span style="" id="handling_charge_gst"> <i> <small>Handling Charge GST( SGST + CGST ): 18/-</small> </i> </span>      -->
                    <span style="color:#e73d4a" id="handling_charges_result"></span>
                </div>
            </div>
        </div>  

        <div class="row">
            <div class="col-md-12">
                <label class="control-label">Comments <span class="required"> *</span></label>
                <div class="">
                    <textarea class="form-control" name="comments" id="comments" rows="3"></textarea>
                    <span style="color:#e73d4a" id="comments_result"></span>
                </div>
            </div>
        </div>
<br>

        <span style="color:#295633" id="modify_commission_result_success"></span>  
        <span style="color:#e73d4a" id="modify_commission_result"></span>  
        <br>
        <button type="button" class="btn blue" name="editCommissionDetails" id="editCommissionDetails">Edit Commission Details</button>  
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary commission_btn" id="commission_btn" disabled>Submit</button>
      </div>
    </div>
  </div>
</div>
<!-- Modify Commission -->

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Orders = function() {

    return {

        //main function to initiate the module
        init: function() {

            //Filters will apply when click the enter
            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

            /**
             * For the Address related Modal
             */

            //get the current shipping address of the order
            $("#modifyAddressModal").on("show.bs.modal", function(e) {
                
                //load the  current values 
                $("#shipping_name").val($(e.relatedTarget).data('shipping_name'));
                $("#shipping_mobile").val($(e.relatedTarget).data('shipping_mobile'));
                $("#shipping_email").val($(e.relatedTarget).data('shipping_email'));
                $("#shipping_address").val($(e.relatedTarget).data('shippingaddressone'));
                $("#shipping_address_2").val($(e.relatedTarget).data('shippingaddresstwo'));                
                $("#shipping_city").val($(e.relatedTarget).data('shippingcity'));
                $("#shipping_state").val($(e.relatedTarget).data('shippingstate'));
                $("#shipping_state_id").val($(e.relatedTarget).data('shippingstateid'));
                $("#shipping_pincode").val($(e.relatedTarget).data('shippingpincode'));
                $("#shipping_order_id").val($(e.relatedTarget).data('order_id'));
                $("#shipping_mobile").val($(e.relatedTarget).data('shipping_mobile'));
                $("#shipping_email").val($(e.relatedTarget).data('shipping_email'));
                $("#current_order_status").val($(e.relatedTarget).data('current_order_status'));
                $("#child_order_id").val($(e.relatedTarget).data('child_order_id'));
                
                //load previous values for checking with new values
                $("#shipping_address_hidden").val($(e.relatedTarget).data('shippingaddressone'));
                $("#shipping_address_2_hidden").val($(e.relatedTarget).data('shippingaddresstwo'));                
                $("#shipping_city_hidden").val($(e.relatedTarget).data('shippingcity'));
                $("#shipping_state_hidden").val($(e.relatedTarget).data('shippingstate'));
                $("#shipping_state_id_hidden").val($(e.relatedTarget).data('shippingstateid'));
                $("#shipping_pincode_hidden").val($(e.relatedTarget).data('shippingpincode'));
            });

            //send the OTP and Email to user when the address is changed
            $('.send_otp').on('click',function(){
                // alert('c');
                //compare the new and existing Address
                var shipping_address = $('#shipping_address').val();
                var shipping_address_2 = $('#shipping_address_2').val();
                var shipping_city = $('#shipping_city').val();
                var shipping_state_id = $('#shipping_state_id').val();
                var shipping_state = $( "#shipping_state_id option:selected" ).text();
                var shipping_pincode = $('#shipping_pincode').val();
                var usermobile = $('#shipping_mobile').val();
                var useremail = $('#shipping_email').val();

                if(shipping_address.trim() == '' ){
                    $('#shipping_address').focus();
                    $("#shipping_address_result").html("Address filed is required.");
                    return false;
                }
                if(shipping_pincode.trim() == '' ){
                    $('#shipping_pincode').focus();
                    $("#shipping_pincode_result").html("Pincode filed is required.");
                    return false;
                }
                if(shipping_city.trim() == '' ){
                    $('#shipping_city').focus();
                    $("#shipping_city_result").html("City filed is required.");
                    return false;
                }
                if(shipping_state_id.trim() == '' ){
                    $('#shipping_state_id').focus();
                    $("#shipping_state_id_result").html("State filed is required.");
                    return false;
                }

                if(shipping_address === $("#shipping_address_hidden").val() && 
                   shipping_address_2 === $("#shipping_address_2_hidden").val() &&
                   shipping_city === $("#shipping_city_hidden").val() && 
                   shipping_state_id === $("#shipping_state_id_hidden").val() && 
                   shipping_state === $("#shipping_state_hidden").val() && 
                   shipping_pincode === $("#shipping_pincode_hidden").val()
                ){
                    $("#address_change_result").html("Previous address and modified address are same, please check.");
                }else{
                    $("#shipping_address_result").html("");
                    $("#shipping_pincode_result").html("");
                    $("#shipping_city_result").html("");
                    $("#shipping_state_id_result").html("");
                    $("#address_change_result").html("");

                    var orderId = $("#shipping_order_id").val();
                    var url = "{{route('admin.childorders.sendVerificationCodes')}}";

                    var arr = { 
                        usermobile:usermobile,
                        useremail:useremail,
                        request_type:'modify_order_address'
                    };
              
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#send_otp').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                            if(result.success){
                                $('#send_otp').removeAttr("disabled");
                                $('#send_otp').css('display', 'none');
                                $('#verifyfinish').css('display', 'block');
                                $('.modal-body').css('opacity', '');
                                $('#verifyblock').css('display', 'block');
                                $('#otpcheck').val(result.otp);

                                $("#shipping_address").attr('readonly', true);
                                $("#shipping_address_2").attr('readonly', true);
                                $("#shipping_city").attr('readonly', true);
                                $("#shipping_state").attr('readonly', true);
                                $("#shipping_state_id").attr('readonly', true);
                                $("#shipping_pincode").attr('readonly', true);

                                // alert(result.success);
                                
                            }else{
                                alert(result.success);
                                $('#send_otp').removeAttr("disabled");
                                $('.modal-body').css('opacity', '');
                                $("#address_change_result").html("Something went wrong, please try again after sometime");
                            }
                        },
                        error:function(err){
                            $('#send_otp').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');
                            $("#address_change_result").html("Something went wrong, please try again after sometime");

                            // console.log(err);
                            // console.log("error"+JSON.stringify(err));
                            // alert("Something went wrong, please try again after sometimeeee");
                           
                        }
                    });
                }

            });

            $('#verifyfinish').on('click',function(){

                var orderId = $("#shipping_order_id").val();
                var otpcode = $("#otpcheck").val();
                var verify_otp = $("#verify_otp").val();
                if(otpcode != verify_otp){
                    $("#address_change_result").html("Verification code mismatch, please check again.");
                }else{
                    var url = "{{route('admin.childorders.updateShippingAddress', ['id'=> ':orderId'])}}";
                    url = url.replace(':orderId', orderId);
                    
                    var arr = { 
                        shipping_name: $("#shipping_name").val(),
                        shipping_mobile: $("#shipping_mobile").val(),
                        shipping_address: $("#shipping_address").val(),
                        shipping_address_2: $("#shipping_address_2").val(),
                        shipping_city: $("#shipping_city").val(),
                        shipping_state_id: $("#shipping_state_id").val(),
                        shipping_state: $( "#shipping_state_id option:selected" ).text(),
                        shipping_pincode: $("#shipping_pincode").val(),
                        usermobile: $('#shipping_mobile').val(),
                        useremail: $('#shipping_email').val(),
                        verify_otp: verify_otp,
                        shipping_address_hidden: $("#shipping_address_hidden").val(),
                        shipping_address_2_hidden: $("#shipping_address_2_hidden").val(),
                        shipping_city_hidden: $("#shipping_city_hidden").val(),
                        shipping_state_id_hidden: $("#shipping_state_id_hidden").val(),
                        shipping_state_hidden: $("#shipping_state_hidden").val(),
                        shipping_pincode_hidden: $("#shipping_pincode_hidden").val(),
                        current_order_status: $("#current_order_status").val(),
                        child_order_id: $("#child_order_id").val(),                        
                    };
              
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#verifyfinish').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                            if(result.success){
                                console.log(result);
                                $('#verifyfinish').removeAttr("disabled");
                                $('#verifyfinish').css('display', 'none');
                                $('.modal-body').css('opacity', '');
                                $('#address_change_result').html("Address Modified Sucessfully");
                                $('#address_change_result').css("color", "#6ac069");
                                // alert(result.success);                      
                            }else{
                                $('#verifyfinish').removeAttr("disabled");
                                $('.modal-body').css('opacity', '');
                                
                                console.log(result);
                                if(result.errors){
                                    console.log(result);

                                    $("#address_change_result").html("Unable to Modify the Address in Shiprocket.");
                                }else{
                                    $("#address_change_result").html(result.message);
                                }
                            }
                        },
                        error:function(err){
                            // alert("error"+JSON.stringify(err));
                            console.log(err.responseJSON);
                            $("#address_change_result").html("Something went wrong, please try again after sometime");
                        
                        }
                    });
                }
            });

            //clear all input fields when clicking data-dismiss button
            $('#modifyAddressModal').on('hidden.bs.modal', function () {
                $(this)
                    .find("input,textarea,select")
                        .val('')
                        .end()
                    .find("input[type=checkbox], input[type=radio]")
                        .prop("checked", "")
                        .end();
                
                $('.modal-body').css('opacity', '');
                $("#verifyfinish").css("display", "none");
                $("#verifyblock").css("display", "none");
                $("#send_otp").css("display", "block");

                $("#shipping_address").removeAttr('readonly');
                $("#shipping_address_2").removeAttr('readonly');
                $("#shipping_city").removeAttr('readonly');
                $("#shipping_state").removeAttr('readonly');
                $("#shipping_state_id").removeAttr('readonly');
                $("#shipping_pincode").removeAttr('readonly');
                
                $("#address_change_result").html("");
            });

            /**
             * For Refund Related Model
             */
            
            //Open the refund model pop up
            $("#refundInitiateModal").on("show.bs.modal", function(e) {
                
                //load the  current values 
                $("#refund_order_id").val($(e.relatedTarget).data('order_id'));
                $("#refund_child_order_id").val($(e.relatedTarget).data('child_order_id'));
                $("#refund_usermobile").val($(e.relatedTarget).data('usermobile'));
                $("#refund_useremail").val($(e.relatedTarget).data('useremail'));
                $("#refund_payment_type").val($(e.relatedTarget).data('userpaymenttype'));
            });


            $('#finish_btn').on('click',function(){

                var orderId = $("#refund_order_id").val();
                var paymentType = $("#refund_payment_type").val();
                if(paymentType == 'c'){
                    $("#cancel_order_result").html("Please select orders related to Online Payment menthod only");
                }else{
                    var url = "{{route('admin.childorders.refundInitiate', ['id'=> ':orderId'])}}";
                    url = url.replace(':orderId', orderId);
                    
                    var arr = { 
                        usermobile: $('#refund_usermobile').val(),
                        useremail: $('#refund_useremail').val(),
                        child_order_id: $("#refund_child_order_id").val(),                        
                        refund_initiate_reason: $("#refund_initiate_reason").val(),                        
                    };
              
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#verifyfinish').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                            if(result.success){
                                
                                $('#cancel_send_otp').removeAttr("disabled");
                                $('#cancel_send_otp').css('display', 'none');

                                $('#cancel_verify_otp_btn').css('display', 'inline');
                                $('#cancel_verify_block').css('display', 'block');
                                $('.modal-body').css('opacity', '');
                                $('#cancel_check_verify_otp').val('');


                                $('#address_change_result').html("Address Modified Sucessfully");
                                $('#address_change_result').css("color", "#6ac069");

                                $("#cancel_order_result").attr('readonly', true);
                                // console.log(result);

                            }else{
                                $('#cancel_verify_block').removeAttr("disabled");
                                $('.modal-body').css('opacity', '');
                                
                                console.log(result);
                                if(result.errors){
                                    console.log(result);

                                    $("#cancel_order_result").html("Unable to Cancel the     Shiprocket.");
                                }else{
                                    $("#cancel_order_result").html(result.message);
                                }
                            }
                        },
                        error:function(err){
                            // alert("error"+JSON.stringify(err));
                            console.log(err.responseJSON);
                            $("#cancel_order_result").html("Something went wrong, please try again after sometime");
                        
                        }
                    });
                }
            }); 

            /**
             * For Cancel Related Model
             */

            //Open the cancel model pop up
            $("#cancelOrderModal").on("show.bs.modal", function(e) {
                
                //load the  current values 
                $("#cancel_order_id").val($(e.relatedTarget).data('order_id'));
                $("#cancel_child_order_id").val($(e.relatedTarget).data('child_order_id'));
                $("#cancel_usermobile").val($(e.relatedTarget).data('usermobile'));
                $("#cancel_useremail").val($(e.relatedTarget).data('useremail'));
            });

            //send the OTP and Email to user when the address is changed
            $('.cancel_send_otp').on('click',function(){

                var cancel_order_id = $("#cancel_order_id").val();
                var cancel_child_order_id = $("#cancel_child_order_id").val();
                var cancel_usermobile = $("#cancel_usermobile").val();
                var cancel_useremail = $("#cancel_useremail").val();
                var order_cancel_reason = $("#order_cancel_reason").val();

                if(order_cancel_reason.trim() == '' ){
                    $('#order_cancel_reason').focus();
                    $("#order_cancel_reason_result").html("Please enter the cancellation reason.");
                    return false;
                }
                else{
                    $("#order_cancel_reason_result").html("");
                    var url = "{{route('admin.childorders.sendVerificationCodes')}}";

                    var arr = { 
                        usermobile:cancel_usermobile,
                        useremail:cancel_useremail,
                        request_type:'cancel_order'
                    };
              
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#cancel_send_otp').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                            if(result.success){
                                $('#cancel_send_otp').removeAttr("disabled");
                                $('#cancel_send_otp').css('display', 'none');
                                $('#cancel_verify_otp_btn').css('display', 'inline');
                                $('#cancel_verify_block').css('display', 'block');
                                $('.modal-body').css('opacity', '');
                                $('#cancel_check_verify_otp').val(result.otp);

                                $("#order_cancel_reason").attr('readonly', true);

                                // alert(result.success);
                                
                            }else{
                                alert(result.success);
                                $('#cancel_send_otp').removeAttr("disabled");
                                $('.modal-body').css('opacity', '');
                                $("#cancel_order_result").html("Something went wrong, please try again after sometime");
                            }
                        },
                        error:function(err){
                            $('#cancel_send_otp').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');
                            $("#cancel_order_result").html("Something went wrong, please try again after sometime");

                            // console.log(err);
                            // console.log("error"+JSON.stringify(err));
                            // alert("Something went wrong, please try again after sometimeeee");
                           
                        }
                    });
                }

            });

            //verify and finish the order cancellation
            $('#cancel_verify_otp_btn').on('click',function(){

                var orderId = $("#cancel_order_id").val();
                var otpcode = $("#cancel_check_verify_otp").val();
                var verify_otp = $("#cancel_verify_otp").val();
                if(otpcode != verify_otp){
                    $("#cancel_order_result").html("Verification code mismatch, please check again.");
                }else{
                    var url = "{{route('admin.childorders.cancelOrder', ['id'=> ':orderId'])}}";
                    url = url.replace(':orderId', orderId);
                    
                    var arr = { 
                        usermobile: $('#cancel_usermobile').val(),
                        useremail: $('#cancel_useremail').val(),
                        verify_otp: verify_otp,
                        child_order_id: $("#cancel_child_order_id").val(),                        
                        order_cancel_reason: $("#order_cancel_reason").val(),                        
                    };
              
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#verifyfinish').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                            if(result.success){
                                // console.log(result);
                                
                                $('#cancel_send_otp').removeAttr("disabled");
                                $('#cancel_send_otp').css('display', 'none');

                                $('#cancel_verify_otp_btn').css('display', 'inline');
                                $('#cancel_verify_block').css('display', 'block');
                                $('.modal-body').css('opacity', '');
                                $('#cancel_check_verify_otp').val('');


                                $('#address_change_result').html("Address Modified Sucessfully");
                                $('#address_change_result').css("color", "#6ac069");

                                $("#cancel_order_result").attr('readonly', true);

                                swal('Order', 'Order Cancelled Successfully', "success");
                                location.reload();

                            }else{
                                $('#cancel_verify_block').removeAttr("disabled");
                                $('.modal-body').css('opacity', '');
                                
                                console.log(result);
                                if(result.errors){
                                    console.log(result);

                                    $("#cancel_order_result").html("Unable to Cancel the     Shiprocket.");
                                }else{
                                    $("#cancel_order_result").html(result.message);
                                }
                            }
                        },
                        error:function(err){
                            // alert("error"+JSON.stringify(err));
                            console.log(err.responseJSON);
                            $("#cancel_order_result").html("Something went wrong, please try again after sometime");
                        
                        }
                    });
                }
            }); 

            //clear all input fields when clicking data-dismiss button
            $('#cancelOrderModal').on('hidden.bs.modal', function () {
                $(this)
                    .find("input,textarea,select")
                        .val('')
                        .end()
                    .find("input[type=checkbox], input[type=radio]")
                        .prop("checked", "")
                        .end();

                $('#cancel_send_otp').removeAttr("disabled");
                $('#cancel_send_otp').css('display', 'block');
                $('#cancel_verify_otp_btn').css('display', 'none');
                $('#cancel_verify_block').css('display', 'none');
                $('.modal-body').css('opacity', '');
                $('#cancel_check_verify_otp').val('');

                $("#order_cancel_reason").removeAttr('readonly');
                
                $("#shipping_pincode").removeAttr('readonly');
                
                $("#cancel_order_result").html("");
            });


            //COmmission Details Updation
            //get the current Commission Values of the order
            $("#commissionDetailsModal").on("show.bs.modal", function(e) {
                //load the  current values 
                var commission_model_name = $(e.relatedTarget).data('commission_name');
                var orderWeight = parseInt( $(e.relatedTarget).data('order_weight') );

                $("#commission_value").val($(e.relatedTarget).data('commission_value'));
                $("#shipping_charges").val($(e.relatedTarget).data('shipping_charges'));
                $("#handling_charges").val($(e.relatedTarget).data('handling_charges'));
                $("#commission_child_order_id").val($(e.relatedTarget).data('child_order_id'));
                $("#commission_model_name").text(commission_model_name);
                $("#commission_model_type").text($(e.relatedTarget).data('commission_type'));
                $("#commission_link").attr("href", "{{route('admin.commissionmodels')}}?search=1&fname="+commission_model_name);

                $("#order_shipping_model").text($(e.relatedTarget).data('order_shipping_model'));
                $("#order_mrp").text($(e.relatedTarget).data('order_mrp'));
                $("#order_price").text($(e.relatedTarget).data('order_price'));
                $("#order_quantity").text($(e.relatedTarget).data('order_quantity'));
                // $("#order_weight").text(orderWeight);
                $("#order_weight").text($(e.relatedTarget).data('order_weight') * 1000);
                $("#order_volume").text($(e.relatedTarget).data('order_volume'));
                $("#order_volume").text($(e.relatedTarget).data('order_volume'));

                if($(e.relatedTarget).data('payout_status') == 1){
                    $("#editCommissionDetails").attr("disabled", true);
                }else{
                    $("#editCommissionDetails").removeAttr("disabled");
                }

                // $("#commission_value_gst").val($(e.relatedTarget).data('handling_charges'));
                // $("#shipping_charge_gst").val($(e.relatedTarget).data('handling_charges'));
                // $("#handling_charge_gst").val($(e.relatedTarget).data('handling_charges'));
            });

            $('#editCommissionDetails').on('click',function(){
                $("#commission_value").attr("readonly", false);
                $("#shipping_charges").attr("readonly", false);
                $("#handling_charges").attr("readonly", false);
                $("#commission_btn").removeAttr("disabled");
            });
            
            $('#commission_btn').on('click',function(){

                var commission_value = $("#commission_value").val();
                var shipping_charges = $("#shipping_charges").val();
                var handling_charges = $("#handling_charges").val();
                var orderId = $("#commission_child_order_id").val();
                var comments = $("#comments").val();
                
                if(commission_value.trim() == ''){
                    $('#commission_value').focus();
                    $("#commission_value_result").html("This filed is required.");
                    return false;
                }else if(shipping_charges.trim() == ''){
                    $('#shipping_charges').focus();
                    $("#shipping_charges_result").html("This filed is required.");
                    return false;
                }else if(handling_charges.trim() == ''){
                    $('#handling_charges').focus();
                    $("#handling_charges_result").html("This filed is required.");
                    return false;
                }else if(comments.trim() == ''){
                    $('#comments').focus();
                    $("#comments_result").html("This filed is required.");
                    return false;
                }else{
                    var url = "{{route('admin.childorders.modifyCommission',['id'=>':orderId'])}}";
                    url = url.replace(':orderId', orderId);
                    
                    var arr = { 
                        commission_value: commission_value,
                        shipping_charges: shipping_charges,
                        handling_charges: handling_charges,                        
                        order_id: orderId,                  
                        comments: comments,                  
                    };

                    $("#modify_commission_result_success").html('');
                    $("#modify_commission_result").html("");
            
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#commission_btn').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                            $('#commission_btn').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');                           
                            $("#modify_commission_result_success").html(result.message);
                        },
                        error:function(err){
                            // alert("error"+JSON.stringify(err));
                            console.log(err.responseJSON);
                            $('.modal-body').css('opacity', '');
                            $("#modify_commission_result").html("Something went wrong, please try again after sometime");                        
                        }
                    });
                }
            }); 

            $('#commissionDetailsModal').on('hidden.bs.modal', function (e) {

                $("#commission_value").attr("readonly", true);
                $("#shipping_charges").attr("readonly", true);
                $("#handling_charges").attr("readonly", true);

                $("#commission_value").val('');
                $("#shipping_charges").val('');
                $("#handling_charges").val('');
                $("#commission_child_order_id").val('');
                $("#commission_model_name").text();
                $("#commission_model_type").text();

                $("#order_shipping_model").text('');
                $("#order_weight").text('');
                $("#order_mrp").text('');
                $("#order_price").text('');
                $("#order_quantity").text('');
                $("#order_volume").text('');
                $("#order_volume").text('');
                $("#editCommissionDetails").attr("disabled", true);
                $("#commission_btn").attr("disabled", true);
                $("#modify_commission_result_success").text();
                $("#modify_commission_result").text();
                $("#comments").val('');

                $('.modal-body').css('opacity', '');
                
            })


            //date Picker
            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.childorders')}}?search=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fostatus').val() != '') url += "&fostatus=" + encodeURIComponent($('#fostatus').val());
                if ($('#fmodel').val() != '') url += "&fmodel=" + encodeURIComponent($('#fmodel').val());
                if ($('#fparentid').val() != '') url += "&fparentid=" + encodeURIComponent($('#fparentid').val());
                if ($('#fchildid').val() != '') url += "&fchildid=" + encodeURIComponent($('#fchildid').val());
                if ($('#fcustomermobile').val() != '') url += "&fcustomermobile=" + encodeURIComponent($('#fcustomermobile').val());
                if ($('#fcustomername').val() != '') url += "&fcustomername=" + encodeURIComponent($('#fcustomername').val());
                if ($('#fproductsku').val() != '') url += "&fproductsku=" + encodeURIComponent($('#fproductsku').val());
                // if ($('#fcustomeremails').val() != '') url += "&fcustomeremails=" + encodeURIComponent($('#fcustomeremails').val());
                if ($('#fproductname').val() != '') url += "&fproductname=" + encodeURIComponent($('#fproductname').val());
               
                window.location.href = url;
            });

            // Export all childorders data
            $('#btn_table_export').on('click', function(e) {
                e.preventDefault();

                var url = "{{-- route('admin.childorders.export') --}}?export=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fostatus').val() != '') url += "&fostatus=" + encodeURIComponent($('#fostatus').val());

                window.location.href = url;
            });

            // Delete Products
            $('body').on('click', '.dt-list-delete', function(event) {
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                swal({
                        title: 'Are you sure?',
                        text: 'You want Delete the record.',
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonClass: 'btn-success',
                        cancelButtonClass: 'btn-danger',
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: 'post',
                                success: function(result) {
                                    swal('Child Order', 'Record Deleted Successfully',
                                        "success");
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
            });
        }

    };
}();

jQuery(document).ready(function() {
    Orders.init();
});
</script>
@endpush
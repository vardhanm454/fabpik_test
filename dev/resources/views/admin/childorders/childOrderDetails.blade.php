@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-table/bootstrap-table.min.css' )}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
@if(isset($orderDetails))
<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="panel panel-info order-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="caption col-md-6 pd-tb-5">
                        <span class="pd-tb-10">
                            <span class="pd-tb-10"><span aria-hidden="true" class="icon-handbag"></span>
                            Order Information </span>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row static-info">
                    <div class="col-md-4 name"> Parent Order ID: </div>
                    <div class="col-md-8 value"> {{$orderDetails->order->parent_order_id}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Order Date: </div>
                    <div class="col-md-8 value"> {{date('d-m-Y', strtotime($orderDetails->order->created_at))}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Order Status: </div>
                    <div class="col-md-8 value"> 
                    @if(isset($orderStatus))
                    @foreach($orderStatus as $status)
                    {{($status->id == $orderDetails->order_status_id)?$status->admin_text:''}}

                    @endforeach
                    @endif 
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Payment Method: </div>
                    <div class="col-md-8 value"> 
                    @foreach(['c'=>'Cash on Delivery', 'o'=>'Online'] as $val=>$label)
                    {{($val == $orderDetails->order->payment_type)?$label:''}}
                    @endforeach
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Payment Status: </div>
                    <div class="col-md-8 value">
                    @if(isset($paymentStatus))
                    @foreach($paymentStatus as $status)
                        {{($status->id == $orderDetails->order->payment_status_id)?$status->admin_text:''}}
                    @endforeach
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-12">
        <div class="panel panel-info customer-info">
            <div class="panel-heading">
                <div class="caption pd-tb-5">
                    <span aria-hidden="true" class="icon-handbag"></span> Order History
                </div>
            </div>
            <div class="panel-body">
            <table data-toggle="table" data-height="150" id="">
                <thead>
                    <tr>
                        <th data-field="date" data-align="center" data-sortable="true">
                            Date
                        </th>
                        <th data-field="status" data-align="center">
                            Status
                        </th>
                    </tr>
                </thead>
                <tbody>
                @if(isset($orderHistory))
                    @foreach($orderHistory as $history)
                    <tr>
                        <td>
                            {{date('d M Y h:m:i', strtotime($history->created_at))}}
                        </td>
                        <td>
                            @foreach($orderStatus as $status)
                            {!! ($status->id == $history->order_status_id)? '<b> Order Status: </b>'.$status->seller_text:'' !!}
                            @endforeach
                            <br />
                            @foreach($shippingStatus as $status)
                            {!! ($status->id == $history->shipping_status_id)? '<b> Shipping Status: </b>'.$status->seller_text:'' !!}
                            @endforeach
                        </td>
                    </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
                
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="panel panel-info customer-info">
            <div class="panel-heading">
                <div class="caption pd-tb-5">
                    <span aria-hidden="true" class="icon-user"></span> Customer Information
                </div>
            </div>
            <div class="panel-body">
                <div class="row static-info">
                    <div class="col-md-4 name"> Customer Name: </div>
                    <div class="col-md-8 value"> {{isset($orderDetails->order->first_name)?$orderDetails->order->first_name:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Email ID: </div>
                    <div class="col-md-8 value"> {{isset($orderDetails->order->email)?$orderDetails->order->email:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Phone No.: </div>
                    <div class="col-md-8 value"> {{isset($orderDetails->order->mobile)?$orderDetails->order->mobile:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Address: </div>
                    <div class="col-md-8 value"> {{isset($orderDetails->order->billing_address1)?$orderDetails->order->billing_address1:''}},
                    {{isset($orderDetails->order->billing_address2)?$orderDetails->order->billing_address2:''}}<br>
                    {{isset($orderDetails->order->billing_state)?$orderDetails->order->billing_state:''}},
                    {{isset($orderDetails->order->billing_city)?$orderDetails->order->billing_city:''}}
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Pincode: </div>
                    <div class="col-md-8 value"> {{isset($orderDetails->order->billing_pincode)?$orderDetails->order->billing_pincode:''}} </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-12">
        <div class="panel panel-info customer-info">
            <div class="panel-heading">
                <div class="caption pd-tb-5">
                    <span aria-hidden="true" class="icon-user"></span> Shipping Information
                </div>
            </div>
            <div class="panel-body">
                <div class="row static-info">
                    <div class="col-md-4 name"> Name: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->shipping_first_name)?$orderDetails->order->shipping_first_name:''}}
                        {{isset($orderDetails->order->shipping_last_name)?$orderDetails->order->shipping_last_name:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Email ID: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->shipping_email)?$orderDetails->order->shipping_email:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Phone No.: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->shipping_mobile)?$orderDetails->order->shipping_mobile:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Address: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->shipping_address1)?$orderDetails->order->shipping_address1:''}},
                        {{isset($orderDetails->order->shipping_address2)?$orderDetails->order->shipping_address2:''}},
                        {{isset($orderDetails->order->shipping_city)?$orderDetails->order->shipping_city:''}},
                        {{isset($orderDetails->order->shipping_state)?$orderDetails->order->shipping_state:''}}
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Pincode: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->shipping_pincode)?$orderDetails->order->shipping_pincode:''}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="panel panel-info products-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="caption col-md-6 pd-tb-5">
                        <span class="pd-tb-10"><span aria-hidden="true" class="icon-handbag"></span> Products Ordered </span>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table  table-centered">
                        <thead>
                            <tr>
                                <th> Sr. No. </th>
                                <th> Child Order ID </th>
                                <th> Description</th>
                                <th> MRP </th>
                                <th> Sale Price </th>
                                <th> Quantity </th>
                                <th> Discount </th>
                                <th> Sub Total </th>
                                <th> Coupon Code </th>
                                <th> Coupon Amount </th>
                                <th width="70"> Coupon Type </th>
                                <th width="70"> Deal Price </th>
                                <th width="70"> Total Amount </th>
                                <th width="80"> Order Status </th>
                                <th width="90"> Shipping Status </th>
                                <th width="100"> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>{{$orderDetails->child_order_id}}</td>
                                <td>{{$orderDetails->productVarient->name}} <br>
                                    @if(isset($attributeOptions))
                                    @foreach($attributeOptions as $options)
                                    <b>{{$options['atrributeName']}}</b> : {{$options['atrributeValue']}}</br>
                                    @endforeach
                                    @endif
                                </td>
                                <td>{{$orderDetails->mrp}}</td>
                                <td>{{$orderDetails->price}}</td>
                                <td>{{$orderDetails->quantity}}</td>
                                <td>{{$orderDetails->discount}}</td>
                                <td>{{$orderDetails->price*$orderDetails->quantity}}</td>
                                <td>{{(isset($orderDetails->coupon_code) ? $orderDetails->coupon_code : '-')}}</td>
                                <td>{{isset($orderDetails->coupon_discount)?$orderDetails->coupon_discount:0}}</td>
                                @if(!is_null($orderDetails->coupon_id))
                                    @if($orderDetails->dis_type == 'p' && $orderDetails->sub_type = 'm')
                                        <td>Percentage on MRP</td>
                                    @elseif($orderDetails->dis_type == 'p' && $orderDetails->sub_type = 's')
                                        <td>Percentage on Selling Price</td>
                                    @elseif($orderDetails->dis_type == 'f' && $orderDetails->sub_type = 's')
                                        <td>Flat on Selling Price</td>
                                    @else
                                        <td>-</td>
                                    @endif
                                @else
                                    <td>-</td>
                                @endif
                                <td>{{number_format((float)(isset($orderDetails->deal_price)?$orderDetails->deal_price:'0'), '1')}}</td>                                
                                
                                <td>{{$orderDetails->total}}</td>
                                <td>
                                    @if(isset($orderStatus))
                                    @foreach($orderStatus as $status)
                                    {{($status->id == $orderDetails->order_status_id)?$status->admin_text:''}}
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(isset($shippingStatus))
                                    @foreach($shippingStatus as $status)
                                        {{($status->id == $orderDetails->shipping_status_id)?$status->admin_text:''}}
                                    @endforeach
                                    @endif
                                </td>
                                <td> 
                                    <div class="">
                                    @if($orderDetails->order_status_id == 8 || $orderDetails->order_status_id == 9)
                                        <a href="{{route('admin.childorders.downloadCustomerInvoice', ['id'=>$orderDetails->id])}}" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Download Invoice" title="Download Invoice">
                                            <span aria-hidden="true" class="icon-cloud-download"></span>
                                        </a>
                                    @endif
                                        <a href="javascript:;" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Track Order" title="Track Order">
                                            <span aria-hidden="true" class="icon-pointer"></span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-table/bootstrap-table.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
@endpush
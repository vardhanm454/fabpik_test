@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <!-- FILTER FORM START -->
                <form action="#" method="get" class="filter-form">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-2 pd-tb-5"><strong>Role:</strong></label>
                                            <div class="col-md-9">
                                            <select name="name" id="name"
                                                    class="form-control form-filter input-sm select2">
                                                    <option value="">Select Role</option>
                                                    @if(isset($roles))
                                                    @foreach($roles as $role)
                                                    <option value="{{$role->name}}"
                                                    {{request()->name==$role->name?'selected="selected"':''}}>
                                                    {{$role->id}} - {{$role->name}}
                                                    </option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label col-md-4 pd-tb-5"><strong>Action:</strong></label>
                                        <div class="col-md-8">
                                            <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span> </button>
                                            <a class="btn btn-sm btn-icon-only white" href="{{route('admin.roles')}}" title="Reset Filter"><span aria-hidden="true" class="icon-refresh"></span> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <a href= "{{route('admin.roles.create')}}" class="btn btn-default btn-sm tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Add Role"><strong><span aria-hidden="true" class="icon-plus text-primary"></span>&nbsp;Add</strong></a>

                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete" data-action="soft-delete"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;Delete</strong></a>

                        <a href="javascript:;" id="btn_table_export" class="btn btn-default btn-sm tooltips" data-container="body" data-placement="top" data-original-title="Export to Excel"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a>

                    </div>

                    <table class="table table-checkable nowrap" id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th class="text-center"> Id </th>
                                <th class="text-center"> Role </th>                           
                                <th class="text-center"> Action </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Roles = function () {

    return {

        //main function to initiate the module
        init: function () {

            // Do Filter
            $('#btn_submit_search').on('click', function(e){
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.roles')}}?search=1";

                if($('#name').val() != '') url += "&name="+encodeURIComponent($('#name').val());

                window.location.href = url;
            }); 

            // Export all Category data
            $('#btn_table_export').on('click', function(e){
                e.preventDefault();

                var url = "{{route('admin.roles.export')}}?export=1";

                if($('#name').val() != '') url += "&name="+encodeURIComponent($('#name').val());

                window.location.href = url;
            });

            // Delete Category
            $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete this Role.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                swal('Roles', 'Role Deleted Successfully', "success");
                                location.reload();
                            }
                        });
                    }
                });
            });
        }

    };

}();

jQuery(document).ready(function() {    
   Roles.init();
});

</script>
@endpush
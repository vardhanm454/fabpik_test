@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<style>
    .rolestyle{
        margin-right:-145px;
    }
</style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal form-row-seperated" action="{{isset($role)?route('admin.roles.edit',['id'=>$role->id]):route('admin.roles.add')}}" method="post">

            {!! csrf_field() !!}

            <div class="portlet light">

                <div class="portlet-body">
                    <div class="form-body">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group {{($errors->first('name'))?'has-error':''}}">
                                    <label class="col-md-5 control-label ">Name:<span class="required"> *
                                        </span></label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control maxlength-handler" name="name" maxlength="100" value="{{old('name', isset($role)?$role->name:'')}}">
                                        <span class="help-block">{{$errors->first('name')}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6" style="max-height: 450px; overflow-y: scroll;">
                                @foreach($moduleWisePerms as $moduleName => $modulePerms)
                                <div class="perm-group">
                                    <label class="sbold"> <i class="fa fa-key"></i> &nbsp; {{$moduleName}}</label>
                                    <div class="mt-checkbox-list">
                                        @foreach($modulePerms as $modulePerm)
                                        <label class="mt-checkbox mt-checkbox-outline"> {{$modulePerm->display_name}}
                                        <input type="checkbox" value="{{$modulePerm->id}}" name="permissions[]" {{(in_array($modulePerm->id,$asignedPerms))?'checked':''}}>
                                        <span></span>
                                        </label>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>

                        </div>

                        <div class="form-actions text-center">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save</button>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var addEditRoles = function () {
    return {
        //main function to initiate the module
        init: function () {

            $('#btn_back').on('click', function(e){
                e.preventDefault();
                window.location.href = "{{route('admin.roles')}}";
            });

            $('#form-submit').on('click', function(e){
                e.preventDefault();
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 5
            });
        }
    };
}();

jQuery(document).ready(function() {
    addEditRoles.init();
});
</script>
@endpush
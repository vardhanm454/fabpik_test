<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>{{ $title ?? '' }} | {{APP_NAME}}</title>
        <base href="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="keywords" content="online shopping, online shopping sites, online shopping india, india shopping, Online shopping site, Online kids shopping, online kids shopping, Online kids shopping india, online kids shopping india" />
        <meta name="description" content="Online Shopping Site for Fashion & Lifestyle in India. Buy Shoes, Clothing, Accessories and lifestyle products for kids. Best Online Fashion Store * COD* Easy returns and exchanges*" />
        <meta content="" name="author" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ __common_asset('global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ __common_asset('global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ __common_asset('global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ __common_asset('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{__common_asset('global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{__common_asset('global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{__common_asset('global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ __common_asset('global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ __common_asset('global/css/components.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        @if(isset($datatable_listing) && $datatable_listing===true)
        <link href="{{ __common_asset('global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ __common_asset('global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
        @endif
        @stack('PAGE_ASSETS_CSS')
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ __common_asset('global/css/components-rounded.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ __common_asset('global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ __common_asset('layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ __common_asset('layouts/layout/css/themes/darkblue.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ __common_asset('layouts/layout/css/custom.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->

        <!-- BEGIN PAGE LEVEL STYLES -->
        @stack('PAGE_STYLES')
        <!-- END PAGE LEVEL STYLES -->

        <script type="text/javascript">
            var locBaseUrl = "{{url('')}}";
        </script>

        <link rel="shortcut icon" href="{{ __common_asset('img/favicon.png') }}" type="image/x-icon">
        <link rel="icon" href="{{ __common_asset('img/favicon.png') }}" type="image/x-icon">

        <!--[if lt IE 9]>
        <script src="{{ __common_asset('global/plugins/respond.min.js') }}"></script>
        <script src="{{ __common_asset('global/plugins/excanvas.min.js') }}"></script>
        <script src="{{ __common_asset('global/plugins/ie8.fix.min.js') }}"></script>
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ __common_asset('global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ __common_asset('global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ __common_asset('global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ __common_asset('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ __common_asset('global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ __common_asset('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <script src="{{__common_asset('global/plugins/bootstrap-toastr/toastr.min.js')}}" type="text/javascript"></script>
        <script src="{{__common_asset('global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
        <script src="{{ __common_asset('global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
    </head>
    <!-- END HEAD -->
    <body class="page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-header-fixed page-footer-fixed page-sidebar-closed">
        <div class="page-wrapper">

            @include(ADMIN_THEME_NAME.'.components.navbar')

            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">

                <!-- BEGIN SIDEBAR -->
                @include(ADMIN_THEME_NAME.'.components.sidebar')
                <!-- END SIDEBAR -->

                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">

                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar margin-bottom-10">
                            @include(ADMIN_THEME_NAME.'.components.breadcrumb')
                        </div>
                        <!-- END PAGE BAR -->

                        @yield('content')

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            @include(ADMIN_THEME_NAME.'.components.footer')
            <!-- END FOOTER -->
        </div>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        @if(isset($datatable_listing) && $datatable_listing===true)
        <script src="{{__common_asset('global/scripts/datatable.js')}}" type="text/javascript"></script>
        <script src="{{__common_asset('global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
        <script src="{{__common_asset('global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
        @endif
        @stack('PAGE_ASSETS_JS')
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ __common_asset('global/scripts/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script type="text/javascript">
            $(function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                App.setAssetsPath(locBaseUrl+"/public/common/");

                $.fn.select2.defaults.set("theme", "bootstrap");

                $(".select2, .select2-multiple").select2({
                    placeholder: "Select",
                    width: null
                });

                $(".select2-allow-clear").select2({
                    allowClear: true,
                    placeholder: "Select",
                    width: null
                });

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-top-right", //toast-top-full-width
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut",
                    "progressBar": true
                }

                @if (Session::has('status') && Session::has('toast'))
                toastr["{{session('status')}}"]("{{session('message')}}", "{{session('title')}}");
                @endif
                // toastr["success"]("sadasdasd", "sadasdasd");

                @if(isset($datatable_listing) && $datatable_listing===true)

                if (!jQuery().dataTable) {
                    return;
                }

                var grid = new Datatable(),
                    dt_ajax_url = "{{$dt_ajax_url}}",
                    dt_tools_columns = new Array(@php echo $dt_tools_columns @endphp),
                    searchParams = '';

                @if(request()->search)
                searchParams += '?';
                @foreach($dt_search_colums as $dt_search_colum)
                @if(isset(request()->$dt_search_colum) && request()->$dt_search_colum != '')
                searchParams += '&{{$dt_search_colum}}={{urlencode(request()->$dt_search_colum)}}';
                @endif
                @endforeach
                @endif

                dt_ajax_url += searchParams;

                @php
                $dt_page = (isset($dt_page))?$dt_page:1;
                $dt_perpage = (isset($dt_perpage))?$dt_perpage:50;
                @endphp

                grid.init({
                    src: $("#dt_listing"),
                    onSuccess: function (grid, response) {},
                    onError: function (grid) {},
                    onDataLoad: function(grid) {
                        // global tooltips
                        $('body').find('.tooltips').tooltip();
                        $('body').find('.status-switch').bootstrapSwitch();
                    },
                    loadingMessage: 'Loading data please wait...',
                    dataTable: {
                        "lengthMenu": [
                            [10, 20, 50],
                            [10, 20, 50] // change per page values here
                        ],
                        "pageLength": 50, // default record count per page
                        "displayStart": {{($dt_page - 1) * $dt_perpage}},
                        "ajax": {
                            "url": dt_ajax_url, // ajax source
                        },
                        "language": {
                            "emptyTable": "No data found!"
                        },
                        "columnDefs": [
                            {
                                "targets": 0,
                                "orderable": false
                            }
                            @if($dt_center_columns)
                            ,{"className": "dt-center", "targets": [@php echo $dt_center_columns @endphp]}
                            @endif
                        ],
                        @if($dt_ordering)
                        "ordering": 1,
                        "order": [
                            [@php echo $dt_ordering @endphp, "desc"]
                        ]
                        @endif
                    }
                });

                // handle Full Product Export button click
                grid.getTableWrapper().on('click', '.table-export-action', function (e) {
                    e.preventDefault();
                    var action = $(this).attr('data-action');
                    var pcategory = $('#fpcategory').val();
                    var sellerId = $('#fseller').val();
                    if(pcategory == '' || pcategory == undefined || sellerId == '' || sellerId == undefined){
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'Select parent category and seller to export products',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    }
                    else if (action=='product-export' && grid.getSelectedRowsCount() > 0 && pcategory != '') {
                        action = 'product-export';
                        var url = "{{route('admin.products.fullexport')}}?export=1&fids="+grid.getSelectedRows();
                        if ($('#fname').val() != '') url += "&fname=" + encodeURIComponent($('#fname').val());
                        if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                        if ($('#fstatus').val() != '') url += "&fstatus=" + encodeURIComponent($('#fstatus').val());
                        if ($('#fpricefrom').val() != '') url += "&fpricefrom=" + encodeURIComponent($('#fpricefrom').val());
                        if ($('#fpriceto').val() != '') url += "&fpriceto=" + encodeURIComponent($('#fpriceto').val());
                        if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                        if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                        if ($('#fcategory').val()) url += "&fcategory=" + encodeURIComponent($('#fcategory').val());
                        if ($('#fsku').val()) url += "&fsku=" + encodeURIComponent($('#fsku').val());
                        if ($('#funiqueid').val()) url += "&funiqueid=" + encodeURIComponent($('#funiqueid').val());
                        if ($('#fpcategory').val() != '') url += "&fpcategory=" + encodeURIComponent($('#fpcategory').val());

                        window.location.href = url;
                    } else if (action == "") {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'Please select an action',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    } else if (grid.getSelectedRowsCount() === 0) {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'Select products to export',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    }
                });

                // handle group actionsubmit button click
                grid.getTableWrapper().on('click', '.table-group-action', function (e) {
                    e.preventDefault();
                    // var action = $(".table-group-action-input", grid.getTableWrapper());
                    // if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                    var action = $(this).attr('data-action');
                    if (action=='soft-delete' && grid.getSelectedRowsCount() > 0) {
                        action = 'delete';
                        swal({
                            title: 'Are you sure?',
                            text: "You want to delete selected records.",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonClass: 'btn-success',
                            cancelButtonClass: 'btn-danger',
                            closeOnConfirm: true,
                            closeOnCancel: true,
                            confirmButtonText: 'Yes',
                            cancelButtonText: 'No',
                        },
                        function(isConfirm){
                            if (isConfirm){
                                grid.setAjaxParam("customActionType", "group_action");
                                grid.setAjaxParam("customActionName", action);
                                grid.setAjaxParam("id", grid.getSelectedRows());
                                grid.getDataTable().ajax.reload();
                                grid.clearAjaxParams();
                            }
                        });
                    }else if (action=='soft-delete-coupon-group' && grid.getSelectedRowsCount() > 0) {
                        action = 'delete-coupon-group';
                        swal({
                            title: 'Are you sure?',
                            text: "You want to delete selected records from the coupon group.",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonClass: 'btn-success',
                            cancelButtonClass: 'btn-danger',
                            closeOnConfirm: true,
                            closeOnCancel: true,
                            confirmButtonText: 'Yes',
                            cancelButtonText: 'No',
                        },
                        function(isConfirm){
                            if (isConfirm){
                                grid.setAjaxParam("customActionType", "group_action");
                                grid.setAjaxParam("customActionName", action);
                                grid.setAjaxParam("id", grid.getSelectedRows());
                                grid.setAjaxParam("groupId", $("#groupId").val());
                                grid.setAjaxParam("checkall", document.getElementById("checkall").checked);
                                grid.setAjaxParam("fpricefrom", document.getElementById('fpricefrom').value);
                                grid.setAjaxParam("fpriceto", document.getElementById('fpriceto').value);
                                grid.setAjaxParam("fpdiscountfrom", document.getElementById('fpdiscountfrom').value);
                                grid.setAjaxParam("fpdiscountto", document.getElementById('fpdiscountto').value);
                                grid.setAjaxParam("fname", document.getElementById('fname').value);
                                grid.setAjaxParam("fbrand", document.getElementById('fbrand').value);
                                grid.setAjaxParam("fcategory", document.getElementById('fcategory').value);
                                grid.setAjaxParam("fsubcategory", document.getElementById('fsubcategory').value);
                                grid.setAjaxParam("fchildcategory", document.getElementById('fchildcategory').value);
                                grid.setAjaxParam("fseller", document.getElementById('fseller').value);
                                grid.setAjaxParam("fdiscountfrom", document.getElementById('fdiscountfrom').value);
                                grid.setAjaxParam("fdiscountto", document.getElementById('fdiscountto').value);
                                grid.setAjaxParam("fdiscountfrom", document.getElementById('fdiscountfrom').value);
                                grid.getDataTable().ajax.reload();
                                grid.clearAjaxParams();
                            }
                        });
                    }


                    else if(action=='change-payment-status' && grid.getSelectedRowsCount() > 0){
                        action = 'changepaymentstatus';
                        swal({
                            title: 'Are you sure?',
                            text: "make sure the product is returned to the seller before doing this.",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonClass: 'btn-success',
                            cancelButtonClass: 'btn-danger',
                            closeOnConfirm: true,
                            closeOnCancel: true,
                            confirmButtonText: 'Yes',
                            cancelButtonText: 'No',
                        },
                        function(isConfirm){
                            if (isConfirm){
                                grid.setAjaxParam("customActionType", "group_action");
                                grid.setAjaxParam("customActionName", action);
                                grid.setAjaxParam("id", grid.getSelectedRows());
                                grid.setAjaxParam("status", 4);
                                grid.getDataTable().ajax.reload();
                                grid.clearAjaxParams();
                            }
                        });
                    }else if (action=='product-tags' && grid.getSelectedRowsCount() > 0) { //for Group Product Tag Update
                        action = 'tagupdate';
                        $("#result").html('');
                        // $("#product_tags").tagsinput('removeAll');
                        $("#product_tags").html('');
                        $("#productTagsModal").modal();

                        $("#modal-btn-yes").unbind().on("click", function(){
                            if($("#product_tags").val() == "" || $("#product_tags").val() == undefined){
                                $("#result").html("Choose atleast one tag");
                            }else{
                                grid.setAjaxParam("customActionName", action);
                                grid.setAjaxParam("id", grid.getSelectedRows());
                                grid.setAjaxParam("product_tags", $("#product_tags").val());
                                grid.getDataTable().ajax.reload();
                                grid.clearAjaxParams();

                                $("#productTagsModal").modal('hide');
                            }
                        });
                    }
                    else if (action == "") {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'Please select an action',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    } else if (grid.getSelectedRowsCount() === 0) {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'No record selected',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    }
                });

                // handle group actionsubmit button click
                grid.getTableWrapper().on('click', '.table-group-action', function (e) {
                    e.preventDefault();
                    // var action = $(".table-group-action-input", grid.getTableWrapper());
                    // if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                    var action = $(this).attr('data-action');
                    if (action=='soft-delete' && grid.getSelectedRowsCount() > 0) {
                        action = 'delete';
                        swal({
                            title: 'Are you sure?',
                            text: "You want to delete selected records.",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonClass: 'btn-success',
                            cancelButtonClass: 'btn-danger',
                            closeOnConfirm: true,
                            closeOnCancel: true,
                            confirmButtonText: 'Yes',
                            cancelButtonText: 'No',
                        },
                        function(isConfirm){
                            if (isConfirm){
                                grid.setAjaxParam("customActionType", "group_action");
                                grid.setAjaxParam("customActionName", action);
                                grid.setAjaxParam("id", grid.getSelectedRows());
                                grid.getDataTable().ajax.reload();
                                grid.clearAjaxParams();
                            }
                        });
                    } else if (action == "") {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'Please select an action',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    } else if (grid.getSelectedRowsCount() === 0) {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'No record selected',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    }
                });

                $('.termsandconditions').click(function(){
                    // Display Modal
                    $('#modaltermsandconditions').modal('show');
                });

                // handle group action change order status submit button click
                grid.getTableWrapper().on('click', '.table-group-action', function (e) {
                    e.preventDefault();
                    var action = $(this).attr('data-action');
                    if (action=='change-status' && grid.getSelectedRowsCount() > 0) {
                        action = 'changestatus';

                        $("#result").html('');
                        $("#reason-result").html('');
                        $("#remark-result").html('');
                        $("#changeStatusModal").modal();
                        var modalConfirm = function(callback){
                            $("#modal-btn-yes").unbind().on("click", function(){
                                if($("#status option:selected").val() == ""){
                                    $("#result").html("Please Select Status");
                                }else if($("#status").val() == 0 && $("#reason").val() == ''){
                                    $("#reason-result").html("Please enter reason for deactivation");
                                }else if($("#status option:selected").val() != "" && $("#remark").val() == ''){
                                    $("#result").html('');
                                    $("#remark-result").html("Please enter remark");
                                }else{
                                    callback(true);
                                    $("#result").html('');
                                    $("#reason-result").html('');
                                    $("#remark-result").html('');
                                    $("#changeStatusModal").modal('hide');
                                }
                            });
                        };

                        modalConfirm(function(confirm){
                            if(confirm){
                                // grid.setAjaxParam("customActionType", "group_action");
                                grid.setAjaxParam("customActionName", action);
                                grid.setAjaxParam("id", grid.getSelectedRows());
                                grid.setAjaxParam("status", $("#status option:selected").val());
                                grid.setAjaxParam("inactive_reason", $("#inactive_reason").val());
                                grid.setAjaxParam("remark", $("#remark").val());
                                grid.getDataTable().ajax.reload();
                                grid.clearAjaxParams();
                            }
                        });
                    }if (action=='refund-initiate' && grid.getSelectedRowsCount() > 0) {
                        action = 'refundInitiate';
                        // alert($("#refund_payment_type").val());
                        // if($("#refund_payment_type").val()=='o'){
                            $("#result").html('');
                            $("#refund_initiate_reason_result").html('');
                            $("#refundInitiateModal").modal();
                            var modalConfirm = function(callback){
                                $("#finish_btn").unbind().on("click", function(){
                                    if($("#reason").val() == ''){
                                        $("#refund_initiate_reason_result").html("Please enter reason for refund intitalization");
                                    }else{
                                        callback(true);
                                        $("#result").html('');
                                        $("#refund_initiate_reason_result").html('');
                                        $("#refundInitiateModal").modal('hide');
                                    }
                                });
                            };
                        // }
                        // else{
                        //     App.alert({
                        //     type: 'danger',
                        //     icon: 'warning',
                        //     message: 'Please select orders whose payment type is online only.',
                        //     container: grid.getTableWrapper(),
                        //     place: 'prepend'
                        // });
                        // }

                        modalConfirm(function(confirm){
                            if(confirm){
                                // grid.setAjaxParam("customActionType", "group_action");
                                grid.setAjaxParam("customActionName", action);
                                grid.setAjaxParam("id", grid.getSelectedRows());
                                grid.setAjaxParam("refund_order_id", $("#refund_order_id").val());
                                grid.setAjaxParam("refund_payment_type", $("#refund_payment_type").val());
                                grid.setAjaxParam("refund_child_order_id", $("#refund_child_order_id").val());
                                grid.setAjaxParam("refund_usermobile", $("#refund_usermobile").val());
                                grid.setAjaxParam("refund_useremail", $("#refund_useremail").val());
                                grid.setAjaxParam("refund_initiate_reason", $("#refund_initiate_reason").val());
                                grid.getDataTable().ajax.reload();
                                grid.clearAjaxParams();
                            }
                        });
                    }else if (action=='help-info') {
                        action = 'help';

                        $("#helpModal").modal();

                    }else if (action=='notify-customers' && grid.getSelectedRowsCount() > 0) {
                        action = 'notify';

                        $("#result").html('');
                        $("#reason-result").html('');
                        $("#notifyModal").modal();
                        var modalConfirm = function(callback){
                            $("#modal-btn-yes1").unbind().on("click", function(){
                                if($("#subject").val() == ""){
                                    $("#subject_result").html("Please Provide Subject");
                                }else if($("#notify_on").val() == ""){
                                    $("#notify_result").html("Please Provide Notify Date");
                                }else if($("#content").val() == ""){
                                    $("#content_result").html("Please Provide Content");
                                }else{
                                    callback(true);
                                    $("#subject_result").html('');
                                    $("#notify_result").html('');
                                    $("#content_result").html('');
                                    $("#notifyModal").modal('hide');
                                }
                            });
                        };
                        modalConfirm(function(confirm){
                            if(confirm){
                                // grid.setAjaxParam("customActionType", "group_action");
                                grid.setAjaxParam("customActionName", action);
                                grid.setAjaxParam("id", grid.getSelectedRows());
                                grid.setAjaxParam("subject", $("#subject").val());
                                grid.setAjaxParam("notify_on", $("#notify_on").val());
                                grid.setAjaxParam("content", $("#content").val());
                                if($("#select_all").is(':checked')){
                                grid.setAjaxParam("select_all", 1);}
                                grid.getDataTable().ajax.reload();
                                grid.clearAjaxParams();
                            }
                        });
                    } else if (action == "") {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'Please select an action',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    } else if (grid.getSelectedRowsCount() === 0) {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'No record selected',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    }
                });


                //change review publish status
                grid.getTableWrapper().on('click', '.table-group-action', function (e) {
                    e.preventDefault();

                    var action = $(this).attr('data-action');
                    if (action=='ReviewPublish' && grid.getSelectedRowsCount() > 0) {
                        action = 'ReviewPublish';

                        $("#result").html('');
                        $("#reason-result").html('');
                        $("#changeStatusModal").modal();
                        // dd('test');
                        var modalConfirm = function(callback){
                            $("#modal-btn-yes").unbind().on("click", function(){
                                if($("#status option:selected").val() == ""){
                                    $("#result").html("Please Select Status");
                                }else if($("#status").val() == 0 && $("#reason").val() == ''){
                                    $("#reason-result").html("Please enter reason for deactivation");
                                }else{
                                    callback(true);
                                    $("#result").html('');
                                    $("#reason-result").html('');
                                    $("#changeStatusModal").modal('hide');
                                }
                            });
                        };

                        modalConfirm(function(confirm){
                            if(confirm){
                                // grid.setAjaxParam("customActionType", "group_action");
                                grid.setAjaxParam("customActionName", action);
                                grid.setAjaxParam("id", grid.getSelectedRows());
                                grid.setAjaxParam("status", $("#status option:selected").val());
                                grid.setAjaxParam("inactive_reason", $("#inactive_reason").val());
                                grid.getDataTable().ajax.reload();
                                grid.clearAjaxParams();
                            }
                        });
                    } else if (action == "") {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'Please select an action',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    } else if (grid.getSelectedRowsCount() === 0) {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'No record selected',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    }
                });

                // handle group action change Prices submit button click
                grid.getTableWrapper().on('click', '.table-group-action-status', function (e) {
                    e.preventDefault();
                    var action = $(this).attr('data-action');
                    if (action=='change-price' && grid.getSelectedRowsCount() > 0) {
                        action = 'changeprice';

                        $("#result").html('');
                        $("#changePriceModal").modal();

                        $("#modal-btn-yes").unbind().on("click", function(){
                            if($("#discount_type option:selected").val() == ""){
                                $("#discount_type_result").html("Please Select Discount Type");
                            }else if($("#discount_amount").val() == ""){
                                $("#discount_amount_result").html("Please Select Amount");
                            }else if($("#start_date").val() == ""){
                                $("#start_date_result").html("Please Select Start Date");
                            }else if($("#end_date").val() == ""){
                                $("#end_date_result").html("Please Select End Date");
                            }else if(Date.parse($("#end_date").val()) <= Date.parse($("#start_date").val())){
                                $("#end_date_result").html("End Date Must Be After Start Date");
                            }else if(!$("#discount_amount").val().match(/^\d*(\.\d+)?$/)){
                                $("#discount_amount_result").html("Please Give Proper Value");
                            }else if($("#discount_type option:selected").val() == "p" && $("#discount_amount").val() >100){
                                $("#discount_amount_result").html("Discount amount must be less than 100");
                            }else{
                                grid.setAjaxParam("customActionName", action);
                                grid.setAjaxParam("ids", grid.getSelectedRows());
                                grid.setAjaxParam("discount_type", $("#discount_type option:selected").val());
                                grid.setAjaxParam("discount_amount", $("#discount_amount").val());
                                grid.setAjaxParam("start_date", $("#start_date").val());
                                grid.setAjaxParam("end_date", $("#end_date").val());
                                grid.getDataTable().ajax.reload();
                                grid.clearAjaxParams();

                                $("#changePriceModal").modal('hide');
                            }
                        });

                    } else if (action == "") {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'Please select an action',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    } else if (grid.getSelectedRowsCount() === 0) {
                        App.alert({
                            type: 'danger',
                            icon: 'warning',
                            message: 'No record selected',
                            container: grid.getTableWrapper(),
                            place: 'prepend'
                        });
                    }
                });

                // handle datatable custom tools
                $('#dt_listing_tools > a.tool-action').on('click', function() {
                    var action = $(this).attr('data-action');
                    grid.getDataTable().button(action).trigger();
                });

                @endif

            });
        </script>
        @stack('PAGE_SCRIPTS')
        <!-- END PAGE LEVEL SCRIPTS -->

        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ __common_asset('layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ __common_asset('layouts/layout/scripts/demo.min.js') }}" type="text/javascript"></script>
        <script src="{{ __common_asset('layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ __common_asset('layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript"></script>
        <script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>
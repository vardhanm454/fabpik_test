@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <!-- FILTER FORM START -->
                <form action="#" method="get" class="filter-form">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label col-md-2 pd-tb-5"><strong>Module Name:</strong></label>
                                            <div class="col-md-9">
                                                <select name="module_name" id="module_name"
                                                    class="form-control form-filter input-sm select2">
                                                    <option value="">Select Seller</option>
                                                    @if(isset($permissions))
                                                    @foreach($permissions as $permission)
                                                    <option value="{{$permission->module_name}}"
                                                    {{request()->module_name==$permission->module_name?'selected="selected"':''}}>
                                                    {{$permission->module_name}}
                                                    </option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label col-md-2 pd-tb-5"><strong>Permission Name:</strong></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control input-sm" name="name" id="name" value="{{request()->name?request()->name:''}}" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="control-label col-md-4 pd-tb-5"><strong>Action:</strong></label>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span> </button>
                                                <a class="btn btn-sm btn-icon-only white" href="{{route('admin.permissions')}}" title="Reset Filter"><span aria-hidden="true" class="icon-refresh"></span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        
                    </div>

                    <table class="table table-checkable nowrap" id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th class="text-center"> Id </th>
                                <th class="text-center"> Module Name </th>                           
                                <th class="text-center"> Permission Name </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Permissions = function () {

    return {

        //main function to initiate the module
        init: function () {

            // Do Filter
            $('#btn_submit_search').on('click', function(e){
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.permissions')}}?search=1";

                if($('#name').val() != '') url += "&name="+encodeURIComponent($('#name').val());
                if($('#module_name').val() != '') url += "&module_name="+encodeURIComponent($('#module_name').val());


                window.location.href = url;
            }); 

        }

    };

}();

jQuery(document).ready(function() {    
   Permissions.init();
});

</script>
@endpush
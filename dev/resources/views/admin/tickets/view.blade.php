@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('apps/css/ticket.min.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<div class="app-ticket app-ticket-details">
   <div class="row">
       <div class="col-md-12">
           <div class="portlet light portlet-fit portlet-datatable bordered">
               <div class="portlet-title">
                   <div class="caption col-md-12">
                       <div class="row">
                            <div class="col-md-8">
                                <div class="ticket-id bold">
                                    #{{$ticket_details->ticket_no}} -
                                    <span class="ticket-title bold uppercase"> {{$ticket_details->issue_title}} </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="ticket-id">
                                    <div class="col-md-3">
                                        <a href="{{route('admin.tickets.closeTicket', ['id'=>$ticket_details->id])}}" id="btn_close" class="btn blue form-submit" {{($ticket_details->issue_status == 2)?'disabled':''}}> Close</a>   
                                    </div>
                                    <div class="col-md-9">
                                        <small> <b> Raised Date:</b> {{date('d/m/Y h:i A', strtotime($ticket_details->created_at))}} </small><br>
                                        <small> <b> Raised By:</b> {{($ticket_details->seller) ? $ticket_details->seller->company_name : '-'}} </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
               <div class="portlet-body">
                   <div class="row">
                       <div class="col-xs-12">
                           <div class="ticket-msg bold">
                              <p> Issue: {{$ticket_details->issue}} -  @if($ticket_details->issue_status == 2) Closed at: {{date('d/m/yy h:i A', strtotime($ticket_details->updated_at))}}  @endif </p><br>
                           </div>
                       </div>
                   </div>
                   <?php if($ticket_details){ ?>
                   <div class="portlet light bordered">
                     <div id="chats">
                         <div class="scroller" style="height: auto;" data-always-visible="1" data-rail-visible1="1">
                         <ul class="chats">
                              @foreach($ticket_details->comments as $ticket_comment)
                              <li class="{{(auth()->user()->id==$ticket_comment->comment_by)? 'in' : 'out'}} pd-tb-5">
                                     <div class="message pd-tb-5">
                                         <!-- <span class="arrow"> </span> -->
                                         <a href="javascript:;" class="name"> <strong>{{ ($ticket_comment->user) ? $ticket_comment->user->name : '' }}</strong> </a>
                                         <span class="datetime"> at {{date('h:i A', strtotime($ticket_comment->created_at))}} </span>
                                         <span class="body"> {{$ticket_comment->comments}} </span>
                                        <?php if(!empty($ticket_comment->attachment)){ ?>
                                         <span><img src="<?php echo URL::to('/').'/'.UPLOAD_PATH.'/support_ticket/'.$ticket_comment->attachment; ?>" width="200px" height="200px"></span>
                                        <?php } ?>
                                     </div>
                                 </li>
                              @endforeach
                             </ul>
                         </div><hr>
                         <?php } ?>
                         <form  id="ticket_comment_form" role="form" method="post" action="{{route('admin.tickets.postComment', ['id'=>$ticket_details->id])}}" enctype="multipart/form-data">
                            @csrf
                            <div class="ticket-chat chat-form bg-light">
                            <div class="col-md-11 pd-tb-0 pd-lr-5">
                                <div class="input-cont {{($errors->first('comments'))?'has-error':''}}">
                                    <input class="form-control" type="text" name="comments" placeholder="Type a message here..." 
                                    value="{{old('comments')}}"/>
                                    <span class="help-block">{{$errors->first('comments')}}</span>
                                </div>
                                    <div class="btn-cont {{($errors->first('attachment'))?'has-error':''}}">
                                        <!-- <span class="arrow"> </span> -->
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn blue btn-file">
                                            <span class="fileinput-new"> <i class="fa fa-paperclip" aria-hidden="true"></i> Attachment File </span>
                                            <span class="fileinput-exists"> <i class="fa fa-paperclip" aria-hidden="true"></i> </span>
                                            <input type="file" name="attachment" id="selAttachmentReply"> 
                                        </span>
                                        <span class="help-block">{{$errors->first('attachment')}}</span>
                                            <!-- <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a> -->
                                            <!-- <span class="ticket-attachment fileinput-filename"> </span> -->
                                        </div>
                                    </div>
                            </div> 
                            <div class="col-md-1 pd-tb-5 pd-lr-0">
                                <button type="submit" id="btn_back" class="btn btn-default btn-outline">
                                    <span aria-hidden="true" class="icon-arrow-left text-primary"></span>  Reply
                                </button>                                   
                                </div>
                            </div>
                         </form>
                     </div>
                     </div>       
               </div>
           </div>
       </div>
   </div>

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('pages/scripts/profile.min.js')}}" type="text/javascript"></script>
<script src="{{ __common_asset('pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">

</script>
@endpush
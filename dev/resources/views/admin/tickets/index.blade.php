@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption pd-tb-5">
                    <span>Support Tickets</span>
                </div>
                <!-- <div class="actions pd-tb-5">
                    <div class="btn-group">
                        <button id="sample_editable_1_new" class="btn btn-default btn-sm tooltips">
                        <strong><span aria-hidden="true" class="icon-plus text-primary"></span> Raise an Issue</strong>                            
                        </button>
                    </div>
                </div> -->
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <table class="table table-checkable nowrap" id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                {{-- <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th> --}}
                                <th class=""> ID </th>
                                <th class=""> Created Date </th>
                                <th class=""> Modified Date </th>
                                <th class=""> Seller </th>
                                <th class=""> Subject </th>
                                <th class=""> Category </th>
                                <th class=""> Attachment </th>
                                <th class=""> Status </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var SubCategories = function() {

    return {

        //main function to initiate the module
        init: function() {

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.subcategories')}}?search=1";

                if ($('#fname').val() != '') url += "&fname=" + encodeURIComponent($('#fname').val());
                if ($('#fstatus').val() != '') url += "&fstatus=" + encodeURIComponent($('#fstatus')
                    .val());

                window.location.href = url;
            });

            // Export all subcategories data
            $('#btn_table_export').on('click', function(e) {
                e.preventDefault();

                var url = "{{route('admin.subcategories.export')}}?export=1";

                if ($('#fname').val() != '') url += "&fname=" + encodeURIComponent($('#fname').val());
                if ($('#fstatus').val() != '') url += "&fstatus=" + encodeURIComponent($('#fstatus')
                    .val());

                window.location.href = url;
            });

            // Delete Subcategory
            $('body').on('click', '.dt-list-delete', function(event) {
                event.preventDefault();
                var url = $(this).attr('del-url');
                swal({
                        title: 'Are you sure?',
                        text: 'You want Delete the record.',
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonClass: 'btn-success',
                        cancelButtonClass: 'btn-danger',
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: 'post',
                                success: function(result) {
                                    if (result.success == 1) {
                                        swal('SubCategory',
                                            'Record Deleted Successfully', "success"
                                        );
                                        location.reload();
                                    } else {
                                        swal("SubCategory",
                                            "Error! Some error occured, please try again.",
                                            "error");
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
            });
        }

    };

}();

jQuery(document).ready(function() {
    SubCategories.init();
});
</script>
@endpush
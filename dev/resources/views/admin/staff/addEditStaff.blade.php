@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<style>
    .role-list {
    margin-bottom: 25px;
    }
    .role-list>label {
    display: block;
    margin-bottom: 8px;
    }
</style>
@endpush

@section('content')

<div class="row categpries-wrap">
    <div class="col-md-9">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    <form class="form-horizontal form-row-seperated" action="{{(isset($staff))?route('admin.staff.edit',['id'=>$id]):route('admin.staff.create')}}" method="post">

                        {!! csrf_field() !!}

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('name'))?'has-error':''}}">
                                                <label class="control-label">Name:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="name" maxlength="100" placeholder="" value="{{old('name',isset($staff)?$staff->name:'')}}">
                                                    <span class="help-block">{{$errors->first('name')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('email'))?'has-error':''}}">
                                                <label class="control-label">Email ID:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="email" maxlength="100" placeholder="" value="{{old('email',isset($staff)?$staff->email:'')}}">
                                                    <span class="help-block">{{$errors->first('email')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('mobile_no'))?'has-error':''}}">
                                                <label class="control-label">Mobile No.:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="mobile_no" maxlength="10" placeholder="" value="{{old('mobile_no',isset($staff)?$staff->mobile_no:'')}}">
                                                    <span class="help-block">{{$errors->first('mobile_no')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('status'))?'has-error':''}}">
                                                <label class="control-label">Status:<span class="required"> * </span></label>
                                                <div class="">
                                                <select name="status" id="status" class="form-control input-sm">
                                                    <option value="1" {{request()->status=='1'?'selected="selected"':''}}>
                                                        Active
                                                    </option>
                                                    <option value="0" {{request()->status=='0'?'selected="selected"':''}}>
                                                        Inactive</option>
                                                </select>
                                                    <span class="help-block">{{$errors->first('status')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h4 class="form-section">Roles</h4>
                                    <div class="role-list">
                                        @foreach($roles as $role)
                                        <label><input type="checkbox" class="icheck" name="roles[]" value="{{$role->id}}" {{in_array($role->id, old('roles', (isset($staff))?$assignedRoles:[]))?'checked':''}}> {{$role->name}} </label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Submit</button>

                            @if(isset($staff))
                            <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Submit &amp; Continue Edit</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var AddEditStaff = function () {
    return {

        //main function to initiate the module
        init: function () {
            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.staff')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });
        }
    };
}();

jQuery(document).ready(function() {
    AddEditStaff.init();
});

</script>
@endpush
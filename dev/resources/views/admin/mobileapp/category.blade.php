@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link rel="stylesheet" type="text/css"  href="{{ __common_asset('global/css/ion.rangeSlider.css') }}" ></link>
<link rel="stylesheet" type="text/css"  href="{{ __common_asset('global/css/ion.rangeSlider.skinFlat.css') }}" ></link>
    
<style>
.mobileapp-section li
{
    display: inline-block;
    padding: 10px 0px;
   
}
.mobileapp-section li a
{
    padding:10px 10px;
    color:#000;
}
.mobileapp-section li a.active
{
    border-bottom: 1px solid #337ab7;
    color: #337ab7;
}
.mobileapp-section ul
{
    padding-left: 1px;
    margin: 0;
    margin-left: -5px;
    border-bottom: 1px solid #ccc;
    background: #fff;
    padding: 0px 0px;
    margin-top: -10px;
}
.mobileapp-section .portlet-title h3
{
    margin: 0;
    font-size: 15px;
    font-weight: 400;
    vertical-align: middle;
    align-items: center;
}
.mobileapp-section .portlet
{
margin-top: 29px!important;
margin-bottom: 0px!important;
}


.d-flex
{
    display:flex
}
.control-label
{
    padding-right:12px;
}
.file-style2 {
     display: grid;
    margin: 0;
    position: relative;
}
.file-style2 .custom-file-input {
    position: relative;
    z-index: 1;
    width: 100px;
    height: 110px;
    margin: 0;
    overflow: hidden;
    opacity: 1;
    padding: 0;
    
}
.file-style2.main-filestyle2 .custom-file-input {
  
    opacity: 0;
    z-index: 999;
}
.fileUpload2 {
    display: none;
}
.file-style2 label {
    margin-bottom: -3px;
}
.file-style2 .custom-file-input::before {
    content: "";
    display: inline-block;
    background: #0000;
    border-radius: 3px;
    padding: 28px 21px;
    outline: none;
    white-space: nowrap;
    -webkit-user-select: none;
    cursor: pointer;
    font-weight: 700;
    font-size: 15px;
    color: #000;
    width: 100px;
    height: 100px;
    border: 1.5px solid #aab7b8;
    border-style: dashed;
    background-color: #fafafa;
    flex: 0 0 auto;
    background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNTdweCIgaGVpZ2h0PSI1MnB4IiB2aWV3Qm94PSIwIDAgNTcgNTIiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDUyLjYgKDY3NDkxKSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5Hcm91cCA1PC90aXRsZT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPgogICAgPGRlZnM+CiAgICAgICAgPHBhdGggZD0iTTM0LjUsNi45NTY3OTAxMiBMNDEuMzg4NzcyNSw2Ljk1Njc5MDEyIEM0My45MzU0ODMxLDYuOTU2NzkwMTIgNDYsOS4wMjg1MTYwMyA0NiwxMS41NzU4MzAyIEw0NiwzNy4xMjE3MDA3IEM0NiwzOS42NzI3MjYxIDQzLjkzNDExMTUsNDEuNzQwNzQwNyA0MS40MjExNzEyLDQxLjc0MDc0MDcgTDQuNTc4ODI4ODEsNDEuNzQwNzQwNyBDMi4wNTAwMTE0OSw0MS43NDA3NDA3IDAsMzkuNjY5MDE0OCAwLDM3LjEyMTcwMDcgTDAsMTEuNTc1ODMwMiBDMCw5LjAyNDgwNDgxIDIuMDY5NjgwMjcsNi45NTY3OTAxMiA0LjU5Mzg3NzcyLDYuOTU2NzkwMTIgTDExLjg4MTk2NDUsNi45NTY3OTAxMiBMMTguNCwwIEwyNy42LDAgTDM0LjUsNi45NTY3OTAxMiBaIiBpZD0icGF0aC0xIj48L3BhdGg+CiAgICA8L2RlZnM+CiAgICA8ZyBpZD0iUllQLUNhcmRpZmllZC1SZWRsaW5lcyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPGcgaWQ9ImV4cGwtY29weS0yNDgiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xNjEuMDAwMDAwLCAtMTM3LjAwMDAwMCkiPgogICAgICAgICAgICA8ZyBpZD0iR3JvdXAtQ29weS0zIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjAwMDAwMCwgMTAuMDAwMDAwKSI+CiAgICAgICAgICAgICAgICA8ZyBpZD0iR3JvdXAtMiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTUuMDAwMDAwLCA5OS4wMDAwMDApIj4KICAgICAgICAgICAgICAgICAgICA8ZyBpZD0iR3JvdXAtNiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoOTAuMDAwMDAwLCAyOC4wMDAwMDApIj4KICAgICAgICAgICAgICAgICAgICAgICAgPGcgaWQ9Ikdyb3VwLTMiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPGcgaWQ9Ikdyb3VwLTUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDU2LjAwMDAwMCwgMC4wMDAwMDApIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZyBpZD0iR3JvdXAtNCI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxtYXNrIGlkPSJtYXNrLTIiIGZpbGw9IndoaXRlIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1c2UgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbWFzaz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGcgaWQ9IlJlY3RhbmdsZSI+PC9nPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cG9seWdvbiBpZD0iUmVjdGFuZ2xlLTMiIGZpbGw9IiNCN0M1QzYiIG1hc2s9InVybCgjbWFzay0yKSIgcG9pbnRzPSItMy42NTA3OTM2NSAtMi45MjkxNzQ3OSA1Mi41NzE0Mjg2IC0yLjkyOTE3NDc5IDUyLjU3MTQyODYgNDUuNDAyMjA5MiAtMy42NTA3OTM2NSA0NS40MDIyMDkyIj48L3BvbHlnb24+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxlbGxpcHNlIGlkPSJPdmFsIiBzdHJva2U9IiNGQUZBRkEiIHN0cm9rZS13aWR0aD0iNCIgbWFzaz0idXJsKCNtYXNrLTIpIiBjeD0iMjMiIGN5PSIyMy40MzMzOTgzIiByeD0iOS4zMTc0NjAzMiIgcnk9IjkuNzE2Njk5MTYiPjwvZWxsaXBzZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHBhdGggZD0iTTQzLjksMzguOSBMNDMuOSwzOS4xIEw0NCwzOS4xIEw0NC4xLDM5LjEgTDQ0LjEsMzguOSBMNDMuOSwzOC45IFogTTQ2LjksMzYuMSBMNTIuNiwzNi4xIEM1NC4yMDE2MjU4LDM2LjEgNTUuNSwzNy4zOTgzNzQyIDU1LjUsMzkgQzU1LjUsNDAuNjAxNjI1OCA1NC4yMDE2MjU4LDQxLjkgNTIuNiw0MS45IEw0Ni45LDQxLjkgTDQ2LjksNDcuNiBDNDYuOSw0OS4yMDE2MjU4IDQ1LjYwMTYyNTgsNTAuNSA0NCw1MC41IEM0Mi4zOTgzNzQyLDUwLjUgNDEuMSw0OS4yMDE2MjU4IDQxLjEsNDcuNiBMNDEuMSw0MS45IEwzNS40LDQxLjkgQzMzLjc5ODM3NDIsNDEuOSAzMi41LDQwLjYwMTYyNTggMzIuNSwzOSBDMzIuNSwzNy4zOTgzNzQyIDMzLjc5ODM3NDIsMzYuMSAzNS40LDM2LjEgTDQxLjEsMzYuMSBMNDEuMSwzMC40IEM0MS4xLDI4Ljc5ODM3NDIgNDIuMzk4Mzc0MiwyNy41IDQ0LDI3LjUgQzQ1LjYwMTYyNTgsMjcuNSA0Ni45LDI4Ljc5ODM3NDIgNDYuOSwzMC40IEw0Ni45LDM2LjEgWiIgaWQ9IlNoYXBlIiBzdHJva2U9IiNGQUZBRkEiIHN0cm9rZS13aWR0aD0iMyIgZmlsbD0iI0FBQjdCOCIgZmlsbC1ydWxlPSJub256ZXJvIj48L3BhdGg+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgIDwvZz4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPg==);
    background-size: 26px 26px;
    background-repeat: no-repeat;
    background-position: center;
}
.docErr {
    color: red;
}

.docErr {
    display: none;
}
.docErr {
    color: red;
    display: none;
}
.file-style2 .custom-file-input {
    color: transparent;
}
.file-style2 .fileUpload2.displayimg {
    display: block;
    position: absolute;
    margin: 0;
    /* top: -19px; */
    /* right: -34%; */
    border-radius: 20px;
    overflow: hidden;
    /* width: 112px; */
    /* height: 120px; */
    width: 100px;
    height: 101px;
    z-index: 10;
}
.img-block {
    margin-bottom: 20px;
}
.float-right
{
    float:right;
}
.fileUpload2.displayimg {
    display: block;
    position: absolute;
    margin: 0;
    border-radius: 20px;
    overflow: hidden;
    width: 100px;
    height: 101px;
    z-index: 10;
}
.file-style2 .label-text
{
    display: none;
}
.main-filestyle2 .label-text
{
    display: block;
    color: #337ab7;
    font-size: 12px;
}
.jusify-center-content
{

    justify-conetnt:center
}
.mobileapp-section  .portlet-title button
{

}.pt-0
{
    padding-top: 0!important;
}
.pb-0
{
    padding-bottom: 0!important;
}
.append-input
{
    width: 20px;
    border-left: 0;
    border-right: 0;
    margin: 0;
    background: #d3d3d3;
}
.p-a20
{
    padding:20px;
}

#select2-check-boxdiv-results .select2-results__option:before {
    content: "";
    display: inline-block;
    position: relative;
    height: 20px;
    width: 20px;
    border: 2px solid #e9e9e9;
    border-radius: 4px;
    background-color: #fff;
    margin-right: 20px;
    vertical-align: middle;
}

#select2-check-boxdiv-results .select2-results__option[aria-selected=true]:before {
    font-family: fontAwesome;
    content: "\f00c";
    color: #fff;
    background-color: #f77750;
    border: 0;
    display: inline-block;
    padding-left: 3px;
}

#select2-check-boxdiv-results .select2-container--default .select2-results__option[aria-selected=true] {
    background-color: #fff;
}

#select2-check-boxdiv-results .select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: #eaeaeb;
    color: #272727;
}

#select2-check-boxdiv-results  .select2-container--default .select2-selection--multiple {
    margin-bottom: 10px;
}

#select2-check-boxdiv-results .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
    border-radius: 4px;
}

#select2-check-boxdiv-results .select2-container--default.select2-container--focus .select2-selection--multiple {
    border-color: #f77750;
    border-width: 2px;
}

#select2-check-boxdiv-results .select2-container--default .select2-selection--multiple {
    border-width: 2px;
}

#select2-check-boxdiv-results .select2-container--open .select2-dropdown--below {

    border-radius: 6px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);

}
.icon-block
{
    float: right;
}
 
.img-block img
{
    max-width: 100%;
    height: 100px;
    width: 100px;
    background: #fff !important;
    background-clip: border-box;
    border-radius: 13px !important;
    border: 0 solid #edeef7 !important;
    margin-bottom: 1.3rem;
    box-shadow: 0 5px 15px 5px rgb(80 102 224 / 8%);
}

.img-block i
{
    display: initial;
    padding: 6px 6px;
    cursor: pointer;

}
.img-block a i.icon-note
{
    background: #337ab7;
    color: #fff;    border-radius: 4px;
    font-size: 11px;
    text-align: center;
}
.img-block i.icon-trash
{
    background: #ff0000ad;
    color: #fff;    border-radius: 4px;
    font-size: 11px;
    text-align: center;
}
a:hover
{
    text-decoration:none;
}
</style>
@endpush

@section('content')
<div class="mobileapp-section">
    <div class="row">
        <!-- Begin: life time stats -->
        <ul>
        <li ><a href="{{route('admin.home')}}" >Home Page</a></li>
            <li ><a href="{{route('admin.category')}}" class="active">Category Page</a></li>
            <li><a href="{{route('admin.offers')}}">Offers Page</a></li>
        </ul>
        <!-- End: life time stats -->
       
        <div class="col-lg-8 col-md-8 col-md-offset-2 col-lg-offset-2">
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title d-flex">
                    <div class="col-lg-6 col-md-6 d-flex">
                        <h3 class="d-flex">Exclusive Launch Banners</h3>
                    </div>
                    <div class="col-lg-6 col-md-6 "> <button class="default-btn btn-primary btn float-right" id="exAddBanner"> Add </button> </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                    @if(count($exclusives) >0)
                        @foreach($exclusives as $exclusiv)
                            <div class="col-lg-2 col-md-2 img-block">
                                <img src='{{url("uploads/mobileAppSliders/$exclusiv->image")}}'>
                                <div class="icon-block">
                                    <a data-toggle="modal" rel="{{$exclusiv->id}}" rel1="c" class="exclusiveBannerId"><i class="icon-note"></i> </a>
                                    <a href="javascript:void(0)" class="dt-list-delete deleteCategoryBannerId" rel="{{$exclusiv->id}}" rel1="c"> <i class="icon-trash "></i> </a>
                                </div>
                            </div>
                        @endforeach
                       @else
                       <h5 class="text-danger" style="margin-left: 2rem; font-weight:bolder">No Exclusive Launch Banners Found</h5>
                       @endif
                    </div>
                </div>
            </div>
        </div>
      
    </div>
</div>
<!-- ADD MODEL -->
<div class="modal fade addBanner" id="addBanner" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Exclusive Banner</h4>
            </div>
            <form class="AddBannerForm" method="post" enctype="multipart/form-data">
            <div class="row p-a20">
                <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                        <div class="">
                            <label class="control-label">Title</label>
                            <div class="">
                                <input type="text" class="form-control maxlength-handler" name="title" placeholder="" value="">
                                <span class="title-error text-danger" style="margin-top: 5px !important;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="">
                            <label class="control-label">Url</label>
                            <div class="">
                                <input type="text" class="form-control maxlength-handler" name="url" placeholder="" value="">
                                <span class="url-error text-danger" style="margin-top: 5px !important;"></span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-6 col-md-6">
                <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label class="">Image:<span class="required"> * </span></label>
                            </div>
                            <div class="col-lg-9">
                            <div class="file-style2">
                                <input type="file" name="image" class="custom-file-input" multiple="">
                                <img src="https://image.flaticon.com/icons/svg/136/136549.svg" class="icon fileUpload2" width="100">
                                <span class="image-error text-danger" style="margin-top: 5px !important;"></span>
                                <label class="label-text">if you want to change click on that image</label>
                            </div>
                            </div>
                        </div>
                      
                    </div>
                  </div>
            </div>
                <div class="row p-a20 pt-0">
                <div class="col-lg-6 col-md-6 check-boxdiv">
                <div class="form-group ">
                        <label class="control-label">Category:</label>
                        <select class="form-control form-filter input-sm select2 categoryDropDown" name="category[]" id="check-boxdiv" multiple="multiple">
                            
                        </select>
                        <span class="category-error text-danger" style="margin-top: 5px !important;"></span>
                    </div> </div>
                <div class="col-lg-6 col-md-6 check-boxdiv">
                    <div class="form-group">
                        <label class="control-label">Brands:</label>
                        <select class="form-control form-filter input-sm select2 brandsDropDown" name="brand[]" id="check-boxdiv" multiple="multiple">
                            
                        </select>
                        <span class="brand-error text-danger" style="margin-top: 5px !important;"></span>
                    </div>
                </div>
               </div>
                <div class="row p-a20 pt-0">
                <div class="col-lg-6 col-md-6 ">
                    <div class="form-group">
                        <label class="control-label">Coupons:</label>
                        <select class="form-control form-filter input-sm select2 coupansDropDown" name="coupan[]">
                            
                        </select>
                        <span class="coupan-error text-danger" style="margin-top: 5px !important;"></span>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 form-group">
                <label class="control-label">Price Range:</label>
                    <div class="d-flex">
                                <input type="text" class="form-control maxlength-handler" name="minPrice" placeholder="Min" value="" id="minPrice">
                                <span class="append-input form-control">-</span>
                                <input type="text" class="form-control maxlength-handler" name="maxPrice" placeholder="Max" value="" id="maxPrice">
                    </div>
                    <span class="minprice-error text-danger" style="margin-top: 5px !important;"></span>
                    <span class="maxprice-error text-danger" style="margin-top: 5px !important;"></span>
                </div>
                <div class="col-lg-6 col-md-6">
                <label class="control-label">Discount Range</label>
                    <div class="d-flex">
                                <input type="text" class="form-control maxlength-handler" name="minDiscount" placeholder="Min" value="" id="minDiscount">
                                <span class="append-input form-control">-</span>
                                <input type="text" class="form-control maxlength-handler" name="maxDiscount" placeholder="Max" value="" id="maxDiscount">
                    </div>
                    <span class="mindiscount-error text-danger" style="margin-top: 5px !important;"></span>
                    <span class="maxdiscount-error text-danger" style="margin-top: 5px !important;"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </form>
       
        </div>
    </div>
</div>

<!-- EDIT MODEL -->
<div class="modal fade editBanner" id="editBanner" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Exclusive Banner</h4>
            </div>
            <form class="UpdateBannerForm" method="post" enctype="multipart/form-data">
            <div class="row p-a20">
                <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                        <div class="">
                            <label class="control-label">Title</label>
                            <div class="">
                                <input type="text" class="form-control maxlength-handler title" name="title" placeholder="" value="">
                                <input type="hidden" name="banner_id" class="banner_id">
                                <input type="hidden" name="panel" value="exclusive">
                                <span class="title-error text-danger" style="margin-top: 5px !important;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="">
                            <label class="control-label">Url</label>
                            <div class="">
                                <input type="text" class="form-control maxlength-handler url" name="url" placeholder="" value="">
                                <span class="url-error text-danger" style="margin-top: 5px !important;"></span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-6 col-md-6">
                <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label class="">Image:<span class="required"> * </span></label>
                            </div>
                            <div class="col-lg-9">
                            <div class="file-style2">
                                <input type="file" name="image" class="custom-file-input" multiple="">
                                <img src="https://image.flaticon.com/icons/svg/136/136549.svg" class="icon fileUpload2" width="100">
                                <span class="image-error text-danger" style="margin-top: 5px !important;"></span>
                                <label class="label-text">if you want to change click on that image</label>
                                <img src="" class="icon editimage" type="image/webp" style="display: none; width:52%; margin-top:-1rem">
                            </div>
                            </div>
                        </div>
                      
                    </div>
                  </div>
            </div>
                <div class="row p-a20 pt-0">
                <div class="col-lg-6 col-md-6 check-boxdiv">
                <div class="form-group ">
                        <label class="control-label">Category:</label>
                        <select class="form-control form-filter input-sm select2 categoryDropDown" name="category[]" id="check-boxdiv" multiple="multiple">
                            
                        </select>
                        <span class="category-error text-danger" style="margin-top: 5px !important;"></span>
                    </div> </div>
                <div class="col-lg-6 col-md-6 check-boxdiv">
                    <div class="form-group">
                        <label class="control-label">Brands:</label>
                        <select class="form-control form-filter input-sm select2 brandsDropDown" name="brand[]" id="check-boxdiv" multiple="multiple">
                            
                        </select>
                        <span class="brand-error text-danger" style="margin-top: 5px !important;"></span>
                    </div>
                </div>
               </div>
                <div class="row p-a20 pt-0">
                <div class="col-lg-6 col-md-6 ">
                    <div class="form-group">
                        <label class="control-label">Coupons:</label>
                        <select class="form-control form-filter input-sm select2 coupansDropDown" name="coupan[]">
                            
                        </select>
                        <span class="coupan-error text-danger" style="margin-top: 5px !important;"></span>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 form-group">
                <label class="control-label">Price Range:</label>
                    <div class="d-flex">
                                <input type="text" class="form-control maxlength-handler minPrice" name="minPrice" placeholder="Min" value="" id="minPrice">
                                <span class="append-input form-control">-</span>
                                <input type="text" class="form-control maxlength-handler maxPrice" name="maxPrice" placeholder="Max" value="" id="maxPrice">
                    </div>
                    <span class="minprice-error text-danger" style="margin-top: 5px !important;"></span>
                    <span class="maxprice-error text-danger" style="margin-top: 5px !important;"></span>
                </div>
                <div class="col-lg-6 col-md-6">
                <label class="control-label">Discount Range</label>
                    <div class="d-flex">
                                <input type="text" class="form-control maxlength-handler minDiscount" name="minDiscount" placeholder="Min" value="" id="minDiscount">
                                <span class="append-input form-control">-</span>
                                <input type="text" class="form-control maxlength-handler maxDiscount" name="maxDiscount" placeholder="Max" value="" id="maxDiscount">
                    </div>
                    <span class="mindiscount-error text-danger" style="margin-top: 5px !important;"></span>
                    <span class="maxdiscount-error text-danger" style="margin-top: 5px !important;"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Update</button> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </form>
       
        </div>
    </div>
</div>



@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
 $(document).ready(function()
  {
        $('.file-style2 .custom-file-input').on('change', function(e) {
            var v = $(this),
                o = $(v.parents(".file-style2"));
            var extension = e.target.files[0].name.split('.').pop().toLowerCase()
            var reader = new FileReader();
            reader.onload = function(e) { 
                if (extension == 'jpg' || extension == 'jpeg' || extension == 'png' || extension == 'webp') {
                    $(o).children('.fileUpload2').addClass("displayimg")
                    $(o).addClass("main-filestyle2")
                    $(o).children('.docErr').removeClass("content-display")
                    $(o).children('.fileUpload2').attr('src', reader.result);
                    $(o).children('.fa-times').css('display', 'block');
                } else {
                    $(o).children('.docErr').addClass("content-display")
                    $(o).children('.fileUpload2').removeClass("displayimg")
                }
            }
            reader.readAsDataURL(e.target.files[0]);
        });


        // ON KEYUP ONLY ALLOW NUMBERS
        $('#minPrice').keypress(function(event){

        if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
            event.preventDefault(); //stop character from entering input
        }

        });
        $('#maxPrice').keypress(function(event){

        if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
            event.preventDefault(); //stop character from entering input
        }

        });
        $('#minDiscount').keypress(function(event){

        if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
            event.preventDefault(); //stop character from entering input
        }

        });
        $('#maxDiscount').keypress(function(event){

        if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
            event.preventDefault(); //stop character from entering input
        }

        });


        // REMOVE ERROS
        function removeErrorsFunction() {

            $( '.title-error' ).html('');
            $( '.image-error' ).html('');
            $( '.category-error' ).html('');
            $( '.brand-error' ).html('');
            $( '.coupan-error' ).html('');
            $( '.minprice-error' ).html('');
            $( '.maxprice-error' ).html('');
            $( '.mindiscount-error' ).html('');
            $( '.maxdiscount-error' ).html('');

        }

            //remove existing image
            function removeExistingImage(){
                
                var v = $('.file-style2 .custom-file-input');
                var o = $(v.parents(".file-style2"));
                $(o).removeClass("main-filestyle2");
                $(o).children('.icon').addClass("displayimg");
                $('.fileUpload2').removeClass("displayimg");
                $("#imgBannerimg").removeAttr("src");
                
            }

        // GET DYNAMIC DATA OF CATEGORIES BRANDS COUPNAS FOR ADD BANNER IN MIDDLE BANNER
        $('#exAddBanner').on('click',function(){
            $('.addBanner').modal('show');
            removeErrorsFunction();
            removeExistingImage();
            $('.AddBannerForm')[0].reset();
            $.ajax({
                url:'{{route("admin.dynamicData")}}',
                type:'POST',
                success:function(data)
                {
                  if(data.status == 200)
                  {
                      $('.categoryDropDown').html(data.categories);
                      $('.brandsDropDown').html(data.brands);
                      $('.coupansDropDown').html(data.coupans);
                  }
                }
            })
        });

        

        // EXCLUSIVE BANNER FORM SUBMIT
        $('.AddBannerForm').submit(function(event){
            event.preventDefault();
            removeErrorsFunction();
            $.ajax({

                url:'{{route("admin.category")}}',
                type:'POST',
                data:new FormData(this),
                contentType:false,
                processData:false,
                success:function(data) {

                    if(data.errors) {

                        if(data.errors.title){
                           $( '.title-error' ).html( data.errors.title[0] );
                        }
                        if(data.errors.image){
                           $( '.image-error' ).html( data.errors.image[0] );
                        }
                        if(data.errors.url){
                           $( '.url-error' ).html( data.errors.url[0] );
                        }
                        if(data.errors.category){
                           $( '.category-error' ).html( data.errors.category[0] );
                        }
                        if(data.errors.brand){
                           $( '.brand-error' ).html( data.errors.brand[0] );
                        }
                        if(data.errors.coupan){
                           $( '.coupan-error' ).html( data.errors.coupan[0] );
                        }
                        if(data.errors.minPrice){
                           $( '.minprice-error' ).html( data.errors.minPrice[0] );
                        }
                        if(data.errors.maxPrice){
                           $( '.maxprice-error' ).html( data.errors.maxPrice[0] );
                        }
                        if(data.errors.minDiscount){
                           $( '.mindiscount-error' ).html( data.errors.minDiscount[0] );
                        }
                        if(data.errors.maxDiscount){
                           $( '.maxdiscount-error' ).html( data.errors.maxDiscount[0] );
                        }

                    }

                    if(data.status == 200) {
                        removeErrorsFunction();
                        $('.addBanner').modal('hide');
                        $('.AddBannerForm')[0].reset();
                        toastr.success( data.message);
                        setTimeout(function(){ location.reload(); }, 1000);   
                    }
                    if(data.status == 201) {
                        removeErrorsFunction()
                        toastr.error(data.message);
                        setTimeout(function(){ location.reload(); }, 3000);  
                    }

                }

            });
        });


        //EDIT EXCLUSIVE BANNER
        $('body').on('click', '.exclusiveBannerId', function(event){
            var execlusiveBanneId = $(this).attr('rel');
            var page = $(this).attr('rel1');
            removeErrorsFunction();
            removeExistingImage();
            $('.AddBannerForm')[0].reset();
            $('.editBanner').modal('show');

            $.ajax({

                url:'{{route("admin.editBanner")}}',
                type:'POST',
                data:{id:execlusiveBanneId,page:page},
                success:function(data)
                {
                    if(data.status == 200)
                    {
                       $('.title').val(data.title);
                       $('.url').val(data.url);

                       $('.editimage').attr('src', data.image);
                       $('.editimage').css('display', 'block');

                       $('.categoryDropDown').html(data.categories);
                       $('.brandsDropDown').html(data.brands);
                       $('.coupansDropDown').html(data.coupans);

                       $('.minPrice').val(data.minPrice);
                       $('.maxPrice').val(data.maxPrice);
                       $('.minDiscount').val(data.minDiscount);
                       $('.maxDiscount').val(data.maxDiscount);

                       $('.banner_id').val(data.banner_id);
                       
                    }
                }

            });
        });


        //UPDATE BANNER FORM
        $('.UpdateBannerForm').submit(function(event){
            event.preventDefault();
            removeErrorsFunction();
            $.ajax({

                url:'{{route("admin.updateBanner")}}',
                type:'POST',
                data:new FormData(this),
                contentType:false,
                processData:false,
                success:function(data) {

                    if(data.errors) {

                        if(data.errors.title){
                           $( '.title-error' ).html( data.errors.title[0] );
                        }
                        if(data.errors.image){
                           $( '.bannerimage-error' ).html( data.errors.image[0] );
                        }
                        if(data.errors.category){
                           $( '.category-error' ).html( data.errors.category[0] );
                        }
                        if(data.errors.brand){
                           $( '.brand-error' ).html( data.errors.brand[0] );
                        }
                        if(data.errors.coupan){
                           $( '.coupan-error' ).html( data.errors.coupan[0] );
                        }
                        if(data.errors.minPrice){
                           $( '.minprice-error' ).html( data.errors.minPrice[0] );
                        }
                        if(data.errors.maxPrice){
                           $( '.maxprice-error' ).html( data.errors.maxPrice[0] );
                        }
                        if(data.errors.minDiscount){
                           $( '.mindiscount-error' ).html( data.errors.minDiscount[0] );
                        }
                        if(data.errors.maxDiscount){
                           $( '.maxdiscount-error' ).html( data.errors.maxDiscount[0] );
                        }

                    }

                    if(data.status == 200) {
                        removeErrorsFunction();
                        $('.addBanner').modal('hide');
                        $('.AddBannerForm')[0].reset();
                        toastr.success( data.message);
                        setTimeout(function(){ location.reload(); }, 1000);   
                    }
                    if(data.status == 201) {
                        removeErrorsFunction();
                        toastr.error(data.message);
                        setTimeout(function(){ location.reload(); }, 3000);  
                    }

                }

            });
        });


        // DELETE BANNER ID
        $('body').on('click', '.deleteCategoryBannerId', function(event){
                
                
                var id = $(this).attr('rel');
                var page = $(this).attr('rel1');

                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: '{{route("admin.deleteBanner")}}',
                            type:'POST',
                            data:{id:id,page:page},
                            success: function(result){
                                swal('Banner', 'Deleted Successfully', "success");
                                window.setTimeout(function(){ 
                                    location.reload();
                                } ,1500);
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });



    })

</script>

<script src="{{ __common_asset('global/plugins/ion.rangeSlider.min.js') }}" ></script>
@endpush
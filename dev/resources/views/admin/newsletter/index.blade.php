@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                    <form action="#" method="get" class="filter-form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <label class="col-md-3 pd-tb-5"><strong>Newsletter Date Range:</strong> </label>    
                                                <div class="input-group date-picker input-daterange" data-date="10/11/2012"
                                                data-date-format="dd/mm/yyyy">
                                                    <input type="text" class="form-control datepicker input-sm" name="ffromdate"
                                                        id="ffromdate" placeholder="From" value="{{request()->ffromdate}}">
                                                    <span class="input-group-addon"> - </span>
                                                    <input type="text" class="form-control input-sm" name="ftodate" id="ftodate"
                                                        placeholder="To" value="{{request()->ftodate}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label col-md-4 pd-tb-5"><strong>Action:</strong></label>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Filter">
                                                    <span aria-hidden="true" class="icon-magnifier"></span> </button>
                                                <a class="btn btn-sm btn-icon-only white" href="{{route('admin.newsletter')}}" title="Reset"><span aria-hidden="true" class="icon-refresh"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <a href="{{route('admin.newsletter.add')}}" class="btn btn-default btn-sm tooltips" data-toggle="tooltip"
                            data-placement="top" data-original-title="Add Newsletter"><strong><span aria-hidden="true" class="icon-plus text-primary"></span>&nbsp;Add</strong></a>
                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip"
                            data-placement="top" data-original-title="Delete" data-action="soft-delete"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;Delete</strong></a>
                        <a href="javascript:;" id="btn_table_export" class="btn btn-sm btn-outline btn-default text-warning tooltips"
                            data-container="body" data-placement="top" data-original-title="Export to Excel"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a>
                    </div>
                    <table class="table table-checkable nowrap"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="25%" class=""> Subject </th>
                                <th width="40%" class=""> Content </th>
                                <th width="15%" class=""> Notify On </th>
                                <th width="20%" class=""> Actions </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
    type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Newsletter = function() {

    return {

        //main function to initiate the module
        init: function() {

            //date picker initiate
            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }
            //date-time picker initiate
            $('#datetimepicker1').datetimepicker({
                language: 'pt-BR'
            });

            $('.datetimepicker1').css("z-index","0");
            
            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.newsletter')}}?search=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());

                window.location.href = url;
            });

            // Export all Newsletter data
            $('#btn_table_export').on('click', function(e) {
                e.preventDefault();

                var url = "{{route('admin.newsletter.export')}}?export=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());

                window.location.href = url;
            });

            // Delete Newsletter
            $('body').on('click', '.dt-list-delete', function(event) {
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                swal({
                        title: 'Are you sure?',
                        text: 'You want Delete the record.',
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonClass: 'btn-success',
                        cancelButtonClass: 'btn-danger',
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: 'post',
                                success: function(result) {
                                    swal('Newsletter', 'Selected Newsletter Deleted Successfully',
                                        "success");
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
            });

            // Change Newsletter Status
            $('body').on('switchChange.bootstrapSwitch', '.status-switch', function(event, state) {
                var switchControl = $(this);
                var stateText = (state) ? 'Activate' : 'Deactivate';
                swal({
                        title: 'Are you sure?',
                        text: 'You want ' + stateText + ' this data.',
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonClass: 'btn-success',
                        cancelButtonClass: 'btn-danger',
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: "{{route('ajax.changeStatus')}}",
                                type: 'post',
                                data: {
                                    table: 'newsletters',
                                    id: switchControl.val(),
                                    status: state
                                },
                                success: function(result) {
                                    swal('Newsletter', 'Data has been ' + stateText + 'd',
                                        "success");
                                }
                            });
                        } else {
                            switchControl.bootstrapSwitch('state', !state);
                        }
                    });
            });

        }

    };

}();

jQuery(document).ready(function() {
    Newsletter.init();
});
</script>
@endpush
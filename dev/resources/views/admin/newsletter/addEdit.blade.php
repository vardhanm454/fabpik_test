@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
<link href="{{ __common_asset('global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
    type="text/css" />
    <style>
    .select2-results__option {
        padding: 0px 4px !important;
    }
    .select2-result-repository__title{
        margin-bottom: 0px !important;
    }
    .select2-result-repository{
        padding-top: 3px !important;
        padding-bottom: 3px !important;
    }

    .bootstrap-tagsinput{
    	width: 100%;
    }
    .bar{
        font-size: 18px !important;
        font-weight: 500 !important;
    }
    
    </style>
@endpush

@section('content')
<div class="row brands-wrap">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    @if(isset($editnewsletter))
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.newsletter.edit',['id'=>$id])}}" method="post" enctype="multipart/form-data">
                    @else
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.newsletter.add')}}" method="post" enctype="multipart/form-data">
                    @endif
                    {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('subject'))?'has-error':''}}">
                                                <label class="control-label">Subject:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="subject" id="subject"
                                                        maxlength="100" placeholder="" value="{{old('subject',(isset($editnewsletter))?$editnewsletter->subject:'')}}">
                                                    <span class="help-block">{{$errors->first('subject')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('notify_on'))?'has-error':''}}">
                                                <label class="control-label">Notify On:<span class="required"> *
                                                    </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control date-picker" name="notify_on" id="notify_on" {{isset($editnewsletter) ? '' : 'data-date-start-date="0d"' }}
                                                        value="{{old('notify_on', (isset($editnewsletter))?date('d-m-Y H:i:s',strtotime($editnewsletter->notify_on)):'')}}"
                                                        readonly>
                                                    {{--<span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>--}}
                                                    <span class="help-block">{{$errors->first('notify_on')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="{{($errors->first('content'))?'has-error':''}}">
                                                <label class="control-label">Content:<span class="required"> *
                                                    </span></label>
                                                <div class="">
                                                    <textarea readonly class="summernote" name="content" id="content">
                                                            {{old('content', (isset($editnewsletter))?$editnewsletter->content:'') }}
                                                    </textarea>
                                                    <span class="help-block">{{$errors->first('content')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                                <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                                <button type="submit" class="btn blue form-submit" name="save" value="save"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                @if(isset($editnewsletter))
                                <button type="submit" class="btn blue form-submit" name="save" value="savecont"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><i
                                        class="fa fa-check-circle"></i> Save &amp; Continue Edit</button>
                                @endif
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
    type="text/javascript">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript">
</script>
<script src="{{ __common_asset('global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var NewsletterAddEdit = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('.summernote').summernote();

            // Data Picker Initialization
            if (jQuery().datetimepicker) {
                @php $db_date =  isset($editnewsletter)? date('m/d/y', strtotime($editnewsletter->notify_on)) :'' ; @endphp
                @if($db_date != null && $db_date != '' && $db_date != 'undefined')
                var start_date = new Date('{{$db_date}}'); 
                @else
                var start_date = new Date(); 
                @endif
                
                $('.date-picker').datetimepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true,
                    startDate: start_date,
                    format: 'dd-mm-yyyy hh:ii:ss',
                    locale: 'en'
                });
            }

            $.fn.select2.defaults.set("theme", "bootstrap");

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.newsletter')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
                var $element = $(this);
                var $popover = $element.data('bs.popover');

                e.preventDefault();
                $('a[data-toggle="image"]').popover('destroy');
                if ($popover) {
                    return;
                }
                $element.popover({
                    html: true,
                    placement: 'right',
                    trigger: 'manual',
                    content: function() {
                        return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
                    }
                });
                $element.popover('show');
                $('#button-image').on('click', function() {
                    var $button = $(this);
                    var $icon = $button.find('> i');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element
                            .parent().find('input').attr('id') + "&thumb=" + $element
                            .attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-pencil');
                            }
                        },
                        success: function(html) {
                            $('body').append(
                                '<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' +
                                html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });
                $('#button-clear').on('click', function() {
                    $element.find('img').attr('src', $element.find('img').attr(
                        'data-placeholder'));
                    $element.parent().find('input').val('');
                    $element.popover('destroy');
                });
            });

            function formatRepoSelection(repo) {
                return repo.text;
            }
            // @see https://select2.github.io/examples.html#data-ajax
            function formatRepo(repo) {
                if (repo.loading) return repo.text;
                
                var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img style='width:90%' src='{{url('/')}}/{{UPLOAD_PATH}}/" + repo.image + "' /></div>" +
                "<div class='select2-result-repository__forks'> Seller:&nbsp" + repo.sname + "</div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.text +  " - "+ repo.primaryattrvalue +" - "+ repo.secondaryattrvalue +"</div>";
            
                if (repo.description) {
                    markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
                }            
                if(repo.disc == 0)
                    disc = '';
                else
                    disc = "("+ repo.disc.toFixed(2)+"%OFF)";
                markup += "<div class='select2-result-repository__statistics'>" +
                    "<div class='select2-result-repository__forks'> Rs.&nbsp" + repo.price + "&nbsp&nbsp <del>Rs.&nbsp" + repo.mrp + "</del>"+disc+" </div>" +
                   "</div>" +
                    "</div></div>";

                return markup;
            }

        }

    };

}();

jQuery(document).ready(function() {
    NewsletterAddEdit.init();
});
</script>
@endpush
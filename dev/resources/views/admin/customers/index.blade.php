@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' )}}"
rel="stylesheet" type="text/css" />

@endpush

@section('content')
<style>
.modal {
    text-align: center;
    padding: 0 !important;
}
.modal
.modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
}

.modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
}
.modal-body{
    max-height: 400px!important;
}
</style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title bg-info pd-15">
                    <!-- FILTER FORM START -->
                    <form action="#" method="get" class="filter-form">
                        <div class="form-body">
                            <div class="row">  
                                <div class="col-md-4">
                                    <label class=""><strong>Date:</strong> </label>
                                    <div class="">
                                        <div class="input-group  input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control date-picker input-sm" name="ffromdate"
                                                id="ffromdate" placeholder="From" value="{{request()->ffromdate}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control date-picker input-sm" name="ftodate" id="ftodate"
                                                placeholder="To" value="{{request()->ftodate}}">
                                        </div>
                                    </div>
                                </div>
                              
                                <div class="col-md-4">
                                    <label class=""><strong>Account Status:</strong> </label><br>
                                    <div class="">
                                        <select name="fstatus" id="fstatus" class="form-control input-sm">
                                            <option value="">All</option>
                                            <option value="1" {{request()->fstatus=='1'?'selected="selected"':''}}>Active
                                            </option>
                                            <option value="0" {{request()->fstatus=='0'?'selected="selected"':''}}>Inactive
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class=""><strong>mobile:</strong> </label><br>
                                    <div class="">
                                        <input type="text" class="form-control input-sm" name="fmobile" id="fmobile"  value="{{request()->fmobile?request()->fmobile:''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class=""><strong>Name:</strong> </label>
                                    <div class="">
                                        <input type="text" class="form-control input-sm" name="fname" id="fname"  value="{{request()->fname?request()->fname:''}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class=""><strong>Email:</strong> </label>
                                    <div class="">
                                        <input type="text" class="form-control input-sm" name="femail" id="femail"  value="{{request()->femail?request()->femail:''}}">
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <label class=""><strong>Action:</strong> </label><br>
                                    <div class="">
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search"
                                            id="btn_submit_search" value="search"
                                            data-loading-text="<i class='fa fa-spinner fa-spin'></i>"
                                            title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span>
                                        </button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('admin.customers')}}"
                                            title="Reset Filter"><span aria-hidden="true" class="icon-refresh"></span> </a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </form>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <a href="{{route('admin.customers.add')}}" class="btn btn-default btn-sm tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Add Customer"><strong><span aria-hidden="true" class="icon-plus text-primary"></span>&nbsp;Add</strong></a>

                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete" data-action="soft-delete"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;Delete</strong></a>

                        <a href="javascript:;" id="btn_table_export" class="btn btn-default btn-sm tooltips" data-container="body" data-placement="top" data-original-title="Export to Excel"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a>

                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-placement="top" data-original-title="Customer Active/In-Active" data-toggle="tooltip" data-action="change-status"><strong><span aria-hidden="true" id="btn-confirm" class="icon-trash text-danger"></span>&nbsp;Change Status</strong></a>

                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-placement="top" data-original-title="Notify" data-toggle="tooltip" data-action="notify-customers"><strong><span aria-hidden="true" id="btn-confirm" class="icon-bell text-danger"></span>&nbsp;Notify</strong></a>

                    </div>
                    <table class="table table-checkable nowrap" id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="" class=""> Name </th>
                                <th width="" class=""> Email </th>
                                <th width="" class=""> Mobile No </th>
                                <th width="" class=""> Registered </th>
                                <th width="" class=""> Modified </th>
                                <th width="" class=""> Status </th>
                                <th width="" class=""> Actions </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

<!-- Change Status Modal -->
<div class="modal fade" id="changeStatusModal" tabindex="-1" role="dialog" aria-labelledby="changeStatusModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changeStatusModalLabel">Change Activation Status!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="">
                    <label class="control-label">Change Status:</label>
                    <form>
                    <div class="">
                        <select class="form-control" name="status" id="status">
                            <option value="">Select</option>
                            @foreach(['1'=>'Active', '0'=>'Inactive'] as $val=>$label)
                            <option value="{{$val}}">
                                {{$label}}
                            </option>
                            @endforeach
                        </select>
                        <span style="color:#e73d4a" id="result"></span>
                    </div>
                    <div class="" id="reason_div" style="margin-top:10px;display:none">
                        <textarea class="form-control" name="inactive_reason" id="inactive_reason" placeholder="Enter reason for deactivation..."></textarea>
                        <span style="color:#e73d4a" id="reason-result"></span>
                    </div>
                    <form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!--Notify Modal -->
<div class="modal fade" id="notifyModal" tabindex="-1" role="dialog" aria-labelledby="notifyModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="notifyModalLabel"><b>Notify</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="">
                    <form>
                        <div class="">
                            <label class="control-label">Title:<span class="required"> * </span></label>
                            <div class="">
                                <input type="text" class="form-control maxlength-handler" name="subject" id="subject"
                                    maxlength="100" placeholder="" value="">
                                <span style="color:#e73d4a" id="subject_result"></span>
                            </div>
                        </div>
                        <div class="">
                            <label class="control-label">Notify On:<span class="required"> *
                                </span></label>
                            <div class="">
                                <input type="text" class="form-control date-picker1" name="notify_on" id="notify_on" data-date-start-date="0d"
                                    value=""
                                    readonly>
                                <!-- <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span> -->
                                <span style="color:#e73d4a" id="notify_result"></span>
                            </div>
                        </div>
                        <div class="">
                            <label class="control-label">Content:<span class="required"> *
                                </span></label>
                            <div class="">
                                <textarea class="form-control" name="content" id="content"></textarea>
                                <span style="color:#e73d4a" id="content_result"></span>
                            </div>
                        </div>
                    <br>
                    <input type="checkbox" class="" style="display:block;float:left;margin-right:6px;" id="select_all" name="select_all" value="1" />
                    <label for="select_all" style="display:block;float:left;">Send to all Customers</label><br>
                    <form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes1">Submit</button>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
    type="text/javascript">
</script>
<script src="{{ __common_asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
    type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Customers = function () {
    
    $('#changeStatusModal').on('hidden.bs.modal', function () {
        $('#changeStatusModal form')[0].reset();
    });
    $('#notifyModal').on('hidden.bs.modal', function () {
        $('#notifyModal form')[0].reset();
    });


    return {

        //main function to initiate the module
        init: function () {

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }

            if (jQuery().datetimepicker) {
                var start_date = new Date(); 
                
                $('.date-picker1').datetimepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true,
                    startDate: start_date,
                    format: 'dd-mm-yyyy hh:ii',
                    locale: 'en'
                });
                $('.date-picker1').css("z-index","10061");
            }

            //Based on the status text box will appear
            $("#status").change(function () {
                // console.log(this.value);
                var status = $("#status option:selected").val();
                if(status == 0){
                    $("#reason_div").css("display", "block");
                }
            });
            
            // Do Filter
            $('#btn_submit_search').on('click', function(e){
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.customers')}}?search=1";

                // if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                // if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());

                if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());
                if($('#ffromdate').val() != '') url += "&ffromdate="+encodeURIComponent($('#ffromdate').val());
                if($('#ftodate').val() != '') url += "&ftodate="+encodeURIComponent($('#ftodate').val());
                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                if($('#fmobile').val() != '') url += "&fmobile="+encodeURIComponent($('#fmobile').val());
                if($('#femail').val() != '') url += "&femail="+encodeURIComponent($('#femail').val());

                window.location.href = url;
            });

            // Export all customers data
            $('#btn_table_export').on('click', function(e){
                e.preventDefault();

                var url = "{{route('admin.customers.export')}}?export=1";
                
                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());
                
                window.location.href = url;
            });

            // Delete Customer
            $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                swal('Customer', 'Record Deleted Successfully', "success");
                                location.reload();
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });

        }

    };

}();

jQuery(document).ready(function() {    
   Customers.init();
});
</script>
@endpush
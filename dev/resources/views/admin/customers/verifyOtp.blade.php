@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.customers.verifyOtp')}}"
                        method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="otp-check">
                                <h3 class="font-green col-md-offset-3 col-md-9 text-center">Verify OTP</h3>
                                <p class=" col-md-offset-3 col-md-9"> Please enter the OTP (one time password) to verify
                                    your account. A Code has been sent to Email and Phone. </p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <i class="fa fa-mobile fa-5x pull-right"></i>
                                    </div>
                                    <div class="form-group col-md-9 {{($errors->first('verify_otp'))?'has-error':''}}">
                                        <input type="password" class="form-control" name="verify_otp" maxlength="100"
                                            placeholder="" value="{{old('verify_otp')}}">
                                        <span class="help-block">{{$errors->first('verify_otp')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-center">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><i
                                    class="fa fa-reply"></i> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save"
                                data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><i
                                    class="fa fa-save"></i> Create Account</button>
                        </div>
                        <div class="otp-check">
                            <div class="form-body">
                                <div class="form-group {{($errors->first('mobile_otp'))?'has-error':''}}">
                                    <label class="col-md-3 control-label">Enter OTP:<span class="required"> *
                                        </span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="mobile_otp" maxlength="100"
                                            placeholder="" value="{{old('mobile_otp')}}">
                                        <span class="help-block">{{$errors->first('mobile_otp')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-center">
                                <button type="button" id="btn_back_1" class="btn btn-secondary-outline"><i
                                        class="fa fa-reply"></i> Back</button>

                                <button type="button" class="btn blue form-submit" name="save" id="save" value="save"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><i
                                        class="fa fa-save"></i> Create Account</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
@endpush
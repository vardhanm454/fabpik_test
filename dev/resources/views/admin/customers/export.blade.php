<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->

<head>
    <meta charset="utf-8" />
    <title>Customer</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">
                <table class="tabletable-bordered">
                    <thead>
                        <tr style="background:#c2c2c2;">
                            <th> S.No </th>
                            <th> Name </th>
                            <th> Email </th>
                            <th> Mobile No </th>
                            <th> Registered </th>
                            <th> Modified </th>
                            <th> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($customers))
                        @foreach($customers as $index=>$customer)
                        <tr>
                            <td>{{++$index}}</td>
                            <td>{{$customer->name}}</td>
                            <td>{{$customer->email}}</td>
                            <td>{{$customer->mobile}}</td>
                            <td>{{date('d M Y', strtotime($customer->created_at))}}</td>
                            <td>{{date('d M Y', strtotime($customer->updated_at))}}</td>
                            <td>{{$customer->statusLabel()}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>
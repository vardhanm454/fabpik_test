@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                {{isset($customerdata)?$customerdata:''}}
                    @if(isset($editcustomer))
                    <form class="form-horizontal form-row-seperated"
                        action="{{route('admin.customers..edit',['id'=>$id])}}" method="post"
                        enctype="multipart/form-data" id="registration_form">
                        @else
                        <form class="form-horizontal form-row-seperated" action="{{route('admin.customers.add')}}"
                            method="post" enctype="multipart/form-data" id="registration_form">
                            @endif
                            {!! csrf_field() !!}
                            <div class="form-body">
                                <div class="form-group {{($errors->first('name'))?'has-error':''}}">
                                    <label class="col-md-3 control-label">Name:<span class="required"> * </span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control maxlength-handler" name="name"
                                            maxlength="100" placeholder=""
                                            value="{{old('name', isset($editcustomer)?$editcustomer->name:'')}}">
                                        <span class="help-block">{{$errors->first('name')}}</span>
                                    </div>
                                </div>
                                <div class="form-group {{($errors->first('email'))?'has-error':''}}">
                                    <label class="col-md-3 control-label">Email:<span class="required"> *
                                        </span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="email" id="email" maxlength="100"
                                            placeholder=""
                                            value="{{old('email', isset($editcustomer)?$editcustomer->email:'')}}">
                                        <span class="help-block">{{$errors->first('email')}}</span>
                                    </div>
                                </div>
                                <div class="form-group {{($errors->first('mobile'))?'has-error':''}}">
                                    <label class="col-md-3 control-label">Mobile:<span class="required"> *
                                        </span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="mobile" id="mobile"
                                            maxlength="100" placeholder=""
                                            value="{{old('mobile', isset($editcustomer)?$editcustomer->mobile:'')}}">
                                        <span class="help-block">{{$errors->first('mobile')}}</span>
                                    </div>
                                </div>
                                @if(!isset($editcustomer))
                                <div class="form-group {{($errors->first('password'))?'has-error':''}}">
                                    <label class="col-md-3 control-label">Password:<span class="required"> *
                                        </span></label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="password" maxlength="100"
                                            placeholder="" value="{{old('password')}}">
                                        <span class="help-block">{{$errors->first('password')}}</span>
                                    </div>
                                </div>
                                <div class="form-group {{($errors->first('confirm_password'))?'has-error':''}}">
                                    <label class="col-md-3 control-label">Confirm Password:<span class="required"> *
                                        </span></label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="confirm_password"
                                            maxlength="100" placeholder="" value="{{old('confirm_password')}}">
                                        <span class="help-block">{{$errors->first('confirm_password')}}</span>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="form-actions text-center">
                                <button type="button" id="btn_back" class="btn btn-secondary-outline"><i
                                        class="fa fa-reply"></i> Back</button>
                                <button type="submit" class="btn blue form-submit" name="continue" id="continue"
                                    value="continue"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><i
                                        class="fa fa-save"></i> Continue</button>
                            </div>
                            <div class="otp-check" style="display:{{isset($verify)?'block':'none'}}">
                                <div class="form-body">
                                    <div class="form-group {{($errors->first('mobile_otp'))?'has-error':''}}">
                                        <label class="col-md-3 control-label">Enter OTP:<span class="required"> *
                                            </span></label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="mobile_otp" maxlength="100"
                                                placeholder="" value="{{old('mobile_otp')}}">
                                            <span class="help-block">{{$errors->first('mobile_otp')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-center">
                                    <button type="button" id="btn_back_1" class="btn btn-secondary-outline"><i
                                            class="fa fa-reply"></i> Back</button>

                                    <button type="submit" class="btn blue form-submit" name="save" id="save"
                                        value="save"
                                        data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><i
                                            class="fa fa-save"></i> Create Account</button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript">
</script>
<script src="{{ __common_asset('global/plugins/jquery-validation/js/additional-methods.min.js') }}"
    type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Customers = function() {

    return {

        //main function to initiate the module
        init: function() {


            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.customers')}}";
            });
            $('#btn_back_1').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.customers')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

        }

    };

}();

jQuery(document).ready(function() {
    Customers.init();
});
</script>
@endpush
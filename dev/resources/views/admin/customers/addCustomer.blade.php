@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    @if(isset($editcustomer))
                    <form class="form-horizontal"
                        action="{{route('admin.customers.edit',['id'=>$id])}}" method="post"
                        enctype="multipart/form-data" id="registration_form">
                        @else
                        <form class="form-horizontal" action="{{route('admin.customers.add')}}"
                            method="post" enctype="multipart/form-data" id="registration_form">
                            @endif
                            {!! csrf_field() !!}
                            <div class="form-body">
                                <div class="form-group {{($errors->first('name'))?'has-error':''}}">
                                    <label class="col-md-3 control-label">Name:<span class="required"> * </span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control maxlength-handler" name="name"
                                            maxlength="100" placeholder=""
                                            value="{{old('name', isset($editcustomer)?$editcustomer->name:'')}}">
                                        <span class="help-block">{{$errors->first('name')}}</span>
                                    </div>
                                </div>
                                <div class="form-group {{($errors->first('email'))?'has-error':''}}">
                                    <label class="col-md-3 control-label">Email:<span class="required"> *
                                        </span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="email" id="email" maxlength="100"
                                            placeholder=""
                                            value="{{old('email', isset($editcustomer)?$editcustomer->email:'')}}">
                                        <span class="help-block">{{$errors->first('email')}}</span>
                                        <span class="help-block" id="unique_email"></span>
                                    </div>
                                </div>
                                <div class="form-group {{($errors->first('mobile'))?'has-error':''}}">
                                    <label class="col-md-3 control-label">Mobile:<span class="required"> *
                                        </span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="mobile" id="mobile"
                                            maxlength="100" placeholder=""
                                            value="{{old('mobile', isset($editcustomer)?$editcustomer->mobile:'')}}">
                                        <span class="help-block">{{$errors->first('mobile')}}</span>
                                        <span class="help-block" id="unique_mobile"></span>

                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <button type="button" id="btn_back" class="btn btn-secondary-outline">
                                <i class="fa fa-reply"></i> Back</button>
                                <button type="submit" class="btn blue form-submit" name="continue" id="continue" value="continue" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing...">
                                    <i class="fa fa-save"></i> Continue</button>
                            </div>
                            <div id="otp-check">
                                <input type="hidden" id="verifytype" name="verifytype" value="sendotp">
                                <div class="form-body">
                                    <div class="form-group {{($errors->first('mobile_otp'))?'has-error':''}}">
                                        <label class="col-md-3 control-label">Enter OTP:<span class="required"> *
                                            </span></label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="mobile_otp" maxlength="100"
                                                placeholder="" value="{{old('mobile_otp')}}">
                                            <span class="help-block">{{$errors->first('mobile_otp')}}</span>
                                            <span class="help-block" id="mobile_otp_check"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-right">
                                    <button type="button" id="btn_back_1" class="btn btn-secondary-outline">
                                        <i class="fa fa-reply"></i> Back
                                    </button>

                                    <button type="submit" class="btn blue form-submit" name="save" id="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing...">
                                        <i class="fa fa-save"></i> Create Account
                                    </button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Customers = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#otp-check').hide();

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.customers')}}";
            });
            $('#btn_back_1').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.customers')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

        }

    };

}();

jQuery(document).ready(function() {
    Customers.init();
});

$('#continue').click(function() {

    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form1 = $('#registration_form');
    var error3 = $('.alert-danger', form1);
    var success3 = $('.alert-success', form1);
    
    $.validator.addMethod("alphabetsnspace", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });

    form1.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
            name: {
                required: true,
                alphabetsnspace: true
            },
            email: {
                required: true,
                email: true

            },
            mobile: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
            name: {
                required: "This Field is Required",
                alphabetsnspace: "Please Enter Only Letters"
            },
            email: {
                required: "This Field is Required"
            },
            mobile: {
                required: "This Field is Required",
                maxlength: "Please enter not more than 10 Numbers."
            }
        },

        errorPlacement: function(error, element) { // render error placement for each input type
            if (element.parent(".input-group").size() > 0) {
                error.insertAfter(element.parent(".input-group"));
            } else if (element.attr("data-error-container")) {
                error.appendTo(element.attr("data-error-container"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
            $('#continue').button('reset');
        },

        invalidHandler: function(event, validator) { //display error alert on form submit   
            success3.hide();
            error3.show();
            App.scrollTo(error3, -200);
        },

        highlight: function(element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function(element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function(label) {
            label.closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function(form, e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{route('admin.customers.add')}}",
                data: $('form').serialize(),
                success: function(result) {
                    if (result.success == 2) {
                        $('#unique_email').html(result.errors.email);
                        $('#unique_mobile').html(result.errors.mobile);
                    } else if(result.success == 3){
                        $('#otp-check').show();
                        $('#continue').hide();
                        $('#btn_back').hide();
                    }else if(result.success == 4) { 
                        $('#mobile_otp_check').html(result.errors);
                        $('#save').button('reset');
                    } else if(result.success == 1){
                        toastr.success('Customer added successfully!', 'Success');
                        window.location.href = "{{route('admin.customers')}}";
                    }
                },
                error: function(error) {
                }
            });
            return false;
        }

    });



});
</script>
@endpush
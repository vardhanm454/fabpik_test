@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />

    <style>
    .select2-results__option {
        padding: 0px 4px !important;
    }
    .select2-result-repository__title{
        margin-bottom: 0px !important;
    }
    .select2-result-repository{
        padding-top: 3px !important;
        padding-bottom: 3px !important;
    }

    .bootstrap-tagsinput{
    	width: 100%;
    }
    .bar{
        font-size: 18px !important;
        font-weight: 500 !important;
    }
    
    </style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    @if(isset($editcoupon))
                    <form class="form-horizontal" action="{{route('admin.coupons.edit',['id'=>$id])}}" method="post"
                        enctype="multipart/form-data">
                        @else
                        <form class="form-horizontal" action="{{route('admin.coupons.add')}}" method="post"
                            enctype="multipart/form-data">
                            @endif
                            {!! csrf_field() !!}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group {{($errors->first('code'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Code:<span class="required"> *
                                                </span></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="code" placeholder=""
                                                    value="{{old('code', isset($editcoupon)?$editcoupon->code:'')}}">
                                                <span class="help-block">{{$errors->first('code')}}</span>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group {{($errors->first('min_cart_amount'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Min Amount:<span class="required"> *
                                                </span>
                                                <small>(minimum cart amount)</small>
                                            </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="min_cart_amount"
                                                    placeholder=""
                                                    value="{{old('min_cart_amount', isset($editcoupon)?$editcoupon->min_cart_amount:'')}}">
                                                <span class="help-block">{{$errors->first('min_cart_amount')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{($errors->first('amount'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Amount:<span class="required"> *
                                                </span></label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" name="amount" placeholder=""
                                                    value="{{old('amount', isset($editcoupon)?$editcoupon->amount:'')}}">
                                                <span class="help-block">{{$errors->first('amount')}}</span>
                                            </div>
                                            <div class="col-md-5">
                                                <select
                                                    class="form-control {{($errors->first('dis_type'))?'has-error':''}}"
                                                    name="dis_type">
                                                    @foreach(['f'=>'Flat (INR)','p'=>'Percentage (%)'] as $val=>$label)
                                                    <option value="{{$val}}"
                                                        {{old('dis_type',  isset($editcoupon)?$editcoupon->dis_type:'f')==$val ? 'selected' : '' }}>
                                                        {{$label}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group {{($errors->first('max_usage'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Max Usage:<span class="required"> *
                                                </span></label>
                                            <div class="col-md-8">
                                                <select class="form-control" name="max_usage">
                                                    <option value="">Select</option>
                                                    @foreach(['o'=>'Only Once','m'=>'Multiple Times'] as $val=>$label)
                                                    <option value="{{$val}}" {{ old('max_usage', (isset($editcoupon)?$editcoupon->max_usage:'') ) ==$val ? 'selected' :'' }} >
                                                        {{$label}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block">{{$errors->first('max_usage')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{($errors->first('fseller'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Sellers:</label>
                                            <div class="col-md-8">
                                            <select class="form-control input-sm" name="fseller[]" id="fseller" multiple>
                                                @if(!$errors->has('fseller'))

                                                    @if (is_array(old('fseller')))
                                                        
                                                        @php $oldsellers = old('fseller'); @endphp
                                                    
                                                        @foreach ($oldsellers as $s)
                                                        @php $seller = App\Models\Seller::selectRaw("id,name")->find($s);
                                                            @endphp
                                                            <option value="{{$seller->id}}" selected="selected">{{$seller->name}}</option>
                                                        @endforeach

                                                    @elseif(isset($editcoupon) && $editcoupon->seller_ids )

                                                        @php $sellers=json_decode($editcoupon->seller_ids, true); @endphp

                                                        @foreach($sellers as $s)
                                                        @php $seller = App\Models\Seller::selectRaw("id,name")->find($s);
                                                            @endphp
                                                            <option value="{{$seller->id}}" selected="selected">{{$seller->name}}</option>
                                                        @endforeach
                                                    
                                                    @endif
                                                @endif
                                            </select>
                                            <span class="help-block">{{$errors->first('fseller')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{($errors->first('fprovariants'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Product Variants:</label>
                                            <div class="col-md-8">
                                                    
                                                <select class="form-control input-sm" name="fprovariants[]" id="fprovariants" multiple>
                                                    @if(!$errors->has('fprovariants'))
                                                        
                                                        @if (is_array(old('fprovariants')))
                                                    
                                                            @php $oldprovariants = old('fprovariants'); @endphp
                                                        
                                                            @foreach ($oldprovariants as $variant)
                                                                @php $proVariants = App\Models\ProductVariant::selectRaw("id,name")->find($variant);
                                                                @endphp
                                                                <option value="{{$proVariants->id}}" selected="selected">{{$proVariants->name}}</option>
                                                            @endforeach
                                                        
                                                        @elseif(isset($editcoupon) && $editcoupon->product_varient_ids)
                                                        
                                                            @php $variants=json_decode($editcoupon->product_varient_ids); @endphp
                                                            
                                                            @foreach($variants as $variant)
                                                            
                                                            @php $proVariants = App\Models\ProductVariant::selectRaw("id,name")->find($variant);
                                                                @endphp
                                                                <option value="{{$proVariants->id}}" selected="selected">{{$proVariants->name}}</option>

                                                            @endforeach

                                                        @endif

                                                    @endif
                                                   
                                                </select>
                                                <span class="help-block">{{$errors->first('fprovariants')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{($errors->first('coupon_groups'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Coupon Group:</label>
                                            <div class="col-md-8">
                                                    
                                                <select class="form-control input-sm select2" name="coupon_groups" id="coupon_groups">
                                                <option value="=">Select</option>
                                                    @php $coupongroups =  App\Models\CouponGroup::get(); @endphp
                                                    @foreach($coupongroups as $group)
                                                    <option value="{{$group->id}}" {{( old('coupon_groups', isset($editcoupon)?$editcoupon->coupon_group_id:'')==$group->id ? 'selected' : '') }}>{{$group->group_name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block">{{$errors->first('coupon_groups')}}</span>
                                            </div>
                                        </div>

                                        @if(isset($editcoupon))
                                        <div class="form-group {{($errors->first('status'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Coupon URL:<span class="required">*</span></label>
                                            <div class="col-md-8">
                                            <input type="text" class="form-control" name="coupon_url" placeholder=""
                                                    value="https://fabpik.in/products?cp={{$id}}" readonly>
                                            </div>
                                        </div>
                                        @endif  
                                       
                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group {{($errors->first('start_date'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Start Date:<span class="required"> *
                                                </span></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control date-picker" name="start_date" {{isset($editcoupon) ? '' : 'data-date-start-date="0d"' }}
                                                    value="{{old('start_date', isset($editcoupon)?date('d-m-Y H:i:s',strtotime($editcoupon->start_date)):'')}}"
                                                    readonly>
                                                {{--<span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>--}}
                                                <span class="help-block">{{$errors->first('start_date')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{($errors->first('expiry_date'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Expiry Date:<span class="required"> *
                                                </span></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control date-picker" name="expiry_date" {{isset($editcoupon) ? '': 'data-date-start-date="0d"' }}
                                                    placeholder="" value="{{old('expiry_date', isset($editcoupon)?date('d-m-Y H:i:s',strtotime($editcoupon->expiry_date)):'')}}" readonly>
                                                {{--<span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>--}}
                                                <span class="help-block">{{$errors->first('expiry_date')}}</span>
                                            </div>
                                        </div>
                                        <div class="form-group {{($errors->first('sub_type'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Deduction From:<span class="required">*</span></label>
                                            <div class="col-md-8">
                                                <select class="form-control" name="sub_type">
                                                    <option value="">Select</option>
                                                    @foreach(['m'=>'MRP','s'=>'Selling Price'] as $val=>$label)
                                                    <option value="{{$val}}" @if(old('sub_type',
                                                        isset($editcoupon)?$editcoupon->sub_type:'')==$val) selected
                                                        @endif>
                                                        {{$label}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block">{{$errors->first('sub_type')}}</span>
                                            </div>
                                        </div>
                                        <div class="form-group {{($errors->first('description'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Description:<span class="required"> *
                                                </span></label>
                                            <div class="col-md-8">
                                                <textarea class="form-control" name="description">{{old('description', isset($editcoupon)?$editcoupon->description:'')}}</textarea>
                                                <span class="help-block">{{$errors->first('description')}}</span>
                                            </div>
                                        </div>
                                        <div class="form-group {{($errors->first('fcategory'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Categories:</label>
                                            <div class="col-md-8">
                                                <select class="form-control input-sm" name="fcategory[]" id="fcategory" multiple>
                                                    @if(!$errors->has('fcategory'))
                                                    
                                                        @if (is_array(old('fcategory')))
                                                    
                                                            @php $oldcategories = old('fcategory'); @endphp
                                                        
                                                            @foreach ($oldcategories as $category)
                                                                @php $category = App\Models\Category::selectRaw("id,title")->find($category);
                                                                @endphp
                                                                <option value="{{$category->id}}" selected="selected">{{$category->title}}</option>
                                                            @endforeach

                                                        @elseif(isset($editcoupon) && $editcoupon->category_ids)
                                                   
                                                            @php $categories=json_decode($editcoupon->category_ids, true); @endphp
                                                            
                                                            @foreach($categories as $cate)
                                                                @php $category = App\Models\Category::selectRaw("id,title")->find($cate);
                                                                @endphp
                                                                <option value="{{$category->id}}" selected="selected">{{$category->title}}</option>
                                                            @endforeach
                                                            
                                                        @endif

                                                    @endif
                                                </select>
                                                <span class="help-block">{{$errors->first('fcategory')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{($errors->first('mobile_numbers'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Mobile Numbers:</label>
                                            <div class="col-md-8">
                                                <!-- <input type="text" class="form-control input-large" name="mobile_numbers" id="mobile_numbers" value="{{old('mobile_numbers', isset($editcoupon)?$editcoupon->mobile_numbers:'')}}" placeholder="Ex: 9000000000, 9000000001, 9000000002" data-role="tagsinput"> -->
                                               @if(isset($editcoupon->mobile_numbers) && !is_null($editcoupon->mobile_numbers))
                                                @php 
                                                    $numbers = json_decode($editcoupon->mobile_numbers); 
                                                @endphp
                                                @endif
                                                <textarea class="form-control" name="mobile_numbers">{{old('mobile_numbers', (isset($editcoupon) && !is_null($editcoupon->mobile_numbers) )? implode("\n", $numbers ) :'')}}</textarea>
                                                (* Please seperate the mobile numbers by clicking the enter)
                                                <span class="help-block">{{$errors->first('mobile_numbers')}}</span>
                                            </div>
                                        </div>  
                                        @if(isset($editcoupon))
                                        <div class="form-group {{($errors->first('status'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Status:<span class="required">*</span></label>
                                            <div class="col-md-8">
                                                <select class="form-control" name="status">
                                                    <option value="">Select</option>
                                                    @foreach([0=>'Inactive', 1=>'Active'] as $val=>$label)
                                                    <option value="{{$val}}" {{ old('status', (isset($editcoupon)?$editcoupon->status:'') )==$val ? 'selected' : '' }}>
                                                        {{$label}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block">{{$errors->first('status')}}</span>
                                            </div>
                                        </div>
                                        @endif  
                                                                        
                                       
                                    </div>
                                        
                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                                <button type="submit" class="btn blue form-submit" name="save" value="save"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                @if(isset($editcoupon))
                                <button type="submit" class="btn blue form-submit" name="save" value="savecont"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><i
                                        class="fa fa-check-circle"></i> Save &amp; Continue Edit</button>
                                @endif
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
    type="text/javascript">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var CouponAddEdit = function() {

    return {

        //main function to initiate the module
        init: function() {
            //Multitag inputes
           

            // Data Picker Initialization
            if (jQuery().datetimepicker) {
                @php $db_date =  isset($editcoupon)? date('m/d/y', strtotime($editcoupon->start_date)) :'' ; @endphp
                @if($db_date != null && $db_date != '' && $db_date != 'undefined')
                var start_date = new Date('{{$db_date}}'); 
                @else
                var start_date = new Date(); 
                @endif
                
                $('.date-picker').datetimepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true,
                    startDate: start_date,
                    format: 'dd-mm-yyyy hh:ii:ss',
                    locale: 'en'
                });
            }

            $.fn.select2.defaults.set("theme", "bootstrap");

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.coupons')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
                var $element = $(this);
                var $popover = $element.data('bs.popover');

                e.preventDefault();
                $('a[data-toggle="image"]').popover('destroy');
                if ($popover) {
                    return;
                }
                $element.popover({
                    html: true,
                    placement: 'right',
                    trigger: 'manual',
                    content: function() {
                        return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
                    }
                });
                $element.popover('show');
                $('#button-image').on('click', function() {
                    var $button = $(this);
                    var $icon = $button.find('> i');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element
                            .parent().find('input').attr('id') + "&thumb=" + $element
                            .attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-pencil');
                            }
                        },
                        success: function(html) {
                            $('body').append(
                                '<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' +
                                html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });
                $('#button-clear').on('click', function() {
                    $element.find('img').attr('src', $element.find('img').attr(
                        'data-placeholder'));
                    $element.parent().find('input').val('');
                    $element.popover('destroy');
                });
            });
            
            //search seller
            $('#fseller').select2({ 
                allowClear:true,
                minimumInputLength: 1,
                placeholder: 'Search seller',
                allowClear: true,
                ajax: {
                    url: "{{route('ajax.sellerAutoComplete')}}?",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,  // search term 
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (sellerarr) {
                                return {
                                    text: sellerarr.text,
                                    id: sellerarr.id,
                                    val: sellerarr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (sellerarr) {
                    return sellerarr.text;
                },
            });

            //search Categories
            $('#fcategory').select2({ 
                allowClear:true,
                minimumInputLength: 1,
                placeholder: 'Search Category',
                allowClear: true,
                ajax: {
                    url: "{{route('ajax.categoryAutoComplete')}}?",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,  // search term 
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (sellerarr) {
                                return {
                                    text: sellerarr.text,
                                    id: sellerarr.id,
                                    val: sellerarr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (sellerarr) {
                    return sellerarr.text;
                },
            });

            //search ProductVariants
            $('#fprovariants').select2({ 
                allowClear:true,
                minimumInputLength: 1,
                placeholder: 'Search Product Variant',
                allowClear: true,
                ajax: {
                    url: "{{route('ajax.productVariantAutoComplete')}}?",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,  // search term 
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (sellerarr) {
                                return {
                                    text: sellerarr.text,
                                    id: sellerarr.id,
                                    val: sellerarr.text,
                                    term: params.term,
                                    mrp: sellerarr.mrp,
                                    price: sellerarr.price,
                                    sname: sellerarr.sname,
                                    scode: sellerarr.scode,
                                    disc: sellerarr.disc,
                                    image: sellerarr.image,
                                    primaryattrvalue: sellerarr.primaryattrvalue,
                                    secondaryattrvalue: sellerarr.secondaryattrvalue,
                                }
                            })
                        };
                    },
                    cache: false
                },
                // templateResult: function (sellerarr) {
                //     return sellerarr.text +' - '+ sellerarr.mrp;
                // },
                escapeMarkup: function(markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            function formatRepoSelection(repo) {
                return repo.text;
            }
            // @see https://select2.github.io/examples.html#data-ajax
            function formatRepo(repo) {
                if (repo.loading) return repo.text;
                
                var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img style='width:90%' src='{{url('/')}}/{{UPLOAD_PATH}}/" + repo.image + "' /></div>" +
                "<div class='select2-result-repository__forks'> Seller:&nbsp" + repo.sname + "</div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.text +  " - "+ repo.primaryattrvalue +" - "+ repo.secondaryattrvalue +"</div>";
            
                if (repo.description) {
                    markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
                }            
                if(repo.disc == 0)
                    disc = '';
                else
                    disc = "("+ repo.disc.toFixed(2)+"%OFF)";
                markup += "<div class='select2-result-repository__statistics'>" +
                    "<div class='select2-result-repository__forks'> Rs.&nbsp" + repo.price + "&nbsp&nbsp <del>Rs.&nbsp" + repo.mrp + "</del>"+disc+" </div>" +
                   "</div>" +
                    "</div></div>";

                return markup;
            }

        }

    };

}();

jQuery(document).ready(function() {
    CouponAddEdit.init();
});
</script>
@endpush
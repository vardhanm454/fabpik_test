@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Price:</strong> </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm" name="fpricefrom"
                                                id="fpricefrom" placeholder="Min" value="{{request()->fpricefrom}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control input-sm" name="fpriceto"
                                                id="fpriceto" placeholder="Max" value="{{request()->fpriceto}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label col-md-3 pd-tb-5"><strong>Status:</strong></label>
                                    <div class="col-md-9">
                                        <select name="fstatus" id="fstatus" class="form-control input-sm">
                                            <option value="">All</option>
                                            <option value="1" {{request()->fstatus=='1'?'selected="selected"':''}}>
                                                Active
                                            </option>
                                            <option value="0" {{request()->fstatus=='0'?'selected="selected"':''}}>
                                                Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Name:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fname" id="fname"
                                            value="{{request()->fname}}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                        <label class="col-md-3 pd-tb-5"><strong>Date:</strong> </label>
                                        <div class="col-md-9">
                                            <div class="input-group date-picker input-daterange" data-date="10/11/2012"
                                                data-date-format="mm/dd/yyyy">
                                                <input type="text" class="form-control datepicker input-sm" name="ffromdate" id="ffromdate"
                                                    placeholder="From" value="{{request()->ffromdate}}">
                                                <span class="input-group-addon"> - </span>
                                                <input type="text" class="form-control input-sm" name="ftodate" id="ftodate" placeholder="To" value="{{request()->ftodate}}">
                                            </div>
                                        </div>
                                </div>
                                <div class="col-md-4">
                                        <label class="col-md-3 pd-tb-5"><strong>Categories:</strong> </label>
                                        
                                        <div class="col-md-9">
                                            <select class="form-control input-sm" name="fcategory" id="fcategory">
                                            @if(request()->fcategory)
                                            @php $categories = \DB::table('view_categories')->where('path_id', request()->fcategory)->first(); @endphp
                                        
                                            <option value="{{$categories->path_id}}" selected="selected">{{$categories->path_title}}</option>
                                            @endif
                                            </select>
                                        </div>

                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>SKU:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fsku" id="fsku"
                                            value="{{request()->fsku}}" autocomplete="off">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                <label class="col-md-3 pd-tb-5"><strong>Unique ID:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="funiqueid" id="funiqueid"
                                            value="{{request()->funiqueid}}" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="col-md-3  pd-tb-5"><strong>Commission Model:</strong> </label>
                                    <div class="col-md-9 pd-tb-5">
                                        <select class="form-control select2" name="fcommissiontype" id="fcommissiontype" data-allow-clear="true">
                                            <option value="">Select</option>
                                            @php $commissionModals = App\Models\CommissionModel::where('status', 1)->get() @endphp
                                            @foreach($commissionModals as $val=>$label)
                                            <option value="{{$label->id}}" {{request()->fcommissiontype==$label->id?'selected="selected"':''}}> {{$label->name}} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3"><strong>Seller:</strong> </label>
                                    <div class="col-md-9">
                                        @php $sellers = App\Models\Seller::where('status',1)->get(); @endphp
                                        <select name="fseller" id="fseller"
                                            class="form-control form-filter input-sm select2" data-allow-clear="true">
                                            <option value="">Select Seller</option>
                                                @if(isset($sellers))
                                                    @foreach($sellers as $seller)
                                                        <option value="{{$seller->id}}"
                                                        {{request()->fseller==$seller->id?'selected="selected"':''}}>
                                                            {{$seller->company_name}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                        </select>
                                    </div>
                                </div>                              
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Brand:</strong> </label>
                                    <div class="col-md-9">
                                        @php $brands = App\Models\Brand::where('status',1)->get(); @endphp
                                        <select name="fbrand" id="fbrand"
                                            class="form-control form-filter input-sm select2"  data-allow-clear="true">
                                            <option value="">Select Brand</option>
                                                @if(isset($brands))
                                                    @foreach($brands as $brand)
                                                        <option value="{{$brand->id}}"
                                                        {{request()->fbrand==$brand->id?'selected="selected"':''}}>
                                                            {{$brand->name}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Comapany Name:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fcompanyname" id="fcompanyname"
                                            value="{{request()->fcompanyname}}" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Fabpik Featured:</strong> </label>
                                    <div class="col-md-9">
                                        <select name="ffeatured" id="ffeatured" class="form-control input-sm">
                                            <option value="">All</option>
                                            <option value="1" {{request()->ffeatured=='1'?'selected="selected"':''}}>
                                                Yes
                                            </option>
                                            <option value="0" {{request()->ffeatured=='0'?'selected="selected"':''}}>
                                                No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Seller Featured:</strong> </label>
                                    <div class="col-md-9">
                                        <select name="fsfeatured" id="fsfeatured" class="form-control input-sm">
                                            <option value="">All</option>
                                            <option value="1" {{request()->fsfeatured=='1'?'selected="selected"':''}}>
                                                Yes
                                            </option>
                                            <option value="0" {{request()->fsfeatured=='0'?'selected="selected"':''}}>
                                                No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Discount (INR):</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fdiscount" id="fdiscount"
                                            value="{{request()->fdiscount}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Fabpik Discount (INR):</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="ffdiscount" id="ffdiscount"
                                            value="{{request()->ffdiscount}}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                            <div class="col-md-4">
                                        <label class="col-md-3 pd-tb-5"><strong>Special Price Start Date Range:</strong> </label>
                                        <div class="col-md-9">
                                            <div class="input-group date-picker input-daterange" data-date="10/11/2012"
                                                data-date-format="mm/dd/yyyy">
                                                <input type="text" class="form-control datepicker input-sm" name="spsfromdate" id="spsfromdate"
                                                    placeholder="From" value="{{request()->spsfromdate}}">
                                                <span class="input-group-addon"> - </span>
                                                <input type="text" class="form-control input-sm" name="spstodate" id="spstodate" placeholder="To" value="{{request()->spstodate}}">
                                            </div>
                                        </div>
                                </div>
                                <div class="col-md-4">
                                        <label class="col-md-3 pd-tb-5"><strong>Special Price End Date Range:</strong> </label>
                                        <div class="col-md-9">
                                            <div class="input-group date-picker input-daterange" data-date="10/11/2012"
                                                data-date-format="mm/dd/yyyy">
                                                <input type="text" class="form-control datepicker input-sm" name="spefromdate" id="spefromdate"
                                                    placeholder="From" value="{{request()->spefromdate}}">
                                                <span class="input-group-addon"> - </span>
                                                <input type="text" class="form-control input-sm" name="spetodate" id="spetodate" placeholder="To" value="{{request()->spetodate}}">
                                            </div>
                                        </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Action:</strong> </label>
                                    <div class="col-md-9">                                    
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('admin.specialprices')}}" title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span></a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                         <button type="button" class="btn btn-default btn-sm table-group-action-status tooltips" data-toggle="modal" data-keyboard="false" data-backdrop="static" data-placement="top" data-original-title="Change Prices" data-toggle="tooltip" data-action="change-price">
                            <strong><span aria-hidden="true" class="icon-settings text-primary"></span>&nbsp;Change Price</strong>
                        </button>                    
                    </div>
                    <table class="table dt-responsive nowrap product-list-table text-center"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="4%" class="">&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="10%" class="text-center"> ID </th>
                                <th width="17%" class="text-center"> Name </th>
                                <th width="10%" class="text-center"> Image </th>
                                <th width="13%" class="none"> Category </th>
                                <th class="none"> SKU </th>
                                <th width="7%" class="text-center"> MRP </th>
                                <th width="10%" class="text-center"> Sale/<br>Selling Price </th>
                                <th width="10%" class="text-center"> Seller <br>Discount (INR) </th>
                                <th width="10%" class="text-center"> Fabpik <br>Selling Price </th>
                                <th width="10%" class="text-center"> Fabpik <br>Discount (INR) </th>
                                <th width="20%" class="text-center"> Fabpik <br>Discount (%) </th>
                                <th width="20%" class="none"> Fabpik Addon Discount (%) </th>
                                <th width="20%" class="none"> Special Price Start Date </th>
                                <th width="20%" class="none"> Special Price End Date </th>
                                <th width="" class="none"> Primary Variant: </th>
                                <th width="" class="none"> Secondary Variant: </th>
                                <th width="" class="none"> Created at: </th>
                                <th width="" class="none"> Modified at: </th>
                                <th width="10%" class="text-center"> Status </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="changePriceModal" tabindex="-1" role="dialog" aria-labelledby="changePriceModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changePriceModalLabel">Change Prices</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">                    
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label class="control-label">Discount <span class="required"> * </span></label>
                                <select class="form-control" name="discount_type" id="discount_type">
                                    <option value="">Select</option>
                                    @foreach(['f'=>'Fixed', 'p'=>'Percentage'] as $val=>$label)
                                    <option value="{{$val}}"> {{$label}} </option>
                                    @endforeach
                                </select>
                                <span style="color:#e73d4a" id="discount_type_result"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Amount (On MRP) <span class="required"> * </span></label>
                                <input type="text" class="form-control" name="discount_amount" id="discount_amount" autocomplete="off">
                                <span style="color:#e73d4a" id="discount_amount_result"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Start Date:<span class="required"> * </span></label>
                                <input type="text" class="form-control special_date_picker" name="start_date" id="start_date"  readonly>
                                <span style="color:#e73d4a" id="start_date_result"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">End Date:<span class="required"> * </span></label>
                                <input type="text" class="form-control special_date_picker" name="end_date" id="end_date" readonly>
                                <span style="color:#e73d4a" id="end_date_result"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>

@stop

@push('PAGE_ASSETS_JS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var SpecialPrices = function() {
    
    $('#changePriceModal').on('hidden.bs.modal', function () {
        $('#changePriceModal form')[0].reset();
    });
    
    return {

        //main function to initiate the module
        init: function() {

            //Filters will apply when click the enter
            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

            $('#fcategory').select2({ 
                allowClear:true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.childcategories.autocomplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (categoryArr) {
                    return categoryArr.text;
                },
            });

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }
            
            $('.special_date_picker').datetimepicker({
                todayHighlight:true,
                todayBtn:true,
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                format: 'dd-mm-yyyy hh:ii',
                locale: 'en',
                startDate: new Date() ,
                container:'#changePriceModal',
            });

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.specialprices')}}?search=1";

                if ($('#fname').val() != '') url += "&fname=" + encodeURIComponent($('#fname').val());
                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fstatus').val() != '') url += "&fstatus=" + encodeURIComponent($('#fstatus').val());
                if ($('#fpricefrom').val() != '') url += "&fpricefrom=" + encodeURIComponent($('#fpricefrom').val());
                if ($('#fpriceto').val() != '') url += "&fpriceto=" + encodeURIComponent($('#fpriceto').val());
                // if ($('#ffeatured').val() != '') url += "&ffeatured=" + encodeURIComponent($('#ffeatured').val());
                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#spsfromdate').val() != '') url += "&spsfromdate=" + encodeURIComponent($('#spsfromdate').val());
                if ($('#spstodate').val() != '') url += "&spstodate=" + encodeURIComponent($('#spstodate').val());
                if ($('#spefromdate').val() != '') url += "&spefromdate=" + encodeURIComponent($('#spefromdate').val());
                if ($('#spetodate').val() != '') url += "&spetodate=" + encodeURIComponent($('#spetodate').val());
                if ($('#fcategory').val()) url += "&fcategory=" + encodeURIComponent($('#fcategory').val());
                if ($('#fsku').val()) url += "&fsku=" + encodeURIComponent($('#fsku').val());
                if ($('#funiqueid').val()) url += "&funiqueid=" + encodeURIComponent($('#funiqueid').val());
                if ($('#fcommissiontype').val()) url += "&fcommissiontype=" + encodeURIComponent($('#fcommissiontype').val());
                if ($('#fbrand').val()) url += "&fbrand=" + encodeURIComponent($('#fbrand').val());
                if ($('#fcompanyname').val()) url += "&fcompanyname=" + encodeURIComponent($('#fcompanyname').val());
                if ($('#fsfeatured').val()) url += "&fsfeatured=" + encodeURIComponent($('#fsfeatured').val());
                if ($('#ffeatured').val()) url += "&ffeatured=" + encodeURIComponent($('#ffeatured').val());
                if ($('#fdiscount').val()) url += "&fdiscount=" + encodeURIComponent($('#fdiscount').val());
                if ($('#ffdiscount').val()) url += "&ffdiscount=" + encodeURIComponent($('#ffdiscount').val());

                window.location.href = url;
            });
        }

    };

}();

jQuery(document).ready(function() {
    SpecialPrices.init();
});
</script>
@endpush
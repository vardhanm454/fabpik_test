@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
<style>
.form-horizontal .form-group {
    margin-bottom: 0;
}
</style>
@endpush

@section('content')
<div class="import-product-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <span aria-hidden="true" class="icon-cloud-upload"></span> Upload Images
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form">
                        <div class="form-body">
                            <div class="note note-info">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-body form">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#product_images" data-toggle="tab"> Product Images </a>
                            </li>
                            <li>
                                <a href="#sizechart_images" data-toggle="tab"> Size Chart Images </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="product_images">
                            <form action="{{route('admin.image.imageStore')}}" class="form-horizontal dropzone dropzone-file-area" id="my-dropzone" style="width: 500px; margin-top: 50px;" method="post" enctype="multipart/form-data">
                                @csrf

                                    <div class="form-group {{($errors->first('seller'))?'has-error':''}}">
                                        <div class="row">
                                            <div class="col-md-2 col-md-offset-2">
                                                <label class="control-label">Seller:<span class="required"> * </span></label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control input-sm select2" name="seller">
                                                <option></option>
                                                <optgroup label="Category">
                                                    @if(isset($sellers))
                                                    @foreach($sellers as $seller)
                                                    <option value="{{$seller->id}}" {{old('seller')}}>{{$seller->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </optgroup>
                                            </select>
                                            <span class="help-block">{{$errors->first('seller')}}</span>
                                            </div>
                                        </div>
                                    </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet-body">
                                            <div>
                                                <h3 class="sbold">Drop files here or click to upload</h3>
                                            </div>
                                            <div class="caption">
                                                <span class="caption-subject font-blue sbold"><span aria-hidden="true" class="icon-cloud-upload"></span> Upload Product Images</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>
                            </div>
                            <div class="tab-pane" id="sizechart_images">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN PORTLET-->
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <span class="caption-subject font-blue sbold"><span aria-hidden="true"
                                                        class="icon-cloud-upload"></span> Upload Size Chart
                                                    Images</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="{{route('admin.image.sizeChartImageStore')}}"
                                                class="form-horizontal dropzone dropzone-file-area"
                                                id="sizechart-dropzone" style="width: 500px; margin-top: 50px;"
                                                method="post" enctype="multipart/form-data">
                                                @csrf
                                                
                                                <div class="form-body">
                                                    <h3 class="sbold">Drop files here or click to upload</h3>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="button" id="button-image" class="btn btn-primary"><span class=""></span> View Uploaded Images</button> 
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/dropzone/dropzone.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
    var FormDropzone = function() {
        return {
            //main function to initiate the module
            init: function() {
                Dropzone.options.myDropzone = {
                    acceptedFiles: ".jpeg,.jpg,.png,.gif",
                    dictDefaultMessage: "",
                    init: function() {
                        this.on("addedfile", function(file) {
                            // Create the remove button
                            var removeButton = Dropzone.createElement(
                                "<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>"
                                );

                            // Capture the Dropzone instance as closure.
                            var _this = this;

                            // Listen to the click event
                            removeButton.addEventListener("click", function(e) {
                                // Make sure the button click doesn't submit the form:
                                e.preventDefault();
                                e.stopPropagation();

                                // Remove the file preview.
                                _this.removeFile(file);
                                // If you want to the delete the file on the server as well,
                                // you can do the AJAX request here.
                                var name = file.name;
                                var csrfToken = "{{ csrf_token() }}";
                                var input = {
                                    "filename": name,
                                    "_token": csrfToken
                                };
                                $.ajax({
                                    type: 'POST',
                                    url: '{{route("admin.image.removeImage")}}',
                                    data: input,
                                    dataType: 'json',
                                    success: function(result) {

                                    }
                                });

                            });

                            // Add the button to the file preview element.
                            file.previewElement.appendChild(removeButton);
                        });
                    }
                }
            }
        };
    }();

    var SizeChartFormDropzone = function() {

        return {
            //main function to initiate the module
            init: function() {

                Dropzone.options.sizechartDropzone = {
                    acceptedFiles: ".jpeg,.jpg,.png,.gif",
                    dictDefaultMessage: "",
                    init: function() {
                        this.on("addedfile", function(file) {
                            // Create the remove button
                            var sizeChartRemoveButton = Dropzone.createElement(
                                "<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>"
                                );

                            // Capture the Dropzone instance as closure.
                            var _this = this;

                            // Listen to the click event
                            sizeChartRemoveButton.addEventListener("click", function(e) {
                                // Make sure the button click doesn't submit the form:
                                e.preventDefault();
                                e.stopPropagation();

                                // Remove the file preview.
                                _this.removeFile(file);
                                // If you want to the delete the file on the server as well,
                                // you can do the AJAX request here.
                                var name = file.name;
                                var csrfToken = "{{ csrf_token() }}";
                                var input = {
                                    "sizechartfilename": name,
                                    "_token": csrfToken
                                };
                                $.ajax({
                                    type: 'POST',
                                    url: '{{route("admin.image.sizeChartRemoveImage")}}',
                                    data: input,
                                    dataType: 'json',
                                    success: function(result) {

                                    }
                                });
                            });

                            // Add the button to the file preview element.
                            file.previewElement.appendChild(sizeChartRemoveButton);
                        });
                    }
                }
            }
        };
    }();

    var FileManager = function() {
        return {

            //main function to initiate the module
            init: function() {

                $('#button-image').on('click', function() {
                    var $element = $(this);
                    var $button = $(this);
                    var $icon = $button.find('> span');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element
                            .parent().find('input').attr('id') + "&thumb=" + $element
                            .attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', '');
                            }
                        },
                        success: function(html) {
                            $('body').append(
                                '<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' +
                                html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });
            }
        };
    }();

    jQuery(document).ready(function() {
        FormDropzone.init();
        SizeChartFormDropzone.init();
        FileManager.init();
    });
</script>
@endpush
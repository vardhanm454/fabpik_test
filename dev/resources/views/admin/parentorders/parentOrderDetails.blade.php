@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')

@if(isset($orderDetails))
<div class="row">
    <div class="col-md-6 col-sm-12 pd-lr-10">
        <div class="panel panel-info order-info">
            <div class="panel-heading">
                <div class="caption">
                    <span class="pd-tb-10"><span aria-hidden="true" class="icon-handbag"></span>
                        Order Information </span>
                </div>
            </div>

            <div class="panel-body">
                <div class="row static-info">
                    <div class="col-md-6 name"> Parent Order ID: </div>
                    <div class="col-md-6 value"> {{$childOrders[0]->order->parent_order_id}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-6 name"> Order Date: </div>
                    <div class="col-md-6 value"> {{date('d-m-Y', strtotime($childOrders[0]->order->created_at))}} </div>
                </div>
                {{--<div class="row static-info">
                    <div class="col-md-6 name"> Order Status: </div>
                    <div class="col-md-6 value">
                        @if(isset($orderStatus))
                        @foreach($orderStatus as $status)
                        {{($status->id == $childOrders[0]->order->order_status_id)?$status->admin_text:''}}
                        @endforeach
                        @endif
                    </div>
                </div>--}}
                <div class="row static-info">
                    <div class="col-md-6 name"> Payment Method: </div>
                    <div class="col-md-6 value">
                        @foreach(['c'=>'Cash on Delivery', 'o'=>'Online'] as $val=>$label)
                        {{($val == $childOrders[0]->order->payment_type)?$label:''}}
                        @endforeach
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-6 name"> Payment Status: </div>
                    <div class="col-md-6 value">
                        @if(isset($paymentStatus))
                        @foreach($paymentStatus as $status)
                        {{($status->id == $childOrders[0]->order->payment_status_id)?$status->admin_text:''}}
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 pd-lr-10">
        <div class="panel panel-info customer-info">
            <div class="panel-heading">
                <div class="caption">
                    <span aria-hidden="true" class="icon-user"></span> Customer Information
                </div>
            </div>
            <div class="panel-body">
                <div class="row static-info">
                    <div class="col-md-5 name"> Customer Name: </div>
                    <div class="col-md-7 value"> {{isset($childOrders[0]->order->first_name)?$childOrders[0]->order->first_name:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name"> Email ID: </div>
                    <div class="col-md-7 value"> {{isset($childOrders[0]->order->email)?$childOrders[0]->order->email:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name"> Phone No.: </div>
                    <div class="col-md-7 value"> {{isset($childOrders[0]->order->mobile)?$childOrders[0]->order->mobile:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name"> Address: </div>
                    <div class="col-md-7 value"> {{isset($childOrders[0]->order->billing_address1)?$childOrders[0]->order->billing_address1:''}},
                    {{isset($childOrders[0]->order->billing_address2)?$childOrders[0]->order->billing_address2:''}}<br>
                    {{isset($childOrders[0]->order->billing_state)?$childOrders[0]->order->billing_state:''}},
                    {{isset($childOrders[0]->order->billing_city)?$childOrders[0]->order->billing_city:''}}
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name"> Pincode: </div>
                    <div class="col-md-7 value"> {{isset($childOrders[0]->order->billing_pincode)?$childOrders[0]->order->billing_pincode:''}} </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 pd-lr-10">
        <div class="panel panel-info products-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="caption col-md-6 pd-tb-5">
                        <span class=""><span aria-hidden="true" class="icon-handbag"></span>
                            Products Ordered </span>
                    </div>
                    <!-- <div class="col-md-6 text-right">
                        <a href="javascript:;" class="btn btn-default btn-sm tooltips" title="Download Invoice">
                            <strong>
                                <span aria-hidden="true" class="icon-eye text-primary"></span> Download</strong> </a>
                        <a href="javascript:;" class="btn btn-default btn-sm" title="Print">
                            <strong>
                                <span aria-hidden="true" class="icon-printer text-warning"></span> Print</strong> </a>
                    </div> -->
                </div>
            </div>
            <div class="panel-body">
                <div class="">
                    <table class="table dt-responsive nowrap" id="dt_listing">
                        <thead>
                            <tr>
                                <th> Sr. No. </th>
                                <th width="100" class="none"> Child Order ID </th>
                                <th> Description </th>
                                <th> MRP </th>
                                <th width="70"> Quantity </th>
                                <th> Discount </th>
                                <th width="70"> Selling Price </th>
                                <th width="70"> Coupon Code </th>
                                <th width="70"> Coupon Amount </th>
                                <th width="70"> Coupon Type </th>
                                <th width="70"> Deal Price </th>
                                <th width="70"> Total </th>
                                <th width="80"> Order Status </th>
                                <th width="90"> Shipping Status </th>
                                <th width="100"> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($childOrders as $index=>$details)
                            <tr>
                                <td>{{++$index}}</td>
                                <td>{{$details->child_order_id}}</td>
                                <td>{{isset($details->productVarient->name)?$details->productVarient->name:''}}</td>
                                <td>{{$details->mrp}}</td>
                                <td>{{$details->quantity}}</td>
                                <td>{{number_format((float)$details->discount, '1')}}</td>
                                <td>{{number_format((float)$details->price, '1')}}</td>
                                <td>{{$details->coupon_code}}</td>
                                <td>{{number_format((float)(isset($details->coupon_discount)?$details->coupon_discount:'0'), '1')}}</td>
                                @if(!is_null($details->coupon_id))
                                    @if($details->dis_type == 'p' && $details->sub_type = 'm')
                                        <td>Percentage on MRP</td>
                                    @elseif($details->dis_type == 'p' && $details->sub_type = 's')
                                        <td>Percentage on Selling Price</td>
                                    @elseif($details->dis_type == 'f' && $details->sub_type = 's')
                                        <td>Flat on Selling Price</td>
                                    @else
                                        <td>-</td>
                                    @endif
                                @else
                                    <td>-</td>
                                @endif
                                <td>{{number_format((float)(isset($details->deal_price)?$details->deal_price:'0'), '1')}}</td>   
                                
                                @if($details->dis_type == 'p' && $details->sub_type = 'm' && ( (isset($details->coupon_discount)?$details->coupon_discount:'0') > ($details->discount)))
                                <td>
                                {{ $details->mrp - (isset($details->coupon_discount)?$details->coupon_discount:'0') }}
                                </td>
                                @else
                                <td>
                                {{ isset($details->deal_price) ? (number_format((float)(isset($details->deal_price)?$details->deal_price:'0'), '1')) : 
                                    (number_format((float) (($details->price * $details->quantity) - (isset($details->coupon_discount)?$details->coupon_discount:'0')), '1')) }}
                                </td>
                                @endif
                                <td>
                                    @if(isset($orderStatus))
                                    @foreach($orderStatus as $status)
                                    {{($status->id == $details->order_status_id)?$status->admin_text:''}}
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(isset($shippingStatus))
                                    @foreach($shippingStatus as $status)
                                    {{($status->id == $details->shipping_status_id)?$status->admin_text:''}}
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    <div class="">
                                    @if($details->order_status_id == 8 || $details->order_status_id == 9)
                                        <a href="{{route('admin.childorders.downloadCustomerInvoice', ['id'=>$details->id])}}"
                                            class="btn btn-icon-only default btn-circle" data-toggle="tooltip"
                                            data-placement="top" data-original-title="Download Invoice"
                                            title="Download Invoice">
                                            <span aria-hidden="true" class="icon-cloud-download"></span>
                                        </a>
                                    @endif
                                        <a href="javascript:;" class="btn btn-icon-only default btn-circle"
                                            data-toggle="tooltip" data-placement="top" data-original-title="Track Order"
                                            title="Track Order">
                                            <span aria-hidden="true" class="icon-pointer"></span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-sm-12 pd-lr-10">
    </div>
    <div class="col-md-4 col-sm-12 pd-lr-10">
    </div>
    <div class="col-md-4 col-sm-12 pd-lr-10">
        <div class="panel panel-info order-summary">
            <div class="panel-heading">
                <div class="caption">
                    <span aria-hidden="true" class="icon-handbag"></span> Order Summary
                </div> 
            </div>
            @php 
            $total = $orderDetails->total;
            $coupon_charge = isset($orderDetails->coupon_discount)?$orderDetails->coupon_discount:0;
            $subtotal = $total+$coupon_charge;
            $finalAmount = ($orderDetails->price*$details->quantity)-$coupon_charge;
            @endphp
            <div class="panel-body">
                <div class="row static-info">
                    <div class="col-md-8 name"> Sub Total: </div>
                    <div class="col-md-4 value text-right"> {{number_format((float)$subtotal, '1')}} </div>
                </div>
                @if(isset($orderDetails->coupon_discount))
                <div class="row static-info">
                    <div class="col-md-9 name"> Coupon Discount : </div>
                    <div class="col-md-3 value text-right"> {{number_format((float)$coupon_charge, '1')}} </div>
                </div>
                @endif
                <div class="row static-info">
                    <div class="col-md-8 name"> Total: </div>
                    <div class="col-md-4 value text-right"> {{number_format((float)$orderDetails->total, '1')}} </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endif

@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
@endpush
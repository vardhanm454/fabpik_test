<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Laravel</title>
      <meta http-equiv="Content-Type" content="text/html;">
      <style media="all">
         *{
         margin: 0;
         padding: 0;
         line-height: 1.3;
         font-family: sans-serif;
         color: #333542;
         }
         body{
         font-size: .875rem;
         }
         .gry-color *,
         .gry-color{
         color:#878f9c;
         }
         table{
         width: 100%;
         }
         table th{
         font-weight: normal;
         }
         table.padding th{
         padding: .5rem .7rem;
         }
         table.padding td{
         padding: .7rem;
         }
         table.sm-padding td{
         padding: .2rem .7rem;
         }
         .border {border:1px solid #CCCCCC;}
         .border-bottom td,
         .border-bottom th{
         border-bottom:1px solid #CCCCCC;
         }
         .border-top th, .border-top td {border-top:1px solid #CCCCCC;}
         .border-right {border-right:1px solid #CCCCCC;}
         .text-left{
         text-align:left;
         }
         .text-right{
         text-align:right;
         }
         .text-center{
         text-align:center;
         }
         .small{
         font-size: .90rem;
         }
         .currency{
         }
         .mt-1 {margin-top:10px;}
         .mt-2 {margin-top:20px;}
         .mt-3 {margin-top:30px;}
         .mt-4 {margin-top:40px;}
         .mt-5 {margin-top:50px;}

         .mb-1 {margin-bottom:10px;}
         .mb-2 {margin-bottom:20px;}
         .mb-3 {margin-bottom:30px;}
         .mb-4 {margin-bottom:40px;}
         .mb-5 {margin-bottom:50px;}

         .bg-gray {background:#CCCCCC;}
      </style>
   </head>
   <body>
      <div style="padding: 0.5rem 1.5rem;">
            <table class="" cellpadding="0" cellspacing="0">
               <tbody>
                  <tr style="height:150px;background:#107d91;">
                     <td class="" style="width:50%;padding-left:30px;box-sizing:border-box">
                        <img src="https://fabpik.in/wp-content/uploads/2020/05/Fabpik_logo.png" width="200px">
                        <h2 style="color:#ffffff;margin:0 20px;">Order Invoice</h2>
                     </td>
                     <td class="" style="width:50%;font-size:1rem;padding:0 1rem;">
                        <p><span style="color:#ffffff;">Order No: 20200908-66-28</span></p>
                        <p><span style="color:#ffffff;">Order Date: 08-09-2020 01:12 PM</span></p>
                        <p><span style="color:#ffffff;">Invoice No: 20200908-66-28</span></p>
                        <p><span style="color:#ffffff;">Invoice Date: 08-09-2020 01:12 PM</span></p>
                        <p><span style="color:#ffffff;">GSTIN: 20200908-66-28</span></p>
                        <p><span style="color:#ffffff;">PAN: QWERT1234Y</span></p>
                     </td>
                  </tr>
               </tbody>
            </table>
    </div>
            <div style="padding: 0.5rem 1.5rem;">
            <table>
               <tbody>
                  <tr>
                     <td class="small" width="33.33%">
                        <p style="margin-bottom:5px;"><strong>Sold By</strong></p>
                        <p style="margin-bottom:5px;">Fabpik</p>
                        <p style="margin-bottom:5px;">2nd Floor, VSSR Square, Vittal Rao Nagar, HITEC City, Hyderabad, Telangana 500081</p>
                        <p style="margin-bottom:5px;"><strong>Email:</strong> customer@example.com</p>
                        <p style="margin-bottom:5px;"><strong>Phone:</strong> 8588090792</p>
                     </td>
                     <td class="small" width="33.33%" style="display: table-cell;vertical-align: top;">
                        <p style="margin-bottom:5px;"><strong>Shipping Address</strong></p>
                        <p style="margin-bottom:5px;">Mr. Customer</p>
                        <p style="margin-bottom:5px;">Demo address, Demo city, India</p>
                        <p style="margin-bottom:5px;"><strong>Email:</strong> customer@example.com</p>
                        <p style="margin-bottom:5px;"><strong>Phone:</strong> 8588090792</p>
                     </td>
                     <td class="small" width="33.33%" style="display: table-cell;vertical-align: top;">
                        <p style="margin-bottom:5px;"><strong>Bill Address</strong></p>
                        <p style="margin-bottom:5px;">Mr. Customer</p>
                        <p style="margin-bottom:5px;">Demo address, Demo city, India</p>
                        <p style="margin-bottom:5px;"><strong>Email:</strong> customer@example.com</p>
                        <p style="margin-bottom:5px;"><strong>Phone:</strong> 8588090792</p>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
         
         <div style="padding: 0.5rem 1.5rem;">
            <table class="padding border" cellspacing="0">
               <thead>
                  <tr class="border-bottom">
                     <th width="35%" class="text-left"><strong>Product</strong></th>
                     <th width="10%" class="text-left"><strong>Qty</strong></th>
                     <th width="15%" class="text-left"><strong>Gross Amount</strong></th>
                     <th width="15%" class="text-left"><strong>Discount</strong></th>
                     <th width="15%" class="text-left"><strong>Unit Price</strong></th>
                     <th width="15%" class="text-left"><strong>Total</strong></th>
                  </tr>
               </thead>
               <tbody class="strong">
                  <tr class="">
                     <td class="small">Test Product (Purple-NB-Silk)</td>
                     <td class="small">
                        1
                     </td>
                     <td class="small">1000.00</td>
                     <td class="small">-0.00</td>
                     <td class="small">1000.00</td>
                     <td class="small">1000.00</td>
                  </tr>
                  <tr class="">
                     <td class="small">Test Product (Purple-NB-Silk)</td>
                     <td class="small">
                        1
                     </td>
                     <td class="small">1000.00</td>
                     <td class="smallsmall">-0.00</td>
                     <td class="small">1000.00</td>
                     <td class="small">1000.00</td>
                  </tr>
                  <tr class="border-top">
                     <td class="small"><strong>Shipping Charges</strong></td>
                     <td class="small">
                        1
                     </td>
                     <td class="small">0.00</td>
                     <td class="small">-0.00</td>
                     <td class="small">0.00</td>
                     <td class="small">0.00</td>
                  </tr>
                  <tr class="border-top" style="background:#f2f2f2;">
                     <td class=""><strong>Total Qty: 1</strong></td>
                     <td colspan="3" class="">&nbsp;</td>
                     <td colspan="2" class="text-right small"><strong>Total Price: 2000.00</strong>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
         <div style="padding: 0.2rem 1.5rem;">
            <table class="mt-1">
               <tbody>
                  <tr>
                     <td class="small" width="30%" style="background:#eeeeee;padding:1rem 2rem;color:#ffffff;">
                        <h3 style="margin-bottom:5px;"><strong style="">Seller Registered Address: </strong></h3>
                        <h3 style="margin-bottom:5px;">FabPik</h3>
                        <p style="margin-bottom:5px;">2nd Floor, VSSR Square, Vittal Rao Nagar, HITEC City, Hyderabad, Telangana 500081</p>
                        <p style="margin-bottom:5px;"><strong style="">Email: </strong> customer@example.com</p>
                        <p style="margin-bottom:5px;"><strong style="">Phone: </strong> 8588090792</p>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>

         <div style="padding: 0.2rem 1.5rem;">
            <table class="mt-1">
               <tbody>
                  <tr>
                     <td class="small" width="30%" style="background:#bcdff5;padding:2rem;color:#ffffff;">
                        <h3 style="margin-bottom:5px;"><strong>Declaration: </strong></h3>
                        <p style="margin-bottom:5px;"><span>The goods sold are intended for end user consumption and not for resale.</span></p>
                        <p style="margin-bottom:5px;"><span>E. & O.E.</span></p>
                        <p><img src="https://fabpik.in/wp-content/uploads/2020/05/Fabpik_logo.png" width="100px" style="margin-left:-10px;"></p>
                     </td>
                     <td class="text-right" width="30%" style="background:#107d91;padding:1rem;">
                        <p class="">
                        <span><strong style="color:#ffffff;">Seller Name</strong></span>
                        <br>
                        <span style="color:#ffffff;">Authorized Signature</span>
                     </p>
                 </td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </body>
</html>
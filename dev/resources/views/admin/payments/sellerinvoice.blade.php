@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="portlet light portlet-fit portlet-datatable bordered">
   <div class="portlet-title">
      <div class="caption">
         <span class=""><span aria-hidden="true" class="icon-handbag"></span> Order Invoice</span>
      </div>
      <div class="actions print-btn pd-0">
         <a href="{{route('admin.payments.sellerInvoice', ['id'=>$orderDetails->id])}}" class="btn btn-default tooltips" title="Download Seller Invoice">
            <strong>
               <span aria-hidden="true" class="icon-printer text-primary"></span> Print
            </strong> 
         </a>         
      </div>
   </div>
   <div class="portlet-body">
      <div class="row">
         <div class="col-md-6">
            <p><span><strong>Order No:</strong> {{$orderDetails->child_order_id}}</span></p>
            <p><span><strong>Order Date:</strong> {{date('d-m-Y', strtotime($orderDetails->created_at))}}</span></p>
         </div>
         <div class="col-md-6 text-right">
            <p><span><strong>Invoice No:</strong> {{$orderDetails->order->invoice_prefix}} {{$orderDetails->order->parent_order_id}}</span></p>
            <p><span><strong>Invoice Date:</strong> {{date('d-m-Y')}}</span></p>
         </div>
      </div>
      <hr class="hr-seperator">
      <div class="row">
         <div class="col-md-6">
            <p><strong>Bill From:</strong></p>
            <p>Fabpik</p>
            <p>2nd Floor, VSSR Square, Vittal Rao Nagar,</p>
            <p>HITEC City, Hyderabad, Telangana 500081</p>
            <p><strong>GSTIN:</strong> {{$orderDetails->seller->gst_no}}</p>
            <p><strong>PAN:</strong></p>
         </div>
         <div class="col-md-6">
            <p><strong>Bill To:</strong></p>
            <p>{{$orderDetails->seller->name}}</p>
            <p>{{$orderDetails->seller->company_address}}, {{$orderDetails->seller->company_state}}, {{$orderDetails->seller->company_pincode}}</p>
            <p><strong>GSTIN:</strong> {{$orderDetails->seller->gst_no}}</p>
            <p><strong>Email:</strong> {{isset($orderDetails->seller->pan)?:$orderDetails->seller->pan}}</p>
            <p><strong>TAN:</strong> {{isset($orderDetails->seller->tan)?:$orderDetails->seller->tan}}</p>
         </div>
      </div>
      <hr class="hr-seperator margin-bottom-30">
      <div class="row">
         <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover dataTable text-center margin-bottom-20" cellspacing="0">
               <thead>
                  <tr class="border-bottom">
                     <th width="200" class="text-center"><strong>Description</strong></th>
                     <th width="100" class="text-center"><strong>Price/Charges</strong></th>
                     <th width="100" class="text-center"><strong>Commission Rate</strong></th>
                     <th width="100" class="text-center"><strong>Amount</strong></th>
                     <th width="100" class="text-center"><strong>SGST</strong></th>
                     <th width="100" class="text-center"><strong>CGST</strong></th>
                     <th width="100" class="text-center"><strong>Total</strong></th>
                  </tr>
               </thead>
               <tbody class="strong">
                  <tr class="">
                     @php 

                     $commission_rate = ($orderDetails->commission / $orderDetails->total)*100;
                     $commission_total = $orderDetails->commission+$orderDetails->commission_sgst+$orderDetails->commission_cgst;

                     //Shipping Charges
                     $shippingcharge = 0;
                     if (!is_null($orderDetails->shipping_charge)) {
                        $shippingcharge = $orderDetails->shipping_charge + $orderDetails->shipping_charge_sgst +$orderDetails->shipping_charge_cgst;
                     }

                     $finalAmount = $commission_total+$shippingcharge;

                     @endphp

                     <td class="small">{{$orderDetails->productVarient->name}} - Commission Amount<br>
                        @if(isset($attributeOptions))
                        @foreach($attributeOptions as $options)
                        <b>{{$options['atrributeName']}}</b> : {{$options['atrributeValue']}}</br>
                        @endforeach
                        @endif
                     </td>
                     <td class="small">{{$orderDetails->total}}</td>
                     <td class="small">{{$commission_rate}} %</td>
                     <td class="small">{{$orderDetails->commission}}</td>
                     <td class="small">{{$orderDetails->commission_sgst}}</td>
                     <td class="small">{{$orderDetails->commission_cgst}}</td>                
                     <td class="small">{{$commission_total}}</td>
                  </tr>
                  <tr>
                  @if(!is_null($orderDetails->shipping_charge))
                     <td> Shipping Charges - ({{$orderDetails->product->weight*1000}} Grams) </td>
                     <td> {{$orderDetails->shipping_charge}} </td>
                     <td>-</td>
                     <td>{{ $orderDetails->shipping_charge }}</td>
                     <td>{{ $orderDetails->shipping_charge_sgst }}</td>
                     <td>{{ $orderDetails->shipping_charge_cgst }}</td>
                     <td>{{ $shippingcharge }}</td>
                  @endif
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
      <hr class="hr-seperator">
      <div class="row">
         <div class="col-md-6">
            <div class="invoice-footer">
               <div>
                  <p></p>
                  <p></p>
                  <p class="">
                     <span><strong>Authorized Signature</strong></span>
                  </p>
               </div>
            </div>
         </div>
         <div class="col-md-6 text-right">
            <p></p>
            <p></p>
            <p><strong>Grand Total: </strong> {{$finalAmount}}</p>
         </div>
      </div>
      
   </div>
</div>

@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
@endpush
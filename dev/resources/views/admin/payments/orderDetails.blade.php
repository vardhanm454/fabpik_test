@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')

@if(isset($orderDetails))
<div class="row">
    <div class="col-md-6 col-sm-12 pd-lr-10">
        <div class="panel panel-info order-info">
            <div class="panel-heading">
                <div class="caption">
                    <span class="pd-tb-10"><span aria-hidden="true" class="icon-handbag"></span>
                        Order Information</span>
                </div>
            </div>
            <div class="panel-body">
                <div class="row static-info">
                    <div class="col-md-6 name"> Order ID: </div>
                    <div class="col-md-6 value"> {{$orderDetails->child_order_id}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-6 name"> Order Date: </div>
                    <div class="col-md-6 value"> {{date('d-m-Y', strtotime($orderDetails->created_at))}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-6 name"> Order Status: </div>
                    <div class="col-md-6 value"> 
                    @if(isset($orderStatus))
                    @foreach($orderStatus as $status)
                    {{($status->id == $orderDetails->order_status_id)?$status->name:''}}
                    @endforeach
                    @endif 
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-6 name"> Payment Method: </div>
                    <div class="col-md-6 value"> 
                    @foreach(['c'=>'Cash on Delivery', 'o'=>'Online'] as $val=>$label)
                    {{($val == $orderDetails->order->payment_type)?$label:''}}
                    @endforeach
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-6 name"> Payment Status: </div>
                    <div class="col-md-6 value">
                    @if(isset($paymentStatus))
                    @foreach($paymentStatus as $status)
                        {{($status->id == $orderDetails->payment_status_id)?$status->name:''}}
                    @endforeach
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 pd-lr-10">
        <div class="panel panel-info customer-info">
            <div class="panel-heading">
                <div class="caption">
                    <span aria-hidden="true" class="icon-user"></span> Customer Information
                </div>
            </div>
            <div class="panel-body">
                <div class="row static-info">
                    <div class="col-md-5 name"> Customer Name: </div>
                    <div class="col-md-7 value"> {{isset($orderDetails->order->first_name)?$orderDetails->order->first_name:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name"> Email ID: </div>
                    <div class="col-md-7 value"> {{isset($orderDetails->order->email)?$orderDetails->order->email:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name"> Phone No.: </div>
                    <div class="col-md-7 value"> {{isset($orderDetails->order->mobile)?$orderDetails->order->mobile:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name"> Address: </div>
                    <div class="col-md-7 value"> {{isset($orderDetails->order->billing_address1)?$orderDetails->order->billing_address1:''}}
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-5 name"> Pincode: </div>
                    <div class="col-md-7 value"> {{isset($orderDetails->order->billing_pincode)?$orderDetails->order->billing_pincode:''}} </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="panel panel-info products-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="caption col-md-6 pd-tb-5">
                        <span class="pd-tb-10"><span aria-hidden="true" class="icon-handbag"></span> Products Ordered
                        </span>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="" cellspacing="0">
                        <thead>
                            <tr class="border-bottom">
                                <th width="200" class=""><strong> Order ID </strong></th>
                                <th width="250" class=""><strong>Product <br>Description</strong></th>
                                <th width="200" class="text-center"><strong>MRP <br>(INR)</strong></th>
                                <th width="130" class="text-center"><strong>Discount <br>(%)</strong></th>
                                <th width="150" class="text-center"><strong>Selling<br> Price (INR)</strong></th>
                                <th width="100" class="text-center"><strong>Qunatity</strong></th>
                                <th width="250" class="text-center"><strong>Commission</strong></th>
                                <th width="170" class="text-center"><strong>Base <br>Commission <br>Rate (%)</strong></th>
                                <th width="170" class="text-center"><strong>Final <br>Commission <br>Rate (%)</strong></th>
                                <th width="150" class="text-center"><strong>Amount</strong></th>
                                <th width="100" class="text-center"><strong>SGST <br> (INR) </strong></th>
                                <th width="100" class="text-center"><strong>CGST <br> (INR)</strong></th>
                                <th width="200" class="text-center"><strong>Total</strong></th>
                                <th width="200"> Actions </th>
                            </tr>
                        </thead>
                        <tbody class="strong">
                            <tr>
                            @php 
  
                                $total = $orderDetails->price*$orderDetails->quantity;
                                $finalCommissionPercentage = ($orderDetails->commission/($orderDetails->price*$orderDetails->quantity))*100;
                                
                                $subtotal = $orderDetails->commission + $orderDetails->order_handling_charge + $orderDetails->shipping_charge;
                                $finalSgst = $orderDetails->commission_sgst + $orderDetails->shipping_charge_sgst + $orderDetails->order_handling_charge_sgst;
                                $finalCgst = $orderDetails->commission_cgst + $orderDetails->shipping_charge_cgst + $orderDetails->order_handling_charge_cgst;

                                $finaltotal = $subtotal + $finalCgst + $finalSgst;

                            @endphp

                            <td>{{$orderDetails->child_order_id}}</td>
                                <td>{{isset($orderDetails->productVarient->name)?$orderDetails->productVarient->name:''}} <br>
                                    @if(isset($attributeOptions))
                                    @foreach($attributeOptions as $options)
                                    @if(isset($options))
                                    <b>{{$options['atrributeName']}}</b> : {{$options['atrributeValue']}}</br>
                                    @endif
                                    @endforeach
                                    @endif
                                </td>
                                <td class="text-center">{{$orderDetails->mrp}}</td>
                                <td class="text-center">{{number_format((float) ($orderDetails->discount/$orderDetails->mrp)*100, '1' )}}</td>
                               
                                <td class="text-center">{{$orderDetails->price}}</td>
                                <td class="text-center">{{$orderDetails->quantity}}</td>
                               
                                <td class=" text-center">
                                    @php $commissionDetails = App\Models\CommissionModel::getCommission($orderDetails->seller->commission_id) @endphp
                                    <b>Model: </b><a href="{{route('admin.commissionmodels')}}?search=1&fname={{$commissionDetails->name}}" target="_blank">{{$commissionDetails->name}}</a>
                                    <b>Type: </b>{{($commissionDetails->commission_type == 'f')?'Fixed':'Variable'}}
                                </td>
                                <td class="text-center">{{number_format((float)$orderDetails->seller->commission, '1')}}</td>
                                <td class="text-center">{{number_format((float) ($orderDetails->commission/($orderDetails->price*$orderDetails->quantity))*100, '1')}}</td>
                                
                                <td class="text-center">{{number_format((float)$orderDetails->commission, '2')}}</td>
                                <td class="text-center">{{round($orderDetails->commission_sgst, 2)}}</td>
                                <td class="text-center">{{round($orderDetails->commission_cgst, 2)}}</td>                
                                <td class="text-center">{{number_format( (float)($orderDetails->commission + $orderDetails->commission_sgst + $orderDetails->commission_cgst), '1')}}</td>
                                <td>
                                <div class=" text-center">
                                    <a href="{{route('admin.payments.downloadSellerInvoice', ['id'=>$orderDetails->id])}}""
                                        class="btn btn-icon-only default btn-circle" data-toggle="tooltip"
                                        data-placement="top" data-original-title="Download Invoice"
                                        title="Download Invoice">
                                        <span aria-hidden="true" class="icon-cloud-download"></span>
                                    </a>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                @if(!is_null($orderDetails->shipping_charge))
                                <td> Shipping Charges - ({{isset($orderDetails->shipping_weight)?( $orderDetails->shipping_weight*1000 ):''}} Grams) </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-center">{{$orderDetails->shipping_charge}}</td>
                                <td class="text-center">{{ $orderDetails->shipping_charge_sgst }}</td>
                                <td class="text-center">{{ $orderDetails->shipping_charge_cgst }}</td>
                                <td class="text-center">{{ $orderDetails->shipping_charge + $orderDetails->shipping_charge_sgst +$orderDetails->shipping_charge_cgst }}</td>
                                @endif
                            </tr>
                            <tr>
                                @if(!is_null($orderDetails->order_handling_charge))
                                <td> Handling Charges</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-center">{{$orderDetails->order_handling_charge}}</td>
                                <td class="text-center">{{ $orderDetails->order_handling_charge_sgst }}</td>
                                <td class="text-center">{{ $orderDetails->order_handling_charge_cgst }}</td>
                                <td class="text-center">{{ $orderDetails->order_handling_charge + $orderDetails->order_handling_charge_sgst +$orderDetails->order_handling_charge_cgst }}</td>
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr class="hr-seperator">
                <div class="row">
                    <div class="col-md-6">
                        <div class="invoice-footer">
                        <div>
                            <p></p>
                            <p></p>
                            <p class="">
                                <span><strong>Authorized Signature</strong></span>
                            </p>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <p></p>
                        <p></p>
                        <p><strong>Total Amount: </strong> {{$total}}</p>
                        <p><strong>Seller Commission: </strong> {{number_format(((float)$total - $finaltotal) , '1') }}</p>
                        <p><strong>Grand Total: </strong> {{number_format((float)$finaltotal, '1')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @endif
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
@endpush
@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<style>
.seller-pricedetails li.sell-price {
    color: #333;
    text-align: center;
    font-size: 11px;
    font-weight: 700;
}

.fabpik-pricedetails li.sell-price span {
    color: #333;
    text-align: center;
    font-size: 11px;
    font-weight: 700;
}

.fabpik-pricedetails li.sell-discount span {
    color: #333;
    text-align: center;
    font-size: 11px;
    font-weight: 700;
}

.fabpik-pricedetails li.sell-discount {
    font-size: 11px;
}

.fabpik-pricedetails li.sell-price {
    font-size: 11px;
}

.badge.badge-light-success {
    background-color: rgba(40, 199, 111, .12);
    color: #28C76F !important;
}

.badge-pill {
    padding-right: .5rem;
    font-size: 12px !important;
    padding-left: .5rem;
    border-radius: 10rem;
}

.row-strip {
    padding-left: 0;
    padding: 7px 0;
    z-index: -1;
    margin: 0;
}

.badge {
    display: inline-block;
    padding: 4px 9px;
    margin-top: -5px;
    font-size: 85%;
    line-height: 1;
    height: 20px;
    text-align: center;
    white-space: nowrap;
    -webkit-transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out, background 0s, border 0s;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out, background 0s, border 0s;
    font-size: 12px !important;
}

.seller-pricedetails li.sell-mrp {
    font-size: 11px;
    color: #aaa;
    text-decoration: line-through;
}

.seller-pricedetails li.sell-discount {
    color: #e75d81;
    font-weight: 600;
    font-size: 5px;
    white-space: nowrap;
    text-align: center;
    font-size: 10px;
}

.product-details h4 {
    margin-top: 0;
    font-weight: 400 !important;
    text-transform: capitalize;
    line-height: 1.6;
    font-size: 11px;
    color: #222;
    width: 185px;
    margin-bottom: 0px;
    text-align: center;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
}

.modal-body {
    position: relative;
    padding: 15px;
    min-height: 70px;
    max-height: max-content;
}

.seller-pricedetails li {
    width: auto;
    display: inline-block;
    text-align: center;
}

.fabpik-pricedetails li {
    width: 48.9%;
    display: inline-block;
    text-align: center;
}

.label-name {
    font-size: 13px;
}

.variantdetails {
    padding-left: 0;
    text-align: center;
    margin: 0;
}

.variantdetails li span {
    width: 56%;
    display: inline-grid;
    font-weight: 600;
}

.variantdetails li {
    list-style: none;
    /* display: inline-block; */
    text-align: left;
    padding: 1px 6px;
    font-size: 11px;
}

.product-details .mt-checkbox {
    position: relative;
    top: 2px;
    left: 8px;
}

.product-details:focus {
    outline: none;
}

.seller-pricedetails {
    padding-left: 0;
    text-align: center;
    margin-bottom: 2px;
}

.fabpik-pricedetails {
    padding-left: 0;
    background: #eef0f8;
    padding: 4px 0;
    margin: 0;
}

.product-details a.view-more {
    font-size: 12px;
    float: right;
    padding: 0px 6px;
    background: #337ab72e;
    color: #337ab7;
    border-radius: 4px !important;
}

.code-border {
    text-align: center;
    border-radius: 50% !important;
    width: 20px;
    height: 20px;
    margin-right: 11px;
    justify-content: center;
    align-items: center;
    vertical-align: middle;
    display: inline-flex;
}

.color-code {
    width: 16px;
    height: 16px;
    position: relative;
    content: "";
    vertical-align: middle;
    border-radius: 50% !important;
    margin-bottom: 0;
    border: 1px solid #cac7f6;
    box-shadow: 0 1px 3px 0 #9ccae8;
    margin-top: 0;
}

.productlist-block {
    width: 20%;
}

#viewdetails li span {
    width: 39%;
    font-size: 12px;
    padding: 4px 2px;
    display: -webkit-inline-box;
    position: relative
}
.select2
{
    width: 100% !important;
}
.select2-container--bootstrap .select2-selection--multiple .select2-search--inline .select2-search__field {
    background: transparent;
    padding: 0 12px;
    height: 32px;
    line-height: 1.42857;
    margin-top: 0;
    min-width: 18em;
}
#viewdetails li span:after {
    content: ':';
    position: absolute;
    height: 100%;
    top: 3px;
    right: 11px;
}

#viewdetails li {
    font-size: 12px;
}

.coupons-groups .form-wizard .steps {
    padding: 0px 0;
    margin-bottom: 5px;
}

.coupons-groups .form .form-body {
    padding: 0px;
}

.coupons-groups .progress {
    height: 8px;
    margin-bottom: 20px;
    background-color: #f5f5f5;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 2px rgb(0 0 0 / 10%);
    box-shadow: inset 0 1px 2px rgb(0 0 0 / 10%);
    border-radius: 33px !important;
}

.coupons-groups .portlet.light.portlet-fit>.portlet-title {
    padding: 10px 20px 0px;
}

.minimize-card {
    border-radius: 4px !important;
    padding: 2px 12px;
}

.coupons-groups h3.block {
    padding-top: 0px;
    padding-bottom: 0px;
    font-size: 16px;
    margin: 0;
}

.coupons-groups .form-actions {
    float: right;
    margin-right: 16px;
    margin-top: 13px;
}

 
strong {
    font-weight: 600;
    font-size: 12px;
}

.filter-tab-btn {
    margin-bottom: 20px;
    display: flex;
    vertical-align: middle;
    align-items: center;

}


#select2-fchildcategory-results .select2-results__option:before {
    content: "";
    display: inline-block;
    position: relative;
    height: 20px;
    width: 20px;
    border: 2px solid #e9e9e9;
    border-radius: 4px;
    background-color: #fff;
    margin-right: 20px;
    vertical-align: middle;
}

#select2-fchildcategory-results .select2-results__option[aria-selected=true]:before {
    font-family: fontAwesome;
    content: "\f00c";
    color: #fff;
    background-color: #f77750;
    border: 0;
    display: inline-block;
    padding-left: 3px;
}

#select2-fchildcategory-results .select2-container--default .select2-results__option[aria-selected=true] {
    background-color: #fff;
}

#select2-fchildcategory-results .select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: #eaeaeb;
    color: #272727;
}

#select2-fchildcategory-results .select2-container--default .select2-selection--multiple {
    margin-bottom: 10px;
}

#select2-fchildcategory-results .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
    border-radius: 4px;
}

#select2-fchildcategory-results .select2-container--default.select2-container--focus .select2-selection--multiple {
    border-color: #f77750;
    border-width: 2px;
}

#select2-fchildcategory-results .select2-container--default .select2-selection--multiple {
    border-width: 2px;
}

#select2-fchildcategory-results .select2-container--open .select2-dropdown--below {

    border-radius: 6px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);

}

#select2-fchildcategory-results .select2-selection .select2-selection--multiple:after {
    content: 'hhghgh';
}


.zoom {
  /* padding: 50px;
  background-color: green; */
  transition: transform .2s; /* Animation */
  /* width: 200px;
  height: 200px; */
  margin: 0 auto;
}

.zoom:hover {
  transform: scale(2); /* (150% zoom)*/
}
</style>
@endpush
@section('content')
<div class="portlet-body form">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered coupons-groups" id="form_wizard_1">
                <div class="portlet-body form">
                    <div class="form-wizard">
                        <div class="form-body">
                            <ul class="nav nav-pills nav-justified steps">
                                <li>
                                    <a href="{{route('admin.coupongroups.edit',['id'=>$groupId])}}" class="step">
                                        <span class="number"> 1 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Group Details </span>
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="#" class="step ">
                                        <span class="number"> 2 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Product Selection </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('admin.coupongroups.finalVariants', ['id'=>$groupId])}}"
                                        class="step ">
                                        <span class="number"> 3 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Final Review </span>
                                    </a>
                                </li>
                            </ul>

                            <div id="bar" class="progress progress-striped" role="progressbar">
                                <div class="progress-bar progress-bar-success" style="width:66.66%"> </div>
                            </div>

                            <div class="tab-content">
                                <div class="alert alert-danger display-none">
                                    <button class="close" data-dismiss="alert"></button> You have some form errors.
                                    Please check below.
                                </div>
                                <div class="alert alert-success display-none">
                                    <button class="close" data-dismiss="alert"></button> Your form validation is
                                    successful!
                                </div>
                                <div class="tab-pane active" id="tab2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Begin: life time stats -->
                                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                                <div class="portlet-title">
                                                    <div class="card table-filters-design">
                                                        <div class="filter-tab-btn card-option row">
                                                            <div class="col-lg-6 ">
                                                                <h3 class="block">Click Filters </h3>
                                                            </div>
                                                            <div class="col-lg-6 text-right">
                                                                <span class="minimize-card btn btn-primary "><i
                                                                        ata-feather="filter"></i>Filters</span>
                                                                <span style="display:none"><i
                                                                        data-feather="filter"></i>Filters</span></li>
                                                            </div>
                                                        </div>
                                                        <!--Search Form -->
                                                        <div id="card-body" class="card-body" style="display:none;">
                                                            <!-- FILTER FORM START -->

                                                            <div class="form-body">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label class="col-md-3 pd-tb-5"><strong>Seller Price:</strong>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control input-sm" name="fspricefrom" id="fspricefrom" placeholder="Min" value="{{request()->fspricefrom}}">
                                                                                <span class="input-group-addon"> - </span>
                                                                                <input type="text" class="form-control input-sm" name="fspriceto" id="fspriceto" placeholder="Max" value="{{request()->fspriceto}}">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <label class="col-md-3 pd-tb-5"><strong>Seller Discount
                                                                                (%):</strong> </label>
                                                                        <div class="col-md-9">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control input-sm" name="fspdiscountfrom" id="fspdiscountfrom" placeholder="Min" value="{{request()->fspdiscountfrom}}">
                                                                                <span class="input-group-addon"> -
                                                                                </span>
                                                                                <input type="text" class="form-control input-sm" name="fspdiscountto" id="fspdiscountto" placeholder="Max"
                                                                                    value="{{request()->fspdiscountto}}">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <label class="col-md-3 pd-tb-5"><strong>Seller Discount
                                                                                (INR):</strong> </label>
                                                                        <div class="col-md-9">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control input-sm" name="fsdiscountfrom" id="fsdiscountfrom" placeholder="Min" value="{{request()->fsdiscountfrom}}">
                                                                                <span class="input-group-addon"> -
                                                                                </span>
                                                                                <input type="text" class="form-control input-sm" name="fsdiscountto" id="fsdiscountto" placeholder="Max" value="{{request()->fsdiscountto}}">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label class="col-md-3 pd-tb-5"><strong>Fabpik Price:</strong>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control input-sm" name="fpricefrom" id="fpricefrom" placeholder="Min" value="{{request()->fpricefrom}}">
                                                                                <span class="input-group-addon"> - </span>
                                                                                <input type="text" class="form-control input-sm" name="fpriceto" id="fpriceto" placeholder="Max" value="{{request()->fpriceto}}">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <label class="col-md-3 pd-tb-5"><strong>Fabpik Discount
                                                                                (%):</strong> </label>
                                                                        <div class="col-md-9">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control input-sm" name="fpdiscountfrom" id="fpdiscountfrom" placeholder="Min" value="{{request()->fpdiscountfrom}}">
                                                                                <span class="input-group-addon"> -
                                                                                </span>
                                                                                <input type="text" class="form-control input-sm" name="fpdiscountto" id="fpdiscountto" placeholder="Max"
                                                                                    value="{{request()->fpdiscountto}}">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <label class="col-md-3 pd-tb-5"><strong>Fabpik Discount
                                                                                (INR):</strong> </label>
                                                                        <div class="col-md-9">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control input-sm" name="fdiscountfrom" id="fdiscountfrom" placeholder="Min" value="{{request()->fdiscountfrom}}">
                                                                                <span class="input-group-addon"> -
                                                                                </span>
                                                                                <input type="text" class="form-control input-sm" name="fdiscountto" id="fdiscountto" placeholder="Max" value="{{request()->fdiscountto}}">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">

                                                                    <div class="col-md-4">
                                                                        <label
                                                                            class="col-md-3 pd-tb-5"><strong>Name:</strong>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" class="form-control input-sm" name="fname" id="fname" value="{{request()->fname}}" autocomplete="off">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <label
                                                                            class="col-md-3 pd-tb-5"><strong>Brand:</strong>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            @php $brands =
                                                                            App\Models\Brand::where('status',1)->get();
                                                                            @endphp
                                                                            <select name="fbrand" id="fbrand"
                                                                                class="form-control form-filter input-sm select2">
                                                                                <option value="">Select Brand</option>
                                                                                @if(isset($brands))
                                                                                @foreach($brands as $brand)
                                                                                <option value="{{$brand->id}}" {{request()->fbrand==$brand->id?'selected="selected"':''}}>
                                                                                    {{$brand->name}}
                                                                                </option>
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <label class="col-md-3"><strong>Seller:</strong>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            @php $sellers =
                                                                            App\Models\Seller::where('status',1)->get();
                                                                            @endphp
                                                                            <select name="fseller" id="fseller"
                                                                                class="form-control form-filter input-sm select2">
                                                                                <option value="">Select Seller</option>
                                                                                @if(isset($sellers))
                                                                                @foreach($sellers as $seller)
                                                                                <option value="{{$seller->id}}"
                                                                                    {{request()->fseller==$seller->id?'selected="selected"':''}}>
                                                                                    {{$seller->company_name}}
                                                                                </option>
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="row">

                                                                    <div class="col-md-4">
                                                                        <label
                                                                            class="col-md-3 pd-tb-5"><strong>Category:</strong>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            @php $categories =
                                                                            App\Models\Category::where('status',1)->get();
                                                                            @endphp
                                                                            <select name="fcategory" id="fcategory"
                                                                                class="form-control form-filter input-sm js-select2">
                                                                                <option value="">Select Category </option>
                                                                                @if(isset($categories))
                                                                                @foreach($categories as $category)
                                                                                <option value="{{$category->id}}"
                                                                                    {{request()->fcategory==$category->id?'selected="selected"':''}}>
                                                                                    {{$category->title}}
                                                                                </option>
                                                                                @endforeach
                                                                                @endif
                                                                            </select>

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <label class="col-md-3 pd-tb-5"><strong>Sub
                                                                                Category:</strong> </label>
                                                                        <div class="col-md-9 pd-tb-5">
                                                                            @php $subcategories =
                                                                            App\Models\Subcategory::where('category_id',
                                                                            request()->fcategory)->where('status',1)->get();
                                                                            @endphp
                                                                            <select name="fsubcategory"
                                                                                id="fsubcategory"
                                                                                class="form-control form-filter input-sm select2">
                                                                                <option value="">Select Sub-category
                                                                                </option>
                                                                                @if(isset($subcategories))
                                                                                @foreach($subcategories as $subcategory)
                                                                                <option value="{{$subcategory->id}}"
                                                                                    {{request()->fsubcategory==$subcategory->id?'selected="selected"':''}}>
                                                                                    {{$subcategory->title}}
                                                                                </option>
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4 checkboxChildCategory">
                                                                        <label class="col-md-3 pd-tb-5"><strong>Child
                                                                                Category:</strong> </label>
                                                                        <div class="col-md-9  child-categorycheckbox pd-tb-5">
                                                                            @php $childcategories =
                                                                            App\Models\ChildCategory::where('category_id',
                                                                            request()->fcategory)->where('subcategory_id',
                                                                            request()->fsubcategory)->where('status',1)->get();
                                                                            @endphp
                                                                            <select name="fchildcategory[]"
                                                                                id="fchildcategory"
                                                                                class="form-control form-filter input-sm select2"
                                                                                multiple="multiple">
                                                                                <option value="">Select Child Category
                                                                                </option>
                                                                                @if(isset($childcategories))
                                                                                @foreach($childcategories as
                                                                                $childcategory)
                                                                                <option value="{{$childcategory->id}}"
                                                                                    {{in_array($childcategory->id, explode(",", request()->fchildcategory)) ? 'selected="selected"' : ''}}>
                                                                                    {{$childcategory->title}}
                                                                                </option>
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <label
                                                                            class="col-md-3 pd-tb-5"><strong>Action:</strong>
                                                                        </label>
                                                                        <div class="col-md-9">

                                                                            <button type="button"
                                                                                class="btn btn-sm btn-icon-only white btn_submit_search"
                                                                                name="search" id="btn_submit_search"
                                                                                value="search"
                                                                                data-loading-text="<i class='fa fa-spinner fa-spin'></i>"
                                                                                title="Apply Filter"><span
                                                                                    aria-hidden="true"
                                                                                    class="icon-magnifier"></span></button>

                                                                            <a class="btn btn-sm btn-icon-only white"
                                                                                href="{{route('admin.coupongroups.selectVariants', ['id'=>$groupId])}}"
                                                                                title="Reset Filter">
                                                                                <span aria-hidden="true"
                                                                                    class="icon-refresh"></span></a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <form action="{{route('admin.coupongroups.storeGroup')}}" method="post"
                                                    id="select_variants_form" class="filter-form">
                                                    {!! csrf_field() !!}



                                                    <input type="hidden" value="{{$groupId}}" name="groupId" />

                                                    <input type="hidden" id="fpricefrom_hidden" name ="fpricefrom_hidden" value="" />
                                                    <input type="hidden" id="fpriceto_hidden" name ="fpriceto_hidden" value="" />
                                                    <input type="hidden" id="fdiscountfrom_hidden" name ="fdiscountfrom_hidden" value="" />
                                                    <input type="hidden" id="fdiscountto_hidden" name ="fdiscountto_hidden" value="" />
                                                    <input type="hidden" id="fpdiscountfrom_hidden" name ="fpdiscountfrom_hidden" value="" />
                                                    <input type="hidden" id="fpdiscountto_hidden" name ="fpdiscountto_hidden" value="" />

                                                    <input type="hidden" id="fspricefrom_hidden" name ="fspricefrom_hidden" value="" />
                                                    <input type="hidden" id="fspriceto_hidden" name ="fspriceto_hidden" value="" />
                                                    <input type="hidden" id="fsdiscountfrom_hidden" name ="fsdiscountfrom_hidden" value="" />
                                                    <input type="hidden" id="fsdiscountto_hidden" name ="fsdiscountto_hidden" value="" />
                                                    <input type="hidden" id="fspdiscountfrom_hidden" name ="fspdiscountfrom_hidden" value="" />
                                                    <input type="hidden" id="fspdiscountto_hidden" name ="fspdiscountto_hidden" value="" />

                                                    <input type="hidden" id="fseller_hidden" name ="fseller_hidden" value="" />
                                                    <input type="hidden" id="fbrand_hidden" name ="fbrand_hidden" value="" />
                                                    <input type="hidden" id="fcategories_hidden" name ="fcategories_hidden" value="" />
                                                    <input type="hidden" id="fname_hidden" name ="fname_hidden" value="" />
                                                    <input type="hidden" id="fcompanyname_hidden" name ="fcompanyname_hidden" value="" />
                                                    <input type="hidden" id="fcategory_hidden" name ="fcategory_hidden" value="" />
                                                    <input type="hidden" id="fsubcategory_hidden" name ="fsubcategory_hidden" value="" />
                                                    <input type="hidden" id="fchildcategory_hidden" name ="fchildcategory_hidden" value="" />

                                                    <div class="form-body">
                                                        <div
                                                            class="form-group {{($errors->first('variantCheckbox'))?'has-error':''}}">
                                                            <span
                                                                class="help-block">{{$errors->first('variantCheckbox')}}</span>
                                                        </div>

                                                        <input type="checkbox" style="margin-left:10px;" id="checkall"
                                                            name="checkall">
                                                        <label for="checkall">Select all</label>

                                                        <div class="portlet-body" style="margin:10px;">
                                                            <table
                                                                class="table dt-responsive nowrap product-list-table text-center"
                                                                id="dt_listing">
                                                                <thead>
                                                                    <tr role="row" class="heading">
                                                                        <th width="4%" class="">&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <label
                                                                                class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                <input type="checkbox"
                                                                                    class="group-checkable"
                                                                                    data-set="#sample_2 .checkboxes" />
                                                                                <span></span>
                                                                            </label>
                                                                        </th>
                                                                        <th width="10%" class="text-center"> ID </th>
                                                                        <th width="17%" class="text-center"> Name </th>
                                                                        <th width="10%" class="text-center"> Image </th>
                                                                        <th width="13%" class="none"> Category </th>
                                                                        <th class="none"> SKU </th>
                                                                        <th width="7%" class="text-center"> MRP </th>
                                                                        <th width="10%" class="text-center"> Sale/<br>Selling Price </th>
                                                                        <th width="10%" class="text-center"> Selle  <br>Discount (INR) </th>
                                                                        <th width="10%" class="text-center"> Fabpik <br>Selling Price </th>
                                                                        <th width="10%" class="text-center"> Fabpik <br>Discount (INR) </th>
                                                                        <th width="20%" class="text-center"> Fabpik <br>Discount (%) </th>
                                                                        <th width="20%" class="none"> Special Price Start Date </th>
                                                                        <th width="20%" class="none"> Special Price End Date </th>
                                                                        <th width="" class="none"> Primary Variant: </th>
                                                                        <th width="" class="none"> Secondary Variant:</th>
                                                                        <th width="" class="none"> Created at: </th>
                                                                        <th width="" class="none"> Modified at: </th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody> </tbody>

                                                            </table>

                                                        </div>
                                                    </div>

                                                    
                                            </div>
                                        </div>
                                        <div class="row float-right text-right">
                                            <div class="col-md-offset-3 col-md-9" style="margin-bottom:10px;marigin-right:10px">
                                                <button type="button" class="btn btn-secondary-outline" id="step_btn_back">
                                                    <span aria-hidden="true" class="icon-arrow-left"></span>
                                                        Back
                                                </button>

                                                <button type="submit" class="btn blue form-submit" name="save" value="prodvarsave" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing...">
                                                    <i class="fa fa-check-circle"></i> Save
                                                </button>

                                                <button type="submit" class="btn blue form-submit" id="btn_continue" name="savecontinue" value="savecontinue" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing...">
                                                    <i class="fa fa-step-forward"></i> Save & Continue
                                                </button>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>

@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var CouponAddEdit = function() {
    return {
        //main function to initiate the module
        init: function() {

            $('#select_variants_form').submit(function() {
                // set our summary div inputs values with our form values
                document.getElementById("fpricefrom_hidden").value = ($('#fpricefrom').val() != '') ? $('#fpricefrom').val() : null;
                document.getElementById("fpriceto_hidden").value = ($('#fpriceto').val() != '') ? $('#fpriceto').val() : null;
                document.getElementById("fdiscountfrom_hidden").value = ($('#fdiscountfrom').val() != '') ? $('#fdiscountfrom').val() : null;
                document.getElementById("fdiscountto_hidden").value = ($('#fdiscountto').val() != '') ? $('#fdiscountto').val() : null;
                document.getElementById("fpdiscountfrom_hidden").value = ($('#fpdiscountfrom').val() != '') ? $('#fpdiscountfrom').val() : null;
                document.getElementById("fpdiscountto_hidden").value = ($('#fpdiscountto').val() != '') ? $('#fpdiscountto').val() : null;
                
                document.getElementById("fspricefrom_hidden").value = ($('#fspricefrom').val() != '') ? $('#fspricefrom').val() : null;
                document.getElementById("fspriceto_hidden").value = ($('#fspriceto').val() != '') ? $('#fspriceto').val() : null;
                document.getElementById("fsdiscountfrom_hidden").value = ($('#fsdiscountfrom').val() != '') ? $('#fsdiscountfrom').val() : null;
                document.getElementById("fsdiscountto_hidden").value = ($('#fsdiscountto').val() != '') ? $('#fsdiscountto').val() : null;
                document.getElementById("fspdiscountfrom_hidden").value = ($('#fspdiscountfrom').val() != '') ? $('#fspdiscountfrom').val() : null;
                document.getElementById("fspdiscountto_hidden").value = ($('#fspdiscountto').val() != '') ? $('#fspdiscountto').val() : null;

                document.getElementById("fseller_hidden").value = ($('#fseller').val() != '') ? $('#fseller').val() : null;
                document.getElementById("fbrand_hidden").value = ($('#fbrand').val() != '') ? $('#fbrand').val() : null;
                document.getElementById("fname_hidden").value = ($('#fname').val() != '') ? $('#fname').val() : null;
                document.getElementById("fcategory_hidden").value = ($('#fcategory').val() != '') ? $('#fcategory').val() : null;
                document.getElementById("fsubcategory_hidden").value = ($('#fsubcategory').val() != '') ? $('#fsubcategory').val() : null;
                document.getElementById("fchildcategory_hidden").value = ($('#fchildcategory').val() != '') ? $('#fchildcategory').val() : null;

            });
            
            //select all product variants
            $(document).on("keypress", "input", function(e) {
                if (e.which == 13) {
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

            var table = $('#dt_listing').DataTable();

            $('#checkall').click(function(event) { //on click 

                var checked = this.checked;
                table.column(0).nodes().to$().each(function(index) {
                    // console.log(`For index ${index}`);
                    if (checked) {
                        $(this).find('.checkBoxClass').prop('checked', 'checked');
                    } else {
                        $(this).find('.checkBoxClass').removeProp('checked');
                    }
                });
            });


            $(".card-option .minimize-card").on("click", function() {
                var e = $(this),
                    o = $(e.parents(".card"));
                $(o).children(".card-block").slideToggle(),
                    $(o).children(".card-body").slideToggle();

                o.hasClass("full-card") || $(o).css("height", "auto"), $(this).children("a").children(
                    "span").toggle()

            })

            $.fn.select2.defaults.set("theme", "bootstrap");

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.coupongroups')}}";
            });

            $('#step_btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.coupongroups.edit',['id'=>$groupId])}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            // Do Filter
            $('.btn_submit_search').on('click', function(e) {

                e.preventDefault();

                var url = "{{route('admin.coupongroups.selectVariants',['id'=>$groupId])}}?search=1";

                if ($('#fpricefrom').val() != '') url += "&fpricefrom=" + encodeURIComponent($('#fpricefrom').val());
                if ($('#fpriceto').val() != '') url += "&fpriceto=" + encodeURIComponent($('#fpriceto').val());
                if ($('#fdiscountfrom').val() != '') url += "&fdiscountfrom=" + encodeURIComponent($('#fdiscountfrom').val());
                if ($('#fdiscountto').val() != '') url += "&fdiscountto=" + encodeURIComponent($('#fdiscountto').val());
                if ($('#fpdiscountfrom').val() != '') url += "&fpdiscountfrom=" + encodeURIComponent($('#fpdiscountfrom').val());
                if ($('#fpdiscountto').val() != '') url += "&fpdiscountto=" + encodeURIComponent($('#fpdiscountto').val());

                if ($('#fspricefrom').val() != '') url += "&fspricefrom=" + encodeURIComponent($('#fspricefrom').val());
                if ($('#fspriceto').val() != '') url += "&fspriceto=" + encodeURIComponent($('#fspriceto').val());
                if ($('#fsdiscountfrom').val() != '') url += "&fsdiscountfrom=" + encodeURIComponent($('#fsdiscountfrom').val());
                if ($('#fsdiscountto').val() != '') url += "&fsdiscountto=" + encodeURIComponent($('#fsdiscountto').val());
                if ($('#fspdiscountfrom').val() != '') url += "&fspdiscountfrom=" + encodeURIComponent($('#fspdiscountfrom').val());
                if ($('#fspdiscountto').val() != '') url += "&fspdiscountto=" + encodeURIComponent($('#fspdiscountto').val());

                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fbrand').val()) url += "&fbrand=" + encodeURIComponent($('#fbrand').val());
                if ($('#fcategories').val()) url += "&fcategories=" + encodeURIComponent($('#fcategories').val());
                if ($('#fname').val()) url += "&fname=" + encodeURIComponent($('#fname').val());
                if ($('#fcompanyname').val()) url += "&fcompanyname=" + encodeURIComponent($('#fcompanyname').val());
                if ($('#fcategory').val()) url += "&fcategory=" + encodeURIComponent($('#fcategory').val());
                if ($('#fsubcategory').val()) url += "&fsubcategory=" + encodeURIComponent($('#fsubcategory').val());
                if ($('#fchildcategory').val()) url += "&fchildcategory=" + encodeURIComponent($('#fchildcategory').val());

                window.location.href = url;
            });

            //Seller AutoComplete
            $('#fseller').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.sellerAutoComplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: $.map(data, function(categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function(categoryArr) {
                    return categoryArr.text;
                },
            });

            //Category Auto Complete
            $('#fcategory').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                // closeOnSelect: false,
                ajax: {
                    url: "{{route('ajax.categoryAutoComplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: $.map(data, function(categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function(categoryArr) {
                    return categoryArr.text;
                },
            });

            $("body").on('change', '#fcategory', function() {
                var parentId = $('#fcategory').val();
                if (parentId != '0' && parentId != undefined && parentId != null && parentId != '') {
                    $.ajax({
                        type: "POST",
                        url: "{{route('ajax.getSubCategories')}}",
                        data: {
                            id: parentId
                        },
                        dataType: "JSON",
                        success: function(response) {
                            console.log(response);
                            var len = response.list.length;
                            $("#fsubcategory").empty();
                            $("#fsubcategory").append("<option value='' >Select</option>");
                            for (var i = 0; i < len; i++) {
                                var id = response.list[i]['id'];
                                var name = response.list[i]['title'];
                                $("#fsubcategory").append(
                                    "<option @php (old('subcategory')=='" + id +
                                    "') ? 'selected' :'' @endphp value='" + id + "'>" +
                                    name + "</option>");
                            }
                        }
                    });
                }
            });

            $("body").on('change', '#fsubcategory', function() {
                var parentId = $('#fsubcategory').val();
                if (parentId != '0' && parentId != undefined && parentId != null && parentId != '') {
                    $.ajax({
                        type: "POST",
                        url: "{{route('ajax.getChildCategoriesList')}}",
                        data: {
                            id: parentId
                        },
                        dataType: "JSON",
                        success: function(response) {
                            console.log(response);
                            var len = response.list.length;
                            $("#fchildcategory").empty();
                            // $("#fchildcategory").append("<option value='' >Select</option>");
                            for (var i = 0; i < len; i++) {
                                var id = response.list[i]['id'];
                                var name = response.list[i]['title'];
                                $("#fchildcategory").append(
                                    "<option @php (old('subcategory')=='" + id +
                                    "') ? 'selected' :'' @endphp value='" + id + "'>" +
                                    name + "</option>");
                            }
                        }
                    });
                }
            });

            $('#fsubcategory').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Sub Category',
            });

            $('#fchildcategory').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Child Category',
                closeOnSelect: false,
                containerCssClass : "error",
                allowHtml: true,
                tags: true
            });

            $('#fbrand').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Brand',
            });

        }
    };
}();

jQuery(document).ready(function() {
    CouponAddEdit.init();

    if (window.location.href.indexOf("search") > -1) {
        $("#card-body").show();
    }
});
</script>

@endpush
@extends(ADMIN_THEME_NAME.'.layouts.master') @push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' )}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/font-awesome/css/font-awesome.min.css' )}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/simple-line-icons/simple-line-icons.min.css' )}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/bootstrap/css/bootstrap.min.css' )}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' )}}" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ __common_asset('global/plugins/select2/css/select2.min.css' )}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/select2/css/select2-bootstrap.min.css' )}}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL STYLES -->
<link href="{{ __common_asset('global/css/components.min.css' )}}" rel="stylesheet" id="style_components"type="text/css" />
<link href="{{ __common_asset('global/css/plugins.min.css' )}}" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->

<!-- BEGIN THEME LAYOUT STYLES -->
<link href="{{ __common_asset('layouts/layout/css/layout.min.css' )}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('layouts/layout/css/themes/darkblue.min.css' )}}" rel="stylesheet" type="text/css" id="style_color" />
<link href="{{ __common_asset('layouts/layout/css/custom.min.css' )}}" rel="stylesheet" type="text/css" />

<style>
.select2-results__option {
    padding: 0px 4px !important;
}

.select2-result-repository__title {
    margin-bottom: 0px !important;
}

.select2-result-repository {
    padding-top: 3px !important;
    padding-bottom: 3px !important;
}

img {
    height: auto;
    max-width: 100%;
}

.product-details {
    padding: 0px 0px 7px;
    background: #fff;
    z-index: 999;
    border-top: 0 !important;
    border: 1px solid #cac7f6;
    box-shadow: 0 1px 1px #9ccae8;
    position: relative;
    vertical-align: middle;
}

.seller-pricedetails li.sell-price {
    color: #333;
    text-align: center;
    font-size: 11px;
    font-weight: 700;
}

.fabpik-pricedetails li.sell-price span {
    color: #333;
    text-align: center;
    font-size: 11px;
    font-weight: 700;
}

.fabpik-pricedetails li.sell-discount span {
    color: #333;
    text-align: center;
    font-size: 11px;
    font-weight: 700;
}

.fabpik-pricedetails li.sell-discount {
    font-size: 11px;
}

.fabpik-pricedetails li.sell-price {
    font-size: 11px;
}

.badge.badge-light-success {
    background-color: rgba(40, 199, 111, .12);
    color: #28C76F !important;
}

.badge-pill {
    padding-right: .5rem;
    font-size: 12px !important;
    padding-left: .5rem;
    border-radius: 10rem;
}

.row-strip {
    padding-left: 0;
    padding: 7px 0;
    z-index: -1;
    margin: 0;
}

.badge {
    display: inline-block;
    padding: 4px 9px;
    margin-top: -5px;
    font-size: 85%;
    line-height: 1;
    height: 20px;
    text-align: center;
    white-space: nowrap;
    -webkit-transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out, background 0s, border 0s;
    transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out, background 0s, border 0s;
    font-size: 12px !important;
}

.seller-pricedetails li.sell-mrp {
    font-size: 11px;
    color: #aaa;
    text-decoration: line-through;
}

.seller-pricedetails li.sell-discount {
    color: #e75d81;
    font-weight: 600;
    font-size: 5px;
    white-space: nowrap;
    text-align: center;
    font-size: 10px;
}

.product-details h4 {
    margin-top: 0;
    font-weight: 400 !important;
    text-transform: capitalize;
    line-height: 1.6;
    font-size: 11px;
    color: #222;
    width: 185px;
    margin-bottom: 0px;
    text-align: center;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
}

.modal-body {
    position: relative;
    padding: 15px;
    min-height: 70px;
    max-height: max-content;
}

.seller-pricedetails li {
    width: auto;
    display: inline-block;
    text-align: center;
}

.fabpik-pricedetails li {
    width: 48.9%;
    display: inline-block;
    text-align: center;
}

.label-name {
    font-size: 13px;
}

.variantdetails {
    padding-left: 0;
    text-align: center;
    margin: 0;
}

.variantdetails li span {
    width: 56%;
    display: inline-grid;
    font-weight: 600;
}

.variantdetails li {
    list-style: none;
    /* display: inline-block; */
    text-align: left;
    padding: 1px 6px;
    font-size: 11px;
}

.product-details .mt-checkbox {
    position: relative;
    top: 2px;
    left: 8px;
}

.product-details:focus {
    outline: none;
}

.seller-pricedetails {
    padding-left: 0;
    text-align: center;
    margin-bottom: 2px;
}

.fabpik-pricedetails {
    padding-left: 0;
    background: #eef0f8;
    padding: 4px 0;
    margin: 0;
}

.product-details a.view-more {
    font-size: 12px;
    float: right;
    padding: 0px 6px;
    background: #337ab72e;
    color: #337ab7;
    border-radius: 4px !important;
}

.code-border {
    text-align: center;
    border-radius: 50% !important;
    width: 20px;
    height: 20px;
    margin-right: 11px;
    justify-content: center;
    align-items: center;
    vertical-align: middle;
    display: inline-flex;
}

.color-code {
    width: 16px;
    height: 16px;
    position: relative;
    content: "";
    vertical-align: middle;
    border-radius: 50% !important;
    margin-bottom: 0;
    border: 1px solid #cac7f6;
    box-shadow: 0 1px 3px 0 #9ccae8;
    margin-top: 0;
}

.productlist-block {
    width: 20%;
}

#viewdetails li span {
    width: 39%;
    font-size: 12px;
    padding: 4px 2px;
    display: -webkit-inline-box;
    position: relative
}

#viewdetails li span:after {
    content: ':';
    position: absolute;
    height: 100%;
    top: 3px;
    right: 11px;
}

#viewdetails li {
    font-size: 12px;
}

.coupons-groups .form-wizard .steps {
    padding: 0px 0;
    margin-bottom: 5px;
}

.coupons-groups .form .form-body {
    padding: 0px;
}

.coupons-groups .progress {
    height: 8px;
    margin-bottom: 20px;
    background-color: #f5f5f5;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 2px rgb(0 0 0 / 10%);
    box-shadow: inset 0 1px 2px rgb(0 0 0 / 10%);
    border-radius: 33px !important;
}

.coupons-groups .portlet.light.portlet-fit>.portlet-title {
    padding: 10px 20px 0px;
}

.minimize-card {
    border-radius: 4px !important;
    padding: 2px 12px;
}

.coupons-groups h3.block {
    padding-top: 0px;
    padding-bottom: 0px;
    font-size: 16px;
    margin: 0;
}

.coupons-groups .form-actions {
    float: right;
    margin-right: 16px;
    margin-top: 13px;
}

.coupons-groups .select2-container {
    width: 100% !important;
    ;
}

strong {
    font-weight: 600;
    font-size: 12px;
}

.filter-tab-btn {
    margin-bottom: 20px;
    display: flex;
    vertical-align: middle;
    align-items: center;

}

.table-filters-design label {
    padding: 0;
    margin-bottom: 19px;
}
</style> 
@endpush 
@section('content') 

<div class="portlet-body form">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered coupons-groups" id="form_wizard_1">
                <div class="portlet-body form">
                @if(isset($editcoupon))
                    <form class="form-horizontal" action="{{route('admin.coupongroups.edit',['id'=>$id])}}" method="post">
                    @else
                    <form class="form-horizontal" action="{{route('admin.coupongroups.addGroupName')}}" method="POST">
                    @endif
                    {!! csrf_field() !!}
                        <div class="form-wizard">
                            <div class="form-body">
                                <ul class="nav nav-pills nav-justified steps">
                                    <li>
                                        <a href="#tab1" data-toggle="tab" class="step">
                                            <span class="number"> 1 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Group Details </span>
                                        </a>
                                    </li>
                                    <li>
                                    @if(isset($editcoupon))
                                        <a href="{{route('admin.coupongroups.selectVariants', ['id'=>$id])}}" class="step ">
                                    @else
                                        <a href="#tab2" data-toggle="tab" class="step ">
                                    @endif
                                            <span class="number"> 2 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Product Selection </span>
                                        </a>
                                    </li>
                                    <li>
                                    @if(isset($editcoupon))
                                        <a href="{{route('admin.coupongroups.finalVariants', ['id'=>$id])}}" class="step ">
                                    @else
                                        <a href="#tab3" data-toggle="tab" class="step ">
                                    @endif
                                        <span class="number"> 3 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Final Review </span>
                                        </a>
                                    </li>
                                </ul>

                                <div id="bar" class="progress progress-striped" role="progressbar">
                                    <div class="progress-bar progress-bar-success"> </div>
                                </div>

                                <div class="tab-content">
                                    
                                    <div class="alert alert-danger display-none">
                                        <button class="close" data-dismiss="alert"></button> You have some form errors.
                                        Please check below.
                                    </div>

                                    <div class="alert alert-success display-none">
                                        <button class="close" data-dismiss="alert"></button> Your form validation is
                                        successful!
                                    </div>

                                    <div class="tab-pane " id="tab1">
                                        <h2 class="block">Enter Details</h2>


                                        <div class="form-group {{($errors->first('group_name'))?'has-error':''}}">
                                            <label class="control-label col-md-3">Group Name <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="group_name"  placeholder="Ex: Summer Group"
                                                    value="{{old('group_name', isset($editcoupon)?$editcoupon->group_name:'')}}" />
                                                <span class="help-block">{{$errors->first('group_name')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{($errors->first('description'))?'has-error':''}}" >
                                            <label class="control-label col-md-3">Description <span class="required"> *
                                                </span>
                                            </label>
                                            <div class="col-md-4">
                                                <textarea class="form-control" rows="3" name="description">{{old('description', isset($editcoupon)?$editcoupon->description:'') }}</textarea>
                                                (Max Character Length 255.)
                                                <span class="help-block">{{$errors->first('description')}}</span>
                                            </div>
                                        </div>

                                        @if(isset($editcoupon))
                                        <div class="form-group {{($errors->first('description'))?'has-error':''}}" >
                                            <label class="control-label col-md-3">Status <span class="required"> *
                                                </span>
                                            </label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="status">
                                                    <option value="">Select</option>
                                                    @foreach([0=>'Inactive', 1=>'Active'] as $val=>$label)
                                                    <option value="{{$val}}" {{ old('status', (isset($editcoupon)?$editcoupon->status:'') )==$val ? 'selected' : '' }}>
                                                        {{$label}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block">{{$errors->first('status')}}</span>
                                            </div>
                                        </div>
                                        @endif 

                                        <div class="row float-right text-right">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="button" class="btn btn-secondary-outline" id="btn_back"><span aria-hidden="true" class="icon-arrow-left"></span>
                                                    Back
                                                </button>
                                                @if(isset($editcoupon))
                                                <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing...">
                                                    <i class="fa fa-check-circle"></i> Save
                                                </button>
                                                @endif

                                                <button type="submit" class="btn blue form-submit" name="save" value="grpnamesave" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing...">
                                                    <i class="fa fa-check-circle"></i> Save &amp; Continue
                                                </button>
                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> 

@stop 

@push('PAGE_ASSETS_JS')

<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"> </script> 
<script src="{{ __common_asset('global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"> </script>
<script src="{{ __common_asset('global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"> </script>
<script src="{{ __common_asset('global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/scripts/app.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('pages/scripts/form-wizard.min.js') }}" type="text/javascript"></script> 

@endpush 
    
@push('PAGE_SCRIPTS') 
<script type="text/javascript">

var CouponAddEdit = function() {
    return {
        //main function to initiate the module
        init: function() {

            $(".card-option .minimize-card").on("click", function() {
                var e = $(this),
                    o = $(e.parents(".card"));
                $(o).children(".card-block").slideToggle(),
                $(o).children(".card-body").slideToggle();

                o.hasClass("full-card") || $(o).css("height", "auto"), $(this).children("a").children("span").toggle()

            })

            $.fn.select2.defaults.set("theme", "bootstrap");
            
            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.coupongroups')}}";
            });
            
            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            // Do Filter
            $('.btn_submit_search').on('click', function(e) {
                e.preventDefault();
                
                var url = "{{route('admin.coupongroups.getProductVariantListData')}}?search=1";

                if ($('#fpricefrom').val() != '') url += "&fpricefrom=" + encodeURIComponent($('#fpricefrom').val());
                if ($('#fpriceto').val() != '') url += "&fpriceto=" + encodeURIComponent($('#fpriceto').val());
                if ($('#fdiscountfrom').val() != '') url += "&fdiscountfrom=" + encodeURIComponent($('#fdiscountfrom').val());
                if ($('#fdiscountto').val() != '') url += "&fdiscountto=" + encodeURIComponent($('#fdiscountto').val());
                if ($('#fpdiscountfrom').val() != '') url += "&fpdiscountfrom=" + encodeURIComponent($('#fpdiscountfrom').val());
                if ($('#fpdiscountto').val() != '') url += "&fpdiscountto=" + encodeURIComponent($('#fpdiscountto').val());
                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fbrand').val()) url += "&fbrand=" + encodeURIComponent($('#fbrand').val());
                if ($('#facategory').val()) url += "&facategory=" + encodeURIComponent($('#facategory').val());
                if ($('#fcategory').val()) url += "&fcategory=" + encodeURIComponent($('#fcategory').val());
                if ($('#fsubcat').val()) url += "&fsubcat=" + encodeURIComponent($('#fsubcat').val());
                if ($('#fchildcat').val()) url += "&fchildcat=" + encodeURIComponent($('#fchildcat').val());

                window.location.href = url;
            });

            //search seller
            $('#fseller').select2({
                allowClear: true,
                minimumInputLength: 1,
                placeholder: 'Search seller',
                allowClear: true,
                ajax: {
                    url: "{{route('ajax.sellerAutoComplete')}}?",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term, // search term 
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: $.map(data, function(sellerarr) {
                                return {
                                    text: sellerarr.text,
                                    id: sellerarr.id,
                                    val: sellerarr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function(sellerarr) {
                    return sellerarr.text;
                },
            });

            //search Categories
            $('#fcategory').select2({
                allowClear: true,
                minimumInputLength: 1,
                placeholder: 'Search Category',
                allowClear: true,
                ajax: {
                    url: "{{route('ajax.categoryAutoComplete')}}?",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term, // search term 
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: $.map(data, function(sellerarr) {
                                return {
                                    text: sellerarr.text,
                                    id: sellerarr.id,
                                    val: sellerarr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function(sellerarr) {
                    return sellerarr.text;
                },
            });

            $('#facategory').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.childcategories.autocomplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: $.map(data, function(categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function(categoryArr) {
                    return categoryArr.text;
                },
            });
        }
    };
}();

jQuery(document).ready(function() {
    CouponAddEdit.init();
});

</script>

@endpush
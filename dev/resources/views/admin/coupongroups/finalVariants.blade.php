@extends(ADMIN_THEME_NAME.'.layouts.master') 

@push('PAGE_ASSETS_CSS')
<style>
.coupons-groups .form-wizard .steps {
    padding: 0px 0;
    margin-bottom: 5px;
}

.coupons-groups .form .form-body {
    padding: 0px;
}

.coupons-groups .progress {
    height: 8px;
    margin-bottom: 20px;
    background-color: #f5f5f5;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 2px rgb(0 0 0 / 10%);
    box-shadow: inset 0 1px 2px rgb(0 0 0 / 10%);
    border-radius: 33px !important;
}

.coupons-groups .portlet.light.portlet-fit>.portlet-title {
    padding: 10px 20px 0px;
}

.minimize-card {
    border-radius: 4px !important;
    padding: 2px 12px;
}

.coupons-groups h3.block {
    padding-top: 0px;
    padding-bottom: 0px;
    font-size: 16px;
    margin: 0;
}

.coupons-groups .form-actions {
    float: right;
    margin-right: 16px;
    margin-top: 13px;
}

.select2 {
    width: 100% !important;
}

strong {
    font-weight: 600;
    font-size: 12px;
}

.filter-tab-btn {
    margin-bottom: 20px;
    display: flex;
    vertical-align: middle;
    align-items: center;

}


#select2-fchildcategory-results .select2-results__option:before {
    content: "";
    display: inline-block;
    position: relative;
    height: 20px;
    width: 20px;
    border: 2px solid #e9e9e9;
    border-radius: 4px;
    background-color: #fff;
    margin-right: 20px;
    vertical-align: middle;
}

#select2-fchildcategory-results .select2-results__option[aria-selected=true]:before {
    font-family: fontAwesome;
    content: "\f00c";
    color: #fff;
    background-color: #f77750;
    border: 0;
    display: inline-block;
    padding-left: 3px;
}

#select2-fchildcategory-results .select2-container--default .select2-results__option[aria-selected=true] {
    background-color: #fff;
}

#select2-fchildcategory-results .select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: #eaeaeb;
    color: #272727;
}

#select2-fchildcategory-results .select2-container--default .select2-selection--multiple {
    margin-bottom: 10px;
}

#select2-fchildcategory-results .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
    border-radius: 4px;
}

#select2-fchildcategory-results .select2-container--default.select2-container--focus .select2-selection--multiple {
    border-color: #f77750;
    border-width: 2px;
}

#select2-fchildcategory-results .select2-container--default .select2-selection--multiple {
    border-width: 2px;
}

#select2-fchildcategory-results .select2-container--open .select2-dropdown--below {

    border-radius: 6px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);

}

#select2-fchildcategory-results .select2-selection .select2-selection--multiple:after {
    content: 'hhghgh';
}

.zoom {
  /* padding: 50px;
  background-color: green; */
  transition: transform .2s; /* Animation */
  /* width: 200px;
  height: 200px; */
  margin: 0 auto;
}

.zoom:hover {
  transform: scale(2); /* (150% zoom)*/
}

</style> 
@endpush 
@section('content') 
<div class="portlet-body form">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered coupons-groups" id="form_wizard_1">
                <div class="portlet-body form">

                        <div class="form-wizard">
                            <div class="form-body">
                                <ul class="nav nav-pills nav-justified steps">
                                    <li>
                                        <a href="{{route('admin.coupongroups.edit',['id'=>$groupId])}}" class="step">
                                            <span class="number"> 1 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Group Details </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.coupongroups.selectVariants', ['id'=>$groupId])}}" class="step ">
                                            <span class="number"> 2 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Product Selection </span>
                                        </a>
                                    </li>
                                    <li class="active">
                                        <a href="#tab3" class="step ">
                                            <span class="number"> 3 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Final Review </span>
                                        </a>
                                    </li>
                                </ul>

                                <div id="bar" class="progress progress-striped" role="progressbar">
                                    <div class="progress-bar progress-bar-success" style="width:100%"> </div>
                                </div>
                                
                                <div class="tab-content">
                                    
                                    <div class="alert alert-danger display-none">
                                        <button class="close" data-dismiss="alert"></button> You have some form errors.
                                        Please check below.
                                    </div>
                                    
                                    <div class="alert alert-success display-none">
                                        <button class="close" data-dismiss="alert"></button> Your form validation is
                                        successful!
                                    </div>

                                    <div class="tab-pane active" id="tab2">

                                        <div class="row">
                                            
                                                <div class="col-md-12">
                                                        <!-- Begin: life time stats -->
                                                    
                                                        <div class="portlet light portlet-fit portlet-datatable bordered">
                                                            <div class="portlet-title">
                                                            <div class="card table-filters-design">
                                                                    
                                                                    <div class="filter-tab-btn card-option row">
                                                                        <div class="col-lg-6 ">
                                                                            <h3 class="block">Click Filters </h3>
                                                                        </div>
                                                                        <div class="col-lg-6 text-right">
                                                                            <span class="minimize-card btn btn-primary "><i ata-feather="filter"></i>Filters</span>
                                                                            <span style="display:none"><i data-feather="filter"></i>Filters</span></li>
                                                                        </div>
                                                                    </div>

                                                                    <!--Search Form -->
                                                                    <div id="card-body" class="card-body" style="display:none;">
                                                                        <!-- FILTER FORM START -->
                                                                        <!-- <form action="#" method="get" class="filter-form" id="search-bar"> -->
                                                                            <div class="form-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Seller Price:</strong>
                                                                                        </label>
                                                                                        <div class="col-md-9">
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control input-sm" name="fspricefrom" id="fspricefrom" placeholder="Min" value="{{request()->fspricefrom}}">
                                                                                                <span class="input-group-addon"> - </span>
                                                                                                <input type="text" class="form-control input-sm" name="fspriceto" id="fspriceto" placeholder="Max" value="{{request()->fspriceto}}">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Seller Discount
                                                                                                (%):</strong> </label>
                                                                                        <div class="col-md-9">
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control input-sm" name="fspdiscountfrom" id="fspdiscountfrom" placeholder="Min" value="{{request()->fspdiscountfrom}}">
                                                                                                <span class="input-group-addon"> -
                                                                                                </span>
                                                                                                <input type="text" class="form-control input-sm" name="fspdiscountto" id="fspdiscountto" placeholder="Max"
                                                                                                    value="{{request()->fspdiscountto}}">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Seller Discount
                                                                                                (INR):</strong> </label>
                                                                                        <div class="col-md-9">
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control input-sm" name="fsdiscountfrom" id="fsdiscountfrom" placeholder="Min" value="{{request()->fsdiscountfrom}}">
                                                                                                <span class="input-group-addon"> -
                                                                                                </span>
                                                                                                <input type="text" class="form-control input-sm" name="fsdiscountto" id="fsdiscountto" placeholder="Max" value="{{request()->fsdiscountto}}">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Fabpik Price:</strong> </label>
                                                                                        <div class="col-md-9">
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control input-sm" name="fpricefrom"
                                                                                                    id="fpricefrom" placeholder="Min" value="{{request()->fpricefrom}}">
                                                                                                <span class="input-group-addon"> - </span>
                                                                                                <input type="text" class="form-control input-sm" name="fpriceto"
                                                                                                    id="fpriceto" placeholder="Max" value="{{request()->fpriceto}}">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Fabpik Discount (%):</strong> </label>
                                                                                        <div class="col-md-9">
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control input-sm" name="fpdiscountfrom"
                                                                                                    id="fpdiscountfrom" placeholder="Min" value="{{request()->fpdiscountfrom}}">
                                                                                                <span class="input-group-addon"> - </span>
                                                                                                <input type="text" class="form-control input-sm" name="fpdiscountto"
                                                                                                    id="fpdiscountto" placeholder="Max" value="{{request()->fpdiscountto}}">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Fabpik Discount (INR):</strong> </label>
                                                                                        <div class="col-md-9">
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control input-sm" name="fdiscountfrom"
                                                                                                    id="fdiscountfrom" placeholder="Min" value="{{request()->fdiscountfrom}}">
                                                                                                <span class="input-group-addon"> - </span>
                                                                                                <input type="text" class="form-control input-sm" name="fdiscountto"
                                                                                                    id="fdiscountto" placeholder="Max" value="{{request()->fdiscountto}}">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    
                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Name:</strong> </label>
                                                                                        <div class="col-md-9">
                                                                                            <input type="text" class="form-control input-sm" name="fname" id="fname"
                                                                                                value="{{request()->fname}}" autocomplete="off">
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Brand:</strong> </label>
                                                                                        <div class="col-md-9">
                                                                                            @php $brands = App\Models\Brand::where('status',1)->get(); @endphp
                                                                                            <select name="fbrand" id="fbrand"
                                                                                                class="form-control form-filter input-sm select2">
                                                                                                <option value="">Select Brand</option>
                                                                                                    @if(isset($brands))
                                                                                                        @foreach($brands as $brand)
                                                                                                            <option value="{{$brand->id}}"
                                                                                                            {{request()->fbrand==$brand->id?'selected="selected"':''}}>
                                                                                                                {{$brand->name}}
                                                                                                            </option>
                                                                                                        @endforeach
                                                                                                    @endif
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3"><strong>Seller:</strong> </label>
                                                                                        <div class="col-md-9">
                                                                                            @php $sellers = App\Models\Seller::where('status',1)->get(); @endphp
                                                                                            <select name="fseller" id="fseller"
                                                                                                class="form-control form-filter input-sm select2">
                                                                                                <option value="">Select Seller</option>
                                                                                                    @if(isset($sellers))
                                                                                                        @foreach($sellers as $seller)
                                                                                                            <option value="{{$seller->id}}"
                                                                                                            {{request()->fseller==$seller->id?'selected="selected"':''}}>
                                                                                                                {{$seller->company_name}}
                                                                                                            </option>
                                                                                                        @endforeach
                                                                                                    @endif
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                </div>

                                                                                <div class="row">

                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Category:</strong> </label>
                                                                                        <div class="col-md-9">
                                                                                            @php $categories = App\Models\Category::where('status',1)->get(); @endphp
                                                                                            <select name="fcategory" id="fcategory"
                                                                                                class="form-control form-filter input-sm js-select2" >
                                                                                                <option value="">Select Category</option>
                                                                                                @if(isset($categories))
                                                                                                    @foreach($categories as $category)
                                                                                                        <option value="{{$category->id}}"
                                                                                                        {{request()->fcategory==$category->id?'selected="selected"':''}}>
                                                                                                            {{$category->title}}
                                                                                                        </option>
                                                                                                    @endforeach
                                                                                                @endif
			                                                                                </select>
      
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Sub Category:</strong> </label>
                                                                                        <div class="col-md-9 pd-tb-5">
                                                                                        @php $subcategories = App\Models\Subcategory::where('category_id', request()->fcategory)->where('status',1)->get(); @endphp
                                                                                            <select name="fsubcategory" id="fsubcategory"
                                                                                                class="form-control form-filter input-sm select2">
                                                                                                <option value="">Select Sub-category</option>
                                                                                                @if(isset($subcategories))
                                                                                                    @foreach($subcategories as $subcategory)
                                                                                                        <option value="{{$subcategory->id}}"
                                                                                                        {{request()->fsubcategory==$subcategory->id?'selected="selected"':''}}>
                                                                                                            {{$subcategory->title}}
                                                                                                        </option>
                                                                                                    @endforeach
                                                                                                @endif
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Child Category:</strong> </label>
                                                                                        <div class="col-md-9  pd-tb-5" >
                                                                                        @php $childcategories = App\Models\ChildCategory::where('category_id', request()->fcategory)->where('subcategory_id', request()->fsubcategory)->where('status',1)->get(); @endphp
                                                                                            <select name="fchildcategory[]" id="fchildcategory"
                                                                                                class="form-control form-filter input-sm select2" multiple="multiple">
                                                                                                <option value="">Select Child Category</option>
                                                                                                @if(isset($childcategories))
                                                                                                    @foreach($childcategories as $childcategory)
                                                                                                        <option value="{{$childcategory->id}}"
                                                                                                        {{in_array($childcategory->id, explode(",", request()->fchildcategory)) ? 'selected="selected"' : ''}}>
                                                                                                            {{$childcategory->title}}
                                                                                                        </option>
                                                                                                    @endforeach
                                                                                                @endif  
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>                            
                                                                                </div>

                                                                                <div class="row">
                                                                                    <div class="col-md-4">
                                                                                    </div>
                                                                                    <div class="col-md-4">
                                                                                    </div>

                                                                                    <div class="col-md-4">
                                                                                        <label class="col-md-3 pd-tb-5"><strong>Action:</strong> </label>
                                                                                        <div class="col-md-9">                                    
                                                                                            
                                                                                            <button type="button" class="btn btn-sm btn-icon-only white btn_submit_search" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                                                                            
                                                                                            <a class="btn btn-sm btn-icon-only white" href="{{route('admin.coupongroups.finalVariants', ['id'=>$groupId])}}" title="Reset Filter">
                                                                                                <span aria-hidden="true" class="icon-refresh"></span></a>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                        <!-- </form> -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <form action="{{route('admin.coupongroups.storeFinalGroup')}}" method="post" class="filter-form">
                                                            {!! csrf_field() !!}

                                                            <input type="hidden" value="{{$groupId}}" name="groupId" id="groupId"/>

                                                            <div class="form-body">
                                                                <div class="form-group {{($errors->first('variantCheckbox'))?'has-error':''}}">
                                                                    <span class="help-block">{{$errors->first('variantCheckbox')}}</span>
                                                            </div>

                                                              <!-- <button type="button" class="btn blue" style="margin-left:10px;" name="checkbox" id="checkbox" value="checkbox">
                                                                        Select All
                                                            </button> -->
                                                                                                                            
                                                            <input type="checkbox" style="margin-left:10px;" id="checkall">
                                                                <label for="checkall">Select all</label>
                                                            

                                                            <div class="portlet-body" style="margin:10px;">
                                                            <div class="table-container">
                                                                <div class="table-actions-wrapper">
                                                                    <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip" style="margin-right:10px;"
                                                                        data-placement="top" data-original-title="Delete" data-action="soft-delete-coupon-group"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;Delete</strong></a>
                                                                </div>
                                                                <table class="table dt-responsive nowrap product-list-table text-center" id="dt_listing">
                                                                    <thead>
                                                                        <tr role="row" class="heading">
                                                                            <th width="4%" class="">&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                    <input type="checkbox" class="group-checkable"
                                                                                        data-set="#sample_2 .checkboxes" />
                                                                                    <span></span>
                                                                                </label>
                                                                            </th>
                                                                            <th width="10%" class="text-center"> ID </th>
                                                                            <th width="17%" class="text-center"> Name </th>
                                                                            <th width="10%" class="text-center"> Image </th>
                                                                            <th width="13%" class="none"> Category </th>
                                                                            <th class="none"> SKU </th>
                                                                            <th width="7%" class="text-center"> MRP </th>
                                                                            <th width="10%" class="text-center"> Sale/<br>Selling Price </th>
                                                                            <th width="10%" class="text-center"> Seller <br>Discount (INR) </th>
                                                                            <th width="10%" class="text-center"> Fabpik <br>Selling Price </th>
                                                                            <th width="10%" class="text-center"> Fabpik <br>Discount (INR) </th>
                                                                            <th width="20%" class="text-center"> Fabpik <br>Discount (%) </th>
                                                                            <th width="20%" class="none"> Special Price Start Date </th>
                                                                            <th width="20%" class="none"> Special Price End Date </th>
                                                                            <th width="" class="none"> Primary Variant: </th>
                                                                            <th width="" class="none"> Secondary Variant: </th>
                                                                            <th width="" class="none"> Created at: </th>
                                                                            <th width="" class="none"> Modified at: </th>
                                                                        </tr>
                                                                    </thead>

                                                                    <tbody> </tbody>
                                                                    
                                                                </table>
                                                            </div>
                                                            </div>
                                                        </div>
                                                </div>   

                                                <div class="row float-right text-right" >
                                                    <div class="col-md-offset-3 col-md-9" style="margin-bottom:10px;">
                                                    <button type="button" class="btn btn-secondary-outline" id="step_btn_back">
                                                        <span aria-hidden="true" class="icon-arrow-left"></span>
                                                            Back
                                                    </button>

                                                        <button type="button" class="btn blue form-submit" id="btn_back"> <i class="fa fa-check-circle"></i>
                                                                        Finish
                                                        </button>                                                                    
                                                    </div>
                                                </div>                                          
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop 

@push('PAGE_ASSETS_JS')
@endpush 
    
@push('PAGE_SCRIPTS')
<script type="text/javascript">
var CouponAddEdit = function() {
    return {
        //main function to initiate the module
        init: function() {

            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

             //select all product variants
             var table = $('#dt_listing').DataTable();

            $('#checkall').click(function(event) {  //on click 
                var checked = this.checked;
                // alert(checked);
                table.column(0).nodes().to$().each(function(index) { 
                    // console.log(`For index ${index}`);
                    if (checked) {
                        $(this).find('.checkBoxClass').prop('checked', 'checked');
                    } else {
                        $(this).find('.checkBoxClass').removeProp('checked');            
                    }
                });
            });

            $(".card-option .minimize-card").on("click", function() {
                var e = $(this),
                    o = $(e.parents(".card"));
                $(o).children(".card-block").slideToggle(),
                $(o).children(".card-body").slideToggle();

                o.hasClass("full-card") || $(o).css("height", "auto"), $(this).children("a").children("span").toggle()

            })

            $.fn.select2.defaults.set("theme", "bootstrap");
            
            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.coupongroups')}}";
            });

            $('#step_btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.coupongroups.selectVariants', ['id'=>$groupId])}}";
            });
            
            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            // Do Filter
            $('.btn_submit_search').on('click', function(e) {

                e.preventDefault();
                
                var url = "{{route('admin.coupongroups.finalVariants',['id'=>$groupId])}}?search=1";

                if ($('#fpricefrom').val() != '') url += "&fpricefrom=" + encodeURIComponent($('#fpricefrom').val());
                if ($('#fpriceto').val() != '') url += "&fpriceto=" + encodeURIComponent($('#fpriceto').val());
                if ($('#fdiscountfrom').val() != '') url += "&fdiscountfrom=" + encodeURIComponent($('#fdiscountfrom').val());
                if ($('#fdiscountto').val() != '') url += "&fdiscountto=" + encodeURIComponent($('#fdiscountto').val());
                if ($('#fpdiscountfrom').val() != '') url += "&fpdiscountfrom=" + encodeURIComponent($('#fpdiscountfrom').val());
                if ($('#fpdiscountto').val() != '') url += "&fpdiscountto=" + encodeURIComponent($('#fpdiscountto').val());

                if ($('#fspricefrom').val() != '') url += "&fspricefrom=" + encodeURIComponent($('#fspricefrom').val());
                if ($('#fspriceto').val() != '') url += "&fspriceto=" + encodeURIComponent($('#fspriceto').val());
                if ($('#fsdiscountfrom').val() != '') url += "&fsdiscountfrom=" + encodeURIComponent($('#fsdiscountfrom').val());
                if ($('#fsdiscountto').val() != '') url += "&fsdiscountto=" + encodeURIComponent($('#fsdiscountto').val());
                if ($('#fspdiscountfrom').val() != '') url += "&fspdiscountfrom=" + encodeURIComponent($('#fspdiscountfrom').val());
                if ($('#fspdiscountto').val() != '') url += "&fspdiscountto=" + encodeURIComponent($('#fspdiscountto').val());

                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fbrand').val()) url += "&fbrand=" + encodeURIComponent($('#fbrand').val());
                if ($('#fcategories').val()) url += "&fcategories=" + encodeURIComponent($('#fcategories').val());
                if ($('#fname').val()) url += "&fname=" + encodeURIComponent($('#fname').val());
                if ($('#fcompanyname').val()) url += "&fcompanyname=" + encodeURIComponent($('#fcompanyname').val());
                if ($('#fcategory').val()) url += "&fcategory=" + encodeURIComponent($('#fcategory').val());
                if ($('#fsubcategory').val()) url += "&fsubcategory=" + encodeURIComponent($('#fsubcategory').val());
                if ($('#fchildcategory').val()) url += "&fchildcategory=" + encodeURIComponent($('#fchildcategory').val());

                window.location.href = url;
            });

            //Seller AutoComplete
            $('#fseller').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.sellerAutoComplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: $.map(data, function(categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function(categoryArr) {
                    return categoryArr.text;
                },
            });

            //search Categories
             $('#fcategories').select2({ 
                allowClear:true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.childcategories.autocomplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (categoryArr) {
                    return categoryArr.text;
                },
            });

            $('#fcategory').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.categoryAutoComplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: $.map(data, function(categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function(categoryArr) {
                    return categoryArr.text;
                },
            });

            $("body").on('change', '#fcategory', function() {
                var parentId = $('#fcategory').val();
                // alert(parentId);
                if (parentId != '0' && parentId != undefined && parentId != null && parentId != '') {
                    $.ajax({
                        type: "POST",
                        url: "{{route('ajax.getSubCategories')}}",
                        data: {id:parentId},
                        dataType: "JSON",
                        success: function(response) {
                            console.log(response);
                            var len = response.list.length;
                            $("#fsubcategory").empty();
                            $("#fsubcategory").append("<option value='' >Select</option>");
                            for (var i = 0; i < len; i++) {
                                var id = response.list[i]['id'];
                                var name = response.list[i]['title'];
                                $("#fsubcategory").append("<option @php (old('subcategory')=='" + id + "') ? 'selected' :'' @endphp value='" + id + "'>" + name + "</option>");
                            }
                        }
                    });
                }
            });

            $("body").on('change', '#fsubcategory', function() {
                var parentId = $('#fsubcategory').val();
                // alert(parentId);
                if (parentId != '0' && parentId != undefined && parentId != null && parentId != '') {
                    $.ajax({
                        type: "POST",
                        url: "{{route('ajax.getChildCategoriesList')}}",
                        data: {id:parentId},
                        dataType: "JSON",
                        success: function(response) {
                            console.log(response);
                            var len = response.list.length;
                            $("#fchildcategory").empty();
                            // $("#fchildcategory").append("<option value='' >Select</option>");
                            for (var i = 0; i < len; i++) {
                                var id = response.list[i]['id'];
                                var name = response.list[i]['title'];
                                $("#fchildcategory").append("<option @php (old('subcategory')=='" + id + "') ? 'selected' :'' @endphp value='" + id + "'>" + name + "</option>");
                            }
                        }
                    });
                }
            });

            $('#fsubcategory').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Sub Category',
            });

            $('#fchildcategory').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Child Category',
                closeOnSelect: false,
            });

            $('#fbrand').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Brand',
            });

        }
    };
}();

jQuery(document).ready(function() {
    CouponAddEdit.init();

    if (window.location.href.indexOf("search") > -1) {
      $("#card-body").show();
    }
});

</script>
@endpush
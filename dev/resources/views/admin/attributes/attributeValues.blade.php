@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"></h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="row">
                <div class="col-md-4">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-blue-sharp bold ">{{$formtitle}}</span>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            @if(isset($optionData))
                            <form class="horizontal-form" action="{{route('admin.attributes.valueEdit',['id'=>$id, 'vid'=>$vid],'.edit')}}" method="post">
                            @else
                            <form class="horizontal-form" action="{{route('admin.attributes.valueAdd',['id'=>$id],'.add')}}" method="post">
                            @endif
                                {!! csrf_field() !!}
                                <div class="form-body">
                                    <div class="form-group {{($errors->first('name'))?'has-error':''}}">
                                        <div class="">
                                            <label class="control-label">Value:<span class="required"> *
                                                </span></label>
                                            <input type="text" class="form-control maxlength-handler" name="name" maxlength="100" placeholder="" value="{{old('name', isset($optionData)?$optionData->option_name:'')}}">
                                            <span class="help-block">{{$errors->first('name')}}</span>
                                        </div>
                                    </div>
                                    <input type="hidden" name="attr_type" value="{{isset($attr_type)?$attr_type:''}}">
                                    @if(isset($attr_type) && $attr_type == 'c')
                                    <div class="form-group {{($errors->first('colorcode'))?'has-error':''}}">
                                        <div class="">
                                            <label class="control-label">Choose Color:<span class="required"> *
                                                </span></label>
                                            <input type="text" id="position-bottom-right" class="form-control demo minicolors-input" data-position="bottom right" size="7" name="colorcode" value="{{old('colorcode',isset($optionData)?$optionData->colour_code:'')}}" autocomplete="off" readonly>
                                            <span class="help-block">{{$errors->first('colorcode')}}</span>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-actions text-right">
                                    <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                                    <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> {{isset($optionData)?'Update':'Save'}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                        <div class="form">
                            <table class="table table-striped table-bordered table-hover" id="">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="10%" class="pd-tb-10 text-center"> # </th>
                                        <th width="70%" class="pd-tb-10 text-center"> Values </th>
                                        <th width="20%" class="pd-tb-10 text-center"> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($attributeValues) >= 1)
                                    @foreach($attributeValues as $index => $attrValue)
                                    @php
                                    $color = $class = '';
                                    if(!is_null($attrValue->colour_code)) {
                                        $class .= "badge badge-default margin-bottom-5";
                                        $color .= "background:".$attrValue->colour_code;
                                    }
                                    @endphp
                                    <tr>
                                        <td class="text-center">{{++$index}}</td>
                                        <td class="text-center">
                                            <div class="">
                                                <span class="{{$class}}" style="{{$color}};">&nbsp;</span>
                                                {{($attrValue->option_name)?$attrValue->option_name:''}}
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="">
                                                <a href="{{ route('admin.attributes.valueEdit',['id'=>$id, 'vid'=>$attrValue->id]) }}" class="btn btn-icon-only default btn-circle" data-toggle="tooltip" data-placement="top" data-original-title="Edit"> <span aria-hidden="true" class="icon-note"></span></a>
                                                <a href="{{route('admin.attributes.valueDelete',['vid'=>$attrValue->id])}}" del-url="{{ route('admin.attributes.valueDelete',['vid'=>$attrValue->id]) }}" class="btn btn-icon-only default btn-circle dt-list-delete tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="3" class="text-center">No records!</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/jquery-minicolors/jquery.minicolors.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('pages/scripts/components-color-pickers.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var AttributeOptions = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.attributes')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            // Delete Attribute
            $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                swal('Attribute Options', 'Record Deleted Successfully', "success");
                                location.reload();
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });
        }
    };
}();

jQuery(document).ready(function() {
    AttributeOptions.init();
});
</script>
@endpush
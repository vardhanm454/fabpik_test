<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->

<head>
    <meta charset="utf-8" />
    <title>Attributes</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">
                <table class="tabletable-bordered">
                    <thead>
                        <tr style="background:#c2c2c2;">
                            <th> # </th>
                            <th> Name </th>
                            <th> Values </th>
                            <th> In Categories </th>
                            <th> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($attributes))
                        @foreach($attributes as $index => $attribute)
                        @php
                        $values = '';
                        foreach($attribute->attributeOptions as $options){
                            $values .=$options->option_name.',';
                        }
                        $values = substr($values, 0, -1);
                        //getting all categories
                        $categories_name = '';
                        foreach ($attribute->attributeCategory as $category) {
                            $categories = App\Models\Category::where('id', $category->category_id)->select('title')->first();
                            $categories_name .= $categories->title.',';
                        }
                        $categories_name = substr($categories_name, 0, -1);
                        @endphp
                        <tr>
                            <td>{{++$index}}</td>
                            <td>{{$attribute->name}}</td>
                            <td>{{$values}}</td>
                            <td></td>
                            <td>{{$attribute->statusLabel()}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>
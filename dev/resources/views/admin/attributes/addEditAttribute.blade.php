@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')

<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"></h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    @if(isset($attribute))
                    <form class="form-horizontal" action="{{route('admin.attributes.edit',['id'=>$id])}}" method="post"
                        enctype="multipart/form-data">
                        @else
                        <form class="form-horizontal" action="{{route('admin.attributes.add')}}" method="post"
                        enctype="multipart/form-data">
                        @endif

                        {!! csrf_field() !!}

                        <div class="form-body">
                            <div class="form-group {{($errors->first('name'))?'has-error':''}}">
                                <label class="col-md-3 control-label">Name:<span class="required"> *
                                    </span></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control maxlength-handler" name="name"
                                        maxlength="100" placeholder="" value="{{old('name', isset($attribute)?$attribute->name:'')}}">
                                    <span class="help-block">{{$errors->first('name')}}</span>
                                </div>
                            </div>
                            <div class="form-group {{($errors->first('display_name'))?'has-error':''}}">
                                <label class="col-md-3 control-label">Display Name:<span class="required"> *
                                    </span></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control maxlength-handler" name="display_name"
                                        maxlength="100" placeholder="" value="{{old('display_name', isset($attribute)?$attribute->display_name:'')}}">
                                    <span class="help-block">{{$errors->first('display_name')}}</span>
                                </div>
                            </div>
                            <div class="form-group {{($errors->first('attribute_type'))?'has-error':''}}">
                                <label class="col-md-3 control-label">Select Type:<span class="required"> *
                                    </span></label>
                                <div class="col-md-9">
                                    <select class="form-control input-sm" name="attribute_type">
                                        <option value="">Select</option>
                                        @foreach(['c'=>'Color','n'=>'Normal'] as $val=>$label)
                                        <option value="{{$val}}" @if(old('attribute_type',
                                            isset($attribute)?$attribute->attr_type:'') == $val) selected @endif>
                                            {{$label}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">{{$errors->first('attribute_type')}}</span>
                                </div>
                            </div>
                            {{--<div class="form-group {{($errors->first('categories'))?'has-error':''}}">
                                <label for="multiple" class="col-md-3 control-label">Categories:<span class="required"> *
                                    </span></label>
                                <div class="col-md-9 ">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th> Select </th>
                                                <th> Category </th>
                                                <th> Primary </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($categories as $val=>$label)
                                            <tr>
                                                <td> <label class=""><input data-index="0" name="categories[name][]" type="checkbox" value="{{$val}}" @if(in_array($val, old('categories.name', (isset($attrCategories))?$attrCategories->toArray():[]))) checked @endif><span></span></label> </td>
                                                <td> {{$label}} </td>
                                                <td> <input data-index="0" name="categories[primary][]" type="checkbox" value="{{$val}}" @if(in_array($val, old('categories.primary', (isset($attrPrimaries))?$attrPrimaries->toArray():[]))) checked @endif><span></span></label> </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <span class="help-block">{{$errors->first('categories')}}</span>
                                </div>
                            </div>--}}
                            @if(isset($attribute))
                            <div class="form-group">
                                <label class="control-label col-md-3">Status:<span class="required"> * </span></label>
                                <div class="col-md-9">
                                    <select name="status" id="status" class="form-control input-sm">
                                        @foreach([1=>'Active',0=>'Inactive'] as $val=>$label)
                                        <option value="{{$val}}" {{(old('status', (isset($attribute))?$attribute->status:1)==$val)?'selected="selected"':''}}>{{$label}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                            @if(isset($attribute))
                            <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var AddEditAttributes = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.attributes')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

        }

    };

}();

jQuery(document).ready(function() {
    AddEditAttributes.init();
});
</script>
@endpush
@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
    type="text/css" />
@endpush

@section('content')
<div class="row brands-wrap">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    @if(isset($condtions))
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.importantdocs.edit',['id'=>$id])}}" method="post" enctype="multipart/form-data">
                    @else
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.importantdocs.add')}}" method="post" enctype="multipart/form-data">
                    @endif
                    {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('name'))?'has-error':''}}">
                                                <label class="control-label">Name:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="name"
                                                        maxlength="100" placeholder="" value="{{old('name',(isset($condtions))?$condtions->name:'')}}">
                                                    <span class="help-block">{{$errors->first('name')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('display_name'))?'has-error':''}}">
                                                <label class="control-label">Display Name:<span class="required"> *
                                                    </span></label>
                                                <div class="">
                                                    <input type="text"  class="form-control maxlength-handler"
                                                        name="display_name" maxlength="100" placeholder=""
                                                        value="{{old('display_name', (isset($condtions))?$condtions->display_name:'')}}">
                                                    <span class="help-block">{{$errors->first('display_name')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="{{($errors->first('description'))?'has-error':''}}">
                                                <label class="control-label">Description:<span class="required"> *
                                                    </span></label>
                                                <div class="">
                                                    <textarea readonly class="summernote" name="description" id="summernote_1">
                                                            {{old('description', (isset($condtions))?$condtions->description:'') }}
                                                    </textarea>
                                                    <span class="help-block">{{$errors->first('description')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            @if(isset($condtions))
                                            <div class="">
                                                <label class="control-label">Status:<span class="required"> * </span></label>
                                                <div class="">
                                                    <select name="status" id="status" class="form-control">
                                                        @foreach([1=>'Active',0=>'Inactive'] as $val=>$label)
                                                        <option value="{{$val}}" {{(old('status', (isset($condtions))?$condtions->status:1)==$val)?'selected="selected"':''}}>{{$label}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true"
                                    class="icon-arrow-left"></span> Back</button>
                          
                                    <button type="submit" class="btn blue form-submit" name="save" value="save"
                                        data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span
                                            aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                    @if(isset($condtions))
                                    <button type="submit" class="btn blue form-submit" name="save" value="savecont"
                                        data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span
                                            aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
                                    @endif
                          
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript">
</script>
<script src="{{ __common_asset('pages/scripts/components-editors.js') }}" type="text/javascript"></script>

@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
    var Documents = function() {
        return {

            //main function to initiate the module
            init: function() {
                $('.summernote').summernote();

                $('#btn_back').on('click', function(e) {
                    e.preventDefault();
                    window.location.href = "{{route('admin.importantdocs')}}";
                });
                
                $('.form-submit').on('click', function(e) {
                    $(this).button('loading');
                });

                //init maxlength handler
                $('.maxlength-handler').maxlength({
                    limitReachedClass: "label label-danger",
                    alwaysShow: true,
                    threshold: 3
                });
            }
        };
    }();

    jQuery(document).ready(function() {
        Documents.init();
    });
</script>
@endpush
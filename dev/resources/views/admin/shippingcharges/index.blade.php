@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-body">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                    <form action="#" method="get" class="filter-form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Name:</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control input-sm" name="fname" id="fname" value="{{request()->fname}}" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Status:</label>
                                                <div class="col-md-8">
                                                    <select name="fstatus" id="fstatus" class="form-control input-sm">
                                                        <option value="">All</option>
                                                        <option value="1" {{request()->fstatus=='1'?'selected="selected"':''}}>Active</option>
                                                        <option value="0" {{request()->fstatus=='0'?'selected="selected"':''}}>Inactive</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-sm blue" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>"><i class="fa fa-filter"></i> Filter</button>
                                    <a class="btn btn-sm btn-default" href="{{route('admin.shippingcharges')}}"><span aria-hidden="true" class="icon-refresh"></span> Reset</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- FILTER FORM END -->
                    <hr>
                </div>
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <a href="{{route('admin.shippingcharges.add')}}" class="btn btn-sm blue tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Add"><i class="fa fa-plus"></i>&nbsp;Add Shipping Charges</a>
                        <a href="javascript:;" class="btn btn-sm red table-group-action tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete" data-action="soft-delete"><i class="fa fa-trash"></i>&nbsp;Delete</a>
                        <a href="javascript:;" id="btn_table_export" class="btn btn-sm purple tooltips" data-container="body" data-placement="top" data-original-title="Export to Excel"><i class="fa fa-file-excel-o"> </i>&nbsp;Export</a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable nowrap" id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="bg-blue-oleo bg-font-blue-oleo">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th class="bg-blue-oleo bg-font-blue-oleo"> Image </th>
                                <th class="bg-blue-oleo bg-font-blue-oleo"> Name </th>
                                <th class="bg-blue-oleo bg-font-blue-oleo"> Seller </th>
                                <th class="bg-blue-oleo bg-font-blue-oleo"> Created </th>
                                <th class="bg-blue-oleo bg-font-blue-oleo"> Modified </th>
                                <th class="bg-blue-oleo bg-font-blue-oleo"> Actions </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Sizechart = function () {

    return {

        //main function to initiate the module
        init: function () {

            // Do Filter
            $('#btn_submit_search').on('click', function(e){
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.categories')}}?search=1";

                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());

                window.location.href = url;
            });

            // Export all Sizechart data
            $('#btn_table_export').on('click', function(e){
                e.preventDefault();

                var url = "{{route('admin.shippingcharges.export')}}?export=1";

                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());

                window.location.href = url;
            });

            // Delete Size Chart
            $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                swal('Charges', 'Record Deleted Successfully', "success");
                                location.reload();
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });
        }

    };

}();

jQuery(document).ready(function() {    
    Sizechart.init();
});
</script>
@endpush
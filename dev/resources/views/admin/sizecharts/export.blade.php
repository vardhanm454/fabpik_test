<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->

<head>
    <meta charset="utf-8" />
    <title>Size Charts</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">
                <table class="tabletable-bordered">
                    <thead>
                        <tr style="background:#c2c2c2;">
                            <th> # </th>
                            <th> Name </th>
                            <th> Seller </th>
                            <th> Created </th>
                            <th> Modified </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($sizecharts))
                        @foreach($sizecharts as $index => $sizechart)
                        <tr>
                            <td>{{++$index}}</td>
                            <td>{{$sizechart->name}}</td>
                            <td>{{isset($sizechart->seller)?$sizechart->seller->name:''}}</td>
                            <td>{{date('d M Y', strtotime($sizechart->created_at))}}</td>
                            <td>{{date('d M Y', strtotime($sizechart->updated_at))}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>
@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
        <!-- <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <span aria-hidden="true" class="icon-cloud-upload"></span> How to add new size chart?
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form">
                        <div class="form-body">
                            <div class="note note-info">
                                <p class="margin-bottom-10">The first row in downloaded sample file should remain as it is. Please do not change the order of columns.</p>
                                <p class="margin-bottom-10">The correct column order is (Type, Name, SKU, Description, Brand, PrimaryCategory, Categories, CountryOfOrigin, DressMaterial, WashingType, IronType, NoOfItemsInPackage, ItemsinPackage, MRP, SellingPrice, Tax, Weight, Length, Breadth, Height, Returnable, Refundable, Cancelable, Featured, Stock, MininimumShippingHours, ShippingWeight, ShippingLength, ShippingBreadth, ShippingHeight, Images, Thumbnail, PrimaryAttributeValue, SecondaryAttributeValue) &amp; you must follow this.</p>
                                <p class="margin-bottom-10">Please make sure the import file is UTF-8 encoded and not saved with byte order mark (BOM).</p>
                                <p class="margin-bottom-10">The images should be uploaded in uploads/products/ folder</p>
                                {{--<p class="margin-bottom-10">System will check if the code belong to any product then will update that product otherwise will add new product.</p>--}}
                                <div class="text-right">
                                    <a class="btn yellow" data-toggle="modal" href="#import_products_modal"> <span aria-hidden="true" class="icon-control-play"></span> Watch video to get more information </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> -->
    </div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    @if(isset($sizechart))
                    <form class="form-horizontal form-row-seperated"
                        action="{{route('admin.sizecharts.edit',['id'=>$id])}}" method="post"
                        enctype="multipart/form-data">
                        @else
                        <form class="form-horizontal form-row-seperated" action="{{route('admin.sizecharts.add')}}"
                            method="post" enctype="multipart/form-data">
                            @endif

                            {!! csrf_field() !!}

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="{{($errors->first('name'))?'has-error':''}}">
                                                    <label class="control-label">Name:<span class="required"> * </span></label>
                                                    <div class="">
                                                        <input type="text" class="form-control maxlength-handler" name="name"
                                                            maxlength="100" placeholder=""
                                                            value="{{old('name', isset($sizechart)?$sizechart->name:'')}}">
                                                        <span class="help-block">{{$errors->first('name')}}</span>
                                                    </div>
                                                </div>
                                                <div class="mt-checkbox-list">
                                                            <label class="mt-checkbox mt-checkbox-outline">
                                                                <input type="checkbox" name="is_default" value="1"
                                                                    {{old('is_default', isset($sizechart)?$sizechart->is_default:'' )== 1 ? 'checked' : ''}}>
                                                                Make it default
                                                                <span></span>
                                                            </label>
                                                        </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="{{($errors->first('seller_id'))?'has-error':''}}">
                                                    <label class="control-label">Seller<span class="required"> *
                                                        </span></label>
                                                    <div class="">
                                                        <select class="form-control" name="seller_id">
                                                            <option value="">Select</option>
                                                            @if(isset($sellers))
                                                            @foreach($sellers as $seller)
                                                            <option value="{{$seller->id}}" @if(old('seller_id',
                                                                isset($sizechart)?$sizechart->seller_id:'') == $seller->id) selected
                                                                @endif >{{$seller->name}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                        <span class="help-block">{{$errors->first('seller_id')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="{{($errors->first('description'))?'has-error':''}}">
                                                    <label class="control-label">Description:</label>
                                                    <div class="">
                                                        <textarea class="form-control maxlength-handler" rows="1" name="description"
                                                            maxlength="255">{!! old('description', isset($sizechart)?$sizechart->description:'') !!}</textarea>
                                                        <span class="help-block"> max 255 chars </span>
                                                        <span class="help-block">{{$errors->first('description')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="{{($errors->first('image'))?'has-error':''}}">
                                            <label class="control-label" for="input-image">Image:<span
                                                    class="required"> *
                                                </span></label>
                                            <div class="">
                                                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                                                    <img src="{{ (isset($sizechart))?route('ajax.previewImage',['image'=>$sizechart->image,'type'=>'sizechart']):url('uploads/no-image.png') }}"
                                                        alt="" title="" data-placeholder="{{ url('uploads/no-image.png') }}"
                                                        width="100" height="100" />
                                                </a>
                                                <input type="hidden" name="image" value="{{isset($sizechart)?$sizechart->image:''}}" id="input-image" />
                                                <span class="help-block">{{$errors->first('image')}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                                <button type="submit" class="btn blue form-submit" name="save" value="save"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                @if(isset($sizechart))
                                <button type="submit" class="btn blue form-submit" name="save" value="savecont"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save &amp; Continue Edit</button>
                                @endif
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var BrandEdit = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.sizecharts')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

            $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
                var $element = $(this);
                var $popover = $element.data('bs.popover');

                e.preventDefault();
                $('a[data-toggle="image"]').popover('destroy');
                if ($popover) {
                    return;
                }
                $element.popover({
                    html: true,
                    placement: 'right',
                    trigger: 'manual',
                    content: function() {
                        return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
                    }
                });
                $element.popover('show');
                $('#button-image').on('click', function() {
                    var $button = $(this);
                    var $icon = $button.find('> i');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element
                            .parent().find('input').attr('id') + "&thumb=" + $element
                            .attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-pencil');
                            }
                        },
                        success: function(html) {
                            $('body').append(
                                '<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' +
                                html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });
                $('#button-clear').on('click', function() {
                    $element.find('img').attr('src', $element.find('img').attr(
                        'data-placeholder'));
                    $element.parent().find('input').val('');
                    $element.popover('destroy');
                });
            });

        }

    };

}();

jQuery(document).ready(function() {
    BrandEdit.init();
});
</script>
@endpush
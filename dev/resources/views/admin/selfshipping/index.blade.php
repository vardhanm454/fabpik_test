@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content')
<style>
.modal {
    text-align: center;
    padding: 0 !important;
}
.modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
}
.modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
}
</style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="">Date: </label><br>
                                    <div class="">
                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control datepicker input-sm" name="ffromdate"
                                                id="ffromdate" placeholder="From" value="{{request()->ffromdate}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control input-sm" name="ftodate" id="ftodate"
                                                placeholder="To" value="{{request()->ftodate}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="">Order Status: </label><br>
                                    <div class="">
                                        <select name="fostatus" id="fostatus"
                                            class="form-control select2 form-filter input-sm margin-bottom-5">
                                            <option value="">Select</option>
                                            @if(isset($orderStatus))
                                            @foreach($orderStatus as $status)
                                            <option value="{{$status->id}}"
                                                {{request()->fostatus==$status->id?'selected="selected"':''}}>
                                                {{$status->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="">Payment Status: </label><br>
                                    <div class="">
                                        <select name="fpstatus" id="fpstatus"
                                            class="form-control select2 form-filter input-sm margin-bottom-5">
                                            <option value="">Select</option>
                                            @if(isset($paymentStatus))
                                            @foreach($paymentStatus as $status)
                                            <option value="{{$status->id}}"
                                                {{request()->fpstatus==$status->id?'selected="selected"':''}}>
                                                {{$status->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="">Seller: </label><br>
                                    <div class="">
                                        <select name="fseller" id="fseller"
                                            class="form-control select2 form-filter input-sm margin-bottom-5">
                                            <option value="">Select</option>
                                            @if(isset($sellers))
                                            @foreach($sellers as $seller)
                                            <option value="{{$seller->id}}"
                                                {{request()->fseller==$seller->id?'selected="selected"':''}}>
                                                {{$seller->company_name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="">Payout Date: </label><br>
                                    <div class="date-picker input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                        <input type="text" class="form-control datepicker input-sm" name="fpayoutdate"
                                                id="fpayoutdate" placeholder="Payout Date" value="{{request()->fpayoutdate}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="">Order ID:</label></br>
                                    <input type="text" class="form-control input-sm" name="forderid" id="forderid" value="{{request()->forderid}}" autocomplete="off">
                                </div>
                                <div class="col-md-4">
                                    <label class="">Action: </label><br>
                                    <div class="">
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search"
                                            id="btn_submit_search" value="search"
                                            data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('admin.selfshipping')}}"
                                            title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips"
                            data-placement="top" data-original-title="Change Payout Status" data-toggle="tooltip"
                            data-action="change-status"><strong><span aria-hidden="true" id="btn-confirm"
                                    class="icon-trash text-danger"></span>&nbsp;Change Status</strong></a>
                    </div>
                    <table class="table dt-responsive table-checkable nowrap text-center"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="" class="text-center"> Date </th>
                                <th width="" class="text-center"> Child Order ID </th>
                                <th width="" class="none"> Seller ID: </th>
                                <th width="" class="none"> Seller Name: </th>
                                <th width="" class="text-center"> MRP: </th>
                                <th width="" class="text-center"> Sale Price </th>
                                <th width="" class="none"> Quantity: </th>
                                <th width="" class="none"> Total Amount: </th>
                                <th width="" class="text-center"> Commission (INR) </th>
                                <th width="" class="none"> GST (INR): </th>
                                <th width="" class="none"> Handling Charges (INR): </th>
                                <th width="" class="text-center"> Final amount to <br />get from Seller </th>
                                <th width="" class="text-center"> Order Status </th>
                                <th width="" class="text-center"> Payout Status </th>
                                <th width="" class="none"> Payout Date from Seller: </th>
                                <th class="text-center"> Actions </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="changeStatusModal" tabindex="-1" role="dialog" aria-labelledby="changeStatusModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changeStatusModalLabel">Payout Status!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="">
                    <label class="control-label">Payout Status:</label>
                    <div class="">
                        <select class="form-control" name="status" id="status">
                            <option value="">Select</option>
                            @foreach(['0'=>'No', '1'=>'Yes'] as $val=>$label)
                            <option value="{{$val}}" {{request()->fostatus==$val?'selected="selected"':''}}>
                                {{$label}}
                            </option>
                            @endforeach
                        </select>
                        <span style="color:#e73d4a" id="result"></span>
                    </div>
                </div>

                <div class="">
                    <label class="control-label">Remark</label>
                    <div class="">
                        <textarea class="form-control" name="remark" id="remark" rows="3"></textarea>
                        <span style="color:#e73d4a" id="remark-result"></span>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
    type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Orders = function() {

    return {

        //main function to initiate the module
        init: function() {

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }

            //Filters will apply when click the enter
            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.selfshipping')}}?search=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fostatus').val() != '') url += "&fostatus=" + encodeURIComponent($('#fostatus').val());
                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fpstatus').val() != '') url += "&fpstatus=" + encodeURIComponent($('#fpstatus').val());
                if ($('#fpayoutdate').val() != '') url += "&fpayoutdate=" + encodeURIComponent($('#fpayoutdate').val());
                if ($('#forderid').val() != '') url += "&forderid=" + encodeURIComponent($('#forderid').val());

                window.location.href = url;
            });

            // Export all parentorders data
            $('#btn_table_export').on('click', function(e) {
                e.preventDefault();

                var url = "{{-- route('admin.parentorders.export') --}}?export=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fostatus').val() != '') url += "&fostatus=" + encodeURIComponent($('#fostatus').val());
                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fpstatus').val() != '') url += "&fpstatus=" + encodeURIComponent($('#fpstatus').val());

                window.location.href = url;
            });
        }
    };
}();

jQuery(document).ready(function() {
    Orders.init();
});
</script>
@endpush
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title>Coupons</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">                        
                <table class="tabletable-bordered">
                    <thead>
                        <tr style="background:#c2c2c2;">
                            <th> # </th>
                            <th>Product Name </th>
                            <th> Start Time </th>
                            <th> End Time </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($deals))
                        @foreach($deals as $index => $deal)
                        <tr>
                            <td>{{++$index}}</td>
                            <td>{{$deal->name}}</td>
                            <td>{{$deal->start_time}} </td>
                            <td>{{$deal->end_time}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>

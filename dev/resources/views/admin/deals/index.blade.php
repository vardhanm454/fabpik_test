@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                    <form action="#" method="get" class="filter-form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 pd-tb-5"><strong>Product Variant:</strong></label>
                                                <div class="col-md-6 form-group {{($errors->first('fprovariants'))?'has-error':''}}">
                                                    <div class="col-md-12">
                                                        <select class="form-control input-sm" id="fprovariants" name="fprovariants[]" multiple >
                                                            @if(!$errors->has('fprovariants'))
                                                                @if (is_array(old('fprovariants')))
                                                                    @php $oldprovariants = old('fprovariants'); @endphp
                                                                
                                                                    @foreach ($oldprovariants as $variant)
                                                                        @php $proVariants = App\Models\ProductVariant::selectRaw("id,name")->find($variant);
                                                                        @endphp
                                                                        <option value="{{$proVariants->id}}" selected="selected">{{$proVariants->name}}</option>
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        </select>
                                                        <span class="help-block">{{$errors->first('fprovariants')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label col-md-4 pd-tb-5"><strong>Action:</strong></label>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Filter">
                                                    <span aria-hidden="true" class="icon-magnifier"></span> </button>
                                                <a class="btn btn-sm btn-icon-only white" href="{{route('admin.deals')}}" title="Reset"><span aria-hidden="true" class="icon-refresh"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <a href="{{route('admin.deals.add')}}" class="btn btn-default btn-sm tooltips" data-toggle="tooltip"
                            data-placement="top" data-original-title="Add Deals"><strong><span aria-hidden="true" class="icon-plus text-primary"></span>&nbsp;Add</strong></a>
                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip"
                            data-placement="top" data-original-title="Delete" data-action="soft-delete"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;Delete</strong></a>
                        <a href="javascript:;" id="btn_table_export" class="btn btn-sm btn-outline btn-default text-warning tooltips"
                            data-container="body" data-placement="top" data-original-title="Export to Excel"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a>
                    </div>
                    <table class="table table-checkable nowrap"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="10%" class=""> Product Name </th>
                                <th width="10%" class=""> MRP </th>
                                <th width="10%" class=""> Price </th>
                                <th width="10%" class=""> Deal Price </th>
                                <th width="10%" class=""> Start Time </th>
                                <th width="20%" class=""> End Time </th>
                                <th width="20%" class=""> Publish Time </th>
                                <th width="20%" class=""> Max Orders Left </th>
                                <th width="10%" class=""> Created By </th>
                                <th width="10%" class=""> Updated By </th>
                                <th width="10%" class=""> Actions </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/scripts/form-repeater.js') }}"
    type="text/javascript">
</script>
<script src="{{ __common_asset('global/scripts/jquery.repeater.js') }}"
    type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Deals = function() {

    return {

        //main function to initiate the module
        init: function() {

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                    e.preventDefault(); 
                    $(this).button('loading');
                    var url = "{{route('admin.deals')}}?search=1";
                    if ($('#fprovariants').val() != '') url += "&fprovariants=" + encodeURIComponent(JSON.stringify($('#fprovariants').val()));
                    window.location.href = url;
            });

            // Export all Coupons data
            $('#btn_table_export').on('click', function(e) {
                e.preventDefault();

                var url = "{{route('admin.deals.export')}}?export=1";

                if ($('#fname').val() != '') url += "&fname=" + encodeURIComponent($('#fname').val());
                if ($('#fstatus').val() != '') url += "&fstatus=" + encodeURIComponent($('#fstatus')
                    .val());

                window.location.href = url;
            });

            // Delete Coupon
            $('body').on('click', '.dt-list-delete', function(event) {
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                swal({
                        title: 'Are you sure?',
                        text: 'You want Delete the record.',
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonClass: 'btn-success',
                        cancelButtonClass: 'btn-danger',
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: 'post',
                                success: function(result) {
                                    swal('Deals', 'Selected Coupon Deleted Successfully',
                                        "success");
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
            });

            // Change Coupon Status
            $('body').on('switchChange.bootstrapSwitch', '.status-switch', function(event, state) {
                var switchControl = $(this);
                var stateText = (state) ? 'Activate' : 'Deactivate';
                swal({
                        title: 'Are you sure?',
                        text: 'You want ' + stateText + ' this data.',
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonClass: 'btn-success',
                        cancelButtonClass: 'btn-danger',
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: "{{route('ajax.changeStatus')}}",
                                type: 'post',
                                data: {
                                    table: 'deals',
                                    id: switchControl.val(),
                                    status: state
                                },
                                success: function(result) {
                                    swal('Deals', 'Data has been ' + stateText + 'd',
                                        "success");
                                }
                            });
                        } else {
                            switchControl.bootstrapSwitch('state', !state);
                        }
                    });
            });
            $('#fprovariants').select2({ 
                allowClear:true,
                minimumInputLength: 1,
                placeholder: 'Search Product Variant',
                allowClear: true,
                ajax: {
                    url: "{{route('ajax.productVariantAutoComplete')}}?",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,  // search term 
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (sellerarr) {
                                return {
                                    text: sellerarr.text,
                                    id: sellerarr.id,
                                    val: sellerarr.text,
                                    term: params.term,
                                    mrp: sellerarr.mrp,
                                    price: sellerarr.price,
                                    sname: sellerarr.sname,
                                    scode: sellerarr.scode,
                                    disc: sellerarr.disc,
                                    image: sellerarr.image,
                                    primaryattrvalue: sellerarr.primaryattrvalue,
                                    secondaryattrvalue: sellerarr.secondaryattrvalue,
                                }
                            })
                        };
                    },
                    cache: false
                },
                // templateResult: function (sellerarr) {
                //     return sellerarr.text +' - '+ sellerarr.mrp;
                // },
                escapeMarkup: function(markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });
            function formatRepoSelection(repo) {
                return repo.text;
            }
            // @see https://select2.github.io/examples.html#data-ajax
            function formatRepo(repo) {
                if (repo.loading) return repo.text;
                
                var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img style='width:90%' src='{{url('/')}}/{{UPLOAD_PATH}}/" + repo.image + "' /></div>" +
                "<div class='select2-result-repository__forks'> Seller:&nbsp" + repo.sname + "</div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.text +  " - "+ repo.primaryattrvalue +" - "+ repo.secondaryattrvalue +"</div>";
            
                if (repo.description) {
                    markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
                }            

                markup += "<div class='select2-result-repository__statistics'>" +
                    "<div class='select2-result-repository__forks'> Rs.&nbsp" + repo.price + "&nbsp&nbsp <del>Rs.&nbsp" + repo.mrp + "</del> ("+ repo.disc.toFixed(2)+"%OFF)</div>" +
                   "</div>" +
                    "</div></div>";

                return markup;
            }
        }

    };
    
}();

jQuery(document).ready(function() {
    Deals.init();
});
</script>
@endpush
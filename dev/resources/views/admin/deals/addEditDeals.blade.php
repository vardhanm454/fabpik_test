@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
    <style>
    .select2-results__option {
        padding: 0px 4px !important;
    }
    .select2-result-repository__title{
        margin-bottom: 0px !important;
    }
    .select2-result-repository{
        padding-top: 3px !important;
        padding-bottom: 3px !important;
    }
    </style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    @if(isset($editdeal))
                    <form class="form-horizontal" action="{{route('admin.deals.edit',['id'=>$id])}}" method="post">
                        @else
                        <form class="form-horizontal" action="{{route('admin.deals.add')}}" method="post">
                            @endif
                            {!! csrf_field() !!}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group {{($errors->first('start_date'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Start Date:<span class="required"> *
                                                </span></label>
                                            <div class="col-md-8">
                                                {{-- <input type="text" class="form-control date-picker" name="start_date" {{isset($editdeal) ? '' : 'data-date-start-date="0d"' }}
                                                    value="{{old('start_date', isset($editdeal)?date('m/d/Y H:i:s',strtotime($editdeal->start_time)):'')}}"
                                                    readonly> --}}
                                                <input type="text" class="form-control date-picker" name="start_date" {{isset($editdeal) ? '' : 'data-date-start-date="0d"' }}
                                                    value="{{old('start_date', isset($editdeal)?date('d-m-Y H:i:s',strtotime($editdeal->start_time)):'')}}"
                                                    readonly>
                                                <span class="help-block">{{$errors->first('start_date')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{($errors->first('expiry_date'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Expiry Date:<span class="required"> *
                                                </span></label>
                                            <div class="col-md-8">
                                                {{-- <input type="text" class="form-control date-picker" name="expiry_date" {{isset($editdeal) ? '': 'data-date-start-date="0d"' }}
                                                    placeholder="" value="{{old('expiry_date', isset($editdeal)?date('m/d/Y H:i:s',strtotime($editdeal->end_time)):'')}}" readonly> --}}
                                                <input type="text" class="form-control date-picker" name="expiry_date" {{isset($editdeal) ? '': 'data-date-start-date="0d"' }}
                                                    placeholder="" value="{{old('expiry_date', isset($editdeal)?date('d-m-Y H:i:s',strtotime($editdeal->end_time)):'')}}" readonly>
                                                <span class="help-block">{{$errors->first('expiry_date')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{($errors->first('publish_date'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Publish Date:<span class="required"> *
                                                </span></label>
                                            <div class="col-md-8">
                                                {{-- <input type="text" class="form-control date-picker" name="publish_date" {{isset($editdeal) ? '': 'data-date-start-date="0d"' }}
                                                    placeholder="" value="{{old('publish_date', isset($editdeal)?date('m/d/Y H:i:s',strtotime($editdeal->publish_date)):'')}}" readonly> --}}
                                                <input type="text" class="form-control date-picker" name="publish_date" {{isset($editdeal) ? '': 'data-date-start-date="0d"' }}
                                                    placeholder="" value="{{old('publish_date', isset($editdeal)?date('d-m-Y H:i:s',strtotime($editdeal->publish_date)):'')}}" readonly>
                                                <span class="help-block">{{$errors->first('publish_date')}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- @dd($errors->messages()) --}}
                                        @if($errors->first('pvariants'))
                                        <p class="alert alert-warning">{{$errors->first('pvariants')}}</p>
                                        @endif

                                        @if($errors->first('empty_fields'))
                                        <p class="alert alert-warning">{{$errors->first('empty_fields')}}</p>
                                        @endif

                                        @if($errors->first('publish_time_error'))
                                        <p class="alert alert-warning">{{$errors->first('publish_time_error')}}</p>
                                        @endif

                                        @if($errors->first('deal_price_invalid'))
                                        <p class="alert alert-warning">{{$errors->first('deal_price_invalid')}}</p>
                                        @endif

                                        @if(array_key_exists("pvariants_stock_error",$errors->messages()))
                                        @php $stockErrors = $errors->messages()['pvariants_stock_error']; @endphp
                                        @foreach ($stockErrors as $stockError)
                                        @php list($pName, $pStock) = explode('||', $stockError[0]); @endphp
                                        <p class="alert alert-warning">{{'Max Global Stock Available For '.$pName . ' is ' . $pStock}}</p>
                                        @endforeach
                                        
                                        @endif
                                        
                                        @if(array_key_exists("deal_exists",$errors->messages()))
                                        @php $dealExistsProducts = $errors->messages()['deal_exists']; @endphp
                                        @foreach ($dealExistsProducts[0] as $dealExistsProduct)  
                                        <p class="alert alert-warning">{{$dealExistsProduct}}</p>
                                        @endforeach
                                        
                                        @endif

                                        <div class="form-group mt-repeater">
                                            <div data-repeater-list="products">
                                                @if(!old('products'))
                                                    <div data-repeater-item class="mt-repeater-item">
                                                        <div class="row mt-repeater-row">
                                                            <div class="col-md-5 form-group {{($errors->first('fprovariants'))?'has-error':''}}">
                                                                <label class="col-md-4 control-label">Product Variants:</label>
                                                                <div class="col-md-12">
                                                                    {{-- @if(isset($editdeal) && $editdeal->product_variant_id) --}}
                                                                        {{-- <select class="fprovariants form-control input-sm" name="fprovariants" disabled="true" > --}}
                                                                    {{-- @else --}}
                                                                        <select class="fprovariants form-control input-sm" name="fprovariants" id="fprovariants" >
                                                                            <option value=""></option>
                                                                    {{-- @endif --}}
                                                                            @if(isset($editdeal) && $editdeal->product_variant_id)
                                                                                @php $variants = json_decode($editdeal->product_variant_id); @endphp
                                                                                @php $proVariants = App\Models\ProductVariant::selectRaw("id,name")->find($editdeal->product_variant_id);
                                                                                    @endphp
                                                                                    <option value="{{$proVariants->id}}" selected="selected">{{$proVariants->name}}</option>
                                                                            
                                                                            @endif
                                                                        </select>
                                                                    <span class="help-block">{{$errors->first('fprovariants')}}</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label class="control-label">Deal Price</label>
                                                                @if (!isset($editdeal))
                                                                    <input type="text" placeholder="Example: 2000" name="deal_price" class="form-control" />

                                                                @elseif(isset($editdeal))
                                                                <input type="text" placeholder="Example: 2000" value="{{$editdeal->deal_price}}" name="deal_price" class="form-control" />
                                                                @endif
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label class="control-label">Max Orders</label>
                                                                @if (!isset($editdeal))
                                                                    <input type="text" placeholder="Example: 20" name="max_orders" class="form-control" />

                                                                @elseif(isset($editdeal))
                                                                <input type="text" placeholder="Example: 20" value="{{$editdeal->max_orders}}" name="max_orders" class="form-control" />
                                                                @endif
                                                            </div>
                                                            @if (!isset($editdeal))
                                                            <div class="col-md-1">
                                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @else
                                                    {{-- @dd(old('products')); --}}
                                                    @foreach(old('products') as $product)
                                                    <div data-repeater-item class="mt-repeater-item">
                                                        <div class="row mt-repeater-row">
                                                            <div class="col-md-5 form-group {{($errors->first('fprovariants'))?'has-error':''}}">
                                                                <label class="col-md-4 control-label">Product Variants:</label>
                                                                <div class="col-md-12">
                                                                    <select class="fprovariants form-control input-sm" name="fprovariants" >
                                                                        {{-- @php $variants = json_decode($editdeal->product_variant_id); @endphp --}}
                                                                        <option value=""></option>
                                                                        @if($product["fprovariants"] != null)
                                                                            @php $proVariants = App\Models\ProductVariant::selectRaw("id,name")->find($product["fprovariants"]);
                                                                                @endphp
                                                                                <option value="{{$proVariants->id}}" selected="selected">{{$proVariants->name}}</option>
                                                                        @endif
                                                                    </select>
                                                                    <span class="help-block">{{$errors->first('fprovariants')}}</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label class="control-label">Deal Price</label>
                                                                    <input type="text" placeholder="Example: 2000" name="deal_price" value="{{$product["deal_price"]}}" class="form-control" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label class="control-label">Max Orders</label>
                                                                    <input type="text" placeholder="Example: 20" name="max_orders" value="{{$product["max_orders"]}}" class="form-control" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>                                                       
                                                    @endforeach
                                                @endif
                                            </div>
                                            @if (!isset($editdeal))
                                            <a href="javascript:;" id="repeater-button" data-repeater-create class="btn btn-info mt-repeater-add">
                                                        <i class="fa fa-plus"></i> Add More</a>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                
                            </div>
                            <div class="form-actions text-right">
                                <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                                <button type="submit" class="btn blue form-submit" name="save" value="save"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                @if(isset($editdeal))
                                <button type="submit" class="btn blue form-submit" name="save" value="savecont"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><i
                                        class="fa fa-check-circle"></i> Save &amp; Continue Edit</button>
                                @endif
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
    type="text/javascript">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript">
</script>
<script src="{{ __common_asset('global/scripts/form-repeater.js') }}"
    type="text/javascript">
</script>
<script src="{{ __common_asset('global/scripts/jquery.repeater.js') }}"
    type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')

<script type="text/javascript">

    function formatRepoSelection(repo) {
        return repo.text;
    }

    function formatRepo(repo) {
        if (repo.loading) return repo.text;  
        var markup = "<div class='select2-result-repository clearfix'>" +
                     "<div class='select2-result-repository__avatar'><img style='width:90%' src='/{{UPLOAD_PATH}}/" + repo.image + "' /></div>" +
                     "<div class='select2-result-repository__forks'> Seller:&nbsp" + repo.sname + "</div>" +
                     "<div class='select2-result-repository__meta'>" +
                     "<div class='select2-result-repository__title'>" + repo.text +  " - "+ repo.primaryattrvalue +" - "+ repo.secondaryattrvalue +"</div>";
            
        if (repo.description) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }            
        markup += "<div class='select2-result-repository__statistics'>" +
                  "<div class='select2-result-repository__forks'> Rs.&nbsp" + repo.price + "&nbsp&nbsp <del>Rs.&nbsp" + repo.mrp + "</del> ("+ repo.disc.toFixed(2)+"%OFF)</div>" +
                  "</div>" +
                  "</div></div>";
        return markup;
    }

    $("#repeater-button").click(function(){
                setTimeout(function(){
                $('.fprovariants').select2({ 
                allowClear:true,
                minimumInputLength: 1,
                placeholder: 'Search Product Variant',
                allowClear: true,
                ajax: {
                    url: "{{route('ajax.productVariantAutoComplete')}}?",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,  // search term 
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (sellerarr) {
                                return {
                                    text: sellerarr.text,
                                    id: sellerarr.id,
                                    val: sellerarr.text,
                                    term: params.term,
                                    mrp: sellerarr.mrp,
                                    price: sellerarr.price,
                                    sname: sellerarr.sname,
                                    scode: sellerarr.scode,
                                    disc: sellerarr.disc,
                                    image: sellerarr.image,
                                    primaryattrvalue: sellerarr.primaryattrvalue,
                                    secondaryattrvalue: sellerarr.secondaryattrvalue,
                                }
                            })
                        };
                    },
                    cache: false
                },
                // templateResult: function (sellerarr) {
                //     return sellerarr.text +' - '+ sellerarr.mrp;
                // },
                escapeMarkup: function(markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

                }, 100);
            });
var CouponAddEdit = function() {

    return {

        //main function to initiate the module
        init: function() {
            // Data Picker Initialization
            if (jQuery().datetimepicker) {
                @php $db_date =  isset($editdeal)? date('m/d/y', strtotime($editdeal->start_date)) :'' ; @endphp
                @if($db_date != null && $db_date != '' && $db_date != 'undefined')
                var start_date = new Date('{{$db_date}}'); 
                @else
                var start_date = new Date(); 
                @endif
                
                $('.date-picker').datetimepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true,
                    startDate: start_date,
                    // format: 'mm/dd/yyyy hh:ii:ss',
                    format: 'dd-mm-yyyy hh:ii:ss',
                    locale: 'en'
                });
            }

            $.fn.select2.defaults.set("theme", "bootstrap");

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.deals')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
                var $element = $(this);
                var $popover = $element.data('bs.popover');

                e.preventDefault();
                $('a[data-toggle="image"]').popover('destroy');
                if ($popover) {
                    return;
                }
                $element.popover({
                    html: true,
                    placement: 'right',
                    trigger: 'manual',
                    content: function() {
                        return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
                    }
                });
                $element.popover('show');
                $('#button-image').on('click', function() {
                    var $button = $(this);
                    var $icon = $button.find('> i');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element
                            .parent().find('input').attr('id') + "&thumb=" + $element
                            .attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-pencil');
                            }
                        },
                        success: function(html) {
                            $('body').append(
                                '<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' +
                                html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });
                $('#button-clear').on('click', function() {
                    $element.find('img').attr('src', $element.find('img').attr(
                        'data-placeholder'));
                    $element.parent().find('input').val('');
                    $element.popover('destroy');
                });
            });
            
            //search seller
            $('#fseller').select2({ 
                allowClear:true,
                minimumInputLength: 1,
                placeholder: 'Search seller',
                allowClear: true,
                ajax: {
                    url: "{{route('ajax.sellerAutoComplete')}}?",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,  // search term 
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (sellerarr) {
                                return {
                                    text: sellerarr.text,
                                    id: sellerarr.id,
                                    val: sellerarr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (sellerarr) {
                    return sellerarr.text;
                },
            });

            //search Categories
            $('#fcategory').select2({ 
                allowClear:true,
                minimumInputLength: 1,
                placeholder: 'Search Category',
                allowClear: true,
                ajax: {
                    url: "{{route('ajax.categoryAutoComplete')}}?",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,  // search term 
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (sellerarr) {
                                return {
                                    text: sellerarr.text,
                                    id: sellerarr.id,
                                    val: sellerarr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (sellerarr) {
                    return sellerarr.text;
                },
            });
          
            //search ProductVariants
            $('.fprovariants').select2({ 
                allowClear:true,
                minimumInputLength: 1,
                placeholder: 'Search Product Variant',
                allowClear: true,
                ajax: {
                    url: "{{route('ajax.productVariantAutoComplete')}}?",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,  // search term 
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (sellerarr) {
                                return {
                                    text: sellerarr.text,
                                    id: sellerarr.id,
                                    val: sellerarr.text,
                                    term: params.term,
                                    mrp: sellerarr.mrp,
                                    price: sellerarr.price,
                                    sname: sellerarr.sname,
                                    scode: sellerarr.scode,
                                    disc: sellerarr.disc,
                                    image: sellerarr.image,
                                    primaryattrvalue: sellerarr.primaryattrvalue,
                                    secondaryattrvalue: sellerarr.secondaryattrvalue,
                                }
                            })
                        };
                    },
                    cache: false
                },
                // templateResult: function (sellerarr) {
                //     return sellerarr.text +' - '+ sellerarr.mrp;
                // },
                escapeMarkup: function(markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });

            function formatRepoSelection(repo) {
                return repo.text;
            }
            // @see https://select2.github.io/examples.html#data-ajax
            function formatRepo(repo) {
                if (repo.loading) return repo.text;
                
                var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img style='width:90%' src='{{url('/')}}/{{UPLOAD_PATH}}/" + repo.image + "' /></div>" +
                "<div class='select2-result-repository__forks'> Seller:&nbsp" + repo.sname + "</div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.text +  " - "+ repo.primaryattrvalue +" - "+ repo.secondaryattrvalue +"</div>";
            
                if (repo.description) {
                    markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
                }            
                if(repo.price != repo.mrp){
                    markup += "<div class='select2-result-repository__statistics'>" +
                        "<div class='select2-result-repository__forks'> Rs.&nbsp" + repo.price + "&nbsp&nbsp <del>Rs.&nbsp" + repo.mrp + "</del> ("+ repo.disc.toFixed(2)+"%OFF)</div>" +
                       "</div>" +
                        "</div></div>";
                }else{
                    markup += "<div class='select2-result-repository__statistics'>" +
                        "<div class='select2-result-repository__forks'> Rs.&nbsp" + repo.price + "&nbsp&nbsp</div>" +
                       "</div>" +
                        "</div></div>";
                }

                return markup;
            }

        }

    };

}();

jQuery(document).ready(function() {
    CouponAddEdit.init();
});
</script>
@endpush
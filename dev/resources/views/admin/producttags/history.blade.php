@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')

<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"></h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    
                    <h4> <strong> Tag History </strong> </h4>
                    <table class="table dt-responsive table-checkable">
                        <thead>
                            <tr role="row" class="heading">
                                <th>Modified By</th>
                                <th>Modified at</th>
                                <th>From Name</th>
                                <th>To Name</th>
                                <th>From Status</th>
                                <th>To Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tagHistories as $taghistory)
                            <tr>
                                <td>{{$taghistory->modifiedUser->name}}</td>
                                <td>{{date( 'd-m-Y', strtotime($taghistory->created_at) )}}</td>
                                <td>{{$taghistory->from_name}}</td>
                                <td>{{$taghistory->to_name}}</td>
                                <td>{{($taghistory->from_status == 1) ? 'Active' : 'In-Active'}}</td>
                                <td>{{($taghistory->to_status == 1) ? 'Active' : 'In-Active'}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                </div>

                <div class="form-actions text-right">
                    <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                </div>
                
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var History = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.producttags')}}";
            });
        }
    };

}();

jQuery(document).ready(function() {
    History.init();
});
</script>
@endpush
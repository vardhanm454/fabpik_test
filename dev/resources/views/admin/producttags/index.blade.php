@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<style>
.modal {
    text-align: center;
    padding: 0 !important;
}

.modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
}

.modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
}
.modal-dialog {
    width: 400px;
}

</style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title bg-info pd-15">
                <div class="filter-actions">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="col-md-3 pd-tb-5"><strong>Name:</strong> </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="fname" id="fname"
                                    value="{{request()->fname}}" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label class="col-md-3 pd-tb-5"><strong>Status:</strong> </label>
                            <div class="col-md-9">
                                <select name="fstatus" id="fstatus" class="form-control input-sm">
                                    <option value="">All</option>
                                    <option value="1" {{request()->fstatus=='1'?'selected="selected"':''}}>
                                        Active
                                    </option>
                                    <option value="0" {{request()->fstatus=='0'?'selected="selected"':''}}>
                                        Inactive</option>
                                </select>
                            </div>
                        </div>
                                
                        <div class="col-md-4">
                            <label class="col-md-3 pd-tb-5"><strong>Action:</strong> </label>
                            <div class="col-md-9">                                    
                                <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                <a class="btn btn-sm btn-icon-only white" href="{{route('admin.producttags')}}" title="Reset Filter">
                                    <span aria-hidden="true" class="icon-refresh"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-container">
                        <div class="table-actions-wrapper">
                            
                            <a href="#" class="btn btn-default btn-sm tooltips open-AddTagModal" data-toggle="modal" data-placement="top" data-original-title="Add New Tag" data-target="#productTagModal"><strong><span aria-hidden="true" class="icon-plus text-primary"></span>&nbsp;Add</strong></a>
                            
                            <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete Brand" data-action="soft-delete"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;Delete</strong></a>

                        {{-- <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-placement="top" data-original-title="Tag Active/In-Active" data-toggle="tooltip" data-action="change-tag-status"><strong><span aria-hidden="true" id="btn-confirm" class="text-danger"></span>&nbsp;Change Status</strong></a> --}}

                        </div>
                        <table class="table dt-responsive table-checkable" id="dt_listing">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="10">
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable"
                                                data-set="#sample_2 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th width=""> Sr. No. </th>
                                    <th width=""> Tag Name </th>
                                    <th width=""> Created By </th>
                                    <th width=""> Created At </th>
                                    <th width=""> Modified By </th>
                                    <th width=""> Modified At </th>
                                    <th width=""> Associated<br />Products Count </th>
                                    <th width="" class="text-center"> Status </th>
                                    <th width="" class="text-center"> References </h>
                                    <th width="" class="text-center"> Actions </th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Modal -->
<div class="modal fade" id="productTagModal" tabindex="-1" role="dialog" aria-labelledby="productTagLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="productTagLabel">Add Product Tag
                    <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h5>
                
            </div>
            <div class="modal-body">
                <span style="color:#27a4b0" id="successResult"></span>                        

                <div class="">
                    <label class="control-label">Tag Name:<span class="required"> * </span></label>
                        <input type="text" class="form-control maxlength-handler" name="tagName" id="tagName"
                                        maxlength="100" placeholder="" value="" autocomplete="off">
                        <span style="color:#e73d4a" id="result"></span>                        
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="add-modal-btn">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="editProductTagModal" tabindex="-1" role="dialog" aria-labelledby="editProductTagModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editProductTagModalLabel">Edit Tag - <strong> <span id="editTagNameDisplay"></span> </strong>
                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </h5>
                
            </div>
            <div class="modal-body">
                <span style="color:#27a4b0" id="editSuccessResult"></span>                        

                <div class="form-group">
                    
                    <input type="hidden" value="" name="from_tag_status" id="from_tag_status">
                    <input type="hidden" value="" name="from_tag_name" id="from_tag_name">
                    <input type="hidden" value="" name="tag_id" id="tag_id">

                    <label class="control-label">Tag Name:<span class="required"> * </span></label>
                        <input type="text" class="form-control maxlength-handler" name="editTagName" id="editTagName"
                                        maxlength="100" placeholder="" value="">

                        <span style="color:#e73d4a" id="editResult"></span>

                </div>
                <div class="form-group">
                    <label class="control-label">Status:<span class="required"> * </span></label>
                    <select name="to_status" id="to_status" class="form-control input-sm">
                        @foreach([1=>'Active',0=>'Inactive'] as $val=>$label)
                        <option value="{{$val}}" >{{$label}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="edit-modal-btn">Save changes</button>
            </div>
        </div>
    </div>
</div>

@stop

@push('PAGE_ASSETS_JS')

@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Tags = function() {

    return {

        //main function to initiate the module
        init: function() {

            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });
            
            //Add Modal 
            $('#productTagModal').on('hidden.bs.modal', function (e) {
                $("#successResult").html(result.message);
                $("#result").html('');
                $("#tagName").val('');
            });

            $(document).on('click', '#add-modal-btn', function(){
                var url = "{{route('admin.producttags.add')}}";

                var name = $('#tagName').val();
                if(name.trim() == '' ){
                    $('#tagName').focus();
                    $("#result").html("Tag Name filed is required.");
                    return false;
                }else{

                    var arr = { name: name };

                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#add-modal-btn').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                            if(result.success){
                                $("#successResult").html(result.message);
                                $("#result").html('');
                                $("#tagName").val('');
                                $('.modal-body').html(result.message);    
                                setTimeout(function(){ location.reload(true); }, 2000);
                            }else{
                                $("#result").html(result.message);
                            }
                            
                            $('#add-modal-btn').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');    
                        },
                        error:function(err){
                            // alert("error"+JSON.stringify(err));
                            alert("Something went wrong, please try again after sometime");
                            $('#add-modal-btn').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');   
                        }
                    });
                }
            });
            //End Add Modal.

            //Edit Modal
            $(document).on("click", ".open-EditTagModal", function (e) {
                e.preventDefault();
                var _self = $(this);
                              
                $("#tag_id").val(_self.data('id'));
                $("#from_tag_status").val(_self.data('myvalue'));                
                $("#to_status").val(_self.data('myvalue'));
                $("#from_tag_name").val(_self.data('name'));
                $('#editTagNameDisplay').html(_self.data('name'));
                $("#editTagName").val(_self.data('name'));

                $(_self.attr('href')).modal('show');
            });

            $(document).on('click', '#edit-modal-btn', function(){
                
                var name = $('#editTagName').val();
                var from_tag_name = $('#from_tag_name').val();
                var tagId = $('#tag_id').val();
                var to_status = $('#to_status').val();
                var from_tag_status = $('#from_tag_status').val();

                if(name.trim() == '' ){

                    $('#editTagName').focus();
                    $("#editResult").html("Tag Name filed is required.");
                    return false;

                }else{
                    var url = "{{route('admin.producttags.edit', ['id'=> ':TagId'])}}";
                    url = url.replace(':TagId', tagId);

                    var arr = { name: name,  from_tag_status:from_tag_status, from_tag_name:from_tag_name, status:to_status};
              
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#edit-modal-btn').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                            if(result.success){
                                $("#editSuccessResult").html(result.message);
                                $("#editResult").html('');
                                $("#editTagName").val('');
                                $('.modal-body').html(result.message);    
                                setTimeout(function(){ location.reload(true); }, 2000);
                                
                            }else{
                                $("#editResult").html(result.message);
                            }
                            
                            $('#edit-modal-btn').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');    
                        },
                        error:function(err){
                            alert("error"+JSON.stringify(err));
                            alert("Something went wrong, please try again after sometime");
                            $('#edit-modal-btn').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');   
                        }
                    });
                }
            });

            //end Edit Modal

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.producttags')}}?search=1";

                if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());
                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
               
                window.location.href = url;
            });

             // Delete Brand
             $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                swal('Product Tag', result.message, ((result.success) ? "success" : "error"));
                                setTimeout(function(){ location.reload(true); }, 1000);
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });

        }

    };

}();

jQuery(document).ready(function() {
    Tags.init();
});
</script>
@endpush
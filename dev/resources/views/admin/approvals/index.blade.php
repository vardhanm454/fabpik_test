@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                    <form action="#" method="get" class="filter-form">
                        <div class="form-body">
                            <div class="row">
                                {{--<div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label col-md-2 pd-tb-5"><strong>Name:</strong></label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control input-sm" name="fname" id="fname" value="{{request()->fname}}" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 pd-tb-5"><strong>Status:</strong></label>
                                                <div class="col-md-9">
                                                    <select name="fstatus" id="fstatus" class="form-control input-sm">
                                                        <option value="">All</option>
                                                        <option value="1" {{request()->fstatus=='1'?'selected="selected"':''}}>Active</option>
                                                        <option value="0" {{request()->fstatus=='0'?'selected="selected"':''}}>Inactive</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label col-md-4 pd-tb-5"><strong>Action:</strong></label>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span> </button>
                                                <a class="btn btn-sm btn-icon-only white" href="" title="Reset"><span aria-hidden="true" class="icon-refresh"></span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <table class="table dt-responsive nowrap product-list-table" id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="" class="text-center"> Type </th>
                                <th width="" class="text-center"> Old Value </th>
                                <th width="" class="text-center"> New Value </th>
                                <th class="text-center"> Requested By </th>
                                <th class="text-center"> Requested Date </th>
                                <th class="text-center"> Verified By </th>
                                <th class="text-center"> Verified Date </th>
                                <th class="text-center"> Action </th>
                                <th class="none"> Seller ID: </th>
                                <th class="none"> Company Name: </th>
                                <th class="none"> Email ID: </th>
                                <th class="none"> Mobile: </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Approvals = function () {

    return {

        //main function to initiate the module
        init: function () {
            // Accept Attribute
            $('body').on('click', '.dt-list-view', function(event){
                event.preventDefault();
                var url = $(this).attr('del-url');
                var title = $(this).attr('del-title');
               
                swal({
                    title: 'Are you sure?',
                    text: 'You want to '+title+' the request.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                    showLoaderOnConfirm: true
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                // console.log(result);
                                if(result.success == 1 && result.type == "accept"){
                                    swal('Approval', 'Accepted Successfully', "success");
                                }else if(result.success == 1 && result.type == "reject"){
                                    swal('Rejected', 'Rejected Successfully', "success");
                                }else{
                                    swal('Error', 'Error! Some error occured, please try again.', "error");
                                }
                                location.reload();
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });

        }

    };

}();

jQuery(document).ready(function() {    
    Approvals.init();
});
</script>
@endpush
@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="tabbable-bordered">
                    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">                       
                        {!! csrf_field() !!}

                        <div class="form-body">
                               <table class="table table-striped table-bordered table-hover table-checkable nowrap" id="dt_listing">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th width="10%" class="pd-tb-10 text-center"> # </th>
                                            <th width="40%" class="pd-tb-10 text-center"> Name </th>
                                            <th width="50%" class="pd-tb-10 text-center"> Priority </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $cat_cout = count($categoryList); @endphp
                                        @if( $cat_cout >= 1)                                        
                                        @foreach($categoryList as $index=>$category)
                                        <tr>
                                            <td class="text-center">{{++$index}}</td>
                                            <td class="text-center">
                                                <div class="">
                                                {{$category->title}}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="{{($errors->first('name.'.$category->id))?'has-error':''}}">
                                                <input type="number" class="form-control" min="1" max="{{$cat_cout}}" name="name[{{$category->id}}]" placeholder="" value="{{old('name.'.$category->id, (($category->priority)?$category->priority:''))}}">
                                                <span class="help-block">{{$errors->first('name.'.$category->id)}}</span>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="3" class="text-center">No records!</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var CategoryPriority = function() {

    return {
        //main function to initiate the module
        init: function() {
            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.categories')}}";
            });
        }
    };

}();
jQuery(document).ready(function() {
    CategoryPriority.init();
});
</script>
@endpush
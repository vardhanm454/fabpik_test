@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<style>

#select2-brands-results .select2-results__option:before {
    content: "";
    display: inline-block;
    position: relative;
    height: 20px;
    width: 20px;
    border: 2px solid #e9e9e9;
    border-radius: 4px;
    background-color: #fff;
    margin-right: 20px;
    vertical-align: middle;
}

#select2-brands-results .select2-results__option[aria-selected=true]:before {
    font-family: fontAwesome;
    content: "\f00c";
    color: #fff;
    background-color: #f77750;
    border: 0;
    display: inline-block;
    padding-left: 3px;
}

#select2-brands-results .select2-container--default .select2-results__option[aria-selected=true] {
    background-color: #fff;
}

#select2-brands-results .select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: #eaeaeb;
    color: #272727;
}

#select2-brands-results .select2-container--default .select2-selection--multiple {
    margin-bottom: 10px;
}

#select2-brands-results .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
    border-radius: 4px;
}

#select2-brands-results .select2-container--default.select2-container--focus .select2-selection--multiple {
    border-color: #f77750;
    border-width: 2px;
}

#select2-brands-results .select2-container--default .select2-selection--multiple {
    border-width: 2px;
}

#select2-brands-results .select2-container--open .select2-dropdown--below {

    border-radius: 6px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);

}

#select2-brands-results .select2-selection .select2-selection--multiple:after {
    content: 'hhghgh';
}
</style>
<div class="row categpries-wrap">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    @if(isset($category))
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.categories.edit',['id'=>$id])}}" method="post"
                        enctype="multipart/form-data">
                    @else
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.categories.add')}}" method="post"
                        enctype="multipart/form-data">
                    @endif
                        {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="{{($errors->first('name'))?'has-error':''}}">
                                                    <label class="control-label">Name:<span class="required"> * </span></label>
                                                    <div class="">
                                                        <input type="text" class="form-control maxlength-handler" name="name"
                                                            maxlength="100" placeholder="" value="{{old('name',isset($category)?$category->title:'')}}">
                                                        <span class="help-block">{{$errors->first('name')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="{{($errors->first('primary_attribute'))?'has-error':''}}">
                                                    <label class="control-label">Primary Attribute:<span class="required"> * </span></label>
                                                    <div class="">
                                                        <select class="form-control select2" name="primary_attribute" data-allow-clear="true">
                                                            <option value="">Select</option>
                                                            @if(count($attributes) >= 1)
                                                            @foreach($attributes as $val=>$label)
                                                            <option value="{{$val}}" @if(old('primary_attribute', isset($category)?$category->primary_attribute:'')==$val) selected @endif>{{$label}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                        <span class="help-block">{{$errors->first('primary_attribute')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="{{($errors->first('secondary_attribute'))?'has-error':''}}">
                                                    <label class="control-label">Secondary Attribute:</label>
                                                    <div class="">
                                                        <select class="form-control select2" name="secondary_attribute"  data-allow-clear="true">
                                                            <option value="">Select</option>
                                                            @if(count($attributes) >= 1)
                                                            @foreach($attributes as $val=>$label)
                                                            <option value="{{$val}}" @if(old('secondary_attribute', isset($category)?$category->secondary_attribute:'')==$val) selected @endif>{{$label}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                        <span class="help-block">{{$errors->first('secondary_attribute')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="{{($errors->first('description'))?'has-error':''}}">
                                                    <label class="control-label">Description:<span class="required"> * </span></label>
                                                    <div class="">
                                                        <textarea class="form-control" rows="2" name="description">{{old('description', isset($category)?$category->description:'')}}</textarea>
                                                        <span class="help-block">{{$errors->first('description')}}</span>
                                                    </div>
                                                </div>
                                            </div>                         
                                            <div class="col-md-4">
                                                <div class="{{($errors->first('commission'))?'has-error':''}}">
                                                    <label class="control-label">Commission:<span class="required"> * </span></label>
                                                    <div class="">
                                                        <input type="text" class="form-control maxlength-handler" name="commission"
                                                            maxlength="100" placeholder="" value="{{old('commission',isset($category)?$category->commission:'')}}">
                                                        <span class="help-block">{{$errors->first('commission')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="{{($errors->first('tax'))?'has-error':''}}">
                                                    <label class="control-label">Tax:<span class="required"> * </span></label>
                                                    <div class="">
                                                        <input type="text" class="form-control maxlength-handler" name="tax"
                                                            maxlength="100" placeholder="" value="{{old('tax',isset($category)?$category->tax:'')}}">
                                                        <span class="help-block">{{$errors->first('tax')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="{{($errors->first('brands'))?'has-error':''}}">
                                                    <label class="control-label">Brand:</label>
                                                    <div class="">
                                                        @php $brands = App\Models\Brand::where('status',1)->get(); @endphp
                                                        <select name="brands[]" id="brands" class="form-control form-filter input-sm select2" multiple="multiple">
                                                            <option value="">Select Brand</option>
                                                             @if(isset($brands))
                                                                @foreach($brands as $brand)
                                                                    <option value="{{$brand->id}}" {{in_array($brand->id, (array)old('brands', isset($category)?json_decode($category->brands):'')) ? 'selected="selected"' : ''}}> {{$brand->name}} </option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                        <span class="help-block">{{$errors->first('brands')}}</span>
                                                    </div>
                                                    <input id="chkall" type="checkbox">Select All

                                                </div>
                                            </div>
                                              
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">    
                                        <div class="col-md-4">
                                            <div class="{{($errors->first('meta_title'))?'has-error':''}}">
                                                <label class="control-label">Meta Title:</label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="meta_title"
                                                        maxlength="100" placeholder="" value="{{old('meta_title', isset($category)?$category->meta_title:'')}}">
                                                    <span class="help-block">{{$errors->first('meta_title')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="col-md-4">
                                            <div class="">
                                                <label class="control-label">Meta Keywords:</label>
                                                <div class="">
                                                    <textarea class="form-control maxlength-handler" rows="1" name="meta_keywords" maxlength="255">{{old('meta_keywords',(isset($category))?$category->meta_keywords:'')}}</textarea>
                                                    <span class="help-block"> max 255 chars </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="{{($errors->first('meta_description'))?'has-error':''}}">
                                                <label class="control-label">Meta Description:</label>
                                                <div class="">
                                                    <textarea class="form-control maxlength-handler" rows="1" name="meta_description" maxlength="255">{{old('meta_description',isset($category)?$category->meta_description:'')}}</textarea>
                                                    <span class="help-block"> max 255 chars </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(isset($category))
                                    <hr>
                                    <div class="row">
                                         <div class="col-md-4">
                                            
                                            <div class="{{($errors->first('status'))?'has-error':''}}">
                                                <label class="control-label">Status:<span class="required"> * </span></label>
                                                <div class="">
                                                    <select class="form-control input-sm" name="status">
                                                        @foreach(['1'=>'Active','0'=>'Inactive'] as $val=>$label)
                                                        <option value="{{$val}}" @if(old('status', isset($category)?$category->status:1)==$val) selected @endif>
                                                            {{$label}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="help-block">{{$errors->first('status')}}</span>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="{{($errors->first('banner_image'))?'has-error':''}}">
                                                <label class="control-label" for="input-image">Banner <small>(200x300)</small>:<span class="required"> * </span></label>
                                                <div class="">
                                                    <a href="" id="thumb-image-banner" data-toggle="image" class="img-thumbnail">
                                                        <img src="{{ route('ajax.previewImage',['image'=>isset($category)?$category->image:'','type'=>'category'])}}" alt="" title="" data-placeholder="{{ url('uploads/no-image.png') }}" width="100" height="100" />
                                                    </a>
                                                    <input type="hidden" name="banner_image" value="{{isset($category)?$category->image:'' }}" id="input-image" />
                                                    <span class="help-block">{{$errors->first('banner_image')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="{{($errors->first('icon_image'))?'has-error':''}}">
                                                <label class="control-label" for="input-image-icon">Icon
                                                    <small>(32x32)</small>:<span class="required"> * </span></label>
                                                <div class="">
                                                    <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                                                        <img src="{{ route('ajax.previewImage',['image'=>isset($category)?$category->icon:'','type'=>'category']) }}" alt="" title="" data-placeholder="{{ url('uploads/no-image.png') }}" width="50" height="50" />
                                                    </a>
                                                    <input type="hidden" name="icon_image" value="{{isset($category)?$category->image:'' }}" id="input-image-icon" />
                                                    <span class="help-block">{{$errors->first('icon_image')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                            @if(isset($category))
                            <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var CategoriesAddEdit = function() {

    return {

        //main function to initiate the module
        init: function() {
            
            // $("#chkall").click(function(){
            //     if($("#chkall").is(':checked')){
            //         $("#brands > option").prop("selected", "selected");
            //         $("#brands").trigger("change");
            //     } else {
            //         $("#brands > option").removeAttr("selected");
            //         $("#brands").trigger("change");
            //     }
            // });

            $('#brands').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Brands',
                closeOnSelect: false,
            });

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.categories')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

            $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
                var $element = $(this);
                var $popover = $element.data('bs.popover');

                e.preventDefault();
                $('a[data-toggle="image"]').popover('destroy');
                if ($popover) {
                    return;
                }
                $element.popover({
                    html: true,
                    placement: 'bottom',
                    trigger: 'manual',
                    content: function() {
                        return '<button type="button" id="button-image" class="btn btn-outline btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-outline btn-danger"><i class="fa fa-trash-o"></i></button>';
                    }
                });
                $element.popover('show');
                $('#button-image').on('click', function() {
                    var $button = $(this);
                    var $icon = $button.find('> i');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element
                            .parent().find('input').attr('id') + "&thumb=" + $element
                            .attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-pencil');
                            }
                        },
                        success: function(html) {
                            $('body').append(
                                '<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' +
                                html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });
                $('#button-clear').on('click', function() {
                    $element.find('img').attr('src', $element.find('img').attr(
                        'data-placeholder'));
                    $element.parent().find('input').val('');
                    $element.popover('destroy');
                });
            });

        }

    };

}();

jQuery(document).ready(function() {
    CategoriesAddEdit.init();
});
</script>
@endpush
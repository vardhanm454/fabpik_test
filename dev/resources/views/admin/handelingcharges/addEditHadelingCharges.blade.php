@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')

<!-- BEGIN PAGE TITLE-->
<!-- <h1 class="page-title"></h1> -->
<div class="row">
                <div class="col-md-4">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-blue-sharp bold ">{{$formtitle}}</span>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            @if(isset($edithandelingCharges))
                            <form class="horizontal-form" action="{{route('admin.handelingcharges.edit',['id'=>$id])}}"
                                method="post">
                                @else
                                <form class="horizontal-form" action="{{route('admin.handelingcharges.add')}}"
                                    method="post">
                                    @endif

                                    {!! csrf_field() !!}
                                    <div class="form-body">
                                        <div class="form-group {{($errors->first('min_amount'))?'has-error':''}}">
                                            <label class="shippingchargescontrol-label">Min. Cart Amount:<span
                                                    class="required">
                                                    * </span></label>

                                            <input type="text" class="form-control maxlength-handler" name="min_amount"
                                                maxlength="100" placeholder=""
                                                value="{{old('min_amount', isset($edithandelingCharges)?$edithandelingCharges->min_amount:'')}}">
                                            <span class="help-block">{{$errors->first('min_amount')}}</span>

                                        </div>
                                        <div class="form-group {{($errors->first('max_amount'))?'has-error':''}}">
                                            <label class="shippingchargescontrol-label">Max. Cart Amount:<span
                                                    class="required">
                                                    * </span></label>

                                            <input type="text" class="form-control maxlength-handler" name="max_amount"
                                                maxlength="100" placeholder=""
                                                value="{{old('max_amount', isset($edithandelingCharges)?$edithandelingCharges->max_amount:'')}}">
                                            <span class="help-block">{{$errors->first('max_amount')}}</span>

                                        </div>
                                        <div class="form-group {{($errors->first('charge_amount'))?'has-error':''}}">
                                            <label class="shippingchargescontrol-label">Charge Cart Amount:<span
                                                    class="required">
                                                    * </span></label>

                                            <input type="text" class="form-control maxlength-handler"
                                                name="charge_amount" maxlength="100" placeholder=""
                                                value="{{old('charge_amount', isset($edithandelingCharges)?$edithandelingCharges->charge_amount:'')}}">
                                            <span class="help-block">{{$errors->first('charge_amount')}}</span>

                                        </div>
                                    </div>
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn blue form-submit" name="save" value="save"
                                            data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span>                                            {{isset($edithandelingCharges)?'Edit':'Save'}}</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-blue-sharp bold ">Handling Charges</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-bordered text-center" id="">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th class=" text-center"> # </th>
                                        <th class=" text-center"> Min. Cart Amount </th>
                                        <th class=" text-center"> Max. Cart Amount </th>
                                        <th class=" text-center"> Charge Amount </th>
                                        <th class=" text-center"> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($handelingcharge))
                                    @foreach($handelingcharge as $index=>$charges)
                                    <tr>
                                        <td width="10%">{{++$index}}</td>
                                        <td>{{$charges->min_amount}}</td>
                                        <td>{{$charges->max_amount}}</td>
                                        <td>{{$charges->charge_amount}}</td>
                                        <td><div class="">
                                            <a href="{{ route('admin.handelingcharges.edit',['id'=>$charges->id]) }}"
                                                class="btn btn-icon-only default btn-circle tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title="Edit"><span aria-hidden="true" class="icon-note"></span></a>
                                            
                                            <a href="{{route('admin.handelingcharges.delete',['id'=>$charges->id])}}"
                                                del-url="" class="btn btn-icon-only default btn-circle tooltips" data-toggle="tooltip"
                                                data-placement="top" data-original-title="Delete" title="Delete"><span aria-hidden="true" class="icon-trash"></span></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Handelingcharges = function() {

    return {

        //main function to initiate the module
        init: function() {
            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });
        }
    };
}();

jQuery(document).ready(function() {
    Handelingcharges.init();
});
</script>
@endpush
@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row brands-wrap">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">                    

                    @if(isset($brand))
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.brands.edit',['id'=>$id])}}" method="post" enctype="multipart/form-data">
                    @else
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.brands.add')}}" method="post" enctype="multipart/form-data">
                    @endif
                        {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('name'))?'has-error':''}}">
                                                <label class="control-label">Name:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="name" maxlength="100" placeholder="" value="{{old('name',(isset($brand))?$brand->name:'')}}">
                                                    <span class="help-block">{{$errors->first('name')}}</span>
                                                </div>
                                            </div>                 
                                        </div>
                                        <div class="col-md-6">
                                           <div class="{{($errors->first('show_on_home'))?'has-error':''}}">
                                                <label class="control-label">Show on home page?<span class="required"> * </span></label>
                                                <div class="">
                                                    <select  class="form-control" name="show_on_home">
                                                        <option value="">Select</option>
                                                        @foreach(['1'=>'Yes','0'=>'No'] as $val=>$label)
                                                        <option value="{{$val}}" @if(old('show_on_home',(isset($brand))?$brand->show_on_home:'') == $val) selected @endif >{{$label}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="help-block">{{$errors->first('show_on_home')}}</span>
                                                </div>
                                            </div>                     
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="">
                                                <label class="control-label">Meta Title:</label>
                                                <div class="">
                                                    <textarea class="form-control maxlength-handler" rows="3" name="meta_title" maxlength="255">{{old('meta_title',(isset($brand))?$brand->meta_title:'')}}</textarea>
                                                    <span class="help-block"> max 255 chars </span>
                                                </div>
                                            </div>                  
                                        </div>
                                        <div class="col-md-6">
                                            <div class="">
                                                <label class="control-label">Meta Keywords:</label>
                                                <div class="">
                                                    <textarea class="form-control maxlength-handler" rows="3" name="meta_keywords" maxlength="255">{{old('meta_keywords',(isset($brand))?$brand->meta_keywords:'')}}</textarea>
                                                    <span class="help-block"> max 255 chars </span>
                                                </div>
                                            </div>                    
                                        </div>
                                        <div class="col-md-6">
                                            <div class="">
                                                <label class="control-label">Meta Description:</label>
                                                <div class="">
                                                    <textarea class="form-control maxlength-handler" rows="3" name="meta_description" maxlength="255">{!! old('meta_description',(isset($brand))?$brand->meta_description:'') !!}</textarea>
                                                    <span class="help-block"> max 255 chars </span>
                                                </div>
                                            </div>                   
                                        </div>
                                        <div class="col-md-6">
                                            @if(isset($brand))
                                                <div class="">
                                                    <label class="control-label">Status:<span class="required"> * </span></label>
                                                    <div class="">
                                                        <select name="status" id="status" class="form-control">
                                                            @foreach([1=>'Active',0=>'Inactive'] as $val=>$label)
                                                            <option value="{{$val}}" {{(old('status', (isset($brand))?$brand->status:1)==$val)?'selected="selected"':''}}>{{$label}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                @endif
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="{{($errors->first('image'))?'has-error':''}}">
                                                <label class="control-label" for="input-image">Image:<span class="required"> * </span></label>
                                                <div class="">
                                                    <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                                                        <img src="{{ (isset($brand))?route('ajax.previewImage',['image'=>$brand->image,'type'=>'brand']):url('uploads/no-image.png') }}" alt="" title="" data-placeholder="{{ url('uploads/no-image.png') }}" />
                                                    </a>
                                                    <input type="hidden" name="image" value="{{ (isset($brand))?$brand->image:'' }}" id="input-image" />
                                                    <span class="help-block">{{$errors->first('image')}}</span>
                                                </div>
                                            </div>   
                                </div>

                                <div class="col-md-6">
                                    <div class="{{($errors->first('seller'))?'has-error':''}}">
                                        <label class="control-label">Seller:</label>
                                            <div class="">
                                                    <select name="seller" id="seller" class="form-control select2">
                                                        <option value="">---Select Please---</option>
                                                            @foreach($sellers as $val=>$seller)
                                                                <option value="{{$seller->id}}" @if(isset($brand)){{(
                                                                $seller->id==$brand->created_by)?'selected="selected"':''                                              }}@endif >{{$seller->name}}
                                                        </option>       
                                                            @endforeach
                                                    </select>
                                                            <span class="help-block">{{$errors->first('seller')}}</span>
                                            </div>
                                    </div>             
                                </div>
                            </div>
                            </div>


                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                @if(isset($brand))
                                    <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
                                    @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var BrandEdit = function () {

    return {

        //main function to initiate the module
        init: function () {

            $('#seller').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Search Seller',
            });
            
            $('#btn_back').on('click', function(e){
                e.preventDefault();
                window.location.href = "{{route('admin.brands')}}";
            });

            $('.form-submit').on('click', function(e){
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

            $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
                var $element = $(this);
                var $popover = $element.data('bs.popover');

                e.preventDefault();
                $('a[data-toggle="image"]').popover('destroy');
                if ($popover) {
                    return;
                }
                $element.popover({
                    html: true,
                    placement: 'bottom',
                    trigger: 'manual',
                    content: function() {
                        return '<button type="button" id="button-image" class="btn btn-outline blue" title="Change Image"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-outline red" title="Delete"><i class="fa fa-trash-o"></i></button>';
                    }
                });
                $element.popover('show');
                $('#button-image').on('click', function() {
                    var $button = $(this);
                    var $icon = $button.find('> i');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element.parent().find('input').attr('id') + "&thumb=" + $element.attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-pencil');
                            }
                        },
                        success: function(html) {
                            $('body').append('<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' + html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });
                $('#button-clear').on('click', function() {
                    $element.find('img').attr('src', $element.find('img').attr('data-placeholder'));
                    $element.parent().find('input').val('');
                    $element.popover('destroy');
                });
            });

        }

    };

}();

jQuery(document).ready(function() {    
   BrandEdit.init();
});
</script>
@endpush
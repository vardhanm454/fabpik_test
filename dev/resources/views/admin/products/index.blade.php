@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet"
    type="text/css">
@endpush

@section('content')
<style>
a.disabled {
    pointer-events: none;
    cursor: default;
}

.modal {
    text-align: center;
    padding: 0 !important;
}

.modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
}

.modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
}

.modal-lg {
    max-width: 28% !important;
}

#select2-product_tags-results .select2-results__option:before {
    content: "";
    display: inline-block;
    position: relative;
    height: 20px;
    width: 20px;
    border: 2px solid #e9e9e9;
    border-radius: 4px;
    background-color: #fff;
    margin-right: 20px;
    vertical-align: middle;
}

#select2-product_tags-results .select2-results__option[aria-selected=true]:before {
    font-family:fontAwesome;
    content: "\f00c";
    color: #fff;
    background-color: #f77750;
    border: 0;
    display: inline-block;
    padding-left: 3px;
}

#select2-product_tags-results .select2-container--default .select2-results__option[aria-selected=true] {
    background-color: #fff;
}

#select2-product_tags-results .select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: #eaeaeb;
    color: #272727;
}

#select2-product_tags-results .select2-container--default .select2-selection--multiple {
    margin-bottom: 10px;
}

#select2-product_tags-results .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
    border-radius: 4px;
}

#select2-product_tags-results .select2-container--default.select2-container--focus .select2-selection--multiple {
    border-color: #f77750;
    border-width: 2px;
        }

.select2-container--default .select2-selection--multiple {
    border-width: 2px;
}

#select2-product_tags-results .select2-container--open .select2-dropdown--below {
    border-radius: 6px;
    box-shadow: 0 0 10px rgba(0,0,0,0.5);
}

#select2-product_tags-results .select2-selection .select2-selection--multiple:after {
    content: 'hhghgh';
}

</style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Price:</strong> </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm" name="fpricefrom"
                                                id="fpricefrom" placeholder="Min" value="{{request()->fpricefrom}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control input-sm" name="fpriceto"
                                                id="fpriceto" placeholder="Max" value="{{request()->fpriceto}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label col-md-3 pd-tb-5"><strong>Status:</strong></label>
                                    <div class="col-md-9">
                                        <select name="fstatus" id="fstatus" class="form-control input-sm">
                                            <option value="">All</option>
                                            <option value="1" {{request()->fstatus=='1'?'selected="selected"':''}}>
                                                Active
                                            </option>
                                            <option value="0" {{request()->fstatus=='0'?'selected="selected"':''}}>
                                                Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 margin-bottom-5">
                                    <label class="col-md-3 pd-tb-5"><strong>Seller:</strong> </label>
                                    <div class="col-md-9">
                                        <select name="fseller" id="fseller"
                                            class="form-control form-filter input-sm select2"  data-allow-clear="true">
                                            <option value="">Select Seller</option>
                                            @if(isset($sellers))
                                            @foreach($sellers as $seller)
                                            <option value="{{$seller->id}}"
                                            {{request()->fseller==$seller->id?'selected="selected"':''}}>
                                                {{$seller->company_name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Date:</strong> </label>
                                    <div class="col-md-9">
                                        <div class="input-group  input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control date-picker input-sm" name="ffromdate" id="ffromdate"
                                                placeholder="From" value="{{request()->ffromdate}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control date-picker input-sm" name="ftodate" id="ftodate" placeholder="To" value="{{request()->ftodate}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Categories:</strong> </label>

                                    <div class="col-md-9">
                                        <select class="form-control input-sm" name="fcategory" id="fcategory">
                                        @if(request()->fcategory)
                                        @php $categories = \DB::table('view_categories')->where('path_id', request()->fcategory)->first(); @endphp

                                        <option value="{{$categories->path_id}}" selected="selected">{{$categories->path_title}}</option>
                                        @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>SKU:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fsku" id="fsku"
                                            value="{{request()->fsku}}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Name:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fname" id="fname"
                                            value="{{request()->fname}}" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                <label class="col-md-3 pd-tb-5"><strong>Unique ID:</strong> </label>
                                    <div class="col-md-9 pd-tb-5">
                                        <input type="text" class="form-control input-sm" name="funiqueid" id="funiqueid"
                                            value="{{request()->funiqueid}}" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Parent Category:</strong> </label>

                                    <div class="col-md-9 pd-tb-5">
                                        <select class="form-control input-sm select2" name="fpcategory" id="fpcategory"  data-allow-clear="true">
                                        <option value=""></option>
                                        @if(isset($categoryList))
                                            @foreach($categoryList as $category)
                                            <option value="{{$category->id}}"
                                            {{request()->fpcategory==$category->id?'selected="selected"':''}}>
                                                {{$category->title}}
                                            </option>
                                            @endforeach
                                        @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 margin-bottom-5">
                                    <label class="col-md-3 pd-tb-5"><strong>Brand:</strong> </label>
                                    <div class="col-md-9">
                                        <select name="fbrand" id="fbrand"
                                            class="form-control form-filter input-sm select2"  data-allow-clear="true">
                                            <option value="">Select Brand</option>
                                            @if(isset($brands))
                                            @foreach($brands as $brand)
                                            <option value="{{$brand->id}}"
                                            {{request()->fbrand==$brand->id?'selected="selected"':''}}>
                                                {{$brand->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Action:</strong> </label>
                                    <div class="col-md-9">

                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('admin.products')}}" title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        @can('ADM_PRD_C_PRODUCT')
                        <a href="{{route('admin.products.add')}}" class="btn btn-default btn-sm tooltips" data-toggle="tooltip"
                            data-placement="top" data-original-title="Add Product"><strong><span aria-hidden="true" class="icon-plus text-primary"></span>&nbsp;&nbsp;Add</strong>
                        </a>
                        @endcan
                        @can('ADM_PRD_D_PRODUCT')
                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip"
                            data-placement="top" data-original-title="Delete" data-action="soft-delete"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;&nbsp;Delete</strong>
                        </a>
                        @endcan
                        @can('ADM_PRD_E_PRODUCT')
                        <a href="javascript:;" id="btn_table_export" class="btn btn-default btn-sm table-export-action tooltips " data-container="body" data-placement="top" data-original-title="Export Records"data-action="product-export"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong>
                        </a>
                        @endcan
                        @can('ADM_PRD_A_TAG')
                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-placement="top" data-original-title="Assign Tags" data-toggle="tooltip" data-action="product-tags">
                            <strong><span aria-hidden="true" class="fa fa-tags"></span>&nbsp;Assign Tags</strong>
                        </a>
                        @endcan
                    </div>
                    <table class="table dt-responsive nowrap product-list-table text-center"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="4%" class="">&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" id="group-check"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="5%" class="text-center"> S.No </th>
                                <th width="15%" class="text-center"> ID </th>
                                <th width="25%" class="text-center"> Name </th>
                                <th width="10%" class="text-center"> Seller ID: </th>
                                <th width="10%" class="text-center"> Category </th>
                                <th width="10%" class="none"> SKU: </th>
                                <th width="8%" class="none"> Created at: </th>
                                <th width="8%" class="none"> Modified at: </th>
                                <th width="5%" class="text-center"> Status </th>
                                <th width="10%" class=" text-center"> Action </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="productTagsModal" tabindex="-1" role="dialog" aria-labelledby="productTagsModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="productTagsModalLabel">Update Tags!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="">
                    <label class="control-label">Tags:
                        <span class="pull-right">
                            <a href="javascript:;" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="For example, if I want to tag some of the dresses specifically for a season like Summer, Diwali or Winter." data-html="true">
                            <span aria-hidden="true" class="icon-question text-danger"></span></a>
                        </span></label>
                    <form>
                        <div class="">
                            <!-- <input type="text" class="form-control input-large" name="product_tags" id="product_tags" value="" placeholder="Ex: Summer, Diwali, Winter" title="or example, if I want to tag some of the dresses specifically for a season like Summer, Diwali or Winter." data-role="tagsinput"> -->
                            <select class="form-control input-large" name="product_tags[]" id="product_tags" multiple>
                            </select>
                            <span style="color:#e73d4a" id="result"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
    type="text/javascript">
</script>
<script src="{{ __common_asset('global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Products = function() {

    return {

        //main function to initiate the module
        init: function() {

            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

            //product Tag Search
            $('#product_tags').select2({
                allowClear:true,
                minimumInputLength: 0,
                maximumSelectionLength: 3,
                placeholder: 'Ex: Summer, Diwali, Winter',
                closeOnSelect: false,
                ajax: {
                    url: "{{route('ajax.ProductTagAutoComplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,  // search term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (tagarr) {
                                return {
                                    text: tagarr.text,
                                    id: tagarr.id,
                                    val: tagarr.id,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (tagarr) {
                    return tagarr.text;
                },
            });

            $('#fcategory').select2({
                allowClear:true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.childcategories.autocomplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (categoryArr) {
                    return categoryArr.text;
                },
            });

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.products')}}?search=1";

                if ($('#fname').val() != '') url += "&fname=" + encodeURIComponent($('#fname').val());
                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fstatus').val() != '') url += "&fstatus=" + encodeURIComponent($('#fstatus').val());
                if ($('#fpricefrom').val() != '') url += "&fpricefrom=" + encodeURIComponent($('#fpricefrom').val());
                if ($('#fpriceto').val() != '') url += "&fpriceto=" + encodeURIComponent($('#fpriceto').val());
                // if ($('#ffeatured').val() != '') url += "&ffeatured=" + encodeURIComponent($('#ffeatured').val());
                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fcategory').val()) url += "&fcategory=" + encodeURIComponent($('#fcategory').val());
                if ($('#fsku').val()) url += "&fsku=" + encodeURIComponent($('#fsku').val());
                if ($('#funiqueid').val()) url += "&funiqueid=" + encodeURIComponent($('#funiqueid').val());
                if ($('#fpcategory').val()) url += "&fpcategory=" + encodeURIComponent($('#fpcategory').val());
                if ($('#fbrand').val()) url += "&fbrand=" + encodeURIComponent($('#fbrand').val());
                // if ($('#freference').val() != '') url += "&freference="+encodeURIComponent($('#freference').val());

                window.location.href = url;
            });

            // Delete Products
            $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url,
                            type:'post',
                            success: function(result){
                                swal('Product', 'Record Deleted Successfully', "success");
                                location.reload();
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });
        }

    };

}();

jQuery(document).ready(function() {
    Products.init();
});
</script>
@endpush
@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row varient-images-wrap">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span>Manage Varients Images</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="">
                    <form class="form-horizontal" action="{{route('admin.products.variantImages',['id'=>$id])}}" method="post" enctype="multipart/form-data">

                        {!! csrf_field() !!}

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="bold">{{isset($primaryAttributes[0]->attr_display_name)?$primaryAttributes[0]->attr_display_name:''}}: </p>
                                </div>
                                    <div class="col-md-10">
                                        <div class="mt-checkbox-inline pd-0">
                                            @foreach($primaryAttributes as $index=>$attribute)                                    
                                            <label class="mt-checkbox mt-checkbox-outline"> 
                                                {{$attribute->option_name}}
                                                <input type="checkbox" value="{{$attribute->option_id}}" {{(in_array($attribute->option_id, old('primary_variant_options',[])) || array_key_exists($attribute->option_id,isset($getVarientImages)?$getVarientImages:[]))?'checked':''}} name="primary_variant_options[]" class="variant-options" />
                                                <span></span>
                                            </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @foreach($primaryAttributes as $index=>$attribute)
                            <div class="row" id="img_panel_{{$attribute->option_id}}" {!!(in_array($attribute->option_id, old('primary_variant_options',[])) || array_key_exists($attribute->option_id,isset($getVarientImages)?$getVarientImages:[]))?'':'style="display: none;"'!!}>
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <span class="{{isset($attribute->colour_code)?'badge badge-default':''}}" style="{{isset($attribute->colour_code)?'background:'.$attribute->colour_code:''}}">&nbsp;</span>
                                                <span class="caption-subject bold uppercase">{{$attribute->option_name}}</span>
                                            </div>
                                            <div class="actions">
                                                <div class="{{($errors->first('default_image_'.$attribute->option_id))?'has-error':''}}">
                                                        <!-- <label class="control-label col-md-6">Select Thumbnail:<span class="required"></span></label> -->
                                                        <div class="col-md-12">
                                                            <select name="default_image_{{$attribute->option_id}}" id="default_image_{{$attribute->option_id}}" class="form-control form-filter">
                                                                <option value="">Select Thumbnail</option>
                                                                @foreach(range(1,6) as $val) 
                                                                <option value="{{$val}}" {{old('default_image_'.$attribute->option_id,(isset($getVarientImages[$attribute->option_id]))?$getVarientImages[$attribute->option_id]['thumbnail']:'')==$val?'selected':''}}>Image {{$val}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="help-block">{{$errors->first('default_image_'.$attribute->option_id)}}</span>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row" style="margin-left: 10px;">
                                                @foreach(range(1,6) as $val)
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <a href="" id="thumb-image-{{$attribute->option_id}}-{{$val}}"
                                                            data-toggle="image" class="img-thumbnail">
                                                            <img src="{{ route('ajax.previewImage',['image'=>(isset($getVarientImages[$attribute->option_id]))?$getVarientImages[$attribute->option_id]['images'][$val-1]:'','type'=>'productimage']) }}" alt="" title="" data-placeholder="{{ url('uploads/no-image.png') }}" style="width: 100px;height:100px;" />
                                                        </a>
                                                        <input type="hidden" name="images[{{$attribute->option_id}}][{{$val}}]" value="{{ (isset($getVarientImages[$attribute->option_id]))?$getVarientImages[$attribute->option_id]['images'][$val-1]:'' }}" id="input-image-{{$attribute->option_id}}-{{$val}}" />
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <hr>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Products = function() {

    return {

        //main function to initiate the module
        init: function() {

            @if(old('primary_variant_options'))
            @foreach(old('primary_variant_options') as $optionId)
            $('#{{$optionId}}').show();
            @endforeach
            @endif

            $(".variant-options").on('click', function() {
                var panelId = $(this).val();
                if($(this).is(':checked')) {
                    $('#img_panel_'+panelId).show();
                } else {
                    $('#img_panel_'+panelId).hide();
                }
            });

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.products')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

            $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
                var $element = $(this);
                var $popover = $element.data('bs.popover');

                e.preventDefault();
                $('a[data-toggle="image"]').popover('destroy');
                if ($popover) {
                    return;
                }

                $element.popover({
                    html: true,
                    placement: 'right',
                    trigger: 'manual',
                    content: function() {
                        return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
                    }
                });
                $element.popover('show');

                $('#button-image').on('click', function() {
                    var $button = $(this);
                    var $icon = $button.find('> i');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element
                            .parent().find('input').attr('id') + "&thumb=" + $element
                            .attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-pencil');
                            }
                        },
                        success: function(html) {
                            $('body').append(
                                '<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' +
                                html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });
                $('#button-clear').on('click', function() {
                    $element.find('img').attr('src', $element.find('img').attr(
                        'data-placeholder'));
                    $element.parent().find('input').val('');
                    $element.popover('destroy');
                });
            });
        }

    };

}();

jQuery(document).ready(function() {
    Products.init();
});
</script>
@endpush
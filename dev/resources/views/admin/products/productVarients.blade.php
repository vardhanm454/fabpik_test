@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <a href="{{route('admin.products.variantAdd',['id'=>$id],'.add')}}" class="btn btn-default btn-sm tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Add"><strong><span aria-hidden="true" class="icon-plus text-primary"></span>&nbsp;Add</strong></a>
                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete" data-action="soft-delete"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;Delete</strong></a>

                        <!-- <a href="javascript:;" id="btn_table_export" class="btn btn-default btn-sm table-group-action tooltips" data-container="body" data-placement="top" data-original-title="Export to Excel"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a> -->
                    </div>
                    <table class="table table-checkable nowrap" id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <<th class="text-center" width="5%"> S.No </th>
                                <th class="text-center" width="10%"> Name </th>
                                <th class="text-center" width="10%"> SKU </th>
                                <th class="text-center" width="13%"> Primary Variant </th>
                                <th class="text-center" width="13%"> Secondary Variant </th>
                                <th width="10%" class="text-center"> Image </th>
                                <th class="text-center" width="8%"> MRP </th>
                                <th class="text-center" width="10%"> Discount (%)</th>
                                <th class="text-center" width="10%"> Selling Price </th>
                                <th class="text-center" width="8%"> In Stock </th>
                                <th class="text-center" width="10%"> Actions </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var ProductVarients = function () {

    return {

        //main function to initiate the module
        init: function () {

            // Do Filter
            $('#btn_submit_search').on('click', function(e){
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.products')}}?search=1";

                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());

                window.location.href = url;
            });

            // Export all products data
            $('#btn_table_export').on('click', function(e){
                e.preventDefault();

                var url = "{{route('admin.products.export')}}?export=1";
                
                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());
                
                window.location.href = url;
            });

            // Delete Brand
            $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                swal('Product Varient', 'Record Deleted Successfully', "success");
                                location.reload();
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });

        }

    };

}();

jQuery(document).ready(function() {    
   ProductVarients.init();
});
</script>
@endpush
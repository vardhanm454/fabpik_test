@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
  <link href="{{ __common_asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ __common_asset('pages/css/profile-2.min.css') }}" rel="stylesheet" type="text/css" />
  <style>
        .popover{
            width:100%;
            max-width: 100%; /* Max Width of the popover (depending on the container!) */
        }
        div[data-placeholder]:not(:focus):not([data-div-placeholder-content]):before {

            content: attr(data-placeholder);

            float: left;

            margin-left: 2px;

            color: #b3b3b3;

        }
    </style>
@endpush

@section('content')

@if($seller_details->approval_status ==0)
<div class="row">
    <div class="col-md-12">
        <div class="note {{ ($seller_details->final_submission == 0)? 'note-success' : 'note-warning'}}">
            <p class="margin-bottom-10">
            @if($seller_details->final_submission == 0)
                Submit required details.
            @else
                Required details are submitted by seller, please <a href="{{route('admin.sellers.view', ['id'=>$seller_details->id])}}" class="" title="View & Verify">Take Action</a>
            @endif
            </p>
        </div>
    </div>
</div>
@else
<div class="note note-success">
    <p class="margin-bottom-10">
        Approved by Fabpik.
    </p>
</div>
@endif

@php $states = \App\Models\State::get(); @endphp
    <div class="tab-content">
        <!--tab_1_2-->
        <div class="tab-pane active" id="tab_1_3">
            <div class="row profile-account">
                <div class="col-md-3">
                    <ul class="profile-wrap ver-inline-menu tabbable margin-bottom-10">
                        <li class="active">
                            <a data-toggle="tab" href="#tab_1-1" aria-expanded="true"> <span aria-hidden="true" class="icon-info"></span> Basic Details </a> <span class="after"> </span> </li>
                        <li>
                            <a data-toggle="tab" href="#tab_3-3" aria-expanded="true"> <span aria-hidden="true" class="icon-briefcase"></span> Company Details </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab_4-4" aria-expanded="true"> <span aria-hidden="true" class="icon-pointer"></span> Warehouse Details </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab_5-5" aria-expanded="true"> <span aria-hidden="true" class="icon-credit-card"></span> Bank Details </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab_6-6" aria-expanded="true"> <span aria-hidden="true" class="icon-call-in"></span> POC Details </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab_7-7" aria-expanded="true"> <!-- <span aria-hidden="true" class="icon-call-in"></span> --><span class="percent">%</span> Commission Details </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#tab_cod" aria-expanded="true"><span aria-hidden="true" class=""></span> COD </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <div class="tab-content">
                                <div id="tab_1-1" class="tab-pane active">
                                    <form  id="form_basic_details" role="form" method="post" action="{{route('admin.sellers.basicDetails')}}">
                                        @csrf
                                        <input type="hidden" name="seller_id" value="{{$seller_details->id}}">
                                        <div class="form-body">
                                            <div class="margin-bottom-5">
                                                <h4 class="text-primary"><strong>Company Basic Details</strong></h4>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label" style="width:100%;">Name
                                                            <span class="required" aria-required="true">*</span>
                                                            <span class="pull-right">
                                                                <a href="#" class="popovers" data-toggle="popover" data-placement="bottom"  data-trigger="focus" title="" data-content="Name of the Primary Member ">
                                                                    <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                </a>
                                                            </span>
                                                        </label>
                                                        <input type="text" required name="name" id="name" value="<?php if(isset($seller_details->name)): echo $seller_details->name; endif; ?>" class="form-control">
                                                        <span class="help-block name-msg"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label" style="width:100%;">Email Address
                                                            <span class="required" aria-required="true">*</span>
                                                            <span class="pull-right">
                                                                <a href="#" class="popovers" data-toggle="popover" data-placement="bottom"  data-trigger="focus" title="" data-content="Primary Email Address to login">
                                                                    <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                </a>
                                                            </span>
                                                        </label>
                                                            <input type="text" required name="email" id="email" value="<?php if(isset($seller_details->email)): echo $seller_details->email; endif; ?>" class="form-control">
                                                        <span class="help-block email-msg"></span> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label" style="width:100%;">Mobile Number
                                                            <span class="required" aria-required="true">*</span>
                                                            <span class="pull-right">
                                                                <a href="#" class="popovers" data-toggle="popover" data-placement="bottom"  data-trigger="focus" title="" data-content="Primary Phone number to login">
                                                                    <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                </a>
                                                            </span>
                                                        </label>
                                                        <input type="text" required name="mobile" id="mobile" value="<?php if(isset($seller_details->mobile)): echo $seller_details->mobile; endif; ?>" maxlength="10" class="form-control"> 
                                                        <span class="help-block mobile-msg"></span>
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Vendor Id Status</label>
                                                        <input type="text" id="vendor_status" disabled placeholder="<?php if($seller_details->approval_status==0): echo "Not Approved"; else: echo "Approved"; endif; ?>" class="form-control"> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Product Categories</label>
                                                        <select class="form-control select2-multiple" required name="categories[]" id="categories" multiple>
                                                            <option value="">--Select--</option>
                                                        @foreach(\App\Models\Category::where('status',1)->get() as $category)
                                                            <option <?php if(in_array($category->id, old('categories',explode(',',$seller_details->categories)))): echo "selected"; endif; ?> value="{{$category->id}}">{{$category->title}}</option>
                                                        @endforeach
                                                        </select>
                                                        <span class="help-block categories-msg"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Shipping Model</label>
                                                        <div class="mt-radio-inline">
                                                          <label class="mt-radio">
                                                              <input type="radio" <?php if($seller_details->shipping_model=='f'): echo "checked"; endif; ?> class="shipping-modal" name="shipping_model" value="f" checked> Fabpik <span></span>
                                                          </label>
                                                          <label class="mt-radio">
                                                              <input type="radio" <?php if($seller_details->shipping_model=='s'): echo "checked"; endif; ?> class="shipping-modal" name="shipping_model" value="s"> Self <span></span>
                                                          </label>
                                                        </div>
                                                        <span class="help-block shipping-modal-msg"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                
                                                <div class="col-md-6">
                                                        <div class="form-group {{($errors->first('brand_name'))?'has-error':''}}">
                                                            <label class="control-label">Brand Name
                                                            
                                                            <span class="required" aria-required="true">*</span>
                                                    <span class="pull-right">
                                                            <a href="#" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="Enter brand name for your products if you sell your own branded products" data-html="true">
                                                            <span aria-hidden="true" class="icon-question text-danger"></span>
                                                            </a>
                                                        </span>
                                                        
                                                        </label>
                                                            <input type="text" required name="brand_name" id="brand_name" value="{{ old('brand_name', isset($seller_details->brand_name)?$seller_details->brand_name:'')}}" class="form-control" data-toggle="tooltip" title=""> 
                                                            <span class="help-block brand-name-msg">{{$errors->first('brand_name')}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{($errors->first('store_name'))?'has-error':''}}">
                                                            <label class="control-label">Store Name
                                                            <span class="required" aria-required="true">*</span>

                                                                <span class="pull-right">
                                                                    <a href="#" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="Pickup name of your store on Fabpik. You can find all your products here on Fabpik." data-html="true">
                                                                    <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                    </a>
                                                                </span>
                                                            </label>
                                                            <input type="text" required name="store_name" id="store_name" value="{{ old('store_name', isset($seller_details->store_name)?$seller_details->store_name:'')}}" class="form-control" data-toggle="tooltip" title=""> 
                                                            <span class="help-block store-name-msg">{{$errors->first('store_name')}}</span>
                                                        </div>
                                                    </div>
                                            </div>

                                                <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="{{($errors->first('shop_url'))?'has-error':''}}">
                                                            <label class="control-label">Shop URL</label>
                                                            <span class="required" aria-required="true">*</span> 
                                                            <div class="row">
                                                                <div class="col-md-6 pd-tb-5 col-sm-5">
                                                                {{ env('APP_URL') }}shop/
                                                                </div>  
                                                                <div class="col-md-6 col-sm-7">
                                                                <input type="text" class="form-control" id="shop_url" name="shop_url" value="{{old('shop_url',  isset($seller_details->url_slug)?$seller_details->url_slug:'')}}">
                                                                <small><span id="showShopUrl"> e.g. puma, nike</span></small> 
                                                                </div>  
                                                            </div>                                         
                                                            <span class="help-block shop-url-msg"> {{$errors->first('shop_url')}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                    
                                                </div>
                                              {{--  <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                        <label class="control-label">Do you want to enable queries section with users?<br>
                                                            <small><strong>Note: </strong>If you enable this, users can send their queries through the application.</small></label>
                                                                <div class="mt-radio-inline">
                                                                    <label class="mt-radio">
                                                                        <input type="radio"  <?php if($seller_details->chat_enable==1): echo "checked"; endif; ?> class="shipping-modal" name="chat_enable" value="1"> Yes <span></span>
                                                                    </label>
                                                                    <label class="mt-radio">
                                                                        <input type="radio" <?php if($seller_details->chat_enable==0): echo "checked"; endif; ?> class="shipping-modal" name="chat_enable" value="0" checked> No <span></span>
                                                                    </label>
                                                                </div>
                                                            <span class="help-block shipping-modal-msg"></span>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                        </div><hr>
                                        <div class="form-actions text-right">
                                          <button type="button" id="basic_submit" class="btn blue form-submit"><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                        </div>
                                    </form>
                                </div>
                                <div id="tab_3-3" class="tab-pane">
                                        <form  id="form_company_details" role="form" method="post" action="{{route('admin.sellers.companyDetails')}}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="seller_id" value="{{$seller_details->id}}">
                                            <div class="form-body">
                                                <div class="margin-bottom-5">
                                                    <h4 class="text-primary"><strong>Company Details</strong></h4>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Name of Business/Company</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" name="company_name" id="company_name" value="<?php if(isset($seller_details->company_name)): echo $seller_details->company_name; endif; ?>" required class="form-control">
                                                            <span class="help-block company-name-msg"></span> 
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Company registration number</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" name="company_reg_no" id="company_reg_no" value="<?php if(isset($seller_details->company_reg_no)): echo $seller_details->company_reg_no; endif; ?>" class="form-control" required>
                                                            <span class="help-block company-reg-no-msg"></span>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Legal Entity</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <div class="mt-radio-inline">
                                                            <label class="mt-radio">
                                                                <input type="radio" <?php if($seller_details->legal_entity=='r'): echo "checked"; endif; ?> name="legal_entity" id="registered" class="legal_entity" value="r"> Registered <span> </span>
                                                            </label>
                                                            <label class="mt-radio">
                                                                <input type="radio" <?php if($seller_details->legal_entity=='n'): echo "checked"; endif; ?> name="legal_entity" id="not_registered" class="legal_entity" value="n"> Not Registered <span> </span>
                                                            </label>
                                                            <span class="help-block legal-entity-msg"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">GST Registered</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <div class="mt-radio-inline">
                                                            <label class="mt-radio">
                                                                <input type="radio" <?php if($seller_details->gst_registered==1): echo "checked"; endif; ?> name="gst_registered" class="gst_registered" value="1"> Yes <span> </span>
                                                            </label>
                                                            <label class="mt-radio">
                                                                <input type="radio" <?php if($seller_details->gst_registered==0): echo "checked"; endif; ?> name="gst_registered" class="gst_registered" value="0"> No <span> </span>
                                                            </label>
                                                            <span class="help-block gst-registered-msg"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Type of Company</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <div class="mt-radio-inline">
                                                            <div class="" style="{{($seller_details->legal_entity=='n')? 'display:block' : 'display:none'}}" id="individual_company">
                                                                <label class="mt-radio">
                                                                    <input type="radio" <?php if($seller_details->company_type=='Individual'): echo "checked"; endif; ?> name="company_type" class="company_type" value="Individual"> Individual <span> </span>
                                                                </label>
                                                            </div>
                                                            <div class="" style="{{($seller_details->legal_entity=='r')? 'display:block' : 'display:none'}}" id="registered_company">
                                                                <label class="mt-radio">
                                                                    <input type="radio" <?php if($seller_details->company_type=='Limited Liability Partnership'): echo "checked"; endif; ?> name="company_type" class="company_type" value="Limited Liability Partnership"> Limited Liability Partnership(LLP) <span> </span>
                                                                </label>
                                                                <br>
                                                                <label class="mt-radio">
                                                                    <input type="radio" <?php if($seller_details->company_type=='Pvt Ltd'): echo "checked"; endif; ?> name="company_type" class="company_type" value="Pvt Ltd"> Private Limited <span> </span>
                                                                </label><br>
                                                                <label class="mt-radio">
                                                                    <input type="radio" <?php if($seller_details->company_type=='Public Ltd'): echo "checked"; endif; ?> name="company_type" class="company_type" value="Public Ltd"> Public Limited <span> </span>
                                                                </label><br>
                                                                <label class="mt-radio">
                                                                    <input type="radio" <?php if($seller_details->company_type=='Sole Proprietorship'): echo "checked"; endif; ?> name="company_type" class="company_type" value="Sole Proprietorship"> Sole Proprietorship <span> </span> 
                                                                </label>
                                                        </div>
                                                            <span class="help-block company-type-msg"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">GST Number</label>
                                                        <input type="text" name="gst_no" id="gst_no" value="<?php if(isset($seller_details->gst_no)){ echo $seller_details->gst_no; } ?>" class="form-control"> 
                                                        <span class="help-block gst-no-msg"></span> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">GST Certififcate</label>
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="input-group">
                                                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                <span class="fileinput-filename"> </span>
                                                            </div>
                                                            <span class="input-group-addon btn default btn-file">
                                                                <span class="fileinput-new"> Select file </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" id="gst_certificate" name="gst_certificate" data-checkstatus="<?php if(isset($seller_details->gst_certificate)): echo 1; else: echo 0; endif; ?>"> </span>
                                                        </div>
                                                    </div> 
                                                        <span class="help-block gst-certificate-msg"></span>
                                                        @if(isset($seller_details->gst_certificate))
                                                        <a href = "<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/gst_certificate/'.$seller_details->gst_certificate; ?>" target="_blank">{{isset($seller_details->gst_certificate)?'GST Certificate Uploaded':''}}</a>
                                                        @endif
                                                        {{-- <input type="file" id="gst_certificate" name="gst_certificate" class="form-control" data-checkstatus="<?php if(isset($seller_details->gst_certificate)): echo 1; else: echo 0; endif; ?>">  
                                                            --}} 
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Incorporation Certificate</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="input-group">
                                                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                <span class="fileinput-filename"> </span>
                                                            </div>
                                                            <span class="input-group-addon btn default btn-file">
                                                                <span class="fileinput-new"> Select file </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" id="incorporation_certificate" name="incorporation_certificate" data-checkstatus="<?php if(isset($seller_details->incorporation_certificate)): echo 1; else: echo 0; endif; ?>"> </span>
                                                        </div>
                                                    </div>
                                                        <span class="help-block incorporation-certificate-msg"></span>
                                                        @if(isset($seller_details->incorporation_certificate))
                                                        <input type="hidden" value="true" name="incorporation_verified" id="incorporation_verified">
                                                        <a href = "<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/incorporation_certificate/'.$seller_details->incorporation_certificate; ?>" target="_blank">{{isset($seller_details->incorporation_certificate)?'Incorporation Certificate Uploaded':''}}</a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Office Phone Number</label>
                                                        <input type="text" name="office_phone_no" id="office_phone_no" value="<?php if(isset($seller_details->office_phone_no)){ echo $seller_details->office_phone_no; } ?>" required maxlength="10" class="form-control">
                                                        <span class="help-block office-phone-no-msg"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Address</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <textarea class="form-control" id="company_address" name="company_address" required rows="3"><?php if(isset($seller_details->company_address)): echo $seller_details->company_address; endif; ?></textarea>
                                                        <span class="help-block company-address-msg"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">State</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <select class="form-control" name="company_state" id="company_state">
                                                            <option value="">Select State</option>
                                                        @foreach($states as $state)
                                                            <option <?php if($seller_details->company_state==$state->id): echo "selected"; endif; ?> value="{{$state->id}}">{{$state->name}}</option>
                                                        @endforeach
                                                        </select>
                                                        <span class="help-block company-state-msg"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Pincode</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <input type="text" name="company_pincode" id="company_pincode" value="<?php if(isset($seller_details->company_pincode)): echo $seller_details->company_pincode; endif; ?>" required class="form-control">
                                                        <span class="help-block company-pincode-msg"></span>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                        </div>
                                        <div class="form-actions text-right">
                                            <button type="button" id="company_submit" class="btn blue form-submit"> <span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                        </div>
                                    </form>
                                </div>
                                <div id="tab_4-4" class="tab-pane">
                                        <form  id="form_warehouse_details" role="form" method="post" action="{{route('admin.sellers.warehouseDetails')}}">
                                            @csrf
                                            <div class="form-body">
                                                <div class="margin-bottom-5">
                                                <h4 class="text-primary">
                                                    <span class="pull-left"><strong>Warehouse Details</strong></span>
                                                    <span class="pull-right">
                                                        <a href="#" class="popovers text-dark" data-toggle="popover" data-placement="left"  data-trigger="focus" title="" data-content="Note that the following location will be your pickup point for collecting the packages. The Default Location is from where the package will be picked up.">
                                                            <span aria-hidden="true" class="icon-question text-danger"></span>
                                                        </a>
                                                    </span>
                                                </h4>
                                            </div>
                                            <div class="clearfix"></div>
                                            <hr>
                                            <input type="hidden" name="warehouse_id" value="" id="warehouse_id">
                                            <input type="hidden" name="seller_id" value="{{$seller_details->id}}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Address Type</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <input type="text" name="warehouse_name" id="warehouse_name" class="form-control" value="" required>
                                                        <small>Example: Bombay-AndheriWarehouse</small>
                                                        <span class="help-block warehouse-name-msg"> </span>
                                                        <!-- <span class="help-block warehouse-name-msg"> Ex: Mumbai head office, office 2, etc. </span> -->
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group {{(Session::has('address'))?'has-error':''}}">
                                                        <label class="control-label">Address</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <textarea class="form-control" name="warehouse_address" id="warehouse_address" value="" placeholder="House no / Flat no, Building Name" required rows="1"></textarea> 
                                                        <span class="help-block warehouse-address-msg">{{Session::get('address')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group {{(Session::has('addresstwo'))?'has-error':''}}">
                                                        <label class="control-label">Address Line 2</label>
                                                        <input type="text" name="warehouse_addresstwo" id="warehouse_addresstwo" class="form-control" placeholder="Location / Street / Area" value="">
                                                        <small>Ex: Hitech City, etc.</small>
                                                        <span class="help-block warehouse-addresstwo-msg">{{Session::get('addresstwo')}}</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Landmark</label>
                                                        <input type="text" name="warehouse_landmark" id="warehouse_landmark" class="form-control" value="">
                                                        <small>Ex: Near Image Hospital, etc.</small>
                                                        <span class="help-block warehouse-landmark-msg"> </span>
                                                    </div>
                                                </div>
                                                </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Postal Code</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <input type="text" name="warehouse_pincode" id="warehouse_pincode" value="" required class="form-control"> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">City</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <input type="text" name="warehouse_city" id="warehouse_city" class="form-control" value="" required>
                                                        <span class="help-block warehouse-city-msg"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">State</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <select class="form-control" name="warehouse_state" id="warehouse_state" required>
                                                            <option value="">Select State</option>
                                                        @foreach($states as $state)
                                                            <option value="{{$state->id}}">{{$state->name}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label class="control-label">Shiprocket Pickup Name</label>
                                                        <input type="text" name="shiprocket_pickup_nickname" id="shiprocket_pickup_nickname" value="{{old('shiprocket_pickup_nickname')}}" minlength="3" maxlength="8" class="form-control" readonly>
                                                    <span class="help-block shiprocket-pickup-name-msg">{{$errors->first('shiprocket_pickup_nickname')}}</span>    
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="row">
                                                <input type="hidden" name="warehouse_country" id="warehouse_country" value=""class="form-control" readonly> 
                                                
                                               

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Make It Default</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <div class="mt-radio-inline">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="is_default" class="is_default" id="is_default_1" value="1"> Yes
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="is_default" class="is_default" id="is_default_0" value="0"> No
                                                            </label>
                                                            <span class="help-block is-default-msg"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="form-actions text-right"> 
                                                <button type="button" id="warehouse_submit" class="btn blue"><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                            </div>
                                        </form>
                                        <hr>
                                        <div class="row">
                                            
                                            <?php if(!empty($seller_warehouse_details)){ 
                                                foreach($seller_warehouse_details as $seller_warehouse){
                                            ?>    
                                            <div class="col-md-6">
                                                <div class="panel panel-default">
                                                    <div class="panel panel-heading">
                                                        <div class="row">
                                                        <div class="col-md-8 pd-tb-10"> <span class="bold uppercase"> <?php echo $seller_warehouse->name; if($seller_warehouse->is_default==1): echo " (Default)"; endif; ?></span> </div>
                                                      <div class="col-md-4 text-right">
                                                            <a class="btn btn-circle btn-icon-only btn-default edit_warehouse" href="javascript:;" data-warehouseid="{{$seller_warehouse->id}}"> <i class="icon-pencil"></i> </a>
                                                            <a class="btn btn-circle btn-icon-only btn-default delete_warehouse" data-warehouseid="{{$seller_warehouse->id}}" href="javascript:;"> <i class="icon-trash"></i> </a>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-body">
                                                        <p> <?php echo $seller_warehouse->address; ?>,
                                                        <br> 
                                                        <p> <?php echo $seller_warehouse->address_two; ?>,
                                                        <?php echo $seller_warehouse->landmark; ?>,
                                                        <?php echo $seller_warehouse->city; ?>, 
                                                        <?php echo $seller_warehouse->state; ?> <br> 
                                                        <?php echo $seller_warehouse->pincode; ?>. </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } } ?>
                                            
                                        </div>
                                </div>
                                <div id="tab_5-5" class="tab-pane">
                                    <form  id="form_bank_details" role="form" method="post" action="{{route('admin.sellers.bankDetails')}}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="seller_id" value="{{$seller_details->id}}">
                                        <div class="form-body">
                                            <div class="margin-bottom-5">
                                                <h4 class="text-primary"><strong>Bank Details</strong></h4>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Name of Account Holder</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <input type="text" id="bank_account_name" required name="bank_account_name" value="<?php if(isset($seller_details->bank_account_name)): echo $seller_details->bank_account_name; endif; ?>" class="form-control">
                                                        <span class="help-block bank-account-name-msg"></span>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Bank Name</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <input type="text" id="bank_name" required name="bank_name" value="<?php if(isset($seller_details->bank_name)): echo $seller_details->bank_name; endif; ?>" class="form-control">
                                                        <span class="help-block bank-name-msg"></span>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Account Number</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <input type="text" id="bank_account_no" required name="bank_account_no" value="<?php if(isset($seller_details->bank_account_no)): echo $seller_details->bank_account_no; endif; ?>" class="form-control">
                                                        <span class="help-block bank-account-no-msg"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Confirm Account Number</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <input type="password" id="confirm_account_no" required name="confirm_account_no" value="<?php if(isset($seller_details->bank_account_no)): echo $seller_details->bank_account_no; endif; ?>" class="form-control" oncopy="return false" onpaste="return false">
                                                        <span class="help-block confirm-account-no-msg"></span>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">IFSC Code</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <input type="text" id="ifsc_code" required name="ifsc_code" value="<?php if(isset($seller_details->ifsc_code)): echo $seller_details->ifsc_code; endif; ?>" class="form-control">
                                                        <span class="help-block ifsc-code-msg"></span>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Swift Code</label>
                                                        <input type="text" id="swift_code" required name="swift_code" value="<?php if(isset($seller_details->swift_code)): echo $seller_details->swift_code; endif; ?>" class="form-control">
                                                        <span class="help-block swift-code-msg"></span>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Branch Name</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <input type="text" id="branch_name" required name="branch_name" value="<?php if(isset($seller_details->branch_name)): echo $seller_details->branch_name; endif; ?>" class="form-control">
                                                        <span class="help-block branch-name-msg"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Upload Cancelled Cheque</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="input-group">
                                                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                <span class="fileinput-filename"> </span>
                                                            </div>
                                                            <span class="input-group-addon btn default btn-file">
                                                                <span class="fileinput-new"> Select file </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" id="cancelled_cheque_file" name="cancelled_cheque_file" data-checkstatus="<?php if(isset($seller_details->cancelled_cheque_file)): echo 1; else: echo 0; endif; ?>"> </span>
                                                        </div>
                                                        <span class="help-block cancelled-cheque-file-msg"></span>
                                                        @if(isset($seller_details->cancelled_cheque_file))
                                                        <input type = "hidden" name="cheque_uploded" value="{{$seller_details->cancelled_cheque_file}}">
                                                            <a href = "<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/cancelled_cheque_files/'.$seller_details->cancelled_cheque_file; ?>" target="_blank">{{isset($seller_details->cancelled_cheque_file)?'uploaded Cancelled Cheque ':''}}</a>
                                                        @endif
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div><hr>
                                        <div class="form-actions text-right">
                                          <button type="button" id="bank_submit" class="btn blue form-submit"> <span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                        </div>
                                    </form>
                                </div>
                                <div id="tab_6-6" class="tab-pane">
                                    <form  id="form_poc_details" role="form" method="post" action="{{route('admin.sellers.pocDetails')}}">
                                        @csrf
                                        <input type="hidden" name="seller_id" value="{{$seller_details->id}}">
                                        <div class="form-body">
                                            <div class="margin-bottom-5">
                                                <h4 class="text-primary"><strong>POC Details</strong></h4>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                          <label class="control-label">Individual Representing Company</label>
                                                          <span class="required" aria-required="true">*</span>
                                                          <input type="text" id="poc_name" required name="poc_name" value="<?php if(isset($seller_details->poc_name)): echo $seller_details->poc_name; endif; ?>" class="form-control">
                                                          <span class="help-block poc-name-msg"></span>
                                                        </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                          <label class="control-label">Designation of the Individual</label>
                                                          <span class="required" aria-required="true">*</span>
                                                          <input type="text" id="poc_designation" required name="poc_designation" value="<?php if(isset($seller_details->poc_designation)): echo $seller_details->poc_designation; endif; ?>" class="form-control">
                                                          <span class="help-block poc-designation-msg"></span>
                                                        </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                          <label class="control-label">Individual Email Id</label>
                                                          <span class="required" aria-required="true">*</span>
                                                          <input type="text" id="poc_email" required name="poc_email" value="<?php if(isset($seller_details->poc_email)): echo $seller_details->poc_email; endif; ?>" class="form-control">
                                                          <span class="help-block poc-email-msg"></span>
                                                        </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                          <label class="control-label">Phone Number</label>
                                                          <span class="required" aria-required="true">*</span>
                                                          <input type="text" id="poc_mobile" required name="poc_mobile" value="<?php if(isset($seller_details->poc_mobile)): echo $seller_details->poc_mobile; endif; ?>" class="form-control">
                                                          <span class="help-block poc-mobile-msg"></span>
                                                        </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                          <label class="control-label">Alternate Phone Number</label>
                                                          <input type="text" id="poc_phone" name="poc_phone" value="<?php if(isset($seller_details->poc_phone)): echo $seller_details->poc_phone; endif; ?>" class="form-control">
                                                          <span class="help-block poc-phone-msg"></span>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-actions text-right">
                                            <button type="button" id="poc_submit" class="btn blue form-submit"> <span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                        </div>
                                    </form>
                                </div>
                                <div id="tab_7-7" class="tab-pane">
                                    <form  id="form_commission_details" role="form" method="post" action="{{route('admin.sellers.commissionDetails')}}">
                                        @csrf
                                        <input type="hidden" name="seller_id" value="{{$seller_details->id}}">
                                        <div class="form-body">
                                            <div class="margin-bottom-5">
                                                <h4 class="text-primary"><strong>Commission</strong></h4>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Commission Rate (%)</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <input type="number" id="commission_value" required name="commission_value" value="<?php if(isset($seller_details->commission)): echo $seller_details->commission; endif; ?>" class="form-control">
                                                        <span class="help-block commission-value-msg"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Commission Model</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        @php $commissionModels = App\Models\CommissionModel::where('status', 1)->whereNull('deleted_at')->get() @endphp
                                                        <select class="form-control select2" required name="commission_type" id="commission_type">
                                                            <option value="">Select</option>                                                         
                                                            
                                                            @foreach($commissionModels as $model)
                                                                <option value="{{$model->id}}" <?php if($seller_details->commission_id == $model->id): echo "selected = 'selected'"; endif; ?> >{{$model->name}}</option>
                                                            @endforeach                      
                                                            
                                                        </select>
                                                        <span class="help-block commission-type-msg"></span>
                                                    </div>

                                                </div>
                                            </div>
                                            <span><?php if(isset($seller_details->commission_accept)): echo (($seller_details->commission_accept == 1)?'<b>Seller Accepted Current Commission Rate</b>':'<b>Seller Acceptence is pending</b>') ; endif; ?></span>

                                            <hr>
                                            <div class="form-actions text-right">
                                                <button type="button" id="commission_submit" class="btn blue form-submit"> <span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="tab_cod" class="tab-pane">
                                    <div class="form-body">
                                        <div class="margin-bottom-5">
                                            <h4 class="text-primary"><strong>Cash On Delivery</strong></h4>
                                        </div>
                                        <hr>
                                        <form  id="form_commission_details" role="form" method="post" action="{{ route('admin.sellers.changeCodDetails') }}">
                                            @csrf
                                            <input type="hidden" name="seller_id" value="{{$seller_details->id}}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Want to enable Cash On Delivery for your products?</label>
                                                        <span class="required" aria-required="true">*</span>
                                                        <div class="mt-radio-inline">
                                                        
                                                        <label class="mt-radio">
                                                            <input type="radio" <?php if($seller_details->cod_enable=='y'): echo "checked"; endif; ?> class="cod-enable" name="cod_enable" value="y" checked> Yes <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" <?php if($seller_details->cod_enable=='n'): echo "checked"; endif; ?> class="cod-enable" name="cod_enable" value="n"> No <span></span>
                                                        </label>
                                                        </div>
                                                        <input type = "hidden" value="cod_enable" name="cod_column_name">
                                                        <input type = "hidden" value="{{$seller_details->cod_enable}}" name="cod_old_value">
                                                        <span class="help-block cod-enable-value-msg"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-actions text-right">
                                                <button type="submit" id="cod_change" class="btn blue form-submit"><span aria-hidden="true" class="fa fa-check-circle"></span> Submit</button>
                                            </div>
                                        </form>                                 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end col-md-9-->
            </div>
        </div>
        <!--end tab-pane-->
    </div>

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/autosize/autosize.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('pages/scripts/components-form-tools.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('pages/scripts/form-validation.min.js') }}" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCea7use-zSkFZxumDrado3k8WSHPbUlk&libraries=places"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
     
    google.maps.event.addDomListener(window, 'load', function () {
        
        address1Field = document.querySelector("#warehouse_address");
        // address2Field = document.querySelector("#warehouse_landmark");
        address2Field = document.querySelector("#warehouse_addresstwo");
        cityField = document.querySelector("#warehouse_city");
        countryField = document.querySelector("#warehouse_country");
        postalField = document.querySelector("#warehouse_pincode");
        stateField = document.querySelector("#warehouse_state");
        

        var places = new google.maps.places.Autocomplete(address1Field, {
            // componentRestrictions: { country: ["in"] },
            // fields: ["address_components", "geometry"],
            // types: ["address"],
        });

        google.maps.event.addListener(places, 'place_changed', function () {

            // Get the place details from the autocomplete object.
            const place = places.getPlace();
            
            let address1 = "";
            let postcode = "";
            let city = "";
            let state = "";
            let country = "";
                
            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            // place.address_components are google.maps.GeocoderAddressComponent objects
            // which are documented at http://goo.gle/3l5i5Mr
            
            // console.log(place.address_components);
            for (const component of place.address_components) {
                const componentType = component.types[0];
                // console.log(componentType);
                switch (componentType) {
                    case "sublocality_level_2":{
                        address1 = `${component.long_name}, ${address1}`;
                        break;
                    }
                    case "sublocality_level_1":{
                        address1 += `${component.long_name}`;
                        break;
                    }

                    case "postal_code": {
                        postcode = `${component.long_name}${postcode}`;
                        break;
                    }

                    case "postal_code_suffix": {
                        postcode = `${postcode}-${component.long_name}`;
                        break;
                    }

                    case "locality":{
                        city = component.long_name;
                        break;
                    }
                    
                    case "administrative_area_level_1": {
                        state = component.long_name;
                        break;
                    }
                    
                    case "country":{
                        country = component.long_name;
                        break;
                    }
                }
            }
       
            address1Field.value = place.name + ', ' + address1;
            cityField.value = city;
            // countryField.value = country;
            postalField.value = postcode;
            
            $("#warehouse_state option").each(function() {
                // console.log(state);
                // console.log($(this).text());
                if($(this).text() == state) {
                    $(this).attr('selected', 'selected');            
                }                        
            });
            
            // stateField.value = state;

            // After filling the form with address components from the Autocomplete
            // prediction, set cursor focus on the second address line to encourage
            // entry of subpremise information such as apartment, unit, or floor number.
            address2Field.focus();
        });
    });

    var re = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;
    
    //Basic Details Form Validations and submit form
    $('#basic_submit').click(function(){
        var name = $('#name').val();
        if(name==''){
            $('#name').parent('div.form-group').addClass('has-error');
            $('.name-msg').text('Please Enter Name.');
            $('#name').focus();
            return false;
        }else{
            if(name.length<=3){
                $('#name').parent('div.form-group').addClass('has-error');
                $('.name-msg').text('Please Enter more than 3 characters.');
                $('#name').focus();
                return false;
            }
            // else{
            //     var nameRegex = /^[a-zA-Z]+$/;
            //     var fullnameRegex = /^[a-zA-Z]+ [a-zA-Z]+$/;
            //     if(!name.match(fullnameRegex)){
            //         if(!name.match(nameRegex)){
            //             $('#name').parent('div.form-group').addClass('has-error');
            //         $('.name-msg').text('Please Enter Valid Name.');
            //         $('#name').focus();
            //         return false;
            //         } 
            //     }
            // }
        }

        var email = $('#email').val();
        var emailRegex = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        if(email==''){
            $('#email').parent('div.form-group').addClass('has-error');
            $('.email-msg').text('Please Enter Email.');
            $('#email').focus();
            return false;
        }else{
            if(!email.match(emailRegex)){
                $('#email').parent('div.form-group').addClass('has-error');
                $('.email-msg').text('Please Enter Valid Email.');
                $('#email').focus();
                return false;
            }
        }

        var mobile = $('#mobile').val();
        var mobileRegex = /[0-9]{10}/;
        if(mobile==''){
            $('#mobile').parent('div.form-group').addClass('has-error');
            $('.mobile-msg').text('Please Enter Mobile.');
            $('#mobile').focus();
            return false;
        }else{

            if(!mobile.match(mobileRegex)){
                $('#mobile').parent('div.form-group').addClass('has-error');
                $('.mobile-msg').text('Please Enter Valid Number.');
                $('#mobile').focus();
                return false;
            }
        }

        //     var categories = $('#categories').val();
        //     if(categories=='' || categories==null){
        //         $('#categories').parent('div.form-group').addClass('has-error');
        //         $('.categories-msg').text('Please Select Categories.');
        //         $('#categories').focus();
        //         return false;
        //     }

        var brand_name = $('#brand_name').val();
        // var brandNameRegex =  /^[A-Za-z][A-Za-z0-9]*(?:[A-Za-z0-9-\s]+)*$/;
        if(brand_name == ''){
            $('#brand_name').parent('div.form-group').addClass('has-error');
            $('.brand-name-msg').text('Please Enter Brand Name.');
            $('#brand_name').focus();
            return false;
        }
        // else if(!brand_name.match(brandNameRegex)){
        //     $('#brand_name').parent('div.form-group').addClass('has-error');
        //     $('.brand-name-msg').text('Please Enter Valid Brand Name.');
        //     $('#brand_name').focus();
        //     return false;
        // }

        var store_name = $('#store_name').val();
        var storeNameRegex =  /^[A-Za-z][A-Za-z0-9]*(?:[A-Za-z0-9-\s]+)*$/;
        if(store_name == ''){
            $('#store_name').parent('div.form-group').addClass('has-error');
            $('.store-name-msg').text('Please Enter Store Name.');
            $('#store_name').focus();
            return false;
        }
        else if(!store_name.match(storeNameRegex)){
            $('#store_name').parent('div.form-group').addClass('has-error');
            $('.store-name-msg').text('Please Enter Valid Store Name.');
            $('#store_name').focus();
            return false;
        }

        var shop_url = $('#shop_url').val();
        // var shopNameRegex = /^[A-Za-z0-9-]*$/;
        var shopNameRegex = /^[A-Za-z][A-Za-z0-9]*(?:[A-Za-z0-9-]+)*$/;
        if(shop_url == ''){
            $('#shop_url').parent('div.form-group').addClass('has-error');
            $('.shop-url-msg').text('Please Enter Shop URL.');
            $('#shop_url').focus();
            return false;
        }
        if(!shop_url.match(shopNameRegex)){
            $('#shop_url').parent('div.form-group').addClass('has-error');
            $('.shop-url-msg').text('Please Enter Valid Shop URL.');
            $('#shop_url').focus();
            return false;
        }
        
        // var chat_enable = $("input[name='chat_enable']:checked ").val();
        // if(typeof chat_enable === "undefined"){
        //     $('#chat_enable').parent('div.form-group').addClass('has-error');
        //     $('.chat-modal-msg').text('Please Select.');
        //     $('#chat_enable').focus();
        //     return false;
        // }
        
        $('#form_basic_details').submit();
    });

    //Commission Details Form Validations and submit form
    $('#commission_submit').click(function(){
        var comValRegex = /^[1-9]\d*(\.\d+)?$/;

        var commission_value = $('#commission_value').val();
        if(commission_value==''){
            $('#commission_value').parent('div.form-group').addClass('has-error');
            $('.commission-value-msg').text('Please Enter Commission Rate.');
            $('#commission_value').focus();
            return false;
        } else if(!commission_value.match(comValRegex)){
            $('#commission_value').parent('div.form-group').addClass('has-error');
            $('.commission-value-msg').text('Please Enter Valid Commission Rate.');
            $('#commission_value').focus();
            return false;
        } else if(commission_value>100){
            $('#commission_value').parent('div.form-group').addClass('has-error');
            $('.commission-value-msg').text('Commission Not Greater than 100.');
            $('#commission_value').focus();
            return false;
        }

        var commission_type = $('#commission_type').val();
        if(commission_type==''){
            $('#commission_type').parent('div.form-group').addClass('has-error');
            $('.commission-type-msg').text('Please Select Commission Type.');
            $('#commission_type').focus();
            return false;
        }

        $('#form_commission_details').submit();

    });

    //POC Details Form Validations and submit form
    $('#poc_submit').click(function(){
        var poc_name = $('#poc_name').val();
        if(poc_name==''){
            $('#poc_name').parent('div.form-group').addClass('has-error');
            $('.poc-name-msg').text('Please Enter Individual Name.');
            $('#poc_name').focus();
            return false;
        }else{
            if(poc_name.length<=3){
                $('#poc_name').parent('div.form-group').addClass('has-error');
                $('.poc-name-msg').text('Please Enter more than 3 characters.');
                $('#poc_name').focus();
                return false;
            }
            // else{
            //     var pocNameRegex = /^[a-zA-Z]+$/;
            //     var pocFullnameRegex = /^[a-zA-Z]+ [a-zA-Z]+$/;
            //     if(!poc_name.match(pocFullnameRegex)){
            //         if(!poc_name.match(pocNameRegex)){
            //             $('#poc_name').parent('div.form-group').addClass('has-error');
            //         $('.poc-name-msg').text('Please Enter Valid Name.');
            //         $('#poc_name').focus();
            //         return false;
            //         } 
            //     }
            // }
        }

        var poc_designation = $('#poc_designation').val();
        if(poc_designation==''){
            $('#poc_designation').parent('div.form-group').addClass('has-error');
            $('.poc-designation-msg').text('Please Enter Designation.');
            $('#poc_designation').focus();
            return false;
        }

        var poc_email = $('#poc_email').val();
        var pocEmailRegex = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        if(poc_email==''){
            $('#poc_email').parent('div.form-group').addClass('has-error');
            $('.poc-email-msg').text('Please Enter POC Email.');
            $('#poc_email').focus();
            return false;
        }else{
            if(!poc_email.match(pocEmailRegex)){
                $('#poc_email').parent('div.form-group').addClass('has-error');
                $('.poc-email-msg').text('Please Enter Valid Email.');
                $('#poc_email').focus();
                return false;
            }
        }

        var poc_mobile = $('#poc_mobile').val();
        var pocMobileRegex = /[0-9]{10}/;
        if(poc_mobile==''){
            $('#poc_mobile').parent('div.form-group').addClass('has-error');
            $('.poc-mobile-msg').text('Please Enter POC Mobile.');
            $('#poc_mobile').focus();
            return false;
        }else{

            if(!poc_mobile.match(pocMobileRegex)){
                $('#poc_mobile').parent('div.form-group').addClass('has-error');
                $('.poc-mobile-msg').text('Please Enter Valid Number.');
                $('#poc_mobile').focus();
                return false;
            }
        }

        var poc_phone = $('#poc_phone').val();
        var pocPhoneRegex = /[0-9]{10}/;
        if(poc_phone!=''){
            if(!poc_phone.match(pocPhoneRegex)){
                $('#poc_phone').parent('div.form-group').addClass('has-error');
                $('.poc-phone-msg').text('Please Enter Valid Alternate Number.');
                $('#poc_phone').focus();
                return false;
            }
        }

        $('#form_poc_details').submit();
    });

    //Bank Details Form Validations and submit form
    $('#bank_submit').click(function(){
        var bank_account_name = $('#bank_account_name').val();
        if(bank_account_name==''){
            $('#bank_account_name').parent('div.form-group').addClass('has-error');
            $('.bank-account-name-msg').text('Please Enter Bank Account Name.');
            $('#bank_account_name').focus();
            return false;
        }else{
            if(bank_account_name.length<=3){
                $('#bank_account_name').parent('div.form-group').addClass('has-error');
                $('.bank-account-name-msg').text('Please Enter more than 3 characters.');
                $('#bank_account_name').focus();
                return false;
            }
        }

        var bank_name = $('#bank_name').val();
        if(bank_name==''){
            $('#bank_name').parent('div.form-group').addClass('has-error');
            $('.bank-name-msg').text('Please Enter Bank Name.');
            $('#bank_name').focus();
            return false;
        }

        var bank_account_no = $('#bank_account_no').val();
        var bankAccountNoRegex = /[0-9]/;
        if(bank_account_no==''){
            $('#bank_account_no').parent('div.form-group').addClass('has-error');
            $('.bank-account-no-msg').text('Please Enter Bank Account No.');
            $('#bank_account_no').focus();
            return false;
        }else{
            if(!bank_account_no.match(bankAccountNoRegex)){
                $('#bank_account_no').parent('div.form-group').addClass('has-error');
                $('.bank-account-no-msg').text('Please Enter Valid Bank Account No.');
                $('#bank_account_no').focus();
                return false;
            }
        }

        var confirm_account_no = $('#confirm_account_no').val();
        var confirmAccountNoRegex = /[0-9]/;
        if(confirm_account_no==''){
            $('#confirm_account_no').parent('div.form-group').addClass('has-error');
            $('.confirm-account-no-msg').text('Please Confirm Bank Account No.');
            $('#confirm_account_no').focus();
            return false;
        }else{
            if(!confirm_account_no.match(confirmAccountNoRegex)){
                $('#confirm_account_no').parent('div.form-group').addClass('has-error');
                $('.confirm-account-no-msg').text('Please Enter Valid Bank Account No.');
                $('#confirm_account_no').focus();
                return false;
            }else{
                if(confirm_account_no!=bank_account_no){
                    $('#confirm_account_no').parent('div.form-group').addClass('has-error');
                    $('.confirm-account-no-msg').text('Different Account No.');
                    $('#confirm_account_no').focus();
                    return false;
                }
            }
        }

        var ifsc_code = $('#ifsc_code').val();
        if(ifsc_code==''){
            $('#ifsc_code').parent('div.form-group').addClass('has-error');
            $('.ifsc-code-msg').text('Please Enter IFSC Code.');
            $('#ifsc_code').focus();
            return false;
        }

        var swift_code = $('#swift_code').val();
        if(swift_code==''){
            $('#swift_code').parent('div.form-group').addClass('has-error');
            $('.swift-code-msg').text('Please Enter Swift Code.');
            $('#swift_code').focus();
            return false;
        }

        var branch_name = $('#branch_name').val();
        if(branch_name==''){
            $('#branch_name').parent('div.form-group').addClass('has-error');
            $('.branch-name-msg').text('Please Enter Branch Name.');
            $('#branch_name').focus();
            return false;
        }

        var cancelled_cheque_file = $('#cancelled_cheque_file').val();
        if(cancelled_cheque_file==''){
            if($('#cancelled_cheque_file').data('checkstatus')==0){
                $('#cancelled_cheque_file').parent('div.form-group').addClass('has-error');
                $('.cancelled-cheque-file-msg').text('Please Upload Cancelled Cheque.');
                $('#cancelled_cheque_file').focus();
                return false;
            }
        }

        $('#form_bank_details').submit();
    });

    //Company Details Form Validations and submit form
    $('#company_submit').click(function(){
        var company_name = $('#company_name').val();
        if(company_name==''){
            $('#company_name').parent('div.form-group').addClass('has-error');
            $('.company-name-msg').text('Please Enter Company Name.');
            $('#company_name').focus();
            return false;
        }else{
            if(company_name.length<=3){
                $('#company_name').parent('div.form-group').addClass('has-error');
                $('.company-name-msg').text('Please Enter more than 3 characters.');
                $('#company_name').focus();
                return false;
            }
        }

        var company_reg_no = $('#company_reg_no').val();
        if(company_reg_no==''){
            $('#company_reg_no').parent('div.form-group').addClass('has-error');
            $('.company-reg-no-msg').text('Please Enter Company Reg. No.');
            $('#company_reg_no').focus();
            return false;
        }

        var legal_entity = $('#legal_entity').val();
        if(legal_entity==''){
            $('#legal_entity').parent('div.form-group').addClass('has-error');
            $('.legal-entity-msg').text('Please pick Legal Entity.');
            $('#legal_entity').focus();
            return false;
        }

        var company_type =  $("input[name='company_type']:checked ").val();
        if(typeof company_type === "undefined"){
            $('#company_type').parent('div.form-group').addClass('has-error');
            $('.company-type-msg').text('Please pick your Company Type.');
            $('#company_type').focus();
            return false;
        }

        var gst_registered = $("input[name='gst_registered']:checked ").val();
        if(typeof gst_registered === "undefined"){
            $('#gst_registered').parent('div.form-group').addClass('has-error');
            $('.gst-registered-msg').text('Please Select.');
            $('#gst_registered').focus();
            return false;
        }

        if(gst_registered != 0){
            var gst_no = $('#gst_no').val();
                if(gst_no==''){
                    $('#gst_no').parent('div.form-group').addClass('has-error');
                    $('.gst-no-msg').text('Please Enter GST No.');
                    $('#gst_no').focus();
                    return false;
                }

                var gst_certificate = $('#gst_certificate').val();
                if(gst_certificate==''){
                    if($('#gst_certificate').data('checkstatus')==0){
                        $('#gst_certificate').parent('div.form-group').addClass('has-error');
                        $('.gst-certificate-msg').text('Please Upload GST Certificate.');
                        $('#gst_certificate').focus();
                        return false;
                    }
                }else{
                    // alert(incorporation_certificate);
                    // alert(re.exec(incorporation_certificate));
                    if (!re.exec(gst_certificate)) {
                        $('#gst_certificate').parent('div.form-group').addClass('has-error');
                        $('.gst-certificate-msg').text('Please Upload jpeg/png/jpg/pdf Only.');
                        $('#gst_certificate').focus();
                        return false;
                    }
                }
        }

        var office_phone_no = $('#office_phone_no').val();
        var ofcMobileRegex = /[0-9]{10}/;
        if(office_phone_no!='' && !office_phone_no.match(ofcMobileRegex)){
            $('#office_phone_no').parent('div.form-group').addClass('has-error');
            $('.office-phone-no-msg').text('Please Enter Valid Number.');
            $('#office_phone_no').focus();
            return false;
        }

        var company_address = $('#company_address').val();
        if(company_address==''){
            $('#company_address').parent('div.form-group').addClass('has-error');
            $('.company-address-msg').text('Please Enter Company Address.');
            $('#company_address').focus();
            return false;
        }

        var incorporation_certificate = $('#incorporation_certificate').val();
        if(incorporation_certificate==''){
            if($('#incorporation_certificate').data('checkstatus')==0){
                $('#incorporation_certificate').parent('div.form-group').addClass('has-error');
                $('.incorporation-certificate-msg').text('Please Upload Incorporation Certificate.');
                $('#incorporation_certificate').focus();
                return false;
            }
        }else{
            
            if (!re.exec(incorporation_certificate)) {
                $('#incorporation_certificate').parent('div.form-group').addClass('has-error');
                $('.incorporation-certificate-msg').text('Please Upload jpeg/png/jpg/pdf Only.');
                $('#incorporation_certificate').focus();
                return false;
            }
        }

        var company_state = $('#company_state').val();
        if(company_state==''){
            $('#company_state').parent('div.form-group').addClass('has-error');
            $('.company-state-msg').text('Please Select Company State.');
            $('#company_state').focus();
            return false;
        }

        var company_pincode = $('#company_pincode').val();
        if(company_pincode==''){
            $('#company_pincode').parent('div.form-group').addClass('has-error');
            $('.company-pincode-msg').text('Please Enter Company Pincode.');
            $('#company_pincode').focus();
            return false;
        }

        $('#form_company_details').submit();
    });

    //Warehouse Details validation and submit
    $('#warehouse_submit').click(function(){
        var warehouse_name = $('#warehouse_name').val();
        if(warehouse_name==''){
            $('#warehouse_name').parent('div.form-group').addClass('has-error');
            $('.warehouse-name-msg').text('Please Enter Warehouse Name.');
            $('#warehouse_name').focus();
            return false;
        }

        var warehouse_address = $('#warehouse_address').val();
        if(warehouse_address==''){
            $('#warehouse_address').parent('div.form-group').addClass('has-error');
            $('.warehouse-address-msg').text('Please Enter Warehouse Address.');
            $('#warehouse_address').focus();
            return false;
        }

        // var warehouse_landmark = $('#warehouse_landmark').val();
        // if(warehouse_landmark==''){
        //     $('#warehouse_landmark').parent('div.form-group').addClass('has-error');
        //     $('.warehouse-landmark-msg').text('Please Enter Warehouse Landmark.');
        //     $('#warehouse_landmark').focus();
        //     return false;
        // }

        var warehouse_pincode = $('#warehouse_pincode').val();
        if(warehouse_pincode==''){
            $('#warehouse_pincode').parent('div.form-group').addClass('has-error');
            $('.warehouse-pincode-msg').text('Please Enter Warehouse Pincode.');
            $('#warehouse_pincode').focus();
            return false;
        }

        var warehouse_city = $('#warehouse_city').val();
        if(warehouse_city==''){
            $('#warehouse_city').parent('div.form-group').addClass('has-error');
            $('.warehouse-city-msg').text('Please Enter Warehouse City.');
            $('#warehouse_city').focus();
            return false;
        }

        var warehouse_state = $('#warehouse_state').val();
        if(warehouse_state==''){
            $('#warehouse_state').parent('div.form-group').addClass('has-error');
            $('.warehouse-state-msg').text('Please Enter Warehouse State.');
            $('#warehouse_state').focus();
            return false;
        }


        // var shiprocket_pickup_nickname = $('#shiprocket_pickup_nickname').val();
        // if(shiprocket_pickup_nickname==''){
        //     $('#shiprocket_pickup_nickname').parent('div.form-group').addClass('has-error');
        //     $('.shiprocket-pickup-name-msg').text('Please Enter Shiprocket Pickup Name.');
        //     $('#shiprocket_pickup_nickname').focus();
        //     return false;
        // }

        var is_default = $('input[name="is_default"]:checked').val();
        if(is_default=='' || is_default==undefined){
            $('.is_default').parent('div.form-group').addClass('has-error');
            $('.is-default-msg').text('Please select type.');
            return false;
        }

        $('#form_warehouse_details').submit();
    });

    //Edit Warehouse ajax get warehouse Data
    $('.edit_warehouse').click(function(){
        var warehouseid = $(this).data('warehouseid');
        var csrfToken = "{{ csrf_token() }}";
        var input = {
            "id" : warehouseid,
            "_token" : csrfToken
        };
        $.ajax({
            url : '{{route("admin.sellers.getWarehouseDetails")}}',
            type : 'POST',
            data : input,
            success : function(response) {
                if(response=='error'){

                }else{
                    $('#warehouse_id').val(response.id);
                    $('#warehouse_name').val(response.name);
                    $('#warehouse_address').text(response.address);
                    $('#warehouse_addresstwo').val(response.address_two);
                    $('#warehouse_landmark').val(response.landmark);
                    $('#warehouse_city').val(response.city);
                    $('#warehouse_pincode').val(response.pincode);
                    $('#warehouse_state').val(response.state_id);
                    $('#shiprocket_pickup_nickname').val(response.shiprocket_pickup_nickname);
                    if(response.is_default==1){
                        $('#is_default_1').attr('checked',true);
                    }else{
                        $('#is_default_0').attr('checked',true);
                    }
                }
            }
        });
    });

    //Delete Warehouse Details
    $('.delete_warehouse').click(function(){
        var warehouseid = $(this).data('warehouseid');
        var csrfToken = "{{ csrf_token() }}";
        var input = {
            "id" : warehouseid,
            "_token" : csrfToken
        };
        swal({
            title: 'Are you sure?',
            text: 'You want Delete the record.',
            type: 'info',
            showCancelButton: true,
            confirmButtonClass: 'btn-success',
            cancelButtonClass: 'btn-danger',
            closeOnConfirm: false,
            closeOnCancel: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
        },
        function(isConfirm){
            if (isConfirm){
                $.ajax({
                    url : '{{route("admin.sellers.deleteWarehouseDetails")}}',
                    type : 'POST',
                    data : input,
                    success: function(result){
                        swal('Warehouse', 'Record Deleted Successfully', "success");
                        location.reload();
                    }
                });
            }
            else {
                swal("Cancelled", "", "error");
            }
        });
    });

    //Show and hide type of company
     $(function() {
        // $("#individual_company").hide();
       $("input[name='legal_entity']").click(function() {

        if ($("#registered").is(":checked")) {
            $("#individual_company").hide();
            $("#registered_company").show();
         }
         if ($("#not_registered").is(":checked")) {
            $("#registered_company").hide();
            $("#individual_company").show();
         }          
       });
     });
jQuery(document).ready(function() {
    // $('#tabMenu a[href="#{{ old('tab') }}"]').tab('show');
    $('#tab_1_3 a[href="#{{ old('tab') }}"]').tab('show');
});

</script>
@endpush
@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')

@endpush

@section('content')
@php $states = \App\Models\State::get(); @endphp
<div class="row addseller-wrap">
  <div class="col-md-12">
     <div class="portlet light bordered">
        <div class="portlet-body form">
           <!-- BEGIN FORM-->
           <form action="{{route('admin.sellers.registerSeller')}}" name="register_seller_form" id="register_seller_form" method="post" class="horizontal-form">
            @csrf
              <div class="form-body">
                 <div class="row">
                    <div class="col-md-6">
                       <div class="form-group {{($errors->first('fullname'))?'has-error':''}}">
                          <label class="control-label">Full Name</label>
                          <div class="input-icon">
                            <i class="fa fa-user text-primary"></i>
                            <input type="text" id="full_name" name="fullname" value="{{old('fullname')}}" class="form-control seller-first-name" placeholder="Seller Full Name">
                          </div>
                          <span class="help-block full_name_msg"> {{$errors->first('name')}}</span>
                       </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                       <div class="form-group {{($errors->first('mobile'))?'has-error':''}}">
                          <label class="control-label">Mobile Number</label>
                          <div class="input-icon">
                          <i class="fa fa-phone text-primary"></i>
                          <input type="text" id="mobile" name="mobile" value="{{old('mobile')}}" class="form-control seller-first-name" placeholder="Enter Mobile Number">
                        </div>
                          <span class="help-block mobile_msg"> {{$errors->first('mobile')}}</span>
                       </div>
                    </div>
                    <!--/span-->
                 </div>
                 <!--/row-->
                 <div class="row">
                    <div class="col-md-6">
                       <div class="form-group {{($errors->first('email'))?'has-error':''}}">
                          <label class="control-label">Email Id</label>
                          <div class="input-icon">
                            <i class="fa fa-envelope-o text-primary"></i>
                            <input type="email" id="email" name="email" value="{{old('email')}}" class="form-control seller-emailid" placeholder="Enter Email ID">
                          </div>
                          <span class="help-block email_msg"> {{$errors->first('email')}}</span>
                       </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                       <div class="form-group {{($errors->first('confirm_email'))?'has-error':''}}">
                          <label class="control-label">Verify Email ID</label>
                          <div class="input-icon">
                            <i class="fa fa-envelope text-primary"></i>
                            <input type="email" id="confirm_email" name="confirm_email" value="{{old('confirm_email')}}" class="form-control seller-verify-emailid" placeholder="Verify Email ID">
                          </div>
                          <span class="help-block confirm_email_msg"> {{$errors->first('confirm_email')}}</span>
                       </div>
                       <!--/span-->
                    </div>
                    </div>
                    <div class="row">
                       <div class="col-md-6">
                          <div class="form-group {{($errors->first('password'))?'has-error':''}}">
                             <label class="control-label">Password</label>
                             <div class="input-icon">
                              <i class="fa fa-lock text-primary"></i>
                              <input type="password" class="form-control seller-password" name="password" id="password" placeholder="Type a password to check its strength ">
                              </div>
                             <span class="help-block password_msg"> {{$errors->first('password')}}</span>
                          </div>
                       </div>
                       <!--/span-->
                       <div class="col-md-6">
                          <div class="form-group {{($errors->first('confirm_password'))?'has-error':''}}">
                             <label class="control-label">Confirm Password</label>
                             <div class="input-icon">
                              <i class="fa fa-lock text-primary"></i>
                              <input type="password" class="form-control seller-confirm-password" name="confirm_password" id="confirm_password" placeholder="Type a password to check its strength ">
                            </div>
                             <span class="help-block confirm_password_msg"> {{$errors->first('confirm_password')}}</span>
                          </div>
                       </div>
                       <!--/span-->
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                       <div class="form-group {{($errors->first('shop_name'))?'has-error':''}}">
                          <label class="control-label">Shop Name</label>
                          <div class="input-icon">
                            <i class="fa fa-building text-primary"></i>
                            <input type="text" id="shop_name" name="shop_name" value="{{old('shop_name')}}" class="form-control seller-shop-name" placeholder="Enter Shop Name">
                          </div>
                          <span class="help-block shop_name_msg"> {{$errors->first('shop_name')}}</span>
                       </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                       <div class="form-group {{($errors->first('shop_address'))?'has-error':''}}">
                          <label class="control-label">Shop Address</label>
                          <div class="input-icon">
                            <i class="fa fa-map-marker text-primary"></i>
                            <input type="text" id="shop_address" name="shop_address" value="{{old('shop_address')}}" class="form-control seller-shop-address" placeholder="Enter Shop Address">
                          </div>
                          <span class="help-block shop_address_msg"> {{$errors->first('shop_address')}}</span>
                       </div>
                    </div>
                    <!--/span-->
                 </div>
                 </div>
                 <div class="form-actions right" id="continueDiv">
                    <button type="button" id="continue" class="btn blue">
                    <span aria-hidden="true" class="icon-cloud-download"></span> Continue</button>
                 </div><hr>
                 <div class="form-body" id="otpDiv" style="display:none;">
                    <input type="hidden" value="not_verified" id="checkVerify" name="checkVerify">
                     <div class="row">
                        <h3 class="col-md-12">Verify OTP</h3>
                        <p class="col-md-12"> Please enter the OTP (one time password) to verify your account. A Code has been sent to your Mobile. </p>
                      <label class="col-md-3">Enter OTP</label>
                      <div class="form-group col-md-6">
                          <input type="text" class="form-control" id="verifyOtpVal" name="verifyOtpVal" value="">
                          <span class="help-block" id="mobile_otp_check"></span>
                      </div>
                      <div class="col-md-3">
                          <a href="{{route('admin.sellers.send-otp')}}" class="pull-right pd-tb-5" id="resend_otp" class="resend_otp">Resend OTP</a>

                          <button type="button" id="create" class="btn blue">
                            <span aria-hidden="true" class="icon-cloud-download"></span> Submit
                        </button>
                      </div>
                    </div>
                </div>
           </form>
           
           </div>
        </div>
     </div>
  </div>
@stop

@push('PAGE_ASSETS_JS')

@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
    $('#continue').click(function(){
        var full_name = $('#full_name').val();
        if(full_name==''){
            $('#full_name').parent('div.form-group').addClass('has-error');
            $('.full_name_msg').text('Please Enter Name.');
            $('#full_name').focus();
            return false;
        }else{
            if(full_name.length<=3){
                $('#full_name').parent('div.form-group').addClass('has-error');
                $('.full_name_msg').text('Please Enter more than 3 characters.');
                $('#full_name').focus();
                return false;
            }else{
                var nameRegex = /^[a-zA-Z]+$/;
                var fullnameRegex = /^[a-zA-Z]+ [a-zA-Z]+$/;
                if(!full_name.match(fullnameRegex)){
                    if(!full_name.match(nameRegex)){
                        $('#full_name').parent('div.form-group').addClass('has-error');
                        $('.full_name_msg').text('Please Enter Valid Name.');
                        $('#full_name').focus();
                        return false;
                    } 
                }
            }
        }

        var mobile = $('#mobile').val();
        var mobileRegex = /^[6789]\d{9}$/;
        if(mobile==''){
            $('#mobile').parent('div.form-group').addClass('has-error');
            $('.mobile_msg').text('Please Enter Mobile.');
            $('#mobile').focus();
            return false;
        }else{
            if(!mobile.match(mobileRegex)){
                $('#mobile').parent('div.form-group').addClass('has-error');
                $('.mobile_msg').text('Please Enter Valid Number.');
                $('#mobile').focus();
                return false;
            }
        }

        var email = $('#email').val();
        var emailRegex = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        if(email==''){
            $('#email').parent('div.form-group').addClass('has-error');
            $('.email_msg').text('Please Enter Email.');
            $('#email').focus();
            return false;
        }else{
            if(!email.match(emailRegex)){
                $('#email').parent('div.form-group').addClass('has-error');
                $('.email_msg').text('Please Enter Valid Email.');
                $('#email').focus();
                return false;
            }
        }

        var confirm_email = $('#confirm_email').val();
        if(confirm_email==''){
            $('#confirm_email').parent('div.form-group').addClass('has-error');
            $('.confirm_email_msg').text('Please Confirm Email.');
            $('#confirm_email').focus();
            return false;
        }else{
            if(!confirm_email.match(emailRegex)){
                $('#confirm_email').parent('div.form-group').addClass('has-error');
                $('.confirm_email_msg').text('Please Enter Valid Email.');
                $('#confirm_email').focus();
                return false;
            }else{
                if(confirm_email!=email){
                    $('#confirm_email').parent('div.form-group').addClass('has-error');
                    $('.confirm_email_msg').text('Confirm Email does not match!.');
                    $('#confirm_email').focus();
                    return false;  
                }
            }
        }

        var passwordRegex = /^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\W).*$/;
        var password = $('#password').val();
        if(password==''){
            $('#password').parent('div.form-group').addClass('has-error');
            $('.password_msg').text('Please Enter New Password.');
            $('#password').focus();
            return false;
        }else{
            if(!password.match(passwordRegex)){
                $('#password').parent('div.form-group').addClass('has-error');
                $('.password_msg').text('Please Enter Valid Password.');
                $('#password').focus();
                return false;
            }
        }

        var confirm_password = $('#confirm_password').val();
        if(confirm_password==''){
            $('#confirm_password').parent('div.form-group').addClass('has-error');
            $('.confirm_password_msg').text('Please Enter Confirm Password.');
            $('#confirm_password').focus();
            return false;
        }else{
            if(!confirm_password.match(passwordRegex)){
                $('#confirm_password').parent('div.form-group').addClass('has-error');
                $('.confirm_password_msg').text('Please Enter Valid Password.');
                $('#confirm_password').focus();
                return false;
            }else{
                if(confirm_password!=password){
                    $('#confirm_password').parent('div.form-group').addClass('has-error');
                    $('.confirm_password_msg').text('Passwords do not match!.');
                    $('#confirm_password').focus();
                    return false;
                }
            }
        }

        var shop_name = $('#shop_name').val();
        if(shop_name==''){
            $('#shop_name').parent('div.form-group').addClass('has-error');
            $('.shop_name_msg').text('Please Enter Shop Name.');
            $('#shop_name').focus();
            return false;
        }

        var shop_address = $('#shop_address').val();
        if(shop_address==''){
            $('#shop_address').parent('div.form-group').addClass('has-error');
            $('.shop_address_msg').text('Please Enter Shop Address.');
            $('#shop_address').focus();
            return false;
        }

        var csrfToken = "{{ csrf_token() }}";
        var input = {
            "mobile" : mobile,
            "email" : email,
            "action" : "sent_otp",
            "_token" : csrfToken
        };
        //console.log(input); return false;
        $.ajax({
            url : '{{route("admin.sellers.send-otp")}}',
            type : 'POST',
            data : input,
            success : function(response) {
                // alert(response.type);
                // console.log(response.type);
                // console.log(response.type);
                if(response.type=='success'){
                    $('#otpDiv').show();
                    $('#continueDiv').hide();
                    $('#checkVerify').val('otp_sent');
                }else if(response.type=='validationErrors'){
                    $('.email_msg').html(response.errors.email);
                    $('.mobile_msg').html(response.errors.mobile);
                }
            }
        });

        $('#create').click(function(){
            var otp = $("#verifyOtpVal").val();
            var mobile = $("#mobile").val();
            var email = $("#email").val();
            var csrfToken = "{{ csrf_token() }}";
            var input = {
                "mobile" : mobile,
                "email" : email,
                "otp" : otp,
                "action" : "verify_otp",
                "_token" : csrfToken
            };
            if (otp.length == 4 && otp != null) {
                $.ajax({
                    url : '{{route("admin.sellers.send-otp")}}',
                    type : 'POST',
                    dataType : "json",
                    data : input,
                    success : function(response) {
                        if(response.type=='success'){
                            $('#checkVerify').val('verified');
                            $('#create').button('loading');
                            $('#register_seller_form').submit();
                        }else if(response.type=='expired'){
                            $('#verifyOtp').val('');
                        }else if(response.type=='error'){
                            $('#verifyOtp').val('');
                            $('#mobile_otp_check').html("Please enter valid OTP");
                        }
                    },
                    error : function() {
                         $('#mobile_otp_check').html("Something went wrong, try again");
                    }
                });
            }else{
                $('#mobile_otp_check').html("Please enter valid OTP");
            }
        });
    });

    $('#resend_otp').click(function(event) {
        //resend otp
        event.preventDefault();
        var csrfToken = "{{ csrf_token() }}";
        var mobile = $('#mobile').val();
        var email = $('#email').val();
        var input = {
            "mobile" : mobile,
            "email" : email,
            "action" : "sent_otp",
            "_token" : csrfToken
        };
        var url = $(this).attr('href');
        //console.log(input); return false;
        $.ajax({
            url : url,
            type : 'POST',
            data : input,
            success : function(response) {
                // alert(response);
                if(response.type=='success'){
                    $('#mobile_otp_check').html("OTP resend successfully.");
                    $('#checkVerify').val('otp_sent');
                }else{
                    $('#mobile_otp_check').html("Something went wrong, try again");
                }
            }
        });
        return false; // for good measure
    });
</script>
@endpush
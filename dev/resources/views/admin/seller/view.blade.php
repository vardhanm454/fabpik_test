@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
  <link href="{{ __common_asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ __common_asset('pages/css/profile-2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
@if(is_null($seller_details->approval_status))
<div class="row">
    <div class="col-md-12">
        <div class="note {{ ($seller_details->final_submission == 0)? 'note-success' : 'note-warning'}}">
            <p class="margin-bottom-10">
            @if($seller_details->final_submission == 0)
                Vendor need to update required details.
            @else
                Please Review and take action!
            @endif
            </p>
        </div>
    </div>
</div>
@else
<div class="note note-success">
    <p class="margin-bottom-10">
    
    {{ ($seller_details->approval_status ==1)?'Approved by Fabpik.':'Rejected By Fabpik' }}
    </p>
</div>
@endif

    <div class="tab-content">
      <!--tab_1_2-->
      <div class="tab-pane active" id="tab_1_3">
          <div class="row profile-account">
              <div class="col-md-3">
                  <ul class="profile-wrap ver-inline-menu tabbable margin-bottom-10">
                      <li class="active">
                          <a data-toggle="tab" href="#tab_1-1" aria-expanded="true"> <span aria-hidden="true" class="icon-info"></span> Basic Details </a> <span class="after"> </span> </li>
                      <li>
                          <a data-toggle="tab" href="#tab_3-3" aria-expanded="true"> <span aria-hidden="true" class="icon-briefcase"></span> Company Details </a>
                      </li>
                      <li>
                          <a data-toggle="tab" href="#tab_4-4" aria-expanded="true"> <span aria-hidden="true" class="icon-pointer"></span> Warehouse Details </a>
                      </li>
                      <li>
                          <a data-toggle="tab" href="#tab_5-5" aria-expanded="true"> <span aria-hidden="true" class="icon-credit-card"></span> Bank Details </a>
                      </li>
                      <li>
                          <a data-toggle="tab" href="#tab_6-6" aria-expanded="true"> <span aria-hidden="true" class="icon-call-in"></span> POC Details </a>
                      </li>
                      <li>
                          <a data-toggle="tab" href="#tab_7-7" aria-expanded="true"> <span class="percent">%</span> Commission Details </a>
                      </li>
                      <li>
                            <a data-toggle="tab" href="#tab_cod" aria-expanded="true"><span aria-hidden="true" class=""></span> COD </a>
                        </li>
                  </ul>
                  <hr>
                  <div class="actions">
                  @php 
                    $isDisabled = ($seller_details->final_submission == 1) ? '' : 'disabled';
                  @endphp
                  
                  @if(is_null($seller_details->approval_status))
                 
                    <div class="row">
                      <div class="col-md-6">
                        <button id="approveBtn" data-sellerid="<?php echo $seller_details->id; ?>" class="btn btn-primary btn-block pd-15" {{$isDisabled}}><span aria-hidden="true" class="icon-check"></span> Approve</button>
                      </div>
                      <div class="col-md-6">
                        <button id="rejectBtn" data-sellerid="<?php echo $seller_details->id; ?>" class="btn btn-danger btn-block pd-15"><span aria-hidden="true" class="icon-check"></span> Reject</button>
                      </div>
                    </div>
                  @elseif( $seller_details->approval_status == 0 )
                    <button id="approveBtn" data-sellerid="<?php echo $seller_details->id; ?>" class="btn btn-primary btn-block pd-15"><span aria-hidden="true" class="icon-check"></span> Approve</button>
                  @else
                    <button id="rejectBtn" data-sellerid="<?php echo $seller_details->id; ?>" class="btn btn-danger btn-block pd-15"><span aria-hidden="true" class="icon-check"></span> Reject</button>
                  @endif
                  <?php if($seller_details->approval_status==0): ?>
                    
                  <?php else: ?>
                   
                  <?php endif; ?>
                </div>
              </div>
              <div class="col-md-9">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                      <div class="tab-content">
                          <div id="tab_1-1" class="tab-pane active">
                                  <div class="form-body">
                                      <div class="margin-bottom-5">
                                        <h4 class="text-primary"><strong>Basic Details</strong></h4>
                                      </div>
                                      <hr>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="control-label">Name</label>
                                              <input type="text" disabled value="<?php if(isset($seller_details->name)): echo $seller_details->name; endif; ?>" class="form-control">
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="control-label">Email Address</label>
                                              <input type="text" disabled value="<?php if(isset($seller_details->email)): echo $seller_details->email; endif; ?>" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="control-label">Mobile Number</label>
                                              <input type="text" disabled value="<?php if(isset($seller_details->mobile)): echo $seller_details->mobile; endif; ?>" class="form-control">
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="control-label">Vendor Id Statuss</label>
                                              <input type="text" disabled placeholder="<?php if($seller_details->approval_status==0): echo "Not Approved"; else: echo "Approved"; endif; ?>" class="form-control"> 
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="control-label">Shipping Model</label>
                                              <input type="text" disabled value="<?php if($seller_details->shipping_model=='f'): echo "Fabpik"; else: echo "Self"; endif; ?>" class="form-control">
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                            @php $categories = \App\Models\Category::whereIn('id', explode(',',$seller_details->categories))->pluck('title'); @endphp
                                              <label class="control-label">Category</label>
                                              <input type="text" value="<?php if($categories): echo implode(', ',$categories->toArray()); endif; ?>" disabled class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div>
                                                    <label class="control-label">Company Name</label>   
                                                    <span class="required" aria-required="true">*</span> 
                                                        <input type="text" class="form-control" id="shop_name" name="shop_name" value="{{old('shop_name', isset($seller_details->company_name)?$seller_details->company_name:'' )}}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">                                                  
                                              <label class="control-label">Shop URL</label>
                                              <span class="required" aria-required="true">*</span>                                             
                                                  <input type="text" class="form-control" id="shop_url" readonly name="shop_url" value="{{old('shop_url',  isset($seller_details->url_slug)?$seller_details->url_slug:'')}}"><small>
                                                  e.g.{{ env('APP_URL') }}shop/<span id="showShopUrl"> YOUR_SHOP_URL</span></small>                                                  
                                            </div>
                                        </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-md-6">
                                              <div>
                                                  <label class="control-label">Brand Name</label>
                                                  <input type="text" readonly name="brand_name" id="brand_name" value="{{ old('brand_name', isset($seller_details->brand_name)?$seller_details->brand_name:'')}}" class="form-control" data-toggle="tooltip" title=""> 
                                                  
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div>
                                                  <label class="control-label">Store Name</label>
                                                  <input type="text" readonly name="store_name" id="store_name" value="{{ old('store_name', isset($seller_details->store_name)?$seller_details->store_name:'')}}" class="form-control" data-toggle="tooltip" title=""> 
                                                  
                                              </div>
                                          </div>
                                      </div>
                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label class="control-label">Do you want to enable queries section with users?<br>
                                                  <small><strong>Note: </strong>If you enable this, users can send their queries through the application.</small></label>
                                                  <input type="text" value="{{($seller_details->chat_enable==1)?'Yes':'No'}}" disabled class="form-control">
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                          </div>
                          <div id="tab_3-3" class="tab-pane">
                            <div class="form-body">
                              <div class="margin-bottom-5">
                                <h4 class="text-primary"><strong>Company Details</strong></h4>
                              </div>
                              <hr>
                              <div class="row">
                                <div class="col-md-8">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Name of Business / Company</label>
                                          <input type="text" disabled value="<?php if(isset($seller_details->company_name)): echo $seller_details->company_name; endif; ?>" class="form-control"> 
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label class="control-label">Company registration number</label>
                                          <input type="text" disabled value="<?php if(isset($seller_details->company_reg_no)): echo $seller_details->company_reg_no; endif; ?>" class="form-control">
                                        </div>
                                      </div>
                                  </div>
                                  <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Legal Entity</label> 
                                                  <input type="text" value="<?php if($seller_details->legal_entity=='r'): echo "Registered"; else: echo "Not Rregistered"; endif; ?>" disabled class="form-control"> 
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Type of Company</label>
                                                    <input type="text" value="<?php echo $seller_details->company_type; ?>" disabled class="form-control">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">GST Registered</label>
                                                  <input type="text" value="<?php
                                                          if($seller_details->gst_registered==1){
                                                            echo "Yes";
                                                          }else{
                                                            echo "No";
                                                          }
                                                          ?>" disabled class="form-control">
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">GST Number</label>
                                                  <input type="text" disabled value="<?php if(isset($seller_details->gst_no)){ echo $seller_details->gst_no; } ?>" class="form-control">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Office Phone Number</label>
                                                  <input type="text" disabled value="<?php if(isset($seller_details->office_phone_no)){ echo $seller_details->office_phone_no; } ?>" class="form-control">
                                              </div>
                                              <div class="form-group">
                                                @php $states = \App\Models\State::find($seller_details->company_state);
                                                @endphp
                                                  <label class="control-label">State</label>
                                                  <input type="text" value="<?php if($states): echo $states->name; endif; ?>" disabled class="form-control">
                                              </div>
                                              <div class="form-group">
                                                  <label class="control-label">Pincode</label>
                                                  <input type="text" value="<?php if(isset($seller_details->company_pincode)): echo $seller_details->company_pincode; endif; ?>" disabled class="form-control">
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Address</label>
                                                  <textarea class="form-control" disabled rows="9"><?php if(isset($seller_details->company_address)): echo $seller_details->company_address; endif; ?></textarea>
                                              </div>
                                          </div>
                                      </div>
                                </div>
                                <div class="col-md-4">
                                      <div class="form-group">
                                        <label class="control-label">GST Certififcate</label>
                                        @php $ext = pathinfo($seller_details->gst_certificate, PATHINFO_EXTENSION); @endphp
                                        @if($ext == 'pdf')
                                        <br/>
                                            <a href = "<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/gst_certificate/'.$seller_details->gst_certificate; ?>" target="_blank">{{isset($seller_details->gst_certificate)?'View GST Certificate':''}}</a>
                                        @else
                                          <img src="<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/gst_certificate/'.$seller_details->gst_certificate; ?>" width="200px" height="200px">
                                        @endif
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label">Incorporation Certificate</label>
                                        @php $ext = pathinfo($seller_details->gst_certificate, PATHINFO_EXTENSION); @endphp
                                        @if($ext == 'pdf')
                                        <br/>
                                            <a href = "<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/incorporation_certificate/'.$seller_details->incorporation_certificate; ?>" target="_blank">{{isset($seller_details->incorporation_certificate)?'View Incorporation Certificate':''}}</a>
                                        @else
                                          <img src="<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/incorporation_certificate/'.$seller_details->incorporation_certificate; ?>" width="200px" height="200px">
                                        @endif
                                          
                                      </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div id="tab_4-4" class="tab-pane">
                            <div class="row">
                              <?php if(!empty($seller_warehouse_details)){ 
                                  foreach($seller_warehouse_details as $seller_warehouse){
                              ?>
                              
                              <div class="col-md-6">
                                  <div class="portlet light bordered">
                                      <div class="portlet-title">
                                          <div class="caption"> <i class="icon-map font-red-sunglo"></i> <span class="caption-subject font-red-sunglo bold uppercase"><?php echo $seller_warehouse->name; if($seller_warehouse->is_default==1): echo " (Default)"; endif; ?></span> </div>
                                      </div>
                                      <div class="portlet-body">
                                          <?php echo $seller_warehouse->phone.','; ?><br> 

                                          <p> <?php echo $seller_warehouse->address.','; ?><br> 
                                          <?php echo $seller_warehouse->address_two.','; ?> 
                                          <?php echo $seller_warehouse->landmark.','; ?> 
                                          <?php echo $seller_warehouse->city.','; ?> 
                                          <?php echo $seller_warehouse->state; ?> <br> 
                                          <?php echo $seller_warehouse->pincode; ?>. </p>
                                          <p><b>Shiprocket Pickup Name:</b> {{( !is_null($seller_warehouse->shiprocket_pickup_nickname)? $seller_warehouse->shiprocket_pickup_nickname : '-' ) }}</p>
                                      </div>
                                  </div>
                              </div>

                              <?php } } ?>
                              </div>
                          </div>
                          <div id="tab_5-5" class="tab-pane">
                                  <div class="form-body">
                                    <div class="margin-bottom-5">
                                            <h4 class="text-primary"><strong>Bank Details</strong></h4>
                                        </div>
                                        <hr>
                                      <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Name of Account Holder</label>
                                                  <input type="text" disabled value="<?php if(isset($seller_details->bank_account_name)): echo $seller_details->bank_account_name; endif; ?>" class="form-control">
                                              </div>
                                          </div>
                                          <!--/span-->
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Bank Name</label>
                                                  <input type="text" disabled value="<?php if(isset($seller_details->bank_name)): echo $seller_details->bank_name; endif; ?>" class="form-control">
                                              </div>
                                          </div>
                                          <!--/span-->
                                      </div>
                                      <!--/row-->
                                      <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Account Number</label>
                                                  <input type="text" disabled value="<?php if(isset($seller_details->bank_account_no)): echo $seller_details->bank_account_no; endif; ?>" class="form-control">
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">IFSC Code</label>
                                                  <input type="text" disabled value="<?php if(isset($seller_details->ifsc_code)): echo $seller_details->ifsc_code; endif; ?>" class="form-control">
                                              </div>
                                          </div>
                                          <!--/span-->
                                      </div>
                                      <!--/row-->
                                      <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Swift Code</label>
                                                  <input type="text" disabled value="<?php if(isset($seller_details->swift_code)): echo $seller_details->swift_code; endif; ?>" class="form-control">
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Branch Name</label>
                                                  <input type="text" disabled value="<?php if(isset($seller_details->branch_name)): echo $seller_details->branch_name; endif; ?>" class="form-control">
                                              </div>
                                          </div>
                                      </div>
                                      <!--/row-->
                                      <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Upload Cancelled Cheque</label>
                                                  @php $ext = pathinfo($seller_details->cancelled_cheque_file, PATHINFO_EXTENSION); @endphp
                                                  {{$ext}}
                                                  @if($ext == 'pdf')
                                                  <br/>
                                                      <a href = "<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/cancelled_cheque_files/'.$seller_details->cancelled_cheque_file; ?>" target="_blank">{{isset($seller_details->cancelled_cheque_file)?'View Cancelled Certificate':''}}</a>
                                                  @else
                                                      <img src="<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/cancelled_cheque_files/'.$seller_details->cancelled_cheque_file; ?>" width="200px" height="200px">
                                                  @endif
                                                  
                                              </div>
                                          </div>
                                      </div>
                                      <!--/row-->
                                  </div>
                          </div>
                          <div id="tab_6-6" class="tab-pane">
                            <div class="margin-bottom-5">
                                <h4 class="text-primary"><strong>POC Details</strong></h4>
                            </div>
                            <hr>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Individual Representing Company</label>
                                  <input type="text" disabled value="<?php if(isset($seller_details->poc_name)): echo $seller_details->poc_name; endif; ?>" class="form-control">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Designation of the Individual</label>
                                  <input type="text" disabled value="<?php if(isset($seller_details->poc_designation)): echo $seller_details->poc_designation; endif; ?>" class="form-control">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Individual Email Id</label>
                                  <input type="text" disabled value="<?php if(isset($seller_details->poc_email)): echo $seller_details->poc_email; endif; ?>" class="form-control">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Phone Number</label>
                                  <input type="text" disabled value="<?php if(isset($seller_details->poc_mobile)): echo $seller_details->poc_mobile; endif; ?>" class="form-control">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Alternate Phone Number</label>
                                  <input type="text" value="<?php if(isset($seller_details->poc_phone)): echo $seller_details->poc_phone; endif; ?>" disabled class="form-control">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div id="tab_7-7" class="tab-pane">
                              <div class="form-body">
                                      <div class="margin-bottom-5">
                                          <h4 class="text-primary"><strong>Commission</strong></h4>
                                      </div>
                                      <hr>
                                      <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Commission Rate (%)</label>
                                                  <span class="required" aria-required="true">*</span>
                                                  <input type="number" id="commission_value" readonly required name="commission_value" value="<?php if(isset($seller_details->commission)): echo $seller_details->commission; endif; ?>" class="form-control">
                                                  <span class="help-block commission-value-msg"></span>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Commission Model</label>
                                                  <span class="required" aria-required="true">*</span>
                                                  @php $commissionModels = App\Models\CommissionModel::where('status', 1)->whereNull('deleted_at')->get() @endphp
                                                  <select class="form-control" disabled required name="commission_type" id="commission_type">
                                                      <option value="">Select</option>                                                         
                                                            
                                                      @foreach($commissionModels as $model)
                                                          <option value="{{$model->id}}" <?php if($seller_details->commission_id == $model->id): echo "selected = 'selected'"; endif; ?> >{{$model->name}}</option>
                                                      @endforeach                
                                                  </select>
                                                  <span class="help-block commission-type-msg"></span>
                                              </div>
                                          </div>
                                          <span><?php if(isset($seller_details->commission_accept)): echo (($seller_details->commission_accept == 1)?'<b>Seller Accepted Current Commission Rate</b>':'<b>Seller Acceptence is pending</b>') ; endif; ?></span>

                                      </div>                                    
                              </div>
                          </div>
                          <div id="tab_cod" class="tab-pane">
                              <div class="form-body">
                                  <div class="margin-bottom-5">
                                      <h4 class="text-primary"><strong>Cash On Delivery</strong></h4>
                                  </div>
                                  <hr>
                               
                                  <div class="row">
                                      <div class="col-md-6">
                                          <div class="form-group">
                                              <label class="control-label">Cash On Delivery for products :<b>{{($seller_details->cod_enable=='y')?'YES':'NO'}} <b></label>
                                          </div>
                                      </div>
                                  </div>                                                             
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
              </div>
              <!--end col-md-9-->
          </div>
      </div>
      <!--end tab-pane-->
    </div>

@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Sellers = function() {
  return {
    //main function to initiate the module
    init: function() {
      $('#approveBtn').click(function() {

        var seller_id = $(this).data('sellerid');
        var csrfToken = "{{ csrf_token() }}";
        var input = {
            "id" : seller_id,
            "_token" : csrfToken
        };
        swal({
            title: 'Are you sure?',
            text: 'You want to Approve this seller.',
            type: 'info',
            showCancelButton: true,
            confirmButtonClass: 'btn-success',
            cancelButtonClass: 'btn-danger',
            closeOnConfirm: false,
            closeOnCancel: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
        },
        function(isConfirm){
            if (isConfirm){
                $.ajax({
                    url : '{{route("admin.sellers.approveSeller")}}',
                    type : 'POST',
                    data : input,
                    success : function(response) {
                        swal('Seller', 'Seller Approved!', "success");
                        location.reload();
                    }
                });
            }
            else {
                swal("Cancelled", "", "error");
            }
        });
      });
      
      $('#rejectBtn').click(function() {

        var seller_id = $(this).data('sellerid');
        var csrfToken = "{{ csrf_token() }}";
        var input = {
            "id" : seller_id,
            "_token" : csrfToken
        };
        swal({
            title: 'Are you sure?',
            text: 'You want to Reject this seller.',
            type: 'info',
            showCancelButton: true,
            confirmButtonClass: 'btn-success',
            cancelButtonClass: 'btn-danger',
            closeOnConfirm: false,
            closeOnCancel: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
        },
        function(isConfirm){
            if (isConfirm){
                $.ajax({
                    url : '{{route("admin.sellers.rejectSeller")}}',
                    type : 'POST',
                    data : input,
                    success : function(response) {
                        swal('Seller', 'Seller Rejected!', "success");
                        location.reload();
                    }
                });
            }
            else {
                swal("Cancelled", "", "error");
            }
        });
      });
    }
  };
}();

jQuery(document).ready(function() {
    Sellers.init();
});
</script>
@endpush
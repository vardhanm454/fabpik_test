@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
    <link href="{{ __common_asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' )}}"
rel="stylesheet" type="text/css" />

@endpush

@section('content')
{{--$dt_ajax_url--}}
<style>
.modal {
    text-align: center;
    padding: 0 !important;
}

.modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
}

.modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
}
.modal-body{
    max-height: 400px!important;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title bg-info pd-15">
                <div class="filter-actions">
                
                        <div class="row">  
                            <div class="col-md-3">
                                <label class=""><strong>Date:</strong> </label>
                                <div class="">
                                    <div class="input-group  input-daterange" data-date="10/11/2012"
                                        data-date-format="mm/dd/yyyy">
                                        <input type="text" class="form-control date-picker input-sm" name="ffromdate"
                                            id="ffromdate" placeholder="From" value="{{request()->ffromdate}}">
                                        <span class="input-group-addon"> - </span>
                                        <input type="text" class="form-control date-picker input-sm" name="ftodate" id="ftodate"
                                            placeholder="To" value="{{request()->ftodate}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class=""><strong>Approval Status?</strong> </label>
                                <div class="">
                                    <select name="fapproval" id="fapproval"
                                        class="form-control form-filter input-sm margin-bottom-5">
                                        <option value="">Select</option>
                                        <option value='null' {{request()->fapproval=='null'?'selected="selected"':''}}>pending</option>
                                        <option value="1" {{request()->fapproval=='1'?'selected="selected"':''}}>Approved</option>
                                        <option value="0" {{request()->fapproval=='0'?'selected="selected"':''}}>Not Approved</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class=""><strong>Account Status:</strong> </label><br>
                                <div class="">
                                    <select name="fstatus" id="fstatus" class="form-control input-sm">
                                        <option value="">All</option>
                                        <option value="1" {{request()->fstatus=='1'?'selected="selected"':''}}>Active
                                        </option>
                                        <option value="0" {{request()->fstatus=='0'?'selected="selected"':''}}>Inactive
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class=""><strong>Primary Category:</strong> </label><br>
                                <div class="">
                                    <select name="fcategory" id="fcategory"
                                        class="form-control select2 form-filter input-sm margin-bottom-5">
                                        <option value="">Primary Category:</option>
                                        @if(isset($categories))
                                        @foreach($categories as $category)
                                          <option value="{{$category->id}}" {{request()->fcategory==$category->id?'selected="selected"':''}}>{{$category->title}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class=""><strong>Name:</strong> </label>
                                <div class="">
                                    <input type="text" class="form-control input-sm" name="fname" id="fname"  value="{{request()->fname?request()->fname:''}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class=""><strong>Company Name:</strong> </label>
                                <div class="">
                                    <input type="text" class="form-control input-sm" name="fcompanyname" id="fcompanyname"  value="{{request()->fcompanyname?request()->fcompanyname:''}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class=""><strong>Email:</strong> </label>
                                <div class="">
                                    <input type="text" class="form-control input-sm" name="femail" id="femail"  value="{{request()->femail?request()->femail:''}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class=""><strong>mobile:</strong> </label><br>
                                <div class="">
                                    <input type="text" class="form-control input-sm" name="fmobile" id="fmobile"  value="{{request()->fmobile?request()->fmobile:''}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class=""><strong>Seller ID:</strong> </label><br>
                                <div class="">
                                    <input type="text" class="form-control input-sm" name="fsellercode" id="fsellercode"  value="{{request()->fsellercode?request()->fsellercode:''}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class=""><strong>Commission Model:</strong> </label><br>
                                <div class="">
                                    <select name="fcommission" id="fcommission" class="form-control select2 form-filter input-sm margin-bottom-5" data-allow-clear="true">
                                        <option value="">Select:</option>
                                        @if(isset($commissionModels))
                                        @foreach($commissionModels as $model)
                                          <option value="{{$model->id}}" {{request()->fcommission==$model->id?'selected="selected"':''}}>{{$model->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-3">
                                <label class=""><strong>Action:</strong> </label><br>
                                <div class="">
                                    <button type="button" class="btn btn-sm btn-icon-only white" name="search"
                                        id="btn_submit_search" value="search"
                                        data-loading-text="<i class='fa fa-spinner fa-spin'></i>"
                                        title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span>
                                    </button>
                                    <a class="btn btn-sm btn-icon-only white" href="{{route('admin.sellers')}}"
                                        title="Reset Filter"><span aria-hidden="true" class="icon-refresh"></span> </a>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-container">
                        <div class="table-actions-wrapper">
                            <!-- <div class="col-md-8 input-icon">
                                <i class="fa fa-search font-blue"></i>
                                <input type="text" class="form-control form-filter input-sm" name="order_date_from"
                                    placeholder="Search with ID / Name / Email ID / Phone No...">
                            </div> -->
                            <a href="{{route('admin.sellers.add')}}" class="btn btn-default btn-sm tooltips"
                                data-toggle="tooltip" data-placement="top"
                                data-original-title="Add Seller"><strong><span aria-hidden="true"
                                        class="icon-plus text-primary"></span>&nbsp;Add</strong></a>

                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-placement="top" data-original-title="Seller Active/In-Active" data-toggle="tooltip" data-action="change-status"><strong><span aria-hidden="true" id="btn-confirm" class="icon-trash text-danger"></span>&nbsp;Change Status</strong></a>
                        
                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-placement="top" data-original-title="Notify" data-toggle="tooltip" data-action="notify-customers"><strong><span aria-hidden="true" id="btn-confirm" class="icon-bell text-danger"></span>&nbsp;Notify</strong></a>


                        </div>
                        <table class="table dt-responsive table-checkable" id="dt_listing">
                            <thead>
                                <tr role="row" class="heading">
                                    <th>
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable"
                                                data-set="#sample_2 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th> Sr. No. </th>
                                    <th class=""> Seller ID </th>
                                    <th> Seller Name </th>
                                    <th> Company Name </th>
                                    <th class="none"> Registration Type: </th>
                                    <th class="none"> Commission Model: </th>
                                    <th> Primary Email ID </th>
                                    <th> Phone No. </th>
                                    <th class="none"> Primary Category: </th>
                                    <th class="none"> Registered Date: </th>
                                    <th class="none"> Approved Date: </th>
                                    <th> Approval Status </th>
                                    <th> Account Status </th>
                                    <th width="90"> Actions </th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="changeStatusModal" tabindex="-1" role="dialog" aria-labelledby="changeStatusModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changeStatusModalLabel">Change Activation Status!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="">
                    <label class="control-label">Change Status:</label>
                    <div class="">
                        <select class="form-control" name="status" id="status">
                            <option value="">Select</option>
                            @foreach(['1'=>'Active', '0'=>'Inactive'] as $val=>$label)
                            <option value="{{$val}}">
                                {{$label}}
                            </option>
                            @endforeach
                        </select>
                        <span style="color:#e73d4a" id="result"></span>
                    </div>
                    <div class="" id="reason_div" style="margin-top:10px;display:none">
                        <textarea class="form-control" name="inactive_reason" id="inactive_reason" placeholder="Enter reason for deactivation..."></textarea>
                        <span style="color:#e73d4a" id="reason-result"></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!--Notify Modal -->
<div class="modal fade" id="notifyModal" tabindex="-1" role="dialog" aria-labelledby="notifyModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="notifyModalLabel"><b>Notify</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="">
                    <form>
                        <div class="">
                            <label class="control-label">Title:<span class="required"> * </span></label>
                            <div class="">
                                <input type="text" class="form-control maxlength-handler" name="subject" id="subject"
                                    maxlength="100" placeholder="" value="">
                                <span style="color:#e73d4a" id="subject_result"></span>
                            
                            </div>
                        </div>
                        <div class="">
                            <label class="control-label">Notify On:<span class="required"> *
                                </span></label>
                            <div class="">
                                <input type="text" class="form-control date-picker1" name="notify_on" id="notify_on" data-date-start-date="0d"
                                    value=""
                                    readonly>
                                <!-- <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span> -->
                                <span style="color:#e73d4a" id="notify_result"></span>
                            </div>
                        </div>
                        <div class="">
                            <label class="control-label">Content:<span class="required"> *
                                </span></label>
                            <div class="">
                                <textarea class="form-control" name="content" id="content">
                                </textarea>
                                <span style="color:#e73d4a" id="content_result"></span>
                            </div>
                        </div>
                    <br>
                    <input type="checkbox" class="" style="display:block;float:left;margin-right:6px;" id="select_all" name="select_all" data-set="" value="1"/>
                    <label for="select_all" style="display:block;float:left;">Send to all Sellers</label><br>
                    <form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes1">Submit</button>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
    type="text/javascript">
</script>
<script src="{{ __common_asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
    type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Seller = function() {

    $('#notifyModal').on('hidden.bs.modal', function () {
        $('#notifyModal form')[0].reset();
    });

    return {

        //main function to initiate the module
        init: function() {
            
            //By Click enter search will happen
            $(document).on("keypress", "input", function(e) {
                if (e.which == 13) {
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

         if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }

        if (jQuery().datetimepicker) {
            var start_date = new Date(); 
            
            $('.date-picker1').datetimepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                startDate: start_date,
                format: 'dd-mm-yyyy hh:ii',
                locale: 'en'
            });
            $('.date-picker1').css("z-index","10061");
        }


        //Based on the status text box will appear
        $("#status").change(function () {
            // console.log(this.value);
            var status = $("#status option:selected").val();
            if(status == 0){
                $("#reason_div").css("display", "block");
            }
        });
            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.sellers')}}?search=1";

                if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());
                if($('#fapproval').val() != '') url += "&fapproval="+encodeURIComponent($('#fapproval').val());
                if($('#fcategory').val() != '') url += "&fcategory="+encodeURIComponent($('#fcategory').val());
                if($('#ffromdate').val() != '') url += "&ffromdate="+encodeURIComponent($('#ffromdate').val());
                if($('#ftodate').val() != '') url += "&ftodate="+encodeURIComponent($('#ftodate').val());
                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                if($('#fmobile').val() != '') url += "&fmobile="+encodeURIComponent($('#fmobile').val());
                if($('#femail').val() != '') url += "&femail="+encodeURIComponent($('#femail').val());
                if($('#fcompanyname').val() != '') url += "&fcompanyname="+encodeURIComponent($('#fcompanyname').val());
                if($('#fsellercode').val() != '') url += "&fsellercode="+encodeURIComponent($('#fsellercode').val());
                if($('#fcommission').val() != '') url += "&fcommission="+encodeURIComponent($('#fcommission').val());
            
                window.location.href = url;
            });

        }

    };

}();

jQuery(document).ready(function() {
    Seller.init();
});
</script>
@endpush
@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content')
<style>
    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        width: 360px;
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    .modal-body {
        position: relative;
        overflow-y: auto;
        max-height: 400px;
        padding: 15px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                    
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="">Date: </label><br>
                                    <div class="">
                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control datepicker input-sm" name="ffromdate"
                                                id="ffromdate" placeholder="From" value="{{request()->ffromdate}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control input-sm" name="ftodate" id="ftodate"
                                                placeholder="To" value="{{request()->ftodate}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="">Delivery Status: </label><br>
                                    <div class="">
                                        <select name="fsstatus" id="fsstatus" class="form-control select2 form-filter input-sm margin-bottom-5" data-allow-clear="true">
                                            <option value="">Select</option>
                                            @if(isset($shippingStatus))
                                            @foreach($shippingStatus as $status)
                                            <option value="{{$status->id}}"
                                                {{request()->fsstatus==$status->id?'selected="selected"':''}}>
                                                {{$status->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="">Seller: </label><br>
                                    <div class="">
                                        <select name="fseller" id="fseller"
                                            class="form-control select2 form-filter input-sm margin-bottom-5">
                                            <option value="">Select</option>
                                            @if(isset($sellers))
                                            @foreach($sellers as $seller)
                                            <option value="{{$seller->id}}"
                                                {{request()->fseller==$seller->id?'selected="selected"':''}}>
                                                {{$seller->company_name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40">
                                <div class="col-md-4">
                                    <label class="">Tracking/AWB number : </label><br>
                                    <div class="">
                                        <input type="text" class="form-control input-sm" name="fnumber" id="fnumber"
                                            value="{{request()->fnumber}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="">Product Name: </label><br>
                                    <div class="">
                                        <input type="text" name="fproductname" id="fproductname" value="{{request()->fproductname}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>                                    
                                </div>
                                <div class="col-md-4">
                                    <label class="">Product SKU: </label><br>
                                    <div class="">
                                        <input type="text" name="fproductsku" id="fproductsku" value="{{request()->fproductsku}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="">Pincode: </label><br>
                                    <div class="">
                                        <input type="text" name="fpincode" id="fpincode" value="{{request()->fpincode}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="">Excepted Delivery Date: </label><br>
                                    <div class="">
                                        <div class="date-picker input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control form-filter input-sm margin-bottom-5 datepicker" name="fexcepteddeliverydate" id="fexcepteddeliverydate" placeholder="Excepted Delivery Date" value="{{request()->fexcepteddeliverydate}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="">Delivered Date: </label><br>
                                    <div class="">
                                        <div class="date-picker input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control form-filter input-sm margin-bottom-5 datepicker" name="fdelivereddate" id="fdelivereddate" placeholder="Deliverd Date" value="{{request()->fdelivereddate}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="">Order ID:</label>
                                        
                                            <input type="text" class="form-control input-sm" name="fcorderid" id="fcorderid"
                                                value="{{request()->fcorderid}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-2">
                                    <label class="">Action: </label>
                                    <div class="">
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search"
                                            id="btn_submit_search" value="search"
                                            data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('admin.shipping')}}"
                                            title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                    <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-placement="top" data-original-title="Change Shipping Status" data-toggle="tooltip" data-action="change-status">
                            <strong><span aria-hidden="true" id="btn-confirm" class="icon-settings text-primary"></span>&nbsp;Take Action</strong></a>
                        <!-- <a href="javascript:;" id="btn_table_export" class="btn btn-default btn-sm tooltips"
                            data-container="body" data-placement="top" data-original-title="Export to Excel"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a> -->
                    </div>
                    <table class="table table-bordered table-hover dt-responsive dataTable no-footer dtr-inline collapsed"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th class="text-center" style="width:7%;"> Date </th>
                                <th class="text-center" style="width:7%;"> Child Order ID </th>
                                <th class="none"> Seller ID: </th>
                                <th class="none"> Seller Name</th>
                                <th class="none"> User Name</th>
                                <th class="text-center" style="width:15%;"> Product Name </th>
                                <th class="none"> Product ID: </th>
                                <th class="text-center" style="width:7%;"> SKU</th>
                                <th class="text-center" style="width:7%;"> Quantity</th>
                                <th class="text-center" style="width:7%;"> Weight <br/><small>(Per Unit<br/>In Grams)</small></th>
                                <th class="text-center" style="width:7%;"> L*B*H <br/>(Per Unit <br/>cm<sup>3</sup>)</th>
                                <th class="none"> Pincode: </th>
                                <th class="none"> Tracking ID: </th>
                                <th class="none"> Order Status: </th>
                                <th class="none"> Expected Delivery Date: </th>
                                <th class="none"> Expected Package Date: </th>
                                <th class="none"> Delivered Date: </th>
                                <th class="text-center" style="width:13%;"> Shipping <br/>Status</th>
                                <th class="text-center" style="width:8%;"> Actions </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="changeStatusModal" tabindex="-1" role="dialog" aria-labelledby="changeStatusModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changeStatusModalLabel">Change Shipping Status! 
                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </h5>
                
            </div>
            <div class="modal-body">
                <div class="">
                    <label class="control-label">Change Status:</label>
                    <div class="">
                        <select class="form-control" name="status" id="status">
                            <option value="">Select</option>
                            @foreach(['10'=>'Return Pickup Initiated' ] as $val=>$label)
                            <option value="{{$val}}"> {{$label}} </option>
                            @endforeach
                        </select>
                        <span style="color:#e73d4a" id="result"></span>
                    </div>
                </div>

                <div class="">
                    <label class="control-label">Remark</label>
                    <div class="">
                        <textarea class="form-control" name="remark" id="remark" rows="3"></textarea>
                        <span style="color:#e73d4a" id="remark-result"></span>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modify AWB Modal -->
<div class="modal fade" id="modifyAWBModal" tabindex="-1" role="dialog" aria-labelledby="modifyAWBModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modifyAWBModalLabel">AWB Details 
                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </h5>
                
            </div>
            <div class="modal-body">
                
                <input type="hidden" id="awb_child_order_id" name="awb_child_order_id">
                <label class="control-label">Current AWB Number: <span id="current_awb_number"> </span></label>
                <br>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="awb_number" class="col-form-label">AWB Number: <span class="required"> *</span></label>
                            <input type="text" class="form-control input-sm" id="awb_number" name="awb_number" autocomplete="off" readonly>
                            <span style="color:#e73d4a" id="awb_number_result"></span>
                        </div>
                    </div>
                </div>
                
                <button type="button" class="btn blue" name="editAWBDetails" id="editAWBDetails">Edit AWB Details</button>  
                <br>
                <br>
                <span style="color:#295633" id="awb_number_result_success"></span>
                <span style="color:#e73d4a" id="awb_number_result_failure"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modify_awb_save_btn" disabled>Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- Modify AWB Modal -->

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
    type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Orders = function() {

    return {

        //main function to initiate the module
        init: function() {

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }

            //Filters will apply when click the enter
            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });
            
            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.shipping')}}?search=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fsstatus').val() != '') url += "&fsstatus=" + encodeURIComponent($('#fsstatus').val());
                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fnumber').val() != '') url += "&fnumber=" + encodeURIComponent($('#fnumber').val());
                if ($('#fproductname').val() != '') url += "&fproductname=" + encodeURIComponent($('#fproductname').val());
                if ($('#fproductsku').val() != '') url += "&fproductsku=" + encodeURIComponent($('#fproductsku').val());
                if ($('#fpincode').val() != '') url += "&fpincode=" + encodeURIComponent($('#fpincode').val());
                if ($('#fexcepteddeliverydate').val() != '') url += "&fexcepteddeliverydate=" + encodeURIComponent($('#fexcepteddeliverydate').val());
                if ($('#fdelivereddate').val() != '') url += "&fdelivereddate=" + encodeURIComponent($('#fdelivereddate').val());
                if ($('#fcorderid').val() != '') url += "&fcorderid=" + encodeURIComponent($('#fcorderid').val());

                window.location.href = url;
            });

            //AWB Details Update
            //get the current AWB Details of the order
            $("#modifyAWBModal").on("show.bs.modal", function(e) {
                //load the  current values
                $("#awb_child_order_id").val($(e.relatedTarget).data('awb_child_order_id'));
                $("#current_awb_number").text($(e.relatedTarget).data('current_awb_number'));
                $("#awb_number").val($(e.relatedTarget).data('current_awb_number'));

                if($(e.relatedTarget).data('current_awb_number') == ''){
                    $("#editAWBDetails").attr("disabled", true);
                }else{
                    $("#editAWBDetails").removeAttr("disabled");
                }
            });

            $('#editAWBDetails').on('click',function(){
                $("#awb_number").attr("readonly", false);
                $("#modify_awb_save_btn").removeAttr("disabled");
            });

            $('#modify_awb_save_btn').on('click',function(){

                var awb_number = $("#awb_number").val();
                var current_awb_number = $("#current_awb_number").text();

                var orderId = $("#awb_child_order_id").val();

                $('#awb_number_result_success').html('');
                $('#awb_number_result_failure').html('');

                if(awb_number.trim() == ''){
                    $('#awb_number').focus();
                    $("#awb_number_result").html("This filed is required.");
                    return false;
                }else if(awb_number == current_awb_number){
                    $('#awb_number').focus();
                    $("#awb_number_result").html("Update AWB Number.");
                }else{
                    var url = "{{route('admin.shipping.modifyAWBNumber',['id'=>':orderId'])}}";
                    url = url.replace(':orderId', orderId);
                    
                    var arr = { 
                        awb_number: awb_number,                   
                        order_id: orderId,                        
                    };

                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#modify_awb_save_btn').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                           console.log(result); 
                           if(result.success){
                                $('#awb_number_result_success').html(result.message);
                                
                           }else{
                                $('#awb_number_result_failure').html(result.message);
                           }
                           $('#modify_awb_save_btn').removeAttr("disabled");
                           $('.modal-body').css('opacity', '');
                        }, 
                        error:function(err){
                            // alert("error"+JSON.stringify(err));
                            console.log(err.responseJSON);
                            $('#modify_awb_save_btn').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');
                            $("#awb_number_result_failure").html("Something went wrong, please try again after sometime");
                        
                        }
                    });
                }
            }); 

            $('#modifyAWBModal').on('hidden.bs.modal', function (e) {
                
                $("#awb_child_order_id").val();
                $("#current_awb_number").text();
                $("#awb_number").val();
                $("#modify_awb_save_btn").attr("disabled", true);
                $('#awb_number_result_success').html('');
                $('#awb_number_result_failure').html('');
                $('#awb_number_result').html('');
                $('.modal-body').css('opacity', '');
                
            })
        }

    };
}();

jQuery(document).ready(function() {
    Orders.init();
});
</script>
@endpush
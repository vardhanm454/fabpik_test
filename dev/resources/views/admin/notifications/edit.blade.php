@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />

    <style>
    .select2-results__option {
        padding: 0px 4px !important;
    }
    .select2-result-repository__title{
        margin-bottom: 0px !important;
    }
    .select2-result-repository{
        padding-top: 3px !important;
        padding-bottom: 3px !important;
    }

    .bootstrap-tagsinput{
    	width: 100%;
    }
    .bar{
        font-size: 18px !important;
        font-weight: 500 !important;
    }
    
    </style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    <form class="form-horizontal" action="{{route('admin.notifications.edit',['id'=>$id])}}" method="post"
                        enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group {{($errors->first('title'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Title:<span class="required"> *
                                                </span></label>
                                            <div class="col-md-8">
                                                    <input type="text" class="form-control" name="title" id="title"
                                                    maxlength="100" placeholder="" value="{{old('title', isset($editnotifications)?$editnotifications->title:'')}}">
                                                <span class="help-block">{{$errors->first('title')}}</span>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group {{($errors->first('notify_on'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Notify On:<span class="required"> *
                                                </span>
                                            </label>
                                            <div class="col-md-8">
                                            <input type="text" class="form-control date-picker1" name="notify_on" id="notify_on" data-date-start-date="0d"
                                                value="{{old('notify_on', isset($editnotifications)?date('d-m-Y H:i:s',strtotime($editnotifications->notify_on)):'')}}" readonly>
                                                <span class="help-block">{{$errors->first('notify_on')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{($errors->first('content'))?'has-error':''}}">
                                            <label class="col-md-4 control-label">Content:<span class="required"> *
                                                </span></label>
                                            <div class="col-md-8">
                                            <textarea class="form-control" name="content" id="content" value="{{old('content', isset($editnotifications)?$editnotifications->content:'')}}">
                                            {{{ old('content', isset($editnotifications)?$editnotifications->content:'') }}}
                                            </textarea> 
                                                <span class="help-block">{{$errors->first('content')}}</span>
                                            </div>
                                        </div>                                       
                                    </div>                                        
                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                                <button type="submit" class="btn blue form-submit" name="save" value="save"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                @if(isset($editnotifications))
                                <button type="submit" class="btn blue form-submit" name="save" value="savecont"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><i
                                        class="fa fa-check-circle"></i> Save &amp; Continue Edit</button>
                                @endif
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
    type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Notifications = function() {

    return {

        //main function to initiate the module
        init: function() {
            //Multitag inputes
           

            // Data Picker Initialization
            if (jQuery().datetimepicker) {
                $('.date-picker1').datetimepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true,
                    format: 'dd-mm-yyyy hh:ii:ss',
                    locale: 'en'
                });
            }

            $.fn.select2.defaults.set("theme", "bootstrap");

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.notifications')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });            

        }

    };

}();

jQuery(document).ready(function() {
    Notifications.init();
});
</script>
@endpush
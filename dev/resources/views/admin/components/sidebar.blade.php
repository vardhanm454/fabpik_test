<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            <!-- <li class="heading">
                <h3 class="uppercase sbold">Navigation</h3>
            </li> -->
            <li class="nav-item  @if($activeMenu=='dashboard') active @endif">
                <a href="{{route('admin.dashboard')}}" class="nav-link ">
                    <i class="icon-grid"></i>
                    <span class="title">Dashboard</span>
                    @if($activeMenu=='dashboard') <span class="selected"></span> @endif
                </a>
            </li>
            <li class="nav-item   ">
                <a href="{{route('admin.home')}}" class="nav-link ">
                    <i class="icon-screen-smartphone"></i>
                    <span class="title">Mobile App</span>
                 
                </a>
            </li>
            <li class="nav-item   ">
                <a href="{{route('admin.tax')}}" class="nav-link ">
                    <i class="icon-screen-smartphone"></i>
                    <span class="title">Tax %</span>
                 
                </a>
            </li>
            <li class="nav-item @if($activeMenu=='prod-mgmt') active open @endif">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bag"></i>
                    <span class="title">Product Management</span>
                    @if($activeMenu=='prod-mgmt')
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    @else
                    <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item @if($activeSubmenu=='brands') active @endif">
                        <a href="{{route('admin.brands')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Brands</span>
                            @if($activeSubmenu=='brands') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='categories') active @endif">
                        <a href="{{route('admin.categories')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Categories</span>
                            @if($activeSubmenu=='categories') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='subcategories') active @endif">
                        <a href="{{route('admin.subcategories')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Sub Categories</span>
                            @if($activeSubmenu=='subcategories') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='childcategories') active @endif">
                        <a href="{{route('admin.childcategories')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Child Categories</span>
                            @if($activeSubmenu=='childcategories') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='attributes') active @endif">
                        <a href="{{route('admin.attributes')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Attributes</span>
                            @if($activeSubmenu=='attributes') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='sizecharts') active @endif">
                        <a href="{{route('admin.sizecharts')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Size Charts</span>
                            @if($activeSubmenu=='sizecharts') <span class="selected"></span> @endif
                        </a>
                    </li>

                    <li class="nav-item @if($activeSubmenu=='stylelayout') active @endif">
                        <a href="{{route('admin.stylelayout')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Style Layout</span>
                            @if($activeSubmenu=='stylelayout') <span class="selected"></span> @endif
                        </a>
                    </li>

                    <li class="nav-item @if($activeSubmenu=='layoutcolumns') active @endif">
                        <a href="{{route('admin.layoutcolumns')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Style Layout Columns</span>
                            @if($activeSubmenu=='layoutcolumns') <span class="selected"></span> @endif
                        </a>
                    </li>

                    @can('ADM_PRD_V_PRODUCT')
                    <li class="nav-item @if($activeSubmenu=='products') active @endif">
                        <a href="{{route('admin.products')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Product</span>
                            @if($activeSubmenu=='products') <span class="selected"></span> @endif
                        </a>
                    </li>
                    @endcan
                    @can('ADM_PRD_C_PRODUCT')
                    <li class="nav-item @if($activeSubmenu=='importproduct') active @endif">
                        <a href="{{route('admin.products.import')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Import Product</span>
                            @if($activeSubmenu=='importproducts') <span class="selected"></span> @endif
                        </a>
                    </li>
                    @endcan
                    @can('ADM_PRD_U_PRODUCT')
                    <li class="nav-item @if($activeSubmenu=='updateproduct') active @endif">
                        <a href="{{route('admin.products.update')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Update Products</span>
                            @if($activeSubmenu=='updateproducts') <span class="selected"></span> @endif
                        </a>
                    </li>
                    @endcan
                    @can('ADM_PRD_VRT_PRICE')
                    <li class="nav-item @if($activeSubmenu=='specialprices') active @endif">
                        <a href="{{route('admin.specialprices')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Special Prices</span>
                            @if($activeSubmenu=='specialprices') <span class="selected"></span> @endif
                        </a>
                    </li>
                    @endcan
                    @can('ADM_PRD_A_TAG')
                    <li class="nav-item @if($activeSubmenu=='producttags') active @endif">
                        <a href="{{route('admin.producttags')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Product Tags</span>
                            @if($activeSubmenu=='producttags') <span class="selected"></span> @endif
                        </a>
                    </li>
                    @endcan
                    @can(['ADM_PRD_C_PRODUCT','ADM_PRD_U_PRODUCT'])
                    <li class="nav-item @if($activeSubmenu=='importimage') active @endif">
                        <a href="{{route('admin.image')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Import Image</span>
                            @if($activeSubmenu=='importimage') <span class="selected"></span> @endif
                        </a>
                    </li>
                    @endcan
                    <li class="nav-item @if($activeSubmenu=='reviews') active @endif">
                        <a href="{{route('admin.reviews')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Reviews</span>
                            @if($activeSubmenu=='reviews') <span class="selected"></span> @endif
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item  @if($activeMenu=='stockreport') active @endif">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Stock Report</span>
                    @if($activeMenu=='stockreport')
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    @else
                    <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item @if($activeSubmenu=='lowstock') active @endif">
                        <a href="{{route('admin.stockreport.lowstock')}}" class="nav-link ">
                             <i class="icon-bag"></i>
                            <span class="title">Low Stock Report</span>
                             @if($activeSubmenu=='lowstock') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='modifiedstock') active @endif">
                        <a href="{{route('admin.stockreport.modifiedstock')}}" class="nav-link ">
                             <i class="icon-bag"></i>
                            <span class="title">Modification Report</span>
                             @if($activeSubmenu=='modifiedstock') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='sellerproductstock') active @endif">
                        <a href="{{route('admin.stockreport.sellerproductstock')}}" class="nav-link ">
                             <i class="icon-bag"></i>
                            <span class="title">Seller Products</span>
                             @if($activeSubmenu=='sellerproductstock') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='expiredspecialprices') active @endif">
                        <a href="{{route('admin.stockreport.expiredspecialprices')}}" class="nav-link ">
                             <i class="icon-bag"></i>
                            <span class="title">Expired Special Prices</span>
                             @if($activeSubmenu=='expiredspecialprices') <span class="selected"></span> @endif
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item  @if($activeMenu=='coupons') active @endif">
                <a href="{{route('admin.coupons')}}" class="nav-link ">
                    <i class="icon-puzzle"></i>
                    <span class="title">Coupons Management</span>
                    @if($activeMenu=='coupons') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='coupongroup') active @endif">
                <a href="{{route('admin.coupongroups')}}" class="nav-link ">
                    <i class="icon-puzzle"></i>
                    <span class="title">Coupon Groups</span>
                    @if($activeMenu=='coupongroup') <span class="selected"></span> @endif
                </a>
            </li>


            <li class="nav-item  @if($activeMenu=='deals') active @endif">
                <a href="{{route('admin.deals')}}" class="nav-link ">
                    <i class="icon-puzzle"></i>
                    <span class="title">Deals of the Day</span>
                    @if($activeMenu=='deals') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='orders') active @endif">
                 <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bag"></i>
                    <span class="title">Order Management</span>
                    @if($activeMenu=='orders')
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    @else
                    <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item @if($activeSubmenu=='parentorders') active @endif">
                        <a href="{{route('admin.parentorders')}}" class="nav-link ">
                             <i class="icon-bag"></i>
                            <span class="title">Parent Orders</span>
                             @if($activeSubmenu=='parentorders') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='childorders') active @endif">
                        <a href="{{route('admin.childorders')}}" class="nav-link ">
                             <i class="icon-bag"></i>
                            <span class="title">Child Orders</span>
                             @if($activeSubmenu=='childorders') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='selfshipping') active @endif">
                        <a href="{{route('admin.selfshipping')}}" class="nav-link ">
                             <i class="icon-bag"></i>
                            <span class="title">Self-Shipping Orders</span>
                             @if($activeSubmenu=='selfshipping') <span class="selected"></span> @endif
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item  @if($activeMenu=='shipping') active @endif">
                <a href="{{route('admin.shipping')}}" class="nav-link ">
                    <i class="icon-pointer"></i>
                    <span class="title">Shipping</span>
                    @if($activeMenu=='shipping') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='payments') active @endif">
                <a href="{{route('admin.payments')}}" class="nav-link ">
                    <i class="icon-credit-card"></i>
                    <span class="title">Payment & Commission</span>
                    @if($activeMenu=='payments') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='tickets') active @endif">
                <a href="{{route('admin.tickets')}}" class="nav-link ">
                    <i class="icon-bubbles"></i>
                    <span class="title">Tickets</span>
                    @if($activeMenu=='tickets') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='sellers') active @endif">
                <a href="{{route('admin.sellers')}}" class="nav-link ">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Sellers</span>
                    @if($activeMenu=='sellers') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='commissionmodels') active @endif">
                <a href="{{route('admin.commissionmodels')}}" class="nav-link ">
                    <!-- <i class="fa fa-percent" aria-hidden="true"></i> -->
                    &nbsp;%
                    <span class="title">Commission Model</span>
                    @if($activeMenu=='commissionmodels') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='importantdocs') active @endif">
                <a href="{{route('admin.importantdocs')}}" class="nav-link ">
                    <i class="fa fa-file"></i>
                    <span class="title">Important Documents</span>
                    @if($activeMenu=='importantdocs') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeSubmenu=='approvals') active @endif">
                <a href="{{route('admin.approvals')}}" class="nav-link ">
                    <i class="icon-user"></i>
                    <span class="title">Approvals</span>
                    @if($activeSubmenu=='approvals') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='customers') active @endif">
                <a href="{{route('admin.customers')}}" class="nav-link ">
                    <i class="icon-users"></i>
                    <span class="title">Customers</span>
                    @if($activeMenu=='customers') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='notifications') active @endif">
                <a href="{{route('admin.notifications')}}" class="nav-link ">
                    <i class="icon-bubbles"></i>
                    <span class="title">Notifications</span>
                    @if($activeMenu=='notifications') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item @if($activeMenu=='usermgmt') active open @endif">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">User Managment</span>
                    @if($activeMenu=='usermgmt')
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    @else
                    <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item @if($activeSubmenu=='staff') active @endif">
                        <a href="{{route('admin.staff')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Staffs</span>
                            @if($activeSubmenu=='staff') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='roles') active @endif">
                        <a href="{{route('admin.roles')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Roles</span>
                            @if($activeSubmenu=='roles') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='permissions') active @endif">
                        <a href="{{route('admin.permissions')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Permissions</span>
                            @if($activeSubmenu=='permissions') <span class="selected"></span> @endif
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item @if($activeMenu=='system') active open @endif">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">System</span>
                    @if($activeMenu=='system')
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    @else
                    <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item @if($activeSubmenu=='slider') active @endif">
                        <a href="{{route('admin.slidermgmt')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Slider</span>
                            @if($activeSubmenu=='slider') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='shippingcharges') active @endif">
                        <a href="{{route('admin.shippingcharges')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Shipping Charges</span>
                            @if($activeSubmenu=='shippingcharges') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='handelingcharges') active @endif">
                        <a href="{{route('admin.handelingcharges')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Handling Charges</span>
                            @if($activeSubmenu=='handelingcharges') <span class="selected"></span> @endif
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
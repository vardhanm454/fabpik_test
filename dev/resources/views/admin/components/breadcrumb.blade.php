<div class="row">
    <div class="col-md-4">
        <h1 class="page-title margin-top-10 margin-bottom-10"> {{$title}}</h1>
    </div>
    <div class="col-md-8">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{route('admin.dashboard')}}">Home</a>       
            </li>
            @foreach($breadcrumbs as $breadcrumb)
            @if($breadcrumb->url)
            <li>
                <i class="fa fa-angle-right"></i> 
                <a href="{{($breadcrumb->url=='javascript:;')?'javascript:;':route($breadcrumb->url)}}"> {{$breadcrumb->title}} </a>        
            </li>
            @else
            <li> <i class="fa fa-angle-right"></i> <span>{{$breadcrumb->title}}</span> </li>
            @endif
            @endforeach
        </ul>
    </div>
</div>
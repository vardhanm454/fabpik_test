@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<!-- BEGIN PAGE TITLE-->
<!-- <h1 class="page-title"> {{$title}} -->
    <!-- <small>blank page layout</small> -->
<!-- </h1> -->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->

@if(Session::has('wlcm_msg'))
<div class="note note-success">
    <p> Welcome back! </p>
</div>
@endif

<!-- Top Blocks -->
@can('ADM_DSH_ORDERS')
<div class="row">
    <div class="col-md-3 margin-bottom-10">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{$ordersToday}} </div>
                <div class="desc"> Orders Today </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{$TotalOrders}} </div>
                <div class="desc"> Total Orders </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-shopping-cart fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{number_format((float)$totalSalesToday, 1, '.', ',')}} </div>
                <div class="desc">  Today Sale value </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{number_format((float)$totalSales, 1, '.', ',')}} </div>
                <div class="desc"> Total Sale Value </div>
            </div>
        </div>
    </div>
</div>
@endcan

<div class="row">
    @can('ADM_DSH_ORDERS')
    <div class="col-md-3 margin-bottom-10">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{$ordersToShip}} </div>
                <div class="desc"> Orders to Ship </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{$ordersDelivered}} </div>
                <div class="desc"> Total Orders Delivered </div>
            </div>
        </div>
    </div>
    @endcan

    @can('ADM_DSH_PAYMENTS')
    <div class="col-md-3">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-shopping-cart fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{number_format((float)$payoutAmount, 1, '.', ',')}} </div>
                <div class="desc">  Total Payout Amount </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{number_format((float)$totalPendingAmount, 1, '.', ',')}} </div>
                <div class="desc"> Pending Payout Amount </div>
            </div>
        </div>
    </div>
    @endcan
</div>

@can('ADM_DSH_ORD_GRAPH')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-blue"></i>
                    <span class="caption-subject font-blue bold">Orders</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#order_today_stats" data-toggle="tab"> Today </a>
                    </li>
                    <li>
                        <a href="#order_last7_stats" id="statistics_orders_weekly_tab" data-toggle="tab"> Last 7 Days </a>
                    </li>
                    <li>
                        <a href="#order_sixmonths_stats" id="statistics_orders_sixmonths_tab" data-toggle="tab"> Last 6 Months </a>
                    </li>
                </ul>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="order_today_stats">
                        <div id="order_today_chart" class="chart"> </div>
                    </div>
                    <div class="tab-pane" id="order_last7_stats">
                        <div id="order_weekly_chart" class="chart"> </div>
                    </div>
                    <div class="tab-pane" id="order_sixmonths_stats">
                        <div id="order_sixmonths_chart" class="chart"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endcan

<div class="row">
    @can('ADM_DSH_ORDERS')
    <div class="col-md-6">
        <!-- Begin: life time stats -->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-blue"></i>
                    <span class="caption-subject font-blue bold">Overview</span>
                    <span class="caption-helper">report overview...</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#top_selling_products" data-toggle="tab"> Top Selling </a>
                        </li>
                        <li>
                            <a href="#featured" data-toggle="tab"> Featured </a>
                        </li>
                        <li class="dropdown">
                            <a href="#latest_orders" data-toggle="tab"> Latest Orders </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="top_selling_products">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th> Product Name </th>
                                            <th> Sold </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($topselling as $items)
                                        <tr>
                                            <td> {{$items->name}} </td>
                                            <td> {{$items->total_sold}} </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="featured">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th> Product Name </th>
                                            <th> MRP </th>
                                            <th> Sale Price </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($featured as $items)
                                        <tr>
                                            <td> {{$items->name}} </td>
                                            <td> {{$items->mrp}} </td>
                                            <td> {{$items->sell_price}} </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="latest_orders">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th> Customer Name </th>
                                            <th> Total Orders </th>
                                            <th> Total Amount </th>
                                            <th> Status</th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($latestOrders as $items)
                                        <tr>
                                            <td> {{$items->name}} </td>
                                            <td> {{date('d-m-Y', strtotime($items->created_at))}} </td>
                                            <td> {{$items->total}} </td>
                                            <td> {{$items->seller_text}} </td>
                                            <td> <a href="{{route('admin.childorders.view', ['id'=>$items->id])}}" class="font-dark">View</a> </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @endcan

    @can('ADM_DSH_SLR_TICKETS')
    <div class="col-md-6">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-bubbles font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Support Tickets</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <!-- BEGIN: Comments -->
                    @foreach($ticket_details as $details)
                        <div class="mt-comments">
                            <div class="mt-comment">
                                <div class="mt-comment-img">
                                    {{-- <img src="{{__common_asset('pages/media/users/avatar1.jpg')}}" /> --}}
                                </div>
                                <div class="mt-comment-body">
                                    <div class="mt-comment-info">
                                        <span class="mt-comment-author">{{$details->issue_title}}</span>
                                        <span class="mt-comment-date">{{$details->created_date}}</span>
                                    </div>
                                    <div class="mt-comment-text"> {{$details->issue}} </div>
                                    <div class="mt-comment-details">
                                        <span class="mt-comment-status mt-comment-status-pending">{{($details->issue_status==0)?'Pending':'Completed'}}</span>
                                        <ul class="mt-comment-actions">
                                            <li>
                                                <a href="{{route('admin.tickets.view', ['id'=>$details->id])}}" class="font-dark">View</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <!-- END: Comments -->
                </div>
            </div>
        </div>
    </div>
    @endcan
</div>

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Dashboard = function() {

    function showChartTooltip(x, y, xValue, yValue) {
        $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
            position: 'absolute',
            display: 'none',
            top: y - 40,
            left: x - 40,
            border: '0px solid #ccc',
            padding: '2px 6px',
            'background-color': '#fff'
        }).appendTo("body").fadeIn(200);
    }

    var orderTodayChart = function() {

        var data = [
            @foreach ($todayOrdersStats as $i=>$stats)
                [ "{{ $stats['day'] }}", "{{ $stats['orders'] }}" ],
            @endforeach
        ];


        // var data = [
        //     ['07/2020', 1223],
        //     ['08/2020', 1365],
        //     ['01/2020', 4243],
        //     ['02/2020', 342],
        //     ['03/2020', 1034],
        //     ['04/2020', 326],
        //     ['05/2020', 2125],
        //     ['06/2020', 324],
        // ];

        var plot_statistics = $.plot(
            $("#order_today_chart"), [{
                data: data,
                lines: {
                    fill: 0.6,
                    lineWidth: 0
                },
                color: ['#f89f9f']
            }, {
                data: data,
                points: {
                    show: true,
                    fill: true,
                    radius: 5,
                    fillColor: "#f89f9f",
                    lineWidth: 3
                },
                color: '#fff',
                shadowSize: 0
            }], {

                xaxis: {
                    tickLength: 0,
                    tickDecimals: 0,
                    mode: "categories",
                    min: 2,
                    font: {
                        lineHeight: 15,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                yaxis: {
                    ticks: 3,
                    tickDecimals: 0,
                    tickColor: "#f0f0f0",
                    font: {
                        lineHeight: 15,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                grid: {
                    backgroundColor: {
                        colors: ["#fff", "#fff"]
                    },
                    borderWidth: 1,
                    borderColor: "#f0f0f0",
                    margin: 0,
                    minBorderMargin: 0,
                    labelMargin: 20,
                    hoverable: true,
                    clickable: true,
                    mouseActiveRadius: 6
                },
                legend: {
                    show: false
                }
            }
        );

        var previousPoint = null;

        $("#order_today_chart").bind("plothover", function(event, pos, item) {
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                    showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1]);
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });

    }

    var orderWeeklyChart = function() {

        var data = [
            @foreach ($ordersStats as $i=>$stats)
                [ '{{ $stats["day"] }}', '{{ $stats["orders"] }}' ],
            @endforeach
        ];


        // var data = [
        //     ['01/00/2020', 10],
        //     ['02/00/2020', 10],
        //     ['01/01/2020', 10],
        //     ['02/01/2020', 0],
        //     ['03/01/2020', 10],
        //     ['04/01/2020', 12],
        //     ['05/01/2020', 212],
        //     ['06/01/2020', 324],
        //     ['07/01/2020', 122]
        // ];

        var plot_statistics = $.plot(
            $("#order_weekly_chart"), [{
                data: data,
                lines: {
                    fill: 0.6,
                    lineWidth: 0
                },
                color: ['#BAD9F5']
            }, {
                data: data,
                points: {
                    show: true,
                    fill: true,
                    radius: 5,
                    fillColor: "#BAD9F5",
                    lineWidth: 3
                },
                color: '#fff',
                shadowSize: 0
            }], {

                xaxis: {
                    tickLength: 0,
                    tickDecimals: 0,
                    mode: "categories",
                    min: 2,
                    font: {
                        lineHeight: 14,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                yaxis: {
                    ticks: 3,
                    tickDecimals: 0,
                    tickColor: "#f0f0f0",
                    font: {
                        lineHeight: 14,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                grid: {
                    backgroundColor: {
                        colors: ["#fff", "#fff"]
                    },
                    borderWidth: 1,
                    borderColor: "#f0f0f0",
                    margin: 0,
                    minBorderMargin: 0,
                    labelMargin: 20,
                    hoverable: true,
                    clickable: true,
                    mouseActiveRadius: 6
                },
                legend: {
                    show: false
                }
            }
        );

        var previousPoint = null;

        $("#order_weekly_chart").bind("plothover", function(event, pos, item) {
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                    showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1]);
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });
    }

    var orderSixMonthsChart = function() {

        var data = [
            @foreach ($orderSixMonthStats as $i=>$stats)
                [ "{{ $stats['month'] }}", "{{ $stats['orders'] }}" ],
            @endforeach
        ];


        // var data = [
        //     ['01/00/2020', 10],
        //     ['02/00/2020', 10],
        //     ['01/01/2020', 10],
        //     ['02/01/2020', 0],
        //     ['03/01/2020', 10],
        //     ['04/01/2020', 12],
        //     ['05/01/2020', 212],
        //     ['06/01/2020', 324],
        //     ['07/01/2020', 122]
        // ];

        var plot_statistics = $.plot(
            $("#order_sixmonths_chart"), [{
                data: data,
                lines: {
                    fill: 0.6,
                    lineWidth: 0
                },
                color: ['#BAD9F5']
            }, {
                data: data,
                points: {
                    show: true,
                    fill: true,
                    radius: 5,
                    fillColor: "#BAD9F5",
                    lineWidth: 3
                },
                color: '#fff',
                shadowSize: 0
            }], {

                xaxis: {
                    tickLength: 0,
                    tickDecimals: 0,
                    mode: "categories",
                    min: 2,
                    font: {
                        lineHeight: 14,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                yaxis: {
                    ticks: 3,
                    tickDecimals: 0,
                    tickColor: "#f0f0f0",
                    font: {
                        lineHeight: 14,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                grid: {
                    backgroundColor: {
                        colors: ["#fff", "#fff"]
                    },
                    borderWidth: 1,
                    borderColor: "#f0f0f0",
                    margin: 0,
                    minBorderMargin: 0,
                    labelMargin: 20,
                    hoverable: true,
                    clickable: true,
                    mouseActiveRadius: 6
                },
                legend: {
                    show: false
                }
            }
        );

        var previousPoint = null;

        $("#order_sixmonths_chart").bind("plothover", function(event, pos, item) {
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                    showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1]);
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });
    }

    return {

        //main function
        init: function() {

            orderTodayChart();

            $('#statistics_orders_weekly_tab').on('shown.bs.tab', function(e) {
                orderWeeklyChart();
            });

            $('#statistics_orders_sixmonths_tab').on('shown.bs.tab', function(e) {
                orderSixMonthsChart();
            });

        }

    };

}();

jQuery(document).ready(function() {
   Dashboard.init();
});
</script>
@endpush
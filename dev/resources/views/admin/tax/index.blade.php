@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                    <form action="#" method="get" class="filter-form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 pd-tb-5"><strong>Tax Code:</strong></label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control input-sm" name="fcode"
                                                        id="fcode" value="{{request()->fcode}}" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 pd-tb-5"><strong>Status:</strong></label>
                                                <div class="col-md-9">
                                                    <select name="fstatus" id="fstatus" class="form-control input-sm">
                                                        <option value="">All</option>
                                                        <option value="1" {{request()->fstatus=='1'?'selected="selected"':''}}>Active</option>
                                                        <option value="0" {{request()->fstatus=='0'?'selected="selected"':''}}>Inactive</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="col-md-3">
                                            <label class="control-label col-md-4 pd-tb-5"><strong>Action:</strong></label>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Filter">
                                                    <span aria-hidden="true" class="icon-magnifier"></span> </button>
                                                <a class="btn btn-sm btn-icon-only white" href="{{route('admin.tax')}}" title="Reset"><span aria-hidden="true" class="icon-refresh"></span></a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <a href="javascript:void(0)" class="btn btn-default btn-sm tooltips add-tax-code" data-toggle="tooltip"
                            data-placement="top" data-original-title="Add Tax Code"><strong><span aria-hidden="true" class="icon-plus text-primary"></span>&nbsp;Add</strong></a>
                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip"
                            data-placement="top" data-original-title="Delete" data-action="soft-delete"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;Delete</strong></a>
                        <a href="javascript:;" id="btn_table_export" class="btn btn-sm btn-outline btn-default text-warning tooltips"
                            data-container="body" data-placement="top" data-original-title="Export to Excel"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a>
                    </div>
                    <table class="table table-checkable nowrap"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="10%" class=""> Code </th>
                                <th width="10%" class=""> IGST </th>
                                <th width="10%" class=""> CGST </th>
                                <th width="20%" class=""> SGST </th>
                                <th width="10%" class=""> Actions </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
        <!-- ## ADD TAXCODE MODEL -->
        <div class="modal fade addtax" id="addBanner" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Tax Code</h4>
                    </div>
                    <form class="AddTaxForm" method="post">
                     <div class="col-lg-12">
                        <div class="row p-a20 pb-0" style="margin-top: 2rem !important;">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="">
                                        <label class="control-label">Tax Code</label>
                                        <div class="">
                                            <input type="text" class="form-control maxlength-handler" name="code" placeholder="" value="">
                                            <input type="hidden" class="form-control maxlength-handler" name="mode" value="insert">
                                            <input type="hidden" class="form-control maxlength-handler" name="tex_id">
                                            <span class="code-error text-danger" style="margin-top: 5px !important;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="">
                                        <label class="control-label">Igst</label>
                                        <div class="">
                                            <input type="text" class="form-control maxlength-handler" name="igst" placeholder="" value="">
                                            <span class="igst-error text-danger" style="margin-top: 5px !important;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row p-a20 pb-0" style="margin-top: 1rem !important;">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="">
                                        <label class="control-label">Cgst</label>
                                        <div class="">
                                            <input type="text" class="form-control maxlength-handler" name="cgst" placeholder="" value="">
                                            <span class="cgst-error text-danger" style="margin-top: 5px !important;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="">
                                        <label class="control-label">Sgst</label>
                                        <div class="">
                                            <input type="text" class="form-control maxlength-handler" name="sgst" placeholder="" value="">
                                            <span class="sgst-error text-danger" style="margin-top: 5px !important;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- ## ADD TAXCODE MODEL -->
        <div class="modal fade edittax" id="addBanner" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Tax Code</h4>
                    </div>
                    <form class="EditTaxForm" method="post">
                     <div class="col-lg-12">
                        <div class="row p-a20 pb-0" style="margin-top: 2rem !important;">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="">
                                        <label class="control-label">Tax Code</label>
                                        <div class="">
                                            <input type="text" class="form-control maxlength-handler code" name="code" placeholder="" value="">
                                            <input type="hidden" class="form-control maxlength-handler" name="mode" value="update">
                                            <input type="hidden" class="form-control maxlength-handler tex_id" name="tex_id">
                                            <span class="code-error text-danger" style="margin-top: 5px !important;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="">
                                        <label class="control-label">Igst</label>
                                        <div class="">
                                            <input type="text" class="form-control maxlength-handler igst" name="igst" placeholder="" value="">
                                            <span class="igst-error text-danger" style="margin-top: 5px !important;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row p-a20 pb-0" style="margin-top: 1rem !important;">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="">
                                        <label class="control-label">Cgst</label>
                                        <div class="">
                                            <input type="text" class="form-control maxlength-handler cgst" name="cgst" placeholder="" value="">
                                            <span class="cgst-error text-danger" style="margin-top: 5px !important;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <div class="">
                                        <label class="control-label">Sgst</label>
                                        <div class="">
                                            <input type="text" class="form-control maxlength-handler sgst" name="sgst" placeholder="" value="">
                                            <span class="sgst-error text-danger" style="margin-top: 5px !important;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Update</button> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Tax = function() {

    return {

        //main function to initiate the module
        init: function() {

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.tax')}}?search=1";

                if ($('#fcode').val() != '') url += "&fcode=" + encodeURIComponent($('#fcode').val());
                
                window.location.href = url;
            });

            // Export all Coupons data
            $('#btn_table_export').on('click', function(e) {
                e.preventDefault();

                var url = "{{route('admin.tax.taxexport')}}?export=1";

                if ($('#fcode').val() != '') url += "&fcode=" + encodeURIComponent($('#fcode').val());

                window.location.href = url;
            });

            // Delete Coupon
            $('body').on('click', '.dt-list-delete', function(event) {
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                swal({
                        title: 'Are you sure?',
                        text: 'You want Delete the record.',
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonClass: 'btn-success',
                        cancelButtonClass: 'btn-danger',
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: 'post',
                                success: function(result) {
                                    swal('Coupons', 'Selected Coupon Deleted Successfully',
                                        "success");
                                    location.reload();
                                }
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
            });

            // Change Coupon Status
            $('body').on('switchChange.bootstrapSwitch', '.status-switch', function(event, state) {
                var switchControl = $(this);
                var stateText = (state) ? 'Activate' : 'Deactivate';
                swal({
                        title: 'Are you sure?',
                        text: 'You want ' + stateText + ' this data.',
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonClass: 'btn-success',
                        cancelButtonClass: 'btn-danger',
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: "{{route('ajax.changeStatus')}}",
                                type: 'post',
                                data: {
                                    table: 'coupons',
                                    id: switchControl.val(),
                                    status: state
                                },
                                success: function(result) {
                                    swal('Coupons', 'Data has been ' + stateText + 'd',
                                        "success");
                                }
                            });
                        } else {
                            switchControl.bootstrapSwitch('state', !state);
                        }
                    });
            });

            // add tax
            $('body').on('click', '.add-tax-code', function(event) {
                  $('.addtax').modal('show');
                  $('.AddTaxForm').submit(function(event) {
                        event.preventDefault();
                        $.ajax({

                            url: '{{route("admin.tax.taxcrud")}}',
                            type: 'POST',
                            data: $('.AddTaxForm').serialize(),
                            success: function(data) {

                                if (data.errors) {

                                    if (data.errors.code) {
                                        $('.code-error').html(data.errors.code[0]);
                                    }
                                    if (data.errors.igst) {
                                        $('.igst-error').html(data.errors.igst[0]);
                                    }
                                    if (data.errors.cgst) {
                                        $('.cgst-error').html(data.errors.cgst[0]);
                                    }
                                    if (data.errors.sgst) {
                                        $('.sgst-error').html(data.errors.sgst[0]);
                                    }

                                }

                                if (data.status == 200) {

                                    $('.addtax').modal('hide');
                                    $('.AddTaxForm')[0].reset();
                                    toastr.success(data.message);
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1000);

                                }
                                if (data.status == 201) {

                                    toastr.error(data.message);
                                    setTimeout(function() {
                                        location.reload();
                                    }, 3000);

                                }

                            }

                        });
                    });


            });

            // edit tax code
            $('body').on('click', '.edit-taxcode', function(event) {
                  var taxid = $(this).attr('rel');
                  var mode = $(this).attr('rel1');

                  $.ajax({
                        url: '{{route("admin.tax.taxcrud")}}',
                        type: 'POST',
                        data: {id:taxid, mode:mode},
                        success: function(data) {
                           
                           $('.code').val(data.result.code);
                           $('.tex_id').val(data.result.id);
                           $('.igst').val(data.result.igst);
                           $('.cgst').val(data.result.cgst);
                           $('.sgst').val(data.result.sgst);
                           $('.edittax').modal('show');

                        }
                  })
            });

            //update tax code
            $('.EditTaxForm').submit(function(event) {
                event.preventDefault();
                $.ajax({
                    url: '{{route("admin.tax.taxcrud")}}',
                    type: 'POST',
                    data: $('.EditTaxForm').serialize(),
                    success: function(data) {

                        if (data.errors) {

                            if (data.errors.code) {
                                $('.code-error').html(data.errors.code[0]);
                            }
                            if (data.errors.igst) {
                                $('.igst-error').html(data.errors.igst[0]);
                            }
                            if (data.errors.cgst) {
                                $('.cgst-error').html(data.errors.cgst[0]);
                            }
                            if (data.errors.sgst) {
                                $('.sgst-error').html(data.errors.sgst[0]);
                            }

                        }

                        if (data.status == 200) {

                            $('.edittax').modal('hide');
                            $('.EditTaxForm')[0].reset();
                            toastr.success(data.message);
                            setTimeout(function() {
                                location.reload();
                            }, 1000);

                        }
                        if (data.status == 201) {

                            toastr.error(data.message);
                            setTimeout(function() {
                                location.reload();
                            }, 3000);

                        }

                    }

                });
            });

            // delete tax code
            $('body').on('click', '.delete-taxcode', function(event) {
                
                var taxid = $(this).attr('rel');
                var mode = $(this).attr('rel1');
                
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the taxcode.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                                url: '{{route("admin.tax.taxcrud")}}',
                                type: 'POST',
                                data: {id:taxid, mode:mode},
                                success: function(data) {
                                
                                    if (data.status == 200) {
                                    swal('Banner', 'Deleted Successfully', "success");
                                    location.reload();
                                    }
                                    if (data.status == 201) {
                                    swal('TaxCode', 'Something went wrong', "danger");
                                    location.reload();
                                    }

                                }
                        });

                    } else {
                        swal("Cancelled", "", "error");
                    }
                });

            });
            

            




        }

    };

}();

jQuery(document).ready(function() {
    Tax.init();
});
</script>
@endpush
@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    @if(isset($commissionmodel))
                    <form class="form-horizontal" action="{{route('admin.commissionmodels.edit',['id'=>$id])}}" method="post"
                        enctype="multipart/form-data">
                        @else
                        <form class="form-horizontal" action="{{route('admin.commissionmodels.add')}}" method="post"
                        enctype="multipart/form-data">
                        @endif

                        {!! csrf_field() !!}

                            <div class=row>
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('name'))?'has-error':''}}">
                                        <label class="col-md-4 control-label ">Name:<span class="required"> *
                                            </span></label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control maxlength-handler" name="name"
                                                maxlength="100" placeholder="" value="{{old('name', isset($commissionmodel)?$commissionmodel->name:'')}}">
                                            <span class="help-block">{{$errors->first('name')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('commission_type'))?'has-error':''}}">
                                        <label class="col-md-4 control-label ">Commission Type:<span class="required"> *
                                            </span></label>
                                        <div class="col-md-6">
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="commission_type"  value="f" {{old('commission_type' , isset($commissionmodel) ? $commissionmodel->commission_type : '') == 'f' ? "checked" : "" }} > Fixed <span> </span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="commission_type"  value="v" {{old('commission_type' , isset($commissionmodel) ? $commissionmodel->commission_type : '') == 'v' ? "checked" : "" }} > Variable <span> </span>
                                                </label>
                                            </div>
                                            <span class="help-block">{{$errors->first('commission_type')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class=row>
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('commission_on'))?'has-error':''}}">
                                        <label class="col-md-4 control-label ">Commission On:<span class="required"> *
                                            </span></label>
                                        <div class="col-md-4">
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="commission_on" value="m" {{ old('commission_on', isset($commissionmodel) ? $commissionmodel->commission_on : '') == 'm' ? "checked" : "" }} > MRP <span> </span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="commission_on" value="s" {{ old('commission_on', isset($commissionmodel) ? $commissionmodel->commission_on : '') == 's' ? "checked" : "" }} > Sell Price <span> </span>
                                                </label>
                                            </div>
                                            <span class="help-block">{{$errors->first('commission_on')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('vary_with_mrp_discount'))?'has-error':''}}">
                                        <label class="col-md-4 control-label ">Vary With Mrp Discount:<span class="required"> *
                                            </span></label>
                                        <div class="col-md-4">
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="vary_with_mrp_discount"  value="y" {{old('vary_with_mrp_discount', isset($commissionmodel) ? $commissionmodel->vary_with_mrp_discount : '') == 'y' ? "checked" : "" }} > Yes <span> </span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="vary_with_mrp_discount"  value="n" {{old('vary_with_mrp_discount', isset($commissionmodel) ? $commissionmodel->vary_with_mrp_discount : '') == 'n' ? "checked" : "" }} > No <span> </span>
                                                </label>
                                            </div>
                                            <span class="help-block">{{$errors->first('vary_with_mrp_discount')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class=row>
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('shipping_charges'))?'has-error':''}}">
                                        <label class="col-md-4 control-label ">Shipping Charges:<span class="required"> *
                                            </span></label>
                                        <div class="col-md-4">
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="shipping_charges"  value="y"  {{old('shipping_charges', isset($commissionmodel) ? $commissionmodel->shipping_charges : '') == 'y' ? "checked" : "" }} > Yes <span> </span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="shipping_charges"  value="n"  {{old('shipping_charges', isset($commissionmodel) ? $commissionmodel->shipping_charges : '') == 'n' ? "checked" : "" }} > No <span> </span>
                                                </label>
                                            </div>
                                            <span class="help-block">{{$errors->first('shipping_charges')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('handling_charges'))?'has-error':''}}">
                                        <label class="col-md-4 control-label">Handling Charges:<span class="required"> *
                                            </span></label>
                                        <div class="col-md-4">
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="handling_charges"  value="y"  {{old('handling_charges', isset($commissionmodel) ? $commissionmodel->handling_charges : '') == 'y' ? "checked" : "" }} > Yes <span> </span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="handling_charges"  value="n"  {{old('handling_charges', isset($commissionmodel) ? $commissionmodel->handling_charges : '') == 'n' ? "checked" : "" }} > No <span> </span>
                                                </label>
                                            </div>
                                            <span class="help-block">{{$errors->first('handling_charges')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=row>
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('description'))?'has-error':''}}">
                                        <label class="col-md-4 control-label">Description:</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" name="description">{{old('description', isset($commissionmodel)?$commissionmodel->description:'')}}</textarea>
                                            <span class="help-block">{{$errors->first('description')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    @if(isset($commissionmodel))
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Status:<span class="required"> * </span></label>
                                        <div class="col-md-9">
                                            <select name="status" id="status" class="form-control input-sm">
                                                @foreach([1=>'Active',0=>'Inactive'] as $val=>$label)
                                                <option value="{{$val}}" {{(old('status', (isset($commissionmodel))?$commissionmodel->status:1)==$val)?'selected="selected"':''}}>{{$label}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif 

                                </div>
                            </div>
                        </div>
                        
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                            @if(isset($commissionmodel))
                            <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
                            <button type="button" del-url="{{route('admin.commissionmodels.delete', ['id'=>$id])}}" class="btn blue delete" name="delete" value="delete"><span aria-hidden="true" class="icon-trash"></span> Delete</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var CommissionModelAddEdit = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.commissionmodels')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

              // Delete CommissionModel
              $('body').on('click', '.delete', function(event){
                event.preventDefault();
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                console.log(result);
                                if(result.error){
                                    swal('Commission Model', result.message, "error");
                                    // window.location.href = "{{route('admin.commissionmodels')}}";
                                }else{
                                    swal('Commission Model', 'Record Deleted Successfully', "success");
                                    window.location.href = "{{route('admin.commissionmodels')}}";
                                }
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });

        }

    };

}();

jQuery(document).ready(function() {
    CommissionModelAddEdit.init();
});
</script>
@endpush
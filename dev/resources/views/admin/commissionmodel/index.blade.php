@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<style>

    .modal-body {
        position: relative;
        overflow-y: auto;
        max-height: 400px;
        padding: 15px;
    }
</style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <!-- FILTER FORM START -->
                
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 pd-tb-5"><strong>Commission Model:</strong></label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control input-sm" name="fname" id="fname" value="{{request()->fname}}" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 pd-tb-5"><strong>Status:</strong></label>
                                                <div class="col-md-8">
                                                    <select name="fstatus" id="fstatus" class="form-control form-filter input-sm select2" data-allow-clear="true">
                                                        <option value="">All</option>
                                                        <option value="1" {{request()->fstatus=='1'?'selected="selected"':''}}>Active</option>
                                                        <option value="0" {{request()->fstatus=='0'?'selected="selected"':''}}>Inactive</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 pd-tb-5"><strong>Commission Type:</strong></label>
                                                <div class="col-md-8">
                                                         
                                                    <select name="fcommissiontype" id="fcommissiontype" class="form-control input-sm select2" data-allow-clear="true" >
                                                        <option value="">All</option>
                                                        <option value="f" {{request()->fcommissiontype=='f'?'selected="selected"':''}}>Fixed</option>
                                                        <option value="v" {{request()->fcommissiontype=='v'?'selected="selected"':''}}>Variable</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 pd-tb-5"><strong>Shipping Charges:</strong></label>
                                                <div class="col-md-8">
                                                    <select name="fshippingcharges" id="fshippingcharges" class="form-control input-sm select2" data-allow-clear="true" >
                                                        <option value="">All</option>
                                                        <option value="y" {{request()->fshippingcharges=='y'?'selected="selected"':''}}>Yes</option>
                                                        <option value="n" {{request()->fshippingcharges=='n'?'selected="selected"':''}}>No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 pd-tb-5"><strong>Handling Charges:</strong></label>
                                                <div class="col-md-8">
                                                    <select name="fhandlingcharges" id="fhandlingcharges" class="form-control input-sm select2" data-allow-clear="true" >
                                                        <option value="">All</option>
                                                        <option value="y" {{request()->fhandlingcharges=='y'?'selected="selected"':''}}>Yes</option>
                                                        <option value="n" {{request()->fhandlingcharges=='n'?'selected="selected"':''}}>No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 pd-tb-5"><strong>Commission On:</strong></label>
                                                <div class="col-md-8">
                                                    <select name="foncommission" id="foncommission" class="form-control input-sm select2" data-allow-clear="true" >
                                                        <option value="">All</option>
                                                        <option value="m" {{request()->foncommission=='m'?'selected="selected"':''}}>MRP</option>
                                                        <option value="s" {{request()->foncommission=='s'?'selected="selected"':''}}>Sell Price</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="control-label col-md-4 pd-tb-5"><strong>Action:</strong></label>
                                            <div class="col-md-8">
                                                <button type="button" class="btn btn-sm btn-icon-only white " name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span> </button>
                                                <a class="btn btn-sm btn-icon-only white" href="{{route('admin.commissionmodels')}}" title="Reset Filter"><span aria-hidden="true" class="icon-refresh"></span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
            </div>
            <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-actions-wrapper">
                            <a href="{{route('admin.commissionmodels.add')}}" class="btn btn-default btn-sm tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Add CommissionModel"><strong><span aria-hidden="true" class="icon-plus text-primary"></span>&nbsp;Add</strong></a>

                            <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete" data-action="soft-delete"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;Delete</strong></a>

                            <a href="javascript:;" class="btn btn-default btn-sm" data-placement="top" data-original-title="Fabpik Commission Model Description" data-toggle="modal" data-target="#helpModal"><strong><span aria-hidden="true" id="btn-confirm" class="fa fa-question-circle text-primary"></span>&nbsp;Help</strong></a>

                        </div>
                        <table class="table table-checkable nowrap" id="dt_listing">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%" class="">
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th width="15%" class=""> Commission <br/>Model </th>
                                    <th width="15%" class=""> Commission <br/>Type </th>
                                    <th width="15%" class=""> Commission On </th>
                                    <th width="15%" class=""> vary With <br /> MRP Discount </th>
                                    <th width="15%" class=""> Shipping <br /> Charges </th>
                                    <th width="15%" class=""> Handling <br /> Charges </th>
                                    <th width="15%" class=""> Created By </th>
                                    <th width="" class="none"> Created At </th>
                                    <th width="15%" class=""> References </th>
                                    <th width="15%" class=""> Status </th>                                    
                                    <th width="15%" class=""> Actions </th>
                                </tr>
                            </thead>
                            <tbody> </tbody>
                        </table>
                    </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Fabpik Commission Model Description</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <p class="text-center"><b>Commission Model is going to have total 9 digits. Each digit represents a meaning.</b></p>
        <img class="img-fluid" src="{{ __common_asset('img/commissionmodel.png') }}" alt="Comission Modal Help Image" >
        <p>X – Represents a Digit</br>
           Y – Represents an Alphabet</p>
           
           <p><b>Details of Each digit</b></p>
                F - Fabpik  (Constant)<br/>
                C – Commission (Constant)<br/>
                9/8/7 - First Digit Represents is it a B2C or B2B model<br/>
                6/5/4 - Second Digit Repreesnts Fullfillment model (Dropship, Fabpik Fullfillment)<br/>
                1/0 - third Digit represents Vary with MRP Discount<br/>
                1/0 - fourth Digital rprenset Shipping Charges<br/>
                1/0 - Fifth Digit Represents Handling Charges<br/>
                F/V – First alphabet represents commission type Fixed (or) Variable<br/>
                M/S – Second alphabet represents commission on which price. MRP (or) selling Price<br>

                <p><b>Each Digit & Alphabet and its meaning:</b></p>
                9 - B2C Commission Model<br />
                8 - B2B Commission Model<br />
                7 - Leaving it blank for now<br />
                6 - drop ship model<br />
                5 - Fabpik Fullfilled model (Fabpik gets the products in their warehouse in future)<br />
                4 - Leaving it blank for now<br />
                Vary with MRP Discount (Yes) 1  ; (No) 0<br />
                Shipping Charges (Yes) 1 ; (No) 0<br />
                Handling Charges (Yes) 1 ; (No) 0<br />

                <p><b>Example:</b></p>
                <p>FC96000FM - Fabpik Commission Model - Fixed MRP Commission model – No variable discount, No Shipping Charges & No Handling Charges.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var CommissionModel = function () {

    
    return {

        //main function to initiate the module
        init: function () {

            //select all product variants
            $(document).on("keypress", "input", function(e) {
                if (e.which == 13) {
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

            // Do Filter
            $('#btn_submit_search').on('click', function(e){
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.commissionmodels')}}?search=1";

                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());
                if($('#fcommissiontype').val() != '') url += "&fcommissiontype="+encodeURIComponent($('#fcommissiontype').val());
                if($('#fshippingcharges').val() != '') url += "&fshippingcharges="+encodeURIComponent($('#fshippingcharges').val());
                if($('#fhandlingcharges').val() != '') url += "&fhandlingcharges="+encodeURIComponent($('#fhandlingcharges').val());
                if($('#foncommission').val() != '') url += "&foncommission="+encodeURIComponent($('#foncommission').val());
                
                window.location.href = url;
            });

            // Export all CommissionModel data
            $('#btn_table_export').on('click', function(e){
                e.preventDefault();

                var url = "{{route('admin.commissionmodels.export')}}?export=1";

                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());

                window.location.href = url;
            });

            // Delete CommissionModel
            $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                console.log(result);
                                if(result.error){
                                    swal('Commission Model', result.message, "error");
                                    //  location.reload();
                                }else{
                                    swal('Commission Model', 'Record Deleted Successfully', "success");
                                    location.reload();
                                }
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });
        }

    };

}();

jQuery(document).ready(function() {    
   CommissionModel.init();
});
</script>
@endpush
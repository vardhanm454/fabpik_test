@extends(ADMIN_THEME_NAME.'.layouts.auth')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{route('admin.login')}}" method="post">
    	{!! csrf_field() !!}

        <h3 class="form-title">Login to your account</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Enter email and password. </span>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" value="" /> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" value="" /> 
            </div>
        </div><hr>
        <div class="row">
            <div class="col-md-7 pd-0">
                <!-- <a href="{{route('admin.forgotPassword')}}" id="forget-password">Forgot password?</a> -->
            </div>
            <div class="col-md-5 pd-0">
                <button type="submit" class="btn green pull-right" id="login_btn" name="doSubmit" value="doLogin" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Logging in..."><span aria-hidden="true" class="icon-arrow-right"></span> Login</button>
            </div>
        </div>
    </form>
    <!-- END LOGIN FORM -->
</div>
<!-- END LOGIN -->
@stop

@push('PAGE_ASSETS_JS')
<script type="text/javascript">
var Login = function() {
    var handleLogin = function() {

        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },

            messages: {
                email: {
                    required: "Email is required.",
                    email: "Enter valid email."
                },
                password: {
                    required: "Password is required."
                }
            },

            invalidHandler: function(event, validator,) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
                $('#login_btn').button('reset');
            },            

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {

            handleLogin();

            $('#login_btn').click(function(){
                $(this).button('loading');
            });

            $('.login-form input').keypress(function(e) {
                if (e.which == 13) {
                    if ($('.login-form').validate().form()) {
                        $( "#login_btn" ).click();
                    }
                    return false;
                }
            });
        }
    };
}();

jQuery(document).ready(function() {
    Login.init();
});
</script>
@endpush
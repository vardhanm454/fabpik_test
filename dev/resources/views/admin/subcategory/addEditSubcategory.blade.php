@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    @if(isset($subcategory))
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.subcategories.edit',['id'=>$id])}}" method="post" enctype="multipart/form-data">
                    @else
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.subcategories.add')}}" method="post" enctype="multipart/form-data">
                    @endif

                        {!! csrf_field() !!}

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="{{($errors->first('parentcategory'))?'has-error':''}}">
                                                <label class="control-label">Parent Category<span class="required"> *
                                                    </span></label>
                                                <div class="">
                                                    <select class="form-control" name="parentcategory">
                                                        <option value="">Select</option>
                                                        @foreach($categories as $category)
                                                        <option value="{{$category->id}}" @if(old('parentcategory',isset($subcategory)?$subcategory->category_id:'')==$category->id) selected @endif>
                                                            {{$category->title}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="help-block">{{$errors->first('parentcategory')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="{{($errors->first('name'))?'has-error':''}}">
                                                <label class="control-label">Name:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="name" maxlength="100" placeholder="" value="{{old('name', isset($subcategory)?$subcategory->title:'')}}">
                                                    <span class="help-block">{{$errors->first('name')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="{{($errors->first('description'))?'has-error':''}}">
                                                <label class="control-label">Description:<span class="required"> * </span></label>
                                                <div class="">
                                                    <textarea class="form-control" rows="1" name="description">{{old('description', isset($subcategory)?$subcategory->description:'')}}</textarea>
                                                    <span class="help-block">{{$errors->first('description')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="{{($errors->first('meta_title'))?'has-error':''}}">
                                                <label class="control-label">Meta Title:</label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="meta_title" maxlength="100" placeholder="" value="{{old('meta_title', isset($subcategory)?$subcategory->meta_title:'')}}">
                                                    <span class="help-block">{{$errors->first('meta_title')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="">
                                                <label class="control-label">Meta Keywords:</label>
                                                <div class="">
                                                    <textarea class="form-control maxlength-handler" rows="1" name="meta_keywords" maxlength="255">{{old('meta_keywords',(isset($subcategory))?$subcategory->meta_keywords:'')}}</textarea>
                                                    <span class="help-block"> max 255 chars </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="{{($errors->first('meta_description'))?'has-error':''}}">
                                                <label class="control-label">Meta Description:</label>
                                                <div class="">
                                                    <textarea class="form-control maxlength-handler" rows="1" name="meta_description" maxlength="255">{{old('meta_description', isset($subcategory)?$subcategory->meta_description:'')}}</textarea>
                                                    <span class="help-block"> max 255 chars </span>
                                                    <span class="help-block">{{$errors->first('meta_description')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(isset($subcategory))
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-4">
                                            
                                            <div class="{{($errors->first('status'))?'has-error':''}}">
                                                <label class="control-label">Status<span class="required"> *
                                                    </span></label>
                                                <div class="">
                                                    <select class="form-control input-sm" name="status">
                                                        @foreach(['1'=>'Active','0'=>'Inactive'] as $val=>$label)
                                                        <option value="{{$val}}" @if(old('status',
                                                            isset($subcategory)?$subcategory->status:'')==$val) selected @endif>
                                                            {{$label}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="help-block">{{$errors->first('status')}}</span>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    <div class="{{($errors->first('banner_image'))?'has-error':''}}">
                                        <label class="control-label" for="input-image">Banner <small>(200x300)</small>:<span class="required"> * </span></label>
                                        <div class="">
                                            <a href="" id="thumb-image-banner" data-toggle="image" class="img-thumbnail">
                                                <img src="{{ route('ajax.previewImage',['image'=>isset($subcategory)?$subcategory->image:'','type'=>'subcategory']) }}" alt="" title="" data-placeholder="{{ url('uploads/no-image.png') }}" width="100" height="100" />
                                            </a>
                                            <input type="hidden" name="banner_image" value="{{isset($subcategory)?$subcategory->image:''}}" id="input-image" />
                                            <span class="help-block">{{$errors->first('banner_image')}}</span>
                                        </div>
                                    </div>
                                    <div class="{{($errors->first('icon_image'))?'has-error':''}}">
                                        <label class="control-label" for="input-image-icon">Icon
                                            <small>(32x32)</small>:<span class="required"> * </span></label>
                                        <div class="">
                                            <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                                                <img src="{{ route('ajax.previewImage',['image'=>isset($subcategory)?$subcategory->icon:'','type'=>'subcategory']) }}" alt="" title="" data-placeholder="{{ url('uploads/no-image.png') }}" width="50" height="50" />
                                            </a>
                                            <input type="hidden" name="icon_image" value="{{isset($subcategory)?$subcategory->icon:''}}" id="input-image-icon" />
                                            <span class="help-block">{{$errors->first('icon_image')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                            @if(isset($subcategory))
                            <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var SubCategoriesAddEdit = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.subcategories')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

            $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
                var $element = $(this);
                var $popover = $element.data('bs.popover');

                e.preventDefault();
                $('a[data-toggle="image"]').popover('destroy');
                if ($popover) {
                    return;
                }
                $element.popover({
                    html: true,
                    placement: 'bottom',
                    trigger: 'manual',
                    content: function() {
                        return '<button type="button" id="button-image" class="btn btn-outline btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-outline btn-danger"><i class="fa fa-trash-o"></i></button>';
                    }
                });
                $element.popover('show');
                $('#button-image').on('click', function() {
                    var $button = $(this);
                    var $icon = $button.find('> i');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element
                            .parent().find('input').attr('id') + "&thumb=" + $element
                            .attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-pencil');
                            }
                        },
                        success: function(html) {
                            $('body').append(
                                '<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' +
                                html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });
                $('#button-clear').on('click', function() {
                    $element.find('img').attr('src', $element.find('img').attr(
                        'data-placeholder'));
                    $element.parent().find('input').val('');
                    $element.popover('destroy');
                });
            });

        }

    };

}();

jQuery(document).ready(function() {
    SubCategoriesAddEdit.init();
});
</script>
@endpush
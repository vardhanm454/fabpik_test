@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')

<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"></h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">

                <div class="tabbable-bordered">
                    @if(isset($columns))
                    <h4> <strong> Column Modification History </strong> </h4>
                    <table class="table dt-responsive table-checkable">
                        <thead>
                            <tr role="row" class="heading">
                                <th>Modified By</th>
                                <th>Modified at</th>
                                <th>From Name</th>
                                <th>To Name</th>
                                <th>From Status</th>
                                <th>To Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                            @if(!is_null($columnHistories))
                                @foreach($columnHistories as $columnhistory)
                                <tr>
                                    <td>{{$columnhistory->modifiedUser->name}}</td>
                                    <td>{{date( 'd-m-Y', strtotime($columnhistory->created_at) )}}</td>
                                    <td>{{$columnhistory->from_name}}</td>
                                    <td>{{$columnhistory->to_name}}</td>
                                    <td>{{($columnhistory->from_status == 1) ? 'Active' : 'In-Active'}}</td>
                                    <td>{{($columnhistory->to_status == 1) ? 'Active' : 'In-Active'}}</td>
                                </tr>
                                @endforeach
                            @endif
                        
                        </tbody>
                    </table>
                    @endif
                </div>

                <div class="form-actions text-right">
                    <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                </div>
                
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var ColumnAddEdit = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.layoutcolumns')}}";
            });
        }
    };

}();

jQuery(document).ready(function() {
ColumnAddEdit.init();
});
</script>
@endpush
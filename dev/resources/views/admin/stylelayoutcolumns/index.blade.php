@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<style>
.modal {
    text-align: center;
    padding: 0 !important;
}

.modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
}

.modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
}
.modal-dialog {
    width: 400px;
}

</style>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title bg-info pd-15">
                <div class="filter-actions">
                <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Name:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fname" id="fname"
                                            value="{{request()->fname}}" autocomplete="off">
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Action:</strong> </label>
                                    <div class="col-md-9">                                    
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('admin.layoutcolumns')}}" title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-container">
                        <div class="table-actions-wrapper">

                            <a href="#" class="btn btn-default btn-sm tooltips" data-toggle="modal" data-placement="top" data-original-title="Add New Column" data-target="#layoutColumnsStatus"><strong><span aria-hidden="true" class="icon-plus text-primary"></span>&nbsp;Add</strong></a>
                            
                            <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete Brand" data-action="soft-delete"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;Delete</strong></a>

                        {{-- <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-placement="top" data-original-title="Column Active/In-Active" data-toggle="tooltip" data-action="change-column-status"><strong><span aria-hidden="true" id="btn-confirm" class="text-danger"></span>&nbsp;Change Status</strong></a> --}}

                        </div>
                        <table class="table dt-responsive table-checkable" id="dt_listing">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="10">
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable"
                                                data-set="#sample_2 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th width=""> Sr. No. </th>
                                    <th width=""> Column Name </th>
                                    <th width=""> Created By </th>
                                    <th width=""> Created At </th>
                                    <th width=""> Modified By </th>
                                    <th width=""> Modified At </th>
                                    <th width="" class="text-center"> Status </h>
                                    <th width="" class="text-center"> References </h>
                                    <th width="" class="text-center"> Actions </th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Modal -->
<div class="modal fade" id="layoutColumnsStatus" tabindex="-1" role="dialog" aria-labelledby="layoutColumnsLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="layoutColumnsLabel">Add New Column
                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </h5>
                
            </div>
            <div class="modal-body">
                <span style="color:#27a4b0" id="successResult"></span>                        

                <div class="">
                    <label class="control-label">Column Name:<span class="required"> * </span></label>
                        <input type="text" class="form-control maxlength-handler" name="columnName" id="columnName"
                                        maxlength="100" placeholder="" value="">
                        <span style="color:#e73d4a" id="result"></span>                        
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="editLayoutColumnsStatus" tabindex="-1" role="dialog" aria-labelledby="editLayoutColumns"
    aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editLayoutColumns">Edit Column
                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </h5>
                
            </div>
            <div class="modal-body">
                <span style="color:#27a4b0" id="editSuccessResult"></span>                        

                <div class="form-group">
                    
                    <input type="hidden" value="" name="from_column_status" id="from_column_status">
                    <input type="hidden" value="" name="from_column_name" id="from_column_name">
                    <input type="hidden" value="" name="column_id" id="column_id">

                    <label class="control-label">Column Name:<span class="required"> * </span></label>
                        <input type="text" class="form-control maxlength-handler" name="editColumnName" id="editColumnName"
                                        maxlength="100" placeholder="" value="">

                        <span style="color:#e73d4a" id="editResult"></span>

                </div>
                <div class="form-group">
                    <label class="control-label">Status:<span class="required"> * </span></label>
                    <select name="to_status" id="to_status" class="form-control input-sm">
                        @foreach([1=>'Active',0=>'Inactive'] as $val=>$label)
                        <option value="{{$val}}" >{{$label}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-edit">Save changes</button>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Columns = function() {

    return {

        //main function to initiate the module
        init: function() {

            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

             // Do Filter
             $('#btn_submit_search').on('click', function(e){
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.layoutcolumns')}}?search=1";

                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                
                window.location.href = url;
            });

            //Edit Modal
            $(document).on("click", ".open-EditColumnModal", function (e) {
                e.preventDefault();
                var _self = $(this);
                
                $("#column_id").val(_self.data('id'));
                $("#from_column_status").val(_self.data('myvalue'));                
                $("#to_status").val(_self.data('myvalue'));
                $("#from_column_name").val(_self.data('name'));
                $("#editColumnName").val(_self.data('name'));

                $(_self.attr('href')).modal('show');
            });

            $('#modal-btn-edit').on('click',function(){
                
                var name = $('#editColumnName').val();
                var from_column_name = $('#from_column_name').val();
                var columnId = $('#column_id').val();
                var to_status = $('#to_status').val();
                var from_column_status = $('#from_column_status').val();

                if(name.trim() == '' ){

                    $('#editColumnName').focus();
                    $("#result").html("Column Name filed is required.");
                    return false;

                }else{
                    var url = "{{route('admin.layoutcolumns.edit', ['id'=> ':columnId'])}}";
                    url = url.replace(':columnId', columnId);

                    var arr = { name: name,  from_column_status:from_column_status, from_column_name:from_column_name, to_status:to_status};
              
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#modal-btn-edit').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                            if(result.success){
                                $("#editSuccessResult").html(result.message);
                                $("#editResult").html('');
                                $("#editColumnName").val('');
                                $('.modal-body').html(result.message);    
                                setTimeout(function(){ location.reload(true); }, 2000);
                                
                            }else{
                                $("#editResult").html(result.message);
                            }
                            
                            $('#modal-btn-edit').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');    
                        },
                        error:function(err){
                            // alert("error"+JSON.stringify(err));
                            alert("Something went wrong, please try again after sometime");
                            $('#modal-btn-edit').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');   
                        }
                    });
                }
            });

            //end Edit Modal

            //Add Modal 
            $('#layoutColumnsStatus').on('hidden.bs.modal', function (e) {
                $("#successResult").html(result.message);
                $("#result").html('');
                $("#columnName").val('');
            });

            $('#modal-btn-yes').on('click',function(){
                var url = "{{route('admin.layoutcolumns.add')}}";

                var name = $('#columnName').val();
                if(name.trim() == '' ){
                    $('#columnName').focus();
                    $("#result").html("Column Name filed is required.");
                    return false;
                }else{

                    var arr = { name: name };
              
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#modal-btn-yes').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                            if(result.success){
                                $("#successResult").html(result.message);
                                $("#result").html('');
                                $("#columnName").val('');
                                $('.modal-body').html(result.message);    
                                setTimeout(function(){ location.reload(true); }, 2000);
                            }else{
                                $("#result").html(result.message);
                            }
                            
                            $('#modal-btn-yes').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');    
                        },
                        error:function(err){
                            // alert("error"+JSON.stringify(err));
                            alert("Something went wrong, please try again after sometime");
                            $('#modal-btn-yes').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');   
                        }
                    });
                }
            });

            //End Add Modal

            // Delete Columns
            $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                swal('Column', result.message, ((result.success) ? "success" : "error"));
                                setTimeout(function(){ location.reload(true); }, 1000);
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });

        }

    };

}();

jQuery(document).ready(function() {
    Columns.init();
});
</script>
@endpush
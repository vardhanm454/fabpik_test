@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')

<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"></h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                        @if(isset($columns))
                        <form class="form-horizontal" action="{{route('admin.layoutcolumns.edit',['id'=>$id])}}" method="post"
                        enctype="multipart/form-data">
                        @else
                        <form class="form-horizontal" action="{{route('admin.layoutcolumns.add')}}" method="post"
                        enctype="multipart/form-data">
                        @endif

                        {!! csrf_field() !!}

                        <div class="form-body">
                            <input type="hidden" value="{{isset($columns)?$columns->name:''}}" name="from_column_name">
                            <div class="form-group {{($errors->first('name'))?'has-error':''}}">
                                <label class="col-md-3 control-label">Name:<span class="required"> *
                                    </span></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control maxlength-handler" name="name"
                                        maxlength="100" placeholder="" value="{{old('name', isset($columns)?$columns->name:'')}}">
                                    <span class="help-block">{{$errors->first('name')}}</span>
                                </div>
                            </div>

                            @if(isset($columns))
                            <div class="form-group">
                                <input type="hidden" value="{{isset($columns)?$columns->status:''}}" name="from_column_status">
                                <label class="control-label col-md-3">Status:<span class="required"> * </span></label>
                                <div class="col-md-6">
                                    <select name="status" id="status" class="form-control input-sm">
                                        @foreach([1=>'Active',0=>'Inactive'] as $val=>$label)
                                        <option value="{{$val}}" {{(old('status', (isset($columns))?$columns->status:1)==$val)?'selected="selected"':''}}>{{$label}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                            @if(isset($columns))
                            <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
                            @endif
                        </div>
                    </form>
                </div>

                <div class="tabbable-bordered">
                    @if(isset($columns))
                    <h4> <strong> Column Modification History </strong> </h4>
                    <table class="table dt-responsive table-checkable">
                        <thead>
                            <tr role="row" class="heading">
                                <th>Modified By</th>
                                <th>Modified at</th>
                                <th>From Name</th>
                                <th>To Name</th>
                                <th>From Status</th>
                                <th>To Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                            @if(!is_null($columnHistories))
                                @foreach($columnHistories as $columnhistory)
                                <tr>
                                    <td>{{$columnhistory->modifiedUser->name}}</td>
                                    <td>{{date( 'd-m-Y', strtotime($columnhistory->created_at) )}}</td>
                                    <td>{{$columnhistory->from_name}}</td>
                                    <td>{{$columnhistory->to_name}}</td>
                                    <td>{{($columnhistory->from_status == 1) ? 'Active' : 'In-Active'}}</td>
                                    <td>{{($columnhistory->to_status == 1) ? 'Active' : 'In-Active'}}</td>
                                </tr>
                                @endforeach
                            @endif
                        
                        </tbody>
                    </table>
                    @endif
                </div>
                
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var ColumnAddEdit = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.layoutcolumns')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });
        }
    };

}();

jQuery(document).ready(function() {
ColumnAddEdit.init();
});
</script>
@endpush
@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css"></script>
@endpush

@section('content')
<style>
.file-front {
  visibility: hidden;
  position: absolute;
}

.file-back {
  visibility: hidden;
  position: absolute;
}

.file-right {
  visibility: hidden;
  position: absolute;
}

.file-left {
  visibility: hidden;
  position: absolute;
}

.select2-results__option:before {
    content: "";
    display: inline-block;
    position: relative;
    height: 20px;
    width: 20px;
    border: 2px solid #e9e9e9;
    border-radius: 4px;
    background-color: #fff;
    margin-right: 20px;
    vertical-align: middle;
}

.select2-results__option[aria-selected=true]:before {
    font-family:fontAwesome;
    content: "\f00c";
    color: #fff;
    background-color: #f77750;
    border: 0;
    display: inline-block;
    padding-left: 3px;
}

.select2-container--default .select2-results__option[aria-selected=true] {
    background-color: #fff;
}

.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: #eaeaeb;
    color: #272727;
}

.select2-container--default .select2-selection--multiple {
    margin-bottom: 10px;
}

.select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
    border-radius: 4px;
}

.select2-container--default.select2-container--focus .select2-selection--multiple {
    border-color: #f77750;
    border-width: 2px;
        }

 .select2-container--default .select2-selection--multiple {
     border-width: 2px;
 }

.select2-container--open .select2-dropdown--below {
    border-radius: 6px;
    box-shadow: 0 0 10px rgba(0,0,0,0.5);
}

.select2-selection .select2-selection--multiple:after {
    content: 'hhghgh';
}

</style>
<div class="row brands-wrap">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">                    
                    @if(!isset($styleLayout))
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.stylelayout.add')}}" method="post" enctype="multipart/form-data">
                    @else
                    <form class="form-horizontal form-row-seperated" action="{{route('admin.stylelayout.edit', ['id'=>$id])}}" method="post" enctype="multipart/form-data">
                    @endif
                        {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            <div class="{{($errors->first('title'))?'has-error':''}}">
                                                <label class="control-label">Title:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="title" maxlength="100" placeholder="" value="{{old('title', (isset($styleLayout) ? $styleLayout->title : ''))}}">
                                                    <span class="help-block">{{$errors->first('title')}}</span>
                                                </div>
                                            </div>                 
                                        </div>

                                        <div class="col-md-6">
                                            <div class="{{($errors->first('category'))?'has-error':''}}">
                                                <label class="control-label">Category:<span class="required"> * </span></label>
                                                <div class="">
                                                    <select name="category" id="category" class="form-control form-filter input-sm select2">
                                                        <option value="">Select Category</option>
                                                        @php 
                                                            $categories = App\Models\Category::where('status',1)->whereNull('deleted_at')->selectRaw("id, title")->get();
                                                        @endphp
                                                                                                              
                                                        @foreach($categories as $cat)
                                                        <option value="{{$cat->id}}" {{ ( old('category', isset($styleLayout) ? $styleLayout->category_id : '') == $cat->id ) ? 'selected' : '' }} > {{$cat->title}}</option>
                                                        @endforeach

                                                    </select>
                                                    <span class="help-block">{{$errors->first('category')}}</span>
                                                </div>
                                            </div>                 
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="{{($errors->first('columns'))?'has-error':''}}">
                                                <label class="control-label">Columns:<span class="required"> * </span></label>
                                                <div class="">

                                                    <select id="columns" name="columns[]" class="form-control form-filter input-sm select2" multiple>
                                                        <option value="">Select Column</option>
                                                        @php 
                                                            $columndata = App\Models\LayoutColumn::where('status',1)->whereNull('deleted_at')->selectRaw("id, name")->get();
                                                        @endphp

                                                        @foreach ($columndata as $key => $data)
                                                            @if (old('columns'))
                                                                <option value="{{$data->id}}" {{ in_array($data->id, old('columns')) ? 'selected' : '' }} > {{ $data->name }}</option>
                                                            @elseif(isset($styleLayout) && !is_null($styleLayout->columns))
                                                               <option value="{{$data->id}}" {{ in_array($data->id, json_decode($styleLayout->columns)) ? 'selected' : '' }} > {{ $data->name }}</option>
                                                            @endif
                                                        @endforeach
                                                        
                                                    </select>
                                                    <span class="help-block">{{$errors->first('columns')}}</span>
                                                </div>
                                            </div>                 
                                        </div>

                                    </div>
                                    @php 
                                    $images = array();
                                    if(isset($styleLayout))
                                        $images = json_decode($styleLayout->images);
                                    @endphp
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label"><strong>Images:</strong></label>
                                        </div>
                                    </div>
                                    <!-- front Image -->
                                    <div class="row" style="margin-top: 20px;">
                                        <div class="col-md-6">
                                            <div class="col-md-8">
                                                <div class="{{($errors->first('frontImage'))?'has-error':''}}">
                                                    <label class="control-label"><strong>Front View:<span class="required"> * </span> </strong></label>
                                                    <input type="file" name="imgfront" class="file-front" accept="image/*" id="front-file">
                                                    <div class="input-group my-3" style="display:flex;">
                                                        <input type="text" class="form-control" disabled placeholder="Upload File" id="file-front">
                                                        <div class="input-group-append">
                                                            <button type="button" class="browse btn btn-primary">Browse...</button>
                                                        </div>
                                                    </div>
                                                    <span class="help-block">{{$errors->first('frontImage')}}</span>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <img src="{{ (!empty($images)  && !is_null($images->front)) ? url($images->front) : url('uploads/no-image.png') }}" id="preview-front" class="img-thumbnail" style="width: 100px;height:100px;" >
                                                <input type="hidden" value="{{ (!empty($images)  && !is_null($images->front)) ? $images->front : '' }}" name="frontImage" id="frontImage">
                                            </div>
                                        </div>
                                        <!-- back Image -->
                                        <div class="col-md-6">
                                            <div class="col-md-8">
                                                <label class="control-label"><strong>Back View:</strong></label>
                                                <input type="file" name="imgback" class="file-back" accept="image/*"  id="back-file">
                                                <div class="input-group my-3" style="display:flex;">
                                                    <input type="text" class="form-control" disabled placeholder="Upload File" id="file-back">
                                                    <div class="input-group-append">
                                                        <button type="button" class="browse-back btn btn-primary">Browse...</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <img src="{{ (!empty($images)  && !is_null($images->back)) ? url($images->back) : url('uploads/no-image.png') }}" id="preview-back" class="img-thumbnail" style="width: 100px;height:100px;" >
                                                <input type="hidden" value="{{ (!empty($images)  && !is_null($images->back)) ? $images->back : '' }}" name="backImage" id="backImage">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- right Image -->
                                    <div class="row" style="margin-top: 20px;">
                                        <div class="col-md-6">
                                            <div class="col-md-8">
                                                <label class="control-label"><strong>Right Side View:</strong></label>
                                                <input type="file" name="imgright" class="file-right" accept="image/*"  id="right-file">
                                                <div class="input-group my-3" style="display:flex;">
                                                    <input type="text" class="form-control" disabled placeholder="Upload File" id="file-right">
                                                    <div class="input-group-append">
                                                        <button type="button" class="browse-right btn btn-primary">Browse...</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <img src="{{ (!empty($images) && !is_null($images->right)) ? url($images->right) : url('uploads/no-image.png') }}"  id="preview-right" class="img-thumbnail" style="width: 100px;height:100px;" >
                                                <input type="hidden" value="{{ (!empty($images) && !is_null($images->right)) ? $images->right : '' }}" name="rightImage" id="rightImage">
                                            </div>
                                        </div>
                                        
                                        <!-- left Image -->
                                        <div class="col-md-6">
                                            <div class="col-md-8">
                                                <label class="control-label"><strong>Left Side View:</strong></label>
                                                <input type="file" name="imgleft" class="file-left" accept="image/*"  id="left-file">
                                                <div class="input-group my-3" style="display:flex;">
                                                    <input type="text" class="form-control" disabled placeholder="Upload File" id="file-left">
                                                    <div class="input-group-append">
                                                        <button type="button" class="browse-left btn btn-primary">Browse...</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <img src="{{ (!empty($images)  && !is_null($images->left)) ? url($images->left) : url('uploads/no-image.png') }}" id="preview-left" class="img-thumbnail" style="width: 100px;height:100px;" >
                                                <input type="hidden" value="{{ (!empty($images)  && !is_null($images->left)) ? $images->left : '' }}" name="leftImage" id="leftImage">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                            
                            @if(isset($styleLayout))
                            <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
                            @endif

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var LayoutCoulmn = function() {

    return {

        //main function to initiate the module
        init: function() {

            $(document).on("click", ".browse", function() {
                var file = $(this).parents().find(".file-front");
                file.trigger("click");
            });

            $('#front-file').change(function(e) {
                var fileName = e.target.files[0].name;
                $("#file-front").val(fileName);
                $("#frontImage").val(fileName);

                var reader = new FileReader();
                reader.onload = function(e) {
                    // get loaded data and render thumbnail.
                    document.getElementById("preview-front").src = e.target.result;
                };
                // read the image file as a data URL.
                reader.readAsDataURL(this.files[0]);
            });
            
            //Back Image
            $(document).on("click", ".browse-back", function() {
                var file = $(this).parents().find(".file-back");
                file.trigger("click");
            });

            $('#back-file').change(function(e) {
                var fileName = e.target.files[0].name;
                $("#file-back").val(fileName);
                $("#backImage").val(fileName);

                var reader = new FileReader();
                reader.onload = function(e) {
                    // get loaded data and render thumbnail.
                    document.getElementById("preview-back").src = e.target.result;
                };
                // read the image file as a data URL.
                reader.readAsDataURL(this.files[0]);
            });

            //right Image
            $(document).on("click", ".browse-right", function() {
                var file = $(this).parents().find(".file-right");
                file.trigger("click");
            });

            $('#right-file').change(function(e) {
                var fileName = e.target.files[0].name;
                $("#file-right").val(fileName);
                $("#rightImage").val(fileName);

                var reader = new FileReader();
                reader.onload = function(e) {
                    // get loaded data and render thumbnail.
                    document.getElementById("preview-right").src = e.target.result;
                };
                // read the image file as a data URL.
                reader.readAsDataURL(this.files[0]);
            });

            //left Image
            $(document).on("click", ".browse-left", function() {
                var file = $(this).parents().find(".file-left");
                file.trigger("click");
            });

            $('#left-file').change(function(e) {
                var fileName = e.target.files[0].name;
                $("#file-left").val(fileName);
                $("#leftImage").val(fileName);

                var reader = new FileReader();
                reader.onload = function(e) {
                    // get loaded data and render thumbnail.
                    document.getElementById("preview-left").src = e.target.result;
                };
                // read the image file as a data URL.
                reader.readAsDataURL(this.files[0]);
            });


            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('admin.stylelayout')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

            $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
                var $element = $(this);
                var $popover = $element.data('bs.popover');

                e.preventDefault();
                $('a[data-toggle="image"]').popover('destroy');
                if ($popover) {
                    return;
                }
                $element.popover({
                    html: true,
                    placement: 'bottom',
                    trigger: 'manual',
                    content: function() {
                        return '<button type="button" id="button-image" class="btn btn-outline btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-outline btn-danger"><i class="fa fa-trash-o"></i></button>';
                    }
                });
                $element.popover('show');
                $('#button-image').on('click', function() {
                    var $button = $(this);
                    var $icon = $button.find('> i');
                    $('#modal-image').remove();
                    $.ajax({
                        url: "{{ route('filemanager') }}" + "?&target=" + $element
                            .parent().find('input').attr('id') + "&thumb=" + $element
                            .attr('id'),
                        dataType: 'html',
                        beforeSend: function() {
                            $button.prop('disabled', true);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                            }
                        },
                        complete: function() {
                            $button.prop('disabled', false);
                            if ($icon.length) {
                                $icon.attr('class', 'fa fa-pencil');
                            }
                        },
                        success: function(html) {
                            $('body').append(
                                '<div id="modal-image" class="modal fade modal-scroll" tabindex="-1" data-replace="true">' +
                                html + '</div>');
                            $('#modal-image').modal('show');
                        }
                    });
                    $element.popover('destroy');
                });
                $('#button-clear').on('click', function() {
                    $element.find('img').attr('src', $element.find('img').attr(
                        'data-placeholder'));
                    $element.parent().find('input').val('');
                    $element.popover('destroy');
                });
            });

            //Search For category
            $('#category').select2({ 
                allowClear:true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.categoryAutoComplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (categoryArr) {
                    return categoryArr.text;
                },
            });

            //Search for All Columns
            $('#columns').select2({ 
                allowClear:true,
                minimumInputLength: 0,
                placeholder: 'Search Columns',
                closeOnSelect: false,
                ajax: {
                    url: "{{route('ajax.layoutColumnAutoComplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (ColumnArr) {
                                return {
                                    text: ColumnArr.text,
                                    id: ColumnArr.id,
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (ColumnArr) {
                    return ColumnArr.text;
                },
            });
        }
    };

}();

jQuery(document).ready(function() {
LayoutCoulmn.init();
});
</script>
@endpush
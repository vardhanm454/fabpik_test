@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Name:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fname" id="fname"
                                            value="{{request()->fname}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label col-md-3 pd-tb-5"><strong>Category:</strong></label>
                                    <div class="col-md-9">
                                        <select name="fcategory" id="fcategory" class="form-control form-filter input-sm select2">
                                            <option value="">Select Category</option>
                                            @if(request()->fcategory)
                                            @php 
                                                $categories = App\Models\Category::where('status',1)->whereNull('deleted_at')->selectRaw("id, title")->get();
                                            @endphp
                                            @foreach($categories as $cat)
                                                <option value="{{$cat->id}}" {{ ( request()->fcategory == $cat->id ) ? 'selected' : '' }} > {{$cat->title}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Action:</strong> </label>
                                    <div class="col-md-9">                                    
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('admin.stylelayout')}}" title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <a href="{{route('admin.stylelayout.add')}}" class="btn btn-default btn-sm tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Add New Size Chart"><strong><span aria-hidden="true" class="icon-plus text-primary"></span>&nbsp;Add</strong></a>

                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Delete Size Chart" data-action="soft-delete"><strong><span aria-hidden="true" class="icon-trash text-danger"></span>&nbsp;Delete</strong></a>

                        <!-- <a href="javascript:;" id="btn_table_export" class="btn btn-default btn-sm tooltips" data-container="body" data-placement="top" data-original-title="Export to Excel"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a> -->
                    </div>
                    <table class="table table-bordered table-checkable nowrap" id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="10%" class="text-center"> Name </th>
                                <th width="10%" class="text-center"> Category</th>
                                <th width="10%" class="text-center"> Columns </th>
                                <th width="10%" class="text-center"> Created by </th>
                                <th width="10%" class="text-center"> Created at </th>
                                <th width="10%" class="text-center"> Updated by </th>
                                <th width="10%" class="text-center"> Updated at </th>
                                <th width="10%" class="text-center"> Actions </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Sizechart = function () {

    return {

        //main function to initiate the module
        init: function () {

            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

            // Do Filter
            $('#btn_submit_search').on('click', function(e){
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('admin.stylelayout')}}?search=1";

                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                if($('#fcategory').val() != '') url += "&fcategory="+encodeURIComponent($('#fcategory').val());

                window.location.href = url;
            });

             //Search For category
             $('#fcategory').select2({ 
                allowClear:true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.categoryAutoComplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (categoryArr) {
                    return categoryArr.text;
                },
            });

            // Delete Size Chart
            $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                // alert($(this).attr('del-url'));
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                // swal('Style Layout', 'Record Deleted Successfully', "success");
                                // location.reload();

                                swal('Style Layout', result.message, ((result.success) ? "success" : "error"));
                                setTimeout(function(){ location.reload(true); }, 1000);
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });
        }

    };

}();

jQuery(document).ready(function() {    
    Sizechart.init();
});
</script>
@endpush
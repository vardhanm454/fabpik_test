<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com"> @push('PAGE_ASSETS_CSS')
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet"> @endpush <title>Invoice</title>
    <style>
        /* -------------------------------------
            INLINED WITH htmlemail.io/inline
            ------------------------------------- */
        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
            ------------------------------------- */
        .seller-support li {
            color: #7f7f7f;
            font-size: 12px;
            margin: 0;
            padding: 0;
            font-weight: 400;
        }

        body {
            margin: 0;
            padding: 0;
            font-family: 'Lato', sans-serif !important;
        }

        /* .seller-support li a {
            color: #7f7f7f;
            font-size: 12px;
            margin: 0;
            padding: 0;
            font-weight: 400;
        }

        .seller-support li span {
            font-weight: 600;
            color: #000;
            width: 150px;
        }

        .sold-block .label {
            font-weight: 600;
            color: #000;
            font-size: 12px;
        }


        .sold-block {
            padding-left: 20px;
        }

        .sold-block .soldby {
            font-weight: 600;
            color: #000;
            font-size: 16px;
            margin-bottom: 0;
        }

        .sold-block span {
            font-weight: 600;
            color: #000;
            font-size: 12px;
        }

        .sold-block ul {
            padding-left: 0;
        }

        .sold-block li {
            list-style-type: none;
            color: #000;
            font-size: 12px;
            margin: 0;
            padding: 0;
            font-weight: 400;
        }

        .seller-name {
            text-transform: capitalize
        }

        .table-dynamic-data th {
            padding: 12px 2px;
        }


        .table-dynamic-data tbody.table-data-hs tr:nth-child(odd) {
            background: #f3f3f5;
        }

        .table-dynamic-data tbody.table-data-hs tr:nth-child(odd) {
            background: #fff;
        }

        .class-grandtotal label {
            font-weight: 600;
            color: #000;
            font-size: 15px;
            padding-left: 10px;
        }



        .address-title {
            text-decoration: underline;
            display: block
        }

        .signature .signature-text {
            display: block;
            font-size: 16px;
            color: #000;
        }

        .signature .sign {
            display: block;
            text-align: center;
            font-size: 14px;
        } */

       .invoice-title {
    font-size: 1.285rem;
    margin-bottom: 1rem;
    font-weight: 700;
    line-height: 1.2;
    color: #5E5873;

}
.class-discount label {
            font-weight:400;


            padding-left: 10px;
        }

        .class-discount  span
        { font-weight:700;
            color: #000;
        }

        .class-grandtotal span {
            padding-right: 30px;
        }

        .class-discount span {
            padding-right: 30px;
        }
body
{
    background:#ebeaef
}
.table td, .table th {
    padding: .72rem 2rem;
    vertical-align: middle;
    color: #6E6B7B;
}
.invoice_details li
{
    list-style-type:none;
    font-size: 13px;
    font-weight: 700;
    line-height: 18px;
    color: #6E6B7B;
}
.invoice_details span
{
    list-style-type:none;
    font-size: 14px;
    font-weight: 400;

}
.table td, .table th {
    padding: .72rem;
    vertical-align: top;
    border-top: 1px solid #EBE9F1;
}
.invoice-block li {

            color: #6E6B7B !important;
            font-size: 13px !important;
            text-align: left;
            font-weight: 400;
            list-style-type:none;

        }
        .invoice-block ul {

        padding-left:0;

      }
.invoice-number {
    font-weight: 600;
}
.total-table td
{
font-size:12px;
}
.soldby
{
    font-weight: 500;
    line-height: 1.2;
    color: #5E5873;
}
.table-data-hs
{
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.45;
    color: #6E6B7B;
    background-color: #F8F8F8;
}
table
{
    width:100%;
}
th strong
{
    font-size:12px;
}
th
{
    font-size:9px;
}
.invoice-div label
{
    font-weight: 400;
    line-height: 1.2;
    color: #5E5873;
    font-size:18px;
    padding-left:155px;
}
.invoice-div
{

}
.invoice-div span
{
    font-weight:700;
    line-height: 1.2;
    color: #5E5873;
}

.logo-div-image
{
   position:absolute;
   top:-30px;
   left:-22px;
}
    </style>
</head>

<body class="" style="background-color: #fff;  -webkit-font-smoothing: antialiased; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapase; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
        <tr>
            <td class="container" style=" font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 750px; width: 750px;">
                <!-- START CENTERED WHITE CONTAINER -->
                <table class="main" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width:700px;max-width: 700px;  border-radius: 3px;">
                    <!-- START MAIN CONTENT AREA -->
                    <tr >
                        <td class="wrapper" >
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapase; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                <table  cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="6cm">
                                            <div class="logo-div-image">
                                            <img src="{{ __common_asset('img/logo1.png') }}" style="height:80px;poisition:absolute;top:10px" />

                                        </div>

                                        </td>
                                        <td width="4cm" class="invoice-div">
                                           {{--<label>Invoice No : <span> {{$orderDetails->invoice_no}}</span> </label>--}}
                                           <label>Invoice No : <span> {{ !is_null($orderDetails->invoice_no) ? ($orderDetails->user_invoice_prefix.''.$orderDetails->invoice_no) : '' }}</span> </label>

                                        </td>

                                    </tr>
                                </table>
                            </table>
                        </td>
                    </tr>


                    <tr  >
                        <td class="wrapper" style="border-bottom:1px dashed #DFE3E7;">
                            <table border="0" cellpadding="0" cellspacing="0" style="">
                                <table style="" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="4cm" style="vertical-align:top">
                                        <div class="invoice-block">
                                               <div style="box-shadow: -8px 12px 18px 0 rgba(25, 42, 70, 0.13);text-align:left;width:250px;">
                                               <ul>
                                                        <li> <span class="soldby">SOLD BY</span></li>
                                                        <li class="seller-name">{{$orderDetails->seller->company_name}}</li>
                                                        <li>{{$orderDetails->seller->company_address}}</li>
                                                        <li> {{$orderDetails->seller->company_state}}</li>
                                                        <li> {{$orderDetails->seller->company_pincode}}</li>

                                                        <li><span>Email   &nbsp; : </span>{{$orderDetails->seller->email}}</li>
                                                        <li><span>GSTIN : </span>{{$orderDetails->seller->gst_no}}</li>
                                                    </ul>
                                                </div>

                                                </div>
                                                <div>
                                        </td>

                                        <td width="4cm" style="vertical-align:top">
                                            <div>
                                                <div style="box-shadow: -8px 12px 18px 0 rgba(25, 42, 70, 0.13);padding-left:230px; text-align:left;vertical-align:top;width:220px;" class="invoice_details">
                                                    <ul>

                                                        <li><span>Invoice Date</span> : &nbsp; {{date('d-m-Y', strtotime($orderDetails->created_at))}}</li>
                                                        <li><span>Order No &nbsp; &nbsp; </span> : &nbsp; {{$orderDetails->child_order_id}}</li>
                                                        <li><span>Order Date &nbsp;</span> : &nbsp; {{date('d-m-Y', strtotime($orderDetails->created_at))}} </li>
                                                    </ul>
                                                </div>
                                                <div>
                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </td>
                    </tr>
                    <tr style="">
                        <td class="">
                            <table border="0"  style="vertical-align:top">
                                <table style="" >
                                    <tr>
                                        <td width="6cm" style="vertical-align:top">
                                        <div class="invoice-block" style="vertical-align:top">
                                               <div style="box-shadow: -8px 12px 18px 0 rgba(25, 42, 70, 0.13);text-align:left;width:350px;">
                                               <ul>
                                                        <li> <span class="soldby">BILLING ADDRESS</span></li>
                                                        <li class="seller-name">{{$orderDetails->order->billing_first_name }} {{$orderDetails->order->billing_last_name}}</li>
                                                        <li> {{$orderDetails->order->billing_address1}} </li>
                                                        <li> {{$orderDetails->order->billing_address2}}</li>
                                                        <li> {{$orderDetails->order->billing_state}}</li>
                                                        <li> {{$orderDetails->order->billing_pincode}}</li>
                                                        <li><span>Email &nbsp; : </span>{{$orderDetails->order->billing_email}}</li>
                                                        <li><span>GSTIN : </span></li>
                                                    </ul>
                                                </div>

                                                </div>
                                                <div>
                                        </td>
                                        <td width="6cm" style="vertical-align:top">
                                        <div class="invoice-block" style="vertical-align:top">
                                               <div style="box-shadow: -8px 12px 18px 0 rgba(25, 42, 70, 0.13);text-align:left;padding-left:100px;">
                                               <ul>
                                                        <li> <span class="soldby">SHIPPING ADDRESS</span></li>
                                                        <li class="seller-name">{{$orderDetails->order->shipping_first_name }} {{$orderDetails->order->shipping_last_name}}</li>
                                                        <li><span class="address-title">Address 1:</span> {{$orderDetails->order->shipping_address1}}</li>
                                                        <li> <span class="address-title">Address 2:</span>{{$orderDetails->order->shipping_address2}}</li>
                                                        <li> {{$orderDetails->order->shipping_city}}</li>
                                                        <li> {{$orderDetails->order->shipping_state}}</li>
                                                        <li> {{$orderDetails->order->shipping_pincode}}</li>
                                                        <li><span>Email &nbsp; : </span> {{$orderDetails->order->shipping_email}}</li>
                                                        <li><span>GSTIN : </span>{{$orderDetails->order->shipping_mobile}}</li>
                                                    </ul>
                                                </div>

                                                </div>
                                                <div>
                                        </td>

                                    </tr>
                                </table>
                            </table>
                        </td>
                    </tr>
                    <tr class="table-dynamic-data">
                        <td class="wrapper" style=" font-size: 14px; vertical-align: top; box-sizing: border-box;">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapase; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                <table style="width:auto;margin:0 auto;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <table class="order-products-table">
                                            <thead style="   background:#e8eaf6;">
                                                <tr class="border-bottom">
                                                    <th  width="5cm" style="text-align: left;font-weight:400;padding: .72rem 0.5rem;width:200px; color: #6E6B7B;border-radius:10px;"><strong>Description</strong></th>
                                                    <th  style="text-align: center;font-weight:400;padding: .72rem 0.5rem; color: #6E6B7B;"><strong>MRP</strong><br />(per item)</th>
                                                    <th  style="text-align: center;font-weight:400;padding: .72rem 0.5rem; color: #6E6B7B;"><strong>Selling Price</strong><br />(per item)</th>

                                                    <th  style="text-align: center;font-weight:400;padding: .72rem 0.5rem; color: #6E6B7B;"><strong>QTY</strong></th>
                                                    <th  style="text-align: center;font-weight:400;padding: .72rem 0.5rem; color: #6E6B7B;"><strong>Discount</strong><br />(%)</th>
                                                    <th  style="text-align: center;font-weight:400;padding: .72rem 0.5rem; color: #6E6B7B;"><strong>Total</strong></th>
                                                </tr>
                                            </thead>
                                            <tbody class="strong table-data-hs">
                                                <tr class="" >
                                                    @php

                                                    $unitPrice = $orderDetails->mrp - $orderDetails->tax;
                                                    $saleAmount = $orderDetails->price*$orderDetails->quantity;
                                                    $total = ($orderDetails->price*$orderDetails->quantity) - ($orderDetails->tax);

                                                    $singleUnitPriceWithOutTax = $orderDetails->price - ($orderDetails->tax/$orderDetails->quantity)

                                                    @endphp

                                                    <td width="7cm" style="width:300px;text-align:left;padding: .72rem 0.5rem;text-transform:capitalize;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;padding:0 8px;font-size: 12px;">
                                                        {{ strtolower($orderDetails->productVarient->name)}}
                                                        <br> @if(isset($attributeOptions)) @foreach($attributeOptions as $options) <b>{{$options['atrributeName']}}</b> : {{$options['atrributeValue']}}<br> @endforeach @endif
                                                    </td>

                                                    <td  width="1.5cm" style="text-align: center;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;font-size: 12px;">
                                                    {{ $orderDetails->mrp }}

                                                    </td>
                                                    <td   width="2.5cm" style="text-align: center;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;font-size: 12px;">
                                                    {{ round($singleUnitPriceWithOutTax, 0) }}
                                                    </td>

                                                    <td width="1cm" style="text-align: center;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;font-size: 12px;">
                                                        {{$orderDetails->quantity}}
                                                    </td>
                                                    <td width="2cm"  style="text-align: center;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;font-size: 12px;">
                                                    @if(isset($orderDetails->deal_price))
                                                        @php
                                                            $discountINR = (($orderDetails->mrp - $orderDetails->price) * $orderDetails->quantity);
                                                        @endphp
                                                        {{ +number_format((float) (($discountINR*100)/$orderDetails->mrp), '1')}}
                                                    @else
                                                        {{ +number_format((float) (($orderDetails->total_seller_discount*100)/$orderDetails->mrp), '1')}}
                                                    @endif
                                                    </td>
                                                    <td  style="text-align: center;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;font-size: 12px;">
                                                        {{round($singleUnitPriceWithOutTax*$orderDetails->quantity, 0)}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </tr>
                                </table>
                            </table>
                        </td>
                    </tr>
                    <tr class="background:#000;position:relative;">
                        <td class="wrapper total-table" style=" font-size: 14px; vertical-align: top; box-sizing: border-box;">
                            <table border="0" cellpadding="0" cellspacing="0" style="background:#f1f1f1;">
                                <table style="background:#f1f1f;width:300px;max-width:300px;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <table class="border: none; margin-top: 20px;width:300px;max-width:300px;background:#000; " class="order-products-table">
                                            <tbody class="strong table-data-hs">
                                                <tr class="class-discount">
                                                    <td colspan="4" style="width:500px;text-align:right;background:transparent">
                                                    </td>
                                                    <td colspan="1" style="text-align:right;background:transparent">
                                                    </td>
                                                    <td width="5cm" style="text-align:left;padding:3px 2px;background:#fff">
                                                        <label>SubTotal </label> <span style="float:right"> {{round($singleUnitPriceWithOutTax*$orderDetails->quantity, 0)}} </span>
                                                    </td>

                                                </tr>
                                                @if($orderDetails->order->billing_state_id == $orderDetails->seller->company_state)
                                                <tr class="class-discount">
                                                    <td colspan="4" style="width:500px;text-align:right;background:transparent">
                                                    </td>
                                                    <td colspan="1" style="text-align:right;background:transparent">
                                                    </td>
                                                    <td width="5cm" style="text-align:left;padding:3px 2px;background:#fff">
                                                        <label>SGST </label> <span style="float:right">{{round($orderDetails->sgst, 0)}}</span>
                                                    </td>
                                                </tr>
                                                <tr class="class-discount">
                                                    <td colspan="4" style="width:500px;text-align:right;background:transparent">
                                                    </td>
                                                    <td colspan="1" style="text-align:right;background:transparent">
                                                    </td>
                                                    <td width="5cm" style="border-bottom:0.5px solid #d1d2d4;text-align:left;padding:3px 2px;background:#fff">
                                                        <label>CGST </label> <span style="float:right">{{round($orderDetails->cgst, 0)}}</span>
                                                    </td>
                                                </tr>
                                                @else
                                                <tr class="class-discount">
                                                    <td colspan="4" style="width:500px;text-align:right;background:transparent">
                                                    </td>
                                                    <td colspan="1" style="text-align:right;background:transparent">
                                                    </td>
                                                    <td width="5cm" style="border-bottom:0.5px solid #d1d2d4;text-align:left;padding:3px 2px;background:#fff">
                                                        <label>IGST </label> <span style="float:right">{{round($orderDetails->tax, 0)}}</span>   <!--tax showing like igst-->
                                                    </td>
                                                </tr>
                                                @endif
                                                <tr class="class-grandtotal">
                                                    <td colspan="4" style="width:500px;text-align:right;background:transparent">
                                                    </td>
                                                    <td colspan="1" style="text-align:right;background:transparent">
                                                    </td>
                                                    <td width="5cm" style="border-bottom:0.5px solid #d1d2d4;text-align:left;padding:3px 2px;background:#fff">
                                                        <label> Grand Total </label> <span style="float:right">
                                                        @if(isset($orderDetails->deal_price))
                                                        {{round( (( ($orderDetails->mrp*$orderDetails->quantity) - ((($orderDetails->mrp - $orderDetails->price) * $orderDetails->quantity)))), 2 )}}
                                                        @else
                                                        {{round( (( ($orderDetails->mrp*$orderDetails->quantity) - $orderDetails->total_seller_discount)), 2 )}}
                                                        @endif

                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </tr>
                                </table>
                            </table>
                        </td>
                    </tr>
                    <tr>
                       <td style="padding-bottom:50px; ">
                    </td>
                    </tr>
                    <tr  style="text-align:center;padding-top:20px; ">
                        <td  style="border-bottom:1px dashed #DFE3E7;border-top:1px dashed #DFE3E7;padding:5px 5px ">
                        <div class="sold-block">
                                                <div style="box-shadow: -8px 12px 18px 0 rgba(25, 42, 70, 0.13); color: #000;">
                                                   <label style="font-size:17px;width:100%;   color: #6E6B7B;">Thanks for your order</label>
                                                </div>
                                                <div>
                        </td>
                    </tr>
                </table>
                <!-- END MAIN CONTENT AREA -->
    </table>
    <!-- END CENTERED WHITE CONTAINER -->
    </td>
    </tr>
    </table>

</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com"> @push('PAGE_ASSETS_CSS')
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet"> @endpush <title>Invoice</title>
    <style>
        /* -------------------------------------
            INLINED WITH htmlemail.io/inline
            ------------------------------------- */
        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
            ------------------------------------- */
        .seller-support li {
            color: #7f7f7f;
            font-size: 12px;
            margin: 0;
            padding: 0;
            font-weight: 400;
        }

        body {
            margin: 0;
            padding: 0;
            font-family: 'Lato', sans-serif !important;
        }


        .invoice-title {
            font-size: 1.285rem;
            margin-bottom: 1rem;
            font-weight: 700;
            line-height: 1.2;
            color: #5E5873;
        }

        .class-discount label {
            font-weight: 400;
            padding-left: 10px;
        }

        .class-discount span {
            font-weight: 700;
            color: #000;
        }

        .commission-div .class-discount label {
            font-weight: 400;
            padding-left: 0px;
        }

        .class-grandtotal span {
            padding-right: 30px;
        }

        .class-discount span {
            padding-right: 30px;
        }

        body {
            background: #ebeaef
        }

        .table td,
        .table th {
            padding: .72rem 2rem;
            vertical-align: middle;
            color: #6E6B7B;
        }

        .invoice_details li {
            list-style-type: none;
            font-size: 13px;
            font-weight: 700;
            line-height: 18px;
            color: #6E6B7B;
        }

        .invoice_details span {
            list-style-type: none;
            font-size: 14px;
            font-weight: 400;
        }

        .table td,
        .table th {
            padding: .72rem;
            vertical-align: top;
            border-top: 1px solid #EBE9F1;
        }

        .invoice-block li {
            color: #6E6B7B !important;
            font-size: 13px !important;
            text-align: left;
            font-weight: 400;
            list-style-type: none;
        }

        .invoice-block ul {
            padding-left: 0;
        }

        .invoice-number {
            font-weight: 600;
        }

        .total-table td {
            font-size: 12px;
        }

        .soldby {
            font-weight: 500;
            line-height: 1.2;
            color: #5E5873;
        }

        .table-data-hs {
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.45;
            color: #6E6B7B;
            background-color: #F8F8F8;
        }

        table {
            width: 100%;
        }

        th strong {
            font-size: 12px;
        }

        th {
            font-size: 9px;
        }

        .invoice-div label {
            font-weight: 400;
            line-height: 1.2;
            color: #5E5873;
            font-size: 18px;
            position:absolute;
            top:50px;
        }

        .invoice-div {}

        .invoice-div span {
            font-weight: 700;
            line-height: 1.2;
            color: #5E5873;
        }

        .logo-div-image {
            position: relative;
            top: -10px;
            left: -22px;
        }
    </style>
</head>

<body class="" style="background-color: #fff;  -webkit-font-smoothing: antialiased; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapase; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
        <tr>
            <td class="container" style=" font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 750px; width: 750px;">
                <!-- START CENTERED WHITE CONTAINER -->
                <table class="main" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width:700px;max-width: 700px;  border-radius: 3px;">
                    <!-- START MAIN CONTENT AREA -->

                    <tr>
                        <td class="wrapper" style="border-bottom:1px dashed #DFE3E7;">
                            <table border="0" cellpadding="0" cellspacing="0" style="">
                                <table style="" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="4cm" style="vertical-align:top">

                                            <div style="box-shadow: -8px 12px 18px 0 rgba(25, 42, 70, 0.13);text-align:left;width:250px;" class="invoice-div">
                                                <div class="logo-div-image">
                                                    <img src="{{ __common_asset('img/logo1.png') }}" style="height:80px;poisition:absolute;top:10px" />
                                                </div>
                                                {{--<label>Invoice No : <span> {{$orderDetails->order->invoice_prefix}}  {{$orderDetails->order->parent_order_id}}</span> </label>--}}
                                                <label>Invoice No : <span> {{!is_null($orderDetails->seller_invoice_no) ? ($orderDetails->seller_invoice_prefix.''.$orderDetails->seller_invoice_no) : ''}}</span> </label>
                                            </div>
                                        </td>
                                        <td width="4cm" style="vertical-align:top">
                                            <div>
                                                <div style="box-shadow: -8px 12px 18px 0 rgba(25, 42, 70, 0.13);padding-left:150px; text-align:left;vertical-align:top;width:300px;" class="invoice_details">
                                                    <ul>
                                                        <li><span>Invoice Date</span> : &nbsp; {{date('d-m-Y', strtotime($orderDetails->created_at))}}</li>
                                                        <li><span>Order No &nbsp; &nbsp; </span> : &nbsp; {{$orderDetails->child_order_id}}</li>
                                                        <li><span>Order Date &nbsp;</span> : &nbsp; {{date('d-m-Y', strtotime($orderDetails->created_at))}} </li>
                                                    </ul>
                                                </div>
                                                <div>
                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </td>
                    </tr>
                    <tr style="">
                        <td class="">
                            <table border="0" style="vertical-align:top">
                                <table style="">
                                    <tr>
                                        <td width="6cm" style="vertical-align:top">
                                            <div class="invoice-block" style="vertical-align:top">
                                                <div style="box-shadow: -8px 12px 18px 0 rgba(25, 42, 70, 0.13);text-align:left;width:250px;">
                                                    <ul>
                                                        <li> <span class="soldby">Bill From:</span></li>
                                                        <li class="seller-name">FABPIK ECOMM LLP</li>
                                                        <li> 2ND FLOOR, VSSR SQUARE, PLOT NO. 64 & 64A, VITTAL RAO NAGAR, MADHAPUR, </li>
                                                        <li> Hyderabad, Telangana - 500081</li>
                                                        <li><span>GSTIN : {{FABPIK_GSTIN}}</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div>
                                        </td>
                                        <td width="6cm" style="vertical-align:top">
                                            <div class="invoice-block" style="vertical-align:top">
                                                <div style="box-shadow: -8px 12px 18px 0 rgba(25, 42, 70, 0.13);text-align:left;padding-left:10px;">
                                                    <ul>
                                                        @php $state = App\Models\State::find($orderDetails->seller->company_state); @endphp
                                                        <li> <span class="soldby">Bill To:</span></li>
                                                        <li class="seller-name">{{$orderDetails->seller->company_name}}</li>
                                                        <li>{{$orderDetails->seller->company_address}},</li>
                                                        <li> {{($state)?$state->name:''}}, {{$orderDetails->seller->company_pincode}}</li>
                                                        <li><span>GSTIN : </span> {{($orderDetails->seller->gst_no)? $orderDetails->seller->gst_no: ''}}</li>
                                                        <li><span>TAN &nbsp; : </span> {{isset($orderDetails->seller->tan)? $orderDetails->seller->tan : ''}}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div>
                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </td>
                    </tr>
                    <tr class="table-dynamic-data">
                        <td class="wrapper" style=" font-size: 14px; vertical-align: top; box-sizing: border-box;">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapase; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                <table style="width:auto;margin:0 auto;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <table class="order-products-table">
                                            <thead style="   background:#e8eaf6;">
                                                <tr class="border-bottom">
                                                    <th width="5cm" style="text-align: left;font-weight:400;padding: .72rem 0.5rem;width:200px; color: #6E6B7B;border-radius:10px;"><strong>Description</strong></th>
                                                    <th style="text-align: center;font-weight:400;padding: .72rem 0.5rem; color: #6E6B7B;"><strong>MRP</strong><br />(per item)</th>
                                                    <th style="text-align: center;font-weight:400;padding: .72rem 0.5rem; color: #6E6B7B;"><strong>Selling Price</strong><br />(per item)</th>
                                                    <th style="text-align: center;font-weight:400;padding: .72rem 0.5rem; color: #6E6B7B;"><strong>QTY</strong></th>
                                                    <th style="text-align: center;font-weight:400;padding: .72rem 0.5rem; color: #6E6B7B;"><strong>Discount</strong><br />(%)</th>
                                                    <th style="text-align: center;font-weight:400;padding: .72rem 0.5rem; color: #6E6B7B;"><strong>Total</strong></th>
                                                </tr>
                                            </thead>
                                            <tbody class="strong table-data-hs">
                                                <tr class="">
                                                    @php

                                                    /**
                                                        * if the order created data is less than 07-july-2021, then show the price cloumns
                                                        * order created date is more than 07-july-2021, then show the seller_price cloumn
                                                    */

                                                    $unitPrice = (strtotime('2021-07-07 00:00:00') > strtotime($order->created_at)) ? $order->price : $order->seller_price;

                                                    $finalCommissionPercentage = ($orderDetails->commission/($unitPrice*$orderDetails->quantity))*100;

                                                    $subtotal = $orderDetails->commission + $orderDetails->order_handling_charge + $orderDetails->shipping_charge;

                                                    if($orderDetails->seller->company_state == 36)
                                                    {
                                                        $finalSgst = $orderDetails->commission_sgst + $orderDetails->shipping_charge_sgst + $orderDetails->order_handling_charge_sgst;
                                                        $finalCgst = $orderDetails->commission_cgst + $orderDetails->shipping_charge_cgst + $orderDetails->order_handling_charge_cgst;
                                                        $finalIgst = 0.0;
                                                    } else {
                                                        $finalSgst = 0.0;
                                                        $finalCgst = 0.0;
                                                        $finalIgst = $orderDetails->commission_igst + $orderDetails->shipping_charge_igst + $orderDetails->order_handling_charge_igst;
                                                    }

                                                    $finaltotal = $subtotal + $finalCgst + $finalSgst + $finalIgst;
                                                @endphp

                                                <td width="7cm" style="width:300px;text-align:left;padding: .72rem 0.5rem;text-transform:capitalize;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;padding:0 8px;font-size: 12px;">
                                                        {{isset($orderDetails->product->name)?ucwords(strtolower($orderDetails->product->name)):''}}
                                                        <br> @if(isset($attributeOptions)) @foreach($attributeOptions as $options) <b>{{$options['atrributeName']}}</b> : {{$options['atrributeValue']}}<br> @endforeach @endif
                                                    </td>
                                                    <td width="1.5cm" style="text-align: center;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;font-size: 12px;">
                                                        {{$orderDetails->mrp}}
                                                    </td>
                                                    <td width="2.5cm" style="text-align: center;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;font-size: 12px;">
                                                        {{$unitPrice}}
                                                    </td>
                                                    <td width="1cm" style="text-align: center;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;font-size: 12px;">
                                                        {{$orderDetails->quantity}}
                                                    </td>
                                                    <td width="2cm" style="text-align: center;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;font-size: 12px;">
                                                        @php
                                                        $discount = $orderDetails->mrp - $unitPrice;
                                                        @endphp
                                                        {{ +number_format((float) ( ($discount/$orderDetails->mrp)*100 ), 2 ) }}
                                                    </td>
                                                    <td style="text-align: center;border-top: 1px solid #e7ecf1;line-height: 1.42857;vertical-align: top;font-size: 12px;">
                                                        {{$unitPrice*$orderDetails->quantity}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </tr>
                                </table>
                            </table>
                        </td>
                    </tr>
                    <tr style="">
                        <td class="commission-div" width="3cm">
                            <table border="0" style="vertical-align:top;background:#fff; margin-top: 3px;">
                                <table style="">
                                    <tr>
                                        <td class="wrapper total-table" style=" font-size: 14px; vertical-align: top; box-sizing: border-box;">
                                            <span class="soldby" style="width:100%">Commission Fees</span>
                                            <table border="0" cellpadding="0" cellspacing="0" style="background:#fff;">
                                                <table style="background:#fff;" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <table style="border: none; background:#fff;" class="order-products-table">
                                                            <tbody class="strong table-data-hs" style="background:#fff;">
                                                                <tr class="class-discount">
                                                                    <td width="3cm" style="text-align:left;">
                                                                        <label>Base Commission Rate (%):</label> <span style="float:right"> {{$orderDetails->seller->commission}}</span>
                                                                    </td>
                                                                </tr>
                                                                <tr class="class-discount">
                                                                    <td width="3cm" style="text-align:left;">
                                                                        <label>Final Commission Rate (%):</label> <span style="float:right"> {{number_format((float)$finalCommissionPercentage, '1')}}</span>
                                                                    </td>
                                                                </tr>
                                                                <tr class="class-discount">
                                                                    <td width="3cm" style="text-align:left;">
                                                                        <label>Final Commision Fee (INR): </label> <span style="float:right"> {{number_format((float)$orderDetails->commission, '1')}}</span>
                                                                    </td>
                                                                </tr>
                                                                @if(!is_null($orderDetails->order_handling_charge) && $orderDetails->order_handling_charge > 0)
                                                                <tr class="class-discount">
                                                                    <td width="3cm" style="text-align:left;">
                                                                        <label>Handling Charges:</label> <span style="float:right"> {{$orderDetails->order_handling_charge}}</span>
                                                                    </td>
                                                                </tr>
                                                                @endif

                                                                @if(!is_null($orderDetails->shipping_charge) && $orderDetails->shipping_charge>0)
                                                                <tr class="class-discount">
                                                                    <td width="3cm" style="text-align:left;">
                                                                        <label>Shipping Charges - ({{isset($orderDetails->shipping_weight)?( $orderDetails->shipping_weight*1000 ):''}} Gms):</label> <span style="float:right"> {{$orderDetails->shipping_charge}}</span>
                                                                    </td>
                                                                </tr>
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </tr>
                                                </table>
                                            </table>
                                        </td>
                                        <td width="9cm" class="wrapper total-table" style=" font-size: 14px; vertical-align: top; box-sizing: border-box;">
                                            <span class="soldby" style="width:100%; text-align: center;">Commission Summary</span>
                                            <table border="0" cellpadding="0" cellspacing="0" style="background:#f1f1f1;">
                                                <table style="background:#f1f1f;width:300px;max-width:300px;" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <table class="border: none; margin-top: 20px;width:300px;max-width:300px;background:#000; " class="order-products-table">
                                                            <tbody class="strong table-data-hs">
                                                                <tr class="class-discount">
                                                                    <td colspan="4" style="width:500px;text-align:right;background:transparent">
                                                                    </td>
                                                                    <td colspan="1" style="text-align:right;background:transparent">
                                                                    </td>
                                                                    <td width="5cm" style="text-align:left;padding:3px 2px;background:#fff">
                                                                        <label>Sub Total </label> <span style="float:right"> {{ +number_format((float)($orderDetails->commission + $orderDetails->order_handling_charge + $orderDetails->shipping_charge) , '1') }}</span>
                                                                    </td>
                                                                </tr>
                                                                @if($orderDetails->seller->company_state == 36)
                                                                <tr class="class-discount">
                                                                    <td colspan="4" style="width:500px;text-align:right;background:transparent">
                                                                    </td>
                                                                    <td colspan="1" style="text-align:right;background:transparent">
                                                                    </td>
                                                                    <td width="5cm" style="text-align:left;padding:3px 2px;background:#fff">
                                                                        <label>SGST </label> <span style="float:right"> {{ +number_format ((float) $finalSgst, '1')}}</span>
                                                                    </td>
                                                                </tr>
                                                                <tr class="class-discount">
                                                                    <td colspan="4" style="width:500px;text-align:right;background:transparent">
                                                                    </td>
                                                                    <td colspan="1" style="text-align:right;background:transparent">
                                                                    </td>
                                                                    <td width="5cm" style="text-align:left;padding:3px 2px;background:#fff">
                                                                        <label>CGST </label> <span style="float:right"> {{ +number_format ((float) $finalCgst, '1')}}</span>
                                                                    </td>
                                                                </tr>
                                                                @else
                                                                <tr class="class-discount">
                                                                    <td colspan="4" style="width:500px;text-align:right;background:transparent">
                                                                    </td>
                                                                    <td colspan="1" style="text-align:right;background:transparent">
                                                                    </td>
                                                                    <td width="5cm" style="border-bottom:0.5px solid #d1d2d4;text-align:left;padding:3px 2px;background:#fff">
                                                                        <label>IGST </label> <span style="float:right"> {{ +number_format ((float) $finalIgst, '1')}}</span>
                                                                    </td>
                                                                </tr>
                                                                @endif
                                                                <tr class="class-grandtotal">
                                                                    <td colspan="4" style="width:500px;text-align:right;background:transparent">
                                                                    </td>
                                                                    <td colspan="1" style="text-align:right;background:transparent">
                                                                    </td>
                                                                    <td width="5cm" style="border-bottom:0.5px solid #d1d2d4;text-align:left;padding:3px 2px;background:#fff">
                                                                        <label> Grand Total </label> <span style="float:right"> <b>{{ +number_format ((float) $finaltotal, '1') }}</b></span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </tr>
                                                </table>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom:100px; ">
                        </td>
                    </tr>
                    <tr style="text-align:center;padding-top:80px; ">
                        <td style="border-bottom:1px dashed #DFE3E7;border-top:1px dashed #DFE3E7;padding:5px 5px ">
                            <div class="sold-block">
                                <div style="box-shadow: -8px 12px 18px 0 rgba(25, 42, 70, 0.13); color: #000;">
                                    <label style="font-size:17px;width:100%;   color: #6E6B7B;">Thanks for your business</label>
                                </div>
                                <div>
                        </td>
                    </tr>
                </table>
                <!-- END MAIN CONTENT AREA -->
    </table>
    <!-- END CENTERED WHITE CONTAINER -->
    </td>
    </tr>
    </table>
</body>

</html>
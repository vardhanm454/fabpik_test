@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ __common_asset('global/plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet"
    type="text/css" />
@endpush

@section('content')

<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"></h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="row">
                <div class="col-md-12">
                        <div class="form">
                            <table class="table table-striped table-bordered table-hover" id="">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="10%" class="pd-tb-10 text-center"> # </th>
                                        <th width="70%" class="pd-tb-10 text-center"> Values </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($attributeValues) >= 1)
                                    @foreach($attributeValues as $index => $attrValue)
                                    @php
                                    $color = $class = '';
                                    if(!is_null($attrValue->colour_code)) {
                                        $class .= "badge badge-default margin-bottom-5";
                                        $color .= "background:".$attrValue->colour_code;
                                    }
                                    @endphp
                                    <tr>
                                        <td class="text-center">{{++$index}}</td>
                                        <td class="text-center">
                                            <div class="">
                                                <span class="{{$class}}" style="{{$color}};">&nbsp;</span>
                                                {{$attrValue->option_name}}
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="3" class="text-center">No records!</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/jquery-minicolors/jquery.minicolors.min.js') }}" type="text/javascript">
</script>
<script src="{{ __common_asset('pages/scripts/components-color-pickers.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var AttributeOptions = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('seller.attributes')}}";
            });
        }
    };
}();

jQuery(document).ready(function() {
    AttributeOptions.init();
});
</script>
@endpush
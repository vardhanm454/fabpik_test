@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')

<!-- BEGIN PAGE TITLE-->
<!-- <h1 class="page-title"> {{$title}}</h1> -->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->

@if(Session::has('wlcm_msg'))
<div class="note note-success">
    <p> Welcome back! </p>
</div>
@endif
<style>

  
  
  .cookie-popup {
    
    font-size: 0.875rem;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    position: fixed;
    bottom: -6.25rem;
    left: 1.25rem;
    padding: 0.625rem 0.9375rem;
    box-shadow: 0 0 0.625rem 0 rgba(0,0,0, .15);
    line-height: 150%;
    transition: opacity .5s;
    opacity: 0;
  }
  .cookie-popup--short {
    right: none;
    width: 33.875rem;
  }
  .cookie-popup--dark {
    z-index: 999;
    background: #fff !important;
    background-clip: border-box;
    border-radius: 10px;
    margin-bottom: 4.3rem;
    box-shadow: 0 5px 15px 5px rgb(80 102 224 / 8%);
    border: 1px solid #cac7f6;
    box-shadow: 0 1px 1px #9ccae8;
  }
  .cookie-popup--not-accepted {
    opacity: 1;
    animation: cookie-popup-in .5s ease forwards;  
  }
  .cookie-popup--accepted {
    opacity: 0;
  }
  .cookie-popup a {
    color: skyblue;
  }
  .cookie-popup a:visited {
    color: skyblue;
    text-decoration: none;
  }
  .cookie-popup-actions {
    flex: 1;
    text-align: right;
    justify-content: flex-end;
  }
  .cookie-popup-actions button {
    margin-top: 10px;
    /* background: none; */
    font-family: inherit;
    font-style: inherit;
    font-size: inherit;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 11px !important;
    padding: 0px 10px !important;
    margin-left: 12px;
    height: 23px;
    cursor: pointer;
    line-height: 23px !important;
  }
  .cookie-popup-actions button:hover {
    text-decoration: underline;
  }
  
  @keyframes cookie-popup-in {
    from { bottom: -6.25rem; }
    to { bottom: 1.25rem; }
  }
  

  .cookie-popup i
  {
 
    width: 35px !important;
    height: 35px !important;
    justify-content: center;
    align-items: center;
    padding: 8px;
    display: inline-flex;
    border-radius: 50%;
    font-size: 15px;
    color: #FF5B5C;
    background-color: rgba(255, 91, 92, 0.17);
    color: #FF5B5C !important;
    margin-right: 11px;
  }

  .cookie-popup  h3
  {
      
    font-size: 14px;
    font-weight: 600;
    line-height: 20px;
    display: flex;
    margin: 0;

  }


  .cookie-popup p
  {
    font-size: 12px;
    line-height: 19px;
    margin-bottom: 0;
    margin: 0;
  }

.d-flex
{
    display:flex
}
</style>
<!-- Top Blocks -->

   
  
   <div class="cookie-popup cookie-popup--short  cookie-popup--dark">
     
   <div class="d-flex">

<div class="">
   <i class="icon-bell"></i> 
 </div> 
 
 <div> 
     <h3>
    FabPik would like to send you updates.
     </h3>
     <p>
       Click <b>ALLOW</b> to get notified on orders, deals & offers.
     </p></div>
   </div>
     <div class="cookie-popup-actions d-flex justify-content-end">

         <button class="btn btn blue text-uppercase" id="allow">Allow</button>
       <button class="btn  btn-default" id="not_allow">Ask Me Later</button>

     </div>
 
   </div>


@if(Auth::user()->system_generated_password == 'y')   
<div class="row">
    <div class="col-md-12">
        <div class="note note-warning">
            <p class="margin-bottom-10">
             <b>System generated passwords are too weak. Please update your password for security purpose.</b>
            </p>
        </div>
    </div>
</div>
@endif

   

<div class="row">
    <div class="col-md-3 margin-bottom-10">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{$ordersToday}} </div>
                <div class="desc"> Orders Today </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{$TotalOrders}} </div>
                <div class="desc"> Total Orders </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-shopping-cart fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{number_format((float)$totalSalesToday, 1, '.', ',')}} </div>
                <div class="desc">  Today Sale value </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{number_format((float)$totalSales, 1, '.', ',')}} </div>
                <div class="desc"> Total Sale Value </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 margin-bottom-10">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{$ordersToShip}} </div>
                <div class="desc"> Products to Ship </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{$ordersDelivered}} </div>
                <div class="desc"> Total Products Delivered </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-shopping-cart fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{number_format((float)$payoutAmount, 1, '.', ',')}} </div>
                <div class="desc">  Total Payout Amount </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-tags fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> {{number_format((float)$totalPendingAmount, 1, '.', ',')}} </div>
                <div class="desc"> Pending Payout Amount </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-blue"></i>
                    <span class="caption-subject font-blue bold">Orders</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#order_today_stats" data-toggle="tab"> Today </a>
                    </li>
                    <li>
                        <a href="#order_last7_stats" id="statistics_orders_weekly_tab" data-toggle="tab"> Last 7 Days </a>
                    </li>
                    <li>
                        <a href="#order_sixmonths_stats" id="statistics_orders_sixmonths_tab" data-toggle="tab"> Last 6 Months </a>
                    </li>
                </ul>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="order_today_stats">
                        <div id="order_today_chart" class="chart"> </div>
                    </div>
                    <div class="tab-pane" id="order_last7_stats">
                        <div id="order_weekly_chart" class="chart"> </div>
                    </div>
                    <div class="tab-pane" id="order_sixmonths_stats">
                        <div id="order_sixmonths_chart" class="chart"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <!-- Begin: life time stats -->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-blue"></i>
                    <span class="caption-subject font-blue bold">Overview</span>
                    <span class="caption-helper">report overview...</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#top_selling_products" data-toggle="tab"> Top Selling </a>
                        </li>
                        <li>
                            <a href="#featured" data-toggle="tab"> Featured </a>
                        </li>
                        <li class="dropdown">
                            <a href="#latest_orders" data-toggle="tab"> Latest Orders </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="top_selling_products">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th> Product Name </th>
                                            <th> Sold </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($topselling as $items)
                                        <tr>
                                            <td> {{$items->name}} </td>
                                            <td> {{$items->total_sold}} </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="featured">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th> Product Name </th>
                                            <th> MRP </th>
                                            <th> Sale Price </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($featured as $items)
                                        <tr>
                                            <td> {{$items->name}} </td>
                                            <td> {{$items->mrp}} </td>
                                            <td> {{$items->sell_price}} </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="latest_orders">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th> Customer Name </th>
                                            <th> Total Orders </th>
                                            <th> Total Amount </th>
                                            <th> Status</th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($latestOrders as $items)
                                        <tr>
                                            <td> {{$items->name}} </td>
                                            <td> {{date('d-m-Y', strtotime($items->created_at))}} </td>
                                            <td> {{$items->total}} </td>
                                            <td> {{$items->seller_text}} </td>
                                            <td> <a href="{{route('seller.orders.view', ['id'=>$items->id])}}" class="font-dark">View</a> </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    <div class="col-md-6">
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-bubbles font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Support Tickets</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <!-- BEGIN: Comments -->
                    @foreach($ticket_details as $details)
                        <div class="mt-comments">
                            <div class="mt-comment">
                                <div class="mt-comment-img">
                                    {{-- <img src="{{__common_asset('pages/media/users/avatar1.jpg')}}" /> --}}
                                </div>
                                <div class="mt-comment-body">
                                    <div class="mt-comment-info">
                                        <span class="mt-comment-author">{{$details->issue_title}}</span>
                                        <span class="mt-comment-date">{{$details->created_date}}</span>
                                    </div>
                                    <div class="mt-comment-text"> {{$details->issue}} </div>
                                    <div class="mt-comment-details">
                                        <span class="mt-comment-status mt-comment-status-pending">{{($details->issue_status==0)?'Pending':'Completed'}}</span>
                                        <ul class="mt-comment-actions">
                                            <li>
                                                <a href="{{route('seller.tickets.view', ['id'=>$details->id])}}" class="font-dark">View</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <!-- END: Comments -->


                    

                </div>
            </div>
        </div>
    </div>
</div>

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase.js"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Dashboard = function() {

    function showChartTooltip(x, y, xValue, yValue) {
        $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
            position: 'absolute',
            display: 'none',
            top: y - 40,
            left: x - 40,
            border: '0px solid #ccc',
            padding: '2px 6px',
            'background-color': '#fff'
        }).appendTo("body").fadeIn(200);
    }

    var orderTodayChart = function() {
        
        var data = [
            @foreach ($todayOrdersStats as $i=>$stats)
                [ "{{ $stats['day'] }}", "{{ $stats['orders'] }}" ], 
            @endforeach
        ];

        console.log(data);

        // var data = [
        //     ['07/2020', 1223],
        //     ['08/2020', 1365],
        //     ['01/2020', 4243],
        //     ['02/2020', 342],
        //     ['03/2020', 1034],
        //     ['04/2020', 326],
        //     ['05/2020', 2125],
        //     ['06/2020', 324],
        // ];

        var plot_statistics = $.plot(
            $("#order_today_chart"), [{
                data: data,
                lines: {
                    fill: 0.6,
                    lineWidth: 0
                },
                color: ['#f89f9f']
            }, {
                data: data,
                points: {
                    show: true,
                    fill: true,
                    radius: 5,
                    fillColor: "#f89f9f",
                    lineWidth: 3
                },
                color: '#fff',
                shadowSize: 0
            }], {

                xaxis: {
                    tickLength: 0,
                    tickDecimals: 0,
                    mode: "categories",
                    min: 2,
                    font: {
                        lineHeight: 15,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                yaxis: {
                    ticks: 3,
                    tickDecimals: 0,
                    tickColor: "#f0f0f0",
                    font: {
                        lineHeight: 15,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                grid: {
                    backgroundColor: {
                        colors: ["#fff", "#fff"]
                    },
                    borderWidth: 1,
                    borderColor: "#f0f0f0",
                    margin: 0,
                    minBorderMargin: 0,
                    labelMargin: 20,
                    hoverable: true,
                    clickable: true,
                    mouseActiveRadius: 6
                },
                legend: {
                    show: false
                }
            }
        );

        var previousPoint = null;

        $("#order_today_chart").bind("plothover", function(event, pos, item) {
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                    showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1]);
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });

    }

    var orderWeeklyChart = function() {

        var data = [
            @foreach ($ordersStats as $i=>$stats)
                [ '{{ $stats["day"] }}', '{{ $stats["orders"] }}' ], 
            @endforeach
        ];

        console.log(data);

        // var data = [
        //     ['01/00/2020', 10],
        //     ['02/00/2020', 10],
        //     ['01/01/2020', 10],
        //     ['02/01/2020', 0],
        //     ['03/01/2020', 10],
        //     ['04/01/2020', 12],
        //     ['05/01/2020', 212],
        //     ['06/01/2020', 324],
        //     ['07/01/2020', 122]
        // ];

        var plot_statistics = $.plot(
            $("#order_weekly_chart"), [{
                data: data,
                lines: {
                    fill: 0.6,
                    lineWidth: 0
                },
                color: ['#BAD9F5']
            }, {
                data: data,
                points: {
                    show: true,
                    fill: true,
                    radius: 5,
                    fillColor: "#BAD9F5",
                    lineWidth: 3
                },
                color: '#fff',
                shadowSize: 0
            }], {

                xaxis: {
                    tickLength: 0,
                    tickDecimals: 0,
                    mode: "categories",
                    min: 2,
                    font: {
                        lineHeight: 14,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                yaxis: {
                    ticks: 3,
                    tickDecimals: 0,
                    tickColor: "#f0f0f0",
                    font: {
                        lineHeight: 14,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                grid: {
                    backgroundColor: {
                        colors: ["#fff", "#fff"]
                    },
                    borderWidth: 1,
                    borderColor: "#f0f0f0",
                    margin: 0,
                    minBorderMargin: 0,
                    labelMargin: 20,
                    hoverable: true,
                    clickable: true,
                    mouseActiveRadius: 6
                },
                legend: {
                    show: false
                }
            }
        );

        var previousPoint = null;

        $("#order_weekly_chart").bind("plothover", function(event, pos, item) {
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                    showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1]);
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });
    }

    var orderSixMonthsChart = function() {
        
        var data = [
            @foreach ($orderSixMonthStats as $i=>$stats)
                [ "{{ $stats['month'] }}", "{{ $stats['orders'] }}" ], 
            @endforeach
        ];

        console.log(data);

        // var data = [
        //     ['01/00/2020', 10],
        //     ['02/00/2020', 10],
        //     ['01/01/2020', 10],
        //     ['02/01/2020', 0],
        //     ['03/01/2020', 10],
        //     ['04/01/2020', 12],
        //     ['05/01/2020', 212],
        //     ['06/01/2020', 324],
        //     ['07/01/2020', 122]
        // ];

        var plot_statistics = $.plot(
            $("#order_sixmonths_chart"), [{
                data: data,
                lines: {
                    fill: 0.6,
                    lineWidth: 0
                },
                color: ['#BAD9F5']
            }, {
                data: data,
                points: {
                    show: true,
                    fill: true,
                    radius: 5,
                    fillColor: "#BAD9F5",
                    lineWidth: 3
                },
                color: '#fff',
                shadowSize: 0
            }], {

                xaxis: {
                    tickLength: 0,
                    tickDecimals: 0,
                    mode: "categories",
                    min: 2,
                    font: {
                        lineHeight: 14,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                yaxis: {
                    ticks: 3,
                    tickDecimals: 0,
                    tickColor: "#f0f0f0",
                    font: {
                        lineHeight: 14,
                        style: "normal",
                        variant: "small-caps",
                        color: "#6F7B8A"
                    }
                },
                grid: {
                    backgroundColor: {
                        colors: ["#fff", "#fff"]
                    },
                    borderWidth: 1,
                    borderColor: "#f0f0f0",
                    margin: 0,
                    minBorderMargin: 0,
                    labelMargin: 20,
                    hoverable: true,
                    clickable: true,
                    mouseActiveRadius: 6
                },
                legend: {
                    show: false
                }
            }
        );

        var previousPoint = null;

        $("#order_sixmonths_chart").bind("plothover", function(event, pos, item) {
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                    showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1]);
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });
    }

    return {

        //main function
        init: function() {
            
            orderTodayChart();
            
            $('#statistics_orders_weekly_tab').on('shown.bs.tab', function(e) {
                orderWeeklyChart();
            });
            
            $('#statistics_orders_sixmonths_tab').on('shown.bs.tab', function(e) {
                orderSixMonthsChart();
            });

        }

    };

}();

/* common fuctions */
function el(selector) { return document.querySelector(selector) }
function els(selector) { return document.querySelectorAll(selector) }
function on(selector, event, action) { els(selector).forEach(e => e.addEventListener(event, action)) }
function cookie(name) { 
  let c = document.cookie.split('; ').find(cookie => cookie && cookie.startsWith(name+'='))
  return c ? c.split('=')[1] : false; 
}

// /* popup button hanler */
// on('.cookie-popup button', 'click', () => {
//   el('.cookie-popup').classList.add('cookie-popup--accepted');
//   document.cookie = `cookie-accepted=true`
// });

// /* popup init hanler */
// if (cookie('cookie-accepted') !== "true") {
//   el('.cookie-popup').classList.add('cookie-popup--not-accepted');
// }

// function _reset() {
//   document.cookie = 'cookie-accepted=false'; 
//   document.location.reload();
// }

// function _switchMode(cssClass) {
//   el('.cookie-popup').classList.toggle(cssClass);
// }

if (!cookie("notification") && Notification.permission == "default") {
    el(".cookie-popup").classList.add("cookie-popup--not-accepted");
}

on(".cookie-popup #allow", "click", () => {
    // alert('allow');
    el(".cookie-popup").classList.add("cookie-popup--accepted");
    const date = new Date();
    date.setTime(date.getTime() + -1 * 24 * 60 * 60 * 1000);
    document.cookie = "notification='yes'; expires=" + date.toUTCString() + "; path=/";
    this.initFirebaseMessagingRegistration();
});

on(".cookie-popup #not_allow", "click", () => {
    // alert('not-allow');
    el(".cookie-popup").classList.add("cookie-popup--accepted");
    const date = new Date();
    date.setTime(date.getTime() + 7 * 24 * 60 * 60 * 1000);
    document.cookie = "notification='yes'; expires=" + date.toUTCString() + "; path=/";
});


var firebaseConfig = {
    apiKey: "AIzaSyA8gPW9L_WE9QK0pfROs-TN2cxwXyEZqBg",
    authDomain: "fabpik-ce8b5.firebaseapp.com",
    projectId: "fabpik-ce8b5",
    storageBucket: "fabpik-ce8b5.appspot.com",
    messagingSenderId: "618785523880",
    appId: "1:618785523880:web:a5abd2b77995f48b2d1a9d",
    measurementId: "G-35THTWY7RD",
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

function initFirebaseMessagingRegistration() {
    messaging
        .requestPermission()
        .then(function () {
            return messaging.getToken()
        })
        .then(function(token) {
            console.log(token);
   
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
  
            $.ajax({
                url: "{{route('seller.saveNotificationToken')}}?",
                type: 'POST',
                data: {
                    token: token
                },
                dataType: 'JSON',
                success: function (response) {
                    // alert('Token saved successfully.');
                },
                error: function (err) {
                    // console.log(err);
                },
            });
  
        }).catch(function (err) {
            // console.log('Catch Token Error'+ err);
        });
}

messaging.onMessage(function(payload) {
    const noteTitle = payload.notification.title;
    const noteOptions = {
        body: payload.notification.body,
        icon: payload.notification.icon,
    };
    new Notification(noteTitle, noteOptions);
});

jQuery(document).ready(function() {    
   Dashboard.init();
});
</script>
@endpush
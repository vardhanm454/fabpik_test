@extends(ADMIN_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')  

<div class="row categpries-wrap">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    <form class="form-horizontal form-row-seperated" action="{{(isset($staff))?route('seller.staffs.edit',['id'=>$id]):route('seller.staffs.create')}}" method="post">

                        {!! csrf_field() !!}

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="{{($errors->first('name'))?'has-error':''}}">
                                                <label class="control-label">Name:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="name" maxlength="100" placeholder="" value="{{old('name',isset($staff)?$staff->name:'')}}">
                                                    <span class="help-block">{{$errors->first('name')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="{{($errors->first('email'))?'has-error':''}}">
                                                <label class="control-label">Email ID:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="email" maxlength="100" placeholder="" value="{{old('email',isset($staff)?$staff->email:'')}}">
                                                    <span class="help-block">{{$errors->first('email')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="{{($errors->first('designation'))?'has-error':''}}">
                                                <label class="control-label">Designation:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="designation" maxlength="100" placeholder="" value="{{old('designation',isset($staff)?$staff->designation:'')}}">
                                                    <span class="help-block">{{$errors->first('designation')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="{{($errors->first('mobile_no'))?'has-error':''}}">
                                                <label class="control-label">Mobile No.:<span class="required"> * </span></label>
                                                <div class="">
                                                    <input type="text" class="form-control maxlength-handler" name="mobile_no" maxlength="10" placeholder="" value="{{old('mobile_no',isset($staff)?$staff->mobile_no:'')}}">
                                                    <span class="help-block">{{$errors->first('mobile_no')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3 class="form-section">Access & Permissions</h3>
                                            @if(!empty($permissions))
                                            <div class="panel-group accordion">
                                                <div class="row">
                                                    @foreach($permissions as $moduleName=>$modulePermissions)
                                                    <div class="col-md-6" style="margin-bottom: 5px;">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_{{__generateSlug($moduleName)}}"> {{ucwords($moduleName)}} </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse_{{__generateSlug($moduleName)}}" class="panel-collapse in">
                                                                <div class="panel-body" style="height:100px; overflow-y:auto;">
                                                                    <div class="row">
                                                                        @foreach($modulePermissions as $perm)
                                                                        <div class="col-md-offset-1 col-md-3 checkbox">
                                                                            <input type="checkbox" name="permissions[]" value="{{$perm->id}}" @if(in_array($perm->id, old('permissions', (isset($staff))?$assignedPerms:[]))) checked @endif > {{ucwords($perm->name)}} 
                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                    
                            @if(isset($staff))
                            <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var AddEditStaff = function () {
    return {

        //main function to initiate the module
        init: function () {
            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('seller.staffs')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });
        }
    };

}();

jQuery(document).ready(function() {    
    AddEditStaff.init();
});    
</script>
@endpush
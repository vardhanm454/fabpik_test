<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->

<head>
    <meta charset="utf-8" />
    <title>Staffs</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">
                <table class="tabletable-bordered">
                    <thead>
                        <tr style="background:#c2c2c2;">
                            <th> # </th>
                            <th> Name </th>
                            <th> Email </th>
                            <th> Designation</th>
                            <th> Mobile </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($staffs))
                        @foreach($staffs as $index => $staff)
                        <tr>
                            <td>{{++$index}}</td>
                            <td>{{$staff->name}}</td>
                            <td>{{$staff->email}}</td>
                            <td>{{$staff->designation}}</td>
                            <td>{{$staff->mobile_no}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>
@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-table/bootstrap-table.min.css' )}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
@if(isset($orderDetails))
<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="panel panel-info order-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="caption col-md-6 pd-tb-5">
                        <span class="pd-tb-10">
                            <span class="pd-tb-10"><span aria-hidden="true" class="icon-handbag"></span>
                                Order Information </span>                                
                    </div>
                    <div class="caption">
                    @php 
                        $downloadurl = route("seller.orders.downloadCustomerInvoice", ["id"=>$orderDetails->id]);
                        $actionurl = '#changeStatusModal';
                    
                    @endphp
                        <a href="{{($orderDetails->order_status_id == 9)?$downloadurl:'#'}}" class="btn btn-default btn-sm table-group-action tooltips" data-placement="top" data-original-title="Download Invoice" data-toggle="tooltip" data-action="change-status" 
                        {{($orderDetails->order_status_id == 9)?"":"disabled"}}>
                            <strong>&nbsp;Download Invoice</strong></a>
                        <a href="#" class="btn btn-default btn-sm table-group-action-status tooltips" data-placement="top" data-target="{{($orderDetails->order_status_id == 8 || $orderDetails->order_status_id == 9 || $orderDetails->order_status_id == 2)?'':$actionurl}}" data-original-title="Change Order Status" data-toggle="modal" data-action="change-status" id="change-status"
                        {{($orderDetails->order_status_id == 8 || $orderDetails->order_status_id == 9 || $orderDetails->order_status_id == 2)?"disabled":""}}>
                            <strong>&nbsp;Take Action</strong></a>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="row static-info">
                    <div class="col-md-4 name"> Order ID: </div>
                    <div class="col-md-8 value"> {{$orderDetails->child_order_id}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Order Date: </div>
                    <div class="col-md-8 value"> {{date('d-m-Y', strtotime($orderDetails->created_at))}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Order Status: </div>
                    <div class="col-md-8 value">
                        @if(isset($orderStatus))
                        @foreach($orderStatus as $status)
                        {{($status->id == $orderDetails->order_status_id)?$status->seller_text:''}}
                        @endforeach
                        @endif
                    </div>
                </div>
                <div class="row static-info">
                    {{--<div class="col-md-4 name"> Payment Method: </div>
                    <div class="col-md-8 value">
                        @foreach(['c'=>'Cash on Delivery', 'o'=>'Online'] as $val=>$label)
                        {{($val == $orderDetails->order->payment_type)?$label:''}}
                        @endforeach
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="panel panel-info customer-info">
            <div class="panel-heading">
                <div class="caption pd-tb-5">
                    <span aria-hidden="true" class="icon-handbag"></span> Order History
                </div>
            </div>
            <div class="panel-body">
            <table data-toggle="table" data-height="150" id="">
                <thead>
                    <tr>
                        <th data-field="date" data-align="center" data-sortable="true">
                            Date
                        </th>
                        <th data-field="status" data-align="center">
                            Status
                        </th>
                    </tr>
                </thead>
                <tbody>
                @if(isset($orderHistory))
                    @foreach($orderHistory as $history)
                    <tr>
                        <td>
                            {{date('d M Y h:m:i', strtotime($history->created_at))}}
                        </td>
                        <td>
                            @foreach($orderStatus as $status)
                            {!! ($status->id == $history->order_status_id)? '<b> Order Status: </b>'.$status->seller_text:'' !!}
                            @endforeach
                            <br />
                            @foreach($shippingStatus as $status)
                            {!! ($status->id == $history->shipping_status_id)? '<b> Shipping Status: </b>'.$status->seller_text:'' !!}
                            @endforeach
                        </td>
                    </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
                
            </div>
        </div>
    </div>
</div>
<div class="row">
    
    <div class="col-md-6 col-sm-12">
        <div class="panel panel-info customer-info">
            <div class="panel-heading">
                <div class="caption pd-tb-5">
                    <span aria-hidden="true" class="icon-user"></span> Customer Information
                </div>
            </div>
            <div class="panel-body">
                <div class="row static-info">
                    <div class="col-md-4 name"> Customer Name: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->first_name)?$orderDetails->order->first_name:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Email ID: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->email)?$orderDetails->order->email:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Phone No.: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->mobile)?$orderDetails->order->mobile:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Address: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->billing_address1)?$orderDetails->order->billing_address1:''}}
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Pincode: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->billing_pincode)?$orderDetails->order->billing_pincode:''}}
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="col-md-6 col-sm-12">
        <div class="panel panel-info customer-info">
            <div class="panel-heading">
                <div class="caption pd-tb-5">
                    <span aria-hidden="true" class="icon-user"></span> Shipping Information
                </div>
            </div>
            <div class="panel-body">
                <div class="row static-info">
                    <div class="col-md-4 name"> Name: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->shipping_first_name)?$orderDetails->order->shipping_first_name:''}}
                        {{isset($orderDetails->order->shipping_last_name)?$orderDetails->order->shipping_last_name:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Email ID: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->shipping_email)?$orderDetails->order->shipping_email:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Phone No.: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->shipping_mobile)?$orderDetails->order->shipping_mobile:''}} </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Address: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->shipping_address1)?$orderDetails->order->shipping_address1:''}},
                        {{isset($orderDetails->order->shipping_address2)?$orderDetails->order->shipping_address2:''}},
                        {{isset($orderDetails->order->shipping_city)?$orderDetails->order->shipping_city:''}},
                        {{isset($orderDetails->order->shipping_state)?$orderDetails->order->shipping_state:''}}
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-4 name"> Pincode: </div>
                    <div class="col-md-8 value">
                        {{isset($orderDetails->order->shipping_pincode)?$orderDetails->order->shipping_pincode:''}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="panel panel-info products-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="caption col-md-6 pd-tb-5">
                        <span class="pd-tb-10"><span aria-hidden="true" class="icon-handbag"></span> Products Ordered
                        </span>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table  table-centered">
                        <thead>
                            <tr>
                                <th> Sr. No. </th>
                                <th> Order ID </th>
                                <th> Product </th>
                                <th> SKU </th>
                                <th> MRP </th>
                                <th width="70"> Discount Amount</th>
                                <th> Selling Price </th>
                                <th> Quantity </th>
                                <th width="70"> Total Amount </th>
                                <th width="80"> Order Status </th>
                                <th width="90"> Shipping Status </th>
                                <!-- <th width="100"> Actions </th> -->
                            </tr>
                        </thead>
                        <tbody>
                        @php 
                        
                        /**
                         * if the order created data is less than 07-july-2021, then show the price cloumn
                         * order created date is more than 07-july-2021, then show the seller_price cloumn
                        */
                        $unitPrice = (strtotime('2021-07-07 00:00:00') > strtotime($orderDetails->created_at)) ? $orderDetails->price : $orderDetails->seller_price;
             
                        $total_discount = 0;
                        if(isset($orderDetails->deal_price)){
                            $total_discount = $orderDetails->mrp - $unitPrice;
                        }else{
                            $total_discount = $orderDetails->total_seller_discount;
                        }
                        @endphp
                            <tr>
                                <td>1</td>
                                <td>{{$orderDetails->child_order_id}}</td>
                                <td>{{isset($orderDetails->productVarient->name)?$orderDetails->productVarient->name:''}}<br>
                                    @if(isset($attributeOptions))
                                    @foreach($attributeOptions as $options)
                                    <b>{{$options['atrributeName']}}</b> : {{$options['atrributeValue']}}</br>
                                    @endforeach
                                    @endif
                                    
                                </td>
                                <td>{{$orderDetails->productVarient->sku}}</td>
                                <td>{{$orderDetails->mrp}}</td>
                                <td>{{$total_discount}}</td>
                                <td>{{$unitPrice}}</td>
                                <td>{{$orderDetails->quantity}}</td>
                                <td>{{($unitPrice)*$orderDetails->quantity}}</td>
                                <td>
                                    @if(isset($orderStatus))
                                    @foreach($orderStatus as $status)
                                    {{($status->id == $orderDetails->order_status_id)?$status->seller_text:''}}
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if(isset($shippingStatus))
                                    @foreach($shippingStatus as $status)
                                    {{($status->id == $orderDetails->shipping_status_id)?$status->seller_text:''}}
                                    @endforeach
                                    @endif
                                </td>
                               
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<!-- Modal -->
<div class="modal fade" id="changeStatusModal" tabindex="-1" role="dialog" aria-labelledby="changeStatusModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changeStatusModalLabel">Change Order Status!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="">
                    <form moethod="post" action="{{route( 'seller.orders.changOrderStatus', ['id'=>$orderDetails->id] )}}" id="changeStatusForm">
                        <label class="control-label">Change Status:</label>
                        <div class="">
                            <select class="form-control" name="status" id="status">
                                <option value="">Select</option>
                                @foreach(['3'=>'Rejected by Seller', '4'=>'Order Confirmed'] as $val=>$label)
                                <option value="{{$val}}"> {{$label}} </option>
                                @endforeach
                            </select>
                            <span style="color:#e73d4a" id="result"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-table/bootstrap-table.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script>
    $("#modal-btn-yes").unbind().on("click", function(){
        if($("#status option:selected").val() == ""){
            $("#result").html("Please Select Status");
            return false;
        } else {
            var id = $("#status option:selected").val();

            var $_this = $("#changeStatusModal").find("#changeStatusForm");
            var action_url = $_this.attr("action");
            var request_method = $_this.attr("method");
            var form_data = $_this.serialize();
            $('#change-status').html('loading');
            $.ajax({
                type: request_method,
                url: action_url,
                data: form_data,
                success: function(result) {
                    if(result.success == 1){
                        toastr.success( result.message, 'Success');                        
                    }else{
                        toastr.error( result.message, 'Error');
                    }
                    setTimeout(function(){ location.reload(); }, 3000);
                },
                error: function() {
                    toastr.error( 'Oops..some error occured. Please try again later.', 'Error');
                    location.reload();
                },
            });
            $("#changeStatusModal").modal('hide');
            $('#changeStatusModal').on('hidden.bs.modal', function () {
                $('#changeStatusModal form')[0].reset();
            });
        }
    });
</script>
@endpush
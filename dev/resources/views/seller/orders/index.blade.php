@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content')
<style>
    .badge{
        position: inherit;
    }

.modal {
    text-align: center;
    padding: 0 !important;
}

.modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
}

.modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
}
.odd.parent td:nth-child(5)
{
width:200px;
}
</style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                    <form action="#" method="get" class="filter-form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class=""><strong>Date:</strong> </label><br>
                                    <div class="">
                                        <div class="input-group input-daterange" >
                                            <input type="text" class="form-control date-picker datepicker input-sm" data-date="10/11/2012" data-date-format="mm/dd/yyyy" name="ffromdate" id="ffromdate" data-date-end-date="0d" placeholder="From" value="{{request()->ffromdate}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control date-picker datepicker input-sm" data-date="10/11/2012" data-date-format="mm/dd/yyyy" name="ftodate" id="ftodate" data-date-end-date="0d" placeholder="To" value="{{request()->ftodate}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class=""><strong>Order Status:</strong> </label><br>
                                    <div class="">
                                        <select name="fostatus" id="fostatus" class="form-control select2 form-filter input-sm margin-bottom-5">
                                            <option value="">Select</option>
                                            @if(isset($orderStatus))
                                            @foreach($orderStatus as $status)
                                            <option value="{{$status->id}}"
                                            {{request()->fostatus==$status->id?'selected="selected"':''}}>
                                                {{$status->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class=""><strong>Order ID:</strong> </label><br>
                                    <div class="">
                                        <input type="text" name="forderid" id="forderid" value="{{request()->forderid}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>
                                </div>   
                                <div class="col-md-3">
                                    <label class=""><strong>Product Name:</strong> </label><br>
                                    <div class="">
                                        <input type="text" name="fproductname" id="fproductname" value="{{request()->fproductname}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>                                    
                                </div>   
                                          
                            </div>                 
                            <div class="row">
                                <div class="col-md-3">
                                    <label class=""><strong>Product SKU:</strong> </label><br>
                                    <div class="">
                                        <input type="text" name="fproductsku" id="fproductsku" value="{{request()->fproductsku}}" class="form-control form-filter input-sm margin-bottom-5">                                           
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class=""><strong>Sell Price:</strong> </label><br>
                                    <div class="">
                                        <div class="input-group input-daterange" >
                                            <input type="text" class="form-control input-sm" name="fminsellprice" id="fminsellprice" placeholder="Min" value="{{request()->fminsellprice}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control input-sm" name="fmaxsellprice" id="fmaxsellprice" placeholder="Max" value="{{request()->fmaxsellprice}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class=""><strong>Action:</strong> </label><br>
                                    <div class="">
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('seller.orders')}}" title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                    <a href="javascript:;" class="btn btn-default btn-sm table-group-action-status tooltips" data-placement="top" data-original-title="Change Order Status" data-toggle="tooltip" data-action="change-status">
                            <strong><span aria-hidden="true" class="icon-settings text-primary"></span>&nbsp;Take Action</strong></a>

                        <!-- <a href="javascript:;" id="btn_table_export" class="btn btn-default btn-sm tooltips"
                            data-container="body" data-placement="top" data-original-title="Export to Excel"><strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a> -->
                    </div>
                    <table class="table table-bordered table-hover dt-responsive dataTable no-footer dtr-inline collapsed"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th class="text-center" style="width:6%;">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="" class="text-center" style="width:7%;"> Date </th>
                                <th width="" class="text-center" style="width:8%;"> Order ID </th>
                                <th width="" class="text-center product-name" style="width:15%;"> Product Name </th>
                                <th width="" class="text-center" style="width:9%;"> MRP <br/><span><small>(Price Per Unit)</small></span> </th>
                                <th width="" class="text-center" style="width:8%;"> Discount <br/><span><small>(Per Unit)</small></span> </th>
                                <th width="" class="text-center" style="width:9%;"> Selling Price <br/><span><small>(Per Unit)</small></span> </th>
                                <th width="" class="text-center" style="width:7%;"> Quantity </th>
                                <th width="" class="min-phone-l text-center" style="width:8%;"> Total </th>
                                <th width="" class="all text-center" style="width:15%;"> Order Status </th>
                                <th width="" class="all text-center" style="width:8%;"> Actions </th>
                                <th width="" class="none"> SKU: </th>
                                <th width="" class="none"> Ship to PINCODE: </th>
                                <th width="" class="none"> User Customization Text </th>

                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="changeStatusModal" tabindex="-1" role="dialog" aria-labelledby="changeStatusModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changeStatusModalLabel">Change Order Status!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="">
                    <label class="control-label">Change Status:</label>
                    <form>
                        <div class="">
                            <select class="form-control" name="status" id="status">
                                <option value="">Select</option>
                                @foreach(['4'=>'Confirm Order', '3'=>'Reject the Order'] as $val=>$label)
                                <option value="{{$val}}"> {{$label}} </option>
                                @endforeach
                            </select>
                            <span style="color:#e73d4a" id="result"></span>
                        </div>
                    </form>
                </div>

                <div class="">
                    <label class="control-label">Remark</label>
                    <div class="">
                        <textarea class="form-control" name="remark" id="remark" rows="3"></textarea>
                        <span style="color:#e73d4a" id="remark-result"></span>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Orders = function() {

    $('#changeStatusModal').on('hidden.bs.modal', function () {
        $('#changeStatusModal form')[0].reset();
    });
    return {

        //main function to initiate the module
        init: function() {

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }
            
            //Filters will apply when click the enter
            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('seller.orders')}}?search=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fostatus').val() != '') url += "&fostatus=" + encodeURIComponent($('#fostatus').val());
                if ($('#forderid').val() != '') url += "&forderid=" + encodeURIComponent($('#forderid').val());
                if ($('#fproductname').val() != '') url += "&fproductname=" + encodeURIComponent($('#fproductname').val());
                if ($('#fproductsku').val() != '') url += "&fproductsku=" + encodeURIComponent($('#fproductsku').val());
                if ($('#fminsellprice').val() != '') url += "&fminsellprice=" + encodeURIComponent($('#fminsellprice').val());
                if ($('#fmaxsellprice').val() != '') url += "&fmaxsellprice=" + encodeURIComponent($('#fmaxsellprice').val());
               
                window.location.href = url;
            });

            // Export all childorders data
            $('#btn_table_export').on('click', function(e) {
                e.preventDefault();

                var url = "{{-- route('seller.childorders.export') --}}?export=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fostatus').val() != '') url += "&fostatus=" + encodeURIComponent($('#fostatus').val());
               
                window.location.href = url;
            });

        }

    };
}();

jQuery(document).ready(function() {
    Orders.init();
});
</script>
@endpush
@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">            
                <div class="col-lg-6 col-xs-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title tabbable-line">
                            <div class="caption">
                                <i class="icon-bubbles font-dark hide"></i>
                                <span class="caption-subject font-dark bold uppercase">Conersations</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="portlet_comments_1">
                                    <!-- BEGIN: Comments -->
                                    <div class="mt-comments">
                                        <div class="mt-comment">
                                            <div class="mt-comment-img">
                                                <img src="../assets/pages/media/users/avatar1.jpg">
                                            </div>
                                            <div class="mt-comment-body">
                                                <div class="mt-comment-info">
                                                    <span class="mt-comment-author">Michael Baker </br> email@mail.com - 929057000</span>
                                                    <span class="mt-comment-date">26 Feb, 10:30AM</span>
                                                </div>
                                                <div class="mt-comment-text"> Lorem Ipsum is simply dummy text of the
                                                    printing and typesetting industry. </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <!-- END: Comments -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12 col-sm-12">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-bubble font-hide hide"></i>
                                <span class="caption-subject font-hide bold uppercase">Chats</span>
                            </div>
                        </div>
                        <div class="portlet-body" id="chats">
                            <div class="slimScrollDiv"
                                style="position: relative; overflow: hidden; width: auto; height: 525px;">
                                <div class="scroller" style="height: 525px; overflow: hidden; width: auto;"
                                    data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                                    <ul class="chats">
                                        <li class="out">
                                            <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar2.jpg">
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Lisa Wong </a>
                                                <span class="datetime"> at 20:11 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing
                                                    elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                    magna aliquam erat volutpat. </span>
                                            </div>
                                        </li>
                                        <li class="out">
                                            <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar2.jpg">
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Lisa Wong </a>
                                                <span class="datetime"> at 20:11 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing
                                                    elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                    magna aliquam erat volutpat. </span>
                                            </div>
                                        </li>
                                        <li class="in">
                                            <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar1.jpg">
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Bob Nilson </a>
                                                <span class="datetime"> at 20:30 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing
                                                    elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                    magna aliquam erat volutpat. </span>
                                            </div>
                                        </li>
                                        <li class="in">
                                            <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar1.jpg">
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Bob Nilson </a>
                                                <span class="datetime"> at 20:30 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing
                                                    elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                    magna aliquam erat volutpat. </span>
                                            </div>
                                        </li>
                                        <li class="out">
                                            <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar3.jpg">
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Richard Doe </a>
                                                <span class="datetime"> at 20:33 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing
                                                    elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                    magna aliquam erat volutpat. </span>
                                            </div>
                                        </li>
                                        <li class="in">
                                            <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar3.jpg">
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Richard Doe </a>
                                                <span class="datetime"> at 20:35 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing
                                                    elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                    magna aliquam erat volutpat. </span>
                                            </div>
                                        </li>
                                        <li class="out">
                                            <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar1.jpg">
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Bob Nilson </a>
                                                <span class="datetime"> at 20:40 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing
                                                    elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                    magna aliquam erat volutpat. </span>
                                            </div>
                                        </li>
                                        <li class="in">
                                            <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar3.jpg">
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Richard Doe </a>
                                                <span class="datetime"> at 20:40 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing
                                                    elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                    magna aliquam erat volutpat. </span>
                                            </div>
                                        </li>
                                        <li class="out">
                                            <img class="avatar" alt="" src="../assets/layouts/layout/img/avatar1.jpg">
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Bob Nilson </a>
                                                <span class="datetime"> at 20:54 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing
                                                    elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                                    magna aliquam erat volutpat. sed diam nonummy nibh euismod tincidunt
                                                    ut laoreet.
                                                </span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="slimScrollBar"
                                    style="background: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 376.023px;">
                                </div>
                                <div class="slimScrollRail"
                                    style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;">
                                </div>
                            </div>
                            <div class="chat-form">
                                <div class="input-cont">
                                    <input class="form-control" type="text" placeholder="Type a message here...">
                                </div>
                                <div class="btn-cont">
                                    <span class="arrow"> </span>
                                    <a href="" class="btn blue icn-only">
                                        <i class="fa fa-check icon-white"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
@endpush
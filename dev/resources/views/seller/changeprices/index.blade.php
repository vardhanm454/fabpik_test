@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <style>
    .badge{
        position: inherit;
    }
    </style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Price:</strong> </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm" name="fpricefrom"
                                                id="fpricefrom" placeholder="Min" value="{{request()->fpricefrom}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control input-sm" name="fpriceto"
                                                id="fpriceto" placeholder="Max" value="{{request()->fpriceto}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label col-md-3 pd-tb-5"><strong>Status:</strong></label>
                                    <div class="col-md-9">
                                        <select name="fstatus" id="fstatus" class="form-control input-sm">
                                            <option value="">All</option>
                                            <option value="1" {{request()->fstatus=='1'?'selected="selected"':''}}>
                                                Active
                                            </option>
                                            <option value="0" {{request()->fstatus=='0'?'selected="selected"':''}}>
                                                Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Name:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fname" id="fname"
                                            value="{{request()->fname}}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                        <label class="col-md-3 pd-tb-5"><strong>Date:</strong> </label>
                                        <div class="col-md-9">
                                            <div class="input-group date-picker input-daterange" data-date="10/11/2012"
                                                data-date-format="mm/dd/yyyy">
                                                <input type="text" class="form-control datepicker input-sm" name="ffromdate" id="ffromdate"
                                                    placeholder="From" value="{{request()->ffromdate}}">
                                                <span class="input-group-addon"> - </span>
                                                <input type="text" class="form-control input-sm" name="ftodate" id="ftodate" placeholder="To" value="{{request()->ftodate}}">
                                            </div>
                                        </div>
                                </div>
                                <div class="col-md-4">
                                        <label class="col-md-3 pd-tb-5"><strong>Categories:</strong> </label>
                                        
                                        <div class="col-md-9">
                                            <select class="form-control input-sm" name="fcategory" id="fcategory">
                                            @if(request()->fcategory)
                                            @php $categories = \DB::table('view_categories')->where('path_id', request()->fcategory)->first(); @endphp
                                        
                                            <option value="{{$categories->path_id}}" selected="selected">{{$categories->path_title}}</option>
                                            @endif
                                            </select>
                                        </div>

                                </div>
                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>SKU:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="fsku" id="fsku"
                                            value="{{request()->fsku}}" autocomplete="off">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                <label class="col-md-3 pd-tb-5"><strong>Unique ID:</strong> </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="funiqueid" id="funiqueid"
                                            value="{{request()->funiqueid}}" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                </div>

                                <div class="col-md-4">
                                    <label class="col-md-3 pd-tb-5"><strong>Action:</strong> </label>
                                    <div class="col-md-9">                                    
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('seller.changeprices')}}" title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span></a>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                         <a href="javascript:;" class="btn btn-default btn-sm table-group-action-status tooltips" data-placement="top" data-original-title="Change Prices" data-toggle="tooltip" data-action="change-price">
                            <strong><span aria-hidden="true" class="icon-settings text-primary"></span>&nbsp;Change Price</strong></a>
                    
                    </div>
                    <table class="table dt-responsive nowrap product-list-table text-center"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="4%" class="">&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="10%" class="text-center"> ID </th>
                                <th width="20%" class="text-center"> Name </th>
                                <th width="" class="none"> Image </th>
                                <th width="15%" class="text-center"> Category </th>
                                <th width="12%" class="text-center"> SKU </th>
                                <th width="8%" class="text-center"> MRP </th>
                                <th width="8%" class="text-center"> Sale Price </th>
                                <th width="10%" class="text-center"> Discount <br/>(INR) </th>
                                <th width="10%" class="text-center"> status </th>
                                <th width="" class="none"> Created at: </th>
                                <th width="" class="none"> Modified at: </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="changePriceModal" tabindex="-1" role="dialog" aria-labelledby="changePriceModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changePriceModalLabel">Change Prices</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="">
                    <label class="control-label">Discount <span class="required"> * </span></label>
                    <form>
                        <div class="form-group">
                            <select class="form-control" name="discount_type" id="discount_type">
                                <option value="">Select</option>
                                @foreach(['f'=>'Fixed', 'p'=>'Percentage'] as $val=>$label)
                                <option value="{{$val}}"> {{$label}} </option>
                                @endforeach
                            </select>
                            <span style="color:#e73d4a" id="discount_type_result"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Amount (On MRP) <span class="required"> * </span></label>
                            <input type="text" class="form-control" name="discount_amount" id="discount_amount" autocomplete="off">
                            <span style="color:#e73d4a" id="discount_amount_result"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var SpecialPrices = function() {
    
    $('#changePriceModal').on('hidden.bs.modal', function () {
        $('#changePriceModal form')[0].reset();
    });
    
    return {

        //main function to initiate the module
        init: function() {

            //Filters will apply when click the enter
            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

            $('#fcategory').select2({ 
                allowClear:true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.childcategories.autocomplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (categoryArr) {
                    return categoryArr.text;
                },
            });

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('seller.changeprices')}}?search=1";

                if ($('#fname').val() != '') url += "&fname=" + encodeURIComponent($('#fname').val());
                // if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fstatus').val() != '') url += "&fstatus=" + encodeURIComponent($('#fstatus').val());
                if ($('#fpricefrom').val() != '') url += "&fpricefrom=" + encodeURIComponent($('#fpricefrom').val());
                if ($('#fpriceto').val() != '') url += "&fpriceto=" + encodeURIComponent($('#fpriceto').val());
                // if ($('#ffeatured').val() != '') url += "&ffeatured=" + encodeURIComponent($('#ffeatured').val());
                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fcategory').val()) url += "&fcategory=" + encodeURIComponent($('#fcategory').val());
                if ($('#fsku').val()) url += "&fsku=" + encodeURIComponent($('#fsku').val());
                if ($('#funiqueid').val()) url += "&funiqueid=" + encodeURIComponent($('#funiqueid').val());

                window.location.href = url;
            });
        }

    };

}();

jQuery(document).ready(function() {
    SpecialPrices.init();
});
</script>
@endpush
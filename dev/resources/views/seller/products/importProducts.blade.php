@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
<style>
    .form-horizontal .form-group {margin-bottom:0;}
</style>
@endpush

@section('content')
<div class="import-product-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <span aria-hidden="true" class="icon-cloud-download"></span> Download Sample File
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form">
                        <div class="form-body">
                            <div class="note note-info">
                                <p class="margin-bottom-10">The first row in downloaded sample file should remain as it is. Please do not change the order of columns.</p>
                                <p class="margin-bottom-10">The correct column order is (Type, Name, SKU, Description, Brand, PrimaryCategory, Categories, CountryOfOrigin, DressMaterial, WashingType, IronType, NoOfItemsInPackage, ItemsinPackage, MRP, SellingPrice, Tax, Weight, Length, Breadth, Height, Returnable, Refundable, Cancelable, Featured, Stock, MininimumShippingHours, ShippingWeight, ShippingLength, ShippingBreadth, ShippingHeight, Images, Thumbnail, PrimaryAttributeValue, SecondaryAttributeValue) &amp; you must follow this.</p>
                                <p class="margin-bottom-10">Please make sure the import file is UTF-8 encoded and not saved with byte order mark (BOM).</p>
                                <p class="margin-bottom-10">The images should be uploaded in uploads/products/ folder</p>
                                {{--<p class="margin-bottom-10">System will check if the code belong to any product then will update that product otherwise will add new product.</p>--}}
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <form class="horizontal-form " action="" method="post">
                                        {!! csrf_field() !!}
                                            <div class="form-group">
                                                <label class="col-md-4 control-label pd-lr-0">Categories:<span class="required"> *
                                                    </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class="form-control input-sm select2" name="category" id="category">
                                                        <option></option>
                                                        <optgroup label="Category">
                                                            @if(isset($categories))
                                                            @foreach($categories as $category)
                                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                                            @endforeach
                                                            @endif
                                                        </optgroup>
                                                    </select>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>                                       
                                        </form>
                                    </div>
                                    <div class="col-md-8">    
                                        <div class="text-right">
                                            <a class="btn yellow" data-toggle="modal" href="#import_products_modal"> <span aria-hidden="true" class="icon-control-play"></span> Watch Video </a>
                                            {{--<a href="{{url('uploads/imports/sample-product-import.xlsx')}}" class="btn blue"><span aria-hidden="true" class="icon-cloud-download"></span> Download Sample File</a>--}}
                                            <a id="category_link" href="#" class="btn blue"><span aria-hidden="true" class="icon-cloud-download"></span>
                                    Download Sample File</a>
                                            <a href="{{route('seller.products.exportCategoriesList')}}" id="btn_table_export" class="btn blue" data-container="body" data-placement="top" data-original-title="Export to Excel">Download Categories Master</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light form-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject font-blue sbold"><span aria-hidden="true" class="icon-cloud-upload"></span> Upload Product Import File</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    @if(isset($errors) && $errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    </div>
                    @endif

                    @if(session()->has('failures'))
                    <div class="note note-danger">
                        <table class="table">
                            <tr>
                                <th>Row</th>
                                <th>Attribute</th>
                                <th>Errors</th>
                            </tr>
                            @foreach(session()->get('failures') as $validation)
                            <tr>
                                <td>{{ $validation->row }}</td>
                                <td>{{ $validation->column }}</td>
                                <td>
                                    <ul>
                                        @foreach($validation->errors as $e)
                                        <li>{!! $e !!}</li>
                                        @endforeach
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    @endif

                    <!-- BEGIN FORM-->
                    <form action="{{route('seller.products.processImport')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                
                                @csrf

                                <div class="form-body">
                                    <div class="form-group {{($errors->first('file'))?'has-error':''}}">
                                        <label class="control-label col-md-3">Upload File</label>
                                        <div class="col-md-3">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="input-group input-large">
                                                    <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                        <span aria-hidden="true" class="icon-doc"></span>&nbsp;
                                                        <span class="fileinput-filename"> </span>
                                                    </div>
                                                    <span class="input-group-addon btn default btn-file">
                                                        <span class="fileinput-new"> Browse... </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="hidden"><input type="file" name="file" accept="application/vnd.ms-excel"> 
                                                    </span>
                                                    <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 text-right">
                                            <button type="submit" class="btn blue" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Import</button>
                                        </div>
                                    </div>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
$(document).ready(function(){
    $("a#category_link").click(function() {
        //Do stuff when clicked
        var download_category = $("#category option:selected").text();
        if(download_category == ''){
            alert("Please Select Category");
        }
    });
});

$("#category").change(function () {
    // console.log(this.value);
    var download_category = $("#category option:selected").text();
    // alert(this.value);
    @php $link; @endphp;
    if(this.value == "1") { //Clothing
        $("#category_link").attr('href', "{{url('uploads/imports/sample-product-clothing-import.xlsx')}}");
    }else if (this.value == "2"){ //Footwear
        $("#category_link").attr('href', "{{url('uploads/imports/sample-product-footware-import.xlsx')}}");
    }else if (this.value == "3"){ //Toys
        $("#category_link").attr('href', "{{url('uploads/imports/sample-product-toys-import.xlsx')}}");
    }else if (this.value == "5"){ //Health & Safety
        $("#category_link").attr('href', "{{url('uploads/imports/sample-product-health-safety-import.xlsx')}}");
    }else if (this.value == "7"){ //Personal Care
        $("#category_link").attr('href', "{{url('uploads/imports/sample-product-personal-care-import.xlsx')}}");
    }else if (this.value == "8"){ //Food & Nutrition
        $("#category_link").attr('href', "{{url('uploads/imports/sample-product-food-nutrition-import.xlsx')}}");
    }else if (this.value == "10"){ //Accessories
        $("#category_link").attr('href', "{{url('uploads/imports/sample-product-accessories-import.xlsx')}}");
    }else if (this.value == "13"){ //Books & Art
        $("#category_link").attr('href', "{{url('uploads/imports/sample-product-books-art-import.xlsx')}}");
    }else if (this.value == "14"){ //Sports
        $("#category_link").attr('href', "{{url('uploads/imports/sample-product-sports-import.xlsx')}}");
    }else if (this.value == "15"){ //School Supplies
        $("#category_link").attr('href', "{{url('uploads/imports/sample-product-school-supplies-import.xlsx')}}");
    }else if (this.value == "16"){ //Organic
        $("#category_link").attr('href', "{{url('uploads/imports/sample-product-organic-import.xlsx')}}");
    }
    // $("#category_link").text("Download "+download_category+" Sample File");
    $("#category_link").text("Download Sample File");
});
</script>
@endpush
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->

<head>
    <meta charset="utf-8" />
    <title>Products</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
@php
use Illuminate\Support\Facades\DB;

// Models
use App\Models\Brand;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\ChildCategory;
use App\Models\AttributeOption;
use App\Models\IronType;
use App\Models\WashingType;
use App\Models\Country;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\VariationImage;
use App\Models\ProductVariant;
use App\Models\ProductVariantOption;
use App\Models\Sizechart;
use App\Models\Seller;

    $yesNo = ['yes'=>1,'no'=>0];

    // get all brands
    $brands = Brand::active()->whereNull('created_by')->orWhere('created_by', $seller_id)->pluck('name', 'id')->toArray();

    //get all categories 
    $categories = Category::active()->pluck('title', 'id')->toArray();

    //get all category wise primary attribute
    $catPrimaryAttr = Category::active()->pluck('title', 'primary_attribute')->toArray();

    //get all category wise secondary attribute
    $catSecondaryAttr = Category::active()->pluck('title', 'secondary_attribute')->toArray();

    //get all categories 
    $childCategories = DB::table('view_categories')->selectRaw("path_id, REPLACE(path_title, ' > ', '>>') as path_title")->pluck('path_title', 'path_id')->toArray();

    // get all attribute options
    $attrOptions = AttributeOption::selectRaw("id, CONCAT(attribute_id,'-',option_name) as name")->pluck('name','id')->toArray();

    // get all attribute options
    $attributeOptions = AttributeOption::selectRaw("id, option_name")->pluck('option_name', 'id')->toArray();

    // get all iron types
    $ironTypes = IronType::pluck('name', 'id')->toArray();

    // get all washing types
    $washingTypes = WashingType::pluck('name', 'id')->toArray();

    // get all countries
    $countries = Country::pluck('name', 'id')->toArray();

    // get all size charts
    $sizeChart = Sizechart::where('seller_id', $seller_id)->pluck('name','id')->toArray();

    //get Seller Code
    $sellerDetails = Seller::select('seller_code', 'commission_type')->where('id', $seller_id)->first();
    $seller_code = '';
    if($sellerDetails != null){
        if ($sellerDetails->seller_code != null) {
            $seller_code = $sellerDetails->seller_code;
        }
    }

    //get varient images
    $productVarientImages = DB::table('view_product_images')->selectRaw("images, product_variant_id, thumbnail")->pluck('images','product_variant_id')->toArray();
    $prodVarImgTumbnail = DB::table('view_product_images')->selectRaw("product_variant_id, thumbnail")->pluck('thumbnail','product_variant_id')->toArray();

@endphp
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">
                <table class="tabletable-bordered">
                		
                    <thead>
                        <tr>
                            <th> Type </th>
                            <th> Name </th>
                            <th> SKU </th>
                            <th> Description </th>
                            <th> Brand </th>
                            <th> HSN </th>
                            @if($catid == 13)
                            <th> ISBN </th>
                            @endif
                            @if($catid == 1  || $catid == 2 )
                            <th> SizeChart </th>
                            @endif
                            <th> PrimaryCategory </th>
                            <th> Categories </th>
                            <th> CountryOfOrigin </th>
                            @if($catid == 1)
                            <th> ClothingMaterial </th>
                            <th> WashCareInstructions </th>
                            <th> IronInstructions </th>
                            @endif
                            <th> NoOfItemsInPackage </th>
                            <th> ItemDetailsInPackage </th>
                            <th> MRP </th>
                            <th> SellingPrice </th>
                            <th> Tax </th>
                            <th> Unit </th>
                            <th> Featured </th>
                            <th> Stock </th>
                            <th> MinimumStockQuantity </th>
                            <th> MinimumShippingDays </th>
                            <th> ShippingWeight </th>
                            <th> ShippingLength </th>
                            <th> ShippingBreadth </th>
                            <th> ShippingHeight </th>
                            <th> Images </th>
                            <th> Thumbnail </th>
                            <!-- for all primary categories -->
                            <!-- 1.clothing, 2.Footwear, 3.Toys, 5.health & Safety, 7.Personal Care, 8.Food & Nutrition, 
                                 10.Accessories, 13.Books & Art, 14.Sports, 16.Organic, 
                             -->
                            @if($catid == 1 || $catid == 2 || $catid == 3 || $catid == 10)
                            <th> Color </th>
                            @endif
                            @if($catid == 14)
                            <th> IndoorAndOutDoorGames </th>
                            @endif
                            @if($catid == 7 || $catid == 8)
                            <th> Quantity </th>
                            @endif
                            @if($catid == 5 || $catid == 16)
                            <th> HealthSize </th>
                            @endif
                            @if($catid == 13)
                            <th> Types </th>
                            @endif

                            <!-- for all Secondary categories -->
                            @if($catid == 1)
                            <th> ClothingSize </th>
                            @endif
                            @if($catid == 13 || $catid == 10 || $catid == 3 || $catid == 7)
                            <th> Age </th>
                            @endif
                            @if($catid == 2)
                            <th> FootwearSize </th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                        <tr>
                            <td>{{'configurable'}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->sku}}</td>
                            <td>{{$product->description}}</td>
                            <td>{{$brands[$product->brand_id]}}</td>
                            <td>{{$product->hsn_code}}</td>
                            @if($catid == 13)
                                <td><td>
                            @endif      
                            @if($catid == 1 || $catid == 2 )
                                <td>
                                    @if($product->size_chart_id !=null)
                                       {{$sizeChart[$product->size_chart_id]}}
                                    @endif
                                </td>
                            @endif
                            <td>{{$product->category->title}}</td>
                            @if(isset($product->productvariant))
                            <td>
                                @php
                                    $childCat = ''; 
                                    $childcatText = '';
                                @endphp
                                @foreach($product->productcategory as $categories)
                                @php
                                    $childCat = $categories->category_id.'-'.$categories->subcategory_id.'-'.$categories->childcategory_id;
                                    $childcatText = $childcatText . $childCategories[$childCat] . ';' ;
                                @endphp
                                @endforeach

                                {{substr($childcatText, 0, -1)}}
                            </td>
                            @else
                                <td></td>
                            @endif
                            <td>{{$countries[$product->country_of_origin]}}</td>
                            @if($catid == 1)
                            <td>{{$product->dress_material}}</td>
                            <td>
                                {{$product->washing_type}}
                            </td>
                            <td>
                                {{$product->iron_type}}
                            </td>
                            @endif
                            <td>{{$product->no_of_items}}</td>
                            <td>{{$product->items_in_package}}</td>
                            <td>{{$product->mrp}}</td>
                            <td>{{$product->sell_price}}</td>
                            <td>{{$product->tax}}</td>
                            <td>{{$product->unit}}</td>
                            <td>{{($product->seller_featured==0)?'no':'yes'}}</td>
                            <td></td>
                            <td>{{$product->min_stock_quantity}}</td>
                            <td>{{$product->min_ship_hours}}</td>
                            <td> {{$product->weight}} </td>
                            <td> {{$product->length}} </td>
                            <td> {{$product->breadth}} </td>
                            <td> {{$product->height}} </td>

                            <td> 
                            @if($product->images != null)
                                @php 
                                    $imgs = json_decode($product->images);
                                    $imgArry = [];
                                    $imgText = '';
                                    foreach($imgs as $i=>$img){
                                        $url = explode("/", $img);
                                        $imgArry[$i] = end($url);
                                        $imgText = $imgText . end($url) . ',' ;
                                    }
                                @endphp

                                {{substr($imgText, 0, -1)}}
                            @endif
                            </td>
                            <td>
                            @php $thumbnail = ''; @endphp
                            @if($product->images != null)
                                    @if($product->thumbnail != null)
                                        @php 
                                            $thumbnail = '';
                                            $imgs = json_decode($product->images);
                                            $getThumnailImg = isset($product->thumbnail) ? $imgs[$product->thumbnail-1] : $imgs[0];
                                            //$getThumnailImg = isset($product->thumbnail) ?( isset($imgs[$product->thumbnail]) ? $imgs[$product->thumbnail] : '' ) : $imgs[0];
                                            $url = explode("/", $getThumnailImg);
                                            $thumbnail = end($url);
                                        @endphp
                                    @endif
                                @endif
                             {{$thumbnail}}</td>
                            <td>N/A</td>
                            <td>N/A</td>
                            
                        </tr>
                            @if(isset($product->productvariant))
                                @foreach($product->productvariant as $variant)
                                <tr>
                                    <td>variant</td>
                                    <td> {{$variant->name}} </td>
                                    <td> {{$variant->sku}} </td>
                                    <td> {{$product->description}} </td>
                                    <td>{{$brands[$product->brand_id]}}</td>
                                    <td> {{$product->hsn_code}} </td>
                                    @if($catid == 13)
                                    <td> {{$variant->isbn_code}} <td>
                                    @endif     
                                    @if($catid == 1 || $catid == 2 )
                                    <td>
                                    @if($product->size_chart_id !=null)
                                        {{$sizeChart[$product->size_chart_id]}}
                                    @endif
                                    </td>
                                    @endif
                                    <td> {{$product->category->title}} </td>
                                    @if(isset($product->productvariant))
                                    <td>
                                        @php
                                            $childCat = ''; 
                                            $childcatText = '';
                                        @endphp
                                        @foreach($product->productcategory as $categories)
                                        @php
                                            $childCat = $categories->category_id.'-'.$categories->subcategory_id.'-'.$categories->childcategory_id;
                                            $childcatText = $childcatText . $childCategories[$childCat] . ';' ;
                                        @endphp
                                        @endforeach

                                        {{substr($childcatText, 0, -1)}}
                                    </td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td>{{$countries[$product->country_of_origin]}}</td>
                                    @if($catid == 1)
                                        <td> {{$product->dress_material}} </td>
                                    
                                        <td>
                                            {{$product->washing_type}}
                                    
                                            {{--@php
                                                $washCareIns = explode(',', $product->washing_type_id);
                                                $washArrayCount = count($washCareIns);
                                            @endphp
                                            @foreach($washCareIns as $ins)
                                                @if(--$washArrayCount <= 0)
                                                    {{$washingTypes[$ins]}}
                                                @else
                                                    {{$washingTypes[$ins]}},
                                                @endif
                                            @endforeach--}}
                                        </td>
                                        <td>
                                            {{ $product->iron_type}}
                                            
                                            {{--@php
                                                $irontypes = explode(',', $product->iron_type_id);
                                            @endphp
                                            @foreach($irontypes as $irontype)
                                                {{$ironTypes[$irontype]}}
                                            @endforeach--}}

 
                                        </td>
                                    @endif
                                    <td> {{$product->no_of_items}} </td>
                                    <td> {{$product->items_in_package}} </td>
                                    <td> {{$variant->mrp}} </td>
                                    <td> {{$variant->price}} </td>
                                    <td> {{$product->tax}} </td>
                                    <td> {{$product->unit}} </td>
                                    <td> {{($product->seller_featured==0)?'no':'yes'}} </td>
                                    <td> {{$variant->stock}} </td>
                                    <td> {{$variant->min_stock_quantity}} </td>
                                    <td> {{$variant->min_ship_hours}} </td>
                                    <td> {{$variant->shipping_weight}} </td>
                                    <td> {{$variant->shipping_length}} </td>
                                    <td> {{$variant->shipping_breadth}} </td>
                                    <td> {{$variant->shipping_height}} </td>
                                    <td> 
                                    @if(isset($productVarientImages[$variant->id]))
                                        @php 
                                            $imgs = json_decode($productVarientImages[$variant->id]);
                                            $imgText = '';
                                            $imgArry = [];
                                            foreach($imgs as $i=>$img){
                                                if ($img != null && $img != "null") {
                                                    $url = explode("/", $img);
                                                    $imgArry[$i] = end($url);
                                                    $imgText = $imgText . end($url) . ',' ;
                                                }
                                            }
                                        @endphp
                                        
                                        {{substr($imgText, 0, -1)}}
                                    @endif
                                    </td>
                                    <td>
                                    @php $thumbnail = ''; 
                                        $thumnailImg = '';
                                    @endphp
                                    @if(isset($productVarientImages[$variant->id]))
                                        @php 
                                            $thumnailImg = '';
                                            $imgs = json_decode($productVarientImages[$variant->id]);
                                            $getThumnailImg = isset($prodVarImgTumbnail[$variant->id]) ? $imgs[ $prodVarImgTumbnail[$variant->id] - 1] : $imgs[0];
                                            $url = explode("/", $getThumnailImg);
                                            $thumnailImg = end($url);

                                        @endphp
                                    @endif
                                    {{$thumnailImg}}</td>
                                    @php $varOptions = __getVariantOptionValues($variant->id); @endphp
                                    <td> 
                                        @if($varOptions['PrimaryAttrValue'] != null && $varOptions['PrimaryAttrValue'] != '' )
                                            {{$varOptions['PrimaryAttrValue']}}
                                        @endif
                                    </td>
                                    <td>
                                        @if($varOptions['SecondaryAttrValue'] != null && $varOptions['SecondaryAttrValue'] != '' )
                                            {{$varOptions['SecondaryAttrValue']}}
                                        @else
                                        {{'N/A'}}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                        
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>
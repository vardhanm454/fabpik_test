<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->

<head>
    <meta charset="utf-8" />
    <title>Products</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">
                <table class="tabletable-bordered">
                    <thead>
                        <tr>
                            <th> # </th>
                            <th> Product ID </th>
                            <th> Product Name </th>
                            <th> Selled ID </th>
                            <th> Category </th>
                            <th> Created </th>
                            <th> Modified </th>
                            <th> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($products))
                        @foreach($products as $index => $product)
                        <tr>
                            <td>{{++$index}}</td>
                            <td></td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->seller->name}}</td>
                            <td>{{$product->category->title}}</td>
                            <td>{{date('d M Y', strtotime($product->created_at))}}</td>
                            <td>{{date('d M Y', strtotime($product->updated_at))}}</td>
                            <td>{{$product->statusLabel()}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>
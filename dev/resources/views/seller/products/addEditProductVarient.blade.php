@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
    type="text/css" />
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">

                @if(isset($productVariantData))
                <form class="" action="{{route('seller.products.variantEdit',['id'=>$id, 'vid'=>$vid],'.edit')}}"
                        method="post" enctype="multipart/form-data">
                @else
                    <form class="" action="{{route('seller.products.variantAdd',['id'=>$id],'.add')}}"
                        method="post" enctype="multipart/form-data">
                        @endif
                        {!! csrf_field() !!}
                        <input type="hidden" name="product_unique_id" value="{{(isset($productInfo)?$productInfo->unique_id:'')}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light portlet-fit portlet-datatable bordered">
                                    <div class="portlet-title">
                                        <span class="pull-right">
                                            <a href="#" class="popovers" data-toggle="popover" data-placement="left"  data-trigger="focus" data-content="Chose the product {{isset($primaryattributes)?$primaryattributes[0]->attr_display_name:''}} and enter the variant data.">
                                                <span aria-hidden="true" class="icon-question text-danger"></span>
                                            </a>
                                        </span>
                                        <div class="caption pd-0">
                                            <span
                                                class="bold">{{isset($primaryattributes)?$primaryattributes[0]->attr_display_name:''}}</span>
                                            <input type="hidden" name="primary_attribute_id"
                                                value="{{isset($primaryattributes)?$primaryattributes[0]->attribute_id:''}}">
                                            <input type="hidden" name="primary_attribute_display_name"
                                                value="{{isset($primaryattributes)?$primaryattributes[0]->attr_display_name:''}}">
                                        </div>
                                    </div>
                                    <div class="portlet-body pd-lr-10 pd-tb-5">
                                        <div class="mt-radio-inline">
                                        @foreach($primaryattributes as $index=>$attributes)
                                            
                                                <label class="mt-radio mt-radio-outline" style="color:{{$attributes->colour_code}};"> {{$attributes->option_name}}
                                                    <input type="radio" value="{{$attributes->option_id}}" {{old('variant_option_primary', isset($varientOptions)?$varientOptions[$primaryattributes[0]->attribute_id]['attribute_option_id']:'') == $attributes->option_id ? 'checked' :''}}
                                                        name="variant_option_primary" />
                                                    <span></span>
                                                </label>
                                            
                                        @endforeach
                                        </div>
                                        <div class="form-group {{($errors->first('variant_option_primary'))?'has-error':''}}">
                                            <span class="help-block">{{$errors->first('variant_option_primary')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(count($secondaryattributes)>=1)
                            <div class="col-md-12 text-left">
                                <div class="portlet light portlet-fit portlet-datatable bordered">
                                    <div class="portlet-title">
                                    <span class="pull-right">
                                            <a href="#" class="popovers" data-toggle="popover" data-placement="left"  data-trigger="focus" data-content="Chose the product {{$secondaryattributes[0]->attr_display_name}} and enter the variant data.">
                                                <span aria-hidden="true" class="icon-question text-danger"></span>
                                            </a>
                                        </span>
                                        <div class="caption pd-0">
                                            <span
                                                class="bold ">{{$secondaryattributes[0]->attr_display_name}}
                                                <input type="hidden" name="secondary_attribute_id"
                                                    value="{{$secondaryattributes[0]->attribute_id}}">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="portlet-body pd-lr-10 pd-tb-5">
                                        <div class="mt-radio-inline">
                                        @foreach($secondaryattributes as $index=>$attributes)
                                                <label class="mt-radio mt-radio-outline"> {{$attributes->option_name}}
                                                    <input type="radio" value="{{$attributes->option_id}}"
                                                        name="variant_option_secondary" class="variant-color-radio" 
                                                        {{old('variant_option_secondary', isset($varientOptions[$secondaryattributes[0]->attribute_id])?$varientOptions[$secondaryattributes[0]->attribute_id]['attribute_option_id']:'') == $attributes->option_id ? 'checked' :''}}
                                                        />
                                                    <span></span>
                                                </label>
                                        @endforeach
                                        </div>
                                        <div class="form-group {{($errors->first('variant_option_secondary'))?'has-error':''}}">
                                            <span class="help-block">{{$errors->first('variant_option_secondary')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <hr>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('varient_name'))?'has-error':''}}">
                                        <label class="control-label">Name
                                            <span class="required" aria-required="true"> *
                                            </span></label>
                                        <div class="">
                                            <input type="text" id="varient_name" name="varient_name"
                                                class="form-control varient-name" placeholder="Enter Variant Name"
                                                title="Enter Variant Name" value="{{old('varient_name', isset($productVariantData) ? $productVariantData->name : (isset($productInfo)?$productInfo->name:'') )}}">
                                            <span class="help-block">{{$errors->first('varient_name')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('variant_mrp'))?'has-error':''}}">
                                        <label class="control-label">MRP
                                            <span class="required" aria-required="true"> *
                                            </span></label>
                                        <div class="">
                                            <input type="text" id="variant_mrp" name="variant_mrp"
                                                class="form-control variant-mrp price" placeholder="Enter MRP"
                                                title="Enter MRP"
                                                value="{{old('variant_mrp',  isset($productVariantData) ? $productVariantData->mrp : (isset($productInfo) ? $productInfo->mrp :'') ) }}">
                                            <span class="help-block">{{$errors->first('variant_mrp')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('variant_stock'))?'has-error':''}}">
                                        <label class="control-label">Stock <small>(Current Stock: {{ isset($productVariantData) ? $productVariantData->stock : 0 }})</small><span class="required" aria-required="true"> *
                                            </span></label>
                                        <div class="">
                                            <input type="text" id="variant_stock" name="variant_stock"
                                                class="form-control variant-stock" placeholder="Enter Current Stock"
                                                title="Enter Current Stock"
                                                value="{{old('variant_stock', isset($productVariantData) ? $productVariantData->stock : '' ) }}">

                                            <span class="help-block">{{$errors->first('variant_stock')}}</span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('variant_discount'))?'has-error':''}}">
                                        <label class="control-label">Discount</label>
                                        <div class="">
                                            <input type="text" id="variant_discount" name="variant_discount"
                                                class="form-control variant-discount price" placeholder="Enter Discount"
                                                title="Enter Discount"
                                                value="{{old('variant_discount', isset($productVariantData) ? $productVariantData->discount : (isset($productInfo)?$productInfo->seller_discount:'0') )}}">
                                            <span class="help-block">{{$errors->first('variant_discount')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('variant_min_quantity'))?'has-error':''}}">
                                        <label class="control-label">Minimum Order Quantity
                                            <span class="required" aria-required="true"> *
                                            </span></label>
                                        <div class="">
                                            <input type="text" id="variant_min_quantity" name="variant_min_quantity"
                                                class="form-control variant-quantity"
                                                placeholder="Minimum order quantity" title="Enter Quantity"
                                                value="{{old('variant_min_quantity', isset($productVariantData) ? $productVariantData->min_order_qty :'') }}">
                                            <span class="help-block">{{$errors->first('variant_min_quantity')}}</span>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('variant_selling_price'))?'has-error':''}}">
                                        <label class="control-label">Selling Price<span class="required"
                                                aria-required="true"> *
                                            </span></label>
                                        <div class="">
                                            <input type="text" id="variant_selling_price" name="variant_selling_price"
                                                class="form-control variant-selling-price"
                                                placeholder="Enter Selling Price" title="Enter Selling Price"
                                                value="{{old('variant_selling_price', isset($productVariantData) ? $productVariantData->price : (isset($productInfo)?$productInfo->sell_price:'') )}}" readonly>

                                            <span class="help-block">{{$errors->first('variant_selling_price')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('variant_shipping_hours'))?'has-error':''}}">
                                        <label class="control-label" style="width:100%">Minimum Packaging Time
                                        <span class="pull-right">
                                            <a href="#" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="Enter the number of days you need to keep the package ready for shipping. – Ex: 0, 1 or 2" data-html="true">
                                                <span aria-hidden="true" class="icon-question text-danger"></span>
                                            </a>
                                        </span>
                                        </label>
                                        <div class="">
                                            <input type="text" id="variant_shipping_hours" name="variant_shipping_hours"
                                                class="form-control variant-shipping-hours"
                                                placeholder="Enter the number of days you need to keep the package ready for shipping. – Ex: 0, 1 or 2"
                                                title="Enter the number of days you need to keep the package ready for shipping. – Ex: 0, 1 or 2"
                                                value="{{old('variant_shipping_hours', isset($productVariantData) ? $productVariantData->min_ship_hours : (isset($productInfo)?$productInfo->min_ship_hours:'') ) }}">
                                            <span class="help-block">{{$errors->first('variant_shipping_hours')}}</span>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{($errors->first('variant_weight'))?'has-error':''}}">
                                        <label class="control-label">Shipping Weight <small>(in KG)</small><span
                                                class="required"> *</span>
                                                <span class="pull-right">
                                                    <a href="javascript:;" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="Enter the Weight of package in KG" data-html="true">
                                                                        <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                    </a>
                                                </span>
                                            </label>
                                        <div class="">
                                            <input type="text" id="variant_weight" name="variant_weight"
                                                class="form-control variant-weight" placeholder="Enter Weight of the package"
                                                title="Enter Package Weight"
                                                value="{{old('variant_weight', isset($productVariantData) ? $productVariantData->shipping_weight : (isset($productInfo)?$productInfo->weight:'') ) }}">
                                                <span class="help-block">{{$errors->first('variant_weight')}}</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="{{($errors->first('product_sku'))?'has-error':''}}">
                                        <label class="control-label">SKU:<span class="required"> * </span></label>
                                        <div class="">
                                            <input type="text" name="product_sku" id="product_sku" class="form-control product-sku" placeholder="Enter product SKU"  title="Enter Product SKU" value="{{old('product_sku', isset($productVariantData)?$productVariantData->sku:'')}}">
                                            <span class="help-block">{{$errors->first('product_sku')}}</span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="{{($errors->first('min_stock_quantity'))?'has-error':''}}">
                                        <span class="pull-left">
                                            <label class="control-label">Minimum Stock Quantity:<span class="required"> * </span>
                                        </span>
                                        <span class="pull-right">
                                                <a href="javascript:;" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="Enter Minimum Stock Quantity, helps you yto send an notification is stock is low" data-html="true">
                                                    <span aria-hidden="true" class="icon-question text-danger"></span>
                                                </a>
                                            </span>
                                        </label>
                                        <input type="text" class="form-control"  name="min_stock_quantity" id="min_stock_quantity" value="{{old('min_stock_quantity',  isset($productVariantData) ? $productVariantData->min_stock_quantity : (isset($productInfo) ? $productInfo->min_stock_quantity :'') ) }}" placeholder="1"
                                                                title="Enter Minimum Stock Quantity, eg: 1">
                                        <span class="help-block">{{$errors->first('min_stock_quantity')}}</span>
                                    </div>
                                                               
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Shipping
                                                Dimensions <span class="required"> *</span></label>
                                            <div class="margin-bottom-10">
                                                <div class="row">
                                                    <div class="col-md-4 {{($errors->first('variant_length'))?'has-error':''}}">
                                                        <input type="text" name="variant_length" id="variant_length"
                                                            class="form-control variant-length" placeholder="Length"
                                                            value="{{old('variant_length', isset($productVariantData) ? $productVariantData->shipping_length : (isset($productInfo)?$productInfo->length:'') ) }}">
                                                        <label>(in cm)</label>
                                                    </div>

                                                    <div class="col-md-4 {{($errors->first('variant_breadth'))?'has-error':''}}">
                                                        <input type="text" name="variant_breadth" id="variant_breadth"
                                                            class="form-control variant-breadth" placeholder="Breadth"
                                                            value="{{old('variant_breadth', isset($productVariantData) ? $productVariantData->shipping_breadth : (isset($productInfo)?$productInfo->breadth:'') ) }}">
                                                        
                                                        <label>(in cm)</label>
                                                    </div>
                                                    <div class="col-md-4 {{($errors->first('variant_height'))?'has-error':''}}">
                                                        <input type="text" name="variant_height" id="variant_height"
                                                            class="form-control variant-height" placeholder="Height"
                                                            value="{{old('variant_breadth', isset($productVariantData) ? $productVariantData->shipping_height : (isset($productInfo)?$productInfo->height:'') ) }}">
                                                        <label>(in cm)</label>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                
                                @if (isset($primaryattributes) && $primaryattributes[0]->attr_display_name == "Books & Art")
                                <div class="col-md-4">
                                    <div class="{{($errors->first('product_isbn'))?'has-error':''}}">
                                        <label class="control-label">ISBN Code:<span class="required"> *<span></label>
                                        <div class="">
                                            <input type="text" name="product_isbn" id="product_isbn" class="form-control product-sku" placeholder="Enter ISBN Code"  title="Enter ISBN Code" value="{{old('product_isbn', isset($productVariantData)?$productVariantData->isbn_code:'')}}">
                                        <span class="help-block">{{$errors->first('product_isbn')}}</span>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-6 pd-tb-20">
                                    <label class="mt-checkbox mt-checkbox-outline">
                                        <input type="checkbox" name="is_default" id="is_default" value="1" {{old('is_default', isset($productVariantData) ? $productVariantData->is_default : '') == 1 ? 'checked' : ''}}> Set as
                                        default varient
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa  fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                            @if(isset($productVariantData))
                            <button type="submit" class="btn blue form-submit" name="save" value="savecont" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-check"></span> Save &amp; Continue Edit</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript">
</script>
<script src="{{ __common_asset('pages/scripts/components-editors.js') }}" type="text/javascript"></script>

@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var ProductVariant = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('seller.products.variants',['id'=>$id])}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

            $(".price").on("change", function() {
                if($("#variant_discount").val() >= 0){
                    var ret = parseFloat($("#variant_mrp").val()).toFixed(2) - parseFloat($("#variant_discount").val() || '0').toFixed(2)
                    $("#variant_selling_price").val((ret>0?parseFloat(ret).toFixed(2):0));
                }
            })
        }

    };

}();

jQuery(document).ready(function() {
    ProductVariant.init();
});
</script>
@endpush
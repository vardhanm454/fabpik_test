@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ __common_asset('global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet"
    type="text/css"></script>

     <style>
        .popover{
            width:100%;
            max-width: 100%; /* Max Width of the popover (depending on the container!) */
        }
        div[data-placeholder]:not(:focus):not([data-div-placeholder-content]):before {

            content: attr(data-placeholder);

            float: left;

            margin-left: 2px;

            color: #b3b3b3;


        }

#select2-productTags-results .select2-results__option:before {
    content: "";
    display: inline-block;
    position: relative;
    height: 20px;
    width: 20px;
    border: 2px solid #e9e9e9;
    border-radius: 4px;
    background-color: #fff;
    margin-right: 20px;
    vertical-align: middle;
}

#select2-productTags-results .select2-results__option[aria-selected=true]:before {
    font-family:fontAwesome;
    content: "\f00c";
    color: #fff;
    background-color: #f77750;
    border: 0;
    display: inline-block;
    padding-left: 3px;
}

#select2-productTags-results .select2-container--default .select2-results__option[aria-selected=true] {
    background-color: #fff;
}

#select2-productTags-results .select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: #eaeaeb;
    color: #272727;
}

#select2-productTags-results .select2-container--default .select2-selection--multiple {
    margin-bottom: 10px;
}

#select2-productTags-results .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
    border-radius: 4px;
}


#select2-productTags-results .select2-container--default .select2-selection--multiple {
    border-width: 2px;
}

#select2-productTags-results .select2-container--open .select2-dropdown--below {
    border-radius: 6px;
    box-shadow: 0 0 10px rgba(0,0,0,0.5);
}
#select2-productTags-results .select2-selection .select2-selection--multiple:after {
    content: 'hhghgh';
}
</style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="">
                    @if(isset($editProduct))
                    <form class="" action="{{route('seller.products.edit',['id'=>$id])}}" method="post" enctype="multipart/form-data" id="productSubmit">
                    @else
                    <form class="" action="{{route('seller.products.add')}}" method="post" enctype="multipart/form-data" id="productSubmit">
                    @endif
                        {!! csrf_field() !!}
                            <div class="tabbable-line">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#product_info" data-toggle="tab"> Product Info </a>
                                    </li>
                                    <li>
                                        <a href="#product_meta_data" data-toggle="tab"> Meta Data </a>
                                    </li>
                                    {{--<li>
                                        <a href="#product_images" data-toggle="tab"> Images </a>
                                    </li>--}}
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="product_info">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('product_name'))?'has-error':''}}">
                                                            <label class="control-label">Name:<span class="required"> * </span></label>
                                                            <div class="">
                                                                <input type="text" maxlength="150" name="product_name" id="product_name" class="form-control product-name" placeholder="Enter Product Name" title="Enter Product Name" value="{{old('product_name', isset($editProduct)?$editProduct->name:'')}}">
                                                                <span class="help-block">{{$errors->first('product_name')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('brand'))?'has-error':''}}">
                                                            <label class="control-label">Brand:<span
                                                                    class="required">
                                                                    *
                                                                </span></label>
                                                            <div class="">
                                                                <select id="brand" name="brand"
                                                                    class="form-control brand-id select2" title="Select Brand" data-allow-clear="true">
                                                                    <option value="">Select Brand</option>
                                                                    @if(isset($brands))
                                                                    @foreach($brands as $brand)
                                                                    <option value="{{$brand->id}}"
                                                                        {{old('brand', isset($editProduct)?$editProduct->brand_id:'')==$brand->id?'selected':''}}>
                                                                        {{$brand->name}}</option>
                                                                    @endforeach
                                                                    @endif
                                                                </select>
                                                                <span class="help-block">{{$errors->first('brand')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('primary_category'))?'has-error':''}}">
                                                            <label class="control-label" style="width:100%;">
                                                                <span class="pull-left">Parent Category:<span class="required"> * </span></span>
                                                                <span class="pull-right">
                                                                    <a href="javascript:;" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="Get a help on <br><a href='https://youtu.be/mGnC13h7t18' target='_blank' title='how to chose child categories'>How to chose child categories</a>" data-original-title="<strong>Help</strong>" data-html="true">
                                                                        <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                    </a>
                                                                </span>
                                                            </label>
                                                            <div class="">
                                                                <select id="primary_category"
                                                                    name="primary_category"
                                                                    class="form-control select2"
                                                                    title="Select Primary Category" data-allow-clear="true">
                                                                    <option></option>
                                                                    <optgroup label="Category">
                                                                        @if(isset($categories))
                                                                        @foreach($categories as $category)
                                                                        <option value="{{$category->id}}"
                                                                            @if(old('primary_category', isset($editProduct)?$editProduct-> primary_category:'')==$category->id) selected @endif >{{$category->title}}
                                                                        </option>
                                                                        @endforeach
                                                                        @endif
                                                                    </optgroup>
                                                                </select>
                                                                <span
                                                                    class="help-block">{{$errors->first('primary_category')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="clothing-fields">
                                                            <label class="control-label">Dress Material: <span
                                                                    class="required">*</span></label>
                                                            <div class="{{($errors->first('dress_material'))?'has-error':''}}">
                                                                <input type="text" name="dress_material"
                                                                    id="dress_material"
                                                                    class="form-control dress-material"
                                                                    placeholder="Ex: Cotton, Silk and etc." title="Ex: Cotton, Silk and etc."
                                                                    value = {{old('dress_material', isset($editProduct)?$editProduct->dress_material:'')}}>
                                                                <span class="help-block">{{$errors->first('dress_material')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="clothing-fields">
                                                            <label class="control-label">Iron Instructions: <span
                                                                    class="required">*</span></label>
                                                            <div class="{{($errors->first('iron_type'))?'has-error':''}}">
                                                                <textarea class="form-control" row="1" name="iron_type">{{ old('iron_type', isset($editProduct)?$editProduct->iron_type:'') }}</textarea>
                                                                
                                                                {{--<select id="iron_type" name="iron_type"
                                                                    class="form-control iron-type"
                                                                    title="Select Iron Type">
                                                                    <option value="">Select</option>
                                                                    @if(isset($ironTypes))
                                                                    @foreach($ironTypes as $ironType)
                                                                    <option value="{{$ironType->id}}" {{old('iron_type', isset($editProduct)?$editProduct->iron_type:'')==$ironType->id?'selected':''}}>{{$ironType->name}}</option>
                                                                    @endforeach
                                                                    @endif
                                                                </select> --}}
                                                                <span class="help-block">{{$errors->first('iron_type')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="clothing-fields">
                                                            <label class="control-label">Wash Care Instructions: <span
                                                                    class="required">*</span></label>
                                                            <div class="{{($errors->first('washing_type'))?'has-error':''}}">
                                                                <textarea class="form-control" row="1" name="washing_type">{{old('washing_type', isset($editProduct)?$editProduct->washing_type:'')}}</textarea>
                                                                
                                                                {{--@php $wType = explode(',', isset($editProduct)?$editProduct->washing_type:''); @endphp
                                                                <select id="washing_type" name="washing_type[]" multiple="multiple"
                                                                    class="form-control select2-multiple select2-hidden-accessible washing-type"
                                                                    title="Select Washing Type">
                                                                    <option value="">Select</option>
                                                                    @if(isset($washingTypes))
                                                                    @foreach($washingTypes as $washingType)
                                                                    <option value="{{$washingType->id}}" {{old('washing_type', in_array($washingType->id, $wType))==$washingType->id?'selected':''}}>{{$washingType->name}}</option>
                                                                    @endforeach
                                                                    @endif
                                                                </select>
                                                                --}}
                                                                <span class="help-block">{{$errors->first('washing_type')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('size_chart'))?'has-error':''}}">
                                                            <label class="control-label">Size Chart:<span
                                                                    class="required"> *
                                                                </span></label>
                                                                
                                                            <div class="">
                                                                <select id="size_chart" name="size_chart"
                                                                    class="form-control select2 size-chart"
                                                                    title="Select Size Chart" data-allow-clear="true">
                                                                    <option value="">Select Chart</option>
                                                                    @if(isset($sizecharts))
                                                                    @foreach($sizecharts as $sizechart)
                                                                    <option value="{{$sizechart->id}}" {{ old('size_chart', isset($editProduct)?$editProduct->size_chart_id:'' ) == $sizechart->id ? "selected" : ( isset($editProduct) ? '' : (($sizechart->is_default == 1)?"selected":''))}} >
                                                                    {{$sizechart->name}}</option>
                                                                    @endforeach
                                                                    @endif
                                                                </select>
                                                                <span class="help-block">{{$errors->first('size_chart')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('product_unit'))?'has-error':''}}">
                                                            <label class="control-label">Unit:<span
                                                                    class="required"> *
                                                                </span></label>
                                                            <div class="">
                                                                <input type="text" name="product_unit" id="product_unit"
                                                                    class="form-control product-unit"
                                                                    placeholder="(ex. KG, Pc, etc.)"
                                                                    title="Enter Product Units"
                                                                    value="{{old('product_unit', isset($editProduct)?$editProduct->unit:'')}}">
                                                                <span class="help-block">{{$errors->first('product_unit')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('product_weight'))?'has-error':''}}">
                                                        <label class="control-label" style="width:100%;">Package Weight:<span
                                                                    class="required">
                                                                    *
                                                                </span>
                                                            <span class="pull-right">
                                                                    <a href="javascript:;" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="Enter the Weight of package in Grams" data-html="true">
                                                                        <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                    </a>
                                                                </span></label>
                                                            <div class="">
                                                                <input type="text" name="product_weight" id="product_weight"
                                                                    class="form-control product-weight"
                                                                    placeholder="Enter the Weight of Package"
                                                                    title="Enter Package Weight"
                                                                    value="{{old('product_weight', isset($editProduct)?$editProduct->weight:'')}}">
                                                                <span class="help-block">{{$errors->first('product_weight')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('product_sku'))?'has-error':''}}">
                                                            <label class="control-label">SKU:<span class="required"> * </span></label>
                                                            <div class="">
                                                                <input type="text" name="product_sku" id="product_sku" class="form-control product-sku" placeholder="Enter product SKU"  title="Enter Product SKU" value="{{old('product_sku', isset($editProduct)?$editProduct->sku:'')}}">
                                                                <span class="help-block">{{$errors->first('product_sku')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('product_hsn'))?'has-error':''}}">
                                                            <label class="control-label">HSN Code:</label>
                                                            <div class="">
                                                                <input type="text" name="product_hsn" id="product_hsn" class="form-control product-sku" placeholder="Enter HSN Code"  title="Enter Product HSN Code" value="{{old('product_hsn', isset($editProduct)?$editProduct->hsn_code:'')}}">
                                                            <span class="help-block">{{$errors->first('product_hsn')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label">Packaging Dimensions:<span
                                                                    class="required"> *
                                                                </span></label>
                                                            <div class="">
                                                                <div class="row">
                                                                    <div class="col-md-4 {{($errors->first('product_length'))?'has-error':''}}">
                                                                        <input type="text" name="product_length" id="product_length" class="form-control product-length" placeholder="Length (in cm)" title="Enter Product Length  (in cm)" value="{{old('product_length', isset($editProduct)?$editProduct->length:'')}}">

                                                                    </div>
                                                                   
                                                                    <div class="col-md-4 {{($errors->first('product_breadth'))?'has-error':''}}">
                                                                        <input type="text" name="product_breadth" id="product_breadth" class="form-control product-breadth" placeholder="Breadth (in cm)" title="Enter Product Breadth  (in cm)" value="{{old('product_breadth', isset($editProduct)?$editProduct->breadth:'')}}">
                                                                    </div>
                                                                    <div class="col-md-4 {{($errors->first('product_height'))?'has-error':''}}">
                                                                        <input type="text" name="product_height" id="product_height" class="form-control product-height" placeholder="Height (in cm)" title="Enter Product Height  (in cm)" value="{{old('product_height', isset($editProduct)?$editProduct->height:'')}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('no_of_items'))?'has-error':''}}">
                                                        <label class="control-label" style="width:100%;">No of Items in the Package:
                                                            <span class="pull-right">
                                                                    <a href="javascript:;" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="Number of items to be part of package (ex: 1)" data-html="true">
                                                                        <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                    </a>
                                                                </span>
                                                            </label>
                                                            <div class="">
                                                                <input type="text" name="no_of_items" id="no_of_items"
                                                                    class="form-control no-of-items"
                                                                    placeholder="Number of items to be part of package (ex: 1)" title="Number of items to be part of package (ex: 1)"
                                                                    value="{{old('no_of_items', isset($editProduct)?$editProduct->no_of_items:'')}}">
                                                                <span
                                                                    class="help-block">{{$errors->first('no_of_items')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('items_in_package'))?'has-error':''}}">
                                                            <label class="control-label">Items Details In Package: <span
                                                                    class="required"> * </span></label>
                                                            <div class="">
                                                                <input type="text" name="items_in_package"
                                                                    id="items_in_package"
                                                                    class="form-control items-in-package"
                                                                    placeholder="Ex: Pant, Tshirt and Court " title="Ex: Pant, Tshirt and Court "
                                                                    value="{{old('items_in_package', isset($editProduct)?$editProduct->items_in_package:'')}}">
                                                                <span
                                                                    class="help-block">{{$errors->first('items_in_package')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('shipping_hours'))?'has-error':''}}">
                                                            <span class="pull-left">
                                                            <label class="control-label">Minimum Packaging Time:<span class="required"> *
                                                                </span></span>
                                                                <span class="pull-right">
                                                                    <a href="javascript:;" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="Enter the number of days you need to keep the package ready for shipping. – Ex: 0, 1 or 2" data-html="true">
                                                                        <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                    </a>
                                                                </span></label>
                                                            <div class="">
                                                                <input type="text" name="shipping_hours" id="shipping_hours"
                                                                    class="form-control shipping-hours"
                                                                    placeholder="Enter the number of days you need to keep the package ready for shipping. – Ex: 0, 1 or 2"
                                                                    title="Enter the number of days you need to keep the package ready for shipping. – Ex: 0, 1 or 2"
                                                                    value="{{old('shipping_hours', isset($editProduct)?$editProduct->min_ship_hours:'')}}">
                                                                <span
                                                                    class="help-block"><small>{{$errors->first('shipping_hours')}}</small></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                   
                                                    @if(isset($editProduct))
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('product_status'))?'has-error':''}}">
                                                            <label class="control-label">Status<span
                                                                        class="required"> *
                                                                    </span></label>
                                                            <div class="">
                                                                <select id="product_status" name="product_status"
                                                                    class="form-control product-status"
                                                                    title="Product Status">
                                                                    <option value="">select</option>
                                                                    <option value="1"
                                                                        {{old('product_status', isset($editProduct)?$editProduct->status:'') == 1?'selected':''}}>
                                                                        Active</option>
                                                                    <option value="0"
                                                                        {{old('product_status', isset($editProduct)?$editProduct->status:'') == 0?'selected':''}}>
                                                                        Inactive</option>
                                                                </select>
                                                                <span
                                                                    class="help-block">{{$errors->first('product_status')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('featured_seller'))?'has-error':''}}">
                                                            <label class="control-label">Featured Product in Store:<span
                                                                        class="required"> *
                                                                    </span></label>
                                                            <div class="">
                                                                <select id="featured_seller" name="featured_seller"
                                                                    class="form-control featured-seller"
                                                                    title="Featured Product?">
                                                                    <option value="">Select</option>
                                                                    <option value="1"
                                                                        {{old('featured_seller', isset($editProduct)?$editProduct->seller_featured:'') == 1?'selected':''}}>
                                                                        Yes</option>
                                                                    <option value="0"
                                                                        {{old('featured_seller', isset($editProduct)?$editProduct->seller_featured:'') == 0?'selected':''}}>
                                                                        No</option>
                                                                </select>
                                                                <span
                                                                    class="help-block">{{$errors->first('featured_seller')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('country_of_origin'))?'has-error':''}}">
                                                            <label class="control-label">Country Of Origin: <span
                                                                    class="required"> * </span></label>
                                                            <div class="">
                                                                <select id="country_of_origin" name="country_of_origin"
                                                                    class="form-control country-of-origin select2"
                                                                    title="Country Of Origin"  data-allow-clear="true">
                                                                    <option value="">select</option>
                                                                    @if(isset($countries))
                                                                    @foreach($countries as $country)
                                                                    <option value="{{$country->id}}" {{old('country_of_origin', isset($editProduct)?$editProduct->country_of_origin:'') == $country->id?'selected':''}}>{{$country->name}}
                                                                    </option>
                                                                    @endforeach
                                                                    @endif
                                                                </select>
                                                                <span
                                                                    class="help-block">{{$errors->first('country_of_origin')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('is_customized'))?'has-error':''}}">
                                                        
                                                            <label class="control-label">Customizable:<span class="required"> * </span>
                                                            <!-- <span class="pull-right">
                                                                    <a href="javascript:;" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="" data-html="true">
                                                                        <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                    </a>
                                                                </span> -->
                                                            </label>
                                                            <div class="">
                                                                <select id="is_customized" name="is_customized" class="form-control select2" data-allow-clear="true" title="Product Customized">
                                                                <option value="">Select</option>
                                                                <option value="0" @if (old('is_customized', isset($editProduct)?$editProduct->is_customized:'') == '0') {{'selected'}} @endif> No</option>
                                                                <option value="1" @if (old('is_customized', isset($editProduct)?$editProduct->is_customized:'') == '1') {{'selected'}} @endif> Yes</option>
                                                                </select>
                                                                <span class="help-block">{{$errors->first('is_customized')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                    @if(isset($editProduct))
                                                    <div class="col-md-4">
                                                        <div class="{{($errors->first('product_status'))?'has-error':''}}">
                                                            <label class="control-label">Status:<span
                                                                        class="required"> *
                                                                    </span></label>
                                                            <div class="">
                                                                <select id="product_status" name="product_status"
                                                                    class="form-control product-status"
                                                                    title="Product Status">
                                                                    <option value="">select</option>
                                                                    <option value="1"
                                                                        {{old('product_status', isset($editProduct)?$editProduct->status:'') == 1?'selected':''}}>
                                                                        Active</option>
                                                                    <option value="0"
                                                                        {{old('product_status', isset($editProduct)?$editProduct->status:'') == 0?'selected':''}}>
                                                                        Inactive</option>
                                                                </select>
                                                                <span
                                                                    class="help-block">{{$errors->first('product_status')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                                <hr>
                                                <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="{{($errors->first('min_stock_quantity'))?'has-error':''}}">
                                                                <span class="pull-left">
                                                                    <label class="control-label">Minimum Stock Quantity:<span class="required"> * </span>
                                                                </span>
                                                                <span class="pull-right">
                                                                        <a href="javascript:;" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="Enter Minimum Stock Quantity, helps you yto send an notification is stock is low" data-html="true">
                                                                            <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                        </a>
                                                                    </span>
                                                                    
                                                                </label>
                                                                <input type="text" class="form-control"  name="min_stock_quantity" id="min_stock_quantity" value="{{ old('min_stock_quantity', isset($editProduct)?$editProduct->min_stock_quantity:'')}}" placeholder="1"
                                                                title="Enter Minimum Stock Quantity, eg: 1">

                                                                <span class="help-block">{{$errors->first('min_stock_quantity')}}</span>
                                                            </div>
                                                               
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <label class="control-label">Tags:
                                                                 <span class="pull-right">
                                                                    <a href="javascript:;" class="popovers" data-toggle="popover" data-placement="right"  data-trigger="focus" title="" data-content="For example, if I want to tag some of the dresses specifically for a season like Summer, Diwali or Winter." data-html="true">
                                                                        <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                    </a>
                                                                </span>
                                                            </label>
                                                            
                                                            <select class="form-control input-sm" data-limit="3" name="product_tags[]" id="productTags" multiple>
                                                                @if (old('product_tags'))
                                                                
                                                                    @php $oldtags = old('product_tags'); @endphp
                                                                    @foreach ($oldtags as $ptag)
                                                                        @php $tag = App\Models\ProductTag::selectRaw("id,name")->where('id', $ptag)->first(); @endphp
                                                                        <option value="{{$tag->id}}" selected="selected">{{$tag->name}}</option>
                                                                    @endforeach
                                                                @elseif( isset($editProduct) && ($editProduct->productTags != null) )

                                                                    @foreach($editProduct->productTags as $ptag)
                                                                        <option value="{{$ptag->productTagDetails->id}}" selected="selected">{{$ptag->productTagDetails->name}}</option>
                                                                    @endforeach

                                                                @endif
                                                            </select>
                                                        </div>
                                                </div>

                                            </div>
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="{{($errors->first('product_categories'))?'has-error':''}}">
                                                            <label class="control-label">Categories:<span
                                                                    class="required"> *
                                                                </span></label>
                                                            <div class="">
                                                                <select  class="form-control input-sm" name="category" id="category">
                                                                </select>
                                                                <span class="help-block">{{$errors->first('product_categories')}}</span>
                                                                <div id="product-category" class="well well-sm" contenteditable='false' class='' data-placeholder='Choose multiple child categories in which the product can be visible to the users on the platform. (Ex: If your product is a Ethnic Wear and also wedding special, chose both child categories under clothing)' style="height: 160px; overflow: auto;">

                                                                    @if(!$errors->has('product_categories'))
                                                                        @if(old('product_categories'))
                                                                            @foreach(old('product_categories') as $pCategory)
                                                                                @php @list($categoryId,$subCategoryId,$childCategoryId) = explode('-',$pCategory); @endphp
                                                                                @php $childCat = \DB::table('view_categories')->where('child_id', $childCategoryId)->first(); @endphp
                                                                                @if($childCat)
                                                                                <div id="product-category{{$childCat->path_id}}">
                                                                                    <i class="fa fa-minus-circle"></i> {{ucwords(strtoupper($childCat->path_title))}}
                                                                                    <input type="hidden" name="product_categories[]" value="{{$childCat->path_id}}" /></div>
                                                                                @endif
                                                                            @endforeach
                                                                        @elseif(isset($editProduct) && $editProduct->categories)
                                                                            @foreach($editProduct->categories as $pCategory)
                                                                                @php $childCat = \DB::table('view_categories')->where('child_id', $pCategory->childcategory_id)->first(); @endphp
                                                                                @if($childCat)
                                                                                <div id="product-category{{$childCat->path_id}}">
                                                                                    <i class="fa fa-minus-circle"></i> {{ucwords(strtoupper($childCat->path_title))}}
                                                                                    <input type="hidden" name="product_categories[]" value="{{$childCat->path_id}}" /></div>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row" style="display:none">
                                                    <div class="col-md-12 pd-tb-5">
                                                        <div class="{{($errors->first('is_refundable'))?'has-error':''}}">
                                                            <label class="control-label col-md-6 pd-lr-0">Refundable?:<span
                                                                    class="required"> *
                                                                </span></label>
                                                            <div class="col-md-4">
                                                                <input type="checkbox" class="make-switch is-refundable"
                                                                    value="1" id="is_refundable" name="is_refundable"
                                                                    data-size="small" checked data-on-text="Yes" data-off-text="No"
                                                                    title="Refundable?"
                                                                    {{old('is_refundable', isset($editProduct)?$editProduct->refund_avl:'') == 1?'checked':''}}>
                                                                <span
                                                                    class="help-block">{{$errors->first('is_refundable')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 pd-tb-5">
                                                        <div class="{{($errors->first('is_return'))?'has-error':''}}">
                                                            <label class="control-label col-md-6 pd-lr-0">Return / Exchange?:<span
                                                                    class="required"> *
                                                                </span></label>
                                                            <div class="col-md-4">
                                                                <input type="checkbox" class="make-switch is-return"
                                                                    id="is_return" checked name="is_return" data-on-text="Yes"
                                                                    value="1" data-off-text="No" data-size="small"
                                                                    title="Return / Exchange"
                                                                    {{old('is_return', isset($editProduct)?$editProduct->return_avbl:'') == 1?'checked':''}}>
                                                                <span
                                                                    class="help-block">{{$errors->first('is_return')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 pd-tb-5">
                                                        <div class="{{($errors->first('is_cancel_available'))?'has-error':''}}">
                                                            <label class="control-label col-md-6 pd-lr-0">Cancel Available?:<span
                                                                    class="required"> *
                                                                </span></label>
                                                            <div class="col-md-4">
                                                                <input type="checkbox"
                                                                    class="make-switch is-cancel_available"
                                                                    id="is_cancel_available"checked  name="is_cancel_available"
                                                                    value="1" data-size="small" data-on-text="Yes"
                                                                    data-off-text="No" title="Cancel Available?"
                                                                    {{old('is_cancel_available', isset($editProduct)?$editProduct->cancel_avl:'') == 1?'checked':''}}>
                                                                <span
                                                                    class="help-block">{{$errors->first('is_cancel_available')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <hr class="">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="{{($errors->first('description'))?'has-error':''}}">
                                                    <label class="control-label">Description:<span class="required"> * </span></label>
                                                    <div class="">
                                                        <textarea class="summernote" name="description" id="summernote_1">{{old('description', isset($editProduct)?$editProduct->description:'')}}</textarea>
                                                        <span class="help-block">{{$errors->first('description')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div
                                                    class="{{($errors->first('product_mrp'))?'has-error':''}}">
                                                    <label class="control-label">MRP:<span
                                                            class="required"> *
                                                        </span></label>
                                                    <div class="">
                                                        <input type="text" id="product_mrp" name="product_mrp"
                                                            class="form-control product-mrp price" placeholder="Enter MRP"
                                                            title="Enter MRP"
                                                            value="{{old('product_mrp', isset($editProduct)?$editProduct->mrp:'')}}">
                                                        <span
                                                            class="help-block">{{$errors->first('product_mrp')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label">Discount:</label>
                                                    <div class="{{($errors->first('product_discount'))?'has-error':''}}">
                                                        <input type="text" id="product_discount"
                                                            name="product_discount"
                                                            class="form-control product-discount price"
                                                            value="{{old('product_discount', isset($editProduct)?$editProduct->seller_discount:'0')}}"
                                                            placeholder="Enter Discount" title="Enter Discount">
                                                        <span class="help-block">{{$errors->first('product_discount')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div
                                                    class="{{($errors->first('product_selling_price'))?'has-error':''}}">
                                                    <label class="control-label">Selling Price / Sale Price:<span
                                                            class="required"> *
                                                        </span></label>
                                                    <div class="">
                                                        <input type="text" id="product_selling_price"
                                                            name="product_selling_price"
                                                            class="form-control product-selling-price"
                                                            placeholder="Enter selling price"
                                                            title="Enter Selling Price" readonly
                                                            value="{{old('product_selling_price', isset($editProduct)?$editProduct->sell_price:'')}}">
                                                        <span
                                                            class="help-block">{{$errors->first('product_selling_price')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div
                                                    class="{{($errors->first('product_invoice_tax'))?'has-error':''}}">
                                                    <label class="control-label" style="width:100%;">Invoice Tax (%):<span class="required"> *
                                                        </span>
                                                    <span class="pull-right">
                                                                    <a href="javascript:;" class="popovers" data-toggle="popover" data-placement="top"  data-trigger="focus" title="" data-content="Enter the GST applicable for the product">
                                                                        <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                    </a>
                                                                </span>
                                                            </label>
                                                    <div class="">
                                                        <input type="text" id="product_invoice_tax"
                                                            name="product_invoice_tax"
                                                            class="form-control product-invoice-tax"
                                                            placeholder="Enter the GST applicable for the product"
                                                            title="Enter Invoice Tax (In %)"
                                                            value="{{old('product_invoice_tax', isset($editProduct)?$editProduct->tax:'')}}">
                                                        <span
                                                            class="help-block">{{$errors->first('product_invoice_tax')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="product_meta_data">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Title</label>
                                                    <div class="">
                                                        <input type="text" id="product_meta_title"
                                                            name="product_meta_title"
                                                            class="form-control product-meta_title"
                                                            placeholder="Enter Meta Title" title="Enter Meta Title"
                                                            value="{{old('product_meta_title', isset($editProduct)?$editProduct->meta_title:'')}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Keywords</label>
                                                    <div class="">
                                                    <input type="text" id="meta_keywords"
                                                            name="meta_keywords"
                                                            class="form-control product-meta_title"
                                                            placeholder="Enter Meta Title" title="Enter Meta Title"
                                                            value="{{old('meta_keywords', isset($editProduct)?$editProduct->meta_keywords:'')}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Description</label>
                                                    <div class="">
                                                        <textarea id="product_meta_description"
                                                            name="product_meta_description"
                                                            class="form-control product-meta-description"
                                                            placeholder="Enter Meta Description"
                                                            title="Enter Meta Description">{{old('product_meta_description', isset($editProduct)?$editProduct->meta_description:'')}} </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="tab-pane" id="product_images">
                                        <div class="{{($errors->first('Images'))?'has-error':''}}">
                                            <span class="help-block">{{$errors->first('Images')}}</span>
                                        </div>
                                        <div class="row">

                                            @foreach(range(1,5) as $val)
                                            <div class="col-md-2">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div class="form-group">
                                                            <a href="" id="thumb-image--{{$val}}"
                                                                data-toggle="image" class="img-thumbnail">
                                                                <img src="{{ route('ajax.previewImage',['image'=>( isset($imgSet) && !empty($imgSet) )? $imgSet[$val-1] :'', 'type'=>'productimage'])}}"
                                                                    alt="" title=""
                                                                    data-placeholder="{{ url('uploads/no-image.png') }}"
                                                                    width="90" height="90" />
                                                            </a>
                                                            <input type="hidden"
                                                                name="images[]" value="{{isset($imgSet) && !empty($imgSet) ? $imgSet[$val-1] :''}}"
                                                                id="input-image-{{$val}}" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div
                                                    class="{{($errors->first('default_image'))?'has-error':''}}">
                                                    <label class="control-label col-md-4">Select Default
                                                        Image: <span class="required"></span></label>
                                                    <div class="col-md-4">
                                                        <select name="default_image" id="default_image"
                                                            class="form-control form-filter">
                                                            <option value="">Select</option>
                                                            <option value="1"
                                                                {{old('default_image', isset($editProduct)?$editProduct->thumbnail:'')==1?'selected':''}}>
                                                                Image 1</option>
                                                            <option value="2"
                                                                {{old('default_image', isset($editProduct)?$editProduct->thumbnail:'')==2?'selected':''}}>
                                                                Image 2</option>
                                                            <option value="3"
                                                                {{old('default_image', isset($editProduct)?$editProduct->thumbnail:'')==3?'selected':''}}>
                                                                Image 3</option>
                                                            <option value="4"
                                                                {{old('default_image', isset($editProduct)?$editProduct->thumbnail:'')==4?'selected':''}}>
                                                                Image 4</option>
                                                            <option value="5"
                                                                {{old('default_image', isset($editProduct)?$editProduct->thumbnail:'')==5?'selected':''}}>
                                                                Image 5</option>
                                                        </select>
                                                        <span
                                                            class="help-block">{{$errors->first('default_image')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                            <button type="submit" class="btn blue form-submit" name="save" value="save"
                                data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                            @if(isset($editProduct))
                            <button type="submit" class="btn blue form-submit" name="save" value="savecont"
                                data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save &amp; Continue Edit</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript">
</script>
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript">
</script>
<script src="{{ __common_asset('pages/scripts/components-editors.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}" type="text/javascript"></script>


@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Products = function() {

    $('.summernote').summernote({
        height: 150,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['view', ['fullscreen', 'codeview']],
            ['help', ['help']]
        ],
    });

    $('.summernote').each(function(){
        var summernote = $(this);
        $('form').on('submit',function(){
            if (summernote.summernote('isEmpty')) {
                summernote.val('');
            }else if(summernote.val()=='<p><br></p>'){
                summernote.val('');
            }else if(summernote.val()=='<br style="color: rgb(97, 97, 97); font-family: Roboto-Regular; font-size: 14px;">'){
                summernote.val('');
            }
        });
    });

    return {

        //main function to initiate the module
        init: function() {


            //product Tag Search
            $('#productTags').select2({
                allowClear: true,
                minimumInputLength: 0,
                placeholder: 'Ex: Summer, Diwali, Winter',
                closeOnSelect: false,
                ajax: {
                    url: "{{route('ajax.ProductTagAutoComplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: $.map(data, function(tagArr) {
                                return {
                                    text: tagArr.text,
                                    id: tagArr.id,
                                    val: tagArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function(tagArr) {
                    return tagArr.text;
                },
            });

            $('#category').select2({ 
                allowClear:true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('seller.childcategories.autocomplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            c: $('#primary_category').val(),
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (categoryArr) {
                    return categoryArr.text;
                },
            });

            $('#category').on('change', function(e) {

                var data = $('#category').select2('data');

                if(data[0]) {
                    $('#product-category' + data[0].id).remove();

                    $('#product-category').append('<div id="product-category' + data[0].id + '"><i class="fa fa-minus-circle"></i> ' + data[0].text + '<input type="hidden" name="product_categories[]" value="' + data[0].id + '" /></div>');

                    $("#category").empty();
                }
            });

            $('#product-category').delegate('.fa-minus-circle', 'click', function() {
                $(this).parent().remove();
            });

            if($('#primary_category').val()=={{CLOTHING_CATEGORY}}) {
                $('.clothing-fields').show();
            } else {
                $('.clothing-fields').hide();
            }

            //show the clothing related fields when category is CLOTHING
            $("body").on('change', '#primary_category', function() {
                if($(this).val() == {{CLOTHING_CATEGORY}}) {
                    $('.clothing-fields').show();
                } else {
                    $('.clothing-fields').hide();
                }
            });

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('seller.products')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });

            $(".price").on("change", function() {
                var ret = parseFloat($("#product_mrp").val()).toFixed(2) - parseFloat($("#product_discount").val() || '0').toFixed(2)
                $("#product_selling_price").val((ret>0?parseFloat(ret).toFixed(2):0));
            })
        }

    };

}();

jQuery(document).ready(function() {
    Products.init();
});
</script>
@endpush
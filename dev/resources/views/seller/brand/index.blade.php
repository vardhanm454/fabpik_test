@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <!-- FILTER FORM START -->
                <form action="#" method="get" class="filter-form">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label col-md-2 pd-tb-5"><strong>Name:</strong></label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control input-sm" name="fname" id="fname"
                                                    value="{{request()->fname}}" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-4">
                                        <div class="form-group">
                                            <label
                                                class="control-label col-md-3 pd-tb-5"><strong>Status:</strong></label>
                                            <div class="col-md-9">
                                                <select name="fstatus" id="fstatus" class="form-control input-sm">
                                                    <option value="">All</option>
                                                    <option value="1"
                                                        {{request()->fstatus=='1'?'selected="selected"':''}}>Active
                                                    </option>
                                                    <option value="0"
                                                        {{request()->fstatus=='0'?'selected="selected"':''}}>Inactive
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-3">
                                        <label class="control-label col-md-4 pd-tb-5"><strong>Action:</strong></label>
                                        <div class="col-md-8">
                                            <button type="button" class="btn btn-sm btn-icon-only white" name="search"
                                                id="btn_submit_search" value="search"
                                                data-loading-text="<i class='fa fa-spinner fa-spin'></i>"
                                                title="Apply Filter"><span aria-hidden="true"
                                                    class="icon-magnifier"></span> </button>
                                            <a class="btn btn-sm btn-icon-only white" href="{{route('seller.brands')}}"
                                                title="Reset Filter"><span aria-hidden="true"
                                                    class="icon-refresh"></span> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                       <a href="javascript:;" id="btn_table_export" class="btn btn-default btn-sm tooltips" data-container="body" data-placement="top" data-original-title="Export to Excel">
                       <strong><span aria-hidden="true" class="icon-docs text-warning"></span>&nbsp;Export</strong></a>
                    </div>
                    <table class="table table-bordered table-checkable nowrap" id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="1%" class=""> S.No</th>
                                <th width="25%" class=""> Name </th>
                                <th width="28%" class=""> Logo </th>
                                <th width="10%" class=""> Status </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Brands = function() {

    return {

        //main function to initiate the module
        init: function() {

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('seller.brands')}}?search=1";

                if ($('#fname').val() != '') url += "&fname=" + encodeURIComponent($('#fname').val());
                if ($('#fstatus').val() != '') url += "&fstatus=" + encodeURIComponent($('#fstatus')
                    .val());

                window.location.href = url;
            });

            // Export all Brands data
            $('#btn_table_export').on('click', function(e){
                e.preventDefault();

                var url = "{{route('seller.brands.export')}}?export=1";
                
                if($('#fname').val() != '') url += "&fname="+encodeURIComponent($('#fname').val());
                if($('#fstatus').val() != '') url += "&fstatus="+encodeURIComponent($('#fstatus').val());
                
                window.location.href = url;
            });
        }

    };

}();

jQuery(document).ready(function() {
    Brands.init();
});
</script>
@endpush
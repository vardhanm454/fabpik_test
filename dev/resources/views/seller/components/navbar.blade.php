<style>
.badge.badge-default {
    background-color: #36c6d3;
    color: #fff;
}
.badge{
    font-family: "Open Sans",sans-serif;
    position: absolute;
    top: 10px;
    right: 20px;
    font-weight: 300;
    padding: 3px 6px;
}
.notif:hover{
    background-color: #3f4f62!important;
}
</style>
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{route('seller.dashboard')}}">
            <img src="{{ __common_asset('img/logo.png') }}" alt="logo" class="logo-default" style="height: 55px;" /> </a>
            <!--<div class="menu-toggler sidebar-toggler">
                <span></span>
            </div>-->
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        <span></span>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li>
                    <a href="{{route('seller.account.notifications')}}" class="text-white notif" style="padding-left: 5px !important;">
                        <i class="icon-bell notification-icon" style="color: #748194;font-size: 17px;top:5px !important; left:4px;"></i>
                        @if($notificationCount[0]->notificationcount!==0)
                            <span class="badge badge-default"> {{$notificationCount[0]->notificationcount}} </span>
                        @else
                            <span></span>
                        @endif
                    </a>
                </li>
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="{{url('/'.UPLOAD_PATH.'/avatar/'.auth()->user()->avatar)}}" />
                        <span class="username username-hide-on-mobile"> {{auth()->user()->name}} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="{{route('seller.account.profile')}}">
                            <i class="icon-user"></i> My Profile </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="{{route('seller.account.changePassword')}}">
                            <i class="icon-lock"></i> Change Password </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="{{route('seller.account.logout')}}">
                            <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            <li class="heading">
                <h3 class="uppercase sbold">Navigation</h3>
            </li>
            @if(\App\Models\Seller::where(['id'=>auth()->user()->ref_id,'approval_status'=>1])->exists())
            <li class="nav-item  @if($activeMenu=='dashboard') active @endif">
                <a href="{{route('seller.dashboard')}}" class="nav-link ">
                    <i class="icon-grid"></i>
                    <span class="title">Dashboard</span>
                    @if($activeMenu=='dashboard') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item @if($activeMenu=='prod-mgmt') active open @endif">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bag"></i>
                    <span class="title">Product Management</span>
                    @if($activeMenu=='prod-mgmt')
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                    @else
                    <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                   {{-- <li class="nav-item @if($activeSubmenu=='brands') active @endif">
                        <a href="{{route('seller.brands')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Brands</span>
                            @if($activeSubmenu=='brands') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='categories') active @endif">
                        <a href="{{route('seller.categories')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Categories</span>
                            @if($activeSubmenu=='categories') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='subcategories') active @endif">
                        <a href="{{route('seller.subcategories')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Sub Categories</span>
                            @if($activeSubmenu=='subcategories') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='childcategories') active @endif">
                        <a href="{{route('seller.childcategories')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Child Categories</span>
                            @if($activeSubmenu=='childcategories') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='attributes') active @endif">
                        <a href="{{route('seller.attributes')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Attributes</span>
                            @if($activeSubmenu=='attributes') <span class="selected"></span> @endif
                        </a>
                    </li>--}}
                    <li class="nav-item @if($activeSubmenu=='sizecharts') active @endif">
                        <a href="{{route('seller.sizecharts')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Size Charts</span>
                            @if($activeSubmenu=='sizecharts') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='products') active @endif">
                        <a href="{{route('seller.products')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Product</span>
                            @if($activeSubmenu=='products') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='importproduct') active @endif">
                        <a href="{{route('seller.products.import')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Import Product</span>
                            @if($activeSubmenu=='importproducts') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='updateproduct') active @endif">
                        <a href="{{route('seller.products.update')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Update Products</span>
                            @if($activeSubmenu=='updateproducts') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='changeprices') active @endif">
                        <a href="{{route('seller.changeprices')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Change Prices</span>
                            @if($activeSubmenu=='changeprices') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='importimage') active @endif">
                        <a href="{{route('seller.image')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Import Image</span>
                            @if($activeSubmenu=='importimage') <span class="selected"></span> @endif
                        </a>
                    </li>
                    <li class="nav-item @if($activeSubmenu=='reviews') active @endif">
                        <a href="{{route('seller.reviews')}}" class="nav-link ">
                            <i class="fa fa-circle fw"></i>
                            <span class="title">Reviews</span>
                            @if($activeSubmenu=='reviews') <span class="selected"></span> @endif
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item  @if($activeMenu=='orders') active @endif">
                <a href="{{route('seller.orders')}}" class="nav-link ">
                    <i class="icon-basket-loaded"></i>
                    <span class="title">Order Management</span>
                    @if($activeMenu=='orders') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='shipping') active @endif">
                <a href="{{route('seller.shipping')}}" class="nav-link ">
                    <i class="icon-pointer"></i>
                    <span class="title">Shipping</span>
                    @if($activeMenu=='shipping') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='payments') active @endif">
                <a href="{{route('seller.payments')}}" class="nav-link ">
                    <i class="icon-credit-card"></i>
                    <span class="title">Payment & Commission</span>
                    @if($activeMenu=='payments') <span class="selected"></span> @endif
                </a>
            </li>

            <li class="nav-item  @if($activeMenu=='tickets') active @endif">
                <a href="{{route('seller.tickets')}}" class="nav-link ">
                    <i class="icon-bubbles"></i>
                    <span class="title">Tickets</span>
                    @if($activeMenu=='tickets') <span class="selected"></span> @endif
                </a>
            </li>
            {{--<li class="nav-item  @if($activeMenu=='staffs') active @endif">
                <a href="{{route('seller.staffs')}}" class="nav-link ">
                    <i class="icon-users"></i>
                    <span class="title">Staffs</span>
                    @if($activeMenu=='staffs') <span class="selected"></span> @endif
                </a>
            </li>
            <li class="nav-item  @if($activeMenu=='conversations') active @endif">
                <a href="{{route('seller.conversations')}}" class="nav-link ">
                    <i class="icon-bubbles"></i>
                    <span class="title">Conversations</span>
                    @if($activeMenu=='conversations') <span class="selected"></span> @endif
                </a>
            </li>--}}
            <li class="nav-item  @if($activeSubmenu=='profile') active @endif">
                <a href="{{route('seller.account.profile')}}" class="nav-link ">
                    <i class="icon-user"></i>
                    <span class="title">Manage Profile</span>
                    @if($activeSubmenu=='profile') <span class="selected"></span> @endif
                </a>
            </li>
            @else
            <li class="nav-item  @if($activeSubmenu=='profile') active @endif">
                <a href="{{route('seller.account.profile')}}" class="nav-link ">
                    <i class="icon-user"></i>
                    <span class="title">Manage Profile</span>
                    @if($activeSubmenu=='profile') <span class="selected"></span> @endif
                </a>
            </li>
            @endif
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
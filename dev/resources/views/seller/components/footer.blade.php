<div class="page-footer">
    <div class="page-footer-inner"> {{date('Y')}} &copy; {{APP_NAME}} By
        <a target="_blank" href="https://mirakitech.com/">Miraki Technologies</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
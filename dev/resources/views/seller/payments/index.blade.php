@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content')
</style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="">Date: </label><br>
                                    <div class="">
                                        <div class="input-group input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control date-picker input-sm" name="ffromdate"
                                                id="ffromdate" placeholder="From" data-date-end-date="0d" value="{{request()->ffromdate}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control date-picker input-sm" name="ftodate" id="ftodate"
                                                placeholder="To" data-date-end-date="0d" value="{{request()->ftodate}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="">Order Status: </label><br>
                                    <div class="">
                                        <select name="fostatus" id="fostatus"
                                            class="form-control select2 form-filter input-sm margin-bottom-5" data-allow-clear="true">
                                            <option value="">Select</option>
                                            @if(isset($orderStatus))
                                            @foreach($orderStatus as $status)
                                            <option value="{{$status->id}}"
                                                {{request()->fostatus==$status->id?'selected="selected"':''}}>
                                                {{$status->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="">Payout Status: </label><br>
                                    <div class="">
                                        <select name="fpstatus" id="fpstatus" class="form-control select2 form-filter input-sm margin-bottom-5" data-allow-clear="true">
                                            <option value="">Select</option>
                                            <option value="0" {{request()->fpstatus==1 ? 'selected="selected"':''}} > Yes </option>
                                            <option value="1" {{request()->fpstatus==0 ? 'selected="selected"':''}} > No </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="">Payout Date: </label><br>
                                    <div class="date-picker input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                        <input type="text" class="form-control datepicker input-sm" name="fpayoutdate"
                                                id="fpayoutdate" placeholder="Payout Date" value="{{request()->fpayoutdate}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="">Order ID:</label></br>
                                    <input type="text" class="form-control input-sm" name="forderid" id="forderid" value="{{request()->forderid}}" autocomplete="off">
                                </div>
                                <div class="col-md-4">
                                    <label class="">Action: </label><br>
                                    <div class="">
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search"
                                            id="btn_submit_search" value="search"
                                            data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('seller.payments')}}"
                                            title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <table class="table dt-responsive table-checkable nowrap text-center"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="" class="text-center"> Date </th>
                                <th width="" class="text-center"> Order ID </th>
                                <th width="" class="text-center"> MRP <br>(INR): </th>
                                <th width="" class="text-center"> Sale Price <br>(INR) </th>
                                <th width="" class="none"> Quantity: </th>
                                <th width="" class="none"> Total Amount (INR): </th>
                                <th width="" class="text-center"> Base Commission <br>Rate (%) </th>
                                <th width="" class="text-center"> Final Commission <br>Rate (%) </th>
                                <th width="" class="text-center"> Final Commission <br>(INR) </th>
                                <!-- <th width="" class="none"> GST (%): </th>
                                <th width="" class="none"> GST (INR): </th> -->
                                <th width="" class="none"> Shipping Charges (INR): </th>
                                <th width="" class="none"> Shipping Charges GST (%): </th>
                                <th width="" class="none"> Shipping Charges GST (INR): </th>
                                <th width="" class="text-center"> Final amount to <br>Get (INR):</th>
                                <th width="" class="none"> Order Status </th>
                                <th width="" class="text-center"> Payout Status </th>
                                <th width="" class="none"> Payout Date: </th>
                                <th class="text-center"> Actions </th>

                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
    type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Orders = function() {

    return {

        //main function to initiate the module
        init: function() {

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }


            //Filters will apply when click the enter
            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('seller.payments')}}?search=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fostatus').val() != '') url += "&fostatus=" + encodeURIComponent($('#fostatus').val());
                if ($('#fpstatus').val() != '') url += "&fpstatus=" + encodeURIComponent($('#fpstatus').val());
                if ($('#forderid').val() != '') url += "&forderid=" + encodeURIComponent($('#forderid').val());
                if ($('#fpayoutdate').val() != '') url += "&fpayoutdate=" + encodeURIComponent($('#fpayoutdate').val());
                
                window.location.href = url;
            });

            // Export all parentorders data
            $('#btn_table_export').on('click', function(e) {
                e.preventDefault();

                var url = "{{-- route('seller.parentorders.export') --}}?export=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fostatus').val() != '') url += "&fostatus=" + encodeURIComponent($('#fostatus').val());
                if ($('#fpstatus').val() != '') url += "&fpstatus=" + encodeURIComponent($('#fpstatus').val());

                window.location.href = url;
            });
        }
    };
}();

jQuery(document).ready(function() {
    Orders.init();
});
</script>
@endpush
@extends(SELLER_THEME_NAME.'.layouts.auth')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<!-- BEGIN : LOGIN PAGE 5-1 -->
<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-8 bs-reset mt-login-5-bsfix">
            <div class="login-bg text-center" style="background-image:url({{ __common_asset('pages/img/bg1.jpg') }}">
                <img class="login-logo" src="{{ __common_asset('img/logo1.png') }}" style="height: 100px;" /> 
            </div>
        </div>
        <div class="col-md-4 login-container bs-reset mt-login-5-bsfix">
            <div class="login-content">
                <h1 class="text-center bold">Forgot Password</h1><hr>
                <form class="login-form" action="{{route('seller.forgotPassword')}}" method="post">
                    {!! csrf_field() !!}

                    <div class="row">
                        <div class="col-md-12">
                            <p>Don't worry! Just fill in your email and we'll send you the password.</p>
                        </div>
                        <div class="col-md-12">
                            <div class="{{($errors->first('email'))?'has-error':''}}">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" name="email"/> 
                                <span class="help-block">{{$errors->first('email')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="forgot-password">
                                <a href="{{route('seller.login')}}" id="login" class="btn btn-success font-white"><span aria-hidden="true" class="icon-arrow-left"></span> Back to Login</a>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-primary" id="forgot_btn" name="doSubmit" value="forgotPasword" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Submit..."><span aria-hidden="true" class="icon-arrow-right"></span> Send Password</button>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END : LOGIN PAGE 5-1 -->
@stop

@push('PAGE_ASSETS_JS')
@endpush
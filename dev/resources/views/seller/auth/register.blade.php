@extends(SELLER_THEME_NAME.'.layouts.auth')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<!-- BEGIN : LOGIN PAGE 5-1 -->
<div class="user-login-5 register-wrap">
    <div class="row bs-reset">
        <div class="col-md-6 bs-reset mt-login-5-bsfix">
            <div class="login-bg" style="background-image:url({{ __common_asset('pages/img/bg1.jpg') }}">
                <img class="login-logo" src="{{ __common_asset('img/logo.png') }}" style="height: 100px;" />
            </div>
        </div>
        <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
            <div class="login-content" style="margin-top: 0;">
                <h1 class="text-center bold">Seller Registration</h1>
                <hr>
                <form class="horizontal-form" action="{{route('seller.register')}}" method="post" role="form" id="registration_form">
                    {!! csrf_field() !!}

                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="{{($errors->first('fullname'))?'has-error':''}}">
                                    <label class="control-label text-left">Full Name:</label>
                                    <div class="input-icon">
                                        <i class="fa fa-user"></i>
                                        <input type="text" class="form-control" id="full_name" name="fullname" value="{{old('fullname')}}">
                                        <span class="help-block"> {{$errors->first('name')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="{{($errors->first('mobile'))?'has-error':''}}">
                                    <label class="control-label">Mobile Number:</label>
                                    <div class="input-icon">
                                        <i class="fa fa-phone"></i>
                                        <input type="text" class="form-control" id="mobile" name="mobile" value="{{old('mobile')}}">
                                        <span class="help-block"> {{$errors->first('mobile')}}</span>
                                        <span class="help-block" id="unique_mobile"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="{{($errors->first('email'))?'has-error':''}}">
                                    <label class="control-label text-left">Email Address:</label>
                                    <div class="input-icon">
                                        <i class="fa fa-envelope"></i>
                                        <input type="text" class="form-control" id="email" name="email" value="{{old('email')}}">
                                        <span class="help-block"> {{$errors->first('email')}}</span>
                                        <span class="help-block" id="unique_email"></span>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col-md-6">
                                <div class="{{($errors->first('email'))?'has-error':''}}">
                                    <label class="control-label">Confirm Email Address:</label>
                                    <div class="input-icon">
                                        <i class="fa fa-envelope"></i>
                                        <input type="password" class="form-control" id="confirm_email" name="confirm_email" value="{{old('confirm_email')}}">
                                        <span class="help-block"> {{$errors->first('email')}}</span>
                                    </div>
                                </div>
                            </div> --}}
                        </div>

                        {{--<div class="row">
                            <div class="col-md-6">
                                <div class="{{($errors->first('shop_name'))?'has-error':''}}">
                                    <label class="control-label text-left">Shop Name:</label>
                                    <div class="input-icon">
                                        <i class="fa fa-building"></i>
                                        <input type="text" class="form-control" id="shop_name" name="shop_name" value="{{old('shop_name')}}">
                                        <span class="help-block"> {{$errors->first('shop_name')}}</span>
                                    </div>
                                </div>
                                <div class="{{($errors->first('shop_url'))?'has-error':''}}">
                                    <label class="control-label">Shop URL:</label>
                                    <div class="input-icon">
                                        <i class="fa fa-globe"></i>
                                        <input type="text" class="form-control" id="shop_url" name="shop_url" value="{{old('shop_url')}}"><small>
                                        e.g.{{ env('APP_URL') }}shop/<span id="showShopUrl"> YOUR_SHOP_URL {{$errors->first('shop_url')}}</span></small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="{{($errors->first('shop_address'))?'has-error':''}}">
                                    <label class="control-label text-left">Shop Address:</label>
                                    <div class="input-icon">
                                        <i class="fa fa-map-marker"></i>
                                        <textarea class="form-control" id="shop_address" name="shop_address" rows="5">{!! old('shop_address') !!}</textarea>
                                        <span class="help-block"> {{$errors->first('shop_address')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                        <!-- <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="{{($errors->first('password'))?'has-error':''}}">
                                    <label class="control-label text-left">Password:</label>
                                    <div class="input-icon">
                                        <i class="fa fa-lock"></i>
                                        <input type="password" class="form-control" name="password" id="password">
                                        <span class="help-block password-msg"> {{$errors->first('password')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="{{($errors->first('password'))?'has-error':''}}">
                                    <label class="control-label">Confirm Password:</label>
                                    <div class="input-icon">
                                        <i class="fa fa-lock"></i>
                                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" value="">
                                        <span class="help-block"> {{$errors->first('password')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div><hr>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12" id="otp_div" style="display:none;">
                            <p class="text-left">Please Check Your Mobile For OTP</p>
                                <div class="row form-group">
                                    <div class="col-md-5 text-left">
                                        <label class="control-label pd-tb-5">Enter OTP sent to your mobile:</label>
                                        <input type="text" class="form-control" id="verifyOtpVal" name="verifyOtpVal" value="" autocomplete="off">
                                        
                                        <span id="resend_otp_text" class="resend_otp_text" name="resend_otp_text"></span>
                                    </div>
                                    <!-- <div class="col-md-5 text-left">
                                        <label class="control-label pd-tb-5">Enter OTP sent to your email:</label>
                                        <input type="text" class="form-control" id="verifyEmailVal" name="verifyEmailVal" value="" autocomplete="off">
                                        <span id="resend_email_text" class="resend_email_text" name="resend_email_text"></span>
                                    </div> -->
                                </div>
                                <a href="{{route('seller.resend-otp')}}" id="resend_otp" class="resend_otp">Resend OTP</a>
                                <hr>
                            </div>
                            <input type="hidden" value="not_verified" id="checkVerify" name="checkVerify">
                            <div class="col-md-12 text-right">
                                <button type="submit" id="continue" class="btn bg-pink font-white"><span aria-hidden="true" class="icon-arrow-right"></span> Continue</button>
                                <button type="button" id="register" class="btn bg-pink font-white"><span aria-hidden="true" class="icon-login"></span> Create Account</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END : LOGIN PAGE 5-1 -->
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_ASSETS_JS')
<script type="text/javascript">
var Register = function() {

    $('#unique_email').html('');
    $('#unique_mobile').html('');
    // By Default create account button will be hidden.
    $('#register').hide();

    var handlePasswordStrengthChecker = function () {
        var initialized = false;
        var input = $("#password");

        input.keydown(function () {
            if (initialized === false) {
                // set base options
                input.pwstrength({
                    raisePower: 1.4,
                    minChar: 8,
                    verdicts: ["Weak", "Normal", "Medium", "Strong", "Very Strong"],
                    scores: [17, 26, 40, 50, 60]
                });

                // add your own rule to calculate the password strength
                input.pwstrength("addRule", "demoRule", function (options, word, score) {
                    return word.match(/[a-z].[A-Z].[0-9]/) && score;
                }, 10, true);

                // set as initialized
                initialized = true;
            }
        });
    }

    var handleRegister = function() {
        var form1 = $('#registration_form');
        var error3 = $('.alert-danger', form1);
        var success3 = $('.alert-success', form1);

        $.validator.addMethod("alphabetsnspace", function(value, element) {
            return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
        });

        $.validator.addMethod("passwordregex", function(value, element) {
            return this.optional(element) || /^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\W).*$/.test(value);
        });

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                fullname: {
                    required: true,
                    alphabetsnspace: true
                 },
                mobile: {
                    required: true,
                    minlength:10,
                    maxlength:10,
                },
                email: {
                    required: true,
                    email: true
                },
                // confirm_email: {
                //     required: true,
                //     email: true,
                //     equalTo: "#email"
                // },
                // password: {
                //     required: true,
                //     passwordregex: true
                // },
                // confirm_password: {
                //     required: true,
                //     equalTo: "#password"
                // },
                // shop_name: {
                //     required: true
                // },
                // shop_url: {
                //     required: true
                // },
                // shop_address: {
                //     required: true
                // }
            },

            messages: {
                fullname: {
                    required: "Enter full name.",
                    alphabetsnspace: "Please Enter Only Letters."
                },
                mobile: {
                    required: "Enter mobile number.",
                    matches: "Enter a valid number.",
                    minlength: "Number should be 10 digits.",
                    maxlength: "Number should be 10 digits."
                },
                email: {
                    required: "Email is required.",
                    email: "Enter valid email."
                },
                // confirm_email: {
                //     required: "Confirm Email is required.",
                //     email: "Enter valid email.",
                //     equalTo: "Email is different."
                // },
                // password: {
                //     required: "Enter Password.",
                //     passwordregex: "Password must be 8 charaters, 1 uppercase and 1 special charater"
                // },
                // confirm_password: {
                //     required: "Enter Password again.",
                //     equalTo: "Password is different."
                // },
                // shop_name: {
                //     required: "Enter Shop Name."
                // },
                // shop_url: {
                //     required: "Enter Shop URL."
                // },
                // shop_address: {
                //     required: "Enter Shop Address."
                // }
            },
            errorPlacement: function(error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
                $('#continue').button('reset');
            },

            invalidHandler: function(event, validator,) { //display error alert on form submit
                success3.hide();
                error3.show();
                App.scrollTo(error3, -200);
            },

            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                
                $('#unique_email').html('');
                $('#unique_mobile').html('');
                $('#continue').button('loading');
                var mobile = $("#mobile").val();
                var email = $("#email").val();
                var name = $("#full_name").val();
                var csrfToken = "{{ csrf_token() }}";
                var input = {
                    "mobile" : mobile,
                    "email" : email,
                    "name" : name,
                    "action" : "sent_otp",
                    "_token" : csrfToken
                };
                $.ajax({
                    url : '{{route("seller.send-otp")}}',
                    type : 'POST',
                    data : input,
                    success : function(response) {
                        if(response.success == 1){
                             
                            $('#unique_email').html('');
                            $('#unique_mobile').html('');
                            $('#otp_div').show();
                            $('#register').show();
                            $('#continue').hide();
                            $('#continue').button('reset');
                            $('#checkVerify').val('otp_sent');
                            $("#email").prop('readonly', true);
                            $("#mobile").prop('readonly', true);
                            
                        }else if(response.success == 2){
                            
                            $('#continue').button('reset');
                            if(response.errors.email)
                            {
                                $('#unique_email').html(response.errors.email[0]);
                            }
                            if(response.errors.mobile)
                            {
                                $('#unique_mobile').html(response.errors.mobile[0]);
                            }
                            
                        }
                    }
                });
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {

            handlePasswordStrengthChecker();

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
               
            });

            $('#continue').click(function(){
                handleRegister();
            });

            $('#register').click(function(){
                
                $('#register').button('loading');
                var otp = $("#verifyOtpVal").val();
                var mobile = $("#mobile").val();
                var email = $("#email").val();
                var csrfToken = "{{ csrf_token() }}";
                var input = {
                    "mobile" : mobile,
                    "email" : email,
                    "otp" : otp,
                    "action" : "verify_otp",
                    "_token" : csrfToken
                };
                if ( (otp.length == 4 && otp != null && otp != '') ) {
                    
                        $.ajax({
                            url : '{{route("seller.send-otp")}}',
                            type : 'POST',
                            dataType : "json",
                            data : input,
                            success : function(response) {
                                if(response.type=='success'){
                                    
                                    // debugger;
                                    
                                    $('#checkVerify').val('verified');
                                    $('#resend_otp').hide();
                                    $('#registration_form').unbind().submit();
                                    
                                }else if(response.type=='expired'){
                                    
                                    
                                    $('#verifyOtp').val('');
                                    $('#register').button('reset');
                                }else if(response.type=='error'){
                                    
                                    
                                    $('#verifyOtp').val('');
                                    $('#register').button('reset');
                                    $('#resend_otp_text').html("Please enter valid OTP");
                                }
                            },
                            error : function() {
                               
                                $('#resend_otp_text').html("Something went wrong, try again");
                                $('#register').button('reset');
                            }
                        });
                    

                }else {
                    
                    $('#resend_otp_text').html("Please enter OTP");
                    $('#register').button('reset');
                }
            });

            $("#shop_url").keypress(function(){
                var val = $('#shop_url').val();
                $("#showShopUrl").text(val);
            });

            // $('#registration_form input').keypress(function(e) {
            //     if (e.which == 13) {
            //         if ($('#registration_form').validate().form()) {
            //             $( "#continue" ).click();
            //         }
            //         return false;
            //     }
            // });

            // init background slide images
            $('.login-bg').backstretch([
                "{{ __common_asset('pages/img/bg1.jpg') }}",
                "{{ __common_asset('pages/img/bg2.jpg') }}",
                "{{ __common_asset('pages/img/bg3.jpg') }}"
                ], {
                  fade: 1000,
                  duration: 8000
                }
            );

        }

    };

}();

jQuery(document).ready(function() {
    Register.init();
    //resend otp
    $('a').click(function(event) {
        
        event.preventDefault();
        $('#register').button('loading');
        var url = $(this).attr('href');
        var mobile = $('#mobile').val();
        var csrfToken = "{{ csrf_token() }}";
        var input = {
            "mobile" : mobile,
            "_token" : csrfToken
        };
        $.ajax({
            url : url,
            type : 'POST',
            dataType : "json",
            data : input,
            success : function(response) {
                if(response.success==1){
                    
                    $('#resend_otp_text').html("OTP resend successfully.");
                    $('#resend_otp_text').css("color","green");
                    $('#register').button('reset');
                } else if(response.success==2){
                   
                    $('#resend_otp_text').html("Something went wrong try after some time.");
                    $('#resend_otp_text').css("color","red");
                    $('#register').button('reset');
                }
            },
            error : function() {
                //showFrontendAlert("ss");
            }
        });
        return false; // for good measure
    });
});
</script>

<script>
        if (window.location.href.indexOf("https://portal.fabpik.in/") > -1) {
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TQ6WMPC');
      }
    </script>
    <!-- End Google Tag Manager -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ6WMPC" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169629500-1"></script>
    <script>
        if (window.location.href.indexOf("https://portal.fabpik.in/") > -1) {
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-169629500-1');
      }
    </script>
@endpush
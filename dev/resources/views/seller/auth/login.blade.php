@extends(SELLER_THEME_NAME.'.layouts.auth')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<!-- BEGIN : LOGIN PAGE 5-1 -->
<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-8 bs-reset mt-login-5-bsfix">
            <div class="login-bg text-center" style="background-image:url({{ __common_asset('pages/img/bg1.jpg') }}">
                <img class="login-logo" src="{{ __common_asset('img/logo1.png') }}" style="height: 100px;" /> 
            </div>
        </div>
        <div class="col-md-4 login-container bs-reset mt-login-5-bsfix">
            <div class="login-content">
                <h1 class="text-center bold">Seller Login</h1><hr>
                    <form class="login-form" action="{{route('seller.login')}}" method="post">
                    {!! csrf_field() !!}
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p>It's great to see you back!</p>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group {{($errors->first('email'))?'has-error':''}}">
                                    <label class="control-label font-dark">Email ID:</label>
                                    <input class="form-control" type="text" autocomplete="off" placeholder="Enter Email ID." name="email"/> 
                                    <span class="help-block"> {{$errors->first('email')}}</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group {{($errors->first('password'))?'has-error':''}}">  
                                    <label class="control-label font-dark">Password:</label>  
                                    <input class="form-control" type="password" autocomplete="off" placeholder="Enter Password" name="password"/> 
                                    <span class="help-block"> {{$errors->first('password')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('seller.forgotPassword')}}" id="forget-password" class="forget-password"><strong>Forgot Password?</strong></a>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button type="submit" class="btn bg-pink font-white pull-right form-submit" id="login_btn" name="doSubmit" value="doLogin" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Logging in..."><span aria-hidden="true" class="icon-login"></span> Login</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END : LOGIN PAGE 5-1 -->
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Login = function () {
    return { //main function to initiate the module
        init: function () {
            $('.form-submit').on('click', function(e){
                $(this).button('loading');
            });
                
        };
    };
}();
    
jQuery(document).ready(function() {    
   Login.init();
});
</script>
<script>
        if (window.location.href.indexOf("https://portal.fabpik.in/") > -1) {
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TQ6WMPC');
      }
    </script>
    <!-- End Google Tag Manager -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TQ6WMPC" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169629500-1"></script>
    <script>
        if (window.location.href.indexOf("https://portal.fabpik.in/") > -1) {
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-169629500-1');
      }
    </script>
@endpush
@extends(SELLER_THEME_NAME.'.layouts.master') @push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/css/sellercalculator.css' )}}" rel="stylesheet" type="text/css" /> @endpush @section('content') </style>
<div class="sellercalculator-page">
    <section class="calculator">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div class="card">
                        <div class="">
                            <div class="card-header">
                                <h5 class="card-title">Commission</h5>
                                <div>
                                    <ul class="main-toggle">
                                        <li class="on">
                                            <label> Commission 1 <span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
                                                        <polyline points="20 6 9 17 4 12"></polyline>
                                                    </svg> </span></label>
                                        </li>
                                        <li> <label>Commission 2 <span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
                                                        <polyline points="20 6 9 17 4 12"></polyline>
                                                    </svg> </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="calculator-form on fixed-form">
                                <form>
                                    <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                                        <div class="card-body pd-20">
                                            <label class="content-colon-none">Choose your product category from the drop down below</label>
                                            <select class="form-control select2-no-search radius-5" data-select2-id="13" tabindex="-1" aria-hidden="true">
                                                <option label="Choose one Category" data-select2-id="15">
                                                </option>
                                                <option value="Clothing" data-select2-id="29"> Clothing </option>
                                                <option value="Accessores" data-select2-id="30"> Accessores </option>
                                                <option value="Toys" data-select2-id="31"> Toys </option>
                                                <option value="Sports" data-select2-id="32"> Sports </option>
                                                <option value="Organic" data-select2-id="33"> Organic </option>
                                            </select>
                                            <div class="form-group row v-row ">
                                                <div class="col-lg-6  col-md-6 col-xs-12 col-sm-12">
                                                    <label for="first-name" class="col-form-label">Enter the MRP price of your product <button type="button" class="info-btn" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                                                            <i class="fa fa-info"></i>
                                                        </button>
                                                    </label>
                                                </div>
                                              
                                                <div class="col-lg-6   col-md-6 col-xs-12 col-sm-12">
                                                    <span class="rupee-sign">&#8377; </span>
                                                    <input type="text" class="form-control" placeholder="1000 ">
                                                </div>
                                               
                                            </div>
                                            <div class="form-group row v-row ">
                                                <div class="col-lg-6  col-md-6 col-xs-12 col-sm-12">
                                                    <label for="first-name" class="col-form-label">Enter the Weight of your product <button type="button" class="info-btn" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                                                            <i class="fa fa-info"></i>
                                                        </button></label>
                                                </div>
                                                <div class="col-lg-6  col-md-6 col-xs-12 col-sm-12">
                                                <span class="rupee-sign">&#8377; </span> <input type="text" class="form-control" placeholder="500 gms">
                                                </div>
                                            </div>
                                            <div class="form-group row v-row ">
                                                <div class="col-lg-12  col-md-12 col-xs-12 col-sm-12 m0-auto">
                                                    <button class="btn btn-style-bg " routerLink="/sellercalculator">Submit</button>
                                                </div>
                                            </div>
                                        </div>  </div>
                                </form>
                                <p class="commission-calculate-text"></p>
                                <div class="commission-calculated ">
                                    <div class="col-lg-12  col-md-12 col-xs-12 col-sm-12 margin-div">
                                        <div class="row v-row">
                                            <div class="col-lg-7 pl-0">
                                                <p class="commission-calculate-text">Total Commission</p>
                                                <p class="p-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                            </div>
                                            <div class="col-lg-5  final-amount">
                                                <div class="float-right">
                                                    <p> Your total make will be <label><span>&#8377;</span>643</label></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                                <label for="first-name" class="col-form-label"> Commission </label>
                                                </label>
                                                <label class="label-result">&#8377; 12%</label>
                                            </div>
                                            <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                                <label for="first-name" class="col-form-label">GST(18%)</label>
                                                </label>
                                                <label class="label-result">&#8377; 12% </label>
                                            </div>
                                            <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                                <label for="first-name" class="col-form-label">SubTotal</label>
                                                </label>
                                                <label class="label-result">&#8377; 12%</label>
                                            </div>
                                            <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                                <label for="first-name" class="col-form-label">Total</label>
                                                </label>
                                                <label class="label-result">&#8377; 12% </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 m0-auto">
                                    <button class="btn btn-go"><span>Click here to start selling your product </span><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right">
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                            <polyline points="12 5 19 12 12 19"></polyline>
                                        </svg></button>
                                </div>
                          
                        </div>
                        <div class="calculator-form variable-form">
                            <form>
                                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                                    <div class="card-body pd-20">
                                        <label class="content-colon-none">Choose your product category from the drop down below</label>
                                        <select class="form-control select2-no-search radius-5" data-select2-id="13" tabindex="-1" aria-hidden="true">
                                            <option label="Choose one Category" data-select2-id="15">
                                            </option>
                                            <option value="Clothing" data-select2-id="29"> Clothing </option>
                                            <option value="Accessores" data-select2-id="30"> Accessores </option>
                                            <option value="Toys" data-select2-id="31"> Toys </option>
                                            <option value="Sports" data-select2-id="32"> Sports </option>
                                            <option value="Organic" data-select2-id="33"> Organic </option>
                                        </select>
                                        <div class="form-group row v-row ">
                                            <div class="col-lg-6  col-md-6 col-xs-12 col-sm-12">
                                                <label for="first-name" class="col-form-label">Enter the MRP price of your product </label>
                                            </div>
                                            <div class="col-lg-5  col-md-6 col-xs-12 col-sm-12 pr-0">
                                            <span class="rupee-sign">&#8377; </span>
                                                <input type="text" class="form-control" placeholder="1000 ">
                                            </div> <button type="button" class="info-btn" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                                                <i class="fa fa-info"></i>
                                            </button>
                                        </div>
                                        <div class="form-group row v-row ">
                                            <div class="col-lg-6  col-md-6 col-xs-12 col-sm-12">
                                                <label for="first-name" class="col-form-label">Enter the Selling price of your product
                                            </div>
                                            <div class="col-lg-5  col-md-6 col-xs-12 col-sm-12  pr-0">
                                            <span class="rupee-sign">&#8377; </span>
                                                <input type="text" class="form-control" placeholder="1000 ">
                                            </div> <button type="button" class="info-btn" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                                                <i class="fa fa-info"></i>
                                            </button>
                                        </div>
                                        <div class="form-group row v-row ">
                                            <div class="col-lg-6  col-md-6 col-xs-12 col-sm-12">
                                                
                                                <label for="first-name" class="col-form-label">Commission (%) </label>
                                            </div>
                                            <div class="col-lg-5  col-md-6 col-xs-12 col-sm-12 pr-0">
                                                <input type="text" class="form-control" placeholder="10 ">
                                            </div> <button type="button" class="info-btn" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                                                <i class="fa fa-info"></i>
                                            </button>
                                        </div>
                                        <div class="form-group row v-row ">
                                            <div class="col-lg-6  col-md-6 col-xs-12 col-sm-12">
                                                <label for="first-name" class="col-form-label">Enter the Weight of your product </label>
                                            </div>
                                            <div class="col-lg-5  col-md-6 col-xs-12 col-sm-12 pr-0 ">
                                                <input type="text" class="form-control" placeholder="500 gms">
                                            </div><button type="button" class="info-btn" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                                                <i class="fa fa-info"></i>
                                            </button>
                                        </div>
                                        <div class="form-group row v-row ">
                                            <div class="col-lg-12  col-md-12 col-xs-12 col-sm-12 m0-auto">
                                                <button class="btn btn-style-bg " routerLink="/sellercalculator">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                            </form>
                            <div class="commission-calculated">
                                <div class="col-lg-12  col-md-12 col-xs-12 col-sm-12 margin-div">
                                    <div class="row v-row">
                                        <div class="col-lg-7 pl-0">
                                            <p class="commission-calculate-text"> Total Fabpik Commission</p>
                                            <p class="p-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                        </div>
                                        <div class="col-lg-5  final-amount">
                                            <div class="float-right">
                                                <p> Your total make will be <label><span>&#8377;</span>643</label></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">Fabpik Commission <button type="button" class="info-btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i></button></label>
                                            </label>
                                            <label class="label-result"> 12%</label>
                                        </div>
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">Handling Fee<button type="button" class="info-btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i></button></label>
                                            </label>
                                            <label class="label-result"> 12% </label>
                                        </div>
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">Shipping Fee </label>
                                            </label>
                                            <label class="label-result"> 12% </label>
                                        </div>
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">SubTotal</label>
                                            </label>
                                            <label class="label-result">&#8377;4000</label>
                                        </div>
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">GST (%)</label>
                                            </label>
                                            <label class="label-result"> 12% </label>
                                        </div>
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">Total</label>
                                            </label>
                                            <label class="label-result">&#8377; 7000 </label>
                                        </div>
                                    </div>
                                  
                                </div>
                                <div class="col-lg-12  col-md-12 col-xs-12 col-sm-12 margin-div">
                                    <div class="row v-row">
                                        <div class="col-lg-7 pl-0">
                                            <p class="commission-calculate-text"> Others Commission</p>
                                            <p class="p-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                        </div>
                                        <div class="col-lg-5  final-amount">
                                            <div class="float-right">
                                                <p> Your total make will be <label><span>&#8377;</span>643</label></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">Commission </label>
                                            </label>
                                            <label class="label-result"> 12%</label>
                                        </div>
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">Handling Fee</label>
                                            </label>
                                            <label class="label-result"> 12% </label>
                                        </div>
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">Shipping Fee </label>
                                            </label>
                                            <label class="label-result">&#8377; 200 </label>
                                        </div>
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">SubTotal</label>
                                            </label>
                                            <label class="label-result">&#8377; 6000</label>
                                        </div>
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">GST (18%)</label>
                                            </label>
                                            <label class="label-result">12% </label>
                                        </div>
                                        <div class="col-lg-2  card col-md-12 col-xs-12 col-sm-12">
                                            <label for="first-name" class="col-form-label">Total</label>
                                            </label>
                                            <label class="label-result">&#8377;7000</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 m0-auto">
                                <button class="btn btn-go"><span>Click here to start selling your product </span><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right">
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                        <polyline points="12 5 19 12 12 19"></polyline>
                                    </svg></button>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-lg-4 offset-lg-1">
<h2 class="sec_title">Make selling profitable</h2>
<p>With flexible fee structures, find the best fit for your business to grow faster and earn profits</p>
<button class="btn btn-style-bg " routerLink="/sellercalculator">Seller Calculator</button>
            </div>
            <div class="col-lg-7 pr-0">
            <img src="{{ __common_asset('pages/img/sellercalculator/slider.png') }}"/>

            </div> -->
</div>
</div>
</section>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div> @stop @push('PAGE_ASSETS_JS') <script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript">
</script> @endpush @push('PAGE_SCRIPTS') <script type="text/javascript">
    // $(".main-toggle").on("click", (function() {
    //     $(this).toggleClass("on");
    //     $(".calculator-form").toggleClass("on")
    // }))
    $('[data-toggle="tooltip"]').tooltip()
    $('.main-toggle li').click(function() {
        $(this).addClass('on').siblings().removeClass('on');
        $(".calculator-form").toggleClass("on")
    });
</script> @endpush
@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<!-- <h1 class="page-title"> {{$title}}</h1> -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                        <form class="" action="{{route('seller.tickets.add')}}"
                            method="post" enctype="multipart/form-data">

                            {!! csrf_field() !!}

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group {{($errors->first('issue_title'))?'has-error':''}}">
                                            <label class="control-label">Subject:<span class="required"> * </span></label>
                                            <div class="">
                                                <input type="text" class="form-control maxlength-handler" name="issue_title" maxlength="100" placeholder="" value="{{old('issue_title')}}">
                                                <span class="help-block">{{$errors->first('issue_title')}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{($errors->first('categories'))?'has-error':''}}">
                                            <label class="control-label">Category:<span class="required"> * </span></label>
                                            <div class="">
                                                <select class="form-control" name="categories" id="categories">
                                                    <option value="">--Select--</option>
                                                    <option value="Orders & Shipping" {{old('categories')=='Orders & Shipping'?'selected':''}}>Orders & Shipping</option>
                                                    <option value="Refund & Exchange Requests" {{old('categories')=='Refund & Exchange Requests'?'selected':''}}>Refund & Exchange Requests</option>
                                                    <option value="Product Help (How to use)" {{old('categories')=='Product Help (How to use)'?'selected':''}}>Product Help (How to use)</option>
                                                    <option value="Techincal Issue" {{old('categories')=='Techincal Issue'?'selected':''}}>Techincal Issue</option>
                                                    <option value="Payments & Commissions" {{old('categories')=='Payments & Commissions'?'selected':''}}>Payments & Commissions</option>
                                                    <option value="Others" {{old('categories')=='Others'?'selected':''}}>Others</option>
                                                </select>
                                                <span class="help-block">{{$errors->first('categories')}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group {{($errors->first('issue'))?'has-error':''}}">
                                            <label class="control-label">Issue<span class="required"> * </span></label>
                                            <div class="">
                                                <textarea class="form-control" id="issue" name="issue" rows="2">{{old('issue')}}</textarea>
                                                <span class="help-block">{{$errors->first('issue')}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{($errors->first('attachment'))?'has-error':''}}">
                                            <label class="control-label">Attachment</label><br>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="input-group">
                                                    <div class="form-control uneditable-input input-fixed" data-trigger="fileinput">
                                                        <span class="fileinput-filename"> </span>
                                                    </div>
                                                    <span class="input-group-addon btn default btn-file">
                                                        <span class="fileinput-new"> Select file </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" id="attachment" name="attachment"> </span>
                                                </div>
                                            </div>
                                            <span class="help-block">{{$errors->first('attachment')}}</span>
                                        </div>
                                        <input type="hidden" name="seller_id" value="{{auth()->user()->ref_id}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <button type="button" id="btn_back" class="btn btn-secondary-outline"><span aria-hidden="true" class="icon-arrow-left"></span> Back</button>
                                <button type="submit" class="btn blue form-submit" name="save" value="save"
                                    data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
    type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}"
    type="text/javascript"></script>

@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Tickets = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#btn_back').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{route('seller.tickets')}}";
            });

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

            //init maxlength handler
            $('.maxlength-handler').maxlength({
                limitReachedClass: "label label-danger",
                alwaysShow: true,
                threshold: 3
            });
        }

    };

}();

jQuery(document).ready(function() {
    Tickets.init();
});
</script>
@endpush
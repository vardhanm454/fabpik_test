@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('apps/css/ticket.min.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<div class="app-ticket app-ticket-details">
   <div class="row">
       <div class="col-md-12">
           <div class="portlet light bordered">
               <div class="portlet-title tabbable-line">
                   <div class="caption col-md-12">
                       <div class="row">
                       <div class="col-xs-6">
                           <div class="ticket-id bold font-blue">#{{$ticket_details->ticket_no}}</div>
                           <div class="ticket-title bold uppercase font-blue">Subject: {{$ticket_details->issue_title}}</div>
                       </div>
                       <div class="col-xs-6 text-right">
                           <div class="ticket-id font-blue">
                              Raised Date: {{date('d/m/yy h:i A', strtotime($ticket_details->created_at))}}
                           </div>
                           @if($ticket_details->issue_status == 2)
                           <div class="ticket-id font-blue">
                              Closed Date: {{date('d/m/yy h:i A', strtotime($ticket_details->updated_at))}}
                           </div>
                           @endif
                       </div>
                   </div>
                   </div>
               </div>
               <div class="portlet-body">                   
                   <div class="row">
                       <div class="col-xs-12">
                           <div class="ticket-msg bold">
                              <p> {{$ticket_details->issue}} </p> <br>
                           </div>
                       </div>
                   </div>
                </div>
           </div>   
                <?php if($ticket_details->comments){ ?>
                  <div class="portlet light bordered">
                     <div class="portlet-body" id="chats">
                         <div class="scroller" style="height: auto;" data-always-visible="1" data-rail-visible1="1">
                             <ul class="chats">
                              @foreach($ticket_details->comments as $ticket_comment)
                                 <li class="{{(auth()->user()->id==$ticket_comment->comment_by)? 'in' : 'out'}} pd-tb-5">
                                     <div class="message pd-tb-5">
                                         <!-- <span class="arrow"> </span> -->
                                         <a href="javascript:;" class="name"> <strong>{{ ($ticket_comment->user) ? $ticket_comment->user->name : '' }}</strong> </a>
                                         <span class="datetime"> at {{date('h:i A', strtotime($ticket_comment->created_at))}} </span>
                                         <span class="body"> {{$ticket_comment->comments}} </span>
                                        <?php if(!empty($ticket_comment->attachment)){ ?>
                                         <span><img src="<?php echo URL::to('/').'/'.UPLOAD_PATH.'/support_ticket/'.$ticket_comment->attachment; ?>" width="200px" height="200px"></span>
                                        <?php } ?>
                                     </div>
                                 </li>
                              @endforeach
                             </ul>
                         </div><hr>
                         <?php } ?>
                         @if($ticket_details->issue_status == 2)
                            <div class="text-right">
                                <a href="{{route('seller.tickets')}}" id="btn_back" class="btn btn-secondary-outline"> <span aria-hidden="true" class="icon-arrow-left"></span> Back</a>
                            </div>
                         @else
                         <div class="ticket-chat chat-form bg-light">
                            <form  id="ticket_comment_form_reply" role="form" method="post" action="{{route('seller.tickets.postComment', ['id'=>$ticket_details->id])}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                            <div class="col-md-12">
                                <div class="input-cont form-group {{($errors->first('sellerComment'))?'has-error':''}}">
                                    <input class="form-control" name="sellerComment" id="selCommentReply" value="{{old('sellerComment')}}" type="text" placeholder="Type a message here..." />
                                    <span class="help-block">{{$errors->first('sellerComment')}}</span>
                                </div>
                            </div> 
                          </div>
                          <div class="row">
                            <div class="col-md-6 text-left">
                                <div class="btn-cont {{($errors->first('sellerAttachment'))?'has-error':''}}">
                                        <!-- <span class="arrow"> </span> -->
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn blue btn-file">
                                            <span class="fileinput-new"> <i class="fa fa-paperclip" aria-hidden="true"></i> </span>
                                            <span class="fileinput-exists"> <i class="fa fa-paperclip" aria-hidden="true"></i> </span>
                                            <input type="file" name="sellerAttachment" id="selAttachmentReply"> 
                                        </span>
                                        <span class="help-block">{{$errors->first('sellerAttachment')}}</span>
                                        </div>
                                    </div>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn blue pull-right">
                                Reply
                                </button>
                            </div>
                          </div>
                            </form>
                        </div> 
                        @endif
                         </div>
                     </div>
               </div>
           </div>
       </div>
   </div>

@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('pages/scripts/profile.min.js')}}" type="text/javascript"></script>
<script src="{{ __common_asset('pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">

</script>
@endpush
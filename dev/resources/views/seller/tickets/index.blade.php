@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
<style>
    .badge{
        position: inherit;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            
                <div class="portlet-title">
                    <div class="caption pd-tb-5">
                        <span>Support Tickets</span>
                    </div>
                    <div class="actions pd-tb-5">
                        <div class="btn-group">
                              <a href="{{route('seller.tickets.add')}}" class="btn btn-sm btn-default tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Add"><i class="fa fa-plus"></i>&nbsp;Raise a Ticket</a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <table class="table table-checkable nowrap" id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                {{-- <th width="2%" class="">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th> --}}
                                <th class="text-center"> ID </th>
                                <th class="text-center"> Created Date </th>
                                <th class="text-center"> Modified Date </th>
                                <th class="text-center"> Subject </th>
                                <th class="text-center"> Category </th>
                                <th class="text-center"> Attachment </th>
                                <th class="text-center"> Status </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">

</script>
@endpush
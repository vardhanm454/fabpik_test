@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ __common_asset('pages/css/profile-2.min.css') }}" rel="stylesheet" type="text/css" />
<style>
.modal-body{
    min-height: 70px;
    max-height: 500px;
    height: 500px;
    overflow-y: auto;
}
    
</style>
@endpush

@section('content')

@if($seller_details->approval_status ==0)
<div class="row">
    <div class="col-md-12">
        <div class="note {{ ($seller_details->final_submission == 0)? 'note-success' : 'note-warning'}}">
            <p class="margin-bottom-10">
            @if($seller_details->final_submission == 0)
                Submit required details to get your account approved.
            @else
                Thank you, our team will get back you soon!
            @endif
            </p>
        </div>
    </div>
</div>
@else
<div class="note note-success">
    <p class="margin-bottom-10">
        Approved by Fabpik.
    </p>
</div>
@endif

<div class="tab-content">
    <div class="tab-pane active" id="tabs_list">
        <div class="row profile-account">
            <div class="col-md-3">
                <ul class="profile-wrap ver-inline-menu tabbable margin-bottom-10 navigation-tabs" id="tabMenu">
                    <li class="active">
                        <a data-toggle="tab" href="#tab_basic_details" aria-expanded="true"><span aria-hidden="true" class="icon-info"></span> Basic Details </a> <span class="after"> </span> </li>
                    
                    <li>
                        <a data-toggle="tab" href="#tab_company_details" aria-expanded="true"><span aria-hidden="true" class="icon-briefcase"></span> Company Details </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#tab_warehouse_details" aria-expanded="true"><span aria-hidden="true" class="icon-pointer"></span> Warehouse Details </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#tab_bank_details" aria-expanded="true"><span aria-hidden="true" class="icon-credit-card"></span> Bank Details </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#tab_poc_details" aria-expanded="true"><span aria-hidden="true" class="icon-call-in"></span> POC Details </a>
                    </li>
                    @if($seller_details->approval_status==1)
                    
                    <li>
                        <a data-toggle="tab" href="#tab_commission_details" aria-expanded="true"><!-- <span aria-hidden="true" class="fa fa-percent"></span> --><span class="percent">%</span> Commission Details </a>
                    </li>
                    
                    <li>
                      <a data-toggle="tab" href="#tab_settings" aria-expanded="true"><span aria-hidden="true" class="fa fa-cog"></span> Settings </a>
                    </li>

                    @endif
                </ul>
                <hr>
                <div class="actions">
                    @php 
                        $isDisabled = '';
                        if( isset($seller_details->final_submission) ){
                            $isDisabled = ($seller_details->final_submission == 1) ? 'disabled' : '';
                        }
                    @endphp
                  @if($seller_details->approval_status==0)
                    <button id="finalSubmitBtn" data-sellerid="{{isset($seller_details->name)?$seller_details->id:''}}" class="btn btn-primary btn-block pd-15" {{ $isDisabled }} ><span aria-hidden="true" class="icon-check"></span> Submit</button>
                  @endif
                </div>
            </div>
            <div class="col-md-9">                
                        <div class="tab-content">
                            <div id="tab_basic_details" class="tab-pane active">
                                <div class="portlet light bordered">
                                    <div class="portlet-body">
                                        <form  id="form_basic_details" role="form" method="post" action="{{route('seller.account.basicDetails')}}">
                                            @csrf
                                            <div class="form-body">
                                                <div class="margin-bottom-5">
                                                    <h4 class="text-primary"><strong>Basic Details</strong></h4>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{($errors->first('name'))?'has-error':''}}">
                                                            <label class="control-label" data-toggle="tooltip" title="Name of the Primary Member">Name</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text"name="name" id="name" value="{{ old('name', isset($seller_details->name)?$seller_details->name:'')}}" class="form-control"  data-toggle="tooltip" title="Name of the Primary Member">
                                                            <span class="help-block name-msg">{{$errors->first('name')}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group  {{($errors->first('email'))?'has-error':''}}">
                                                            <label class="control-label">Email Address</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text"name="email" id="email" value="{{ old('email', isset($seller_details->email)?$seller_details->email:'')}}" class="form-control" data-toggle="tooltip" title="Primary Email Address to login">
                                                            <span class="help-block email-msg">{{$errors->first('email')}}</span> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{($errors->first('mobile'))?'has-error':''}}">
                                                            <label class="control-label">Mobile Number</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text"name="mobile" id="mobile" value="{{ old('mobile', isset($seller_details->mobile)?$seller_details->mobile:'')}}" maxlength="10" class="form-control" data-toggle="tooltip" title="Primary Phone number to login."> 
                                                            <span class="help-block mobile-msg">{{$errors->first('mobile')}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Vendor Id Status</label>
                                                            <input type="text" id="vendor_status" disabled placeholder="<?php if($seller_details->approval_status==0): echo "Not Approved"; else: echo "Approved"; endif; ?>" class="form-control"> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{($errors->first('categories'))?'has-error':''}}">
                                                            <label class="control-label">Product Categories</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            
                                                            <select class="form-control select2-multiple" name="categories[]" id="categories" multiple>
                                                                <option value="">--Select--</option>
                                                                @foreach(\App\Models\Category::where('status',1)->get() as $category)
                                                                <option <?php if(in_array($category->id, old('categories',explode(',',$seller_details->categories)))): echo "selected"; endif; ?> value="{{$category->id}}">{{$category->title}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="help-block categories-msg">{{$errors->first('categories')}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{($errors->first('store_name'))?'has-error':''}}">
                                                            <span class="pull-left">
                                                            <label class="control-label">Store Name
                                                            <span class="required" aria-required="true">*</span>
                                                            </span>
                                                                <span class="pull-right">
                                                                    <a href="#" class="popovers" data-toggle="popover" data-placement="left"  data-trigger="focus" title="" data-content="Pickup name of your store on Fabpik. You can find all your products here on Fabpik." data-html="true">
                                                                    <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                    </a>
                                                                </span>
                                                            </label>
                                                            <input type="text" name="store_name" id="store_name" value="{{ old('store_name', isset($seller_details->store_name)?$seller_details->store_name:'')}}" placeholder="Pickup name of your store on Fabpik" class="form-control" data-toggle="tooltip" title=""> 
                                                            <span class="help-block store-name-msg">{{$errors->first('store_name')}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                   
                                                    <div class="col-md-6">
                                                        <div class="form-group {{($errors->first('brand_name'))?'has-error':''}}" style="position:relative;">
                                                            <span class="pull-left">
                                                                <label class="control-label">Brand Name
                                                                <span class="required" aria-required="true">*</span>
                                                            </span>
                                                            <span class="pull-right">
                                                                    <a href="#" class="popovers" data-toggle="popover" data-placement="left"  data-trigger="focus" title="" data-content="Enter brand name for your products if you sell your own branded products" data-html="true">
                                                                    <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                    </a>
                                                                </span>
                                                            </label>  

                                                            <input type="text" name="brand_name" id="brand_name" value="{{ old('brand_name', isset($seller_details->brand_name)?$seller_details->brand_name:'')}}" autocomplete="off" placeholder='Enter brand name for your products' class="typeahead form-control" data-toggle="tooltip" title="">
                                                            <small><span id="showShopUrl"> e.g. puma, nike</span></small>
                                                            <span class="help-block brand-name-msg">{{$errors->first('brand_name')}}</span>
                                                            
                                                              {{--<select class="form-control" name="brands" id="brands">
                                                                @php  
                                                                $_brand = (old('brands'))?old('brands'):(isset($seller_details)?$seller_details->brand_id:'');
                                                                @endphp
                                                                @if(!$errors->has('brand'))
                                                                @if($_brand)
                                                                    @php $brand = App\Models\Brand::selectRaw("id,name")->find($_brand); @endphp
                                                                    <option value="{{$brand->id}}" data-id="{{$brand->id}}" selected="selected">{{$brand->name}}</option>
                                                                @endif
                                                                @endif
                                                                <input type="hidden" value="{{$brand->id}}" name="hiddenbrand">
                                                             </select>--}}
                                                        </div>
                                                    </div>
                                                   
                                                    
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="{{($errors->first('shop_url'))?'has-error':''}}">
                                                                <label class="control-label">Shop URL</label>
                                                                <span class="required" aria-required="true">*</span>    
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="pull-left" style="padding-top:8px;width:37%">

                                                                        <span class="">{{ env('APP_URL') }}shop/</span>
                                                                        </div>  
                                                                        <div class="pull-left w-30">
                                                                        <input type="text" class="form-control" id="shop_url" name="shop_url" value="{{old('shop_url',  isset($seller_details->url_slug)?$seller_details->url_slug:'')}}">
                                                                        <small><span id="showShopUrl"> e.g. puma, nike</span></small> 
                                                                        </div>  
                                                                    </div>  
                                                                </div>                                         
                                                                <span class="help-block shop-url-msg"> {{$errors->first('shop_url')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                
                                            {{--    <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Do you want to enable queries section with users?<br>
                                                                <small><strong>Note: </strong>If you enable this, users can send their queries through the application.</small></label>
                                                                <div class="mt-radio-inline">
                                                                    <label class="mt-radio">
                                                                        <input type="radio"  <?php if($seller_details->chat_enable==1): echo "checked"; endif; ?> class="chat-modal" name="chat_enable" value="1"> Yes <span></span>
                                                                    </label>
                                                                    <label class="mt-radio">
                                                                        <input type="radio" <?php if($seller_details->chat_enable==0): echo "checked"; endif; ?> class="chat-modal" name="chat_enable" value="0" checked> No <span></span>
                                                                    </label>
                                                                </div>
                                                            <span class="help-block chat-modal-msg"></span>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                            </div><hr>
                                            <div class="form-actions text-right">
                                            @if(!is_null($seller_details->url_slug) && !is_null($seller_details->shop_url) && !is_null($seller_details->brand_name) && !is_null($seller_details->store_name) && !is_null($seller_details->categories) && !is_null($seller_details->categories) &&
                                            !is_null($seller_details->mobile) && !is_null($seller_details->email) && !is_null($seller_details->name) )
                                            <a class="btn blue continue">Next</a>
                                            @endif
                                            <button type="submit" id="basic_submit" class="btn blue form-submit"><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div id="tab_company_details" class="tab-pane">
                                <div class="portlet light bordered">
                                    <div class="portlet-body">
                                        <form  id="form_company_details" role="form" method="post" action="{{route('seller.account.companyDetails')}}" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="isVerified" id="isVerified" value="{{$isVerified}}">
                                                <div class="form-body">
                                                    <div class="margin-bottom-5">
                                                        <h4 class="text-primary"><strong>Company Details</strong></h4>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group {{($errors->first('company_name'))?'has-error':''}}">
                                                                <label class="control-label">Name of Business / Company</label>
                                                                <span class="required" aria-required="true">*</span>
                                                                <input type="text" name="company_name" id="company_name"  value="{{old('company_name', isset($seller_details)?$seller_details->company_name:'')}}" class="form-control">
                                                                <span class="help-block company-name-msg">{{$errors->first('company_name')}}</span> 
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group {{($errors->first('company_reg_no'))?'has-error':''}}">
                                                                <label class="control-label">Company registration number</label>
                                                                <!-- <span class="required" aria-required="true">*</span> -->
                                                                <input type="text" name="company_reg_no" id="company_reg_no" value="<?php if(isset($seller_details->company_reg_no)): echo $seller_details->company_reg_no; endif; ?>" class="form-control" >
                                                                <span class="help-block company-reg-no-msg">{{$errors->first('company_reg_no')}}</span>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{($errors->first('legal_entity'))?'has-error':''}}">
                                                            <label class="control-label">Legal Entity</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <div class="mt-radio-inline">
                                                                <label class="mt-radio">
                                                                    <input type="radio" <?php if($seller_details->legal_entity=='r'): echo "checked"; endif; ?> name="legal_entity" id="registered" class="legal_entity" value="r"> Registered <span> </span>
                                                                </label>
                                                                <label class="mt-radio">
                                                                    <input type="radio" <?php if($seller_details->legal_entity=='n'): echo "checked"; endif; ?> name="legal_entity" id="not_registered" class="legal_entity" value="n"> Not Registered <span> </span>
                                                                </label>
                                                                <span class="help-block legal-entity-msg">{{$errors->first('legal_entity')}}</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group {{($errors->first('gst_registered'))?'has-error':''}}">
                                                            <label class="control-label">GST Registered</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <div class="mt-radio-inline">
                                                                <label class="mt-radio">
                                                                    <input type="radio" <?php if($seller_details->gst_registered==1): echo "checked"; endif; ?> name="gst_registered" id="yes" class="gst_registered" value="1"> Yes <span> </span>
                                                                </label>
                                                                <label class="mt-radio">
                                                                    <input type="radio" <?php if($seller_details->gst_registered==0): echo "checked"; endif; ?> name="gst_registered" id="no" class="gst_registered" value="0"> No <span> </span>
                                                                </label>
                                                                <span class="help-block gst-registered-msg">{{$errors->first('gst_registered')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{($errors->first('company_type'))?'has-error':''}}">
                                                            <label class="control-label">Type of Companies</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <div class="mt-radio-inline" id="type_of_companies" style="height:195px;">                                        
                                                            <div class="" style="{{($seller_details->legal_entity=='n')? 'display:block' : 'display:none'}}" id="individual_company">
                                                                    <label class="mt-radio">
                                                                        <input type="radio" <?php if($seller_details->company_type=='Individual'): echo "checked"; endif; ?> name="company_type" id="individual_company_type" class="company_type" value="Individual"> Individual <span> </span> 
                                                                    </label>
                                                                </div>
                                                                <div class="" style="{{($seller_details->legal_entity=='r')? 'display:block' : 'display:none'}}" id="registered_company">
                                                                    <label class="mt-radio">
                                                                        <input type="radio" <?php if($seller_details->company_type=='Limited Liability Partnership'): echo "checked"; endif; ?> name="company_type" class="company_type" value="Limited Liability Partnership"> Limited Liability Partnership (LLP) <span> </span>
                                                                    </label><br>
                                                                    <label class="mt-radio">
                                                                        <input type="radio" <?php if($seller_details->company_type=='Pvt Ltd'): echo "checked"; endif; ?> name="company_type" class="company_type" value="Pvt Ltd"> Private Limited <span> </span> 
                                                                    </label><br>
                                                                    <label class="mt-radio">
                                                                        <input type="radio" <?php if($seller_details->company_type=='Public Ltd'): echo "checked"; endif; ?> name="company_type" class="company_type" value="Public Ltd"> Public Limited <span> </span> 
                                                                    </label><br>
                                                                    <label class="mt-radio">
                                                                        <input type="radio" <?php if($seller_details->company_type=='Sole Proprietorship'): echo "checked"; endif; ?> name="company_type" class="company_type" value="Sole Proprietorship"> Sole Proprietorship <span> </span> 
                                                                    </label>
                                                                </div>
                                                                <span class="help-block company-type-msg">{{$errors->first('company_type')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group" id="gst_number" style="{{($seller_details->gst_registered==1)?'':'display:none'}}">
                                                            <label class="control-label">GST Number</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" name="gst_no" id="gst_no" value="<?php if(isset($seller_details->gst_no)){ echo $seller_details->gst_no; } ?>" class="form-control"> 
                                                            <span class="help-block gst-no-msg"></span> 
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group" id="gst_certificate" style="{{($seller_details->gst_registered==1)?'':'display:none'}}">
                                                            <label class="control-label">GST Certificate</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="input-group">
                                                                    <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                        <span class="fileinput-filename"> </span>
                                                                    </div>
                                                                    <span class="input-group-addon btn default btn-file">
                                                                        <span class="fileinput-new"> {{ isset($seller_details->gst_certificate) ? 'Change' : 'Select file' }} </span>
                                                                        <input type="file" id="gst_certificate" name="gst_certificate" data-checkstatus="<?php if(isset($seller_details->gst_certificate)): echo 1; else: echo 0; endif; ?>"> </span>
                                                                </div>
                                                            </div>
                                                            @if(isset($seller_details->gst_certificate))
                                                            <a href = "<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/gst_certificate/'.$seller_details->gst_certificate; ?>" target="_blank">{{isset($seller_details->gst_certificate)?'GST Certificate Uploaded':''}}</a>
                                                            @endif
                                                            <span class="help-block gst-certificate-msg"></span>
                                                        {{-- <input type="file" id="gst_certificate" name="gst_certificate" class="form-control" data-checkstatus="<?php if(isset($seller_details->gst_certificate)): echo 1; else: echo 0; endif; ?>">  
                                                            --}} 
                                                            
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Incorporation Certificate</label>
                                                            <!-- <span class="required" aria-required="true">*</span> -->
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="input-group">
                                                                    <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                        <span class="fileinput-filename"> </span>
                                                                    </div>
                                                                    <span class="input-group-addon btn default btn-file">
                                                                        <span class="fileinput-new"> {{ isset($seller_details->incorporation_certificate)?'Change':'Select File' }} </span>
                                                                        <input type="file" id="incorporation_certificate" name="incorporation_certificate" value="{{$seller_details->incorporation_certificate}}" data-checkstatus="<?php if(isset($seller_details->incorporation_certificate)): echo 1; else: echo 0; endif; ?>" {{(($isVerified)?"disabled":'')}}> </span>
                                                                </div>
                                                            </div>
                                                            <span class="help-block incorporation-certificate-msg"></span>
                                                            @if(isset($seller_details->incorporation_certificate))
                                                            <input type="hidden" value="true" name="incorporation_verified" id="incorporation_verified">
                                                            <a href = "<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/incorporation_certificate/'.$seller_details->incorporation_certificate; ?>" target="_blank">{{isset($seller_details->incorporation_certificate)?'Incorporation Certificate Uploaded':''}}</a>
                                                            @endif
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Office Phone Number</label>
                                                            <!-- <span class="required" aria-required="true">*</span> -->
                                                            <input type="text" name="office_phone_no" id="office_phone_no" value="<?php if(isset($seller_details->office_phone_no)){ echo $seller_details->office_phone_no; } ?>"maxlength="10" class="form-control">
                                                            <span class="help-block office-phone-no-msg"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Address</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <textarea class="form-control" id="company_address" name="company_address"rows="3"><?php if(isset($seller_details->company_address)): echo $seller_details->company_address; endif; ?></textarea>
                                                            <span class="help-block company-address-msg"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">State</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <select class="form-control select2" name="company_state" id="company_state">
                                                                <option value="">Select State</option>
                                                            @foreach($states as $state)
                                                                <option <?php if($seller_details->company_state==$state->id): echo "selected"; endif; ?> value="{{$state->id}}">{{$state->name}}</option>
                                                            @endforeach
                                                            </select>
                                                            <span class="help-block company-state-msg"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Pincode</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" name="company_pincode" id="company_pincode" value="<?php if(isset($seller_details->company_pincode)): echo $seller_details->company_pincode; endif; ?>"class="form-control">
                                                            <span class="help-block company-pincode-msg"></span>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                            </div><hr>
                                            <div class="form-actions text-right">
                                                <a class="btn blue back">Prevoius</a>
                                                <a class="btn blue continue">Next</a>
                                                <button type="submit" id="company_submit" class="btn blue form-submit"><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div id="tab_warehouse_details" class="tab-pane">
                                <div class="portlet light bordered">
                                    <div class="portlet-body">
                                        <form  id="form_warehouse_details" role="form" method="post" action="{{route('seller.account.warehouseDetails')}}">
                                                @csrf
                                                <div class="form-body">
                                                    <div class="margin-bottom-5">
                                                        <h4 class="text-primary">
                                                            <span class="pull-left"><strong>Warehouse Details</strong></span>
                                                            <span class="pull-right">
                                                            <button type="button" id="add_new_warehouse" style="" class="btn blue"><span aria-hidden="true" class="icon-plus"></span> Add New Warehouse</button>
                                                            
                                                                <a href="javascript:;" class="popovers text-dark" data-toggle="popover" data-placement="left"  data-trigger="focus" title="" data-content="Note that the following location will be your pickup point for collecting the packages. The Default Location is from where the package will be picked up.">
                                                                    <span aria-hidden="true" class="icon-question text-danger"></span>
                                                                </a>
                                                            </span>
                                                        </h4>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <hr>
                                                <input type="hidden" name="warehouse_id" value="" id="warehouse_id">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Address Type</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" name="warehouse_name" id="warehouse_name" class="form-control" value=""  required>
                                                            <small>Ex: Bombay-AndheriWarehouse</small>
                                                            <span class="help-block warehouse-name-msg"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" name="warehouse_phone" id="warehouse_phone" value=""class="form-control"> 
                                                            <span class="help-block warehouse-phone-msg"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{(Session::has('address'))?'has-error':''}}">
                                                            <label class="control-label">Address Line 1</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <textarea class="form-control" name="warehouse_address" id="warehouse_address" placeholder="House no / Flat no, Building Name" value=""rows="1"></textarea> 
                                                            <span class="help-block warehouse-address-msg">{{Session::get('address')}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{(Session::has('addresstwo'))?'has-error':''}}">
                                                            <label class="control-label">Address Line 2</label>
                                                            <input type="text" name="warehouse_addresstwo" id="warehouse_addresstwo" class="form-control" placeholder="Location / Street / Area" value="" required>
                                                            <small>Ex: Hitech City, etc.</small>
                                                            <span class="help-block warehouse-addresstwo-msg">{{Session::get('addresstwo')}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Landmark</label>
                                                            <input type="text" name="warehouse_landmark" id="warehouse_landmark" class="form-control" value="" required>
                                                            <small>Ex: Near Image Hospital, etc.</small>
                                                            <span class="help-block warehouse-landmark-msg"> </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Postal Code</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" name="warehouse_pincode" id="warehouse_pincode" value=""class="form-control"> 
                                                            <span class="help-block warehouse-pincode-msg"></span>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">City</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" name="warehouse_city" id="warehouse_city" class="form-control" value="" required>
                                                            <span class="help-block warehouse-city-msg"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">State</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <select class="form-control" name="warehouse_state" id="warehouse_state" required>
                                                                <option value="">Select State</option>
                                                            @foreach($states as $state)
                                                                <option value="{{$state->id}}">{{$state->name}}</option>
                                                            @endforeach
                                                            </select>
                                                            <span class="help-block warehouse-state-msg"></span>
                                                        </div>
                                                    </div>
                                                   
                                                    
                                                </div>
                                                <div class="row">
                                                    <input type="hidden" name="warehouse_country" id="warehouse_country" value=""class="form-control" readonly> 
                                                   
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Make It Default</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <div class="mt-radio-inline">
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="is_default" class="is_default" id="is_default_1" value="1"> Yes
                                                                </label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" name="is_default" class="is_default" id="is_default_0" value="0"> No
                                                                </label>
                                                                <span class="help-block is-default-msg"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="form-actions text-right"> 
                                                    <a class="btn blue back">Prevoius</a>
                                                    <a class="btn blue continue">Next</a>
                                                        <button type="button" id="warehouse_submit" class="btn blue form-submit"><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                                </div>
                                        </form>
                                            <hr>
                                            <div class="row">
                                            @if(!empty($seller_warehouse_details))
                                            @foreach($seller_warehouse_details as $seller_warehouse)
                                            <div class="col-md-6">
                                                <div class="panel panel-default">
                                                    <div class="panel panel-heading">
                                                        <div class="row">
                                                            <div class="col-md-8 pd-tb-10"> <span class="bold uppercase"><?php echo $seller_warehouse->name; if($seller_warehouse->is_default==1): echo " (Default)"; endif; ?></span> 
                                                            </div>
                                                            <div class="col-md-4 text-right">
                                                                <a class="btn btn-circle btn-icon-only btn-default edit_warehouse" href="javascript:;" data-warehouseid="{{$seller_warehouse->id}}"> <i class="icon-pencil"></i> </a>
                                                                <a class="btn btn-circle btn-icon-only btn-default delete_warehouse" data-warehouseid="{{$seller_warehouse->id}}" href="javascript:;"> <i class="icon-trash"></i> </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-body">
                                                        <?php echo $seller_warehouse->address; ?>,<br>
                                                        <p> <?php echo $seller_warehouse->address_two; ?>,
                                                        <?php echo $seller_warehouse->landmark; ?>, 
                                                        <?php echo $seller_warehouse->city; ?>, 
                                                        <?php $state =  \App\Models\State::where('id', $seller_warehouse->state_id)->select('name')->first() ?>
                                                        <?php echo $state->name; ?> <br> 
                                                        <?php echo $seller_warehouse->pincode; ?>. </p>
                                                        <?php echo $seller_warehouse->phone; ?> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            @endif
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab_bank_details" class="tab-pane">
                                <div class="portlet light bordered">
                                    <div class="portlet-body">
                                        <form  id="form_bank_details" role="form" method="post" action="{{route('seller.account.bankDetails')}}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-body">
                                                <div class="margin-bottom-5">
                                                    <h4 class="text-primary"><strong>Bank Details</strong></h4>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Name of Account Holder</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" id="bank_account_name"name="bank_account_name" value="<?php if(isset($seller_details->bank_account_name)): echo $seller_details->bank_account_name; endif; ?>" class="form-control">
                                                            <span class="help-block bank-account-name-msg"></span>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Bank Name</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" id="bank_name"name="bank_name" value="<?php if(isset($seller_details->bank_name)): echo $seller_details->bank_name; endif; ?>" class="form-control">
                                                            <span class="help-block bank-name-msg"></span>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Account Number</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" id="bank_account_no"name="bank_account_no" value="<?php if(isset($seller_details->bank_account_no)): echo $seller_details->bank_account_no; endif; ?>" class="form-control">
                                                            <span class="help-block bank-account-no-msg"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Confirm Account Number</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="password" id="confirm_account_no"name="confirm_account_no" value="<?php if(isset($seller_details->bank_account_no)): echo $seller_details->bank_account_no; endif; ?>" class="form-control" oncopy="return false" onpaste="return false">
                                                            <span class="help-block confirm-account-no-msg"></span>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">IFSC Code</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" id="ifsc_code"name="ifsc_code" value="<?php if(isset($seller_details->ifsc_code)): echo $seller_details->ifsc_code; endif; ?>" class="form-control">
                                                            <span class="help-block ifsc-code-msg"></span>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Swift Code</label>
                                                            <input type="text" id="swift_code"name="swift_code" value="<?php if(isset($seller_details->swift_code)): echo $seller_details->swift_code; endif; ?>" class="form-control">
                                                            <span class="help-block swift-code-msg"></span>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Branch Name</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <input type="text" id="branch_name"name="branch_name" value="<?php if(isset($seller_details->branch_name)): echo $seller_details->branch_name; endif; ?>" class="form-control">
                                                            <span class="help-block branch-name-msg"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Upload Cancelled Cheque</label>
                                                            <span class="required" aria-required="true">*</span>                                          
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="input-group">
                                                                    <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                        <span class="fileinput-filename"> </span>
                                                                    </div>
                                                                    <span class="input-group-addon btn default btn-file">
                                                                        <span class="fileinput-new"> Select file </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" id="cancelled_cheque_file" name="cancelled_cheque_file" data-checkstatus="<?php if(isset($seller_details->cancelled_cheque_file)): echo 1; else: echo 0; endif; ?>"> </span>
                                                                </div>
                                                            </div>
                                                            <span class="help-block cancelled-cheque-file-msg"></span>
                                                            @if(isset($seller_details->cancelled_cheque_file))
                                                            <input type = "hidden" name="cheque_uploded" value="{{$seller_details->cancelled_cheque_file}}">
                                                                <a href = "<?php echo URL::to('/').'/'.UPLOAD_PATH.'/seller/cancelled_cheque_files/'.$seller_details->cancelled_cheque_file; ?>" target="_blank">{{isset($seller_details->cancelled_cheque_file)?'uploaded Cancelled Cheque ':''}}</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                            </div><hr>
                                            <div class="form-actions text-right">
                                                <a class="btn blue back">Prevoius</a>
                                                <a class="btn blue continue">Next</a>
                                            <button type="button" id="bank_submit" class="btn blue form-submit"><span aria-hidden="true" class="icon-cloud-download"></span> Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div id="tab_poc_details" class="tab-pane">
                                <div class="portlet light bordered">
                                    <div class="portlet-body">
                                        <form  id="form_poc_details" role="form" method="post" action="{{route('seller.account.pocDetails')}}">
                                            @csrf
                                            <div class="form-body">
                                                <div class="margin-bottom-5">
                                                    <h4 class="text-primary"><strong>POC Details</strong></h4>
                                                </div>
                                                <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label class="control-label">Individual Representing Company</label>
                                                    <span class="required" aria-required="true">*</span>
                                                    <input type="text" id="poc_name"name="poc_name" value="<?php if(isset($seller_details->poc_name)): echo $seller_details->poc_name; endif; ?>" class="form-control">
                                                    <span class="help-block poc-name-msg"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label class="control-label">Designation of the Individual</label>
                                                    <span class="required" aria-required="true">*</span>
                                                    <input type="text" id="poc_designation"name="poc_designation" value="<?php if(isset($seller_details->poc_designation)): echo $seller_details->poc_designation; endif; ?>" class="form-control">
                                                    <span class="help-block poc-designation-msg"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label class="control-label">Individual Email Id</label>
                                                    <span class="required" aria-required="true">*</span>
                                                    <input type="text" id="poc_email"name="poc_email" value="<?php if(isset($seller_details->poc_email)): echo $seller_details->poc_email; endif; ?>" class="form-control">
                                                    <span class="help-block poc-email-msg"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label class="control-label">Phone Number</label>
                                                    <span class="required" aria-required="true">*</span>
                                                    <input type="text" id="poc_mobile"name="poc_mobile" value="<?php if(isset($seller_details->poc_mobile)): echo $seller_details->poc_mobile; endif; ?>" class="form-control">
                                                    <span class="help-block poc-mobile-msg"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label class="control-label">Alternate Phone Number</label>
                                                    <span class="required" aria-required="true">*</span>

                                                    <input type="text" id="poc_phone" name="poc_phone" value="<?php if(isset($seller_details->poc_phone)): echo $seller_details->poc_phone; endif; ?>" class="form-control">
                                                    <span class="help-block poc-phone-msg"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <hr>
                                            <div class="form-actions text-right">
                                                <a class="btn blue back">Prevoius</a>
                                                <a class="btn blue continue">Next</a>
                                                <button type="button" id="poc_submit" class="btn blue form-submit"><span aria-hidden="true" class="icon-cloud-download"></span> Save & Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div id="tab_commission_details" class="tab-pane">
                                <div class="portlet light bordered">
                                    <div class="portlet-body">
                                        <div class="form-body">
                                            <div class="margin-bottom-5">
                                                <h4 class="text-primary"><strong>Commission</strong></h4>
                                            </div>
                                            <hr>
                                            <form  id="form_commission_details" role="form" method="post" action="{{ route('seller.account.acceptCommissionDetails') }}">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Commission Rate (%):</label>
                                                            <strong>
                                                                <?php if(isset($seller_details->commission)): echo $seller_details->commission; endif; ?>
                                                            </strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">                                            
                                                            <div class="mt-checkbox-list">
                                                                <label class="control-label mt-checkbox mt-checkbox-outline">
                                                                    <input type="checkbox" name="is_accept" id="is_accept" value="1" <?php if(isset($seller_details->commission_accept)): echo (($seller_details->commission_accept == 1)?'checked':'') ; endif; ?> >
                                                                    I agree to the 
                                                                    <a href="#" 
                                                                    data-id= "{{isset($seller_details->commission_type)? (($seller_details->commission_type == 'f')?'f':'v') : ''}}" class='termsandconditions'>terms & conditions</a>
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                            <span class="help-block commission-accept-msg"></span>
                                                        </div>
                                                        <span><?php if(isset($seller_details->commission_accept)): echo (($seller_details->commission_accept == 1)?'<b>Thanks For Your Acceptence</b>':'') ; endif; ?></span>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-actions text-right">
                                                    <a class="btn blue back">Prevoius</a>
                                                    <button type="button" id="commission_accept" class="btn blue form-submit" {{($seller_details->commission_accept == 1) ? 'disabled' : ''}}><span aria-hidden="true" class="fa fa-check-circle"></span> 
                                                    
                                                    Submit
                                                    
                                                    </button>
                                                </div>
                                            </form>                                 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab_settings" class="tab-pane">
                                <div class="portlet light bordered">
                                    <div class="portlet-body">
                                        <div class="form-body">
                                            <div class="margin-bottom-5">
                                                <h4 class="text-primary"><strong>Cash On Delivery</strong></h4>
                                            </div>
                                            <hr>
                                            <form  id="form_change_cod_details" role="form" method="post" action="{{ route('seller.account.changeCodDetails') }}">
                                                @csrf
                                                <!--
                                                    if record exists in seller_setting_change_logs table by the seller id and "approved_by", "approved_at" is null
                                                    will show the undo button with warning message.
                                                -->
                                                @foreach($settingLogs as $log)
                                                @if($log->settings_column_name == "cod_enable")
                                                <div class="row note note-warning">
                                                <div class="">
                                                    <div class="col-md-10 col-lg-10">
                                                        
                                                            <p class="margin-bottom-10">
                                                            You have requested for COD <strong>{{($log->new_value=='y')?'Enable':'Disable'}}</strong> on <strong>{{date('d-m-Y h:i a', strtotime($log->created_at))}}</strong>, Once approved by admin it will reflect in your portal.
                                                            </p>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2">

                                                        <div class="form-actions text-right">
                                                        <input type = "hidden" value="{{$log->id}}" name="request_id">
                                                        <button type="submit" id="undo_cod_change" class="btn blue settings-form-submit" value="undo" name="undo"><span aria-hidden="true" class="fa fa-undo" ></span> Undo Changes</button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                            
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Want to enable Cash On Delivery for your products?</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <div class="mt-radio-inline">
                                                            
                                                            <label class="mt-radio">
                                                                <input type="radio" <?php if($seller_details->cod_enable=='y'): echo "checked"; endif; ?> class="cod-enable" name="cod_enable" value="y" checked> Yes <span></span>
                                                            </label>
                                                            <label class="mt-radio">
                                                                <input type="radio" <?php if($seller_details->cod_enable=='n'): echo "checked"; endif; ?> class="cod-enable" name="cod_enable" value="n"> No <span></span>
                                                            </label>
                                                            </div>
                                                            <input type = "hidden" value="cod_enable" name="cod_column_name">
                                                            <input type = "hidden" value="{{$seller_details->cod_enable}}" name="cod_old_value">
                                                            <span class="help-block cod-enable-value-msg"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-actions text-right">
                                                    <a class="btn blue back">Prevoius</a>
                                                    <button type="submit" id="cod_change" class="btn blue form-submit" name="save" value="save"><span aria-hidden="true" class="fa fa-check-circle" ></span> Submit</button>
                                                </div>
                                            </form>                                 
                                        </div>
                                
                                    </div>
                                </div>

                                <div class="portlet light bordered">
                                    <div class="portlet-body">
                                        <div class="form-body">
                                            <div class="margin-bottom-5">
                                                <h4 class="text-primary"><strong>Shipping Model</strong></h4>
                                            </div>
                                            <hr>
                                            <form  id="form_change_shipping_details" role="form" method="post" action="{{ route('seller.account.changeShippingDetails') }}">
                                                @csrf
                                                <!--
                                                    if record exists in seller_setting_change_logs table by the seller id and "approved_by", "approved_at" is null
                                                    will show the undo button with warning message.
                                                -->
                                                @foreach($settingLogs as $log)
                                                @if($log->settings_column_name == "shipping_model")
                                                <div class="row note note-warning">
                                                <div class="">
                                                    <div class="col-md-10 col-lg-10">
                                                   
                                                            <p class="">
                                                            You have requested for change the Shipping Model to<strong> {{($log->new_value=='f')?'Fabship':'Self'}}</strong> on <strong>{{date('d-m-Y h:i a', strtotime($log->created_at))}}</strong>, Once approved by admin it will reflect in your portal.
                                                            </p>
                                                        </div>
                                                        <div class="col-md-2 col-lg-2">
                                                        <div class="form-actions text-right">
                                                        <input type = "hidden" value="{{$log->id}}" name="request_id">
                                                        <button type="submit" id="undo_shipping_change" class="btn blue settings-form-submit" value="undo" name="undo"><span aria-hidden="true" class="fa fa-undo" ></span> Undo Changes</button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                            
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Want to change your shipping Model?</label>
                                                            <span class="required" aria-required="true">*</span>
                                                            <div class="mt-radio-inline">
                                                                <label class="mt-radio">
                                                                    <input type="radio" <?php if($seller_details->shipping_model=='f'): echo "checked"; endif; ?> class="shipping-modal" name="shipping_model" value="f" checked> Fabship <span></span>
                                                                </label>
                                                                <label class="mt-radio">
                                                                    <input type="radio" <?php if($seller_details->shipping_model=='s'): echo "checked"; endif; ?> class="shipping-modal" name="shipping_model" value="s"> Self <span></span>
                                                                </label>
                                                            </div>
                                                            <input type = "hidden" value="shipping_model" name="shipping_model_column_name">
                                                            <input type = "hidden" value="{{$seller_details->shipping_model}}" name="shipping_model_old_value">
                                                            <span class="help-block shipping-modal-msg"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-actions text-right">
                                                    <a class="btn blue back">Prevoius</a>
                                                    <button type="submit" id="cod_change" class="btn blue settings-form-submit" name="save" value="save"><span aria-hidden="true" class="fa fa-check-circle" ></span> Submit</button>
                                                </div>
                                            </form>                                 
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    
            </div>
            <!--end col-md-9-->
        </div>
    </div>
    <!--end tab-pane-->
  </div>

  <!-- Modal -->
<div id="modaltermsandconditions" class="modal fade" role="dialog">
  <div class="modal-dialog  modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Terms And Condtions</h4>
      </div>
      <div class="modal-body"></div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/autosize/autosize.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('pages/scripts/components-form-tools.min.js') }}" type="text/javascript"></script>
<script src="{{ __common_asset('pages/scripts/form-validation.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCea7use-zSkFZxumDrado3k8WSHPbUlk&libraries=places"></script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">

    google.maps.event.addDomListener(window, 'load', function () {
        
        address1Field = document.querySelector("#warehouse_address");
        // address2Field = document.querySelector("#warehouse_landmark");
        address2Field = document.querySelector("#warehouse_addresstwo");
        cityField = document.querySelector("#warehouse_city");
        countryField = document.querySelector("#warehouse_country");
        postalField = document.querySelector("#warehouse_pincode");
        stateField = document.querySelector("#warehouse_state");
        

        var places = new google.maps.places.Autocomplete(address1Field, {
            // componentRestrictions: { country: ["in"] },
            // fields: ["address_components", "geometry"],
            // types: ["address"],
        });

        google.maps.event.addListener(places, 'place_changed', function () {

            // Get the place details from the autocomplete object.
            const place = places.getPlace();
            
            let address1 = "";
            let postcode = "";
            let city = "";
            let state = "";
            let country = "";
                
            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            // place.address_components are google.maps.GeocoderAddressComponent objects
            // which are documented at http://goo.gle/3l5i5Mr
            
            // console.log(place.address_components);
            for (const component of place.address_components) {
                const componentType = component.types[0];
                // console.log(componentType);
                switch (componentType) {
                    case "sublocality_level_2":{
                        address1 = `${component.long_name}, ${address1}`;
                        break;
                    }
                    case "sublocality_level_1":{
                        address1 += `${component.long_name}`;
                        break;
                    }

                    case "postal_code": {
                        postcode = `${component.long_name}${postcode}`;
                        break;
                    }

                    case "postal_code_suffix": {
                        postcode = `${postcode}-${component.long_name}`;
                        break;
                    }

                    case "locality":{
                        city = component.long_name;
                        break;
                    }
                    
                    case "administrative_area_level_1": {
                        state = component.long_name;
                        break;
                    }
                    
                    case "country":{
                        country = component.long_name;
                        break;
                    }
                }
            }
       
            address1Field.value = place.name + ', ' + address1;
            cityField.value = city;
            // countryField.value = country;
            postalField.value = postcode;
            
            $("#warehouse_state option").each(function() {
                if($(this).text() == state) {
                    $(this).attr('selected', 'selected');            
                }                        
            });
            
            // stateField.value = state;

            // After filling the form with address components from the Autocomplete
            // prediction, set cursor focus on the second address line to encourage
            // entry of subpremise information such as apartment, unit, or floor number.
            address2Field.focus();
        });
    });

    $('#brands').change(function(){
        //get the selected option's data-id value using jquery `.data()`
        var selected_brand_id =  $(':selected',this).val(); 
        var selected_brand_name =  $(':selected',this).text(); 
        //   alert(selected_brand_id);
        //   alert(selected_brand_name);
        //populate the rate.
        // $('.criteria_rate').val(criteria_rate);
    });

    $('.termsandconditions').click(function(){
    
        var type = $(this).data('id');
        var commission_name = '';
        if(type == 'v'){
            commission_name = 'variablecommission';
        }else if(type == 'f'){           
            commission_name = 'fixedcommission';
        }
        // alert(commission_name);
        // AJAX request
        $.ajax({
            url: "{{route('seller.account.getCommissionDetails')}}",
            type: 'get',
            dataType: "JSON",
            data: {commission_name: commission_name},
            success: function(response){ 
                // console.log(response.polices);
            // Add response in Modal body
            $('.modal-body').html(response.polices.description);
            // Display Modal
            $('#modaltermsandconditions').modal('show'); 
            }
        });
    });

    var re = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;
    var pincodeRegex = /^(\d{4}|\d{6})$/;
    var is_verified = $('#isVerified').val();
    var pocMobileRegex = /^[6789]\d{9}$/;
        
    //Basic Details Form Validations and submit form
    // $('#basic_submit').click(function(){
        //     var name = $('#name').val();
        //     if(name==''){
        //         $('#name').parent('div.form-group').addClass('has-error');
        //         $('.name-msg').text('Please Enter Name.');
        //         $('#name').focus();
        //         return false;
        //     }else{
        //         if(name.length<=3){
        //             $('#name').parent('div.form-group').addClass('has-error');
        //             $('.name-msg').text('Please Enter more than 3 characters.');
        //             $('#name').focus();
        //             return false;
        //         }else{
        //             var nameRegex = /^[a-zA-Z]+$/;
        //             // var fullnameRegex = /^[a-zA-Z]+ [a-zA-Z]+$/;
        //             var fullnameRegex = /^[a-zA-Z ]+$/;
        //             if(!name.match(fullnameRegex)){
        //                 if(!name.match(nameRegex)){
        //                     $('#name').parent('div.form-group').addClass('has-error');
        //                     $('.name-msg').text('Please Enter Valid Name.');
        //                     $('#name').focus();
        //                     return false;
        //                 } 
        //             }
        //         }
        //     }

        //     var email = $('#email').val();
        //     var emailRegex = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        //     if(email==''){
        //         $('#email').parent('div.form-group').addClass('has-error');
        //         $('.email-msg').text('Please Enter Email.');
        //         $('#email').focus();
        //         return false;
        //     }else{
        //         if(!email.match(emailRegex)){
        //             $('#email').parent('div.form-group').addClass('has-error');
        //             $('.email-msg').text('Please Enter Valid Email.');
        //             $('#email').focus();
        //             return false;
        //         }
        //     }

        //     var mobile = $('#mobile').val();
        //     var mobileRegex = /[0-9]{10}/;
        //     if(mobile==''){
        //         $('#mobile').parent('div.form-group').addClass('has-error');
        //         $('.mobile-msg').text('Please Enter Mobile.');
        //         $('#mobile').focus();
        //         return false;
        //     }else{

        //         if(!mobile.match(mobileRegex)){
        //             $('#mobile').parent('div.form-group').addClass('has-error');
        //             $('.mobile-msg').text('Please Enter Valid Number.');
        //             $('#mobile').focus();
        //             return false;
        //         }
        //     }

        //     var categories = $('#categories').val();
        //     if(categories=='' || categories==null){
        //         $('#categories').parent('div.form-group').addClass('has-error');
        //         $('.categories-msg').text('Please Select Categories.');
        //         $('#categories').focus();
        //         return false;
        //     }
        
        
        //     // var shipping_model = $("input[name='shipping_model']:checked ").val();
        //     // if(typeof shipping_model === "undefined"){
        //     //     $('#shipping_model').parent('div.form-group').addClass('has-error');
        //     //     $('.shipping-modal-msg').text('Please Select.');
        //     //     $('#shipping_model').focus();
        //     //     return false;
        //     // }

        //     var brand_name = $('#brand_name').val();
        //     // var brandNameRegex =  /^[A-Za-z][A-Za-z0-9]*(?:[A-Za-z0-9-\s]+)*$/;
        //     if(brand_name == ''){
        //         $('#brand_name').parent('div.form-group').addClass('has-error');
        //         $('.brand-name-msg').text('Please Enter Brand Name.');
        //         $('#brand_name').focus();
        //         return false;
        //     }
        //     // else if(!brand_name.match(brandNameRegex)){
        //     //     $('#brand_name').parent('div.form-group').addClass('has-error');
        //     //     $('.brand-name-msg').text('Please Enter Valid Brand Name.');
        //     //     $('#brand_name').focus();
        //     //     return false;
        //     // }

        //     var store_name = $('#store_name').val();
        //     var storeNameRegex =  /^[A-Za-z][A-Za-z0-9]*(?:[A-Za-z0-9-\s]+)*$/;
        //     if(store_name == ''){
        //         $('#store_name').parent('div.form-group').addClass('has-error');
        //         $('.store-name-msg').text('Please Enter Store Name.');
        //         $('#store_name').focus();
        //         return false;
        //     }
        //     else if(!store_name.match(storeNameRegex)){
        //         $('#store_name').parent('div.form-group').addClass('has-error');
        //         $('.store-name-msg').text('Please Enter Valid Store Name.');
        //         $('#store_name').focus();
        //         return false;
        //     }

        //     var shop_url = $('#shop_url').val();
        //     // var shopNameRegex = /^[A-Za-z0-9-]*$/;
        //     var shopNameRegex = /^[A-Za-z][A-Za-z0-9]*(?:[A-Za-z0-9-]+)*$/;
        //     if(shop_url == ''){
        //         $('#shop_url').parent('div.form-group').addClass('has-error');
        //         $('.shop-url-msg').text('Please Enter Shop URL.');
        //         $('#shop_url').focus();
        //         return false;
        //     }
        //     if(!shop_url.match(shopNameRegex)){
        //         $('#shop_url').parent('div.form-group').addClass('has-error');
        //         $('.shop-url-msg').text('Please Enter Valid Shop URL.');
        //         $('#shop_url').focus();
        //         return false;
        //     }


        //     // var chat_enable = $("input[name='chat_enable']:checked ").val();
        //     // if(typeof chat_enable === "undefined"){
        //     //     $('#chat_enable').parent('div.form-group').addClass('has-error');
        //     //     $('.chat-modal-msg').text('Please Select.');
        //     //     $('#chat_enable').focus();
        //     //     return false;
        //     // }
            
        //     $('#form_basic_details').submit();
            
    // });

    $('#commission_accept').click(function(){
        var is_accept = $("input[name='is_accept']:checked").val()
        if(is_accept == undefined){
            $('#is_accept').parent('div.form-group').addClass('has-error');
            $('.commission-accept-msg').text('Please Agree the Terms & Conditions.');
            $('#is_accept').focus();
            return false;
        }
        $('#form_commission_details').submit();
    });

    //POC Details Form Validations and submit form
    $('#poc_submit').click(function(){
        var poc_name = $('#poc_name').val();
        if(poc_name==''){
            $('#poc_name').parent('div.form-group').addClass('has-error');
            $('.poc-name-msg').text('Please Enter Individual Name.');
            $('#poc_name').focus();
            return false;
        }else{
            if(poc_name.length<=3){
                $('#poc_name').parent('div.form-group').addClass('has-error');
                $('.poc-name-msg').text('Please Enter more than 3 characters.');
                $('#poc_name').focus();
                return false;
            }else{
                // var pocNameRegex = /^[a-zA-Z]+$/;
                // var pocFullnameRegex = /^[a-zA-Z]+ [a-zA-Z]+$/;
                // if(!poc_name.match(pocFullnameRegex)){
                //     if(!poc_name.match(pocNameRegex)){
                //         $('#poc_name').parent('div.form-group').addClass('has-error');
                //     $('.poc-name-msg').text('Please Enter Valid Name.');
                //     $('#poc_name').focus();
                //     return false;
                //     } 
                // }
            }
        }

        // var poc_designation = $('#poc_designation').val();
        // if(poc_designation==''){
        //     $('#poc_designation').parent('div.form-group').addClass('has-error');
        //     $('.poc-designation-msg').text('Please Enter Designation.');
        //     $('#poc_designation').focus();
        //     return false;
        // }

        var poc_email = $('#poc_email').val();
        var pocEmailRegex = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        if(poc_email==''){
            $('#poc_email').parent('div.form-group').addClass('has-error');
            $('.poc-email-msg').text('Please Enter POC Email.');
            $('#poc_email').focus();
            return false;
        }else{
            if(!poc_email.match(pocEmailRegex)){
                $('#poc_email').parent('div.form-group').addClass('has-error');
                $('.poc-email-msg').text('Please Enter Valid Email.');
                $('#poc_email').focus();
                return false;
            }
        }

        var poc_mobile = $('#poc_mobile').val();
        if(poc_mobile==''){
            $('#poc_mobile').parent('div.form-group').addClass('has-error');
            $('.poc-mobile-msg').text('Please Enter POC Mobile.');
            $('#poc_mobile').focus();
            return false;
        }else{
            if(!pocMobileRegex.test(poc_mobile)){
                $('#poc_mobile').parent('div.form-group').addClass('has-error');
                $('.poc-mobile-msg').text('Please Enter Valid Number.');
                $('#poc_mobile').focus();
                return false;
            }
        }

        // var poc_phone = $('#poc_phone').val();
        // if(poc_phone!=''){
        //     $('#poc_phone').parent('div.form-group').addClass('has-error');
        //     $('.poc-phone-msg').text('Please Enter Alternate Number.');
        //     $('#poc_phone').focus();
        //     return false; 
        // }else if(!pocMobileRegex.test(poc_phone)){
        //     $('#poc_phone').parent('div.form-group').addClass('has-error');
        //     $('.poc-phone-msg').text('Please Enter Valid Alternate Number.');
        //     $('#poc_phone').focus();
        //     return false;
        // }

        $('#form_poc_details').submit();
    });

    //Bank Details Form Validations and submit form
    $('#bank_submit').click(function(){
        var bank_account_name = $('#bank_account_name').val();
        if(bank_account_name==''){
            $('#bank_account_name').parent('div.form-group').addClass('has-error');
            $('.bank-account-name-msg').text('Please Enter Account Name.');
            $('#bank_account_name').focus();
            return false;
        }else{
            if(bank_account_name.length<=3){
                $('#bank_account_name').parent('div.form-group').addClass('has-error');
                $('.bank-account-name-msg').text('Please Enter more than 3 characters.');
                $('#bank_account_name').focus();
                return false;
            }
        }

        var bank_name = $('#bank_name').val();
        if(bank_name==''){
            $('#bank_name').parent('div.form-group').addClass('has-error');
            $('.bank-name-msg').text('Please Enter Bank Name.');
            $('#bank_name').focus();
            return false;
        }

        var bank_account_no = $('#bank_account_no').val();
        var bankAccountNoRegex = /^\d{9,18}$/;
        if(bank_account_no==''){
            $('#bank_account_no').parent('div.form-group').addClass('has-error');
            $('.bank-account-no-msg').text('Please Enter Bank Account No.');
            $('#bank_account_no').focus();
            return false;
        }else if(!bankAccountNoRegex.test(bank_account_no)){
            $('#bank_account_no').parent('div.form-group').addClass('has-error');
            $('.bank-account-no-msg').text('Please Enter Valid Bank Account No.');
            $('#bank_account_no').focus();
            return false;
        }

        var confirm_account_no = $('#confirm_account_no').val();
        var confirmAccountNoRegex = /^\d{9,18}$/;
        if(confirm_account_no==''){
            $('#confirm_account_no').parent('div.form-group').addClass('has-error');
            $('.confirm-account-no-msg').text('Please Confirm Bank Account No.');
            $('#confirm_account_no').focus();
            return false;
        } else if(!confirmAccountNoRegex.test(confirm_account_no)){
            $('#confirm_account_no').parent('div.form-group').addClass('has-error');
            $('.confirm-account-no-msg').text('Please Enter Valid Bank Account No.');
            $('#confirm_account_no').focus();
            return false;
        } else if(confirm_account_no!=bank_account_no){
            $('#confirm_account_no').parent('div.form-group').addClass('has-error');
            $('.confirm-account-no-msg').text('Different Account No.');
            $('#confirm_account_no').focus();
            return false;
        }

        var ifsc_code = $('#ifsc_code').val();
        if(ifsc_code==''){
            $('#ifsc_code').parent('div.form-group').addClass('has-error');
            $('.ifsc-code-msg').text('Please Enter IFSC Code.');
            $('#ifsc_code').focus();
            return false;
        }

        var swift_code = $('#swift_code').val();
        // if(swift_code==''){
        //     $('#swift_code').parent('div.form-group').addClass('has-error');
        //     $('.swift-code-msg').text('Please Enter Swift Code.');
        //     $('#swift_code').focus();
        //     return false;
        // }

        var branch_name = $('#branch_name').val();
        if(branch_name==''){
            $('#branch_name').parent('div.form-group').addClass('has-error');
            $('.branch-name-msg').text('Please Enter Branch Name.');
            $('#branch_name').focus();
            return false;
        }

        // var cancelled_cheque_file = $('#cancelled_cheque_file').val();
        // if(cancelled_cheque_file==''){
        //     if($('#cancelled_cheque_file').data('checkstatus')==0){
        //         $('#cancelled_cheque_file').parent('div.form-group').addClass('has-error');
        //         $('.cancelled-cheque-file-msg').text('Please Upload Cancelled Cheque.');
        //         $('#cancelled_cheque_file').focus();
        //         return false;
        //     }
        // }else{
        //     if (!re.exec(cancelled_cheque_file)) {
        //         $('#cancelled_cheque_file').parent('div.form-group').addClass('has-error');
        //         $('.cancelled-cheque-file-msg').text('Please Upload jpeg/png/jpg/pdf Only.');
        //         $('#cancelled_cheque_file').focus();
        //         return false;
        //     }
        // }

        $('#form_bank_details').submit();
    });

    //Company Details Form Validations and submit form
    // $('#company_submit').click(function(){
        //     var company_name = $('#company_name').val();
        //     if(company_name==''){
        //         $('#company_name').parent('div.form-group').addClass('has-error');
        //         $('.company-name-msg').text('Please Enter Company Name.');
        //         $('#company_name').focus();
        //         return false;
        //     }else{
        //         if(company_name.length<=3){
        //             $('#company_name').parent('div.form-group').addClass('has-error');
        //             $('.company-name-msg').text('Please Enter more than 3 characters.');
        //             $('#company_name').focus();
        //             return false;
        //         }
        //     }

        //     // var company_reg_no = $('#company_reg_no').val();
        //     // if(company_reg_no==''){
        //     //     $('#company_reg_no').parent('div.form-group').addClass('has-error');
        //     //     $('.company-reg-no-msg').text('Please Enter Company Reg. No.');
        //     //     $('#company_reg_no').focus();
        //     //     return false;
        //     // }

        //     var legal_entity = $('#legal_entity').val();
        //     if(legal_entity==''){
        //         $('#legal_entity').parent('div.form-group').addClass('has-error');
        //         $('.legal-entity-msg').text('Please pick Legal Entity.');
        //         $('#legal_entity').focus();
        //         return false;
        //     }

        //     var company_type =  $("input[name='company_type']:checked ").val();
        //     if(typeof company_type === "undefined"){
        //         $('#company_type').parent('div.form-group').addClass('has-error');
        //         $('.company-type-msg').text('Please pick your Company Type.');
        //         $('#company_type').focus();
        //         return false;
        //     }

        //     var gst_registered = $("input[name='gst_registered']:checked ").val();
        //     if(typeof gst_registered === "undefined"){
        //         $('#gst_registered').parent('div.form-group').addClass('has-error');
        //         $('.gst-registered-msg').text('Please Select.');
        //         $('#gst_registered').focus();
        //         return false;
        //     }

        //     if(gst_registered != 0){
        //         var gst_no = $('#gst_no').val();
        //             if(gst_no==''){
        //                 $('#gst_no').parent('div.form-group').addClass('has-error');
        //                 $('.gst-no-msg').text('Please Enter GST No.');
        //                 $('#gst_no').focus();
        //                 return false;
        //             }

        //             var gst_certificate = $('#gst_certificate').val();
        //             if(gst_certificate==''){
        //                 if($('#gst_certificate').data('checkstatus')==0){
        //                     $('#gst_certificate').parent('div.form-group').addClass('has-error');
        //                     $('.gst-certificate-msg').text('Please Upload GST Certificate.');
        //                     $('#gst_certificate').focus();
        //                     return false;
        //                 }
        //             }else{
        //                 if (!re.exec(gst_certificate)) {
        //                     $('#gst_certificate').parent('div.form-group').addClass('has-error');
        //                     $('.gst-certificate-msg').text('Please Upload jpeg/png/jpg/pdf Only.');
        //                     $('#gst_certificate').focus();
        //                     return false;
        //                 }
        //             }
        //     }
            
        //     var office_phone_no = $('#office_phone_no').val();
        //     var ofcMobileRegex = /[0-9]{10}/;
        //     // if (office_phone_no=='') {
        //     //     $('#office_phone_no').parent('div.form-group').addClass('has-error');
        //     //     $('.office-phone-no-msg').text('Please Enter Office Number.');
        //     //     $('#office_phone_no').focus();
        //     //     return false;
        //     // }
        //     if(office_phone_no!='' && !office_phone_no.match(ofcMobileRegex)){
        //         $('#office_phone_no').parent('div.form-group').addClass('has-error');
        //         $('.office-phone-no-msg').text('Please Enter Valid Number.');
        //         $('#office_phone_no').focus();
        //         return false;
        //     }

        //     var company_address = $('#company_address').val();
        //     if(company_address==''){
        //         $('#company_address').parent('div.form-group').addClass('has-error');
        //         $('.company-address-msg').text('Please Enter Company Address.');
        //         $('#company_address').focus();
        //         return false;
        //     }

        //     // if ($("#registered").is(":checked")){
        //     //     var incorporation_certificate = $('#incorporation_certificate').val();
        //     //     if(incorporation_certificate==''){
        //     //         if(is_verified != 1){
        //     //             if($('#incorporation_certificate').data('checkstatus')==0){
        //     //                 $('#incorporation_certificate').parent('div.form-group').addClass('has-error');
        //     //                 $('.incorporation-certificate-msg').text('Please Upload Incorporation Certificate.');
        //     //                 $('#incorporation_certificate').focus();
        //     //                 return false;
        //     //             }
        //     //         }
        //     //     }else{
        //     //         if (!re.exec(incorporation_certificate)) {
        //     //             $('#incorporation_certificate').parent('div.form-group').addClass('has-error');
        //     //             $('.incorporation-certificate-msg').text('Please Upload jpeg/png/jpg/pdf Only.');
        //     //             $('#incorporation_certificate').focus();
        //     //             return false;
        //     //         }
        //     //     }
        //     // }

        //     var incorporation_certificate = $('#incorporation_certificate').val();
        //     if(incorporation_certificate!=''){
        //         if (!re.exec(incorporation_certificate)) {
        //             $('#incorporation_certificate').parent('div.form-group').addClass('has-error');
        //             $('.incorporation-certificate-msg').text('Please Upload jpeg/png/jpg/pdf Only.');
        //             $('#incorporation_certificate').focus();
        //             return false;
        //         }
        //     }


        //     var company_state = $('#company_state').val();
        //     if(company_state==''){
        //         $('#company_state').parent('div.form-group').addClass('has-error');
        //         $('.company-state-msg').text('Please Select Company State.');
        //         $('#company_state').focus();
        //         return false;
        //     }

        //     var company_pincode = $('#company_pincode').val();
        //     if(company_pincode==''){
        //         $('#company_pincode').parent('div.form-group').addClass('has-error');
        //         $('.company-pincode-msg').text('Please Enter Company Pincode.');
        //         $('#company_pincode').focus();
        //         return false;
        //     }else if(!pincodeRegex.test(company_pincode)){
        //         $('#company_pincode').parent('div.form-group').addClass('has-error');
        //         $('.company-pincode-msg').text('Please Enter Valid Pincode.');
        //         $('#company_pincode').focus();
        //         return false;
        //     }

        //     $('#form_company_details').submit();
    // });

    
    //Warehouse Details validation and submit
    $('#warehouse_submit').click(function(){
        var warehouse_name = $('#warehouse_name').val();
        if(warehouse_name==''){
            $('#warehouse_name').parent('div.form-group').addClass('has-error');
            $('.warehouse-name-msg').text('Please Enter Warehouse Name.');
            $('#warehouse_name').focus();
            return false;
        }else{
            $('#warehouse_name').parent('div.form-group').removeClass('has-error');
            $('.warehouse-name-msg').text('');
        }

        var warehouse_phone = $('#warehouse_phone').val();
        var warehousePhoneRegex = /^[6789]\d{9}$/;

        if(warehouse_phone==''){
            $('#warehouse_phone').parent('div.form-group').addClass('has-error');
            $('.warehouse-phone-msg').text('Please Enter Warehouse Phone Number.');
            $('#warehouse_phone').focus();
            return false;
        }else if(!warehousePhoneRegex.test(warehouse_phone)){
            $('#warehouse_phone').parent('div.form-group').addClass('has-error');
            $('.warehouse-phone-msg').text('Please Enter Valid Phone Number.');
            $('#warehouse_phone').focus();
            return false;
        }else{
            $('#warehouse_phone').parent('div.form-group').removeClass('has-error');
            $('.warehouse-phone-msg').text('');
        }

        var warehouse_address = $('#warehouse_address').val();
        if(warehouse_address==''){
            $('#warehouse_address').parent('div.form-group').addClass('has-error');
            $('.warehouse-address-msg').text('Please Enter Warehouse Address.');
            $('#warehouse_address').focus();
            return false;
        }else{
            $('#warehouse_address').parent('div.form-group').removeClass('has-error');
            $('.warehouse-address-msg').text('');
        }

        // var warehouse_landmark = $('#warehouse_landmark').val();
        // if(warehouse_landmark==''){
        //     $('#warehouse_landmark').parent('div.form-group').addClass('has-error');
        //     $('.warehouse-landmark-msg').text('Please Enter Warehouse Landmark.');
        //     $('#warehouse_landmark').focus();
        //     return false;
        // }

        var warehouse_pincode = $('#warehouse_pincode').val();
        if(warehouse_pincode==''){
            $('#warehouse_pincode').parent('div.form-group').addClass('has-error');
            $('.warehouse-pincode-msg').text('Please Enter Warehouse Pincode.');
            $('#warehouse_pincode').focus();
            return false;
        } else if(!pincodeRegex.test(warehouse_pincode)){
            $('#warehouse_pincode').parent('div.form-group').addClass('has-error');
            $('.warehouse-pincode-msg').text('Please Enter Valid Pincode.');
            $('#warehouse_pincode').focus();
            return false;
        }else{
            $('#warehouse_pincode').parent('div.form-group').removeClass('has-error');
            $('.warehouse-pincode-msg').text('');
        }

        var warehouse_city = $('#warehouse_city').val();
        if(warehouse_city==''){
            $('#warehouse_city').parent('div.form-group').addClass('has-error');
            $('.warehouse-city-msg').text('Please Enter Warehouse City.');
            $('#warehouse_city').focus();
            return false;
        }else{
            $('#warehouse_city').parent('div.form-group').removeClass('has-error');
            $('.warehouse-city-msg').text(''); 
        }

        var warehouse_state = $('#warehouse_state').val();
        if(warehouse_state==''){
            $('#warehouse_state').parent('div.form-group').addClass('has-error');
            $('.warehouse-state-msg').text('Please Enter Warehouse State.');
            $('#warehouse_state').focus();
            return false;
        }else{
            $('#warehouse_state').parent('div.form-group').removeClass('has-error');
            $('.warehouse-state-msg').text('');
        }

        var is_default = $('input[name="is_default"]:checked').val();
        if(is_default=='' || is_default==undefined){
            $('.is_default').parent('div.form-group').addClass('has-error');
            $('.is-default-msg').text('Please select type.');
            return false;
        }else{
            $('.is_default').parent('div.form-group').removeClass('has-error');
            $('.is-default-msg').text('');
        }

        $('#form_warehouse_details').submit();
    });

    //add new address button click
    $( "#add_new_warehouse" ).click(function() {
        $('#warehouse_id').val('');
        $('#warehouse_name').val('');
        $('#warehouse_landmark').val('');
        $('#warehouse_address').text('');
        $('#warehouse_addresstwo').val('');
        $('#warehouse_city').val('');
        $('#warehouse_pincode').val('');
        $('#warehouse_state').val('');
        $('#warehouse_phone').val('');
        $('#is_default_1').attr('checked', false);
        $('#is_default_0').attr('checked', false);
    });

    //Edit Warehouse ajax get warehouse Data
    $('.edit_warehouse').click(function(){
        var warehouseid = $(this).data('warehouseid');
        var csrfToken = "{{ csrf_token() }}";
        var input = {
            "id" : warehouseid,
            "_token" : csrfToken
        };
        $.ajax({
            url : '{{route("seller.account.getWarehouseDetails")}}',
            type : 'POST',
            data : input,
            success : function(response) {
                if(response=='error'){

                }else{
                    $('#warehouse_id').val(response.id);
                    $('#warehouse_name').val(response.name);
                    $('#warehouse_address').text(response.address);
                    $('#warehouse_landmark').val(response.landmark);
                    $('#warehouse_addresstwo').val(response.address_two);
                    $('#warehouse_city').val(response.city);
                    $('#warehouse_pincode').val(response.pincode);
                    $('#warehouse_state').val(response.state_id);
                    $('#warehouse_phone').val(response.phone);
                    if(response.is_default==1){
                        $('#is_default_1').attr('checked',true);
                    }else{
                        $('#is_default_0').attr('checked',true);
                    }
                }
            }
        });
    });

    //Delete Warehouse Details
    $('.delete_warehouse').click(function(){
        var warehouseid = $(this).data('warehouseid');
        var csrfToken = "{{ csrf_token() }}";
        var input = {
            "id" : warehouseid,
            "_token" : csrfToken
        };
        swal({
            title: 'Are you sure?',
            text: 'You want Delete the record.',
            type: 'info',
            showCancelButton: true,
            confirmButtonClass: 'btn-success',
            cancelButtonClass: 'btn-danger',
            closeOnConfirm: false,
            closeOnCancel: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
        },
        function(isConfirm){
            if (isConfirm){
                $.ajax({
                    url : '{{route("seller.account.deleteWarehouseDetails")}}',
                    type : 'POST',
                    data : input,
                    success: function(result){
                        swal('Warehouse', 'Record Deleted Successfully', "success");
                        location.reload();
                    }
                });
            }
            else {
                swal("Cancelled", "", "error");
            }
        });
    });

    //Change Password Form Validation and submit
    $('#password_submit').click(function(){
        var passwordRegex = /^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\W).*$/;
        var current_password = $('#current_password').val();
        if(current_password==''){
            $('#current_password').parent('div.form-group').addClass('has-error');
            $('.current-password-msg').text('Please Enter Current Password.');
            $('#current_password').focus();
            return false;
        }

        var password = $('#password').val();
        if(password==''){
            $('#password').parent('div.form-group').addClass('has-error');
            $('.password-msg').text('Please Enter New Password.');
            $('#password').focus();
            return false;
        }else{
            if(!password.match(passwordRegex)){
                $('#password').parent('div.form-group').addClass('has-error');
                $('.password-msg').text('Please Enter Valid Password, Password must be 8 charaters, 1 uppercase and 1 special charater');
                $('#password').focus();
                return false;
            }
        }

        var confirm_password = $('#confirm_password').val();
        if(confirm_password==''){
            $('#confirm_password').parent('div.form-group').addClass('has-error');
            $('.confirm-password-msg').text('Please Enter Confirm Password.');
            $('#confirm_password').focus();
            return false;
        }else if(confirm_password != password){
            $('#confirm_password').parent('div.form-group').addClass('has-error');
            $('.confirm-password-msg').text('Passwords do not match!.');
            $('#confirm_password').focus();
            return false;
        }

        $('#form_change_password').submit();
    });

    //Show and hide type of company
     $(function() {
        // $("#individual_company").hide();
       $("input[name='legal_entity']").click(function() {

        if ($("#registered").is(":checked")) {
            $("#individual_company").hide();
            $("#registered_company").show();
            $("#individual_company_type").prop('checked', false);
         }
         if ($("#not_registered").is(":checked")) {
            $("#registered_company").hide();
            $("#individual_company").show();
            $("#individual_company_type").prop('checked', true);
         }          
       });
     });

     //Show and hide type of gst_registered
     $(function() {
        // $("#individual_company").hide();
       $("input[name='gst_registered']").click(function() {

        if ($("#yes").is(":checked")) {
            $("#gst_number").show();
            $("#gst_certificate").show();
         }
         if ($("#no").is(":checked")) {
            $("#gst_number").hide();
            $("#gst_certificate").hide();
         }          
       });
     });

    var Register = function() {
        var handlePasswordStrengthChecker = function () {
            var initialized = false;
            var input = $("#password");

            input.keydown(function () {
                if (initialized === false) {
                    // set base options
                    input.pwstrength({
                        raisePower: 1.4,
                        minChar: 8,
                        verdicts: ["Weak", "Normal", "Medium", "Strong", "Very Strong"],
                        scores: [17, 26, 40, 50, 60]
                    });

                    // add your own rule to calculate the password strength
                    input.pwstrength("addRule", "demoRule", function (options, word, score) {
                        return word.match(/[a-z].[A-Z].[0-9]/) && score;
                    }, 10, true);

                    // set as initialized 
                    initialized = true;
                }
            });
        }

        return {
            //main function to initiate the module
            init: function() {
                handlePasswordStrengthChecker();
            }
        };

    }();

    jQuery(document).ready(function() {

        $('#finalSubmitBtn').click(function() {

            var seller_id = $(this).data('sellerid');
            var csrfToken = "{{ csrf_token() }}";
            var input = {
                "id" : seller_id,
                "_token" : csrfToken
            };
            swal({
                title: 'Are you sure?',
                text: '',
                type: 'info',
                showCancelButton: true,
                confirmButtonClass: 'btn-success',
                cancelButtonClass: 'btn-danger',
                closeOnConfirm: false,
                closeOnCancel: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
            },
            function(isConfirm){
                if (isConfirm){
                    $.ajax({
                        url : '{{route("seller.account.finalSubmission")}}',
                        type : 'POST',
                        data : input,
                        success : function(response) {
                            console.log(response.data);
                            if(response.success == 0){
                                swal('', response.message, "error");
                            }
                            else if(response.success == 1 && response.data == true){
                                swal('Seller', response.message, "success");
                                location.reload();
                            }
                        }
                    });
                }
                else {
                    swal("Cancelled", "", "error");
                }
            });
        });

        $('.settings-form-submit').on('click', function(e){
            $(this).button('loading');
        });

        $('.continue').click(function(){
            $('.navigation-tabs > .active').next('li').find('a').trigger('click');
        });
        $('.back').click(function(){
            $('.navigation-tabs > .active').prev('li').find('a').trigger('click');
        });
        Register.init();
        $('#tabs_list a[href="#{{ old('tab') }}"]').tab('show');
       
        //Search Brands
        var path = "{{ route('ajax.brandsAutoComplete') }}";
        $('input.typeahead').typeahead({
            source: function (query, process) {
            return $.get(path, { q: query }, function (data) {
                // console.log(data)
                    return process(data);
                });
            },
            autoSelect: true,
            displayText: function(item){ return item.text;}
        });
    });  
</script>
@endpush
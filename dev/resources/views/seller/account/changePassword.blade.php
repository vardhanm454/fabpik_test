@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')

<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"></h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="portlet light">
            <div class="portlet-body form">
                <div class="tabbable-bordered">
                    <form class="form-horizontal" action="{{route('seller.account.changePassword')}}" method="post"
                        enctype="multipart/form-data">
                        
                        {!! csrf_field() !!}
                        <div class="note note-success">
                            <p> Password must be 8 charaters, 1 uppercase and 1 special charater. </p>
                        </div>
                        <div class="form-body">
                            <div class="form-group {{($errors->first('current_password'))?'has-error':''}}">
                                <label class="col-md-3 control-label">Current Password:<span class="required"> *
                                    </span></label>
                                <div class="col-md-5">
                                    <input type="password" class="form-control maxlength-handler" name="current_password"
                                        maxlength="100" placeholder="" value="">
                                    <span class="help-block">{{$errors->first('current_password')}}</span>
                                </div>
                            </div>
                            <div class="form-group {{($errors->first('password'))?'has-error':''}}">
                                <label class="col-md-3 control-label">Password:<span class="required"> *
                                    </span></label>
                                <div class="col-md-5">
                                    <input type="password" class="form-control maxlength-handler" name="password"
                                        maxlength="100" placeholder="" value="">
                                    <span class="help-block">{{$errors->first('password')}}</span>
                                </div>
                            </div>
                            <div class="form-group {{($errors->first('password_confirmation'))?'has-error':''}}">
                                <label class="col-md-3 control-label">Confirm Password:<span class="required"> *
                                    </span></label>
                                <div class="col-md-5">
                                    <input type="password" class="form-control maxlength-handler" name="password_confirmation"
                                        maxlength="100" placeholder="" value="">
                                    <span class="help-block">{{$errors->first('password_confirmation')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="submit" class="btn blue form-submit" name="save" value="save" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing..."><span aria-hidden="true" class="icon-key"></span> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var ChangePassword = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('.form-submit').on('click', function(e) {
                $(this).button('loading');
            });

        }

    };

}();

jQuery(document).ready(function() {
    ChangePassword.init();
});
</script>
@endpush
@extends(SELLER_THEME_NAME.'.layouts.master') @push('PAGE_ASSETS_CSS') @endpush @section('content') <style>
    .notifications {
        padding: 2px 0;
        margin-bottom: env(safe-area-inset-bottom);
    }

    .listview.flush {
        border-top: 0;
        border-bottom: 0;
    }

    .listview {
        display: block;
        padding: 0;
        margin: 0;
        color: #27173E;
        background: #fff;
        border-top: 1px solid #DCDCE9;
        border-bottom: 1px solid #DCDCE9;
        line-height: 1.3em;
    }

    .image-listview>li {
        padding: 0;
        min-height: auto;
    }

    .listview>li {
        display: inline-block;
        /* align-items: center; */
        /* justify-content: space-between; */
        position: relative;
        min-height: 50px;
        width: 100%;
    }

    .image-listview>li.active .item {
        background: rgba(220, 220, 233, 0.3) !important;
    }

    .image-listview>li a.item {
        color: #27173E !important;
        padding-right: 36px;
    }

    .image-listview>li a.item .mb-05.font-15 {
        font-size: 12px;
    }

    .image-listview>li .item {
        padding: 8px 16px;
        width: 100%;
        min-height: 50px;
        display: flex;
        align-items: center;
    }

    .icon-circle {
        width: 32px;
        height: 29px;
        display: flex;
        align-items: center;
        justify-content: center;
        line-height: 1em;
        font-size: 15px;
        border-radius: 49% !important;
        margin-right: 16px;
    }

    /**/
    .notifications .bg-pink {
        background: #ffbdbd;
    }

    .notifications .bg-blue {
        background-color: rgba(255, 159, 67, .12);
        color: #FF9F43 !important
    }

    .notifications .bg-green {
        background-color: rgba(40, 199, 111, .12);
        color: #28C76F !important;
    }

    .notifications .bg-orange {
        background: #ffbd86;
    }

    .bg-info-not {
        background-color: rgba(0, 207, 232, .12);
        color: #00CFE8 !important;
    }

    .mt_7 {
        margin-top: -7px;
    }

    svg {
        width: 25px;
    }

    .image-listview>li a.item:hover {
        text-decoration: none;
    }

    .notifications .bg-red {
        background-color: rgba(234, 84, 85, .12) !important;
        color: #EA5455 !important;
    }

    .notifications .panel {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background: #fff !important;
        background-clip: border-box;
        border-radius: 10px;
        border: 0 solid #edeef7 !important;
        margin-bottom: 3px;
        border-radius: 5px !important;
        box-shadow: 0 5px 15px 5px rgb(80 102 224 / 8%) !important;
    }

    .notifications .panel .panel-body p {
        margin: 0;
        font-size: 11px;
    }

    .panel-title {
        margin-top: 0;
        background: #fff;
        font-size: 16px;
        border-radius: 7px 7px 0 0 !important;
    }

    .accordion .panel .panel-title .accordion-toggle.accordion-toggle-styled::after {
        background: none;
        content: "\f106";
        font-family: "FontAwesome";
        font-size: 15px;
        position: absolute;
        right: 16px;
        color: #A9ABAD;
        opacity: 1;
        line-height: 1em;
        height: 18px;
        top: 24px;
        margin-top: -9px;
        margin-right: 0px;
    }

    .accordion .panel .panel-title .accordion-toggle.accordion-toggle-styled.collapsed,
    .accordion .panel .panel-title .accordion-toggle.accordion-toggle-styled {
        background: none;
    }

    .accordion .panel .panel-title .accordion-toggle.accordion-toggle-styled.collapsed::after {
        background: none;
        content: "\f105";
        font-family: "FontAwesome";
        font-size: 15px;
        position: absolute;
        right: 16px;
        color: #A9ABAD;
        opacity: 1;
        line-height: 1em;
        height: 18px;
        top: 24px;
        margin-top: -9px;
        margin-right: 0px;
    }

    .panel-default>.panel-headinge>.panel-body {}

    .notifications .panel a {
        font-size: 12px;
        line-height: 12px;
        display: block;
        margin: 0;
        font-weight: bold;
    }

    .panel-default>.panel-heading+.panel-collapse>.panel-body {
        min-height: 54px !important;
        max-height: 230px;
        height: auto !important;
    }

    .notifications span {
        margin: 0;
        font-size: 11px;
        color: #A9ABAD;
        padding: 0;
        line-height: 19px;
        display: block;
        font-weight: 500;
    }

    .notifications .accordion .panel .panel-title .accordion-toggle.accordion-toggle-styled.collapsed {
        background-position: right 20px;
    }
    .pl-0
    {
        padding-left:0;
    }
</style>
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"></h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-8 col-md-offset-2 notifications">
        <div class="portlet-body">
            <div class="panel-group accordion" id="accordion1">
            @foreach($notifications as $notification)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed " data-toggle="collapse" data-parent="#accordion1" href="#{{$notification->id}}">
                                <div class="row">
                                    <div class="col-lg-1">
                                        <div class="icon-circle px-1 bg-red"><i class="icon-bell"></i> </div>
                                    </div>
                                    <div class="col-lg-11 pl-0"> 
                                        {{$notification->title}}<span>{{date("d/m/y H:i A",strtotime($notification->notify_on))}}</span>
                                    </div>
                                </div>
                            </a> 
                        </h4>
                    </div>
                    <div id="{{$notification->id}}" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p> {{$notification->content}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
</div>
@stop
 
@push('PAGE_ASSETS_JS')
@endpush @push('PAGE_SCRIPTS')

<script type="text/javascript">
    var ChangePassword = function() {
        return {
            //main function to initiate the module
            init: function() {
                $('.form-submit').on('click', function(e) {
                    $(this).button('loading');
                });
            }
        };
    }();
    jQuery(document).ready(function() {
        ChangePassword.init();
    });
</script> @endpush
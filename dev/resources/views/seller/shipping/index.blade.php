@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content')
<style>
    .badge{
        position: inherit;
    }
    
    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        width: 400px;
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    .modal-body {
        position: relative;
        overflow-y: auto;
        max-height: 400px;
        padding: 15px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">Mobile:</label>
                                        
                                            <input type="text" class="form-control input-sm" name="fmobile" id="fmobile"
                                                value="{{request()->fmobile}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">SKU:</label>
                                        
                                            <input type="text" class="form-control input-sm" name="fsku" id="fsku"
                                                value="{{request()->fsku}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">Track ID:</label>
                                        
                                            <input type="text" class="form-control input-sm" name="ftrackid" id="ftrackid"
                                                value="{{request()->ftrackid}}" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label class="">Order Status: </label><br>
                                    <div class="">
                                        <select name="fostatus" id="fostatus"
                                            class="form-control select2 form-filter input-sm margin-bottom-5">
                                            <option value="">Select</option>
                                            @if(isset($orderStatus))
                                            @foreach($orderStatus as $status)
                                            <option value="{{$status->id}}"
                                                {{request()->fostatus==$status->id?'selected="selected"':''}}>
                                                {{$status->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                            <div class="col-md-3">
                                    <label class="">Date: </label><br>
                                    <div class="">
                                        <div class="input-group input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control date-picker input-sm" name="ffromdate"
                                                id="ffromdate" data-date-end-date="0d" placeholder="From" value="{{request()->ffromdate}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control date-picker input-sm" name="ftodate" id="ftodate"
                                                data-date-end-date="0d" placeholder="To" value="{{request()->ftodate}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="">Shipping Status: </label><br>
                                    <div class="">
                                        <select name="fsstatus" id="fsstatus"
                                            class="form-control select2 form-filter input-sm margin-bottom-5">
                                            <option value="">Select</option>
                                            @if(isset($shippingStatus))
                                            @foreach($shippingStatus as $status)
                                            <option value="{{$status->id}}"
                                                {{request()->fsstatus==$status->id?'selected="selected"':''}}>
                                                {{$status->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">Order ID:</label>
                                        
                                            <input type="text" class="form-control input-sm" name="forderid" id="forderid"
                                                value="{{request()->forderid}}" autocomplete="off">
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <label class="">Action: </label><br>
                                    <div class="">
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search"
                                            id="btn_submit_search" value="search"
                                            data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('seller.shipping')}}"
                                            title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <a href="javascript:;" class="btn btn-default btn-sm table-group-action-status tooltips" data-placement="top" data-original-title="Change Shipping Status" 
                        data-toggle="tooltip" data-action="change-status" data-target="#changeStatusModal">
                            <strong><span aria-hidden="true" id="btn-confirm" class="icon-settings text-primary"></span>&nbsp;Take Action</strong></a>
                    </div>
                    <table class="table table-bordered table-hover dt-responsive dataTable no-footer dtr-inline collapsed"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th class="text-center" style="width:6%;">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th class="text-center" style="width:7%;"> Date </th>
                                <th class="text-center" style="width:7%;"> Order ID </th>
                                <th class="text-center" style="width:15%;"> Product Name </th>
                                <th class="text-center" style="width:7%;"> Quantity</th>
                                <th class="text-center" style="width:7%;"> Weight <br/><small>(Per Unit<br/>In Grams)</small></th>
                                <th class="text-center" style="width:7%;"> L*B*H <br/>(Per Unit <br/>cm<sup>3</sup>)</th>
                                <th class="text-center" style="width:7%;"> Expected <br/>Packaging <br/>Date </th>
                                <th class="text-center" style="width:7%;"> Delivery <br/>Date </th>
                                <th class="text-center" style="width:13%;"> Shipping <br/>Status</th>
                                <th class="text-center" style="width:12%;"> Actions</th>
                                <th class="none"> User Name: </th>
                                <th class="none"> Product ID: </th>
                                <th class="none"> Pincode: </th>
                                <th class="none"> Tracking ID: </th>
                                <th class="none"> Expected Delivery Date: </th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="changeStatusModal" tabindex="-1" role="dialog" aria-labelledby="changeStatusModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changeStatusModalLabel">Change Shipping Status!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="">
                    <label class="control-label">Change Status:</label>
                    <form>
                        <div class="">
                        @if($seller->shipping_model == 'f')
                            <select class="form-control" name="status" id="status">
                                <option value="">Select</option>
                                @foreach(['2'=>'Ready For Pickup', '8'=>'Undelivered Product Returned Confirmed', '12'=>'Product Return Confirmed'] as $val=>$label)
                                <option value="{{$val}}"> {{$label}} </option>
                                @endforeach
                            </select>
                            <span style="color:#e73d4a" id="result"></span>
                        @else
                            <select class="form-control" name="status" id="status">
                                <option value="">Select</option>
                                @foreach(['4'=>'Shipped', '5'=>'Delivered', '6'=>'Delivery Unsuccessful', '8'=>'Undelivered Product Returned Confirmed', '10'=>'Return Pickup Initiated', '12'=>'Product Return Confirmed'] as $val=>$label)
                                <option value="{{$val}}"> {{$label}} </option>
                                @endforeach
                            </select>
                            <span style="color:#e73d4a" id="result"></span>
                        @endif
                        </div>
                    </form>
                </div>

                <div class="">
                    <label class="control-label">Remark</label>
                    <div class="">
                        <textarea class="form-control" name="remark" id="remark" rows="3"></textarea>
                        <span style="color:#e73d4a" id="remark-result"></span>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal-btn-yes">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modify AWB Modal -->
<div class="modal fade" id="modifyAWBModal" tabindex="-1" role="dialog" aria-labelledby="modifyAWBModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modifyAWBModalLabel">Enter AWB Details 
                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </h5>
                
            </div>
            <div class="modal-body">
                <input type="hidden" id="current_awb_number" name="current_awb_number">
                <input type="hidden" id="awb_child_order_id" name="awb_child_order_id">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="awb_number" class="col-form-label">AWB Number: <span class="required"> *</span></label>
                            <input type="text" class="form-control input-sm" id="awb_number" name="awb_number" autocomplete="off" >
                            <span style="color:#e73d4a" id="awb_number_result"></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="awb_courier_partner" class="col-form-label">Choose Courier Partner: <span class="required"> *</span></label>
                            <select name="awb_courier_partner" id="awb_courier_partner" class="form-control select2 form-filter input-sm margin-bottom-5">
                                <option value="">Select</option>
                                @php $courierPartners = App\Models\CourierPartner::get() @endphp
                                    @foreach($courierPartners as $partner)
                                    <option value="{{$partner->id}}">{{$partner->name}}</option>
                                    @endforeach
                                    <option value="o">Other</option>

                            </select>
                             <span style="color:#e73d4a" id="awb_courier_partner_result"></span>
                        </div>
                    </div>
                </div>

                <div id="other_courier_partner_details" name="other_courier_partner_details" style="display:none;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="courier_partner_name" class="col-form-label">Courier Partner Name: <span class="required"> *</span></label>
                                <input type="text" class="form-control input-sm" id="courier_partner_name" name="courier_partner_name" autocomplete="off" >
                                <span style="color:#e73d4a" id="courier_partner_name_result"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tracking_url" class="col-form-label">Tracking URL: <span class="required"> *</span></label>
                                <input type="text" class="form-control input-sm" id="tracking_url" name="tracking_url" autocomplete="off" >
                                <span style="color:#e73d4a" id="tracking_url_result"></span>
                            </div>
                        </div>
                    </div>

                </div>

                <span style="color:#295633" id="awb_number_result_success"></span>
                <span style="color:#e73d4a" id="awb_number_result_failure"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modify_awb_save_btn">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- Modify AWB Modal -->
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
    type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Shipping = function() {
    $('#changeStatusModal').on('hidden.bs.modal', function () {
        $('#changeStatusModal form')[0].reset();
    });
    return {

        //main function to initiate the module
        init: function() {

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }

            //Filters will apply when click the enter
            $(document).on("keypress", "input", function(e){
                if(e.which == 13){
                    var inputVal = $(this).val();
                    document.getElementById("btn_submit_search").click();
                }
            });

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('seller.shipping')}}?search=1";

                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fsstatus').val() != '') url += "&fsstatus=" + encodeURIComponent($('#fsstatus').val());
                if ($('#fostatus').val() != '') url += "&fostatus=" + encodeURIComponent($('#fostatus').val());
                if ($('#ftrackid').val() != '') url += "&ftrackid=" + encodeURIComponent($('#ftrackid').val());
                if ($('#fsku').val() != '') url += "&fsku=" + encodeURIComponent($('#fsku').val());
                if ($('#fmobile').val() != '') url += "&fmobile=" + encodeURIComponent($('#fmobile').val());
                if ($('#forderid').val() != '') url += "&forderid=" + encodeURIComponent($('#forderid').val());

                window.location.href = url;
            });

            //AWB Details Update
            //get the current AWB Details of the order
            $("#modifyAWBModal").on("show.bs.modal", function(e) {
                //load the  current values
                $("#awb_child_order_id").val($(e.relatedTarget).data('awb_child_order_id'));
                $("#current_awb_number").val($(e.relatedTarget).data('current_awb_number'));
                $("#awb_number").val($(e.relatedTarget).data('current_awb_number'));
            });

            $("#awb_courier_partner").change(function() {
                // alert( $('option:selected', this).val() );
                $('#other_courier_partner_details').css('display', 'none');

                if($('option:selected', this).val() == 'o'){
                    $('#other_courier_partner_details').css('display', 'block');
                }

            });

            $('#modify_awb_save_btn').on('click',function(){

                var awb_number = $("#awb_number").val();
                var current_awb_number = $("#current_awb_number").val();
                var courier_partner = $('#awb_courier_partner').val();

                var courier_partner_name = $('#courier_partner_name').val();
                var tracking_url = $('#tracking_url').val();

                
                var orderId = $("#awb_child_order_id").val();

                $('#awb_number_result_success').html('');
                $('#awb_number_result_failure').html('');

                if(awb_number.trim() == ''){
                    $('#awb_number').focus();
                    $("#awb_number_result").html("This filed is required.");
                    return false;
                }else if(awb_number == current_awb_number){
                    $('#awb_number').focus();
                    $("#awb_number_result").html("Update AWB Number.");
                    return false;
                }else if(courier_partner == ''){
                    $('#awb_courier_partner').focus();
                    $("#awb_courier_partner_result").html("This filed is required.");
                    return false;
                }else if(courier_partner == 'o'){
                    if(courier_partner_name == ''){
                        $('#courier_partner_name').focus();
                        $("#courier_partner_name_result").html("This filed is required.");
                        return false;
                    }else if(tracking_url == ''){
                        $('#tracking_url').focus();
                        $("#tracking_url_result").html("This filed is required.");
                        return false;
                    }
                }else{
                    var url = "{{route('seller.shipping.modifyAWBNumber',['id'=>':orderId'])}}";
                    url = url.replace(':orderId', orderId);
                    
                    var arr = { 
                        awb_number: awb_number,                   
                        order_id: orderId,
                        courier_partner_id: courier_partner,
                        courier_partner_name: courier_partner_name,
                        tracking_url: tracking_url,
                    };

                    $.ajax({
                        type: 'POST',
                        url: url,
                        data:JSON.stringify(arr),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            $('#modify_awb_save_btn').attr("disabled","disabled");
                            $('.modal-body').css('opacity', '.5');
                        },
                        success: function(result) {
                           console.log(result); 
                           if(result.success){
                                $('#awb_number_result_success').html(result.message);
                                
                           }else{
                                $('#awb_number_result_failure').html(result.message);
                           }
                           $('#modify_awb_save_btn').removeAttr("disabled");
                           $('.modal-body').css('opacity', '');
                        }, 
                        error:function(err){
                            // alert("error"+JSON.stringify(err));
                            console.log(err.responseJSON);
                            $('#modify_awb_save_btn').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');
                            $("#awb_number_result_failure").html("Something went wrong, please try again after sometime");
                        
                        }
                    });

                }
            }); 

        }

    };
}();

jQuery(document).ready(function() {
    Shipping.init();
});
</script>
@endpush
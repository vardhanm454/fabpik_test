@extends(SELLER_THEME_NAME.'.layouts.master')

@push('PAGE_ASSETS_CSS')
<link href="{{ __common_asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' )}}"
    rel="stylesheet" type="text/css" />
@endpush

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <!-- FILTER FORM START -->
                    <form action="#" method="get" class="filter-form">
                        <div class="form-body">

                            <div class="row">

                                <div class="col-md-4">
                                    <label class="control-label col-md-3 pd-tb-5"><strong>Published:</strong></label>
                                    <div class="col-md-9">
                                        <select name="fstatus" id="fstatus" class="form-control input-sm">
                                            <option value="">All</option>
                                            <option value="1" {{request()->fstatus=='1'?'selected="selected"':''}}>
                                                Yes
                                            </option>
                                            <option value="0" {{request()->fstatus=='0'?'selected="selected"':''}}>
                                                No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label col-md-3 pd-tb-5"><strong>Customer:</strong> </label>
                                    <div class="col-md-9">
                                        <select name="fcustomers" id="fcustomers"
                                            class="form-control form-filter input-sm select2">
                                            <option value="">Select Customer</option>
                                            @if(isset($customers))
                                            @foreach($customers as $customer)
                                            <option value="{{$customer->id}}"
                                            {{request()->fcustomers==$customer->id?'selected="selected"':''}}>
                                                {{$customer->name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label col-md-3 pd-tb-5"><strong>Date:</strong> </label>
                                    <div class="col-md-9">
                                        <div class="input-group  input-daterange" data-date="10/11/2012"
                                            data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control date-picker input-sm" name="ffromdate" id="ffromdate"
                                                placeholder="From" value="{{request()->ffromdate}}">
                                            <span class="input-group-addon"> - </span>
                                            <input type="text" class="form-control date-picker input-sm" name="ftodate" id="ftodate" placeholder="To" value="{{request()->ftodate}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                        <label class="control-label col-md-3 pd-tb-5"><strong>Product Variant:</strong></label>
                                        <div class="col-md-9 {{($errors->first('fprovariants'))?'has-error':''}}">
                                           
                                                <select class="form-control input-sm" id="fprovariants" name="fprovariants[]" multiple >
                                                    @if(!$errors->has('fprovariants'))
                                                        @if (is_array(old('fprovariants')))
                                                            @php $oldprovariants = old('fprovariants'); @endphp
                                                            @foreach ($oldprovariants as $variant)
                                                                @php $proVariants = App\Models\ProductVariant::selectRaw("id,name")->find($variant);
                                                                @endphp
                                                                <option value="{{$proVariants->id}}" selected="selected">{{$proVariants->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </select>
                                                <span class="help-block">{{$errors->first('fprovariants')}}</span>
                                           
                                        </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label col-md-3 pd-tb-5"><strong>Action:</strong> </label>
                                    <div class="col-md-9 pd-tb-5">
                                        <button type="button" class="btn btn-sm btn-icon-only white" name="search" id="btn_submit_search" value="search" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" title="Apply Filter"><span aria-hidden="true" class="icon-magnifier"></span></button>
                                        <a class="btn btn-sm btn-icon-only white" href="{{route('seller.reviews')}}" title="Reset Filter">
                                            <span aria-hidden="true" class="icon-refresh"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body">
                <div class="table-container">
                    <table class="table dt-responsive nowrap product-list-table text-center"
                        id="dt_listing">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="50px" class="">&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable"
                                            data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="5%" class="text-center"> S.No </th>
                                <th width="15%" class="text-center"> Order ID </th>
                                <th width="10%" class="text-center"> Customer Name </th>
                                <th width="10%" class="text-center"> Customer Email </th>
                                <th width="25%" class="text-center"> Product Name </th>
                                <th width="10%" class="text-center"> Review Title: </th>
                                <th width="10%" class="text-center"> Review Comment: </th>
                                <th width="8%" class="text-center"> Rating: </th>
                                <th width="8%" class="text-center"> Modified at: </th>
                                <th width="5%" class="text-center"> Published </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
@stop

@push('PAGE_ASSETS_JS')
<script src="{{ __common_asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
    type="text/javascript">
</script>
@endpush

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Reviews = function() {

    return {

        //main function to initiate the module
        init: function() {

            $('#fcategory').select2({ 
                allowClear:true,
                minimumInputLength: 0,
                placeholder: 'Search Category',
                ajax: {
                    url: "{{route('ajax.childcategories.autocomplete')}}?",
                    dataType: 'json',
                    // delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (categoryArr) {
                                return {
                                    text: categoryArr.text,
                                    id: categoryArr.id,
                                    val: categoryArr.text,
                                    term: params.term
                                }
                            })
                        };
                    },
                    cache: false
                },
                templateResult: function (categoryArr) {
                    return categoryArr.text;
                },
            });

            $('#fprovariants').select2({ 
                allowClear:true,
                minimumInputLength: 1,
                placeholder: 'Search Product Variant',
                allowClear: true,
                ajax: {
                    url: "{{route('ajax.productVariantAutoComplete')}}?",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,  // search term 
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: $.map(data, function (sellerarr) {
                                return {
                                    text: sellerarr.text,
                                    id: sellerarr.id,
                                    val: sellerarr.text,
                                    term: params.term,
                                    mrp: sellerarr.mrp,
                                    price: sellerarr.price,
                                    sname: sellerarr.sname,
                                    scode: sellerarr.scode,
                                    disc: sellerarr.disc,
                                    image: sellerarr.image,
                                    primaryattrvalue: sellerarr.primaryattrvalue,
                                    secondaryattrvalue: sellerarr.secondaryattrvalue,
                                }
                            })
                        };
                    },
                    cache: false
                },
                // templateResult: function (sellerarr) {
                //     return sellerarr.text +' - '+ sellerarr.mrp;
                // },
                escapeMarkup: function(markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection
            });
            function formatRepoSelection(repo) {
                return repo.text;
            }
            // @see https://select2.github.io/examples.html#data-ajax
            function formatRepo(repo) {
                if (repo.loading) return repo.text;
                
                var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img style='width:90%' src='/{{UPLOAD_PATH}}/" + repo.image + "' /></div>" +
                "<div class='select2-result-repository__forks'> Seller:&nbsp" + repo.sname + "</div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.text +  " - "+ repo.primaryattrvalue +" - "+ repo.secondaryattrvalue +"</div>";
            
                if (repo.description) {
                    markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
                }            

                markup += "<div class='select2-result-repository__statistics'>" +
                    "<div class='select2-result-repository__forks'> Rs.&nbsp" + repo.price + "&nbsp&nbsp <del>Rs.&nbsp" + repo.mrp + "</del> ("+ repo.disc.toFixed(2)+"%OFF)</div>" +
                   "</div>" +
                    "</div></div>";

                return markup;
            }

            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            }

            // Do Filter
            $('#btn_submit_search').on('click', function(e) {
                e.preventDefault();
                $(this).button('loading');

                var url = "{{route('seller.reviews')}}?search=1";

                // if ($('#fname').val() != '') url += "&fname=" + encodeURIComponent($('#fname').val());
                if ($('#fstatus').val() != '') url += "&fstatus=" + encodeURIComponent($('#fstatus').val());
                // if ($('#ffeatured').val() != '') url += "&ffeatured=" + encodeURIComponent($('#ffeatured').val());
                if ($('#ffromdate').val() != '') url += "&ffromdate=" + encodeURIComponent($('#ffromdate').val());
                if ($('#ftodate').val() != '') url += "&ftodate=" + encodeURIComponent($('#ftodate').val());
                if ($('#fcustomers').val() != '') url += "&fcustomers=" + encodeURIComponent($('#fcustomers').val());
                if ($('#fprovariants').val()) url += "&fprovariants=" + encodeURIComponent(JSON.stringify($('#fprovariants').val()));


                window.location.href = url;
            });

            // Export all products data
            $('#btn_table_export').on('click', function(e) {
                e.preventDefault();

                var url = "{{route('seller.reviews')}}?export=1";

                if ($('#fname').val() != '') url += "&fname=" + encodeURIComponent($('#fname').val());
                if ($('#fseller').val() != '') url += "&fseller=" + encodeURIComponent($('#fseller').val());
                if ($('#fstatus').val() != '') url += "&fstatus=" + encodeURIComponent($('#fstatus').val());
                if ($('#fpricefrom').val() != '') url += "&fpricefrom=" + encodeURIComponent($('#fpricefrom').val());
                if ($('#fpriceto').val() != '') url += "&fpriceto=" + encodeURIComponent($('#fpriceto').val());
                if ($('#ffeatured').val() != '') url += "&ffeatured=" + encodeURIComponent($('#ffeatured').val());
                if ($('#fcategory').val()) url += "&fcategory=" + encodeURIComponent($('#fcategory').val());

                window.location.href = url;
            });

            // Delete Products
            $('body').on('click', '.dt-list-delete', function(event){
                event.preventDefault();
                var url = $(this).attr('del-url');
                swal({
                    title: 'Are you sure?',
                    text: 'You want Delete the record.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn-success',
                    cancelButtonClass: 'btn-danger',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: url, 
                            type:'post',
                            success: function(result){
                                swal('Product', 'Record Deleted Successfully', "success");
                                location.reload();
                            }
                        });
                    }
                    else {
                        swal("Cancelled", "", "error");
                    }
                });
            });
        }

    };

}();

jQuery(document).ready(function() {
    Reviews.init();
});
</script>
@endpush
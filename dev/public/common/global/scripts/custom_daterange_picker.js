$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange').html(start.format('YYYY-MM-DD') + '-' + end.format('YYYY-MM-DD'));
    }

    $('#reportrange').daterangepicker({
        ranges: {
           'Week': [moment().subtract(1, 'week'), moment()],
           'Month': [moment().subtract(1, 'month'), moment()],
           '2 Months': [moment().subtract(2, 'month'), moment()],
           '3 Months': [moment().subtract(3, 'month'), moment()],
           '6 Months': [moment().subtract(6, 'month'), moment()],
           'Year': [moment().subtract(1, 'year'), moment()],

        }
    }, cb);

    cb(start, end);

});
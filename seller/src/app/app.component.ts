import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit{
  fixed: boolean=false;

	@HostListener('window:scroll', ['$event'])
	onWindowScroll(e:any)
	{
	  if (window.pageYOffset > 150) 
	  {
		this.fixed = true;
	  } 
	  else
	  {
		this.fixed = false;
	  }
  } 
  ngOnInit()
  {
    
  }
}

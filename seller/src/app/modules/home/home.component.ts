import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap'; 
import { OwlOptions } from 'ngx-owl-carousel-o';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title = 'sellerlanding';
  videoUrl: any;
  constructor(public modalService: NgbModal,
    private dom:DomSanitizer,
   ) { }

  openVerticallyVideoCentered(content2: any)
  {
    this.modalService.open(content2,
    {
      centered: true,
      size: 'lg',
      backdropClass: 'light-blue-backdrop',
      windowClass: 'dark-modal'
    });
  }
  public sliders =
  [
    {  image: "assets/images/video_bg-min.png", title:"How to register as a seller on Fabpik",videolink:"https://www.youtube.com/embed/1wUNyhc-w44"},
    {  image: "assets/images/video_bg-min.png", title:"How to get your Seller Account approved by Fabpik",videolink:"https://www.youtube.com/embed/6rk27WqAaSU"},
   
  ]

  public bannersliders =
  [
    {  image: "assets/images/slider/sell_on_fabpik.jpg"},
    {  image: "assets/images/slider/product_online.jpg"},
    {  image: "assets/images/slider/free_deals.jpg"},
    {  image: "assets/images/slider/go_online.jpg"},
    {  image: "assets/images/slider/across_india.jpg"},
  ]
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    autoplay:true,
    autoplayHoverPause:true,
    navSpeed: 700,
    autoplaySpeed:7000,
    smartSpeed:10000,
    margin:20,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1,
       
      }
    },
    nav: false
  }

  setModalVideo(url: any) {
     this.videoUrl = this.dom.bypassSecurityTrustResourceUrl(url);
  }
  ngOnInit()
  {
    
  }
 
  slideConfig = {
  arrows:false,
  dots: false,
 
  "slidesToShow":1, "slidesToScroll": 1,
  centerPadding: "20px",
  centerMargin: "20px",
  draggable: true,
  infinite: true,
  pauseOnHover: true,
  swipe: false,
  touchMove: false,
  vertical: false,
  speed: 1000,
  autoplay:true,
  autoplaySpeed: 3000,
  adaptiveHeight: false,}
  

  
 
  
 public testimonials=[

  {content:'I was a small time seller with a physical store. Selling on Fabpik has always added additional benefit to my brand, due to its focused target market for kids and with a huge number of good quality products,including my brand among them has added value to my brand too.', name:'Sneha reddy Yanamadala'},
  {content:' I get paid promptly at regular intervals, which is very important for a seller, thanks for the Good service Fabpik!', name:'SaiHari Bijjam'},
  {content:'I have switched from a physical store to online selling with Fabpik recently, and so far my experience has been great.', name:'Shaili Ahuja'},

  {content:'Our business has doubled ever since we have registered with Fabpik.Fabpik seller support team was fabulous.', name:' Rahul Jaiswal'},
  {content:'Luckily i have been able to get good sales so far, I was lil skeptical in the beginning with Fabpik being a new online platform, but trust me you are on a safe platform', name:'Arlene Thomas'},
]

  
 
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sellercalculator',
  templateUrl: './sellercalculator.component.html',
  styleUrls: ['./sellercalculator.component.scss']
})
export class SellercalculatorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
 
  slideConfig = {
    arrows:true,
    dots: false,
   
    "slidesToShow":3, "slidesToScroll": 1,
    centerPadding: "20px",
    centerMargin: "20px",
    draggable: true,
    infinite: true,
    pauseOnHover: true,
    swipe: false,
    touchMove: false,
    vertical: false,
    speed: 1000,
    autoplay:true,
    autoplaySpeed: 3000,
    nextArrow:
      '<button class="btn-next" (click)="next()"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg></button>\n',
    prevArrow:
      '<button class="btn-prev" (click)="prev()"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg></button>\n',
    adaptiveHeight: false,}
    
    public testimonials=[

      {content:'Starts at 2%, varies based on product category', title:'Product category based fees', subtitle:'REFERRAL FEES'},
      {content:'Starts at 2%, varies based on product category', title:'Product category based fees', subtitle:'REFERRAL FEES'},
      {content:'Starts at 2%, varies based on product category', title:'Product category based fees', subtitle:'REFERRAL FEES'},
      {content:'Starts at 2%, varies based on product category', title:'Product category based fees', subtitle:'REFERRAL FEES'},
      {content:'Starts at 2%, varies based on product category', title:'Product category based fees', subtitle:'REFERRAL FEES'},
     
    ]
}

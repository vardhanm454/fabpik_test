import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SellercalculatorComponent } from './sellercalculator.component';

describe('SellercalculatorComponent', () => {
  let component: SellercalculatorComponent;
  let fixture: ComponentFixture<SellercalculatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SellercalculatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SellercalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {


  public Shippings = [
    
    {
      question: '',
      ans: 'To check the status of your order, visit the ‘Orders’ page of your FabPik account. Tap the order you would like to track. Tap ‘Track’ for the product to know its whereabouts. Once your order is shipped, you can also track the package on our courier partner’s website by using the link mentioned in the tracking details page.'
    },
    {
      question: 'Do you offer Priority delivery?',
      ans: 'Unfortunately, at this moment we are unable to offer priority shipping. However, we will always do our best to deliver your product as soon as we can!'
    },
    {
      question: 'How much does shipping cost?',
      ans: 'The FabPik FREE shipping for all orders above Rs. 999 in India. A shipping charge of Rs. 100 is payable only on orders below Rs. 999.'
    },
    {
      question: 'Is there any Cash on Delivery Charges?',
      ans: 'Yes, there will be a minimal Cash on delivery charges and that can be see at your total cart value.'
    },
    {
      question: 'What is the estimated delivery time?',
      ans: 'Orders in India, once shipped, are typically delivered in 2-6 business days Delivery time may vary depending upon the shipping address and other factors (public holidays, extreme weather conditions, etc.).'
    },
    {
      question: 'Why cant I track my order even though it has been shipped?',
      ans: 'Courier services usually take up to 24 hours to activate tracking for an order once it has been shipped. Please check again after the mentioned time.'
    },
    {
      question: 'I missed the delivery of my order today. What should I do',
      ans: 'If you are not present when our courier partner attempts to deliver your order, they will try to contact you. Two additional attempts will be made on consecutive days, after which your package will be returned to our fulfilment centre and your account will be refunded for the order amount. In case your order comes back to our fulfilment centre, you can request for it to be re-dispatched depending on the availability (This is not applicable on Cash on Delivery Orders).'
    },
    {
      question: 'Do you deliver internationally?',
      ans: 'Unfortunately, we only deliver within India, but we do accept most international credit cards.'
    }
  ]
  public Cancellation  = [
    
    {
      question: 'How Do I cancel my order?',
      ans: 'If the order or the item(s) that you want to cancel have not been shipped yet, you can cancel it right from your account dashboard or write to our customer support team on vendorsupport@fabpik.inn or call us on +91 9948037153.'
    },
    {
      question: 'My order status says my order has been cancelled. I have not cancelled it. What happened?',
      ans: 'My order status says my order has been cancelled. I have not cancelled it. What happened?'
    },
    {
      question: 'I just cancelled my order. When will I receive my refund?',
      ans: 'if you have selected Cash on Delivery, there’s no amount to be refunded because you haven t paid for your order. For payments made via Credit/Debit Card/Netbanking, you will receive your refund to the source account within 7 business days from the time you cancel your order.'
    },
   
  
  ]

  public payment=[
    {

   
    question: 'What is Fabpik wallet?',

    ans: 'You can use Fabpik wallet for your future purchase which has unlimited validity even you can transfer the amount of Fabpik wallet to your source account.'
  },
  {
  question: 'How will I get refund?',
    
    ans: 'In case of refund to your Credit/Debit/Netbanking account, we take 7 Working days from the time you cancel your order. If you have placed your order using Cash on Delivery, there will be no refund because the order has not been paid for.'
  },
  {
    question: ' My amount got debited but order did not get confirmed, what should I do?',
    
    ans: 'Generally, it happens due to technical glitch, the amount will get reversed to your account within 7 Working days. If not credited to your account within 7 Working days. Kindly write to our customer support team on vendorsupport@fabpik.in .'
    }  ,
  {
    question: 'I had ordered on COD. But I want to pay using Debit/Credit/Net banking now. How can I do this?',
    
    ans: 'We currently do not support online payments for orders placed with Cash on Delivery payment option.'
   } ,
  {
   question: 'Can I pay by Debit/Credit card on delivery of my order?',
    
    
    ans: 'We currently do not provide POS machines on delivery.'
   } ,
  {
    question: 'Why was my Credit/Debit/Net banking account debited twice?',
    
    ans: 'Kindly write to our customer support team on vendorsupport@fabpik.in .There might have been an issue with the payment gateway. We will refund the additional amount you were charged within 24 hours. The refund will reflect in your account in 7 business days.'
   } ,
  {
    question: ' My amount got debited but order did not get confirmed, what should I do?',
    
    ans: 'Kindly write to our customer support team on vendorsupport@fabpik.in .We could not process your order because we did not receive a confirmation from the payment gateway. If you have been charged, your refund will be processed within 24 hours and will reflect in your account in 7 business days. You can place your order once again.'
  },
  {
    question: 'I have returned the item to you myself. How do I get a refund for my courier charges?',
    
    ans:'Please email the courier receipt with your order number to vendorsupport@fabpik.in, We will reimburse the charges to your FabPik wallet which has unlimited validity and can be used for future purchase. The courier charges should not be more than INR 150.'
  },
  {
    question: 'I still have not received the refund, and the standard timeframe has passed, what shall I do?',
    
    ans:'If you have not received a refund yet, request you to check your bank account/bank statement. If it is a Payment Online refund, then request you to contact your debit or credit card company, it may take some time before your refund is officially posted. If you have done all of this and you still have not received your refund yet, request you to get the same in written from your respective bank and share it with us on vendorsupport@fabpik.in .'
  },
  {
    question: 'I am being charged GST amount on my order. What is GST?',
    
    ans:'GST is a single tax on the supply of goods and services that is levied on every value addition (through production and services) and is added to a product s sale price. GST has to borne/paid by the ultimate consumer of the product or service. If your order is fulfilled on or after July 1st',  
 
   }
   ]

   public return=[
    {
      question: ' How do I place a return request?',
      ans: '  You can return products purchased from us within 7 days of receiving the product, except for personal use items such as Baby Diapers, innerwear and wipes, as well as food items. Login to your account and go the ‘My Orders’ section. Click on ‘View/Edit Order Details’ for the respective order.'
    },{
      question: '  How do I get a replacement for an item I ordered?',
      ans: '  We are sorry to inform you, at this moment we are accepting only refund request.'
    }, {
      question: '  My Pincode is not eligible for Pick-up, how do I place a return request?',
      ans: '  In case your area pin code is not eligible for pick-up, please self-ship the product to us. The product must be unused, unwashed, Freebies, accessories and all the tags must be intact. Please pack the product with the order invoice and tax invoice in the return packet. In the absence of the invoice, please mention the order number and your phone number on a piece of paper along with the package.'
    },{
      question: '  Please send the package to the following address:',
      ans: '  Fabpik Ecomm LLP, 2nd Floor, VSSR Square, Madhapur, Hitech City, Hyderabad – 500081.'
    },  
    {
      question: ' What I do received Damaged or Wrong product?',
    
      ans: ' Damaged/Different Product can be exchanged with the right product provided a clear video is taken while opening the package and the defect is seen. Also, its essential to retain all the labelling and tags whatsoever comes along with package in order to qualify for full and proper exchange.'
    },
   ]
   public AccountSetting  = [

    {
      question: '    I cannot sign into my account.',
      ans: 'If you are using the website, click the Sign In button at the top right corner of the page. Click on Forgot and enter your registered email id. We will email you a link. Click on the link and enter a new password.'
    },
    {
      question: 'I want to change my account information.',
      ans: ' To change your account information, please visit your account profile details . '
    },
    
    {
      question: 'How do I subscribe to emails and SMS?',
      ans: 'Please email your registered email id and mobile number to wecare@hopscotch.in with the subject line “Subscribe to promotions”.'
    },
    {
      question: 'I do not want to receive promotional Emails and SMS. How can I unsubscribe?',
      ans: 'Please email your registered email id and mobile number to vendorsupport@fabpik.in with the subject line “Unsubscribe from promotions”.'
     
    },
  
  ]
   public Generic  = [
    
    {
      question: 'How are items packaged?',
      ans: 'We package our products in boxes, which are covered in a plastic layer. Each individual product is packaged in bubble wrap while fragile items like bottles are safely secured with additional bubble wrap. We pride ourselves on the quality of our packaging. Till date, we have received minimal complaints about damaged products due to our packaging. '
    },
    {
      question: 'Can I modify the shipping address of my order after it has been placed?',
      ans: '  Yes, we can modify the shipping address only if your order has not been shipped. Please email your order number and new shipping address to vendorsupport@fabpik.in with the subject line. We will modify the address as per the possibility, '
    },
    
    {
      question: 'What is Pre-order?',
      ans: 'Pre-order indicates that certain products can be booked ahead of being shipped by the vendor: this is the case with several of our international and domestic brands. With this nifty feature, we manage to offer you a wider array of products at prices that are far more competitive. The delivery time for pre-order can be found on the product page and may be anywhere from 4 to 5 weeks depending upon vendor location. Why my account is blocked? Unfortunately, we fount '
    },
    {
      question: 'I want to place a bulk order/I have a business query',
      ans: 'Please email the details of the order you wish to place to businessinquiry@hopscotch.in with the subject line “Bulk order”.'
     
    },
  
  ]

  constructor() { }

  ngOnInit(): void {
  }

}

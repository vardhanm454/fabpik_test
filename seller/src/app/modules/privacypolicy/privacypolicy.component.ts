import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacypolicy',
  templateUrl: './privacypolicy.component.html',
  styleUrls: ['./privacypolicy.component.scss']
})
export class PrivacypolicyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  public Shippings = [
    
    {
      question: 'How do I track my Order status?',
      ans: 'To check the status of your order, visit the ‘Orders’ page of your FabPik account. Tap the order you would like to track. Tap ‘Track’ for the product to know its whereabouts. Once your order is shipped, you can also track the package on our courier partner’s website by using the link mentioned in the tracking details page.'
    },
    {
      question: 'Do you offer Priority delivery?',
      ans: 'Unfortunately, at this moment we are unable to offer priority shipping. However, we will always do our best to deliver your product as soon as we can!'
    },
    {
      question: 'How much does shipping cost?',
      ans: 'The FabPik FREE shipping for all orders above Rs. 999 in India. A shipping charge of Rs. 100 is payable only on orders below Rs. 999.'
    },
    {
      question: 'Is there any Cash on Delivery Charges?',
      ans: 'Yes, there will be a minimal Cash on delivery charges and that can be see at your total cart value.'
    },
    {
      question: 'What is the estimated delivery time?',
      ans: 'Orders in India, once shipped, are typically delivered in 2-6 business days Delivery time may vary depending upon the shipping address and other factors (public holidays, extreme weather conditions, etc.).'
    },
    {
      question: 'Why cant I track my order even though it has been shipped?',
      ans: 'Courier services usually take up to 24 hours to activate tracking for an order once it has been shipped. Please check again after the mentioned time.'
    },
    {
      question: 'I missed the delivery of my order today. What should I do',
      ans: 'If you are not present when our courier partner attempts to deliver your order, they will try to contact you. Two additional attempts will be made on consecutive days, after which your package will be returned to our fulfilment centre and your account will be refunded for the order amount. In case your order comes back to our fulfilment centre, you can request for it to be re-dispatched depending on the availability (This is not applicable on Cash on Delivery Orders).'
    },
    {
      question: 'Do you deliver internationally?',
      ans: 'Unfortunately, we only deliver within India, but we do accept most international credit cards.'
    }
  ]
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FaqComponent } from './modules/faq/faq.component';
import { HomeComponent } from './modules/home/home.component';
import { PrivacypolicyComponent } from './modules/privacypolicy/privacypolicy.component';
import { SellercalculatorComponent } from './modules/sellercalculator/sellercalculator.component';
import { TermsandconditionsComponent } from './modules/termsandconditions/termsandconditions.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'faq', component: FaqComponent},
  { path: 'privacypolicy', component: PrivacypolicyComponent},
  { path: 'termsandconditions', component:TermsandconditionsComponent},
  { path: 'sellercalculator', component:SellercalculatorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

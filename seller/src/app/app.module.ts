import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule,NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './modules/home/home.component';
import { FaqComponent } from './modules/faq/faq.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { PrivacypolicyComponent } from './modules/privacypolicy/privacypolicy.component';
import { TermsandconditionsComponent } from './modules/termsandconditions/termsandconditions.component';
import { MatTabsModule,MatExpansionModule } from '@angular/material';
import { SellercalculatorComponent } from './modules/sellercalculator/sellercalculator.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FaqComponent,
    PrivacypolicyComponent,
    TermsandconditionsComponent,
    SellercalculatorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    CarouselModule,
    MatTabsModule,
    MatExpansionModule,
    BrowserAnimationsModule,
    SlickCarouselModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./helpers/auth.guard";

// import { HomeoneComponent } from './modules/homeone/homeone.component';
import { CouponsComponent } from "./modules/pages/coupons/coupons.component";
import { ErrorComponent } from "./modules/pages/error/error.component";
import { HomeComponent } from "./modules/pages/home/home.component";
import { OffersComponent } from "./modules/pages/offers/offers.component";
import { SearchComponent } from "./modules/pages/search/search.component";
import { WishlistComponent } from "./modules/pages/wishlist/wishlist.component";
import { ShoppingcartComponent } from "./modules/shop/shoppingcart/shoppingcart.component";
import { OrderConfirmComponent } from "./modules/pages/order-confirm/order-confirm.component";
import { CategoryComponent } from "./modules/pages/category/category.component";
import { SubcategoryComponent } from "./modules/pages/subcategory/subcategory.component";
import { SubSubcategoryComponent } from "./modules/pages/sub-subcategory/sub-subcategory.component";
import { SupportComponent } from "./modules/pages/support/support.component";
import { ProductlistComponent } from "./modules/pages/productlist/productlist.component";
import { ProductdetailsComponent } from "./modules/pages/productdetails/productdetails.component";
import { SellerShopComponent } from "./modules/seller-shop/seller-shop.component";
// import { OrderConfirm1Component } from './modules/pages/order-confirm1/order-confirm1.component';
import { PoliciesComponent } from "./modules/pages/policies/policies.component";
import { NotfoundComponent } from "./modules/pages/notfound/notfound.component";
import { HomeMobileComponent } from "./modules/pages/home-mobile/home-mobile.component";
import { LegalComponent } from "./modules/pages/legal/legal.component";
import { FeedbackformComponent } from "./modules/pages/feedbackform/feedbackform.component";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
    pathMatch: "full",
  },

  {
    path: "home",
    component: HomeComponent,
  },

  {
    path: "",
    component: HomeMobileComponent,
  },

  {
    path: "products",
    component: ProductlistComponent,
  },
  {
    path: "products/:product_variant_slug/:unique_id",
    component: ProductdetailsComponent,
  },

  {
    path: "category/:slug/:subslug/:childslug",
    component: ProductlistComponent,
  },

  {
    path: "category",
    component: CategoryComponent,
  },

  {
    path: "policies/:policyname",
    component: PoliciesComponent,
  },

  {
    path: "category/:slug/:subslug",
    component: SubSubcategoryComponent,
  },
  {
    path: "wishlist",
    component: WishlistComponent,
  },

  {
    path: "order-confirm",
    component: OrderConfirmComponent,
  },

  {
    path: "search",
    component: SearchComponent,
  },

  {
    path: "legal",
    component: LegalComponent,
  },

  {
    path: "support",
    component: SupportComponent,
  },

  {
    path: "shoppingcart",
    component: ShoppingcartComponent,
  },

  {
    path: "offers",
    component: OffersComponent,
  },

  {
    path: "coupons",
    component: CouponsComponent,
  },

  {
    path: "category/:slug",
    component: SubcategoryComponent,
  },

  {
    path: "category/:subslug/:slug",
    component: SubSubcategoryComponent,
  },

  {
    path: "wishlist",
    component: WishlistComponent,
  },

  {
    path: "seller-shop/:slug",
    component: SellerShopComponent,
  },

  {
    path: "feedback",
    component: FeedbackformComponent,
  },

  {
    path: "myaccount",
    loadChildren: () =>
      import("./modules/dashboard/dashboard.module").then(
        (module) => module.DashboardModule
      ),
    canActivate: [AuthGuard],
  },

  {
    path: "",
    loadChildren: () =>
      import("./modules/authentication/authentication.module").then(
        (module) => module.AuthenticationModule
      ),
  },

  {
    path: "shop",
    loadChildren: () =>
      import("./modules/shop/shop.module").then((module) => module.ShopModule),
  },

  {
    path: "**",
    pathMatch: "full",
    component: NotfoundComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: "enabled",
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}

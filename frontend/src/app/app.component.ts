import { Component, HostListener, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { GoogleTagManagerService } from "angular-google-tag-manager";
import { AuthenticationService } from "./service/authentication/authentication.service";
import { DataService } from "./service/data/data.service";
import { ShopService } from "./service/shop/shop.service";
import { AngularFireMessaging } from "@angular/fire/messaging";
import { BehaviorSubject } from "rxjs";
import { AngularnotifierService } from "./service/angularnotifier/angularnotifier.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  toastyPosition: string;
  currentMessage = new BehaviorSubject(null);

  constructor(
    private router: Router,
    public dataService: DataService,
    public shopService: ShopService,
    private gtmService: GoogleTagManagerService,
    public authenticationService: AuthenticationService,
    private afMessaging: AngularFireMessaging,
    public angularnotifierService:AngularnotifierService
  ) {}

  ngOnInit() {
    console.log(this.isMobile())
    console.log(navigator.userAgent)
    /* common fuctions */
    if(!this.isMobile()){
      this.afMessaging.messages.subscribe((payload: any) => {
        if (localStorage.getItem("customer") !== null) {
          var token = JSON.parse(localStorage.getItem("customer"));
          if (!this.authenticationService.tokenExpired(token.access_token)) {
            this.shopService.getNotificationsCount().subscribe((res: any) => {
              if(res.success){
                this.dataService.changeNotificationsCount(res.notifications_count);
              }else{
                this.angularnotifierService.showNotification("error", res.msg);
              }
            });
          }
        }
        let notify_data = payload.notification;
        let title = notify_data.title;
        let options = {
          body: notify_data.body,
        };
        let notify: Notification = new Notification(title, options);
        notify.onclick = (event) => {
          window.location.href = "https://www.google.com";
        };
        this.currentMessage.next(payload);
      });
    }


    function el(selector) {
      return document.querySelector(selector);
    }
    function els(selector) {
      return document.querySelectorAll(selector);
    }
    function on(selector, event, action) {
      els(selector).forEach((e) => e.addEventListener(event, action));
    }
    function cookie(name) {
      let c = document.cookie
        .split("; ")
        .find((cookie) => cookie && cookie.startsWith(name + "="));
      return c ? c.split("=")[1] : false;
    }
    // if (localStorage.getItem("customer") !== null) {
    //   var token = JSON.parse(localStorage.getItem("customer"));
    //   var tokenExpiry = this.authenticationService.tokenExpired(
    //     token.access_token
    //   );
    //   if (!tokenExpiry) {
      if(!this.isMobile()){
        if (!cookie("notification") && Notification.permission == "default") {
          // this.displayPopUp = true;
          el(".cookie-popup").classList.add("cookie-popup--not-accepted");
        }
        if (Notification.permission == "granted") {
          // this.displayPopUp = false;
          el(".cookie-popup").classList.add("cookie-popup--accepted");
        }
      }
    //   }else{

    //   }
    // }

    on(".cookie-popup #allow", "click", () => {
      el(".cookie-popup").classList.add("cookie-popup--accepted");
      el(".cookie-popup").classList.remove("cookie-popup--not-accepted");
      // this.shopService.enableNotifications().subscribe((res: any) => {
      //   if (res.success) {
      const date = new Date();
      date.setTime(date.getTime() + -1 * 24 * 60 * 60 * 1000);
      document.cookie =
        "notification='yes'; expires=" + date.toUTCString() + "; path=/";
      if(!this.isMobile()){
        this.notificationPermissionRequest();
      }
      // window.location.reload();
      // }
      // });
    });

    on(".cookie-popup #not_allow", "click", () => {
      el(".cookie-popup").classList.add("cookie-popup--accepted");
      const date = new Date();
      date.setTime(date.getTime() + 7 * 24 * 60 * 60 * 1000);
      document.cookie =
        "notification='yes'; expires=" + date.toUTCString() + "; path=/";
    });
    /* popup button hanler */

    /* page buttons handlers */

    function _reset() {
      document.cookie = "cookie-accepted=false";
      document.location.reload();
    }

    function _switchMode(cssClass) {
      el(".cookie-popup").classList.toggle(cssClass);
    }
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });

    this.shopService.getCategory().subscribe((res: any) => {
      if(res.success){
        this.dataService.changeCategories(res.categories);
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });

    this.router.events.forEach((item) => {
      if (item instanceof NavigationEnd) {
        const gtmTag = {
          event: "page",
          pageName: item.url,
        };

        this.gtmService.pushTag(gtmTag);
      }
    });
  }

  isMobile() {
    return /iPhone|iPad|iPod/i.test(navigator.userAgent);
  }

  notificationPermissionRequest() {
    this.afMessaging.requestToken.subscribe(
      (token) => {
        // this.afMessaging.getToken.subscribe((token) => {
        if (token != null) {
          if (localStorage.getItem("customer") != null) {
            var jwt_token = JSON.parse(localStorage.getItem("customer"));
            var tokenExpiry = this.authenticationService.tokenExpired(
              jwt_token.access_token
            );
            if (tokenExpiry) {
              this.shopService
                .updateFCMTokenPublic({ fcm_token: token })
                .subscribe((res: any) => {
                  if(!res.success){
                    this.angularnotifierService.showNotification("error", res.msg);
                  }
                });
            } else {
              this.shopService
                .updateOrInsertFCMTokenLoggedInUsers({
                  fcm_token: token,
                })
                .subscribe((res: any) => {
                  if(!res.success){
                    this.angularnotifierService.showNotification("error", res.msg);
                  }
                });
            }
          } else {
            this.shopService
              .updateFCMTokenPublic({ fcm_token: token })
              .subscribe((res: any) => {
                if(!res.success){
                  this.angularnotifierService.showNotification("error", res.msg);
                }
              });
          }
        }
        // });
      },
      (error) => {
        console.error(error);
      }
    );
  }
  el(selector) {
    return document.querySelector(selector);
  }

  allowNotification() {
    this.el(".cookie-popup").classList.add("cookie-popup--accepted");
  }
}

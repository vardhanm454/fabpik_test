import { FormGroup } from "@angular/forms";
import { formatDate } from "@angular/common";

// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (
      (control.value !== null || matchingControl.value !== null) &&
      control.value !== matchingControl.value
    ) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

// custom validator to validate that two dates are valid
export function ValidateDates(controlName: string, compareControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const compareControl = formGroup.controls[compareControlName];

    if (compareControl.errors && !compareControl.errors.validateDates) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (
      control.value !== null &&
      control.value != "" &&
      compareControl.value != "" &&
      compareControl.value !== null &&
      compareControl.value < control.value
    ) {
      compareControl.setErrors({ validateDates: true });
    } else {
      compareControl.setErrors(null);
    }
  };
}

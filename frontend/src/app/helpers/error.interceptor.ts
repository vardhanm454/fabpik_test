import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((err) => {
        console.log(err.error.error)
        if ([401, 403].includes(err.status)) {
          // debugger;
          // auto logout if 401 or 403 response returned from api
          if (
            request.url == `${environment.apiUrl}/customer/wishlists/count` ||
            request.url == `${environment.apiUrl}/cart/count`
          ) {
            this.authenticationService.logout();
            return next.handle(request);
          } else {
            this.authenticationService.logout();
          }
        }
        const error = err.error?.message || err.statusText;
        return throwError(error);
      })
    );
  }
}

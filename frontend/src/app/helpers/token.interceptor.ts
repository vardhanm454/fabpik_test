import { Injectable } from '@angular/core';
import {HttpRequest,HttpHandler,HttpEvent,HttpInterceptor} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/service/authentication/authentication.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const customer = this.authenticationService.customerValue;
        const isLoggedIn = customer && customer.access_token;
        const isApiUrl = request.url.startsWith(environment.apiUrl);
        
        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    'Authorization': `Bearer ${customer.access_token}`,
                    'Accept'       : 'application/json',
                    'api-version'  : '1.0.0' 
                    //,
                    //'Content-Type': 'multipart/form-data'
                }
            });
        }else{
          request = request.clone({
            setHeaders: {
                'Accept'       : 'application/json',
                'api-version'  : '1.0.0' 
                //,
                //'Content-Type': 'multipart/form-data'
            }
        });
        }

    return next.handle(request);
  }
}

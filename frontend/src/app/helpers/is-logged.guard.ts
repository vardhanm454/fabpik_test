 import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthenticationService } from "../service/authentication/authentication.service";

@Injectable({
  providedIn: "root",
})
export class IsLoggedGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        return true;
      } else {
        this.router.navigate(["/"]);
        return false;
      }
    } else {
      return true;
    }
  }
}

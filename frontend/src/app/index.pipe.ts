import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'index'
})
export class IndexPipe implements PipeTransform {

  transform(items: any[], item: any): any {
    return items.indexOf(item);
  }

}

import { Component, HostListener, Input, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { ShopService } from "src/app/service/shop/shop.service";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { Router } from "@angular/router";
import { DataService } from "src/app/service/data/data.service";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
@Component({
  selector: "app-breadcrumb",
  templateUrl: "./breadcrumb.component.html",
  styleUrls: ["./breadcrumb.component.scss"],
})
export class BreadcrumbComponent implements OnInit {
  @Input() headername: string; // decorate the property with @Input()
  cartcount: any = 0;
  fixed: boolean;

  constructor(
    private _location: Location,
    private shopService: ShopService,
    public authService: AuthenticationService,
    public router: Router,
    public dataService: DataService,
    public angularnotifierService:AngularnotifierService
  ) {}

  ngOnInit(): void {
    this.dataService.currentCartCount.subscribe((res) => {
      this.cartcount = res;
    });
  }

  goback() {
    this._location.back();
  }

  getCartCount() {
    if (localStorage.getItem("customer") !== null) {
      this.shopService.getCartCount().subscribe((res:any) => {
        if(res.success){
          this.dataService.changeCartCount(res.count);
        }else{
          this.angularnotifierService.showNotification(
            "error",
            res.msg
          );
        }
      });
    }
  }

  @HostListener("window:scroll", ["$event"])
  onWindowScroll(e) {
    if (window.pageYOffset > 150) {
      this.fixed = true;
    } else {
      this.fixed = false;
    }
  }

  gotoCart() {
    this.router.navigate(["shop/shoppingcart"]);
  }
}

import { HttpClient } from "@angular/common/http";
import { Component, OnInit, Input } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable } from "rxjs";

@Component({
  selector: "app-size-chart",
  templateUrl: "./size-chart.component.html",
  styleUrls: ["./size-chart.component.scss"],
})
export class SizeChartComponent implements OnInit {
  private _jsonURL = "assets/js/filters.json";
  @Input() sizeChartNotes: any;
  @Input() productSizeChartTitle: any;
  @Input() sizeChartData: any;
  @Input() sizeChartImageTitle: any;
  @Input() sizeChartImages: any;
  @Input() sizes: any;
  @Input() columns: any;
  @Input() notes: any;
  @Input() serverSizeChartUrl: any;
  sizecharts: any;
  loading: Boolean = true;
  constructor(public modalService: NgbModal, public http: HttpClient) {
    // this.sizeChartDataParsed = JSON.parse(this.sizeChartData);
    // this.sizeChartImagesParsed = JSON.parse(this.sizeChartImages);
    // this.columnsParsed = JSON.parse(this.columns);
    // this.sizesParsed = JSON.parse(this.sizes);
  }

  public getJSON(): Observable<any> {
    return this.http.get(this._jsonURL);
  }

  ngOnInit(): void {
    this.sizes = this.sizes
      .replace("[", "")
      .replace("]", "")
      .replace('"', "")
      .replace('"', "")
      .split(",");
    this.columns = this.columns
      .replace("[", "")
      .replace("]", "")
      .replace('"', "")
      .replace('"', "")
      .split(",");

    this.sizeChartData = JSON.parse(this.sizeChartData);
    this.sizeChartImages = JSON.parse(this.sizeChartImages);
    this.getJSON().subscribe((data) => {
      this.sizecharts = data;
    });
    this.loading = false;
  }

  close() {
    this.modalService.dismissAll();
  }

  onImgError(e) {
    e.target.src = "/assets/images/no-image.jpg";
  }
}

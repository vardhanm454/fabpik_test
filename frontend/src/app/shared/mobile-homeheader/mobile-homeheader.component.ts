import { DOCUMENT, ViewportScroller } from "@angular/common";
import { Component, HostListener, Inject, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { first } from "rxjs/operators";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { DataService } from "src/app/service/data/data.service";
import { ShopService } from "src/app/service/shop/shop.service";
declare var $: any;
@Component({
  selector: "app-mobile-homeheader",
  templateUrl: "./mobile-homeheader.component.html",
  styleUrls: ["./mobile-homeheader.component.scss"],
})
export class MobileHomeheaderComponent implements OnInit {
  fixed: boolean = false;
  AllCategory: any;
  AllSubCategory: any;
  cartcount: any = 0;
  deferredPrompt: any;
  showButton = false;
  categorySlug: any;
  subCategorySlug: any;
  logoutVisible: Boolean = false;

  constructor(
    @Inject(DOCUMENT) document,
    private shopService: ShopService,
    public dataService: DataService,
    public router: Router,
    public authService: AuthenticationService,
    public angularnotifierService:AngularnotifierService
  ) {}

  ngOnInit(): void {
    this.dataService.currentCartCount.subscribe((res) => {
      this.cartcount = res;
    });
    this.getCategorySubCategory();
    this.getCartCount();
    var values = JSON.parse(localStorage.getItem("customer"));
    if (values) {
      if (this.authService.tokenExpired(values.access_token)) {
        this.logoutVisible = false;
      } else {
        this.logoutVisible = true;
      }
    } else {
      this.logoutVisible = false;
    }
    const body = $("body");
    const mobileMenu = $(".mobile-menu");
    const mobileMenuBody = mobileMenu.children(".mobile-menu__body");

    if (mobileMenu.length) {
      const open = function () {
        const bodyWidth = body.width();
        // body.css('overflow', 'hidden');
        body.css("paddingRight", body.width() - bodyWidth + "px");

        mobileMenu.addClass("mobile-menu--open");
      };
      const close = function () {
        body.css("overflow", "auto");
        body.css("paddingRight", "");

        mobileMenu.removeClass("mobile-menu--open");
      };

      $(".home-header__menu-button").on("click", function () {
        open();
      });
      $(".mobile-menu__backdrop, .mobile-menu__close").on("click", function () {
        close();
      });
    }

    const panelsStack = [];
    let currentPanel = mobileMenuBody.children(".mobile-menu__panel");

    mobileMenu.on("click", "[data-mobile-menu-trigger]", function (event) {
      const trigger = $(this);
      const item = trigger.closest("[data-mobile-menu-item]");
      let panel = item.data("panel");

      if (!panel) {
        panel = item
          .children("[data-mobile-menu-panel]")
          .children(".mobile-menu__panel");

        if (panel.length) {
          mobileMenuBody.append(panel);
          item.data("panel", panel);
          panel.width(); // force reflow
        }
      }

      if (panel && panel.length) {
        event.preventDefault();

        panelsStack.push(currentPanel);
        currentPanel.addClass("mobile-menu__panel--hide");

        panel.removeClass("mobile-menu__panel--hidden");
        currentPanel = panel;
      }
    });
    mobileMenu.on("click", ".mobile-menu__panel-back", function () {
      currentPanel.addClass("mobile-menu__panel--hidden");
      currentPanel = panelsStack.pop();
      currentPanel.removeClass("mobile-menu__panel--hide");
    });
  }
  @HostListener("window:beforeinstallprompt", ["$event"])
  onbeforeinstallprompt(e) {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault();
    // Stash the event so it can be triggered later.
    this.deferredPrompt = e;
    this.showButton = true;
  }
  addToHomeScreen() {
    // hide our user interface that shows our A2HS button
    this.showButton = false;
    // Show the prompt
    this.deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    this.deferredPrompt.userChoice.then((choiceResult) => {
      if (choiceResult.outcome === "accepted") {
      } else {
      }
      this.deferredPrompt = null;
    });
  }

  gotoCart() {
    this.router.navigate(["shop/shoppingcart"]);
  }

  categoryClick(cat) {
    if (cat == "Organic") {
      this.router.navigate(["products"], {
        queryParams: {
          q: "organic",
        },
      });
    }
  }

  @HostListener("window:scroll", ["$event"])
  onWindowScroll(e) {
    if (window.pageYOffset > 100) {
      this.fixed = true;
    } else {
      this.fixed = false;
    }
  }

  //get cart count
  getCartCount() {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      if (this.authService.tokenExpired(token.access_token)) {
        return;
      } else {
        this.shopService.getCartCount().subscribe((res:any) => {
          if(res.success){
            this.dataService.changeCartCount(res.count);
          }else{
            this.angularnotifierService.showNotification(
              "error",
              res.msg
            );
          }
        });
      }
    } else {
      return;
    }
  }

  // logout
  logout() {
    this.authService.logout();
  }

  // get category list
  getCategorySubCategory() {
    this.dataService.currentCategories.subscribe((res) => {
      if (res != null) {
        this.AllCategory = res;
      }
    });
  }

  //get sub and child category list
 
  childcatid(id, categoryslug, subcategoryslug, childcategoryslug) {
    localStorage.setItem("childcategoryid", id);
    localStorage.setItem("childcategoryslug", childcategoryslug);
    localStorage.setItem("filterbasedId", "childcategory");
  }
}

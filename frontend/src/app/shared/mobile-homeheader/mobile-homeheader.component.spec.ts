import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileHomeheaderComponent } from './mobile-homeheader.component';

describe('MobileHomeheaderComponent', () => {
  let component: MobileHomeheaderComponent;
  let fixture: ComponentFixture<MobileHomeheaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileHomeheaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileHomeheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

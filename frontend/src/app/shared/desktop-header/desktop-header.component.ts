import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  HostListener,
  NgZone,
} from "@angular/core";
import { Location } from "@angular/common";
import { first } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { SharedService } from "src/app/shared/shared.service";
import { ShopService } from "src/app/service/shop/shop.service";
import { SearchService } from "src/app/service/search/search.service";
import { AuthenticationService } from "../../service/authentication/authentication.service";
import { DataService } from "src/app/service/data/data.service";
import { Observable } from "rxjs";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
@Component({
  selector: "app-desktop-header",
  templateUrl: "./desktop-header.component.html",
  styleUrls: ["./desktop-header.component.scss"],
})
export class DesktopHeaderComponent implements OnInit {
  @Output() searchEvent = new EventEmitter<string>();
  @Output() sendCategories = new EventEmitter<any>();

  @Input() queryString: string;
  fixed: boolean;
  isbottomfixed: boolean;
  apiCalls: number = 0;
  addedClass: boolean = false;
  sellerClass: boolean = true;
  searchCategories: any = [];
  searchProducts: any = [];
  cartcount: any = 0;
  categories: any;
  childCategories: any;
  catdata: any;
  subCategories: any;
  id: any;
  keyword = "name";
  listing: any;
  myControl = new FormControl();
  product_cat: any;
  sidemenuclass: boolean = false;
  searchText: any;
  value: any;
  searchform: FormGroup;
  wishlistcount: any = 0;
  emptySearch: boolean;
  loading1: boolean = true;
  lastScrollTop: number = 0;
  direction: string = "";
  loading: boolean = true;
  parentCategories: any = [];
  isNearBottom: any;
  brands: any = [];
  searchValue: any = "";
  searchLoading: boolean = false;
  imgUrl: any;
  autoSuggestion: any = null;
  tags: any = [];
  notificationsCount: any = 0;
  constructor(
    private _location: Location,
    lc: NgZone,
    private shopService: ShopService,
    private sharedService: SharedService,
    private searchService: SearchService,
    private router: Router,
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    public dataService: DataService,
    public angularnotifierService:AngularnotifierService
  ) {
    window.onscroll = () => {
      let st = window.pageYOffset;
      let dir = "";
      if (st > this.lastScrollTop) {
        dir = "down";
        this.isbottomfixed = false;
      } else if (
        window.innerHeight + window.scrollY >=
        document.body.offsetHeight
      ) {
        this.isbottomfixed = true;
      } else {
        dir = "up";
        this.isbottomfixed = true;
      }
      this.lastScrollTop = st;
      lc.run(() => {
        this.direction = dir;
      });
    };
  }
  /*sticky Header*/
  @HostListener("window:scroll", ["$event"])
  onWindowScroll(e) {
    if (window.pageYOffset > 50) {
      this.fixed = true;
    } else {
      this.fixed = false;
    }
  }
  // @HostListener("window:scroll", ['$event'])
  // onScroll2(e): void {
  // if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
  //   console.log("hello");
  //     }
  //     else
  //     {
  //       console.log("end");
  //     }
  // }
  ngOnInit() {
    var values = JSON.parse(localStorage.getItem("customer"));
    if (values) {
      if (this.authService.tokenExpired(values.access_token)) {
        this.sellerClass = true;
      } else {
        this.sellerClass = false;
      }
    }
    this.searchform = this.formBuilder.group({
      search_query: [""],
      product_cat: [""],
    });

    if (localStorage.getItem("customer") !== null) {
      this.addedClass = true;
    }
    //   this.getCategory();
    this.dataService.currentCategories.subscribe((res) => {
      if (res != null) {
        this.apiCalls++;
        if (this.apiCalls == 4) {
          this.loading = false;
        }
        this.categories = res;
      }
    });
    this.dataService.currentWishlistCount.subscribe((res) => {
      this.wishlistcount = res;
    });
    this.dataService.currentCartCount.subscribe((res) => {
      this.cartcount = res;
    });
    this.dataService.currentNotificationsCount.subscribe((res) => {
      this.notificationsCount = res;
    });
    this.getWishlistCount();
    this.getCartCount();
    this.getNotificationsCount();
  }
  ngAfterViewInit() {
    // this.searchform.patchValue({search_query:this.queryString})
  }
  goback() {
    this._location.back();
  }
  clickCategoryMenu() {
    this.sidemenuclass = !this.sidemenuclass;
  }
  //get wishlist count
  getWishlistCount() {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      if (this.authService.tokenExpired(token.access_token)) {
        this.apiCalls++;
        if (this.apiCalls == 4) {
          this.loading = false;
        }
      } else {
        this.shopService.getWishlistCount().subscribe((res:any) => {
          this.apiCalls++;
          if (this.apiCalls == 4) {
            this.loading = false;
          }
          if(res.success){
            this.dataService.changeWishlishtCount(res.count);
          }else{
            this.angularnotifierService.showNotification("error", res.msg);
          }
        });
      }
    } else {
      this.apiCalls++;
      if (this.apiCalls == 4) {
        this.loading = false;
      }
    }
  }

  getNotificationsCount() {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      if (this.authService.tokenExpired(token.access_token)) {
        this.apiCalls++;
        if (this.apiCalls == 4) {
          this.loading = false;
        }
      } else {
        this.shopService.getNotificationsCount().subscribe((res: any) => {
          this.apiCalls++;
          if (this.apiCalls == 4) {
            this.loading = false;
          }
          if(res.success){
            this.dataService.changeNotificationsCount(res.notifications_count);
          }else{
            this.angularnotifierService.showNotification("error", res.msg);
          }
        });
      }
    } else {
      this.apiCalls++;
      if (this.apiCalls == 4) {
        this.loading = false;
      }
    }
  }

  gotoWishlist() {
    var values = JSON.parse(localStorage.getItem("customer"));
    if (values) {
      if (this.authService.tokenExpired(values.access_token)) {
        this.router.navigate(["login"], {
          queryParams: {
            q: "/wishlist",
          },
        });
      } else {
        this.router.navigate(["wishlist"]);
      }
    } else {
      this.router.navigate(["login"]);
    }
  }

  gotoCart() {
    this.router.navigate(["shop/shoppingcart"]);
  }

  //get cart count
  getCartCount() {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      if (this.authService.tokenExpired(token.access_token)) {
        this.shopService.guestCartCount().subscribe((res: any) => {
          this.apiCalls++;
          if (this.apiCalls == 4) {
            this.loading = false;
          }
          if(res.success){
            this.dataService.changeCartCount(res.count);
          }else{
            this.angularnotifierService.showNotification(
              "error",
              res.msg
            );
          }
        });
      } else {
        this.shopService.getCartCount().subscribe((res:any) => {
          this.apiCalls++;
          if (this.apiCalls == 4) {
            this.loading = false;
          }
          if(res.success){
            this.dataService.changeCartCount(res.count);
          }else{
            this.angularnotifierService.showNotification(
              "error",
              res.msg
            );
          }
        });
      }
    } else {
      this.shopService.guestCartCount().subscribe((res: any) => {
        this.apiCalls++;
        if (this.apiCalls == 4) {
          this.loading = false;
        }
        if(res.success){
          this.dataService.changeCartCount(res.count);
        }else{
          this.angularnotifierService.showNotification(
            "error",
            res.msg
          );
        }
      });
    }
  }

  onImgError(e) {
    e.target.src = "/assets/images/no-image.jpg";
  }

  logout() {
    this.authenticationService.logout();
    this.shopService.guestCartCount().subscribe((res: any) => {
      if(res.success){
        this.dataService.changeCartCount(res.count);
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
    this.dataService.changeWishlishtCount(0);
  }

  //get category list
  getCategory() {
    this.shopService
      .getCategory()
      .pipe(first())
      .subscribe((res) => {
        this.apiCalls++;
        if (this.apiCalls == 4) {
          this.loading = false;
        }
        this.categories = res;
      });
  }

  categoryid(id, categoryslug, type, category?) {
    this.sendCategories.emit({ cid: id, total: 1 });
    // const cids = [id];
    // var ccids = this.route.snapshot.params.ccids;
    // if(ccids !=undefined){
    //   ccids = JSON.parse(this.route.snapshot.params.ccids)
    // }else{
    //   ccids = []
    // }
    // this.router.navigate(['products'],
    // {queryParams:
    //     {
    //       cids:cids.length != 0 ?JSON.stringify (cids) : undefined,
    //       ccids:ccids.length != 0 ?JSON.stringify (ccids) : undefined,
    //     }
    // })
    if (type == "static") {
      this.router.navigate(["products"], {
        queryParams: {
          q: categoryslug,
        },
      });
    } else {
      this.router.navigate(["category", categoryslug]);
    }
    this.listing = {
      categoryid: id,
      categoryslug: categoryslug,
      filterbasedId: "category",
    };
    // this.shopService.Clicked(this.listing);
    localStorage.setItem("categoryid", id);
    // localStorage.setItem('categoryslug',categoryslug);
    // this.router.navigate(['/shop/listing/'+ categoryslug]);
    localStorage.removeItem("filterCId");
    localStorage.removeItem("child");
    localStorage.setItem("filterbasedId", "category");
  }
  subcategoryid(id, categoryslug, subcategoryslug, catid) {
    this.sendCategories.emit({ cid: catid, scid: id, total: 2 });
    this.listing = {
      subcategoryid: id,
      subcategoryslug: subcategoryslug,
      filterbasedId: "subcategory",
    };
    localStorage.setItem("categoryid", catid);
    localStorage.setItem("subcategoryid", id);
    // this.shopService.Clicked(this.listing);
    // localStorage.setItem('subcategoryid',id);
    // localStorage.setItem('subcategoryslug',subcategoryslug);
    this.router.navigate(["category", categoryslug, subcategoryslug]);
    localStorage.setItem("subfilterid", id);
    localStorage.removeItem("child");
    localStorage.removeItem("filterCId");
    localStorage.setItem("filterbasedId", "subcategory");
  }
  childcatid(child, subcatSlug, categorySlug) {
    this.sendCategories.emit({
      cid: child.category_id,
      scid: child.subcategory_id,
      ccids: [child.id],
      total: 3,
    });
    localStorage.setItem("catid", child.category_id);
    localStorage.setItem("catslug", categorySlug);
    localStorage.setItem("subid", child.subcategory_id);
    localStorage.setItem("subslug", subcatSlug);
    localStorage.setItem("childcategoryid", child.id);
    localStorage.setItem("childcategoryslug", child.slug);
    // routerLink="/category/{{
    //   category.slug
    // }}/{{ subcat.slug }}/{{ child.slug }}"
    this.router.navigate(["category", categorySlug, subcatSlug, child.slug]);
    // debugger;
    // window.location.replace()
    // console.log(subcat)
    // console.log(child)
    // this.listing={
    //   childcategoryid : childCatId,
    //   childcategoryslug :childcategoryslug,
    //   filterbasedId:'childcategory'
    // }
    // this.shopService.Clicked(this.listing);
    // console.log(this.listing);
    // localStorage.setItem('childcategoryid',id);
    // localStorage.setItem('childcategoryslug',childcategoryslug);
    // this.router.navigate(['/shop/listing/'+ categoryslug +'/'+ subcategoryslug +'/'+ childcategoryslug]);
    // localStorage.setItem("child",   id );
    // localStorage.setItem("subfilterid",   subfilterid );
    // localStorage.setItem("filterbasedId", "childcategory")
    // localStorage.setItem("filterCId", '[' + id + ']');
    // localStorage.setItem("subfilterid", subfilterid );
  }

  onSearchKey(evt) {
    this.listing = {
      name: evt.target.value,
    };
    if (window.location.href.indexOf("https://fabpik.in/") > -1) {
      let node = document.createElement("script"); // creates the script tag
      node.text = `fbq('track', 'Search')`;
      // append to head of document
      document.getElementsByTagName("head")[0].appendChild(node);
    }
    this.searchValue = evt.target.value;
    if (evt.target.value.length >= 3) {
      this.searchLoading = true;
      this.searchCategories = [];
      this.searchProducts = [];
      this.brands = [];
      this.tags = [];
      if (this.autoSuggestion) {
        this.autoSuggestion.unsubscribe();
        this.getSearchResult(this.listing);
      } else {
        this.getSearchResult(this.listing);
      }
    } else {
      this.searchCategories = [];
      this.searchProducts = [];
      this.brands = [];
      this.tags = [];
      this.emptySearch =
        this.searchCategories.length == 0 &&
        this.searchProducts.length == 0 &&
        this.brands.length == 0 &&
        this.tags.length == 0 &&
        this.searchValue.length >= 3
          ? true
          : false;
    }
  }

  showSubMenu(categoryTitle) {
    if (categoryTitle == "Organic" || categoryTitle == "Rakhi") {
      return false;
    } else {
      return true;
    }
  }

  getSearchResult(data) {
    this.autoSuggestion = this.shopService
      .searchautosuggestion(data)
      .subscribe((res: any) => {
        if(res.success){
        this.imgUrl = res.imgUrl;
        this.searchLoading = false;
        if (
          res.categories.length == 0 &&
          res.products.length == 0 &&
          res.brands.length == 0 &&
          res.tags.length == 0
        ) {
          this.searchCategories = res.categories;
          this.searchProducts = res.products;
          this.brands = res.brands;
          this.tags = res.tags;
          this.emptySearch =
            this.searchCategories.length == 0 &&
            this.searchProducts.length == 0 &&
            this.brands.length == 0 &&
            this.tags.length == 0 &&
            this.searchValue.length >= 3
              ? true
              : false;
        } else {
          this.searchCategories = res.categories;
          this.searchProducts = res.products;
          this.brands = res.brands;
          this.tags = res.tags;
        }
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
      });
  }
  selectEvent(item, index?, type?) {
    if (item.type == "product") {
      if (this.searchProducts[index].stock <= 0) {
        this.shopService
          .nextVariantNavigation({
            product_id: this.searchProducts[index].product_id,
          })
          .subscribe((res: any) => {
            if (res.success) {
              const url =
                "/products/" + res.data.slug + "/" + res.data.unique_id;
              window.open(url, "_blank");
            } else {
              const url =
                "/products/" +
                this.searchProducts[index].slug +
                "/" +
                this.searchProducts[index].unique_id;
              window.open(url, "_blank");
            }
          });
      } else {
        const url =
          "/products/" +
          this.searchProducts[index].slug +
          "/" +
          this.searchProducts[index].unique_id;
        window.open(url, "_blank");
      }
    } else if (type == "brand") {
      this.router.navigate(["products"], {
        queryParams: {
          brands: JSON.stringify([item.id]),
        },
      });
    } else if (type == "tags") {
      this.router.navigate(["products"], {
        queryParams: {
          tg: item.id,
        },
      });
    } else {
      localStorage.setItem("childcategoryid", item.child_id);
      localStorage.setItem("childcategoryslug", item.child_slug);
      localStorage.setItem("catid", item.category_id);
      localStorage.setItem("catslug", item.cat_slug);
      localStorage.setItem("subid", item.subcategory_id);
      localStorage.setItem("subslug", item.subcat_slug);
      localStorage.setItem("filterbasedId", "childcategory");
      this.router.navigate([
        "/category/" +
          item.cat_slug +
          "/" +
          item.subcat_slug +
          "/" +
          item.child_slug,
      ]);
    }
  }

  onSubmit() {
    // this.searchEvent.emit(this.searchform.value.name)
    this.route.queryParamMap.subscribe((params) => {
      if (this.searchform.value.search_query) {
        this.router.navigate(["products"], {
          queryParams: {
            q: this.searchform.value.search_query,
          },
        });
      }
    });
  }

  notificationClick() {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      if (this.authService.tokenExpired(token.access_token)) {
        return;
      } else {
        this.shopService.updateVisitedTimeAndDate().subscribe((res: any) => {
          if (res.success) {
            this.shopService.getNotificationsCount().subscribe((res: any) => {
              if(res.success){
                this.dataService.changeNotificationsCount(
                  res.notifications_count
                );
              }else{
                this.angularnotifierService.showNotification("error", res.msg);
              }
            });
          }
        });
      }
    }
  }
}

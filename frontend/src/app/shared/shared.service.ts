import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  private sharedData: Subject<any> = new Subject<any>();
  sharedData$: Observable<any> = this.sharedData.asObservable();

  constructor() { }

  // setData(updatedData) {
  //   this.sharedData.next(updatedData);
  // }
  setFiltersData(updatedFiltersData) {
    this.sharedData.next(updatedFiltersData);
  }
  setData(categoryid) {
    this.sharedData.next(categoryid);
  }
  
}

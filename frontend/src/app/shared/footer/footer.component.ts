import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { CommonapiService } from "src/app/service/commonapi/commonapi.service";
import { Router } from "@angular/router";
import { ShopService } from "src/app/service/shop/shop.service";
import { first } from "rxjs/operators";
import { DataService } from "src/app/service/data/data.service";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"],
})
export class FooterComponent implements OnInit {
  policies: any;
  categories: unknown;
  loading1: boolean;
  subscribeForm: FormGroup;
  public issubscribeSubmit: boolean = false;
  public subscribeLoading: boolean = false;
  success: string;
  constructor(
    private _location: Location,
    private shopService: ShopService,
    public commonapiService: CommonapiService,
    private router: Router,
    private angularnotifierService: AngularnotifierService,
    private formBuilder: FormBuilder,
    public dataService: DataService,
    public authService: AuthenticationService
  ) {}
  goback() {
    this._location.back();
  }
  get email() {
    return this.subscribeForm.controls["email"];
  }
  ngOnInit() {
    this.subscribeForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
    });
    // this.getCategory();
    this.dataService.currentCategories.subscribe((res) => {
      if (res != null) {
        this.categories = res;
      }
    });
  }

  getCategory() {
    this.shopService
      .getCategory()
      .pipe(first())
      .subscribe((res) => {
        this.categories = res;
      });
  }
  public features = [
    {
      image: "assets/images/icons/secure_payments.png",
      name: "Secured Payments",
    },
    {
      image: "assets/images/icons/safe_and_hygienic.png",
      name: "Safe And Hygienic Products",
    },
    {
      image: "assets/images/icons/quick_deliveries.png",
      name: "Super Quick Delivery",
    },
    {
      image: "assets/images/icons/best_price.png",
      name: "Best Price Challenge",
    },
  ];
  getPolicy(data) {
    this.router.navigate(["policies", data]);
  }

  gotoWishlist() {
    var values = JSON.parse(localStorage.getItem("customer"));
    if (values) {
      if (this.authService.tokenExpired(values.access_token)) {
        this.router.navigate(["login"]);
      } else {
        this.router.navigate(["wishlist"]);
      }
    } else {
      this.router.navigate(["login"]);
    }
  }

  subcategoryid(id, categoryslug, subcategoryslug, catid) {
    localStorage.setItem("categoryid", catid);
    localStorage.setItem("subcategoryid", id);

    // this.shopService.Clicked(this.listing);
    // localStorage.setItem('subcategoryid',id);
    // localStorage.setItem('subcategoryslug',subcategoryslug);
    this.router.navigate(["category", categoryslug, subcategoryslug]);
    localStorage.setItem("subfilterid", id);
    localStorage.removeItem("child");
    localStorage.removeItem("filterCId");
    localStorage.setItem("filterbasedId", "subcategory");
  }

  categoryid(id, categoryslug, category?) {
    this.router.navigate(["category", categoryslug]);
    localStorage.setItem("categoryid", id);
    localStorage.removeItem("filterCId");
    localStorage.removeItem("child");
    localStorage.setItem("filterbasedId", "category");
  }
  onsubscribeSubmit() {
    this.issubscribeSubmit = true;
    this.subscribeLoading = true;
    if (this.subscribeForm.invalid) {
      this.subscribeLoading = false;
      this.success = "";
      return;
    } else {
      this.subscribeLoading = false;
      this.issubscribeSubmit = false;

      this.angularnotifierService.showNotification(
        "default",
        "Thank You For Subscribing"
      );
      this.subscribeForm.reset();
    }
  }
}

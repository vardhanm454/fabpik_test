import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CarouselModule } from "ngx-owl-carousel-o";
import {
  MatExpansionModule,
  MatFormFieldModule,
  MatProgressBarModule,
  MatInputModule,
  MatTabsModule,
  MatTooltipModule,
  MatProgressSpinnerModule,
} from "@angular/material";
import { MatDialogModule } from "@angular/material/dialog";
import { FooterComponent } from "./footer/footer.component";
import { RouterModule } from "@angular/router";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { ScrollDirective } from "../modules/directives/scroll-directive.directive";
import { DesktopHeaderComponent } from "./desktop-header/desktop-header.component";
import { MobileHomeheaderComponent } from "./mobile-homeheader/mobile-homeheader.component";
import { BreadcrumbComponent } from "./breadcrumb/breadcrumb.component";
import { DesktopSideMenuComponent } from "./desktop-side-menu/desktop-side-menu.component";

// import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ReviewsComponent } from "../modules/dashboard/reviews/reviews.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgxSkeletonLoaderModule } from "ngx-skeleton-loader";
import { DashboardListComponent } from "../modules/dashboard/dashboard-list/dashboard-list.component";
// import { GlobalsearchFiltersComponent } from './globalsearch-filters/globalsearch-filters.component';
import { SlickCarouselModule } from "ngx-slick-carousel";
import { NotifierModule, NotifierOptions } from "angular-notifier";
import { CountdownModule } from "ngx-countdown";
// import { SizeChartComponent } from "./size-chart/size-chart.component";
import { NgxCaptchaModule } from "ngx-captcha";

const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: "right",
      distance: 12,
    },
    vertical: {
      position: "bottom",
      distance: 12,
      gap: 10,
    },
  },
  theme: "material",
  behaviour: {
    // autoHide: 5000,
    onClick: "hide",
    onMouseover: "pauseAutoHide",
    showDismissButton: true,
    stacking: 1,
  },
  animations: {
    enabled: true,
    show: {
      preset: "slide",
      speed: 300,
      easing: "ease",
    },
    hide: {
      preset: "fade",
      speed: 300,
      easing: "ease",
      offset: 50,
    },
    shift: {
      speed: 300,
      easing: "ease",
    },
    overlap: 150,
  },
};
@NgModule({
  declarations: [
    FooterComponent,
    DashboardListComponent,
    BreadcrumbComponent,
    ScrollDirective,
    DesktopHeaderComponent,
    MobileHomeheaderComponent,
    BreadcrumbComponent,
    DesktopSideMenuComponent,
    ReviewsComponent,
    DashboardListComponent,
    // SizeChartComponent,
  ],

  imports: [
    CommonModule,
    CarouselModule,
    MatTabsModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatExpansionModule,
    RouterModule,
    Ng2SearchPipeModule,
    FormsModule,
    SlickCarouselModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    NgbModule,
    CountdownModule,
    MatProgressBarModule,
    NgxCaptchaModule,
    NgxSkeletonLoaderModule.forRoot(),
    NotifierModule.withConfig(customNotifierOptions),
  ],
  exports: [
    FooterComponent,
    CountdownModule,
    DesktopHeaderComponent,
    MobileHomeheaderComponent,
    DesktopSideMenuComponent,
    CarouselModule,
    MatTabsModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatProgressBarModule,
    MatExpansionModule,
    Ng2SearchPipeModule,
    BreadcrumbComponent,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatTooltipModule,
    SlickCarouselModule,
    MatProgressSpinnerModule,
    // SizeChartComponent,
    NgxCaptchaModule,
    DashboardListComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SharedModule {}

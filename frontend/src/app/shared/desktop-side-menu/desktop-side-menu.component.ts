import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/service/authentication/authentication.service';

@Component({
  selector: 'app-desktop-side-menu',
  templateUrl: './desktop-side-menu.component.html',
  styleUrls: ['./desktop-side-menu.component.scss']
})
export class DesktopSideMenuComponent implements OnInit {
  
  customerprofile: any;
  id: number;
  loading = true;
 
  constructor(  private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    var values = JSON.parse(localStorage.getItem("customer"));
    if(values){
      this.id  = values.id;
      this.myaccount(this.id);
    }  
    
  }
  
 // logout 
 logout() {
    this.authenticationService.logout();
  }

 // myaccount (user details)
 private myaccount(id) {
    this.authenticationService.getById(id)
      .subscribe(res => {
      this.customerprofile = res;
      this.loading = false;
    });
  }
}

import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { DataService } from "src/app/service/data/data.service";
import { ShopService } from "src/app/service/shop/shop.service";
@Component({
  selector: "app-product-item",
  templateUrl: "./product-item.component.html",
  styleUrls: ["./product-item.component.scss"],
  host: {
    "(window:resize)": "onWindowResize($event)",
  },
})
export class ProductItemComponent implements OnInit {
  isMobile: boolean = false;
  isDesktop: boolean = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth: number = 767;

  id: string;
  AllProduct: any;
  @Input() addedToWishlist: boolean;
  @Input() productItem: any;
  @Input() imgUrl: any;
  wishlistdata: any;
  wishlistCount = 0;
  cartdata: any;
  constructor(
    private shopService: ShopService,
    private angularnotifierService: AngularnotifierService,
    public dataService: DataService,
    public authService: AuthenticationService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.id = localStorage.getItem("id");
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }
  onImgError(e) {
    e.target.src = "/assets/images/no-image.jpg";
  }

  addToWishlist(product_id, product_variant_id) {
    this.wishlistCount++;
    this.wishlistdata = {
      product_id: product_id,
      product_variant_id: product_variant_id,
    };
    if (this.wishlistCount === 1) {
      this.shopService.addToWishlist(product_variant_id, product_id).subscribe({
        next: (res: any) => {
          // this.alertService.success('Product Added to the wishlist', 'success');
          this.angularnotifierService.showNotification(
            "default",
            "Product Added to the wishlist"
          );
        },
        error: (error) => {
          var err = JSON.stringify(error);
          // this.alertService.error(err, 'error');
          this.angularnotifierService.showNotification("error", err);
        },
      });
    }
  }

  addProductToCart(product_variant_id) {
    this.cartdata = {
      product_variant_id: product_variant_id,
      quantity: 1,
    };
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        this.shopService
          .guestAddToCart({
            product_variant_id: product_variant_id,
            quantity: 1,
          })
          .subscribe((res: any) => {
            if (res.success) {
              this.dataService.changeCartCount(res.cartCount);
              this.angularnotifierService.showNotification(
                "default",
                "Product Added to the Cart!"
              );
            } else {
              this.angularnotifierService.showNotification("error", res.msg);
            }
          });
        return;
      } else {
        this.shopService.addProductToCart(this.cartdata).subscribe({
          next: (res: any) => {
            if (res.success) {
              this.dataService.changeCartCount(res.cartCount);
              this.angularnotifierService.showNotification(
                "default",
                "Product Added to the cart"
              );
            }
          },
          error: (error) => {
            var err = JSON.stringify(error);
            this.angularnotifierService.showNotification("error", err);
          },
        });
      }
    } else {
      this.shopService
        .guestAddToCart({
          product_variant_id: product_variant_id,
          quantity: 1,
        })
        .subscribe((res: any) => {
          if (res.success) {
            this.dataService.changeCartCount(res.cartCount);
            this.angularnotifierService.showNotification(
              "default",
              "Product Added to the Cart!"
            );
          } else {
            this.angularnotifierService.showNotification("error", res.msg);
          }
        });
    }
  }

  productvariant(product_variant_id) {
    localStorage.setItem("product_detail_id", product_variant_id);
    if (this.productItem.stock <= 0) {
      this.shopService
        .nextVariantNavigation({ product_id: this.productItem.product_id })
        .subscribe((res: any) => {
          if (res.success) {
            const url = "/products/" + res.data.slug + "/" + res.data.unique_id;
            if (this.isMobile) {
              window.open(url, "_self");
            } else {
              window.open(url, "_blank");
            }
          } else {
            const url =
              "/products/" +
              this.productItem.slug +
              "/" +
              this.productItem.unique_id;
            if (this.isMobile) {
              window.open(url, "_self");
            } else {
              window.open(url, "_blank");
            }
          }
        });
    } else {
      const url =
        "/products/" + this.productItem.slug + "/" + this.productItem.unique_id;
      if (this.isMobile) {
        window.open(url, "_self");
      } else {
        window.open(url, "_blank");
      }
    }
    // this.shopService.ClickedProductdetails(id);
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }
}

import { Component, HostListener, OnInit } from "@angular/core";
import { OwlOptions } from "ngx-owl-carousel-o";
import { Location } from "@angular/common";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ShopService } from "src/app/service/shop/shop.service";
import Swal from "sweetalert2";
import { environment } from "src/environments/environment";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/service/data/data.service";

@Component({
  selector: "app-shoppingcart",
  templateUrl: "./shoppingcart.component.html",
  styleUrls: ["./shoppingcart.component.scss"],
})
export class ShoppingcartComponent implements OnInit {
  fixed: boolean = false;
  public headername: string = "Shopping Cart";
  qty: any;
  url: any;
  images = [];
  searchText: any;
  qtyIncrement: Boolean = false;
  qtyDecrement: Boolean = false;
  totalMRP = 0;
  totalSellingPrice = 0;
  tax = 0;
  appliedCoupon = null;
  orderTotal = 0;
  quantity: any = [];
  loading: Boolean = true;
  couponDiscount: any = 0;
  products = [];
  allCoupons: any;
  handlingCharges;
  couponLoad: boolean = true;
  showDealsExpiredMsg: Boolean = false;
  loggedIn: Boolean = false;
  cod_min_amount: any = "";
  convenience_fee: any;
  mrpDiscount: any = 0;
  stockAvailablity: boolean = true;
  constructor(
    private _location: Location,
    public modalService: NgbModal,
    public shopService: ShopService,
    private angularnotifierService: AngularnotifierService,
    public authService: AuthenticationService,
    public router: Router,
    public route: ActivatedRoute,
    public dataService: DataService
  ) {}
  @HostListener("window:scroll", ["$event"])
  onWindowScrollSummary(e) {
    if (window.pageYOffset > 50) {
      this.fixed = true;
    } else {
      this.fixed = false;
    }
  }
  ngOnInit() {
    if (window.location.href.indexOf("https://fabpik.in/") > -1) {
      this.loadCartScript();
    }
    var values = JSON.parse(localStorage.getItem("customer"));
    if (values) {
      if (this.authService.tokenExpired(values.access_token)) {
        this.loggedIn = false;
        this.getGuestCart();
      } else {
        this.loggedIn = true;
        this.shopService.getCart().subscribe((res: any) => {
          if(res.success){
            this.url = res.finalCart.imagePrefixUrl + "/";
            // var groupedCart = [];
            // var sellerIds = [];
  
            res.finalCart.groupedCart.forEach((cartItem, i) => {
              this.quantity.push([]);
              // var totalSum = 0;
              // var groupItems = [];
              // res.finalCart.cart.forEach((product) => {
              //   if (
              //     cartItem.seller_id == product.seller_id &&
              //     product.min_order_amount != null &&
              //     product.is_deal == "n" &&
              //     sellerIds.indexOf(product.seller_id) == -1
              //   ) {
              //     totalSum += product.seller_original_price;
              //     groupItems.push(product);
              //   }
              // });
              // if (totalSum < cartItem.min_order_amount && groupItems.length > 0) {
              //   var data = {
              //     groupDesign: true,
              //     convenience_fee: cartItem.convenience_fee,
              //     seller_name: cartItem.company_name,
              //     products: groupItems,
              //   };
              //   groupedCart.push(data);
              // } else {
              //   var data = {
              //     groupDesign: false,
              //     convenience_fee: null,
              //     seller_name: null,
              //     products: [cartItem],
              //   };
              //   groupedCart.push(data);
              // }
              // sellerIds.push(cartItem.seller_id);
              cartItem.products.forEach((product) => {
                const quantityObj = {
                  product_variant_id: product.product_variant_id,
                  quantity: product.quantity,
                };
                this.quantity[i].push(quantityObj);
              });
            });
            this.convenience_fee = res.finalCart.convenience_fee;
            this.showDealsExpiredMsg = res.finalCart.showDealsExpiredMsg;
            this.totalMRP = res.finalCart.totalMrp;
            this.cod_min_amount = res.finalCart.cod_min_amount;
            this.stockAvailablity = res.finalCart.stockAvailablity;
            if (!res.finalCart.stockAvailablity) {
              Swal.fire({
                title: "Stock Not Available!",
                text: "Few products in the cart are out of stock, please remove those products and proceed to checkout.",
                icon: "error",
                confirmButtonColor: "#e42b5abf",
                confirmButtonText: "Okay",
                showClass: {
                  popup: "swal-wish-wide animated fadeInDown faster",
                },
              });
            }
            this.couponDiscount = res.finalCart.couponDiscount;
            this.mrpDiscount = res.finalCart.mrpDiscount;
            this.totalSellingPrice = res.finalCart.totalSellingPrice;
            if (res.finalCart.coupon) {
              this.appliedCoupon = res.finalCart.coupon;
            }
            this.orderTotal = res.finalCart.totalOrderAmount;
            this.handlingCharges = res.finalCart.handling_charges;
            // this.products = res.finalCart.cart;
            this.products = res.finalCart.groupedCart;
          }else{
            this.angularnotifierService.showNotification(
              "error",
              res.msg
            );
          }
          this.getCoupons();
          // var wishlist = params.get("wishlist");
          var wishlist = localStorage.getItem("cartwishlist");
          if (wishlist != null) {
            var wishlistParsed;
            if ((wishlistParsed = JSON.parse(wishlist))) {
              this.moveToWishList(wishlistParsed[1], wishlistParsed[0]);
            }
            localStorage.removeItem("cartwishlist");
          }
        });
      }
    } else {
      this.loggedIn = false;
      this.getGuestCart();
    }
  }

  getGuestCart() {
    this.shopService.getGuestCart().subscribe((res: any) => {
      if (res.success) {
        if(res.finalCart.cart.length == 0){
          this.loading = false
          return;
        }
        res.finalCart.cart.forEach((cartItem, i) => {
          this.quantity.push([]);
          cartItem.products.forEach((product) => {
            const quantityObj = {
              product_variant_id: product.product_variant_id,
              quantity: product.quantity,
            };
            this.quantity[i].push(quantityObj);
          });
        });
        this.totalMRP = res.finalCart.totalMrp;
        this.totalSellingPrice = res.finalCart.totalSellingPrice;
        if (res.finalCart.coupon) {
          this.appliedCoupon = res.finalCart.coupon;
        }
        this.orderTotal = res.finalCart.totalOrderAmount;
        this.handlingCharges = res.finalCart.handling_charges;
        this.mrpDiscount = res.finalCart.mrpDiscount;
        this.convenience_fee = res.finalCart.convenience_fee;
        this.products = res.finalCart.cart;
        this.loading = false;
        this.getCoupons();
      } else {
          this.angularnotifierService.showNotification(
            "error",
            res.msg
          );
        this.loading = false;
      }
    });
  }

  getCoupons() {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        this.shopService.guestCoupons().subscribe((res: any) => {
          if(res.success){
            this.allCoupons = res.coupons;
            this.couponLoad = false;
          }else{
            this.angularnotifierService.showNotification(
              "error",
              res.msg
            );
          }
          this.loading = false;
        });
      } else {
        this.shopService.getCoupons().subscribe((res: any) => {
          if(res.success){
            this.allCoupons = res.coupons;
            this.couponLoad = false;
            this.loading = false;
            var couponCode = localStorage.getItem("code");
            if (couponCode != null) {
              var couponFound = 0;
              var applyCoupon;
              this.allCoupons.forEach((coupon) => {
                if (coupon.code == couponCode) {
                  couponFound = 1;
                  applyCoupon = coupon;
                }
              });
              if (couponFound == 1) {
                if (this.totalSellingPrice > applyCoupon.min_cart_amount) {
                  this.applyCoupon(applyCoupon);
                } else {
                  this.angularnotifierService.showNotification(
                    "error",
                    `SubTotal should be greater than ${applyCoupon.min_cart_amount}!`
                  );
                }
              } else {
                this.angularnotifierService.showNotification(
                  "error",
                  "Invalid Coupon! Coupon Not Found!"
                );
              }
              localStorage.removeItem("code");
            }
          }else{
            this.angularnotifierService.showNotification(
              "error",
              res.msg
            );
          }
        });
      }
    } else {
      this.shopService.guestCoupons().subscribe((res: any) => {
        if(res.success){
          this.allCoupons = res.coupons;
          this.couponLoad = false;
        }else{
          this.angularnotifierService.showNotification(
            "error",
            res.msg
          );
        }
        this.loading = false;
      });
    }
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView({ behavior: "smooth" });
  }
  loadCartScript() {
    let node = document.createElement("script"); // creates the script tag
    node.text = `fbq('track', 'AddToCart')`;
    // append to head of document
    document.getElementsByTagName("head")[0].appendChild(node);
  }

  openCoupons(coupons) {
    this.modalService.open(coupons, {
      centered: true,
      size: "md",
      animation: true,
      windowClass: "coupons-modal dark1-modal",
      backdrop: "static",
      keyboard: false,
    });
  }

  close() {
    this.searchText = "";
    this.modalService.dismissAll();
  }

  applyCoupon(coupon) {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        var urlWithApplyCoupon = this.router.url;
        localStorage.setItem("url", urlWithApplyCoupon);
        localStorage.setItem("code", coupon.code);
        this.close();
        this.router.navigate(["login"], {
          queryParams: {
            q: urlWithApplyCoupon,
          },
        });
      } else {
        if (this.totalSellingPrice > coupon.min_cart_amount) {
          this.loading = true;
          this.shopService.applyCoupon(coupon).subscribe((res: any) => {

            if (res.success) {
              this.appliedCoupon = coupon;
              this.products = res.finalCart.groupedCart;
              this.totalSellingPrice = res.finalCart.totalSellingPrice;
              this.totalMRP = res.finalCart.totalMrp;
              this.orderTotal = res.finalCart.totalOrderAmount;
              this.convenience_fee = res.finalCart.convenience_fee;
              this.handlingCharges = res.finalCart.handling_charges;
              this.couponDiscount = res.finalCart.couponDiscount;
              this.mrpDiscount = res.finalCart.mrpDiscount;
              this.quantity = [];
                res.finalCart.groupedCart.forEach((cartItem, i) => {
                  this.quantity.push([]);
                  cartItem.products.forEach((product) => {
                    const quantityObj = {
                      product_variant_id: product.product_variant_id,
                      quantity: product.quantity,
                    };
                    this.quantity[i].push(quantityObj);
                  });
                });
              // this.alertService.success("Success!", "Coupon Applied Successfully!");
              this.angularnotifierService.showNotification(
                "default",
                "Coupon Applied Successfully!"
              );
              this.close();
              
            } else {
              // this.alertService.warning("Coupon Error!", res.msg)
              this.angularnotifierService.showNotification("error", res.msg);
            }
            this.loading = false
          });
        } else {
          this.loading = true;
          this.shopService.applyCoupon(coupon).subscribe((res: any) => {
            if (res.succcess) {
              this.appliedCoupon = coupon;
              this.products = res.finalCart.groupedCart;
              this.totalSellingPrice = res.finalCart.totalSellingPrice;
              this.totalMRP = res.finalCart.totalMrp;
              this.orderTotal = res.finalCart.totalOrderAmount;
              this.convenience_fee = res.finalCart.convenience_fee;
              this.handlingCharges = res.finalCart.handling_charges;
              this.couponDiscount = res.finalCart.couponDiscount;
              this.mrpDiscount = res.finalCart.mrpDiscount;
              this.quantity = [];
                res.finalCart.groupedCart.forEach((cartItem, i) => {
                  this.quantity.push([]);
                  cartItem.products.forEach((product) => {
                    const quantityObj = {
                      product_variant_id: product.product_variant_id,
                      quantity: product.quantity,
                    };
                    this.quantity[i].push(quantityObj);
                  });
                });
              // this.alertService.success("Success!", "Coupon Applied Successfully!");
              this.angularnotifierService.showNotification(
                "default",
                "Coupon Applied Successfully!"
              );
              this.close();
            } else {
              // this.alertService.warning("Coupon Error!", res.msg)
              this.angularnotifierService.showNotification("error", res.msg);
            }
            this.loading = false;
          });
        }
      }
    } else {
      // var urlWithApplyCoupon = this.router.url + `?applyCoupon=${coupon.code}`;
      var urlWithApplyCoupon = this.router.url;
      localStorage.setItem("url", urlWithApplyCoupon);
      localStorage.setItem("code", coupon.code);
      this.close();
      this.router.navigate(["login"], {
        queryParams: {
          q: urlWithApplyCoupon,
        },
      });
    }
  }

  applyCouponFromSearch(searchText) {
    if (searchText == null) {
      return;
    }
    var couponCode = searchText.toUpperCase();
    var couponFound = 0;
    var applyCoupon;
    this.allCoupons.forEach((coupon) => {
      if (coupon.code == couponCode) {
        couponFound = 1;
        applyCoupon = coupon;
      }
    });
    if (couponFound == 1) {
      if (this.totalSellingPrice > applyCoupon.min_cart_amount) {
        this.applyCoupon(applyCoupon);
      } else {
        this.angularnotifierService.showNotification(
          "error",
          `SubTotal should be greater than ${applyCoupon.min_cart_amount}!`
        );
      }
    } else {
      // this.alertService.warning("Invalid Coupon!", "Coupon Not Found!")
      this.angularnotifierService.showNotification(
        "error",
        "Invalid Coupon! Coupon Not Found!"
      );
    }
  }

  removeCoupon() {
    this.loading = true;
    this.shopService.removeCoupon().subscribe((res: any) => {
      if (res.cart) {
        this.appliedCoupon = null;
        this.couponDiscount = 0;
        this.products = res.groupedCart;
        this.totalMRP = res.totalMrp;
        this.totalSellingPrice = res.totalSellingPrice;
        this.orderTotal = res.totalOrderAmount;
        this.handlingCharges = res.handling_charges;
        this.convenience_fee = res.convenience_fee;
        this.mrpDiscount = res.mrpDiscount;
        this.quantity = [];
        res.groupedCart.forEach((cartItem, i) => {
          this.quantity.push([]);
          cartItem.products.forEach((product) => {
            const quantityObj = {
              product_variant_id: product.product_variant_id,
              quantity: product.quantity,
            };
            this.quantity[i].push(quantityObj);
          });
        });
      }
      this.loading = false;
    });
  }

  @HostListener("window:scroll", ["$event"])
  onWindowScroll(e) {
    if (window.pageYOffset > 100) {
      this.fixed = true;
    } else {
      this.fixed = false;
    }
  }

  increment(quantity, product_variant_id, i, j) {
    this.loading = true;
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        this.qtyIncrement = true;
        quantity++;
        this.shopService
          .guestAddToCart({
            product_variant_id,
            quantity: 1,
            total_quantity: quantity,
          })
          .subscribe((res: any) => {
            if (res.success) {
              this.qtyIncrement = false;
              this.getCoupons();
              this.products = res.cart.cart;
              this.totalSellingPrice = res.cart.totalSellingPrice;
              this.totalMRP = res.cart.totalMrp;
              this.orderTotal = res.cart.totalOrderAmount;
              this.mrpDiscount = res.cart.mrpDiscount;
              this.convenience_fee = res.cart.convenience_fee;
              this.handlingCharges = res.cart.handling_charges;
              this.quantity = [];
              res.cart.cart.forEach((cartItem, i) => {
                this.quantity.push([]);
                cartItem.products.forEach((product) => {
                  const quantityObj = {
                    product_variant_id: product.product_variant_id,
                    quantity: product.quantity,
                  };
                  this.quantity[i].push(quantityObj);
                });
              });
            } else {
              this.qtyIncrement = false;
              this.angularnotifierService.showNotification("error", res.msg);
            }
            this.loading = false;
          });
      } else {
        this.qtyIncrement = true;
        quantity++;
        const postdata = {
          product_variant_id: product_variant_id,
          quantity: quantity,
        };
        this.shopService.updateCart(postdata).subscribe((res: any) => {
          if(res.success){
              this.qtyIncrement = false;
              this.getCoupons();
              this.products = res.finalCart.groupedCart;
              this.totalSellingPrice = res.finalCart.totalSellingPrice;
              this.totalMRP = res.finalCart.totalMrp;
              this.orderTotal = res.finalCart.totalOrderAmount;
              this.couponDiscount = res.finalCart.couponDiscount;
              this.convenience_fee = res.finalCart.convenience_fee;
              this.mrpDiscount = res.finalCart.mrpDiscount;
              this.handlingCharges = res.finalCart.handling_charges;
              this.quantity = [];
              res.finalCart.groupedCart.forEach((cartItem, i) => {
                this.quantity.push([]);
                cartItem.products.forEach((product) => {
                  const quantityObj = {
                    product_variant_id: product.product_variant_id,
                    quantity: product.quantity,
                  };
                  this.quantity[i].push(quantityObj);
                });
              });
          } else {
            this.angularnotifierService.showNotification("error", res.msg);
          }
          this.loading = false;
          this.qtyIncrement = false;
        });
      }
    } else {
      this.qtyIncrement = true;
      quantity++;
      this.shopService
        .guestAddToCart({
          product_variant_id,
          quantity: 1,
          total_quantity: quantity,
        })
        .subscribe((res: any) => {
          if (res.success) {
            this.qtyIncrement = false;
            this.getCoupons();
            this.products = res.cart.cart;
            this.totalSellingPrice = res.cart.totalSellingPrice;
            this.totalMRP = res.cart.totalMrp;
            this.orderTotal = res.cart.totalOrderAmount;
            this.mrpDiscount = res.cart.mrpDiscount;
            this.handlingCharges = res.cart.handling_charges;
            this.convenience_fee = res.cart.convenience_fee;
            this.quantity = [];
            res.cart.cart.forEach((cartItem, i) => {
              this.quantity.push([]);
              cartItem.products.forEach((product) => {
                const quantityObj = {
                  product_variant_id: product.product_variant_id,
                  quantity: product.quantity,
                };
                this.quantity[i].push(quantityObj);
              });
            });
          } else {
            this.qtyIncrement = false;
            this.angularnotifierService.showNotification("error", res.msg);
          }
          this.loading = false;
        });
    }
  }

  decrement(quantity, product_variant_id, i, j) {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        if (quantity != 1) {
          this.loading = true;
          this.qtyDecrement = true;
          quantity--;

          this.shopService
            .guestAddToCart({
              product_variant_id,
              quantity: -1,
              total_quantity: quantity,
            })
            .subscribe((res: any) => {
              this.qtyDecrement = false;
              if (res.success) {
                this.getCoupons();
                this.products = res.cart.cart;
                this.totalSellingPrice = res.cart.totalSellingPrice;
                this.totalMRP = res.cart.totalMrp;
                this.orderTotal = res.cart.totalOrderAmount;
                this.mrpDiscount = res.cart.mrpDiscount;
                this.convenience_fee = res.cart.convenience_fee;
                this.handlingCharges = res.cart.handling_charges;
                this.quantity = [];
                res.cart.cart.forEach((cartItem, i) => {
                  this.quantity.push([]);
                  cartItem.products.forEach((product) => {
                    const quantityObj = {
                      product_variant_id: product.product_variant_id,
                      quantity: product.quantity,
                    };
                    this.quantity[i].push(quantityObj);
                  });
                });
              } else {
                this.angularnotifierService.showNotification("error", res.msg);
              }
              this.loading = false;
            });
        }
      } else {
        if (quantity != 1) {
          this.loading = true;
          this.qtyDecrement = true;
          quantity--;
          const postdata = {
            product_variant_id: product_variant_id,
            quantity: quantity,
          };
          this.shopService.updateCart(postdata).subscribe((res: any) => {
            if (res.success) {
              this.qtyDecrement = false;
              this.appliedCoupon = res.finalCart.coupon;
              this.getCoupons();
              this.products = res.finalCart.groupedCart;
              this.totalSellingPrice = res.finalCart.totalSellingPrice;
              this.couponDiscount = res.finalCart.couponDiscount;
              this.mrpDiscount = res.finalCart.mrpDiscount;
              this.convenience_fee = res.finalCart.convenience_fee;
              this.totalMRP = res.finalCart.totalMrp;
              this.orderTotal = res.finalCart.totalOrderAmount;
              this.handlingCharges = res.finalCart.handling_charges;
              this.stockAvailablity = res.finalCart.stockAvailablity
              this.quantity = [];
              res.finalCart.groupedCart.forEach((cartItem, i) => {
                this.quantity.push([]);
                cartItem.products.forEach((product) => {
                  const quantityObj = {
                    product_variant_id: product.product_variant_id,
                    quantity: product.quantity,
                  };
                  this.quantity[i].push(quantityObj);
                });
              });
            }else{
              this.angularnotifierService.showNotification(
                "error",
                res.msg
              );
            }
            this.qtyDecrement = false;
            this.loading = false;
          });
        }
      }
    } else {
      if (quantity != 1) {
        this.loading = true;
        quantity--;
        this.qtyDecrement = true;
        this.shopService
          .guestAddToCart({
            product_variant_id,
            quantity: -1,
            total_quantity: quantity,
          })
          .subscribe((res: any) => {
            this.qtyDecrement = false;
            if (res.success) {
              this.products = res.cart.cart;
              this.getCoupons();
              this.totalSellingPrice = res.cart.totalSellingPrice;
              this.totalMRP = res.cart.totalMrp;
              this.orderTotal = res.cart.totalOrderAmount;
              this.mrpDiscount = res.cart.mrpDiscount;
              this.convenience_fee = res.cart.convenience_fee;
              this.handlingCharges = res.cart.handling_charges;
              this.quantity = [];
              res.cart.cart.forEach((cartItem, i) => {
                this.quantity.push([]);
                cartItem.products.forEach((product) => {
                  const quantityObj = {
                    product_variant_id: product.product_variant_id,
                    quantity: product.quantity,
                  };
                  this.quantity[i].push(quantityObj);
                });
              });
            }else {
              this.angularnotifierService.showNotification("error", res.msg);
            }
          });
      }
    }
  }

  goback() {
    this._location.back();
  }

  customOptions: OwlOptions = {
    loop: true,
    autoplay: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    nav: false,
    autoHeight: true,
    navSpeed: 700,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
  };

  deleteCartItem(id) {
    Swal.fire({
      title: "Are you sure you want to delete this product from cart?",
      icon: "warning",
      showClass: {
        popup: "swal-wish-wide animated fadeInDown faster",
      },
      cancelButtonColor: "#e42b5a08",
      confirmButtonColor: "#e42b5abf",
      showCloseButton: true,
      showCancelButton: true,
    }).then((check) => {
      if (check.value) {
        if (localStorage.getItem("customer") !== null) {
          var token = JSON.parse(localStorage.getItem("customer"));
          var tokenExpiry = this.authService.tokenExpired(token.access_token);
          if (tokenExpiry) {
            this.loading = true;
            this.shopService
              .guestCartRemoveItem({ product_variant_id: id })
              .subscribe((res: any) => {
                this.loading = false;
                if (res.success) {
                  this.dataService.changeCartCount(res.cartCount);
                  this.getCoupons();
                  this.products = res.finalCart.cart;
                  this.totalMRP = res.finalCart.totalMrp;
                  this.totalSellingPrice = res.finalCart.totalSellingPrice;
                  this.orderTotal = res.finalCart.totalOrderAmount;
                  this.mrpDiscount = res.finalCart.mrpDiscount;
                  this.convenience_fee = res.finalCart.convenience_fee;
                  this.handlingCharges = res.finalCart.handling_charges;
                  this.quantity = [];
                  res.finalCart.cart.forEach((cartItem, i) => {
                    this.quantity.push([]);
                    cartItem.products.forEach((product) => {
                      const quantityObj = {
                        product_variant_id: product.product_variant_id,
                        quantity: product.quantity,
                      };
                      this.quantity[i].push(quantityObj);
                    });
                  });
                  Swal.fire({
                    title: "Yes deleted!",
                    text: "The product has been deleted from cart.",
                    icon: "success",

                    confirmButtonColor: "#e42b5abf",
                    confirmButtonText: "Okay",

                    showClass: {
                      popup: "swal-wish-wide animated fadeInDown faster",
                    },
                  });
                }else{
                  this.angularnotifierService.showNotification("error", res.msg);
                }
              });
          } else {
            this.loading = true;
            this.shopService.deleteCartProduct(id).subscribe((res: any) => {
              if (res.success) {
                this.dataService.changeCartCount(res.cartCount);
                this.getCoupons();
                this.products = res.finalCart.groupedCart;
                this.totalMRP = res.finalCart.totalMrp;
                this.totalSellingPrice = res.finalCart.totalSellingPrice;
                this.orderTotal = res.finalCart.totalOrderAmount;
                this.handlingCharges = res.finalCart.handling_charges;
                this.mrpDiscount = res.finalCart.mrpDiscount;
                this.couponDiscount = res.finalCart.couponDiscount;
                this.convenience_fee = res.finalCart.convenience_fee;
                this.appliedCoupon = res.finalCart.coupon;
                this.quantity = [];
                res.finalCart.groupedCart.forEach((cartItem, i) => {
                  this.quantity.push([]);
                  cartItem.products.forEach((product) => {
                    const quantityObj = {
                      product_variant_id: product.product_variant_id,
                      quantity: product.quantity,
                    };
                    this.quantity[i].push(quantityObj);
                  });
                });
                Swal.fire({
                  title: "Yes deleted!",
                  text: "The product has been deleted from cart.",
                  icon: "success",
                  confirmButtonColor: "#e42b5abf",
                  confirmButtonText: "Okay",
                  showClass: {
                    popup: "swal-wish-wide animated fadeInDown faster",
                  },
                });
              }
              this.loading = false;
            });
          }
        } else {
          this.loading = true;
          this.shopService
            .guestCartRemoveItem({ product_variant_id: id })
            .subscribe((res: any) => {
              this.loading = false;
              if (res.success) {
                this.dataService.changeCartCount(res.cartCount);
                this.getCoupons();
                this.products = res.finalCart.cart;
                this.totalMRP = res.finalCart.totalMrp;
                this.totalSellingPrice = res.finalCart.totalSellingPrice;
                this.orderTotal = res.finalCart.totalOrderAmount;
                this.handlingCharges = res.finalCart.handling_charges;
                this.quantity = [];
                res.finalCart.cart.forEach((cartItem, i) => {
                  this.quantity.push([]);
                  cartItem.products.forEach((product) => {
                    const quantityObj = {
                      product_variant_id: product.product_variant_id,
                      quantity: product.quantity,
                    };
                    this.quantity[i].push(quantityObj);
                  });
                });
                Swal.fire({
                  title: "Yes deleted!",
                  text: "The product has been deleted from cart.",
                  icon: "success",

                  confirmButtonColor: "#e42b5abf",
                  confirmButtonText: "Okay",

                  showClass: {
                    popup: "swal-wish-wide animated fadeInDown faster",
                  },
                });
              }else{
                this.angularnotifierService.showNotification("error", res.msg);
              }
            });
        }
      } else if (check.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }

  moveToWishList(product_id, product_variant_id) {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        const urlWithWishlist =
          this.router.url + `?wishlist=[${product_variant_id},${product_id}]`;
        localStorage.setItem("url", urlWithWishlist);
        localStorage.setItem(
          "cartwishlist",
          JSON.stringify([product_variant_id, product_id])
        );
        this.router.navigate(["login"], {
          queryParams: {
            q: urlWithWishlist,
          },
        });
      } else {
        this.loading =  true;
        this.shopService
          .addToWishlist(product_variant_id, product_id)
          .subscribe((res: any) => {
            if (res.success) {
              this.dataService.changeWishlishtCount(res.wishlistCount);
              
              this.shopService
                .deleteCartProduct(product_variant_id)
                .subscribe((res: any) => {
                  if (res.success) {
                    this.dataService.changeCartCount(res.cartCount);
                    Swal.fire({
                      title: "Success!",
                      text: "Product added to wishlist!",
                      icon: "success",

                      confirmButtonColor: "#e42b5abf",
                      confirmButtonText: "Okay",

                      showClass: {
                        popup: "swal-wish-wide animated fadeInDown faster",
                      },
                    })
                    this.dataService.changeCartCount(res.cartCount);
                    this.getCoupons();
                    this.products = res.finalCart.groupedCart;
                    this.totalMRP = res.finalCart.totalMrp;
                    this.totalSellingPrice = res.finalCart.totalSellingPrice;
                    this.orderTotal = res.finalCart.totalOrderAmount;
                    this.handlingCharges = res.finalCart.handling_charges;
                    this.mrpDiscount = res.finalCart.mrpDiscount;
                    this.couponDiscount = res.finalCart.couponDiscount;
                    this.convenience_fee = res.finalCart.convenience_fee;
                    this.appliedCoupon = res.finalCart.coupon;
                    this.quantity = [];
                    res.finalCart.groupedCart.forEach((cartItem, i) => {
                      this.quantity.push([]);
                      cartItem.products.forEach((product) => {
                        const quantityObj = {
                          product_variant_id: product.product_variant_id,
                          quantity: product.quantity,
                        };
                        this.quantity[i].push(quantityObj);
                      });
                    });
                }
                this.loading = false;
                });
            }
          });
      }
    } else {
      const urlWithWishlist =
        this.router.url + `?wishlist=[${product_variant_id},${product_id}]`;
      localStorage.setItem("url", urlWithWishlist);
      localStorage.setItem(
        "cartwishlist",
        JSON.stringify([product_variant_id, product_id])
      );
      this.router.navigate(["login"], {
        queryParams: {
          q: urlWithWishlist,
        },
      });
    }
  }

  placeOrder() {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        localStorage.setItem("url", "/shop/checkout");
        this.router.navigate(["/login"], {
          queryParams: {
            q: "/shop/checkout",
          },
        });
      } else {
        this.router.navigate(["/shop/checkout"]);
      }
    } else {
      localStorage.setItem("url", "/shop/checkout");
      this.router.navigate(["/login"], {
        queryParams: {
          q: "/shop/checkout",
        },
      });
    }
  }

  addMoreFromWishlist() {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        localStorage.setItem("url", "/wishlist");
        this.router.navigate(["login"], {
          queryParams: {
            q: "/wishlist",
          },
        });
      } else {
        this.router.navigate(["wishlist"]);
      }
    } else {
      localStorage.setItem("url", "/wishlist");
      this.router.navigate(["login"], {
        queryParams: {
          q: "/wishlist",
        },
      });
    }
  }

  onImgError(e) {
    e.target.src = "/assets/images/no-image.jpg";
  }
}

import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  isDevMode,
  NgZone,
} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { OwlOptions } from "ngx-owl-carousel-o";
import { Location } from "@angular/common";
import { ShopService } from "src/app/service/shop/shop.service";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import Swal from "sweetalert2";
declare var $: any;
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";

declare var Razorpay: any;

@Component({
  selector: "app-payments",
  templateUrl: "./payments.component.html",
  styleUrls: ["./payments.component.scss"],
})
export class PaymentsComponent implements OnInit {
  @ViewChild("gPayBtn") gPayBtn: ElementRef;
  public headername: string = "Payment";
  buttonText = "Place Order";
  cod_charge = 0;
  submitted = false;
  cod = 0;
  color = "red";
  captchaimg = "captcha-bg4.png";
  totalMrp;
  totalOrderAmount;
  paymentType = null;
  paymentLoading: Boolean = false;
  totalSellingPrice;
  handling_charges;
  user;
  codMessage: any =
    "COD is available only for oders above INR 499 (Final cart amount)";
  // otpverifyform: FormGroup;
  loading: Boolean = true;
  btnLoading = false;
  coupon: any;
  resendloader: boolean = false;
  resendloader1: boolean = true;
  url: string;
  env: string;
  cod_enable: any = null;
  randomNumber: any;
  codEnabled: Boolean = false;
  captchaSubmit: any = null;
  finalCart: any;
  cod_min_amount: any = 500;
  mrpDiscount: any = 0;
  couponDiscount: any = 0;
  loadingPaymentOption: boolean = false;
  convenience_fee: any = 0;

  constructor(
    public modalService: NgbModal,
    private _location: Location,
    public shopService: ShopService,
    private router: Router,
    private angularnotifierService: AngularnotifierService,
    private ngZone: NgZone
  ) {
    // this.otpverifyform = this.formBuilder.group({
    //   otp1: ["", [Validators.required, Validators.minLength(1)]],
    //   otp2: ["", [Validators.required, Validators.minLength(1)]],
    //   otp3: ["", [Validators.required, Validators.minLength(1)]],
    //   otp4: ["", [Validators.required, Validators.minLength(1)]],
    // });
  }
  customOptions: OwlOptions = {
    loop: true,
    autoplay: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    nav: false,
    autoHeight: true,
    navSpeed: 700,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
  };

  loadPaymentScript() {
    let node = document.createElement("script"); // creates the script tag
    node.text = `fbq('track', 'AddPaymentInfo')`;
    // append to head of document
    document.getElementsByTagName("head")[0].appendChild(node);
  }

  scroll() {
    document
      .getElementById("viewdetails")
      .scrollIntoView({ behavior: "smooth" });
  }

  ngOnInit() {
    // this.captchaForm = this.formBuilder.group({
    //   number: ["", Validators.required],
    // });
    if (window.location.href.indexOf("https://fabpik.in/") > -1) {
      this.loadPaymentScript();
    }
    this.getOrderSummary();
  }

  // onLoadPaymentData = (event:Event):void =>{
  //   const eventDetail = event as CustomEvent<google.payments.api.PaymentData>
  //   console.log('load payment data ',eventDetail.detail)
  // }

  // onPaymentDataAuthorized:google.payments.api.PaymentAuthorizedHandler = (paymentData)=>{
  //   console.log('payment authorized',paymentData)
  //   return {
  //     transactionState:'SUCCESS'
  //   }
  // }

  onError = (event: ErrorEvent): void => {};

  // get f() {
  //   return this.otpverifyform.controls;
  // }

  close() {
    this.modalService.dismissAll();
  }

  getOrderSummary() {
    this.shopService.getOrderSummary().subscribe((res: any) => {
      this.close();
      if (res.success && !res.pincodeError) {
        if (res.finalCart.cart.length == 0) {
          this.router.navigate(["/"]);
        }
        this.totalMrp = res.finalCart.totalMrp;
        this.cod_enable = res.finalCart.cart.findIndex(
          (cartItem) => cartItem.cod_enable == "n"
        );
        if (!res.finalCart.stockAvailablity) {
          this.router.navigate(["/shop/shoppingcart"]);
        }
        this.finalCart = res.finalCart;
        this.totalOrderAmount = res.finalCart.totalOrderAmount;
        this.totalSellingPrice = res.finalCart.totalSellingPrice;
        this.mrpDiscount = res.finalCart.mrpDiscount;
        this.convenience_fee = res.finalCart.convenience_fee;
        this.couponDiscount = res.finalCart.couponDiscount;
        this.handling_charges = res.finalCart.handling_charges;
        this.cod_charge = res.finalCart.cod_charge;
        this.cod_min_amount = res.cod_min_amount;
        this.codMessage = `COD is available only for oders above INR ${
          res.cod_min_amount
        } (Final cart amount). Shop Rs.${(
          this.cod_min_amount - this.totalOrderAmount
        ).toFixed(0)} more to enable COD`;
        this.cod = res.finalCart.cod;
        this.coupon = res.finalCart.coupon;
        this.user = res.user;
        this.loading = false;
        if (this.paymentType == "cod") {
          // this.totalSellingPrice += this.cod_charge;
          // this.totalOrderAmount =
          //   this.totalOrderAmount - this.handling_charges + this.cod_charge;
          // this.handling_charges = this.cod_charge;
          this.totalOrderAmount += this.cod_charge;
        }
      } else if (res.pincodeError) {
        this.router.navigate(["/shop/checkout"]);
      }
    });
  }
  goback() {
    this._location.back();
  }

  openOtpverify(otpverify: any) {}

  verifyOtpForCod() {
    // const otpdata = {
    //   otp:
    //     this.otpverifyform.value.otp1 +
    //     this.otpverifyform.value.otp2 +
    //     this.otpverifyform.value.otp3 +
    //     this.otpverifyform.value.otp4,
    // };
    // this.shopService.verifyOtpForCod(otpdata).subscribe((res: any) => {
    //   if (res.success) {
    //     this.modalService.dismissAll();
    //     this.paymentLoading = true;
    //     // this.alertService.success('OTP Verification',"Otp Verification Success!");
    //     this.angularnotifierService.showNotification(
    //       "default",
    //       "Otp Verification Success!"
    //     );
    //     this.shopService
    //       .createOrder({ payment_type: "COD" })
    //       .subscribe((res: any) => {
    //         this.paymentLoading = false;
    //         if (res.success) {
    //           localStorage.setItem("order_id", res.order_id);
    //           this.router.navigate(["/order-confirm"]);
    //         } else {
    //           this.angularnotifierService.showNotification("error", res.error);
    //         }
    //       });
    //   } else {
    //     // this.alertService.warning('OTP Verification',"Otp Verification Failed!");
    //     this.angularnotifierService.showNotification(
    //       "error",
    //       "Otp Verification Failed!"
    //     );
    //   }
    // });
  }

  sendOtpForCod() {
    this.resendloader = true;
    this.resendloader1 = false;
    this.shopService.sendOtpForCod().subscribe((res: any) => {
      if (res.success) {
        // this.alertService.success('OTP',"Otp Sent Successfully!");
        this.angularnotifierService.showNotification(
          "default",
          "OTP Sent to your phone."
        );
        this.resendloader = false;
        this.resendloader1 = true;
      } else {
        // this.alertService.warning('OTP Verification',"Otp Verification Failed!");
        this.angularnotifierService.showNotification(
          "error",
          "Otp Verification Failed!"
        );
        this.resendloader = true;
        this.resendloader1 = false;
      }
    });
  }
  openCoupons(paymenterror) {
    this.modalService.open(paymenterror, {
      centered: true,
      size: "sm",
      animation: true,
      windowClass: "coupons-modal dark1-modal",
      backdrop: "static",
      keyboard: false,
    });
  }

  move(backOtp, fromOtp, toOtp, keycode) {
    if (keycode == 8) {
      var length = fromOtp.length;
      var maxlength = fromOtp.getAttribute(maxlength);
      backOtp.focus();
    } else {
      var length = fromOtp.length;
      var maxlength = fromOtp.getAttribute(maxlength);
      if (length == maxlength) {
        toOtp.focus();
      }
    }
  }

  payment(Otpverify: any, CashonDelivery, loadingiocn) {
    if (!this.loadingPaymentOption) {
      if (this.paymentType == "cod") {
        if (
          this.totalOrderAmount >= this.cod_min_amount &&
          this.cod_enable == -1
        ) {
          this.codEnabled = true;
          this.paymentLoading = true;
          this.shopService.stockDeduction().subscribe((res: any) => {
            if (res.success) {
              this.shopService
                .createOrder({ payment_type: "COD" })
                .subscribe((res: any) => {
                  console.log(res)
                  this.paymentLoading = false;
                  if (res.success) {
                    localStorage.setItem("order_id", res.order_id);
                    localStorage.setItem("mode", "c");
                    this.router.navigate(["/order-confirm"]);
                  } else {
                    this.angularnotifierService.showNotification(
                      "error",
                      res.error
                    );
                  }
                });
            } else {
              this.paymentLoading = false;
              if ((res.msg = "No Stock!")) {
                Swal.fire({
                  title: "Stock Not Available!",
                  text: "Few products in the cart are out of stock, please remove those products and proceed to checkout.",
                  icon: "error",
                  confirmButtonColor: "#e42b5abf",
                  confirmButtonText: "Okay",
                  showClass: {
                    popup: "swal-wish-wide animated fadeInDown faster",
                  },
                }).then((check) => {
                  if (check.value) {
                    this.router.navigate(["/shop/shoppingcart"]);
                  } else {
                    this.openLoading(loadingiocn);
                    this.getOrderSummary();
                  }
                });
              }
            }
          });

          // this.modalService.open(Otpverify, {
          //   centered: true,
          //   size: "md",
          //   animation: true,
          //   windowClass: "bottom-modal  captcha-verify-modal dark1-modal",
          // });
        } else {
          this.modalService.open(CashonDelivery, {
            centered: true,
            size: "xs",
            animation: true,
            windowClass: "bottom-modal codpopup-div",
          });
        }
      } else if (this.paymentType == "razorpay") {
        this.paymentLoading = true;
        this.shopService
          .generateOrderId({ amount: this.totalOrderAmount, currency: "INR" })
          .subscribe((res: any) => {
            if (res.success) {
              var order_data = JSON.parse(res.orderRes);
              this.payWithRazor(order_data);
            } else {
              if (res.msg == "No Stock!") {
                Swal.fire({
                  title: "Stock Not Available!",
                  text: "Few products in the cart are out of stock, please remove those products and proceed to checkout.",
                  icon: "error",
                  confirmButtonColor: "#e42b5abf",
                  confirmButtonText: "Okay",
                  showClass: {
                    popup: "swal-wish-wide animated fadeInDown faster",
                  },
                }).then((check) => {
                  if (check.value) {
                    this.router.navigate(["/shop/shoppingcart"]);
                  } else {
                    this.openLoading(loadingiocn);
                    this.getOrderSummary();
                  }
                });
              }
              this.paymentLoading = false;
            }
          });
      } else if (this.paymentType == "gpay") {
        $(".gpay-button").trigger("click");
        // this.gPayBtn.nativeElement.click();
        // console.log(this.gPayBtn)
      } else {
        this.paymentLoading = false;
        this.angularnotifierService.showNotification(
          "error",
          "please select a payment option"
        );
      }
    }
  }

  openLoading(loadingiocn) {
    this.modalService.open(loadingiocn, {
      centered: true,
      size: "sm",
      animation: true,
      windowClass: "loading-modal",
      backdrop: "static",
      keyboard: false,
    });
  }

  reloadRandomNumber() {
    this.randomNumber = this.generateRandomNumber(100000, 999999);
    // this.captchaForm.reset();
  }

  paymentOption(type, CashonDelivery, loadingiocn) {
    if (type == "cod") {
      if (
        this.totalOrderAmount >= this.cod_min_amount &&
        this.cod_enable == -1
      ) {
        this.openLoading(loadingiocn);
        this.buttonText = "Place Order";
        this.paymentType = type;
        this.getOrderSummary();
      } else {
        this.paymentType = type;
        this.modalService.open(CashonDelivery, {
          centered: true,
          size: "xs",
          animation: true,
          windowClass: "bottom-modal codpopup-div",
        });
      }
    } else {
      this.openLoading(loadingiocn);
      this.buttonText = "Make Payment";
      this.paymentType = type;
      this.getOrderSummary();
    }
  }

  generateRandomNumber(min, max) {
    var rand = min + Math.random() * (max - min);
    rand = Math.round(rand);
    return rand.toString();
  }

  payWithRazor(order) {
    const options: any = {
      key: environment.razorpayKey,
      amount: order.amount, // amount should be in paise format to display Rs 1255 without decimal point
      currency: "INR",
      name: "Fabpik", // company name or product name
      description: "", // product description
      image: "/assets/images/logo.png", // company logo or product image
      order_id: order.id, // order_id created by you in backend
      modal: {
        // We should prevent closing of the form when esc key is pressed.
        escape: false,
      },
      prefill: {
        name: this.user.name,
        email: this.user.email,
        contact: this.user.mobile,
      },
      notes: {
        // include notes if any
        finalCart: this.finalCart,
        customer_id: this.user.ref_id,
        email: this.user.email,
        mobile: this.user.mobile,
      },
      theme: {
        color: "#e83e8c",
      },
    };
    options.handler = (response, error) => {
      options.response = response;
      if (response) {
        this.ngZone.run(() => {
        setTimeout(()=>{
          this.shopService
            .verifyPaymentSignature(response)
            .subscribe((res: any) => {
              this.ngZone.run(() => {
                this.paymentLoading = false;
                if (res.success) {
                  localStorage.setItem("order_id", res.order_id);
                  localStorage.setItem("mode", "o");
                  this.router.navigate(["/order-confirm"]);
                } else {
                  this.angularnotifierService.showNotification(
                    "error",
                    "Something Went Wrong"
                  );
                }
              });
            });
       }, 2000);
      })
      } else {
        this.ngZone.run(() => {
          this.paymentLoading = false;
          this.shopService.reStock().subscribe((res: any) => {});
          this.angularnotifierService.showNotification(
            "error",
            "Something Went Wrong"
          );
        });
      }
    };
    options.modal.ondismiss = () => {
      this.ngZone.run(() => {
        this.paymentLoading = false;
        this.shopService.reStock().subscribe((res: any) => {});
      });
      // handle the case when user closes the form while transaction is in progress
    };
    const rzp = Razorpay(options);
    rzp.open();
  }

  // get f() {
  //   return this.otpverifyform.controls;
  // }

  captchaVerify() {
    // console.log(this.captchaForm.value.number);
    // console.log(this.randomNumber);
    // return;
    // if (this.captchaForm.value.number == this.randomNumber) {
    //   this.shopService
    //     .createOrder({ payment_type: "COD" })
    //     .subscribe((res: any) => {
    //       this.paymentLoading = false;
    //       this.modalService.dismissAll();
    //       if (res.success) {
    //         localStorage.setItem("order_id", res.order_id);
    //         this.router.navigate(["/order-confirm"]);
    //       } else {
    //         this.angularnotifierService.showNotification("error", res.error);
    //       }
    //     });
    // } else {
    //   this.angularnotifierService.showNotification(
    //     "error",
    //     "Number Doesn't Match!"
    //   );
    // }
  }

  handleCaptchaSuccess(e) {
    this.captchaSubmit = e;
  }

  submitCaptcha() {
    if (this.captchaSubmit) {
      this.shopService
        .createOrder({ payment_type: "COD" })
        .subscribe((res: any) => {
          this.paymentLoading = false;
          this.modalService.dismissAll();
          if (res.success) {
            localStorage.setItem("order_id", res.order_id);
            localStorage.setItem("mode", "c");
            this.router.navigate(["/order-confirm"]);
          } else {
            this.angularnotifierService.showNotification("error", res.error);
          }
        });
    } else {
      this.angularnotifierService.showNotification(
        "error",
        "Please verify the captcha!"
      );
    }
  }
}

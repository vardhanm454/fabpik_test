import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ShopRoutingModule } from "./shop-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { ShoppingcartComponent } from "./shoppingcart/shoppingcart.component";
import { CheckoutComponent } from "./checkout/checkout.component";
import { PaymentsComponent } from "./payments/payments.component";
import { NgxSkeletonLoaderModule } from "ngx-skeleton-loader";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

@NgModule({
  declarations: [ShoppingcartComponent, CheckoutComponent, PaymentsComponent],
  imports: [
    CommonModule,
    ShopRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule.forRoot(),
    GooglePlaceModule,
  ],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ShopModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckoutComponent } from './checkout/checkout.component';
import { PaymentsComponent } from './payments/payments.component';
// import { ProductdetailsComponent } from './productdetails/productdetails.component';
// import { ProductlistComponent } from './productlist/productlist.component';
import { ShoppingcartComponent } from './shoppingcart/shoppingcart.component';

const routes: Routes =
[
  
  // { 
  //   path: 'listing/:catslug',
  //   component: ProductlistComponent
  // },
  // { 
  //   path: 'listing/:catslug/:subslug',
  //   component: ProductlistComponent
  // },
  // { 
  //   path: 'listing/:catslug/:subslug/:slug',
  //   component: ProductlistComponent
  // },
  // { 
  //   path: 'detail/:slug',
  //   component: ProductdetailsComponent
  // },
  { 
    path: 'shoppingcart',
    component: ShoppingcartComponent
  
  },
  { 
    path: 'checkout',
    component:CheckoutComponent
  
  },
  { 
    path: 'payment',
    component:PaymentsComponent
  
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }

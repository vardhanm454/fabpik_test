import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { ShopService } from "src/app/service/shop/shop.service";
import { environment } from "src/environments/environment";
import { Router } from "@angular/router";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import Swal from "sweetalert2";
import { DataService } from "src/app/service/data/data.service";
import { AddressService } from "src/app/service/address/address.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
declare var $: any;
declare const google: any;

@Component({
  selector: "app-checkout",
  templateUrl: "./checkout.component.html",
  styleUrls: ["./checkout.component.scss"],
})
export class CheckoutComponent implements OnInit {
  public headername: string = "Checkout";
  url: any;
  address: any = null;
  isAddMode: Boolean;
  loading: Boolean = true;
  addAddressForm: Boolean = false;
  AddressList: Boolean = false;
  images = [];
  cartProducts: any = [];
  totalMrp: any = 0;
  totalSellingPrice: any = 0;
  totalOrderAmount: any = 0;
  pincodeError: Boolean = false;
  coupon: any = null;
  handling_charges: any = 0;
  isUserDetailsSet: Boolean;
  addr_type: any;
  addAddressform: FormGroup;
  options = {
    componentRestrictions: {
      country: ["IN"],
    },
  };
  // products = [{cart:[],totalMrp:0,totalSellingPrice:0,totalOrderAmount:0,coupon:null,handling_charges:0}];
  Office: any;
  cod_enable: any = null;
  addressLoading: Boolean = true;
  allUserAddresses: any = [];
  states: any = [];
  mapAddress: any;
  mapState: any;
  mapcountry: any;
  street: any;
  houseNumber: any;
  submitted: boolean;
  editAddressId: any;
  cod_min_amount: any = "";
  addressButtonLoading: boolean = false;
  mrpDiscount: any = 0;
  couponDiscount: any = 0;
  convenience_fee: any = 0;
  use_my_location_button:Boolean = false;
  google_auto_suggestions:Boolean =false;
  constructor(
    private _location: Location,
    public shopService: ShopService,
    private angularnotifierService: AngularnotifierService,
    private router: Router,
    public dataService: DataService,
    private addressService: AddressService,
    private formBuilder: FormBuilder
  ) {
    $("input[type='radio']").change(function () {
      if ($(this).val() == "other") {
        $("#otherAnswer").show();
      } else {
        $("#otherAnswer").hide();
      }
    });

    const body = document.getElementsByTagName('body')[0];
    body.classList.add('checkout-page');
  }

  ngOnDestroy(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('checkout-page');
  }

  loadCheckoutScript() {
    let node = document.createElement("script"); // creates the script tag
    node.text = `fbq('track', 'InitiateCheckout')`;
    // append to head of document
    document.getElementsByTagName("head")[0].appendChild(node);
  }

  scroll2(el: any) {
    el.scrollIntoView({ behavior: "smooth" });
  }

  scroll(e) {
    document
      .getElementById("viewdetails")
      .scrollIntoView({ behavior: "smooth" });
  }

  ngOnInit() {
    if (window.location.href.indexOf("https://fabpik.in/") > -1) {
      this.loadCheckoutScript();
    }
    this.getCheckoutDetails();
    this.getStates();
    this.addAddressform = this.formBuilder.group({
      name: [
        "",
        [Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z ]+")],
      ],
      mobile: [
        "",
        [
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(10),
          Validators.maxLength(10),
        ],
      ],
      pincode: [
        "",
        [
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(6),
          Validators.maxLength(6),
        ],
      ],
      state_id: ["", Validators.required],
      address: ["", Validators.required],
      locality: ["", Validators.required],
      city: ["", Validators.required],
      addr_type: ["", Validators.required],
      addr_other: [""],
      is_default: [1],
    });
    this.addAddressform.get("addr_type").valueChanges.subscribe((val) => {
      if (val == "o") {
        this.addAddressform.controls["addr_other"].setValidators([
          Validators.required,
        ]);
        this.addAddressform.controls["addr_other"].updateValueAndValidity();
      } else {
        this.addAddressform.controls["addr_other"].clearValidators();
        this.addAddressform.controls["addr_other"].updateValueAndValidity();
      }
    });
  }

  getStates() {
    this.addressService.getstates().subscribe((res) => {
      if(res.success){
        this.states = res.states;
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
    });
  }

  getCheckoutDetails() {
    this.shopService.getCheckoutDetails().subscribe((res: any) => {
      this.url = res.finalCart.imagePrefixUrl + "/";
      if (res.success == true) {
        if (res.finalCart.cart.length == 0) {
          this.router.navigate(["/"]);
        }
        this.cod_enable = res.finalCart.cart.findIndex(
          (cartItem) => cartItem.cod_enable == "n"
        );
        var checkPincodeError = res.finalCart.cart.findIndex(
          (cartItem) => cartItem.pincodeError == true
        );
        if (checkPincodeError > -1) {
          this.pincodeError = true;
          this.angularnotifierService.showNotification(
            "error",
            "Courier Service is not available for the pincode!"
          );
        }
        if (!res.finalCart.stockAvailablity) {
          this.router.navigate(["/shop/shoppingcart"]);
        }
        this.isUserDetailsSet = res.isUserDetailsSet;
        this.cartProducts = res.finalCart.cart;
        this.use_my_location_button = res.use_my_location_button;
        this.google_auto_suggestions = res.google_auto_suggestions
        this.totalMrp = res.finalCart.totalMrp;
        this.mrpDiscount = res.finalCart.mrpDiscount;
        this.convenience_fee = res.finalCart.convenience_fee;
        this.couponDiscount = res.finalCart.couponDiscount;
        this.totalSellingPrice = res.finalCart.totalSellingPrice;
        this.totalOrderAmount = res.finalCart.totalOrderAmount;
        this.coupon = res.finalCart.coupon;
        this.handling_charges = res.finalCart.handling_charges;
        this.address = res.address;
        this.cod_min_amount = res.cod_min_amount;
        this.loading = false;
      } else if (res.success == 2) {
        this.isUserDetailsSet = res.isUserDetailsSet;
        if (res.errType == "addr") {
          this.address = res.address;
        } else if (res.errType == "pincode") {
          this.pincodeError = true;
        }
        // this.alertService.warning('Pincode',res.msg);
        this.angularnotifierService.showNotification("error", res.msg);
        // }
        this.address = res.address;
        this.cartProducts = res.finalCart.cart;
        this.totalMrp = res.finalCart.totalMrp;
        this.use_my_location_button = res.use_my_location_button;
        this.google_auto_suggestions = res.google_auto_suggestions
        this.totalSellingPrice = res.finalCart.totalSellingPrice;
        this.totalOrderAmount = res.finalCart.totalOrderAmount;
        this.coupon = res.finalCart.coupon;
        this.handling_charges = res.finalCart.handling_charges;
        this.loading = false;
      }
    });
  }

  goback() {
    this._location.back();
  }

  changeAddress() {
    // localStorage.setItem("checkoutChangeAddress", "1");
    this.isAddMode = true;
    this.addAddressForm = true;
    this.AddressList = false;
  }

  getAllAddress() {
    this.addressLoading = true;
    this.addressService.getAllAddress().subscribe((res) => {
      if(res.success){
        this.allUserAddresses = res.addresses;
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
      this.addressLoading = false;
    });
  }

  addressForm() {
    this.isAddMode = true;
    this.addAddressForm = true;
    this.AddressList = false;
  }

  addressList() {
    this.getAllAddress();
    this.addAddressForm = false;
    this.AddressList = true;
  }

  checkErrors() {
    if (this.pincodeError) {
      // this.alertService.warning('Pincode',"Delivery pincode not serviceable");
      this.angularnotifierService.showNotification(
        "error",
        "Delivery pincode not serviceable"
      );
    } else if (!this.isUserDetailsSet) {
      // this.alertService.warning('Profile',"Please Update Your Profile To Place An Order!");
      this.angularnotifierService.showNotification(
        "error",
        "Please Update Your Profile To Place An Order!"
      );
    } else if (this.address == null) {
      this.angularnotifierService.showNotification(
        "error",
        "Add ADDRESS to continue further!"
      );
    }
  }

  deleteCartProduct(id) {
    this.shopService.deleteCartProduct(id).subscribe((res: any) => {
      if (res.success) {
        this.dataService.changeCartCount(res.cartCount);
        if (res.cartCount == 0) {
          this.router.navigate(["/shop/shoppingcart"]);
        } else {
          this.getCheckoutDetails();
        }
      }
    });
  }

  removeAddress() {}

  isdefault(value, id) {
    this.addressService
      .updatedefaultAddress(id, {
        is_default: value,
      })
      .subscribe({
        next: (res) => {
          // this.alertService.success('Address updated as default', 'success');
          if(res.success){
            this.angularnotifierService.showNotification(
              "default",
              "Address updated as default"
            );
            var checkoutChangeAddress = localStorage.getItem(
              "checkoutChangeAddress"
            );
            if (checkoutChangeAddress == "1") {
              localStorage.removeItem("checkoutChangeAddress");
              this.router.navigate(["/shop/checkout"]);
              return;
            }
  
            this.getCheckoutDetails();
            this.addAddressForm = false;
            this.AddressList = false;
          }else{
            this.angularnotifierService.showNotification(
              "default",
              res.msg
            );
          }
        },
        error: (error) => {
          var err = JSON.stringify(error.error.error);
          // this.alertService.error(err, 'error');
          this.angularnotifierService.showNotification("error", err);
          this.angularnotifierService.showNotification("error", err);
        },
      });
  }

  get f() {
    return this.addAddressform.controls;
  }

  onSubmit(e) {
    e.preventDefault();
    this.submitted = true;
    if (this.addAddressform.invalid) {
      return;
    }
    this.addressButtonLoading = true;
    if (this.isAddMode) {
      this.addAddress();
    } else {
      this.updateAddress();
    }
  }
  private updateAddress() {
    this.addressService
      .update(this.editAddressId, this.addAddressform.value)
      .subscribe({
        next: (res) => {
          this.addressButtonLoading = false;
          // this.alertService.success('Update Successfully' ,'success');
          this.angularnotifierService.showNotification(
            "default",
            "Updated Successfully"
          );
          // this.router.navigate(["/myaccount/addresses"]);
          this.getCheckoutDetails();
          this.addAddressForm = false;
          this.AddressList = false;
        },
        error: (error) => {
          var err = JSON.stringify(error);
          //  this.alertService.error(err ,'error');
          this.angularnotifierService.showNotification("error", err);
          this.addressButtonLoading = false;
        },
      });
  }

  private addAddress() {
    this.addressService.addAddress(this.addAddressform.value).subscribe({
      next: (res) => {
        if (res.success) {
          this.addressButtonLoading = false;
          // this.alertService.success('Save Successfully' ,'success');
          this.angularnotifierService.showNotification(
            "default",
            "Save Successfully"
          );
          // this.router.navigate(["/myaccount/addresses"]);
          this.getCheckoutDetails();
          this.addAddressForm = false;
          this.AddressList = false;
          this.addAddressform.reset();
        } else {
          this.addressButtonLoading = false;
          // this.alertService.error(res.error,'error');
          this.angularnotifierService.showNotification("error", res.msg);
        }
      },
      error: (error) => {
        var err = JSON.stringify(error);
        // this.alertService.error(err ,'error');
        this.angularnotifierService.showNotification("error", error);
        this.addressButtonLoading = false;
      },
    });
  }

  editAddress(id) {
    this.editAddressId = id;
    this.isAddMode = false;
    this.addAddressForm = true;
    this.AddressList = false;
    this.addressService.getById(id).subscribe((res:any) => {
      if(res.success){
        this.addAddressform.patchValue(res.data)
        this.addAddressform.patchValue({ is_default: 1 });
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
    });
  }

  handleAddressChange(address: any) {
    // this.addAddressform.patchValue({
    //   locality: address.vicinity,
    // });

    var addressGenerate = "";
    for (let i = 0; i < address.address_components.length; i++) {
      this.mapAddress = address.address_components[i];
      if (this.mapAddress.long_name != "") {
        if (this.mapAddress.types[0] == "premise") {
          addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
        }
        if (this.mapAddress.types[0] == "neighborhood") {
          addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
        }
        if (this.mapAddress.types[0] == "sublocality_level_2") {
          addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
          this.addAddressform.patchValue({
            locality: this.mapAddress.long_name,
          });
        } else if (
          this.mapAddress.types[0] == "sublocality_level_1" ||
          this.mapAddress.types[2] == "sublocality_level_1"
        ) {
          addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
          this.addAddressform.patchValue({
            locality: this.mapAddress.long_name,
          });
        }
        if (this.mapAddress.types[0] == "locality") {
          addressGenerate = addressGenerate + this.mapAddress.long_name;
          this.addAddressform.patchValue({
            city: this.mapAddress.long_name,
          });
        }

        if (this.mapAddress.types[0] == "administrative_area_level_1") {
          this.mapState = this.mapAddress.long_name;
          this.states.forEach((state) => {
            if (state.name == this.mapState) {
              this.addAddressform.patchValue({ state_id: state.id });
            }
          });
        }
        if (this.mapAddress.types[0] == "country") {
          this.mapcountry = this.mapAddress.long_name;
        } else {
          this.mapcountry = "";
        }
        if (this.mapAddress.types[0] == "postal_code") {
          this.addAddressform.patchValue({
            pincode: this.mapAddress.long_name,
          });
        }
        let streets =
          this.street && this.houseNumber
            ? this.houseNumber + ", " + this.street
            : "";
        this.mapAddress = streets ? streets : this.street;
      }
    }
    this.addAddressform.patchValue({
      address: addressGenerate,
    });
  }

  viewAllAddresses() {
    this.addAddressForm = false;
    this.AddressList = true;
    this.getAllAddress();
  }

  useMyLocation() {
    navigator.geolocation.getCurrentPosition((location) => {
      let geocoder = new google.maps.Geocoder();
      let latlng = new google.maps.LatLng(
        location.coords.latitude,
        location.coords.longitude
      );
      let request = {
        latLng: latlng,
      };
      geocoder.geocode(
        request,
        (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0] != null) {
              this.handleAddressChange(results[0]);
            } else {
            }
          }
        },
        (error) => {}
      );
    });
  }

  onImgError(e) {
    e.target.src = "/assets/images/no-image.jpg";
  }
}

import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { ShopService } from 'src/app/service/shop/shop.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-seller-shop',
  templateUrl: './seller-shop.component.html',
  styleUrls: ['./seller-shop.component.scss'],
  host: {
    "(window:resize)": "onWindowResize($event)"
  }
})
export class SellerShopComponent implements OnInit {
  headerName: any = "Seller Shop"
  currentRate: any;
  currentRate5: any = 5;
  currentRate4: any = 4;
  currentRate3: any = 3;
  currentRate2: any = 2;
  isMobile: boolean = false;
  isDesktop: boolean = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth: number = 767;
  shopdetails: any;
  seller_id: any;
  featured: any;
  newcollection: any;
  seller_slug: any;
  listing: any;
  public products = [
    {
      image: "assets/images/products/1.jpg",
      name: "Baby Sleepsuit",
      storename: "storename",
      saleprice: "2999",
      originalprice: "3999",
      offerpercentage: "20%"
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Cute Llama printed kimono style onesie",
      storename: "storename",
      saleprice: "2999",
      originalprice: "3999",
      offerpercentage: "20%"
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Baby Sleepsuit",
      saleprice: "2999",
      storename: "storename",
      originalprice: "3999",
      offerpercentage: "20%"
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Cute Llama printed kimono style onesie",
      storename: "storename",
      saleprice: "2999",
      originalprice: "3999",
      offerpercentage: "20%"
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Baby Sleepsuit",
      saleprice: "2999",
      storename: "storename",
      originalprice: "3999",
      offerpercentage: "20%"
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Cute Llama printed kimono style onesie",
      storename: "storename",
      saleprice: "2999",
      originalprice: "3999",
      offerpercentage: "20%"
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Cute Llama printed kimono style onesie",
      storename: "storename",
      saleprice: "2999",
      originalprice: "3999",
      offerpercentage: "20%"
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Cute Llama printed kimono style onesie",
      storename: "storename",
      saleprice: "2999",
      originalprice: "3999",
      offerpercentage: "20%"
    },

  ]

  constructor(private shopService: ShopService, private route: ActivatedRoute) {
  }

  RecommendOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    autoplay: true,
    dots: false,
    nav: false,
    navSpeed: 700,
    margin: 5,
    navText: ['', ''],
    responsive: {
      0: {
        items: 4
      },
      400: {
        items: 4
      },
      740: {
        items: 4
      },
      940: {
        items: 6
      }
    },
  }

  ngOnInit(): void {
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
    this.seller_id = localStorage.getItem('sellerid'); 
    this.seller_slug = this.route.snapshot.params.slug; 
    //this.seller_id = 11;
    //this.shopDetails(this.seller_id);
    this.featured_NewCollectionProduct(this.seller_id, this.seller_slug);
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }
  onImgError(e){
    e.target.src = '/assets/images/no-image.jpg'
}
  // shopDetails(seller_id){
  //   this.shopService.getShopDetails(seller_id)
  //   .subscribe({
  //       next: (res: any) => {
  //           this.shopdetails = res;
  //           console.log(this.shopdetails);
  //       },
  //       error: error => {
  //           var err = JSON.stringify(error.error);
  //       }
  //   });
  // }

  featured_NewCollectionProduct(seller_id, seller_slug) {
    this.listing = {
      sellerid: seller_id,
      sellerslug: seller_slug
    }
    this.shopService.getfetured_NewCollection(this.listing)
      .subscribe({
        next: (res: any) => {
          this.shopdetails = res.seller;
          this.featured = res.featured;
          this.newcollection = res.new_collection; 
        },
        error: error => {
          var err = JSON.stringify(error.error);
        }
      });
  }
}

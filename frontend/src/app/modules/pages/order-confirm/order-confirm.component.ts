import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/service/data/data.service";
import { ShopService } from "src/app/service/shop/shop.service";

@Component({
  selector: "app-order-confirm",
  templateUrl: "./order-confirm.component.html",
  styleUrls: ["./order-confirm.component.scss"],
})
export class OrderConfirmComponent implements OnInit {
  headerName = "Order Confirm";
  orderdata: any;
  orderDetails: any;
  loading: Boolean = true;
  totalOrders: any;
  constructor(
    private router: Router,
    public shopService: ShopService,
    public dataService: DataService
  ) {}
  loadConfirmOrderScript() {
    let node = document.createElement("script"); // creates the script tag
    node.text = `gtag('event', 'conversion', { 'send_to': 'AW-591637777/NzfQCNS93Y0CEJHajpoC', 'transaction_id': '' })`;
    document.getElementsByTagName("head")[0].appendChild(node);
  }
  loadPurchaseScript() {
    let node = document.createElement("script"); // creates the script tag
    node.text = `fbq('track', 'Purchase', {value: 0.00, currency: 'USD'})`;
    // append to head of document
    document.getElementsByTagName("head")[0].appendChild(node);
  }

  ngOnInit() {
    if (window.location.href.indexOf("https://fabpik.in/") > -1) {
      this.loadPurchaseScript();
      this.loadConfirmOrderScript();
    }
    this.orderdata = {
      order_id: localStorage.getItem("order_id"),
      mode: localStorage.getItem("mode"),
    };
    if (localStorage.getItem("order_id") != null) {
      this.shopService.getOrderSuccess(this.orderdata).subscribe((res: any) => {
        this.loading = false;
        this.orderDetails = res.order;
        this.totalOrders = res.totalOrders;
        this.dataService.changeCartCount(0);
        localStorage.removeItem("order_id");
      });
    } else {
      this.router.navigate(["/"]);
    }
  }
}

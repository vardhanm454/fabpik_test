import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { OwlOptions } from "ngx-owl-carousel-o";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ActivatedRoute } from "@angular/router";
import { ShopService } from "src/app/service/shop/shop.service";
import { ReviewService } from "src/app/service/review/review.service";
import { Lightbox, LightboxConfig } from "ngx-lightbox";
import { Router } from "@angular/router";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { CommonapiService } from "src/app/service/commonapi/commonapi.service";
import { SlickCarouselComponent } from "ngx-slick-carousel";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { Title, Meta } from "@angular/platform-browser";
import { DataService } from "src/app/service/data/data.service";
import { ProgressSpinnerMode } from "@angular/material";

declare var $: any;
@Component({
  selector: "app-productdetails",
  templateUrl: "./productdetails.component.html",
  styleUrls: ["./productdetails.component.scss"],
  host: {
    "(window:resize)": "onWindowResize($event)",
  },
})
export class ProductdetailsComponent implements OnInit {
  serverSizeChartUrl: any;
  mode: ProgressSpinnerMode = "determinate";
  isMobile: boolean = false;
  isDesktop: boolean = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth: number = 767;
  fixed: boolean;
  enableZoom: Boolean = true;
  previewImageSrc: any;
  zoomImageSrc: any;
  addedClass: Boolean = false;
  url: any;
  coupons: any = [];
  estimatedDelivery: any;
  flashDeals: any = [];
  headerName: any;
  isSubmit: Boolean;
  loading: Boolean = true;
  userNotified: Boolean = null;
  pincodeLoading: Boolean;
  productDetails: any;
  pincodeForm: FormGroup;
  pincode: any;
  addedtitle = "Success";
  quantity = 1;
  colorrating: any = 1;
  selectedItem2: any;
  pageTitle: any = "";
  apisCalls: any = 0;
  productmoreclass: boolean = false;
  productlessclass: boolean = false;
  selectedItem: any = null;
  selectedProductVariant: any = {};
  imgags = [];
  offsetY = 0;
  offsetX = 0;
  offerOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    autoplay: true,
    dots: true,
    nav: false,
    navSpeed: 700,
    margin: 10,
    autoHeight: true,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
  };
  recommendedProducts: any = [];
  imgUrl: any;
  userNotificationEnabled: any = "n";
  userLoggedIn: boolean = false;
  currentUrl: string;
  showSelectSizeText: boolean = false;
  addToCartLoading: boolean = false;

  writeReview(review) {
    this.modalService.open(review, {
      centered: true,
      size: "md",
      animation: true,
      windowClass: "coupons-modal dark1-modal",
      backdrop: "static",
      keyboard: false,
    });
  }
  reviewFilters: any = { sortBy: "recent" };
  variantOptions: any;
  productImage: any;
  public features = [
    {
      image: "assets/images/icons/secure_payments.png",
      name: "Secured Payments",
    },
    {
      image: "assets/images/icons/safe_and_hygienic.png",
      name: "Safe And Hygienic Products",
    },
    {
      image: "assets/images/icons/quick_deliveries.png",
      name: "Super Quick Delivery",
    },
    {
      image: "assets/images/icons/best_price.png",
      name: "Best Price Challenge",
    },
  ];

  ProductOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    autoplay: false,
    autoWidth: true,
    autoHeight: true,
    dots: true,
    nav: false,
    navSpeed: 700,
    margin: 10,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 2,
      },
    },
  };
  flashDeals1: any;
  policies: any;
  thumbnailimg: any;
  pimage: any;
  product: any;
  product2: any;
  imgzoom: number;
  initialSlide: any;
  currentRate: number = 0;
  wishlistExists: Boolean = false;
  reviewForm: FormGroup;
  userReviews: any = [];
  userStats: any = [];
  userReviewLoading: Boolean = true;
  avgRating: any = 0;
  shareUrl: string;
  wasClicked = false;
  constructor(
    private http: HttpClient,
    private renderer2: Renderer2,
    public modalService: NgbModal,
    private _location: Location,
    private route: ActivatedRoute,
    private shopService: ShopService,
    private formBuilder: FormBuilder,
    private reviewService: ReviewService,
    private _lightbox: Lightbox,
    private router: Router,
    private angularnotifierService: AngularnotifierService,
    private _lightboxConfig: LightboxConfig,
    public authService: AuthenticationService,
    public commonapiService: CommonapiService,
    private metaService: Meta,
    private titleService: Title,
    public dataService: DataService
  ) {
    this.url = this.router.url;
    this._lightboxConfig.disableScrolling = true;
    this._lightboxConfig.alwaysShowNavOnTouchDevices = true;
    const baseUrl = window.location.protocol + "//" + window.location.hostname;
    const imageUrl = this.productImage;

    this.pincodeForm = this.formBuilder.group({
      pincode: [
        "",
        [Validators.required, Validators.minLength(6), Validators.maxLength(6)],
      ],
    });
    this.reviewForm = this.formBuilder.group({
      review_comment: ["", [Validators.required]],
    });
  }

  on_clcik(image) {
    this.productImage = image;
    $("#clickbtn").trigger("click");
    $("#clickbtn2").trigger("click");
    $(".pswp__img").attr("src", image);
  }

  onImgError(e) {
    e.target.src = "/assets/images/no-image.jpg";
  }
  ngOnInit() {
    // this.initialSlide=1;
    this.currentUrl =
      "https://api.whatsapp.com/send?text=" + window.location.href;
    this.shareUrl = this.router.url;
    // console.log(this.initialSlide)
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
    const postdata = {
      product_variant_slug: this.route.snapshot.paramMap.get(
        "product_variant_slug"
      ),
      unique_id: this.route.snapshot.paramMap.get("unique_id"),
    };
    this.route.params.subscribe((val) => {
      const postdata = {
        product_variant_slug: this.route.snapshot.paramMap.get(
          "product_variant_slug"
        ),
        unique_id: this.route.snapshot.paramMap.get("unique_id"),
      };
      this.getProductDetails(postdata);
    });
    // this.getProductDetails(postdata);
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        // token expired
        this.apisCalls++;

        if (this.apisCalls == 4) {
          this.loading = false;
        }
        return;
      } else {
        this.shopService.checkPincode(this.route.snapshot.paramMap.get("unique_id")).subscribe((res: any) => {
          this.apisCalls++;
          if (this.apisCalls == 4) {
            this.loading = false;
          }
          if (res.success == 2) {
            this.angularnotifierService.showNotification("error", res.msg);
            this.pincodeForm.patchValue({
              pincode: "",
            });
            this.pincodeLoading = false;
            this.isSubmit = false;
          }
          if (res != []) {
            this.pincode = res.address.pincode;
            this.estimatedDelivery = res.estimateddelivery;
            this.pincodeForm.patchValue({
              pincode: this.pincode,
            });
            this.pincodeLoading = false;
            this.isSubmit = false;
          }
        });
      }
    } else {
      this.apisCalls++;

      if (this.apisCalls == 4) {
        this.loading = false;
      }
    }
  }

  el(selector) {
    return document.querySelector(selector);
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }

  submitReview() {
    var data = this.reviewForm.value;
    data.product_id = this.productDetails.product_id;
    data.product_variant_id = this.productDetails.product_variant_id;
    this.reviewService.addreview(data).subscribe((res: any) => {});
  }

  getReviews() {
    this.userReviewLoading = true;
    this.shopService
      .getProductReviews(this.productDetails.product_id, this.reviewFilters)
      .subscribe((res: any) => {
        if(res.success){
          this.userReviewLoading = false;
          this.userReviews = res.reviews;
          this.userStats = res.stats;
          if (res.avgRating != null) {
            this.avgRating = parseFloat(res.avgRating).toFixed(1);
            this.colorrating = parseFloat(res.avgRating).toFixed(0);
          }
        }else{
          this.angularnotifierService.showNotification("error", res.msg);
        }
      });
  }

  changeReviewFilter(e) {
    this.reviewFilters.sortBy = e;
    this.getReviews();
  }

  tabChange(e) {
    if (e.tab.textLabel == "reviews") {
      this.getReviews();
    }
  }

  getProductDetails(id) {
    this.shopService.getProductDetails(id).subscribe({
      next: (res: any) => {
        this.apisCalls++;

        if (this.apisCalls == 4) {
          this.loading = false;
        }
        if (res.msg == "Product not found") {
          this.loading = false;
          this.productDetails = null;
          return;
        }
        if(res.success){
          this.imgags = res.data.product.images;
          this.selectedItem2 =
            res.data.product.images[parseInt(res.data.product.thumbnail) - 1];
          this.productImage =
            res.data.product.images[parseInt(res.data.product.thumbnail) - 1];
          this.product = this.selectedItem2;
          this.pageTitle = res.data.product.pv_name;
          this.metaService.updateTag({
            property: "og:image",
            content: this.productImage,
          });
          this.metaService.addTags([
            { name: "keywords", content: res.data.product.meta_keywords },
            { name: "description", content: res.data.product.meta_description },
          ]);
          // this.titleService.setTitle(res.data.product.pv_name)
          // this.productImage = res.data.product.images[parseInt(res.data.product.thumbnail) - 1];
          this.headerName = res.data.product.dress_material;
          this.productDetails = res.data.product;
          this.serverSizeChartUrl = res.data.url;
          // this.selectedItem = res.data.product.product_variant_id;
          this.variantOptions = res.data.variantOptions;
          this.coupons = res.data.coupons;
  
          res.data.variantOptions.primaryOptions.forEach((primaryOption) => {
            // if (
            //   primaryOption.unique_id ==
            //   this.route.snapshot.paramMap.get("unique_id")
            // ) {
            if (
              res.data.variantOptions.selectedPrimaryAttr == primaryOption.option_name
            ) {
              this.selectedProductVariant.primary_attribute_option_color =
                primaryOption.colour_code;
              this.selectedProductVariant.primary_attribute_option =
                primaryOption.option_name;
            }
          });
          res.data.variantOptions.secondaryOptions.forEach((secondaryOption) => {
            if (
              secondaryOption.unique_id ==
              this.route.snapshot.paramMap.get("unique_id")
            ) {
              this.selectedProductVariant.secondary_attribute_option_color =
                secondaryOption.colour_code;
              this.selectedProductVariant.secondary_attribute_option =
                secondaryOption.option_name;
            }
          });
          if (res.data.variantOptions.secondaryOptions.length == 0) {
            var variant = { product_variant_id: res.data.product.product_variant_id };
            this.selectedItem = variant;
          } else if (res.data.product.stock == 0) {
            var variant = { product_variant_id: res.data.product.product_variant_id };
            this.selectedItem = variant;
          }
          this.getSimilarProducts(res.data.product.cc_id);
          this.checkProductExistenceInWishlist();
          this.checkUserNotifiedForTheProduct();
  
          if (this.isMobile) {
            this.getReviews();
          }
        }else{
          this.angularnotifierService.showNotification("error", res.msg);
        }
      },
      error: (error) => {
        this.apisCalls++;
        if (this.apisCalls == 4) {
          this.loading = false;
        }
        var err = JSON.stringify(error);
      },
    });
  }

  checkProductExistenceInWishlist() {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        // token expired
        this.apisCalls++;

        if (this.apisCalls == 4) {
          this.loading = false;
        }
      } else {
        this.apisCalls++;
        if (this.apisCalls == 4) {
          this.loading = false;
        }
        this.shopService
          .checkProductExistInWishlist({ id: this.selectedItem })
          .subscribe((res: any) => {
            if(res.success){
              this.wishlistExists = res.wishlistExists;
              if (!res.wishlistExists) {
                var wishlistcheck = localStorage.getItem("detailwishlist");
                if (wishlistcheck != null) {
                  this.addToWishlist();
                  localStorage.removeItem("detailwishlist");
                }
              }
            }else{
              this.angularnotifierService.showNotification("error", res.msg);
            }
          });
      }
    } else {
      this.apisCalls++;

      if (this.apisCalls == 4) {
        this.loading = false;
      }
    }
  }

  checkUserNotifiedForTheProduct() {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        this.userNotified = false;
        this.userLoggedIn = false;
      } else {
        this.userLoggedIn = true;
        this.shopService
          .checkUserNotifiedForTheProduct({
            product_variant_id: this.productDetails?.product_variant_id,
          })
          .subscribe((res: any) => {
            if(res.success){
              this.userNotificationEnabled = res.get_notified;
              this.userNotified = res.userNotified;
            }else{
              this.angularnotifierService.showNotification("error", res.msg);
            }
          });
      }
    } else {
      this.userNotified = false;
      this.userLoggedIn = false;
    }
  }

  getSimilarProducts(cc_id) {
    this.shopService
      .similarProducts(cc_id, this.route.snapshot.paramMap.get("unique_id"))
      .subscribe((res: any) => {
        this.apisCalls++;

        if (this.apisCalls == 4) {
          this.loading = false;
        }
        if(res.success){
          this.recommendedProducts = res.products;
          this.imgUrl = res.imgUrl;
        }else{
          this.angularnotifierService.showNotification("default", res.msg);
        }
        // this.flashDeals.push(this.flashDeals1[0]);
      });
  }
  open(index: number): void {
    this._lightbox.open(this.productDetails.images, index);
  }

  finishTest() {
    this.route.params.subscribe((val) => {
      const postdata = {
        product_variant_slug: this.route.snapshot.paramMap.get(
          "product_variant_slug"
        ),
        unique_id: this.route.snapshot.paramMap.get("unique_id"),
      };
      this.getProductDetails(postdata);
    });
  }

  addToWishlist(action?) {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        // token expired
        var urlWithWishlist = this.router.url;
        localStorage.setItem("url", urlWithWishlist);
        localStorage.setItem("detailwishlist", "1");
        this.router.navigate(["login"], {
          queryParams: {
            q: urlWithWishlist,
          },
        });
        return;
      } else {
        if (action != "click") {
          if (this.wishlistExists) {
            return;
          }
        }
        this.shopService
          .addToWishlist(
            this.productDetails.product_variant_id,
            this.productDetails.product_id
          )
          .subscribe((res: any) => {
            if (res.success) {
              this.dataService.changeWishlishtCount(res.wishlistCount);
              const postdata = {
                product_variant_slug: this.route.snapshot.paramMap.get(
                  "product_variant_slug"
                ),
                unique_id: this.route.snapshot.paramMap.get("unique_id"),
              };
              this.checkProductExistenceInWishlist();

              // this.alertService.success("Success",res.msg)
              this.angularnotifierService.showNotification("default", res.msg);
            }
          });
      }
    } else {
      var urlWithWishlist = this.router.url;
      localStorage.setItem("url", urlWithWishlist);
      localStorage.setItem("detailwishlist", "1");
      this.router.navigate(["login"], {
        queryParams: {
          q: urlWithWishlist,
        },
      });
    }
  }
  close() {
    this._lightbox.close();
  }
  increment(qty) {
    qty++;
    this.quantity = qty;
  }
  decrement(qty) {
    if (qty != 1) {
      qty--;
    }
    this.quantity = qty;
  }
  onFormSubmit() {
    this.isSubmit = true;
    this.pincodeLoading = true;
    if (this.pincodeForm.invalid) {
      this.pincodeLoading = false;
      return;
    }
    var data = this.pincodeForm.value;
    data.unique_id = this.productDetails.unique_id
    this.shopService
      .checkenterPincode(data)
      .subscribe((data: any) => {
        if (!data.success) {
          this.angularnotifierService.showNotification("error", data.msg);
        }else{
          this.estimatedDelivery = data.etd;
        }
        this.pincodeLoading = false;
        this.isSubmit = false;
      });
  }
  omit_char(event) {
    var k;
    k = event.charCode;
    if ((k < 48 || k > 57) && k !== 13) {
      event.preventDefault();
      return false;
    }
  }

  filterPopup() {
    this.productmoreclass = !this.productmoreclass;
    this.productlessclass = !this.productlessclass;
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  openAddToCartModal(content2) {
    if (this.selectedItem) {
      this.modalService.open(content2, {
        centered: true,
        size: "md",
        animation: true,
        windowClass: "bottom-modal",
        keyboard: false,
      });
    } else {
      // var msg =
      //   "Please select the " + this.variantOptions?.secondaryAttributeName;
      // this.angularnotifierService.showNotification("error", msg);
      $(".Zoom-effect").addClass("background-selectsize");
      this.showSelectSizeText = true;
      document
        .getElementById("gotosizesection")
        .scrollIntoView({ behavior: "smooth" });
      $(".Zoom-effect").addClass("selectsize");
      setTimeout(RemoveClass, 1000);
      function RemoveClass() {
        $(".Zoom-effect").removeClass("selectsize");
      }
    }
  }
  //   scroll(el: HTMLElement) {
  //   el.scrollIntoView({ behavior: "smooth" });
  // }
  onFilterChange(e, productVariant) {
    this.addedClass = false;
    const url =
      "/products/" + productVariant.slug + "/" + productVariant.unique_id;
    this.router.navigate([
      "products",
      productVariant.slug,
      productVariant.unique_id,
    ]);
    if (this.variantOptions.secondaryOptions.length == 0) {
      var variant = {
        product_variant_id: this.productDetails.product_variant_id,
      };
      this.selectedItem = variant;
    } else {
      this.selectedItem = null;
    }
  }

  onSecondaryAttributeChange(e, productVariant) {
    this.addedClass = false;
    this.showSelectSizeText = false;
    $(".Zoom-effect").removeClass("background-selectsize");
    this.selectedItem = productVariant;
    if (
      productVariant.product_variant_id !=
      this.productDetails?.product_variant_id
    ) {
      const postdata = {
        product_variant_slug: productVariant.slug,
        unique_id: productVariant.unique_id,
      };
      this.getProductDetails(postdata);
    }
    // const url =
    //   "/products/" + productVariant.slug + "/" + productVariant.unique_id;
    // this.router.navigate([
    //   "products",
    //   productVariant.slug,
    //   productVariant.unique_id,
    // ]);
  }
  RecommendOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    autoplay: true,
    dots: false,
    nav: false,
    autoWidth: false,
    navSpeed: 700,
    margin: 10,
    navText: ["", ""],
    responsive: {
      0: {
        items: 2,
        dots: false,
      },
      400: {
        items: 2,
        dots: false,
      },
      740: {
        items: 3,
        dots: false,
      },
      940: {
        items: 6,
      },
    },
  };

  listClick(e, productVariant) {}

  addToCart(loadingiocn) {
    if (this.selectedItem) {
      this.addToCartLoading = true;
      // this.openLoading(loadingiocn);
      const cartdata = {
        product_variant_id: this.selectedItem.product_variant_id,
        quantity: this.quantity,
      };
      if (localStorage.getItem("customer") !== null) {
        var token = JSON.parse(localStorage.getItem("customer"));
        var tokenExpiry = this.authService.tokenExpired(token.access_token);
        if (tokenExpiry) {
          this.shopService.guestAddToCart(cartdata).subscribe((res: any) => {
            // this.closeModal();
            $(".Zoom-effect").removeClass("background-selectsize");
            this.showSelectSizeText = false;
            this.addToCartLoading = false;
            if (res.success) {
              this.addedClass = !this.addedClass;
              this.dataService.changeCartCount(res.cartCount);
              this.angularnotifierService.showNotification(
                "default",
                "Product Added to the Cart!"
              );
            } else {
              this.angularnotifierService.showNotification("error", res.msg);
            }
          });
          return;
        } else {
          this.shopService.addProductToCart(cartdata).subscribe({
            next: (res: any) => {
              $(".Zoom-effect").removeClass("background-selectsize");
              this.showSelectSizeText = false;
              this.addToCartLoading = false;
              // this.closeModal();
              if (res.success) {
                this.addedClass = !this.addedClass;
                this.dataService.changeCartCount(res.cartCount);
                this.angularnotifierService.showNotification(
                  "default",
                  "Success! , Product Added to the cart"
                );
              } else {
                this.angularnotifierService.showNotification("error", res.msg);
              }
            },
            error: (error) => {
              var err = JSON.stringify(error);
            },
          });
        }
      } else {
        this.shopService.guestAddToCart(cartdata).subscribe((res: any) => {
          $(".Zoom-effect").removeClass("background-selectsize");
          this.showSelectSizeText = false;
          this.addToCartLoading = false;
          // this.closeModal();
          if (res.success) {
            this.addedClass = !this.addedClass;
            this.dataService.changeCartCount(res.cartCount);
            this.angularnotifierService.showNotification(
              "default",
              "Product Added to the Cart!"
            );
          } else {
            this.angularnotifierService.showNotification("error", res.msg);
          }
        });
        return;
      }
    } else {
      // var msg =
      //   "Please select the " + this.variantOptions?.secondaryAttributeName;
      // this.angularnotifierService.showNotification("error", msg);
      $(".Zoom-effect").addClass("background-selectsize");
      $(".Zoom-effect").addClass("selectsize");
      this.showSelectSizeText = true;
      setTimeout(RemoveClass, 1000);
      function RemoveClass() {
        $(".Zoom-effect").removeClass("selectsize");
      }
    }
  }
  scroll() {
    alert("i");
    document
      .getElementById("viewdetails")
      .scrollIntoView({ behavior: "smooth" });
  }

  openLoading(loadingiocn) {
    this.modalService.open(loadingiocn, {
      centered: true,
      size: "sm",
      animation: true,
      windowClass: "loading-modal",
      backdrop: "static",
      keyboard: false,
    });
  }

  getNotified() {
    // if (this.userNotificationEnabled == "y") {
    this.shopService
      .stockNotify({
        product_variant_id: this.productDetails?.product_variant_id,
      })
      .subscribe((res: any) => {
        if (res.success) {
          this.checkUserNotifiedForTheProduct();
          this.angularnotifierService.showNotification(
            "default",
            "You will be notified!"
          );
        }
      });
    // } else {
    //   if (!this.userLoggedIn) {
    //     this.router.navigate(["/login"]);
    //   } else {
    //     this.el(".cookie-popup").classList.add("cookie-popup--not-accepted");
    //     this.el(".cookie-popup").classList.remove("cookie-popup--accepted");
    //   }
    // }
  }

  openSizechart(sizechart) {
    this.modalService.open(sizechart, {
      centered: true,
      size: "md",
      animation: true,
      windowClass: "bottom-modal",
    });
  }

  openPriceguarantee(priceguarantee) {
    this.modalService.open(priceguarantee, {
      centered: true,
      size: "xs",
      animation: true,
      windowClass: "bottom-modal",
    });
  }

  openSimilar(similar) {
    this.modalService.open(similar, {
      centered: true,
      size: "md",
      animation: true,
      windowClass: "bottom-modal",
    });
  }

  listImageClick(event, newValue2) {
    this.selectedItem2 = newValue2;
  }

  changeimage(item) {
    this.productImage = item;
    this.fixed = true;
  }

  getPolicy(data) {
    this.commonapiService.getPolicies(data).subscribe((res: any) => {
      if(res.success){
        this.policies = res.policies;
        this.router.navigate(["policies", data]);
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
  }

  @ViewChild("slickModal", { static: true }) slickModal: SlickCarouselComponent;
  slideConfig = {
    arrows: false,
    dots: false,
    slidesToShow: 6,
    slidesToScroll: 1,
    centerPadding: "20px",
    draggable: true,
    infinite: true,
    pauseOnHover: true,
    swipe: false,
    touchMove: true,
    vertical: false,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 3000,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
    ],
  };
  thumbConfig = {
    arrows: true,
    dots: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    centerPadding: "20px",
    draggable: true,
    nextArrow:
      '<button class="btn-next" (click)="next()"><i class="fa fa-angle-up"></i></button>\n',
    prevArrow:
      '<button class="btn-prev" (click)="prev()"><i class="fa fa-angle-down"></i></button>\n',
    infinite: false,
    pauseOnHover: true,
    swipe: false,
    touchMove: true,
    vertical: true,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 3000,
    adaptiveHeight: true,

    responsive: [
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
    ],
  };

  zoomConfig = {
    arrows: true,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    centerPadding: "20px",
    draggable: true,
    nextArrow:
      '<button class="btn-next" (click)="next()"><i class="fa fa-angle-up"></i></button>\n',
    prevArrow:
      '<button class="btn-prev" (click)="prev()"><i class="fa fa-angle-down"></i></button>\n',
    infinite: false,
    pauseOnHover: true,
    swipe: false,
    swipeToSlide: true,
    touchMove: true,
    vertical: true,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 3000,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  addSlide() {
    this.imgags.push({ img: "http://placehold.it/350x150/777777" });
  }

  removeSlide() {
    this.imgags.length = this.imgags.length - 1;
  }

  slickInit(e) {
    // console.log('slick initialized');
  }

  breakpoint(e) {
    // console.log('breakpoint');
  }

  afterChange(e) {
    // console.log('afterChange');
  }

  beforeChange(e) {
    // console.log('beforeChange');
  }
}

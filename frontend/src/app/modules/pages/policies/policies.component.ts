import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonapiService } from 'src/app/service/commonapi/commonapi.service';
import { Router } from '@angular/router';
import { AngularnotifierService } from 'src/app/service/angularnotifier/angularnotifier.service';
@Component({
  selector: 'app-policies',
  templateUrl: './policies.component.html',
  styleUrls: ['./policies.component.scss']
})
export class PoliciesComponent implements OnInit {
 
  policies:any;
  policyname: string;
  // public headername = this.policytitle;
  policytitle: any;
  constructor(  public commonapiService:CommonapiService, private route: ActivatedRoute,private router: Router,
    public angularnotifierService:AngularnotifierService) { }
 

  ngOnInit(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    let policyname= this.route.snapshot.paramMap.get('policyname');
    this.commonapiService.getPolicies(policyname).subscribe((res:any)=>{
      if(res.success){
        this.policies =res.policies
        this.policytitle=this.policies.display_name;
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    })
  }

}

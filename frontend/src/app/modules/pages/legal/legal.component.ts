import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss']
})
export class LegalComponent implements OnInit {

  public headername = "More Information";
  offers:any = []
  constructor(  private router: Router) { }

  getPolicy(data)
  {
    this.router.navigate(['policies',data]);
  
  }
  ngOnInit(): void {
  }

}

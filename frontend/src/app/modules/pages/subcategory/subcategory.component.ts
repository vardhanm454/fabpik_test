import { Component, HostListener, OnInit } from "@angular/core";
import { OwlOptions } from "ngx-owl-carousel-o";
import { Location } from "@angular/common";
import { ActivatedRoute, Router } from "@angular/router";
import { first } from "rxjs/operators";

import { ShopService } from "src/app/service/shop/shop.service";
declare var $: any;
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Options } from "@angular-slider/ngx-slider";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
@Component({
  selector: "app-subcategory",
  templateUrl: "./subcategory.component.html",
  styleUrls: ["./subcategory.component.scss"],
})
export class SubcategoryComponent implements OnInit {
  loading: boolean = true;
  subCategories: any[];
  categoryTitle: any;
  categorySlug: any;
  categoryId: any;
  loadinglength = [1, 2, 3, 4, 5, 6];
  scrHeight: any;
  scrWidth: any;
  products: any;
  images: any;
  catid: void;
  hide: Boolean = false;

  constructor(
    private _location: Location,
    private route: ActivatedRoute,
    private shopService: ShopService,
    private http: HttpClient,
    public modalService: NgbModal,
    public angularnotifierService:AngularnotifierService

  ) {
    this.getScreenSize();
    this.catid = this.getSubCategory(localStorage.getItem("catid"));
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('bgcategory-light');
  }
  @HostListener("window:resize", ["$event"])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    if (this.scrWidth < 760) {
      this.hide = true;
      // this.getSubCategory(localStorage.getItem("catid"));
    } else {
      this.hide = false;
    }
  }
  ngOnDestroy(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('bgcategory-light');
  }
  ngOnInit(): void {
    this.categoryId = localStorage.getItem("catid");
    this.shopService.getData().subscribe((products) => {
      this.products = products;
      const slug = this.route.snapshot.paramMap.get("slug");
      var w = this.products.results.filter((item) => item.slug === slug);
      this.images = w[0].images;
    });

  }

  getSubCategory(id) {
    this.loading = true;
    this.shopService
      .getCategorySubCategorymobile(id)
      .pipe(first())
      .subscribe((res) => {
        this.loading = false;
        if(res.success){
          this.categoryTitle = res.categories.category.title;
          this.categorySlug = res.categories.category.slug;
          this.subCategories = res.categories.subCategories;
        }else{
          this.angularnotifierService.showNotification(
            "error",
            res.msg
          );
        }
      });
  }

  subcategoryid(id, slug) {
    localStorage.setItem("subid", id);
    localStorage.setItem("subslug", slug);
  }
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    autoplay: true,
    autoplayHoverPause: true,
    navSpeed: 400,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
    nav: false,
  };

  bannerClick(link){
    window.open(link, "_self");
  }
}

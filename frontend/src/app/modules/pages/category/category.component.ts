import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ShopService } from 'src/app/service/shop/shop.service';
import { first, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { DataService } from 'src/app/service/data/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})

export class CategoryComponent implements OnInit {

  categories: any;
  loading: Boolean = true;
  category: any;
  categorieslength;
  loadinglength = [1,2,3,4,5,6,7,8,9]

  public headername: string = 'Categories';
  cardDetails: any;
  products: any;
  images: any;
  constructor(private _location: Location,
    private http: HttpClient,
    public dataService:DataService,
    private router:Router,
    private shopService: ShopService) {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('bgcategory-light');
  }


  goback() {
    this._location.back();
  }

  ngOnInit(): void {
    this.getCategory();
  }

  ngOnDestroy(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('bgcategory-light');
  }
  getCategory() {
    // this.shopService.getCategory()
    //   .pipe(first())
    //   .subscribe(res => {
    //     this.loading = false;
    //     this.categories = res;
    //     this.categorieslength = res.length;
    //   });
    this.dataService.currentCategories.subscribe(res => {
      if(res != null){
       this.loading = false;
       this.categories = res;
       this.categorieslength = res.length
      }
})
  }

  categoryid(id, slug,type) {
    localStorage.setItem('catid', id);
    localStorage.setItem('catslug', slug);
    if (type == "static") {
      this.router.navigate(["/products"], { queryParams: { q: slug } });
    } else {
      this.router.navigate(["/category", slug]);
    }
  
  }
  // get()
  // {
 
  //   this.shopService.getData().subscribe(
  //     products => {
       
  //       this.products = products; // commented this line
  //       console.log(products);
  //       var wantedData = this.products.filter(x => x.id == 11 );
  //       console.log(wantedData);
      
  //     }
  //    )
  // }
  
}

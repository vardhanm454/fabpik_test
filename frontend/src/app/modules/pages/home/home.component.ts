import {
  Component,
  HostListener,
  NgModule,
  OnInit,
  ViewChild,
} from "@angular/core";
import { Location } from "@angular/common";
import { OwlOptions } from "ngx-owl-carousel-o";
import { ShopService } from "src/app/service/shop/shop.service";
import { first } from "rxjs/operators";
import { Router, RouterModule } from "@angular/router";
import { Meta, Title } from "@angular/platform-browser";
import { DataService } from "src/app/service/data/data.service";
import { CountdownComponent } from "ngx-countdown";
import { ProgressSpinnerMode } from "@angular/material";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
declare var $: any;

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  title = "One Stop Portal For Kids Needs | Fabpik";
  mode: ProgressSpinnerMode = "determinate";
  cartCount = 0;
  dealsNumber = 172000;
  upcomingDeals: any = [];
  cartdata: any;
  flashdeals: any;
  wishlistdata: any;
  AllCategory: any;
  AllCategoryProduct: any;
  id: any;
  deals: any = [];
  loading: boolean = true;
  status = "start";
  @ViewChild("countdown") counter: CountdownComponent;
  imgUrl: any;

  resetTimer() {
    this.counter.restart();
  }
  test = "policies/price-guarantee";
  public brands = [
    { image: "assets/images/brands/kooka.png", name: "kooka", id: 49 },
    {
      image: "assets/images/brands/whitewater.png",
      name: "whitewater",
      id: 26,
    },
    { image: "assets/images/brands/logos-brands3.png", name: "poeny", id: 15 },
    { image: "assets/images/brands/neenee.png", name: "neenee", id: 14 },
    { image: "assets/images/brands/piakboo.png", name: "piakboo", id: 11 },
    { image: "assets/images/brands/rabi.png", name: "rabitat", id: 30 },
    { image: "assets/images/brands/sd.png", name: "solly dolly", id: 39 },
    { image: "assets/images/brands/sm.png", name: "smily kiddos", id: 33 },
    { image: "assets/images/brands/toiing.png", name: "toiing", id: 25 },
    // { image: "assets/images/brands/tt.png", name: "tt", id: 45 },
  ];

  sliders = [];

  public offersliders = [
    { image: "assets/images/home/slider1.jpg" },
    { image: "assets/images/home/slider2.jpg" },
    { image: "assets/images/home/slider3.jpg" },
    { image: "assets/images/home/slider4.jpg" },
    { image: "assets/images/home/slider5.jpg" },
  ];

  slickInit(e) {}

  RecommendOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    autoplay: false,
    dots: false,
    nav: true,
    navSpeed: 700,
    margin: 10,
    navText: [
      '<i class="fa fa-angle-left"></i>',
      '<i class="fa fa-angle-right"></i>',
    ],

    responsive: {
      0: {
        items: 4,
      },
      400: {
        items: 4,
      },
      740: {
        items: 4,
      },
      940: {
        items: 6,
      },
    },
  };

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    nav: false,
    autoplay: true,
    autoplayHoverPause: true,
    smartSpeed: 1000,
    // navSpeed:10000,
    // animateIn: 'fake',
    // animateOut: 'fake',
    navText: [
      '<i class="far fa-angle-left"></i>',
      '<i class="far fa-angle-right"></i>',
    ],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
  };
  OfferOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    autoplay: true,
    center: true,
    dots: false,
    nav: false,
    margin: 10,
    navSpeed: 700,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 3,
      },
    },
  };
  scrHeight: number;
  scrWidth: number;
  hide: boolean = false;
  constructor(
    private _location: Location,
    private shopService: ShopService,
    private metaTagService: Meta,
    private titleService: Title,
    private router: Router,
    public dataService: DataService,
    public angularnotifierService:AngularnotifierService
  ) {
    this.getScreenSize();
  }
  @HostListener("window:resize", ["$event"])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    if (this.scrWidth < 760) {
      this.hide = true;
    } else {
    }
  }

  finishTest() {
    this.getDeals();
  }

  goback() {
    this._location.back();
  }
  // prettyConfig: CountdownConfig[];

  flashConfig = {
    arrows: true,
    dots: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    centerPadding: "20px",
    autoplayspeed: 3000,
    draggable: false,
    nextArrow:
      '<button class="btn-next" (click)="next()"><i class="far fa-angle-right"></i></button>\n',
    prevArrow:
      '<button class="btn-prev" (click)="prev()"><i class="far fa-angle-left"></i></button>\n',
    infinite: false,
    pauseOnHover: true,
    swipe: false,
    touchMove: true,
    vertical: false,
    autoplay: true,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
    ],
  };

  dealsConfig = {
    arrows: true,
    centerPadding: "20px",
    draggable: false,
    infinite: true,
    pauseOnHover: true,
    swipe: false,
    touchMove: true,
    autoplay: true,
    adaptiveHeight: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    // autoplay: true,
    // centerMode: true,
    focusOnSelect: true,
    vertical: false,
    nextArrow:
      '<button class="btn-next"  id="btn-mainn" (click)="next()"><i class="far fa-angle-right"></i></button>\n',
    prevArrow:
      '<button class="btn-prev" (click)="prev()"><i class="far fa-angle-left"></i></button>\n',
    responsive: [
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
    ],
  };

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.metaTagService.updateTag({
      name: "keywords",
      content:
        "online shopping for kids, baby products online, kids dress online shopping, best online shopping for kids,  kids toys online shopping",
    });
    this.metaTagService.updateTag({
      name: "description",
      content:
        "Fabpik is an online kids needs store offering a massiverange of kids clothing, shoes, accessories, baby care products, kids toys and a lot more. Pay online or COD for convenient shopping experience and affordable place to shop for all kids needs.",
    });

    // this.getCategory();
    this.getFlashDealProduct();
    // this.CategoryProductlist(localStorage.getItem('id'));
    localStorage.removeItem("filterVarId");
    localStorage.removeItem("filterVarVal");
    localStorage.removeItem("filterBrandId");
    localStorage.removeItem("filterCId");
    this.loading = false;
    this.shopService.sliderImages("d").subscribe((res: any) => {
      if(res.success){
        this.sliders = res.images;
        this.imgUrl = res.imgUrl;
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
    this.getDeals();
  }

  getDeals() {
    this.shopService.deals().subscribe((res: any) => {
      if(res.success){
        this.deals = res.products;
        this.upcomingDeals = res.upcomingDeals;
        var temp = [];
        this.deals.forEach((deal) => {
          const config = {
            leftTime: 10,
            format: "HH:mm:ss",
            prettyText: (text) => {
              return text
                .split(":")
                .map(
                  (v) =>
                    `<div class="countdown-box"><span class="item time">${v}</span><span class="text"></span></div>`
                )
                .join("");
            },
          };
          temp.push(config);
        });
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
  }

  scrollToElement($element, targetUrl) {
    if (targetUrl == "https://fabpik.in/deals") {
      $element.scrollIntoView({
        behavior: "smooth",
      });
    } else {
      window.open(targetUrl, "_self");
    }
  }

  priceFilters(min, max) {
    this.router.navigate(["products"], {
      queryParams: {
        minpr: min,
        maxpr: max,
      },
    });
  }

  productvariant(product_variant_id) {
    localStorage.setItem("product_detail_id", product_variant_id);
    localStorage.setItem("product_listing", "y");
  }

  bannerClick(url) {
    window.open(url, "_self");
  }

  //get category list
  getCategory() {
    // this.shopService.getfiveCategory()
    //   .subscribe(res => {
    //     this.AllCategory = res;
    //     this.id = this.AllCategory[0]['id'];
    //     localStorage.setItem('id',this.id);
    //   });
  }

  clothingStarts99() {
    this.router.navigate(["products"], {
      queryParams: {
        cids: JSON.stringify([1]),
      },
    });
  }
  organic() {
    this.router.navigate(["products"], {
      queryParams: {
        q: "organic",
      },
    });
  }
  getProductBrands(id) {
    this.router.navigate(["products"], {
      queryParams: {
        brands: JSON.stringify([id]),
      },
    });
  }

  toys() {
    this.router.navigate(["products"], {
      queryParams: {
        cids: JSON.stringify([3]),
      },
    });
  }

  personalCare() {
    this.router.navigate(["products"], {
      queryParams: {
        cids: JSON.stringify([7]),
      },
    });
  }

  accessories() {
    this.router.navigate(["products"], {
      queryParams: {
        cids: JSON.stringify([10]),
      },
    });
  }

  flatFifty() {
    this.router.navigate(["products"], {
      queryParams: {
        ediscount: 50,
      },
    });
  }

  uptoFifty() {
    this.router.navigate(["products"], {
      queryParams: {
        udiscount: 50,
      },
    });
  }

  fourtyPOff() {
    this.router.navigate(["products"], {
      queryParams: {
        ediscount: 40,
      },
    });
  }

  thirtyPOff() {
    this.router.navigate(["products"], {
      queryParams: {
        ediscount: 30,
      },
    });
  }

  //get flash deal product
  getFlashDealProduct() {
    this.shopService.getFlashDealProduct().subscribe((res: any) => {
      if(res.success){
        this.flashdeals = res.products;
        this.imgUrl = res.imgUrl;
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
  }

  // add product to cart
  addProductToCart(id) {
    this.cartCount += 1;
    this.cartdata = {
      product_variant_id: id,
      quantity: this.cartCount,
    };

    this.shopService
      .addProductToCart(this.cartdata)
      .pipe(first())
      .subscribe({
        next: (res: any) => {
          if (res.success) {
            alert("Product Added to the cart");
          }
        },
        error: (error) => {
          var err = JSON.stringify(error);
          //  alert(err);
        },
      });
  }

  // add product to wishlist
  addToWishlist(product_id, product_variant_id) {
    this.wishlistdata = {
      product_id: product_id,
      product_variant_id: product_variant_id,
    };

    this.shopService.addToWishlist(product_variant_id, product_id).subscribe({
      next: (res: any) => {
        alert("Product Added to the wishlist");
      },
      error: (error) => {
        var err = JSON.stringify(error);
        alert(err);
      },
    });
  }

  //get category wise product list
  CategoryProductlist(id) {
    this.shopService.CategoryProductlist(id).subscribe((res) => {
      this.AllCategoryProduct = res;
    });
  }
  CategoryProd(id) {
    // this.shopService.CategoryProductlist(id)
    // .subscribe(res => {
    //   this.AllCategoryProduct = res;
    // });
  }

  onImgError(e) {
    e.target.src = "/assets/images/no-image.jpg";
  }
}

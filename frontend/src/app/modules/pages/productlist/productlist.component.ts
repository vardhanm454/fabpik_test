import {
  Component,
  HostListener,
  Input,
  OnInit,
  ɵConsole,
} from "@angular/core";
declare var $: any;
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { OwlOptions } from "ngx-owl-carousel-o";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Location } from "@angular/common";
import { filter, first } from "rxjs/operators";
import { LabelType, Options } from "@angular-slider/ngx-slider";
import { ActivatedRoute, Router } from "@angular/router";

import { ShopService } from "src/app/service/shop/shop.service";
import { SearchService } from "src/app/service/search/search.service";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { DataService } from "src/app/service/data/data.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-productlist",
  templateUrl: "./productlist.component.html",
  styleUrls: ["./productlist.component.scss"],
  host: {
    "(window:resize)": "onWindowResize($event)",
  },
})
export class ProductlistComponent implements OnInit {
  isMobile: boolean = false;
  isDesktop: boolean = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth: number = 767;
  lazyLoadSpinner = "assets/images/no-image.jpg";
  lazyLoadOffset = 100;
  @Input() Interests: any;
  @Input() parentCategory = null;
  @Input() subCategory = null;
  brands: any = [];
  pageTitle: string = "Products";
  fixedfilters: boolean = false;
  filterclass: boolean = false;
  filters: any = {
    q: null,
    discount: null,
    udiscount: null,
    ediscount: null,
    page: 1,
    sortBy: "popularity",
    cids: [],
    scids: [],
    ccids: [],
    brands: [],
    attrs: [],
    minpr: null,
    maxpr: null,
    tag: null,
    coupon: null,
  }; // cids: Category ids, scids: Sub Category Ids,
  products = [];
  searchText: any;
  wishlists: any = [];
  params: any = {};
  categories: any = [];
  minMaxPrices: any = { min: 0, max: 25000 };
  primaryAttributeName: any;
  secondaryAttributeName: any;
  primaryAttributeType: any;
  secondaryAttributeType: any;
  primaryAttributeOptions: any = [];
  secondaryAttributeOptions: any = [];
  parentSlug: any;
  url: any;
  subSlug: any;
  childSlug: any;
  apisCalls: any = 0;
  notEmptyProduct = true;
  imageToShowOnError: any = "/assets/images/no-image.jpg";
  notscrolly = true;
  n = 1;

  productlistjsonURL = "http://13.233.197.247/dev/api/product/productList";

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    nav: false,

    navSpeed: 700,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 2,
      },
      740: {
        items: 3,
      },
      940: {
        items: 4,
      },
    },
  };
  options: Options = {
    floor: 0,
    ceil: 25000,
  };
  min: any = 0;
  max: any = 25000;
  loading: boolean = true;
  array: any;
  sum: any = 100;
  direction: string = "";
  scrolled: boolean = false;
  pageProducts: any[];
  chilid: string;
  childid: any;
  defaultChildId: number;
  AllCategory: any;
  parentCategories: any = null;
  hiddencatdiv: boolean = true;
  hiddensubdiv: boolean = true;
  hiddenchilddiv: boolean = true;
  subCategories: any = null;
  childCategories: any = null;
  productListHeading: any;
  selectedDiscount: any;
  scrHeight: any;
  scrWidth: any;
  activeclass: any;
  parentCategoryBreadCrumb: any = null;
  subCategoryBreadCrumb: any = null;
  childCategoryBreadCrumb: any = [];
  categoriesForBreadCrumb: any;
  imgUrl: any;
  priceFilterForm: FormGroup;
  goback() {
    this._location.back();
  }

  constructor(
    private http: HttpClient,
    public modalService: NgbModal,
    private _location: Location,
    private shopService: ShopService,
    private route: ActivatedRoute,
    private router: Router,
    private searchService: SearchService,
    private spinner: NgxSpinnerService,
    private authService: AuthenticationService,
    private angularnotifierService: AngularnotifierService,
    public dataService: DataService,
    private formBuilder: FormBuilder
  ) {
    this.priceFilterForm = this.formBuilder.group({
      min: [this.min, [Validators.nullValidator]],
      max: [this.max, [Validators.nullValidator]],
    });
    this.defaultChildId = parseInt(localStorage.getItem("childcategoryid"));
    this.getScreenSize();
  }

  @HostListener("window:resize", ["$event"])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    if (this.scrWidth < 760) {
    } else {
    }
  }

  subcategory() {
    this.hiddencatdiv = false;
    this.hiddensubdiv = false;
  }

  backcat() {
    this.hiddencatdiv = true;
    this.hiddensubdiv = true;
    this.hiddenchilddiv = true;
  }

  child() {
    this.hiddenchilddiv = false;
    this.hiddensubdiv = true;
  }

  backsub() {
    this.hiddenchilddiv = true;
    this.hiddensubdiv = false;
  }

  ngOnInit(): void {
    this.activeclass = "popularity";
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
    this.url = this.router.url;
    this.filters.page = 1;
    const regexForProducts = /products/g;
    const regexForCats = /category/g;

    if (this.url.match(regexForProducts)) {
      this.route.queryParamMap.subscribe((params) => {
        var cids = JSON.parse(params.get("cids"));
        var scids = JSON.parse(params.get("scids"));
        var ccids = JSON.parse(params.get("ccids"));
        var brands = JSON.parse(params.get("brands"));
        var attrs = JSON.parse(params.get("attrs"));
        var minpr = params.get("minpr");
        var maxpr = params.get("maxpr");
        var discount = params.get("discount");
        var ediscount = params.get("ediscount");
        var udiscount = params.get("udiscount");
        var tag = params.get("tg");
        var coupon = params.get("cp");
        // var wishlist = params.get("wishlist");
        // if (wishlist) {
        //   var wishlistParsed;
        //   if ((wishlistParsed = JSON.parse(wishlist))) {
        //     this.addToWishlist(wishlistParsed[0], wishlistParsed[1]);
        //   }
        // }
        var q = params.get("q");
        if (q != null) {
          this.productListHeading = `Search results for "${q}"`;
        }
        this.filters.cids = cids == null ? [] : cids;
        this.filters.scids = scids == null ? [] : scids;
        this.filters.ccids = ccids == null ? [] : ccids;
        this.filters.brands = brands == null ? [] : brands;
        this.filters.attrs = attrs == null ? [] : attrs;
        this.filters.minpr = minpr == null ? null : parseInt(minpr);
        this.filters.maxpr = maxpr == null ? null : parseInt(maxpr);
        this.filters.discount = discount == null ? null : parseInt(discount);
        this.filters.ediscount = ediscount == null ? null : parseInt(ediscount);
        this.filters.udiscount = udiscount == null ? null : parseInt(udiscount);
        this.filters.q = q == null ? null : q;
        this.filters.tag = tag == null ? null : tag;
        this.filters.coupon = coupon == null ? null : coupon;
        this.selectedDiscount = discount;
        if (this.filters.cids.length > 0) {
          this.getAttributeFilters();
        }
        this.getAllBrands();
        if (minpr != null && maxpr != null) {
          // const newOptions: Options = Object.assign({}, this.options);
          // newOptions.floor = parseInt(minpr);
          // newOptions.ceil = parseInt(maxpr);
          // this.options = newOptions;
          this.min = parseInt(minpr);
          this.max = parseInt(maxpr);
        }
        if (cids != null && scids != null) {
          this.shopService
            .getChildCategories({
              parentCategoryId: cids[0],
              subCategoryId: scids[0],
            })
            .subscribe((res: any) => {
              this.apisCalls++;
              if (this.apisCalls == 5) {
                this.loading = false;
              }
              if(res.success){
                this.parentCategories = res.parentCategory;
                this.subCategories = res.subCategory;
                this.childCategories = res.childCategories;
              }else{
                this.angularnotifierService.showNotification("error", res.msg);
              }
            });
        } else if (cids != null && scids == null) {
          this.shopService
            .getSubCategories({ parentCategoryId: cids[0] })
            .subscribe((res: any) => {
              this.apisCalls++;
              if (this.apisCalls == 5) {
                this.loading = false;
              }
              if(res.success){
                this.parentCategories = res.parentCategory;
                this.subCategories = res.subCategories;
              }else{
                this.angularnotifierService.showNotification("error", res.msg);
              }
            });
        } else if (cids == null) {
          this.shopService.getParentCategoires().subscribe((res: any) => {
            this.apisCalls++;
            if (this.apisCalls == 5) {
              this.loading = false;
            }
            if(res.success){
              this.parentCategories = res.parentCategories;
            }else{
              this.angularnotifierService.showNotification("error", res.msg);
            }
          });
        }
        this.getListingProducts();
      });
    } else if (this.url.match(regexForCats)) {
      // this.route.queryParamMap.subscribe((params) => {
      //   var wishlist = params.get("wishlist");
      //   if (wishlist) {
      //     var wishlistParsed;
      //     if ((wishlistParsed = JSON.parse(wishlist))) {
      //       this.addToWishlist(wishlistParsed[0], wishlistParsed[1]);
      //     }
      //   }
      // });
      var subUrls = this.url.split("/");
      if (subUrls.length == 3) {
        this.shopService
          .getSubCategoriesBySlugs({
            parentCategorySlug: this.route.snapshot.paramMap.get("slug"),
          })
          .subscribe((res: any) => {
            this.apisCalls++;
            if (this.apisCalls == 5) {
              this.loading = false;
            }
            if (res.success) {
              this.filters.cids.push(res.parentCategory.id);
              this.getAttributeFilters();
              this.getAllBrands();
              // this.filters.scids.push(localStorage.getItem("subcategoryid"))
              this.getListingProducts();
              this.parentCategories = res.parentCategory;
              this.subCategories = res.subCategories;
            } else {
              this.router.navigate(["products"]);
            }
          });
      } else if (subUrls.length == 4) {
        this.shopService
          .getChildCategoriesBySlug({
            parentCategorySlug: this.route.snapshot.paramMap.get("slug"),
            subCategorySlug: this.route.snapshot.paramMap.get("subslug"),
          })
          .subscribe((res: any) => {
            this.apisCalls++;
            if (this.apisCalls == 5) {
              this.loading = false;
            }
            if (res.success) {
              this.filters.cids.push(res.parentCategory.id);
              this.filters.scids.push(res.subCategory.id);
              this.getAttributeFilters();
              this.getAllBrands();
              // this.filters.ccids.push(localStorage.getItem("childcategoryid"))
              this.getListingProducts();
              this.parentCategories = res.parentCategory;
              this.subCategories = res.subCategory;
              this.childCategories = res.childCategories;
            } else {
              this.router.navigate(["products"]);
            }
          });
      } else if (subUrls.length == 5) {
        this.shopService
          .getChildCategoriesBySlug({
            parentCategorySlug: this.route.snapshot.paramMap.get("slug"),
            subCategorySlug: this.route.snapshot.paramMap.get("subslug"),
          })
          .subscribe((res: any) => {
            this.apisCalls++;
            if (this.apisCalls == 5) {
              this.loading = false;
            }
            if (res.success) {
              this.filters.cids.push(res.parentCategory.id);
              this.filters.scids.push(res.subCategory.id);
              this.getAttributeFilters();
              this.getAllBrands();
              var found = 0;
              res.childCategories.forEach((child) => {
                if (
                  child.slug == this.route.snapshot.paramMap.get("childslug")
                ) {
                  this.filters.ccids.push(child.id);
                  found = 1;
                }
              });
              if (found == 0) {
                this.router.navigate(["products"]);
              }
              // this.filters.ccids.push(localStorage.getItem("childcategoryid"))
              this.getListingProducts();
              this.parentCategories = res.parentCategory;
              this.subCategories = res.subCategory;
              this.childCategories = res.childCategories;
            } else {
              this.router.navigate(["products"]);
            }
          });
      } else if (subUrls.length > 5) {
        this.router.navigate(["products"]);
      }
    }

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.getWishlists();
  }

  ngAfterViewInit() {
    this.priceFilterForm.patchValue({ min: this.min, max: this.max });
  }

  getAttributeFilters() {
    if (this.filters.cids.length > 0) {
      this.shopService
        .getAttributeFilters({ parentCategory: this.filters.cids[0] })
        .subscribe((res: any) => {
          if (res.success) {
            this.primaryAttributeName = res.attributes.primaryAttributeName;
            this.secondaryAttributeName = res.attributes.secondaryAttributeName;
            this.primaryAttributeOptions =
              res.attributes.primaryAttributeOptions;
            this.secondaryAttributeOptions =
              res.attributes.secondaryAttributeOptions;
            this.primaryAttributeType = res.attributes.primary_attribute_type;
            this.secondaryAttributeType =
              res.attributes.secondary_attribute_type;
          }
        });
    }
  }

  onSubmitPriceFilter() {
    if (
      this.priceFilterForm.value.min == null &&
      this.priceFilterForm.value.max == null
    ) {
      return;
    } else if (
      this.priceFilterForm.value.min != null &&
      this.priceFilterForm.value.max == null
    ) {
      this.min = this.priceFilterForm.value.min;
      this.max = 25000;
      this.sliderEvent("d");
    } else if (
      this.priceFilterForm.value.min == null &&
      this.priceFilterForm.value.max != null
    ) {
      this.min = 0;
      this.max = this.priceFilterForm.value.max;
      this.sliderEvent("d");
    } else if (
      this.priceFilterForm.value.min != null &&
      this.priceFilterForm.value.max != null
    ) {
      this.min = this.priceFilterForm.value.min;
      this.max = this.priceFilterForm.value.max;
      this.sliderEvent("d");
    }
  }

  mobileInputPriceFilter(value, type) {
    if (type == "min") {
      this.filters.minpr = value;
    } else {
      this.filters.maxpr = value;
    }
  }

  receiveSearchQuery($event) {
    // this.message = $event
    this.filters.q = $event;
    this.filters.page = 1;
    this.getListingProducts();
  }

  getAllBrands() {
    this.shopService
      .getAllBrands({ cid: this.filters.cids[0] })
      .subscribe((res: any) => {
        if(res.success){
          this.brands = res.brands;
        }else{
          this.angularnotifierService.showNotification("error", res.msg);
        }
        this.apisCalls++;
        if (this.apisCalls == 5) {
          this.loading = false;
        }
      });
  }

  parentCategoryFilterClick($event) {
    this.filters.cids.push($event);
    this.filters.page = 1;
    this.getListingProducts();
  }

  parentCategoryClick(id, allParents?) {
    if (id == 16) {
      this.router.navigate(["products"], {
        queryParams: {
          q: "organic",
        },
      });
      return;
    }
    const parentSlug = this.route.snapshot.paramMap.get("slug");
    const subSlug = this.route.snapshot.paramMap.get("subslug");
    const childSlug = this.route.snapshot.paramMap.get("childslug");
    const regexForProducts = /products/g;
    const regexForCats = /category/g;
    this.route.queryParamMap.subscribe((params) => {
      const q = params.get("q");
      this.router.navigate(["products"], {
        queryParams: {
          cids: !allParents ? JSON.stringify([id]) : undefined,
          q: params.get("q"),
          brands: params.get("brands"),
          attrs: params.get("attrs"),
          tg: params.get("tg"),
          cp: params.get("cp"),
          minpr: params.get("minpr"),
          maxpr: params.get("maxpr"),
          discount: params.get("discount"),
          ediscount: params.get("ediscount"),
          udiscount: params.get("udiscount"),
        },
      });
    });

    // if(this.url.match(regexForProducts)){

    // }
    // else if(this.url.match(regexForCats)){
    //         this.router.navigate(['category',parentSlug],
    //         {queryParams:
    //             {
    //                 cids:JSON.stringify ([id]),
    //             }
    //         })

    // }
  }

  subCategoryClick(id, pid, allSubCats?) {
    const parentSlug = this.route.snapshot.paramMap.get("slug");
    const subSlug = this.route.snapshot.paramMap.get("subslug");
    const childSlug = this.route.snapshot.paramMap.get("childslug");
    const regexForProducts = /products/g;
    const regexForCats = /category/g;
    if (this.url.match(regexForProducts)) {
      this.route.queryParamMap.subscribe((params) => {
        this.router.navigate(["products"], {
          queryParams: {
            cids: params.get("cids"),
            q: params.get("q"),
            scids: !allSubCats ? JSON.stringify([id]) : undefined,
            brands: params.get("brands"),
            attrs: params.get("attrs"),
            tg: params.get("tg"),
            cp: params.get("cp"),
            minpr: params.get("minpr"),
            maxpr: params.get("maxpr"),
            discount: params.get("discount"),
            ediscount: params.get("ediscount"),
            udiscount: params.get("udiscount"),
          },
        });
      });
    } else if (this.url.match(regexForCats)) {
      this.route.queryParamMap.subscribe((params) => {
        this.router.navigate(["products"], {
          queryParams: {
            cids: JSON.stringify([pid]),
            scids: JSON.stringify([id]),
          },
        });
      });
    }
  }

  childCategoriesClick(e, id, pid, sid) {
    this.route.queryParamMap.subscribe((params) => {
      if (e.target.checked) {
        var ccids = JSON.parse(params.get("ccids"));
        if (ccids != null) {
          ccids.push(id);
        } else {
          ccids = [id];
        }
        const regexForProducts = /products/g;
        const regexForCats = /category/g;
        if (this.url.match(regexForProducts)) {
          this.router.navigate(["products"], {
            queryParams: {
              cids: params.get("cids"),
              scids: params.get("scids"),
              ccids: JSON.stringify(ccids),
              q: params.get("q"),
              tg: params.get("tg"),
              cp: params.get("cp"),
              brands: params.get("brands"),
              attrs: params.get("attrs"),
              minpr: params.get("minpr"),
              maxpr: params.get("maxpr"),
              discount: params.get("discount"),
              ediscount: params.get("ediscount"),
              udiscount: params.get("udiscount"),
            },
          });
        } else if (this.url.match(regexForCats)) {
          this.router.navigate(["products"], {
            queryParams: {
              cids: JSON.stringify([pid]),
              scids: JSON.stringify([sid]),
              ccids: JSON.stringify(ccids),
            },
          });
        }
      } else {
        const regexForProducts = /products/g;
        const regexForCats = /category/g;
        if (this.url.match(regexForProducts)) {
          var ccids = JSON.parse(params.get("ccids"));
          const filtersIndex = ccids.indexOf(parseInt(id));
          if (filtersIndex > -1) {
            ccids.splice(filtersIndex, 1);
          }
          this.router.navigate(["products"], {
            queryParams: {
              cids: params.get("cids"),
              scids: params.get("scids"),
              ccids: ccids.length == 0 ? undefined : JSON.stringify(ccids),
              q: params.get("q"),
              brands: params.get("brands"),
              tg: params.get("tg"),
              cp: params.get("cp"),
              attrs: params.get("attrs"),
              minpr: params.get("minpr"),
              maxpr: params.get("maxpr"),
              discount: params.get("discount"),
              ediscount: params.get("ediscount"),
              udiscount: params.get("udiscount"),
            },
          });
        } else if (this.url.match(regexForCats)) {
          this.router.navigate(["products"], {
            queryParams: {
              cids: JSON.stringify([pid]),
              scids: JSON.stringify([sid]),
              q: params.get("q"),
              tg: params.get("tg"),
              cp: params.get("cp"),
              brands: params.get("brands"),
              attrs: params.get("attrs"),
              minpr: params.get("minpr"),
              maxpr: params.get("maxpr"),
              discount: params.get("discount"),
              ediscount: params.get("ediscount"),
              udiscount: params.get("udiscount"),
              // ccids:ccids.length == 0 ? undefined : JSON.stringify(ccids)
            },
          });
        }
      }
    });
  }

  parentCategoryMobileViewClick(id) {
    this.filters.cids = [id];
    this.shopService
      .getSubCategories({ parentCategoryId: id })
      .subscribe((res: any) => {
        // this.router.navigate([], { relativeTo: this.route, queryParams: { cids:JSON.stringify([id]) }});
        if(res.success){
          this.parentCategories = res.parentCategory;
          this.subCategories = res.subCategories;
          this.childCategories = null;
        }else{
          this.angularnotifierService.showNotification("error", res.msg);
        }
      });
  }

  subCategoryMobileViewClick(sid, pid) {
    this.filters.scids = [sid];
    this.shopService
      .getChildCategories({ parentCategoryId: sid, subCategoryId: pid })
      .subscribe((res: any) => {
        // this.router.navigate([], { relativeTo: this.route, queryParams: { cids:JSON.stringify([pid]),
        //                                                                      scids:JSON.stringify([sid]) }});
        if(res.success){
          this.parentCategories = res.parentCategory;
          this.subCategories = res.subCategory;
          this.childCategories = res.childCategories;
        }else{
          this.angularnotifierService.showNotification("error", res.msg);
        }
      });
  }

  childCategoriesMobileViewClick(e, ccid) {
    // this.filters.ccids.push(ccid)
    if (e.target.checked) {
      this.filters.ccids.push(parseInt(ccid));
    } else {
      const filtersIndex = this.filters.ccids.indexOf(parseInt(ccid));
      if (filtersIndex > -1) {
        this.filters.ccids.splice(filtersIndex, 1);
      }
    }
  }

  receiveCategories($event) {
    var cats = $event;
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }

  onBrandsChange(e, slug, id) {
    const regexForProducts = /products/g;
    const regexForCats = /category/g;
    if (this.url.match(regexForProducts)) {
      this.route.queryParamMap.subscribe((params) => {
        if (e.target.checked) {
          var brands = JSON.parse(params.get("brands"));
          if (brands != null) {
            brands.push(id);
            this.router.navigate(["products"], {
              queryParams: {
                cids: params.get("cids"),
                scids: params.get("scids"),
                ccids: params.get("ccids"),
                brands: JSON.stringify(brands),
                attrs: params.get("attrs"),
                minpr: params.get("minpr"),
                maxpr: params.get("maxpr"),
                tg: params.get("tg"),
                cp: params.get("cp"),
                q: params.get("q"),
                ediscount: params.get("ediscount"),
                udiscount: params.get("udiscount"),
                discount: params.get("discount"),
              },
            });
          } else {
            this.router.navigate(["products"], {
              queryParams: {
                cids: params.get("cids"),
                scids: params.get("scids"),
                ccids: params.get("ccids"),
                brands: JSON.stringify([id]),
                attrs: params.get("attrs"),
                minpr: params.get("minpr"),
                maxpr: params.get("maxpr"),
                q: params.get("q"),
                tg: params.get("tg"),
                cp: params.get("cp"),
                ediscount: params.get("ediscount"),
                udiscount: params.get("udiscount"),
                discount: params.get("discount"),
              },
            });
          }
        } else {
          var brands = JSON.parse(params.get("brands"));
          const filtersIndex = brands.indexOf(parseInt(id));
          if (filtersIndex > -1) {
            brands.splice(filtersIndex, 1);
          }
          this.router.navigate(["products"], {
            queryParams: {
              cids: params.get("cids"),
              scids: params.get("scids"),
              ccids: params.get("ccids"),
              brands: brands.length == 0 ? undefined : JSON.stringify(brands),
              attrs: params.get("attrs"),
              minpr: params.get("minpr"),
              maxpr: params.get("maxpr"),
              q: params.get("q"),
              tg: params.get("tg"),
              cp: params.get("cp"),
              ediscount: params.get("ediscount"),
              udiscount: params.get("udiscount"),
              discount: params.get("discount"),
            },
          });
        }
      });
    } else if (this.url.match(regexForCats)) {
      var subUrls = this.url.split("/");
      if (subUrls.length == 3) {
        this.router.navigate(["products"], {
          queryParams: {
            cids: JSON.stringify(this.filters.cids),
            brands: JSON.stringify([id]),
          },
        });
      } else if (subUrls.length == 4) {
        this.router.navigate(["products"], {
          queryParams: {
            cids: JSON.stringify(this.filters.cids),
            scids: JSON.stringify(this.filters.scids),
            brands: JSON.stringify([id]),
          },
        });
      } else if (subUrls.length == 5) {
        this.router.navigate(["products"], {
          queryParams: {
            cids: JSON.stringify(this.filters.cids),
            scids: JSON.stringify(this.filters.scids),
            ccids: JSON.stringify(this.filters.ccids),
            brands: JSON.stringify([id]),
          },
        });
      }
    }
  }

  onAttributesChange(e, attribute_option_id) {
    const regexForProducts = /products/g;
    const regexForCats = /category/g;
    var cids;
    var scids;
    var ccids;
    if (this.url.match(regexForCats)) {
      if (this.filters.cids.length > 0) {
        cids = JSON.stringify(this.filters.cids);
      } else {
        cids = undefined;
      }

      if (this.filters.scids.length > 0) {
        scids = JSON.stringify(this.filters.scids);
      } else {
        scids = undefined;
      }

      if (this.filters.ccids.length > 0) {
        ccids = JSON.stringify(this.filters.ccids);
      } else {
        ccids = undefined;
      }
    } else {
      this.route.queryParamMap.subscribe((params) => {
        cids = params.get("cids");
        scids = params.get("sccids");
        ccids = params.get("ccids");
      });
    }
    this.route.queryParamMap.subscribe((params) => {
      if (e.target.checked) {
        var attrs = JSON.parse(params.get("attrs"));
        if (attrs != null) {
          attrs.push(attribute_option_id);
          this.router.navigate(["products"], {
            queryParams: {
              cids: cids,
              scids: scids,
              ccids: ccids,
              attrs: JSON.stringify(attrs),
              brands: params.get("brands"),
              minpr: params.get("minpr"),
              maxpr: params.get("maxpr"),
              q: params.get("q"),
              tg: params.get("tg"),
              cp: params.get("cp"),
              ediscount: params.get("ediscount"),
              udiscount: params.get("udiscount"),
              discount: params.get("discount"),
            },
          });
        } else {
          this.router.navigate(["products"], {
            queryParams: {
              cids: cids,
              scids: scids,
              ccids: ccids,
              attrs: JSON.stringify([attribute_option_id]),
              brands: params.get("brands"),
              minpr: params.get("minpr"),
              maxpr: params.get("maxpr"),
              q: params.get("q"),
              tg: params.get("tg"),

              cp: params.get("cp"),
              ediscount: params.get("ediscount"),
              udiscount: params.get("udiscount"),
              discount: params.get("discount"),
            },
          });
        }
      } else {
        var attrs = JSON.parse(params.get("attrs"));
        const filtersIndex = attrs.indexOf(parseInt(attribute_option_id));
        if (filtersIndex > -1) {
          attrs.splice(filtersIndex, 1);
        }
        this.router.navigate(["products"], {
          queryParams: {
            cids: cids,
            scids: scids,
            ccids: ccids,
            attrs: attrs.length == 0 ? undefined : JSON.stringify(attrs),
            brands: params.get("brands"),
            minpr: params.get("minpr"),
            maxpr: params.get("maxpr"),
            q: params.get("q"),
            tg: params.get("tg"),
            cp: params.get("cp"),
            ediscount: params.get("ediscount"),
            udiscount: params.get("udiscount"),
            discount: params.get("discount"),
          },
        });
      }
    });
  }

  onScroll() {
    if (this.notEmptyProduct) {
      if (this.scrolled == false) {
        this.filters.page++;
        this.scrolled = true;
        this.shopService.getProductList(this.filters).subscribe({
          next: (res: any) => {
            if(res.success){
              this.imgUrl = res.data.imgUrl;
              this.pageProducts = res.data.products;
              // this.productListHeading = `Showing 1-${res.data.products.length} of ${res.data.totalResults} products`;
              this.scrolled = false;
              this.products = [...this.products, ...this.pageProducts];
              if (res.data.products.length === 0) {
                this.notEmptyProduct = false;
              } else {
                if (this.products.length == 0) {
                  this.productListHeading = "";
                } else {
                  this.productListHeading = `Showing 1-${this.products.length} of ${res.data.totalResults} products`;
                }
              }
            }else{
              this.angularnotifierService.showNotification("error", res.msg);
            }
            this.loading = false;
          },
          error: (error) => {
            var err = JSON.stringify(error.error);
          },
        });
      }
    }
  }

  getCategorySubCategory() {
    this.shopService.getCategory().subscribe((res) => {
      this.AllCategory = res;
    });
  }

  getListingProducts() {
    this.loading = true;
    this.shopService.getProductList(this.filters).subscribe({
      next: (res: any) => {
        // this.pageProducts = res;
        // if(this.pageProducts.length === 0) {
        //     this.notEmptyProduct = false;
        //     this.scrolled = false;
        // }
        if(res.success){
          this.imgUrl = res.data.imgUrl;
          this.products = res.data.products;
          if (res.data.totalResults > 0) {
            if (this.products.length == 0) {
              this.productListHeading = "";
            } else {
              this.productListHeading = `Showing 1-${res.data.products.length} of ${res.data.totalResults} products`;
            }
          }
        }else{
          this.angularnotifierService.showNotification("error", res.msg);
        }
        this.loading = false;
        this.apisCalls++;
        // this.filters.page++;
        if (this.apisCalls == 5) {
          this.loading = false;
        }
      },
      error: (error) => {
        var err = JSON.stringify(error.error);
      },
    });
  }

  getproductfilters() {
    this.shopService.getParentCategoires().subscribe((res: any) => {
      if(res.success){
        this.parentCategories = res.parentCategories;
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
    });
    // this.params['subCategoryId'] = localStorage.getItem('subid');
    // const parentCategorySlug = this.route.snapshot.paramMap.get('slug');
    // const subCategorySlug = this.route.snapshot.paramMap.get('subslug');
    // const currentChildCategorySlug = this.route.snapshot.paramMap.get('childslug');
    // const postData = { parentCategorySlug, subCategorySlug, currentChildCategorySlug };

    // this.shopService.getProductsFilters(postData).subscribe(
    //     (res: any) => {
    //         this.categories = res.categories;
    //         this.brands = res.brands;
    //         this.minMaxPrices = res.minMaxPrices;
    //         this.options.floor = res.minMaxPrices.min;
    //         this.options.ceil = res.minMaxPrices.max;
    //         this.primaryAttributeName = res.attributes.primaryAttributeName;
    //         this.secondaryAttributeName = res.attributes.secondaryAttributeName;
    //         this.primaryAttributeOptions = res.attributes.primaryAttributeOptions;
    //         this.secondaryAttributeOptions = res.attributes.secondaryAttributeOptions;
    //         this.primaryAttributeType = res.attributes.primary_attribute_type;
    //         this.secondaryAttributeType = res.attributes.secondary_attribute_type;
    //         this.apisCalls++;
    //         if(this.apisCalls == 3){
    //             this.loading = false;
    //         }
    //         // this.categories.forEach(category => {   //change the algo
    //         //     this.filters.ccids.forEach(ccid => {
    //         //         if(category.id == parseInt(ccid)){
    //         //             this.userFilters.ccids.push(category.title);
    //         //         }
    //         //     });
    //         // });
    //         // this.brands.forEach(brand => {     //change the algo
    //         //     this.filters.brands.forEach(filterBrand => {
    //         //         if(brand.id == filterBrand){
    //         //             this.userFilters.brands.push(brand.name);
    //         //         }
    //         //     });
    //         // });
    //         // this.primaryAttributeOptions.forEach(option => {   //change the algo
    //         //     this.filters.attrs.forEach(attr => {
    //         //         if(option.id == attr){
    //         //             this.userFilters.attrs.push(option.option_name);
    //         //         }
    //         //     });
    //         // });
    //         // this.secondaryAttributeOptions.forEach(option => {   //change the algo
    //         //     this.filters.attrs.forEach(attr => {
    //         //         if(option.id == attr){
    //         //             this.userFilters.attrs.push(option.option_name);
    //         //         }
    //         //     });
    //         // });
    //     }
    // );
  }

  getWishlists(action?) {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        // token expired
        this.apisCalls++;
        if (this.apisCalls == 5) {
          this.loading = false;
        }
        return;
      } else {
        this.shopService.wishlistIds().subscribe((res) => {
          if(res.success){
            this.wishlists = res.wishlistIds;
            this.route.queryParamMap.subscribe((params) => {
              // var wishlist = params.get("wishlist");
              var wishlist = localStorage.getItem("listingwishlist");
              if (wishlist) {
                var wishlistParsed;
                if ((wishlistParsed = JSON.parse(wishlist))) {
                  this.addToWishlist(wishlistParsed[0], wishlistParsed[1]);
                  localStorage.removeItem("listingwishlist");
                }
              }
            });
          }else{
            this.angularnotifierService.showNotification("error", res.msg);
          }
          this.apisCalls++;
          if (this.apisCalls == 5) {
            this.loading = false;
          }
        });
      }
    } else {
      this.apisCalls++;
      if (this.apisCalls == 5) {
        this.loading = false;
      }
    }
  }

  productvariant(product_variant_id, index, product_id) {
    localStorage.setItem("product_detail_id", product_variant_id);
    if (this.products[index].stock <= 0) {
      this.shopService
        .nextVariantNavigation({ product_id })
        .subscribe((res: any) => {
          if (res.success) {
            const url = "/products/" + res.data.slug + "/" + res.data.unique_id;
            if (this.isMobile) {
              window.open(url, "_self");
            } else {
              window.open(url, "_blank");
            }
          } else {
            const url =
              "/products/" +
              this.products[index].slug +
              "/" +
              this.products[index].unique_id;
            if (this.isMobile) {
              window.open(url, "_self");
            } else {
              window.open(url, "_blank");
            }
          }
        });
    } else {
      const url =
        "/products/" +
        this.products[index].slug +
        "/" +
        this.products[index].unique_id;
      if (this.isMobile) {
        window.open(url, "_self");
      } else {
        window.open(url, "_blank");
      }
    }
  }

  @HostListener("window:scroll", ["$event"])
  onWindowScrollFixed(e) {
    if (window.pageYOffset > 60) {
      this.fixedfilters = true;
    } else {
      this.fixedfilters = false;
    }
  }

  openSortwidget(sortwidget) {
    this.modalService.open(sortwidget, {
      centered: true,
      size: "md",
      animation: true,
      windowClass: "bottom-modal",
    });
  }

  close() {
    this.modalService.dismissAll();
  }

  onSelectSort(value) {
    this.filters.page = 1;
    this.notEmptyProduct = true;
    this.filters.sortBy = value;
    this.activeclass = value;
    this.getListingProducts();
  }

  filterPopup() {
    this.filterclass = !this.filterclass;
  }

  addToWishlist(product_variant_id, product_id, action?) {
    const regexForProducts = /products/g;
    const regexForCats = /category/g;
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        // token expired
        if (this.url.match(regexForProducts)) {
          var urlWithWishlist = this.router.url;
          localStorage.setItem("url", urlWithWishlist);
          localStorage.setItem(
            "listingwishlist",
            JSON.stringify([product_variant_id, product_id])
          );
        } else if (this.url.match(regexForCats)) {
          var urlWithWishlist = this.router.url;
          localStorage.setItem("url", urlWithWishlist);
          localStorage.setItem(
            "listingwishlist",
            JSON.stringify([product_variant_id, product_id])
          );
        }
        this.router.navigate(["login"], {
          queryParams: {
            q: urlWithWishlist,
          },
        });
        return;
      } else {
        this.shopService
          .addToWishlist(product_variant_id, product_id)
          .subscribe({
            next: (res: any) => {
              if (res.success) {
                this.angularnotifierService.showNotification(
                  "default",
                  res.msg
                );
                this.dataService.changeWishlishtCount(res.wishlistCount);
                this.getWishlists(action);
              }
            },
            error: (error) => {
              var err = JSON.stringify(error);
              if (error == "Unauthenticated.") {
                // this.angularnotifierService.showNotification('warning','Please login to add products to wish list!');
              } else {
                this.angularnotifierService.showNotification("warning", err);
              }
            },
          });
      }
    } else {
      if (this.url.match(regexForProducts)) {
        var urlWithWishlist = this.router.url;
        localStorage.setItem("url", urlWithWishlist);
        localStorage.setItem(
          "listingwishlist",
          JSON.stringify([product_variant_id, product_id])
        );
      } else if (this.url.match(regexForCats)) {
        var urlWithWishlist = this.router.url;
        localStorage.setItem("url", urlWithWishlist);
        localStorage.setItem(
          "listingwishlist",
          JSON.stringify([product_variant_id, product_id])
        );
      }
      this.router.navigate(["login"], {
        queryParams: {
          q: urlWithWishlist,
        },
      });
    }

    var animateButton = function (e) {
      e.preventDefault;
      //reset animation
      e.target.classList.remove("animate");

      e.target.classList.add("animate");
      setTimeout(function () {
        e.target.classList.remove("animate");
      }, 700);
    };

    var bubblyButtons = document.getElementsByClassName("bubbly-button");

    for (var i = 0; i < bubblyButtons.length; i++) {
      bubblyButtons[i].addEventListener("click", animateButton, false);
    }
  }

  sliderEvent(view) {
    this.filters.page = 1;
    this.filters.minpr = this.min;
    this.filters.maxpr = this.max;
    if (view == "d") {
      const regexForProducts = /products/g;
      const regexForCats = /category/g;

      if (this.url.match(regexForProducts)) {
        this.route.queryParamMap.subscribe((params) => {
          this.router.navigate(["products"], {
            queryParams: {
              cids: params.get("cids"),
              scids: params.get("scids"),
              ccids: params.get("ccids"),
              brands: params.get("brands"),
              minpr: this.min,
              maxpr: this.max,
              q: params.get("q"),
              tg: params.get("tg"),
              cp: params.get("cp"),
              attrs: params.get("attrs"),
              ediscount: params.get("ediscount"),
              udiscount: params.get("udiscount"),
              discount: params.get("discount"),
            },
          });
        });
      } else if (this.url.match(regexForCats)) {
        var subUrls = this.url.split("/");
        if (subUrls.length == 3) {
          this.router.navigate(["products"], {
            queryParams: {
              cids: JSON.stringify(this.filters.cids),
              minpr: this.min,
              maxpr: this.max,
            },
          });
        } else if (subUrls.length == 4) {
          this.router.navigate(["products"], {
            queryParams: {
              cids: JSON.stringify(this.filters.cids),
              scids: JSON.stringify(this.filters.scids),
              minpr: this.min,
              maxpr: this.max,
            },
          });
        } else if (subUrls.length == 5) {
          this.router.navigate(["products"], {
            queryParams: {
              cids: JSON.stringify(this.filters.cids),
              scids: JSON.stringify(this.filters.scids),
              ccids: JSON.stringify(this.filters.ccids),
              minpr: this.min,
              maxpr: this.max,
            },
          });
        }
      }
    }
  }

  mobileViewPriceFilterChange() {
    this.filters.page = 1;
    this.filters.minpr = this.min;
    this.filters.maxpr = this.max;
  }

  priceFilterSubmit() {
    // this.route.queryParamMap.subscribe(params => {
    //     this.router.navigate(['products'],
    //                             {queryParams:
    //                                 {
    //                                     cids:params.get('cids'),
    //                                     scids:params.get('scids'),
    //                                     ccids:params.get('ccids'),
    //                                     brands:params.get('brands'),
    //                                     minpr:this.minMaxPrices.min,
    //                                     maxpr:this.minMaxPrices.max
    //                                 }
    //                         })
    // })
  }

  getProductsByFilters() {
    const cids = this.filters.cids;
    const scids = this.filters.scids;
    const ccids = this.filters.ccids;
    const brands = this.filters.brands;
    const attrs = this.filters.attrs;
    const minpr = this.filters.minpr;
    const maxpr = this.filters.maxpr;
    const discount = this.filters.discount;
    const q = this.filters.q;
    const tag = this.filters.tag;
    const coupon_group = this.filters.coupon;
    const ediscount = this.filters.ediscount;
    const udiscount = this.filters.udiscount;
    this.filters.page = 1;
    this.router.navigate(["products"], {
      queryParams: {
        cids: cids.length != 0 ? JSON.stringify(cids) : undefined,
        scids: scids.length != 0 ? JSON.stringify(scids) : undefined,
        ccids: ccids.length != 0 ? JSON.stringify(ccids) : undefined,
        brands: brands.length != 0 ? JSON.stringify(brands) : undefined,
        attrs: attrs.length != 0 ? JSON.stringify(attrs) : undefined,
        minpr: minpr != null ? minpr : undefined,
        maxpr: maxpr != null ? maxpr : undefined,
        discount: discount != null ? discount : undefined,
        q: q != null ? q : undefined,
        tg: tag != null ? tag : undefined,
        cp: coupon_group != null ? coupon_group : undefined,
        ediscount: ediscount != null ? q : ediscount,
        udiscount: udiscount != null ? q : udiscount,
      },
    });
    // this.getListingProducts();
  }

  onFilterCategoryChange(e, slug, id, view) {
    if (e.target.checked) {
      // this.userFilters.ccids.push(slug)
      this.filters.ccids.push(parseInt(id));
      // if(view == 'd'){
      //     this.getProductsByFilters();
      // }
    } else {
      const filtersIndex = this.filters.ccids.indexOf(parseInt(id));
      // const userFilterIndex = this.userFilters.ccids.indexOf(slug);
      if (filtersIndex > -1) {
        this.filters.ccids.splice(filtersIndex, 1);
        // if(view == 'd'){
        //     this.getProductsByFilters();
        // }
      }
      // if(userFilterIndex > -1){
      //     this.userFilters.ccids.splice(userFilterIndex, 1);
      // }
    }
  }

  onFilterBrandChange(e, id) {
    if (e.target.checked) {
      this.filters.brands.push(id);
    } else {
      const filtersIndex = this.filters.brands.indexOf(id);
      if (filtersIndex > -1) {
        this.filters.brands.splice(filtersIndex, 1);
      }
    }
  }

  onAttributeFilterChange(e, name, a_o_id, view?) {
    if (e.target.checked) {
      this.filters.attrs.push(a_o_id);
    } else {
      const filtersIndex = this.filters.attrs.indexOf(a_o_id);
      if (filtersIndex > -1) {
        this.filters.attrs.splice(filtersIndex, 1);
      }
    }
  }

  clearAllfilters() {
    this.router.navigate(["products"]);
  }

  changeDiscount(e) {
    const regexForProducts = /products/g;
    const regexForCats = /category/g;

    if (this.url.match(regexForProducts)) {
      this.route.queryParamMap.subscribe((params) => {
        this.router.navigate(["products"], {
          queryParams: {
            cids: params.get("cids"),
            scids: params.get("scids"),
            ccids: params.get("ccids"),
            brands: params.get("brands"),
            attrs: params.get("attrs"),
            minpr: params.get("minpr"),
            maxpr: params.get("maxpr"),
            q: params.get("q"),
            tg: params.get("tg"),
            cp: params.get("cp"),
            ediscount: params.get("ediscount"),
            udiscount: params.get("udiscount"),
            discount: e.target.value,
          },
        });
      });
    } else if (this.url.match(regexForCats)) {
      var subUrls = this.url.split("/");
      if (subUrls.length == 3) {
        this.router.navigate(["products"], {
          queryParams: {
            cids: JSON.stringify(this.filters.cids),
            discount: e.target.value,
          },
        });
      } else if (subUrls.length == 4) {
        this.router.navigate(["products"], {
          queryParams: {
            cids: JSON.stringify(this.filters.cids),
            scids: JSON.stringify(this.filters.scids),
            discount: e.target.value,
          },
        });
      } else if (subUrls.length == 5) {
        this.router.navigate(["products"], {
          queryParams: {
            cids: JSON.stringify(this.filters.cids),
            scids: JSON.stringify(this.filters.scids),
            ccids: JSON.stringify(this.filters.ccids),
            discount: e.target.value,
          },
        });
      }
    }
  }

  changeDiscountMobile(e) {
    this.filters.discount = e.target.value;
  }
  removeFilter(e, id, type) {
    if (type == "cids") {
      this.route.queryParamMap.subscribe((params) => {
        var cids = JSON.parse(params.get("cids"));
        if (cids != null) {
          cids = cids.filter((cid) => {
            return cid != id;
          });
          this.router.navigate(["products"], {
            queryParams: {
              cids: cids.length > 0 ? JSON.stringify(cids) : undefined,
              brands: params.get("brands"),
              attrs: params.get("attrs"),
              minpr: params.get("minpr"),
              maxpr: params.get("maxpr"),
              q: params.get("q"),
              cp: params.get("cp"),
              tg: params.get("tg"),
              discount: params.get("discount"),
              ediscount: params.get("ediscount"),
              udiscount: params.get("udiscount"),
            },
          });
        } else {
          this.router.navigate(["products"]);
          // const regexForCats = /category/g;

          // if(this.url.match(regexForCats)){

          // }
          // this.goback();
        }
      });
    } else if (type == "scids") {
      this.route.queryParamMap.subscribe((params) => {
        var scids = JSON.parse(params.get("scids"));
        if (scids != null) {
          scids = scids.filter((scid) => {
            return scid != id;
          });
          this.router.navigate(["products"], {
            queryParams: {
              cids: params.get("cids"),
              scids: scids.length > 0 ? JSON.stringify(scids) : undefined,
              // ccids:params.get('ccids'),
              brands: params.get("brands"),
              attrs: params.get("attrs"),
              q: params.get("q"),
              tg: params.get("tg"),
              cp: params.get("cp"),
              minpr: params.get("minpr"),
              maxpr: params.get("maxpr"),
              discount: params.get("discount"),
              ediscount: params.get("ediscount"),
              udiscount: params.get("udiscount"),
            },
          });
        } else {
          const regexForCats = /category/g;

          if (this.url.match(regexForCats)) {
            const parentSlug = this.route.snapshot.paramMap.get("slug");
            this.router.navigate(["category", parentSlug]);
          }
          // this.goback();
        }
      });
    } else if (type == "ccids") {
      // if(id == localStorage.getItem('childcategoryid')){
      //     retur    n;
      // }
      this.route.queryParamMap.subscribe((params) => {
        var ccids = JSON.parse(params.get("ccids"));
        if (ccids != null) {
          ccids = ccids.filter((ccid) => {
            return ccid != id;
          });
          this.router.navigate(["products"], {
            queryParams: {
              cids: params.get("cids"),
              scids: params.get("scids"),
              ccids: ccids.length > 0 ? JSON.stringify(ccids) : undefined,
              brands: params.get("brands"),
              attrs: params.get("attrs"),
              q: params.get("q"),
              tg: params.get("tg"),
              cp: params.get("cp"),
              minpr: params.get("minpr"),
              maxpr: params.get("maxpr"),
              discount: params.get("discount"),
              ediscount: params.get("ediscount"),
              udiscount: params.get("udiscount"),
            },
          });
        } else {
          const regexForCats = /category/g;
          if (this.url.match(regexForCats)) {
            const parentSlug = this.route.snapshot.paramMap.get("slug");
            const subSlug = this.route.snapshot.paramMap.get("subslug");
            this.router.navigate(["category", parentSlug, subSlug]);
          }
          // this.goback()
        }
      });
      // this.filters.ccids = this.filters.ccids.filter((ccid)=>{
      //     return ccid != id;
      // })
    } else if (type == "brands") {
      this.route.queryParamMap.subscribe((params) => {
        var brands = JSON.parse(params.get("brands"));
        brands = brands.filter((brand) => {
          return brand != id;
        });
        this.router.navigate(["products"], {
          queryParams: {
            cids: params.get("cids"),
            scids: params.get("scids"),
            ccids: params.get("ccids"),
            q: params.get("q"),
            tg: params.get("tg"),
            cp: params.get("cp"),
            brands: brands.length > 0 ? JSON.stringify(brands) : undefined,
            attrs: params.get("attrs"),
            minpr: params.get("minpr"),
            maxpr: params.get("maxpr"),
            discount: params.get("discount"),
            ediscount: params.get("ediscount"),
            udiscount: params.get("udiscount"),
          },
        });
      });
    } else if (type == "attr") {
      this.route.queryParamMap.subscribe((params) => {
        var attrs = JSON.parse(params.get("attrs"));
        attrs = attrs.filter((attr) => {
          return attr != id;
        });
        this.router.navigate(["products"], {
          queryParams: {
            cids: params.get("cids"),
            scids: params.get("scids"),
            ccids: params.get("ccids"),
            q: params.get("q"),
            tg: params.get("tg"),
            cp: params.get("cp"),
            attrs: attrs.length > 0 ? JSON.stringify(attrs) : undefined,
            brands: params.get("brands"),
            minpr: params.get("minpr"),
            maxpr: params.get("maxpr"),
            discount: params.get("discount"),
            ediscount: params.get("ediscount"),
            udiscount: params.get("udiscount"),
          },
        });
      });
    } else if (type == "price") {
      this.route.queryParamMap.subscribe((params) => {
        this.router.navigate(["products"], {
          queryParams: {
            cids: params.get("cids"),
            scids: params.get("scids"),
            ccids: params.get("ccids"),
            q: params.get("q"),
            tg: params.get("tg"),
            cp: params.get("cp"),
            attrs: params.get("attrs"),
            brands: params.get("brands"),
            discount: params.get("discount"),
            ediscount: params.get("ediscount"),
            udiscount: params.get("udiscount"),
          },
        });
      });
    } else if (type == "discount") {
      this.route.queryParamMap.subscribe((params) => {
        this.router.navigate(["products"], {
          queryParams: {
            cids: params.get("cids"),
            scids: params.get("scids"),
            ccids: params.get("ccids"),
            q: params.get("q"),
            tg: params.get("tg"),
            cp: params.get("cp"),
            attrs: params.get("attrs"),
            minpr: params.get("minpr"),
            maxpr: params.get("maxpr"),
            brands: params.get("brands"),
            ediscount: params.get("ediscount"),
            udiscount: params.get("udiscount"),
          },
        });
      });
    } else if (type == "ediscount") {
      this.route.queryParamMap.subscribe((params) => {
        this.router.navigate(["products"], {
          queryParams: {
            cids: params.get("cids"),
            scids: params.get("scids"),
            ccids: params.get("ccids"),
            q: params.get("q"),
            tg: params.get("tg"),
            cp: params.get("cp"),
            attrs: params.get("attrs"),
            minpr: params.get("minpr"),
            maxpr: params.get("maxpr"),
            brands: params.get("brands"),
            discount: params.get("discount"),
            udiscount: params.get("udiscount"),
          },
        });
      });
    } else if (type == "udiscount") {
      this.route.queryParamMap.subscribe((params) => {
        this.router.navigate(["products"], {
          queryParams: {
            cids: params.get("cids"),
            scids: params.get("scids"),
            ccids: params.get("ccids"),
            q: params.get("q"),
            tg: params.get("tg"),
            cp: params.get("cp"),
            attrs: params.get("attrs"),
            minpr: params.get("minpr"),
            maxpr: params.get("maxpr"),
            brands: params.get("brands"),
            ediscount: params.get("ediscount"),
            discount: params.get("discount"),
          },
        });
      });
    }
    // this.getProductsByFilters();
  }

  addToCart(product_variant_id, i) {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        // token expired
        this.shopService
          .guestAddToCart({
            product_variant_id: product_variant_id,
            quantity: 1,
          })
          .subscribe((res: any) => {
            if (res.success) {
              this.dataService.changeCartCount(res.cartCount);
              this.angularnotifierService.showNotification(
                "default",
                "Product Added to the Cart!"
              );
            } else {
              this.angularnotifierService.showNotification("error", res.msg);
            }
          });
        return;
      } else {
        const cartdata = {
          product_variant_id: product_variant_id,
          quantity: 1,
        };
        this.shopService.addProductToCart(cartdata).subscribe((res: any) => {
          if (res.success) {
            this.dataService.changeCartCount(res.cartCount);
            this.angularnotifierService.showNotification(
              "default",
              "Product Added to the Cart!"
            );
          } else {
              this.angularnotifierService.showNotification("error", res.msg);
          }
        });
      }
    } else {
      this.shopService
        .guestAddToCart({
          product_variant_id: product_variant_id,
          quantity: 1,
        })
        .subscribe((res: any) => {
          if (res.success) {
            this.dataService.changeCartCount(res.cartCount);
            this.angularnotifierService.showNotification(
              "default",
              "Product Added to the Cart!"
            );
          } else {
            this.angularnotifierService.showNotification("error", res.msg);
          }
        });
    }
  }

  onImgError(e) {
    e.target.src = "/assets/images/no-image.jpg";
  }

  // showClearAll(){
  //     console.log(this.filters)
  //     if(this.filters.ccids.length >0 || this.filters.cids.length >0 || this.filters.scids.length > 0 || this.filters.brands.length >0 || this.filters.attrs.length > 0){
  //         return true;
  //     }else{
  //         return false;
  //     }
  // }
}

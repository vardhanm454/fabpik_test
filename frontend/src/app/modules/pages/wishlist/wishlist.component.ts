import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { ShopService } from "src/app/service/shop/shop.service";
import { first } from "rxjs/operators";
import "sweetalert2/src/sweetalert2.scss";
import Swal from "sweetalert2";
import { OwlOptions } from "ngx-owl-carousel-o";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { WINDOW_PROVIDERS } from "ngx-owl-carousel-o/lib/services/window-ref.service";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { DataService } from "src/app/service/data/data.service";

@Component({
  selector: "app-wishlist",
  templateUrl: "./wishlist.component.html",
  styleUrls: ["./wishlist.component.scss"],
  host: {
    "(window:resize)": "onWindowResize($event)",
  },
})
export class WishlistComponent implements OnInit {
  isMobile: boolean = false;
  isDesktop: boolean = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth: number = 767;

  AllWishlist: any = [];
  cartcount: any;
  cartdata: any;
  cartCount = 0;
  count: any;
  products: any;
  wishlistcount: any = 0;
  apiCalls: any = 0;
  public headername: string = "Wishlist";
  private productlistjsonURL = "assets/js/products.json";
  loading: boolean = true;
  imgUrl: any;

  constructor(
    private _location: Location,
    private http: HttpClient,
    private shopService: ShopService,
    private router: Router,
    private angularnotifierService: AngularnotifierService,
    private authService: AuthenticationService,
    public dataService: DataService
  ) {
    const body = document.getElementsByTagName("body")[0];
    body.classList.add("bgcategory-light");
  }
  
  loadWishlistScript() {
    let node = document.createElement("script"); // creates the script tag
    node.text = `fbq('track', 'AddToWishlist')`;
    // append to head of document
    document.getElementsByTagName("head")[0].appendChild(node);
  }
  ngOnInit(): void {
   
    this.dataService.currentWishlistCount.subscribe((res) => {
      this.wishlistcount = res;
    });
    if (window.location.href.indexOf("https://fabpik.in/") > -1) {
      this.loadWishlistScript();
    
    }
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;

    var values = JSON.parse(localStorage.getItem("customer"));
    if (values) {
      if (this.authService.tokenExpired(values.access_token)) {
        this.router.navigate(["login"]);
        this.loading = false;
      } else {
        this.getWishlist();
        this.getCartCount();
        this.getWishlistCount();
      }
    } else {
      this.router.navigate(["login"]);
      this.loading = false;
    }
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }
  ngOnDestroy(): void {
    const body = document.getElementsByTagName("body")[0];
    body.classList.remove("bgcategory-light");
  }
  goback() {
    this._location.back();
  }

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    autoplay: true,
    autoplayHoverPause: true,
    navSpeed: 400,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
    nav: false,
  };

  //get wishlist count
  getWishlistCount() {
    if (localStorage.getItem("customer") !== null) {
      this.shopService.getWishlistCount().subscribe((res:any) => {
        this.apiCalls++;
        if (this.apiCalls == 3) {
          this.loading = false;
        }
        if(res.success){
          this.dataService.changeWishlishtCount(res.count);
        }else{
          this.angularnotifierService.showNotification("error", res.msg);
        }
        
      });
    } else {
      this.apiCalls++;
      if (this.apiCalls == 3) {
        this.loading = false;
      }
    }
  }

  //get all wishlist product
  getWishlist() {
    if (localStorage.getItem("customer") !== null) {
      this.shopService.getWishlist().subscribe((res) => {
        this.apiCalls++;
        if (this.apiCalls == 3) {
          this.loading = false;
        }
        if(res.success){
          this.AllWishlist = res.products;
          this.imgUrl = res.imgUrl;
        }else{
          this.angularnotifierService.showNotification("error", res.msg);
        }
        this.loading = false;
      });
    } else {
      this.apiCalls++;
      if (this.apiCalls == 3) {
        this.loading = false;
      }
    }
  }

  productvariant(product_variant_id, index, product_id) {
    localStorage.setItem("product_detail_id", product_variant_id);
    if (this.AllWishlist[index].stock <= 0) {
      this.shopService
        .nextVariantNavigation({ product_id })
        .subscribe((res: any) => {
          if (res.success) {
            const url = "/products/" + res.data.slug + "/" + res.data.unique_id;
            if (this.isMobile) {
              window.open(url, "_self");
            } else {
              window.open(url, "_blank");
            }
          } else {
            const url =
              "/products/" +
              this.AllWishlist[index].slug +
              "/" +
              this.AllWishlist[index].unique_id;
            if (this.isMobile) {
              window.open(url, "_self");
            } else {
              window.open(url, "_blank");
            }
          }
        });
    } else {
      const url =
        "/products/" +
        this.AllWishlist[index].slug +
        "/" +
        this.AllWishlist[index].unique_id;
      if (this.isMobile) {
        window.open(url, "_self");
      } else {
        window.open(url, "_blank");
      }
    }
  }

  //remove product from wishlist
  removeWishlist(id, from) {
    if (from == 1) {
      Swal.fire({
        title: "Are you sure you want to delete this product from wishlist?",
        icon: "warning",
        showClass: {
          popup: "swal-wish-wide",
        },
        cancelButtonColor: "#e42b5a08",
        confirmButtonColor: "#e42b5abf",
        showCloseButton: true,
        showCancelButton: true,
      }).then((check) => {
        if (check.value) {
          this.shopService.removewishlist(id).subscribe((res: any) => {
            if (res.success) {
              this.dataService.changeWishlishtCount(res.wishlistCount);
              this.AllWishlist = this.AllWishlist.filter(
                (product) => product.product_variant_id != id
              );
              Swal.fire({
                title: "Yes deleted!",
                text: "Product Removed from Wishlist.",
                icon: "success",

                confirmButtonColor: "#e42b5abf",
                confirmButtonText: "Okay",

                showClass: {
                  popup: "swal-wish-wide animated fadeInDown faster",
                },
              });
            } else {
              Swal.fire({
                title: "Failed!",
                text: "Failed to remove from wishlist.",
                icon: "warning",

                confirmButtonColor: "#e42b5abf",
                confirmButtonText: "Okay",

                showClass: {
                  popup: "swal-wish-wide animated fadeInDown faster",
                },
              });
            }
          });
        } else {
          return;
        }
      });
    } else {
      this.shopService.removewishlist(id).subscribe((res: any) => {
        if (res.success) {
          this.AllWishlist = this.AllWishlist.filter(
            (product) => product.product_variant_id != id
          );
          this.dataService.changeWishlishtCount(res.wishlistCount);
        } else {
          return;
        }
      });
    }
  }

  //add to cart
  addProductToCart(id) {
    this.cartCount += 1;
    this.cartdata = {
      product_variant_id: id,
      quantity: this.cartCount,
    };
    this.shopService.addProductToCart(this.cartdata).subscribe({
      next: (res: any) => {
        // this.addedClass = !this.addedClass;
        if (res.success) {
          this.dataService.changeCartCount(res.cartCount);
          // this.alertService.success('Product Added to the cart', 'success');
          this.angularnotifierService.showNotification(
            "default",
            "Product Added to the cart"
          );
          this.removeWishlist(id, 2);
          // this.router.navigate(['/wishlist']);
        } else {
          // this.alertService.warning("No Stock!", res.msg)
          this.angularnotifierService.showNotification("error", res.msg);
        }
      },
      error: (error) => {
        var err = JSON.stringify(error);
        // this.alertService.error(err, 'error');
        this.angularnotifierService.showNotification("error", err);
      },
    });
  }

  //get cart count
  getCartCount() {
    if (localStorage.getItem("customer") !== null) {
      this.shopService.getCartCount().subscribe((res:any) => {
        this.apiCalls++;
        if (this.apiCalls == 3) {
            this.loading = false;
        }
        if(res.success){
          this.cartcount = res.count;
        }else{
          this.angularnotifierService.showNotification(
            "error",
            res.msg
          );
        }
      });
    }
  }
}

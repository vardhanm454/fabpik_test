import { Component, HostListener, OnDestroy, OnInit } from "@angular/core";
import { OwlOptions } from "ngx-owl-carousel-o";
declare var $: any;
import { Location } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { ShopService } from "src/app/service/shop/shop.service";
import { Router } from "@angular/router";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { SearchService } from "src/app/service/search/search.service";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"],
})
export class SearchComponent implements OnInit, OnDestroy {
  searchform: FormGroup;
  keyword = "name";
  searchText: string;
  isCategory = 0;
  categories: any;
  productData: any;
  listing: any;
  productlength: any;
  categorylength: any;
  errormsg: any;
  wishlist: any = [];
  popularCategory: any;
  fixed: boolean = false;
  categorySlug: any;
  subCategorySlug: any;
  brands: any = [];
  topSellingProducts: any = [];
  searchCategories: any = [];
  searchProducts: any = [];
  response = "assets/js/search.json";
  emptySearch: boolean;
  autoSuggestion: any = null;
  searchLoading: boolean = false;
  imgUrl: any;
  searchValue: any = "";
  tags: any = [];

  constructor(
    private _location: Location,
    private shopService: ShopService,
    private searchService: SearchService,
    public authService: AuthenticationService,
    private router: Router,
    private formBuilder: FormBuilder,
    public angularnotifierService:AngularnotifierService
  ) {
    const body = document.getElementsByTagName("body")[0];
    body.classList.add("search");
  }
  goback() {
    this._location.back();
  }

  ngOnDestroy(): void {
    const body = document.getElementsByTagName("body")[0];
    body.classList.remove("search");
  }

  RecommendOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    autoplay: true,
    dots: false,
    nav: false,
    navSpeed: 700,
    margin: 10,
    navText: ["", ""],
    responsive: {
      0: {
        items: 2,
      },
      400: {
        items: 2,
      },
      740: {
        items: 2,
      },
      940: {
        items: 2,
      },
    },
  };
  @HostListener("window:scroll", ["$event"])
  onWindowScroll(e) {
    if (window.pageYOffset > 100) {
      this.fixed = true;
    } else {
      this.fixed = false;
    }
  }

  loadSearchScript() {
    let node = document.createElement("script"); // creates the script tag
    node.text = `fbq('track', 'Search')`;
    // append to head of document
    document.getElementsByTagName("head")[0].appendChild(node);
  }
  ngOnInit(): void {
    if (window.location.href.indexOf("https://fabpik.in/") > -1) {
      this.loadSearchScript();
    }
    this.searchform = this.formBuilder.group({
      name: [""],
    });
    this.getPopularCategory();
    this.getWishlist();
  }

  myControl = new FormControl();

  onSearchKey(evt) {
    if (evt.keyCode == 13) {
      this.router.navigate(["products"], {
        queryParams: {
          q: evt.target.value,
        },
      });
    }
    this.listing = {
      name: evt.target.value,
    };
    this.searchValue = evt.target.value;
    if (evt.target.value.length >= 3) {
      this.searchLoading = true;
      this.searchCategories = [];
      this.searchProducts = [];
      this.brands = [];
      this.tags = [];
      if (this.autoSuggestion) {
        this.autoSuggestion.unsubscribe();
        this.getSearchResults(this.listing);
      } else {
        this.getSearchResults(this.listing);
      }
    } else {
      this.searchCategories = [];
      this.searchProducts = [];
      this.brands = [];
      this.tags = [];
    }
  }

  getSearchResults(data) {
    this.autoSuggestion = this.shopService
      .searchautosuggestion(data)
      .subscribe((res: any) => {
        if(res.success){
          this.imgUrl = res.imgUrl;
          this.searchLoading = false;
          if (res.categories.length == 0 && res.products.length == 0) {
            this.errormsg = "NOT Found";
            this.searchCategories = res.categories;
            this.searchProducts = res.products;
            this.brands = res.brands;
            this.tags = res.tags;
            this.emptySearch = this.searchCategories.length == 0 ? true : false;
          } else {
            this.searchCategories = res.categories;
            this.brands = res.brands;
            this.tags = res.tags;
            this.searchProducts = res.products;
          }
        }else{
          this.angularnotifierService.showNotification("error", res.msg);
        }
      });
  }

  // getSubSubCategory(id) {
  //   this.shopService.getSubChildCategorymobile(id)
  //     .subscribe(res => {
  //       console.log(res)
  //       this.loading = false;
  //       res.category.forEach(category => {
  //         this.categorySlug = category.slug;
  //       });
  //       this.subCategorySlug = res.subCategory.slug;
  //     });
  // }

  onImgError(e) {
    e.target.src = "/assets/images/no-image.jpg";
  }

  selectEvent(item, index?, type?) {
    if (item.type == "product") {
      localStorage.setItem("product_detail_id", item.id);
      if (this.searchProducts[index].stock <= 0) {
        this.shopService
          .nextVariantNavigation({
            product_id: this.searchProducts[index].product_id,
          })
          .subscribe((res: any) => {
            if (res.success) {
              this.router.navigate([
                "/products/" + res.data.slug + "/" + res.data.unique_id,
              ]);
            } else {
              this.router.navigate([
                "/products/" +
                  this.searchProducts[index].slug +
                  "/" +
                  this.searchProducts[index].unique_id,
              ]);
            }
          });
      } else {
        this.router.navigate([
          "/products/" +
            this.searchProducts[index].slug +
            "/" +
            this.searchProducts[index].unique_id,
        ]);
      }
    } else if (type == "brand") {
      this.router.navigate(["products"], {
        queryParams: {
          brands: JSON.stringify([item.id]),
        },
      });
    } else if (type == "tags") {
      this.router.navigate(["products"], {
        queryParams: {
          tg: item.id,
        },
      });
    } else {
      localStorage.setItem("childcategoryid", item.child_id);
      localStorage.setItem("childcategoryslug", item.child_slug);
      localStorage.setItem("catid", item.category_id);
      localStorage.setItem("catslug", item.cat_slug);
      localStorage.setItem("subid", item.subcategory_id);
      localStorage.setItem("subslug", item.subcat_slug);
      localStorage.setItem("filterbasedId", "childcategory");
      this.router.navigate([
        "/category/" +
          item.cat_slug +
          "/" +
          item.subcat_slug +
          "/" +
          item.child_slug,
      ]);
      // this.shopService.getCatSubSlug(item.id)
      // .subscribe(res => {
      //   res.category.forEach(category => {
      //     this.categorySlug = category.slug;
      //   });
      //   console.log( this.categorySlug)
      //   res.subCategory.forEach(subCategory => {
      //     this.subCategorySlug = subCategory.slug;
      //   });
      //   this.router.navigate(['/category/'+ this.categorySlug + '/' + this.subCategorySlug + '/' + item.slug]);
      // });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    localStorage.setItem("childslug", this.searchform.value.name);
    localStorage.setItem("product_name", this.searchform.value.name);
    // this.router.navigate(['/shop/listing/'+ this.searchform.value.name]);
    //  this.router.navigate(['/category/'+ this.categorySlug + '/' + this.subCategorySlug + '/' + item.slug]);
  }

  //get all wishlist product
  getWishlist() {
    if (localStorage.getItem("customer") !== null) {
      var values = JSON.parse(localStorage.getItem("customer"));
      if (this.authService.tokenExpired(values.access_token)) {
        this.shopService.topSellingProducts().subscribe((res: any) => {
          this.topSellingProducts = res.products;
          this.imgUrl = res.imgUrl;
        });
      } else {
        this.shopService.getWishlist().subscribe((res) => {
          if (res.length == 0) {
            this.shopService.topSellingProducts().subscribe((res: any) => {
              this.topSellingProducts = res.products;
              this.imgUrl = res.imgUrl;
            });
          }
          this.wishlist = res.products;
          this.imgUrl = res.imgUrl;
        });
      }
    } else {
      this.shopService.topSellingProducts().subscribe((res: any) => {
        this.topSellingProducts = res.products;
        this.imgUrl = res.imgUrl;
      });
    }
  }

  //popolar category
  getPopularCategory() {
    this.searchService.getPopularCategory().subscribe((res:any) => {
      if(res.success){
        this.popularCategory = res.categories;
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
    });
  }

  gotoProductList(category) {
    localStorage.setItem("childcategoryid", category.id);
    localStorage.setItem("childcategoryslug", category.slug);
    localStorage.setItem("catid", category.category_id);
    localStorage.setItem("catslug", category.cat_slug);
    localStorage.setItem("subid", category.subcategory_id);
    localStorage.setItem("subslug", category.subcat_slug);
    localStorage.setItem("filterbasedId", "childcategory");
    this.router.navigate([
      "/category/" +
        category.cat_slug +
        "/" +
        category.subcat_slug +
        "/" +
        category.slug,
    ]);
  }

  productvariantwishlist(product_variant_id, index, product_id) {
    localStorage.setItem("product_detail_id", product_variant_id);
    if (this.wishlist[index].stock <= 0) {
      this.shopService
        .nextVariantNavigation({ product_id })
        .subscribe((res: any) => {
          if (res.success) {
            const url = "/products/" + res.data.slug + "/" + res.data.unique_id;
            window.open(url, "_self");
          } else {
            const url =
              "/products/" +
              this.wishlist[index].slug +
              "/" +
              this.wishlist[index].unique_id;
            window.open(url, "_self");
          }
        });
    } else {
      const url =
        "/products/" +
        this.wishlist[index].slug +
        "/" +
        this.wishlist[index].unique_id;
      window.open(url, "_self");
    }
  }

  productvarianttopselling(product_variant_id, index, product_id) {
    if (this.topSellingProducts[index].stock <= 0) {
      this.shopService
        .nextVariantNavigation({ product_id })
        .subscribe((res: any) => {
          if (res.success) {
            const url = "/products/" + res.data.slug + "/" + res.data.unique_id;
            window.open(url, "_self");
          } else {
            const url =
              "/products/" +
              this.topSellingProducts[index].slug +
              "/" +
              this.topSellingProducts[index].unique_id;
            window.open(url, "_self");
          }
        });
    } else {
      const url =
        "/products/" +
        this.topSellingProducts[index].slug +
        "/" +
        this.topSellingProducts[index].unique_id;
      window.open(url, "_self");
    }
  }
}

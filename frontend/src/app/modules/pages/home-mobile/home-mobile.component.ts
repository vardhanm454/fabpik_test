import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { OwlOptions } from "ngx-owl-carousel-o";
import { ShopService } from "src/app/service/shop/shop.service";
import { Router } from "@angular/router";
import { DataService } from "src/app/service/data/data.service";
import { ProgressSpinnerMode } from "@angular/material";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";

@Component({
  selector: "app-home-mobile",
  templateUrl: "./home-mobile.component.html",
  styleUrls: ["./home-mobile.component.scss"],
})
export class HomeMobileComponent implements OnInit {
  categories: any;
  flashdeals: any;
  loading: boolean = true;
  listing: any;
  upcomingDeals: any = [];
  mode: ProgressSpinnerMode = "determinate";
  public sliders = [];
  deals: any = [];
  public offersliders = [
    { image: "assets/images/home/slider1.jpg" },
    { image: "assets/images/home/slider2-mobile.jpg" },
    { image: "assets/images/home/slider3-mobile.jpg" },
    { image: "assets/images/home/slider4-mobile.jpg" },
    { image: "assets/images/home/slider5-mobile.jpg" },
  ];
  // prettyConfig: CountdownConfig[];

  public prices = [
    { price: "99" },
    { price: "299" },
    { price: "599" },
    { price: "799" },
  ];
  imgUrl: any;
  priceFilters(min, max) {
    this.router.navigate(["products"], {
      queryParams: {
        minpr: min,
        maxpr: max,
      },
    });
  }
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    autoplay: true,
    navSpeed: 700,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
        dots: false,
      },
      400: {
        items: 1,
        dots: false,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
    nav: false,
  };

  slideConfig = {
    arrows: true,
    dots: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    centerPadding: "20px",
    draggable: false,
    nextArrow:
      '<button class="btn-next" (click)="next()"><i class="far fa-angle-right"></i></button>\n',
    prevArrow:
      '<button class="btn-prev" (click)="prev()"><i class="far fa-angle-left"></i></button>\n',
    infinite: false,
    pauseOnHover: true,
    swipe: true,
    touchMove: true,
    vertical: false,
    autoplay: true,
    autoplaySpeed: 3000,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
    ],
  };

  constructor(
    private _location: Location,
    private shopService: ShopService,
    private router: Router,
    public dataService: DataService,
    public angularnotifierService:AngularnotifierService
  ) {}

  goback() {
    this._location.back();
  }

  scrollToElement(el: HTMLElement, targetUrl) {
    if (targetUrl == "https://fabpik.in/deals") {
      el.scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "nearest",
      });
    } else {
      window.open(targetUrl, "_self");
    }
  }

  public features = [
    {
      image: "assets/images/icons/guarantee-gray.png",
      name: "Verified Products",
    },
    {
      image: "assets/images/icons/securepayments-gray.png",
      name: "Secured Payments",
    },
    {
      image: "assets/images/icons/returnproduct-gray.png",
      name: "Easy Returns",
    },
    {
      image: "assets/images/icons/bestprice-gray.png",
      name: "Best Price Guaranteed",
    },
  ];

  public offers = [{ offer: "30" }, { offer: "40" }, { offer: "50" }];
  dealsConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    centerPadding: "20px",
    draggable: true,
    infinite: true,
    pauseOnHover: true,
    swipe: true,
    touchMove: true,
    autoplay: true,
    adaptiveHeight: true,
    autoplaySpeed: 3000,
    dots: false,
    // centerMode: true,
    focusOnSelect: true,
    vertical: false,
    nextArrow:
      '<button class="btn-next"  id="btn-mainn" (click)="next()"><i class="far fa-angle-right"></i></button>\n',
    prevArrow:
      '<button class="btn-prev" (click)="prev()"><i class="far fa-angle-left"></i></button>\n',
    responsive: [
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
    ],
  };
  public girlsclothing = [
    {
      image: "assets/images/category/girlsclothing.png",
      name: "Verified Products",
    },
    {
      image: "assets/images/category/girlsclothing1.png",
      name: "Secured Payments",
    },
    {
      image: "assets/images/category/girlsclothing2.png",
      name: "Exchange Products",
    },
  ];

  public boysclothing = [
    { image: "assets/images/category/boys.png", name: "Verified Products" },
    { image: "assets/images/category/boys1.png", name: "Secured Payments" },
    { image: "assets/images/category/boys2.png", name: "Exchange Products" },
  ];

  public toys = [
    { image: "assets/images/category/toys.png", name: "Verified Products" },
    { image: "assets/images/category/toys1.png", name: "Secured Payments" },
    { image: "assets/images/category/toys2.png", name: "Exchange Products" },
  ];

  public supersaver = (discount) => {
    this.router.navigate(["/products"], {
      queryParams: { ediscount: discount },
    });
  };

  ngOnInit() {
    this.getCategory();
    this.getFlashDealProduct();
    this.shopService.sliderImages("m").subscribe((res: any) => {
      if(res.success){
        this.sliders = res.images;
        this.imgUrl = res.imgUrl;
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
    this.shopService.deals().subscribe((res: any) => {
      if(res.success){
        this.deals = res.products;
        this.upcomingDeals = res.upcomingDeals;
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
  }

  bannerClick(url) {
    window.open(url, "_self");
  }

  onImgError(e) {
    e.target.src = "/assets/images/no-image.jpg";
  }

  // get category list
  getCategory() {
    this.dataService.currentCategories.subscribe((res) => {
      if (res != null) {
        this.loading = false;
        this.categories = res;
      }
    });
  }

  //get flash deal product
  getFlashDealProduct() {
    this.shopService.getFlashDealProduct().subscribe((res: any) => {
      if(res.success){
        this.flashdeals = res.products;
        this.imgUrl = res.imgUrl;
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
  }

  categoryid(id, slug, type) {
    localStorage.setItem("catid", id);
    localStorage.setItem("catslug", slug);
    if (type == "static") {
      this.router.navigate(["/products"], { queryParams: { q: slug } });
    } else {
      this.router.navigate(["/category", slug]);
    }
  }

  productvariant(product_variant_id, index, product_id) {
    if (this.flashdeals[index].stock <= 0) {
      this.shopService
        .nextVariantNavigation({ product_id })
        .subscribe((res: any) => {
          if (res.success) {
            const url = "/products/" + res.data.slug + "/" + res.data.unique_id;
            window.open(url, "_self");
          } else {
            const url =
              "/products/" +
              this.flashdeals[index].slug +
              "/" +
              this.flashdeals[index].unique_id;
            window.open(url, "_self");
          }
        });
    } else {
      const url =
        "/products/" +
        this.flashdeals[index].slug +
        "/" +
        this.flashdeals[index].unique_id;
      window.open(url, "_self");
    }
  }

  girlClothing() {
    this.router.navigate(["products"], {
      queryParams: {
        cids: JSON.stringify([1]),
        scids: JSON.stringify([2]),
      },
    });
  }

  boyClothing() {
    this.router.navigate(["products"], {
      queryParams: {
        cids: JSON.stringify([1]),
        scids: JSON.stringify([1]),
      },
    });
  }

  toyslink() {
    this.router.navigate(["products"], {
      queryParams: {
        cids: JSON.stringify([3]),
      },
    });
  }

  finishTest() {
    this.shopService.deals().subscribe((res: any) => {
      if(res.success){
        this.deals = res.products;
        this.upcomingDeals = res.upcomingDeals;
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
  }
}

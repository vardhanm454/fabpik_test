import { Component, HostListener, OnInit } from '@angular/core';
declare var $: any;
import { Location } from '@angular/common';
import { OwlOptions } from 'ngx-owl-carousel-o';
@Component({
  selector: 'app-coupons',
  templateUrl: './coupons.component.html',
  styleUrls: ['./coupons.component.scss']
})
export class CouponsComponent implements OnInit {
  fixed: boolean = false;
  constructor(private _location: Location) { 

  }
  goback() {
    this._location.back();
  }
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
     if (window.pageYOffset > 100) {
         this.fixed = true;
     } else {
        this.fixed = false;
     }
  }
  ngOnInit(): void {
  }
  customOptions: OwlOptions = {
    loop: true,
    autoplay: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    nav: false,
    autoHeight: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
  }
}

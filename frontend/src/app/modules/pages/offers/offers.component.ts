import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { OwlOptions } from "ngx-owl-carousel-o";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { ShopService } from "src/app/service/shop/shop.service";
import { CommonapiService } from "src/app/service/commonapi/commonapi.service";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
@Component({
  selector: "app-offers",
  templateUrl: "./offers.component.html",
  styleUrls: ["./offers.component.scss"],
})
export class OffersComponent implements OnInit {
  public headername = "Offers";
  offers: any = [];
  totalMrp: any = 0;
  totalSellingPrice: any = 0;
  totalOrderAmount: any = 0;
  cart: any = [];
  handlingCharges: any = 0;
  loadingVirtualCart: boolean = false;
  policies: any;
  errorMsg: any = null;
  error: boolean = false;
  constructor(
    public commonapiService: CommonapiService,
    public shopService: ShopService,
    private router: Router,
    public authService: AuthenticationService,
    public modalService: NgbModal,
    private angularnotifierService: AngularnotifierService
  ) {}

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    autoplay: true,
    navSpeed: 700,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
    nav: false,
  };

  RecommendOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    autoplay: true,
    dots: false,
    nav: false,
    navSpeed: 700,
    margin: 5,
    navText: ["", ""],
    responsive: {
      0: {
        items: 4,
      },
      400: {
        items: 4,
      },
      740: {
        items: 4,
      },
      940: {
        items: 4,
      },
    },
  };

  copyMessage(val: string) {
    const selBox = document.createElement("textarea");
    selBox.style.position = "fixed";
    selBox.style.left = "0";
    selBox.style.top = "0";
    selBox.style.opacity = "0";
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand("copy");
    document.body.removeChild(selBox);
    this.angularnotifierService.showNotification(
      "default",
      "Coupon Code Copied!"
    );
  }
  getPolicy(data) {
    this.commonapiService.getPolicies(data).subscribe((res: any) => {
      if(res.success){
        this.policies = res.policies;
        this.router.navigate(["policies", data]);
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
  }
  ngOnInit() {
    this.shopService.getOffers().subscribe((res: any) => {
      if(res.success){
        this.offers = res.offers;
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
  }

  onImgError(e) {
    e.target.src = "/assets/images/no-image.jpg";
  }

  openSizechart(sizechart) {
    this.modalService.open(sizechart, {
      centered: true,
      size: "md",
      animation: true,
      windowClass: "bottom-modal",
    });
  }
  closeModal() {
    this.modalService.dismissAll();
  }
  getVirtualCart(id) {
    if (localStorage.getItem("customer") !== null) {
      var token = JSON.parse(localStorage.getItem("customer"));
      var tokenExpiry = this.authService.tokenExpired(token.access_token);
      if (tokenExpiry) {
        this.loadingVirtualCart = true;
        this.shopService
          .guestVirtualCart({ cp_id: id })
          .subscribe((res: any) => {
            if (res.success) {
              this.totalMrp = res.totalMrp;
              this.loadingVirtualCart = false;
              this.totalSellingPrice = res.totalSellingPrice;
              this.totalOrderAmount = res.totalOrderAmount;
              this.handlingCharges = res.handling_charges;
              this.cart = res.cart;
            } else {
              this.loadingVirtualCart = false;
              this.error = true;
              this.errorMsg = res.msg;
            }
          });
      } else {
        this.loadingVirtualCart = true;
        this.shopService.getVirtualCart({ cp_id: id }).subscribe((res: any) => {
          if (res.success) {
            this.totalMrp = res.totalMrp;
            this.loadingVirtualCart = false;
            this.totalSellingPrice = res.totalSellingPrice;
            this.totalOrderAmount = res.totalOrderAmount;
            this.handlingCharges = res.handling_charges;
            this.cart = res.cart;
          } else {
            this.loadingVirtualCart = false;
            this.error = true;
            this.errorMsg = res.msg;
          }
        });
      }
    } else {
      this.loadingVirtualCart = true;
      this.shopService.guestVirtualCart({ cp_id: id }).subscribe((res: any) => {
        if (res.success) {
          this.totalMrp = res.totalMrp;
          this.loadingVirtualCart = false;
          this.totalSellingPrice = res.totalSellingPrice;
          this.totalOrderAmount = res.totalOrderAmount;
          this.handlingCharges = res.handling_charges;
          this.cart = res.cart;
        } else {
          this.loadingVirtualCart = false;
          this.error = true;
          this.errorMsg = res.msg;
        }
      });
    }
  }

  clothingStarts99() {
    this.router.navigate(["products"], {
      queryParams: {
        cids: JSON.stringify([1]),
      },
    });
  }

  toys() {
    this.router.navigate(["products"], {
      queryParams: {
        cids: JSON.stringify([3]),
      },
    });
  }

  personalCare() {
    this.router.navigate(["products"], {
      queryParams: {
        cids: JSON.stringify([7]),
      },
    });
  }

  accessories() {
    this.router.navigate(["products"], {
      queryParams: {
        cids: JSON.stringify([10]),
      },
    });
  }
}

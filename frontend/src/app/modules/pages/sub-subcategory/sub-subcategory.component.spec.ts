import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSubcategoryComponent } from './sub-subcategory.component';

describe('SubSubcategoryComponent', () => {
  let component: SubSubcategoryComponent;
  let fixture: ComponentFixture<SubSubcategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubSubcategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSubcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

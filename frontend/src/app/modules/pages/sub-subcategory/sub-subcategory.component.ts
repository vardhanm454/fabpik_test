import { Component, HostListener, OnInit } from "@angular/core";
import { OwlOptions } from "ngx-owl-carousel-o";
import { Location } from "@angular/common";
import { ActivatedRoute, Router } from "@angular/router";
import { first } from "rxjs/operators";

import { ShopService } from "src/app/service/shop/shop.service";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";

@Component({
  selector: "app-sub-subcategory",
  templateUrl: "./sub-subcategory.component.html",
  styleUrls: ["./sub-subcategory.component.scss"],
})
export class SubSubcategoryComponent implements OnInit {
  loading: boolean = true;
  headername: string;
  categorySlug: any;
  subCategoryTitle: any;
  subCategorySlug: any;
  childCategories: any;
  loadinglength = [1, 2, 3, 4, 5, 6];
  products: any;
  images: any;
  scrHeight: any;
  scrWidth: any;
  hide: Boolean = false;
  constructor(
    private _location: Location,
    private route: ActivatedRoute,
    private shopService: ShopService,
    private router: Router,
    public angularnotifierService:AngularnotifierService
  ) {
    this.getScreenSize();
  }

  @HostListener("window:resize", ["$event"])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    if (this.scrWidth < 760) {
      this.hide = true;
    } else {
      this.hide = false;
    }
  }

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    nav: false,
    navSpeed: 700,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 2,
      },
      740: {
        items: 3,
      },
      940: {
        items: 4,
      },
    },
  };

  ngOnInit(): void {
    this.categorySlug = localStorage.getItem("catslug");
    this.getSubSubCategory(localStorage.getItem("subid"));
    this.shopService.getData().subscribe((products) => {
      this.products = products;
      const slug = this.route.snapshot.paramMap.get("slug");
      var w = this.products.results.filter((item) => item.slug === slug);
      this.images = w[0].images;
    });
  }

  getSubSubCategory(id) {
    this.loading = true;
    this.shopService.getSubChildCategorymobile(id).subscribe((res:any) => {
      this.loading = false;
      if(res.success){
        res.categories.category.forEach((category) => {
          this.categorySlug = category.slug;
        });
        this.subCategoryTitle = res.categories.subCategory.title;
        this.subCategorySlug = res.categories.subCategory.slug;
        this.childCategories = res.categories.childCategories;
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
  }

  bannerClick(link){
    window.open(link, "_self");
  }

  childcatid(id, categoryslug, subcategoryslug, childcategoryslug) {
    // localStorage.setItem('childid', id);
    // localStorage.setItem('childslug', slug);
    // this.router.navigate(['/category'], { queryParams: { val: [1,2,3] }});
    // const ccids = [id];
    // // const cids = JSON.parse(this.route.snapshot.params.cids == undefined ? this.route.snapshot.params.cids : {});
    // var cids:any = this.route.snapshot.params.cids
    // if(cids != undefined){
    //   JSON.parse(this.route.snapshot.params.cids);
    // }else{
    //   cids = []
    // }
    // console.log(cids)
    // this.router.navigate(['products'],
    // {queryParams:
    //     {
    //         ccids:ccids.length != 0 ?JSON.stringify (ccids) : undefined,
    //         cids:cids.length != 0 ?JSON.stringify (cids) : undefined,
    //     }
    // })
    localStorage.setItem("childcategoryid", id);
    localStorage.setItem("childcategoryslug", childcategoryslug);
    localStorage.setItem("filterbasedId", "childcategory");
  }
  productListRoute(ccid) {
    const arr: any = [ccid];

    return "/products?ccid=" + JSON.parse(arr);
    return ["/products"];
  }
  goback() {
    this._location.back();
  }
}

import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { AngularFireMessaging } from "@angular/fire/messaging";

@Component({
  selector: "app-login-verify",
  templateUrl: "./login-verify.component.html",
  styleUrls: ["./login-verify.component.scss"],
})
export class LoginVerifyComponent implements OnInit {
  mobileNumber: any;
  email: any;
  otpverifyform: FormGroup;
  type: any;
  loading: Boolean = true;
  emailLength: any;
  fcm_token: any = null;
  resendloader: boolean = false;
  resendloader1: boolean = true;
  constructor(
    private _location: Location,
    public authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private angularnotifierService: AngularnotifierService,
    public router: Router,
    private route: ActivatedRoute,
    private afMessaging: AngularFireMessaging
  ) {
    this.otpverifyform = this.formBuilder.group({
      otp1: ["", [Validators.required, Validators.minLength(1)]],
      otp2: ["", [Validators.required, Validators.minLength(1)]],
      otp3: ["", [Validators.required, Validators.minLength(1)]],
      otp4: ["", [Validators.required, Validators.minLength(1)]],
    });
  }

  goback() {
    this._location.back();
  }

  move(backOtp, fromOtp, toOtp, keycode) {
    if (keycode == 8) {
      var length = fromOtp.length;
      var maxlength = fromOtp.getAttribute(maxlength);
      backOtp.focus();
    } else {
      var length = fromOtp.length;
      var maxlength = fromOtp.getAttribute(maxlength);
      if (length == maxlength) {
        toOtp.focus();
      }
    }
  }

  ngOnInit(): void {
    var user = localStorage.getItem("user");
    this.authService.verifyOtpLoginDetails({ user }).subscribe((res: any) => {
      if (res.success) {
        this.type = res.type;
        this.mobileNumber = res.user.mobile;
        this.email = res.user.email;
        this.emailLength = res.user.email.length;
      } else {
        this.goback();
      }
      this.loading = false;
    });
    this.afMessaging.getToken.subscribe((token) => {
      this.fcm_token = token;
    });
  }

  verifyOtp() {
    var user = localStorage.getItem("user");
    const postdata = {
      otp:
        this.otpverifyform.value.otp1 +
        this.otpverifyform.value.otp2 +
        this.otpverifyform.value.otp3 +
        this.otpverifyform.value.otp4,
      user,
      fcm_token: this.fcm_token,
      //  url: this.url
    };
    this.loading = true;
    this.authService.verifyOtpLogin(postdata).subscribe((res: any) => {
      this.loading = false;
      if (res.success) {
        // this.route.queryParamMap.subscribe((params) => {
        //   if (params.get("q")) {
        //     this.router.navigateByUrl(params.get("q"));
        //   }
        // });
        const url = localStorage.getItem("url");
        this.router.navigateByUrl(url);
        localStorage.removeItem("url");
        this.angularnotifierService.showNotification(
          "default",
          "Successfully Logged In!"
        );
        // this.router.navigate(['/']);
      } else {
        // this.alertService.error('Failed!', res.msg)
        this.angularnotifierService.showNotification("error", res.msg);
      }
    });
  }

  loginWithPassword() {
    this.route.queryParamMap.subscribe((params) => {
      this.router.navigate(["login-password"], {
        skipLocationChange: true,
        queryParams: {
          q: params.get("q"),
        },
      });
    });
    // this.router.navigate(["/"], { skipLocationChange: true });
  }

  resend() {
    this.resendloader1 = false;
    this.resendloader = true;
    this.otpverifyform.patchValue({ otp1: null });
    this.otpverifyform.patchValue({ otp2: null });
    this.otpverifyform.patchValue({ otp3: null });
    this.otpverifyform.patchValue({ otp4: null });
    this.authService
      .resendotp({
        mobile: this.mobileNumber,
        type: "login_otp",
        from: "login",
      })
      .subscribe((res: any) => {
        if (res.success) {
          this.angularnotifierService.showNotification(
            "default",
            "OTP Sent to your phone."
          );
          // this.alertService.success("Success", "Otp sent to you phone!")
          this.resendloader = false;
          this.resendloader1 = true;
        } else {
          this.resendloader = true;
          this.resendloader1 = false;
        }
      });
  }
}

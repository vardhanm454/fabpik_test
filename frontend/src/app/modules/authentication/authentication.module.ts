import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { OtpverificationComponent } from './otpverification/otpverification.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SignwithemailComponent } from './signwithemail/signwithemail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginVerifyComponent } from './login-verify/login-verify.component';
import { LoginPasswordComponent } from './login-password/login-password.component';
import { ResetpasswordComponent } from './resetpassowrd/resetpassword.component';

@NgModule({
  declarations: [SigninComponent, SignupComponent, OtpverificationComponent, ForgotpasswordComponent, ResetpasswordComponent, SignwithemailComponent, LoginVerifyComponent, LoginPasswordComponent],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class AuthenticationModule { }

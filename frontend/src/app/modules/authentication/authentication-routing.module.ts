import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";
import { OtpverificationComponent } from "./otpverification/otpverification.component";
import { ForgotpasswordComponent } from "./forgotpassword/forgotpassword.component";
import { SignwithemailComponent } from "./signwithemail/signwithemail.component";
import { LoginVerifyComponent } from "./login-verify/login-verify.component";
import { LoginPasswordComponent } from "./login-password/login-password.component";
import { ResetpasswordComponent } from "./resetpassowrd/resetpassword.component";
import { IsLoggedGuard } from "../../helpers/is-logged.guard";

const routes: Routes = [
  {
    path: "",
    canActivate: [IsLoggedGuard],
    component: SigninComponent,
  },
  {
    path: "login",
    canActivate: [IsLoggedGuard],
    component: SigninComponent,
  },
  {
    path: "signup",
    canActivate: [IsLoggedGuard],
    component: SignupComponent,
  },
  {
    path: "signwithemail",
    canActivate: [IsLoggedGuard],
    component: SignwithemailComponent,
  },
  {
    path: "otpverify",
    canActivate: [IsLoggedGuard],
    component: OtpverificationComponent,
  },
  {
    path: "forgotpassword",
    canActivate: [IsLoggedGuard],
    component: ForgotpasswordComponent,
  },
  {
    path: "resetpassword",
    component: ResetpasswordComponent,
  },
  {
    path: "otp-verify",
    canActivate: [IsLoggedGuard],
    component: LoginVerifyComponent,
  },
  {
    path: "login-password",
    canActivate: [IsLoggedGuard],
    component: LoginPasswordComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}

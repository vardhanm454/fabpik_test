import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { OwlOptions } from "ngx-owl-carousel-o";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-login-password",
  templateUrl: "./login-password.component.html",
  styleUrls: ["./login-password.component.scss"],
})
export class LoginPasswordComponent implements OnInit {
  fieldTextType: boolean;
  loginPassword: FormGroup;
  user: any;
  errorMessages = {
    password: [{ type: "required", message: "Password is required." }],
  };
  constructor(
    private _location: Location,
    private formBuilder: FormBuilder,
    public authService: AuthenticationService,
    private angularnotifierService: AngularnotifierService,
    public route: ActivatedRoute,
    public router: Router
  ) {
    this.loginPassword = this.formBuilder.group({
      password: ["", [Validators.required]],
    });
  }

  public sliders = [
    { image: "assets/images/slider/slider.jpg" },
    { image: "assets/images/slider/slider.jpg" },
    { image: "assets/images/slider/slider.jpg" },
  ];

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    autoplay: true,
    navSpeed: 700,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
    nav: false,
  };

  ngOnInit(): void {
    this.user = localStorage.getItem("user");
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }

  goback() {
    this._location.back();
  }

  submit() {
    const postdata = {
      password: this.loginPassword.value.password,
      user: localStorage.getItem("user"),
    };
    this.authService.loginWithPassword(postdata).subscribe((res: any) => {
      if (res.success) {
        // this.route.queryParamMap.subscribe((params) => {
        //   if (params.get("q")) {
        //     this.router.navigateByUrl(params.get("q"));
        //   }
        // });
        const url = localStorage.getItem("url");
        this.router.navigateByUrl(url);
        localStorage.removeItem("url");
        // this.alertService.success("Success","Logged In!");
        this.angularnotifierService.showNotification(
          "default",
          "Success , Logged In!"
        );
      } else {
        // this.alertService.error("Failed",res.msg)
        this.angularnotifierService.showNotification("warning", res.msg);
      }
    });
  }

  slickInit(e) {
    // console.log('slick initialized');
  }

  breakpoint(e) {
    // console.log('breakpoint');
  }

  afterChange(e) {
    // console.log('afterChange');
  }

  beforeChange(e) {
    // console.log('beforeChange');
  }
}

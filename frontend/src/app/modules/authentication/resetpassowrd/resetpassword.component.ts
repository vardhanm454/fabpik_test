import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {
  public headername: string = 'Change Password';
  constructor(private _location: Location,
    public modalService: NgbModal,) { }
  goback() {
    this._location.back();
  }

  ngOnInit(): void {
  }
 
}

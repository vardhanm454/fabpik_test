import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { OwlOptions } from "ngx-owl-carousel-o";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { first } from "rxjs/operators";
import { NotifierService } from "angular-notifier";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.scss"],
})
export class SigninComponent implements OnInit {
  loginform: FormGroup;
  submitted = false;
  isError: Boolean = false;
  errorMessage: any;
  showLoginError: Boolean = false;
  loading = false;
  showLoginErrorMsg: any = "";
  constructor(
    private _location: Location,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private angularnotifierService: AngularnotifierService
  ) {}
  goback() {
    this._location.back();
  }
  public sliders = [
    { image: "assets/images/slider/slider.jpg" },
    { image: "assets/images/slider/slider.jpg" },
    { image: "assets/images/slider/slider.jpg" },
  ];

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    autoplay: true,
    navSpeed: 700,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
    nav: false,
  };

  ngOnInit(): void {
    // this.loginform = this.formBuilder.group({
    // mobile: ['', [Validators.required, Validators.minLength(10)]]
    //mobile:["", { validators: [Validators.required,,Validators.pattern("^[0-9]*$"), Validators.minLength(10)], updateOn: "blur" }],
    // });pattern="^[7-9][0-9]{8}$"  pattern("^[0-9]*$")
    this.loginform = this.formBuilder.group({
      user: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(/^(?:\d{10}|\w+@\w+\.\w{2,3})$/),
        ]),
      ],
    });
  }
  // /^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/
  get f() {
    return this.loginform.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginform.invalid) {
      return;
    }
    this.loading = true;

    this.authenticationService.login(this.loginform.value).subscribe({
      next: (res: any) => {
        this.loading = false;
        if (res.success) {
          this.showLoginError = false;
          localStorage.setItem("user", this.loginform.value.user);
          this.isError = false;
          localStorage.setItem("response", JSON.stringify(res));
          // this.alertService.success('Otp send to your phone' ,'success');
          this.angularnotifierService.showNotification(
            "default",
            "OTP Sent to your phone."
          );
          this.authenticationService
            .sendMailLoginOtp(this.loginform.value)
            .subscribe((res: any) => {});
          this.route.queryParamMap.subscribe((params) => {
            // var cids = JSON.parse(params.get("cids"));
            this.router.navigate(["otp-verify"], {
              skipLocationChange: true,
              queryParams: {
                q: params.get("q"),
              },
            });
          });
          // this.router.navigate(["/otp-verify/"], { skipLocationChange: true });
        } else {
          if (res.msg == "New User!") {
            localStorage.setItem("user", this.loginform.value.user);
            // this.router.navigate(["/signup"]);
            this.route.queryParamMap.subscribe((params) => {
              this.router.navigate(["signup"], {
                queryParams: {
                  q: params.get("q"),
                },
              });
            });
          } else if (
            res.msg ==
            "The phone number is registered for different account type, please use other mobile number to register."
          ) {
            // this.alertService.error('error',res.msg)
            // this.alertService.warning('Account Login',res.msg)
            this.angularnotifierService.showNotification("warning", res.msg);
          } else if (
            res.msg ==
            "Try again after sometime. If it occurs again, reach out to Fabpik Customer care."
          ) {
            this.showLoginError = true;
            this.showLoginErrorMsg = res.msg;
          } else {
            // this.errorMessage = res.error;
            //  this.alertService.error('error',res.error)
            this.angularnotifierService.showNotification("error", res.error);
            this.isError = true;
          }
        }
      },
      error: (error) => {
        var err = JSON.stringify(error);
        //  this.alertService.error(err ,'error');
        this.loading = false;
      },
    });
  }
}

import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { filter, first } from "rxjs/operators";
import {
  ActivatedRoute,
  Router,
  ParamMap,
  NavigationEnd,
} from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { DataService } from "src/app/service/data/data.service";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
@Component({
  selector: "app-otpverification",
  templateUrl: "./otpverification.component.html",
  styleUrls: ["./otpverification.component.scss"],
})
export class OtpverificationComponent implements OnInit {
  otpverifyform: FormGroup;
  submitted = false;
  loading = false;
  otp: any;
  mobile: any;
  otpdata: any;
  url: any;
  registerDetails: any;

  resendloader: boolean = false;
  resendloader1: boolean = true;

  constructor(
    private _location: Location,
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private angularnotifierService: AngularnotifierService,
    public dataService: DataService,
    public route: ActivatedRoute
  ) {}

  goback() {
    this._location.back();
  }
  loadSignUpScript() {
    let node = document.createElement("script"); // creates the script tag
    node.text = `fbq('track', 'CompleteRegistration')`;
    // append to head of document
    document.getElementsByTagName("head")[0].appendChild(node);
  }
  ngOnInit(): void {
    if (window.location.href.indexOf("https://fabpik.in/") > -1) {
      this.loadSignUpScript();
    }
    this.dataService.currentRegisterDetails.subscribe((res) => {
      if (
        res.email == "" ||
        res.password == "" ||
        res.name == "" ||
        res.mobile == ""
      ) {
        this.goback();
      } else {
        this.registerDetails = res;
      }
    });
    this.otpverifyform = this.formBuilder.group({
      otp1: ["", [Validators.required, Validators.minLength(1)]],
      otp2: ["", [Validators.required, Validators.minLength(1)]],
      otp3: ["", [Validators.required, Validators.minLength(1)]],
      otp4: ["", [Validators.required, Validators.minLength(1)]],
    });
    var values = JSON.parse(localStorage.getItem("response"));
    this.mobile = localStorage.getItem("mobile");
    this.url = values.url;
  }

  // move(fromOtp, toOtp) {
  //   var length = fromOtp.length;
  //   var maxlength = fromOtp.getAttribute(maxlength);
  //   if (length == maxlength) {
  //     toOtp.focus();
  //   }
  // }
  move(backOtp, fromOtp, toOtp, keycode) {
    if (keycode == 8) {
      var length = fromOtp.length;
      var maxlength = fromOtp.getAttribute(maxlength);
      backOtp.focus();
    } else {
      var length = fromOtp.length;
      var maxlength = fromOtp.getAttribute(maxlength);
      if (length == maxlength) {
        toOtp.focus();
      }
    }
  }

  get f() {
    return this.otpverifyform.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.otpverifyform.invalid) {
      return;
    }

    this.otpdata = {
      otp:
        this.otpverifyform.value.otp1 +
        this.otpverifyform.value.otp2 +
        this.otpverifyform.value.otp3 +
        this.otpverifyform.value.otp4,
      mobile: this.mobile,
    };
    const postdata = {
      email: this.registerDetails.email,
      mobile: this.registerDetails.mobile,
      password: this.registerDetails.password,
      name: this.registerDetails.name,
      otp: this.otpdata.otp,
    };
    this.loading = true;
    this.authenticationService.verifyOtpRegister(postdata).subscribe({
      next: (res: any) => {
        this.loading = false;
        if (res.success) {
          // this.route.queryParamMap.subscribe((params) => {
          //   if (params.get("q")) {
          //     this.router.navigateByUrl(params.get("q"));
          //   }
          // });
          const url = localStorage.getItem("url");
          this.router.navigateByUrl(url);
          localStorage.removeItem("url");
          this.angularnotifierService.showNotification(
            "default",
            "Otp Verified!"
          );
        } else {
          // this.alertService.error('Failed!',res.msg)
          this.angularnotifierService.showNotification("error", res.msg);
        }
      },
      error: (error) => {
        var err = JSON.stringify(error);
        // this.alertService.error('Invalid Otp' ,'error');
        this.angularnotifierService.showNotification("error", "Invalid Otp");
        this.loading = false;
      },
    });
  }

  resend() {
    this.resendloader1 = false;
    this.resendloader = true;
    this.otpdata = {
      mobile: this.mobile,
      type: "verify_mobile",
      email: this.registerDetails.email,
      from: "register",
    };
    this.authenticationService.resendotp(this.otpdata).subscribe((res: any) => {
      if (res.success) {
        // this.alertService.success("Success","Otp sent to your phone!")
        this.angularnotifierService.showNotification(
          "default",
          "OTP Sent to your phone."
        );
        this.resendloader = false;
        this.resendloader1 = true;
      } else {
        this.resendloader = true;
        this.resendloader1 = false;
        this.angularnotifierService.showNotification(
          "default",
          "Something went wrong!."
        );
      }
    });
  }
}

import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";

@Component({
  selector: "app-forgotpassword",
  templateUrl: "./forgotpassword.component.html",
  styleUrls: ["./forgotpassword.component.scss"],
})
export class ForgotpasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;

  constructor(
    private _location: Location,
    public authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private angularnotifierService: AngularnotifierService,
    public router: Router
  ) {}

  goback() {
    this._location.back();
  }

  ngOnInit() {
    // console.log(this._location.getState());
    var values = JSON.parse(localStorage.getItem("customer"));
    if (values) {
      if (!this.authService.tokenExpired(values.access_token)) {
        this.router.navigate(["/"]);
      }
    }
    this.forgotPasswordForm = this.formBuilder.group({
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,5}$"),
        ],
      ],
    });
  }

  submit() {
    const postdata = this.forgotPasswordForm.value;
    this.authService.forgotPassword(postdata).subscribe((res: any) => {
      if (res.success) {
        this.forgotPasswordForm.reset();
        // this.alertService.success("Success",res.msg)
        this.angularnotifierService.showNotification("default", res.msg);
      } else {
        // this.alertService.warning("Invalid",res.msg)
        this.angularnotifierService.showNotification("error", res.msg);
      }
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Location } from '@angular/common';
@Component({
  selector: 'app-signwithemail',
  templateUrl: './signwithemail.component.html',
  styleUrls: ['./signwithemail.component.scss']
})
export class SignwithemailComponent implements OnInit {
 
  constructor(private _location: Location) {}
  goback() {
    this._location.back();
  }
  public sliders =
   [
    { image: "assets/images/slider/slider.jpg"},
    { image: "assets/images/slider/slider.jpg"},
    {image: "assets/images/slider/slider.jpg"}
  ]

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    autoplay:true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: false
  }


  ngOnInit(): void {
  }

}

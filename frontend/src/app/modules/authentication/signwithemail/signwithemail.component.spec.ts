import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignwithemailComponent } from './signwithemail.component';

describe('SignwithemailComponent', () => {
  let component: SignwithemailComponent;
  let fixture: ComponentFixture<SignwithemailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignwithemailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignwithemailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

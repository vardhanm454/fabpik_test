import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/service/data/data.service";
import { OwlOptions } from "ngx-owl-carousel-o";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"],
})
export class SignupComponent implements OnInit {
  RegisterForm: FormGroup;
  mobileServerErrors: any = [];
  emailServerErrors: any = [];
  fieldTextType: boolean;
  errorMessages = {
    name: [{ type: "required", message: "Name is required." }],
    email: [{ type: "required", message: "E-mail is required." }],
    mobile: [
      { type: "required", message: "Phone number is required." },
      {
        type: "pattern",
        message:
          "Should start with 6,7,8 or 9 and it should be 10 digits only.",
      },
      // { type: "cannotContainSpace", message: 'Spaces (" ") are not allowed.' }
    ],
    password: [{ type: "required", message: "password is required." }],
  };
  public sliders = [
    { image: "assets/images/slider/slider.jpg" },
    { image: "assets/images/slider/slider.jpg" },
    { image: "assets/images/slider/slider.jpg" },
  ];
  loading: boolean = false;

  getPolicy(data) {
    this.router.navigate(["policies", data]);
  }

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    autoplay: true,
    navSpeed: 700,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
    nav: false,
  };

  constructor(
    private _location: Location,
    private formBuilder: FormBuilder,
    public authService: AuthenticationService,
    private angularnotifierService: AngularnotifierService,
    private router: Router,
    private dataService: DataService,
    public route: ActivatedRoute
  ) {
    this.RegisterForm = this.formBuilder.group({
      name: ["", [Validators.required]],

      mobile: [
        "",
        [
          Validators.required,
          Validators.pattern("^[6-9]\\d{9}$"),
          // CustomValidators.cannotContainSpace
        ],
      ],
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,5}$"),
          // CustomValidators.cannotContainSpace
        ],
      ],
      password: ["", [Validators.required, Validators.minLength(8)]],
    });
  }

  goback() {
    this._location.back();
  }

  ngOnInit() {
    this;
    var user = localStorage.getItem("user");
    var intRegex = /[0-9 -()+]+$/;
    if (intRegex.test(user)) {
      this.RegisterForm.patchValue({
        mobile: user,
      });
    } else {
      this.RegisterForm.patchValue({
        email: user,
      });
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onSubmit() {
    if (this.RegisterForm.invalid) {
      this.validateAllFormFields(this.RegisterForm);
      return;
    }
    this.loading = true;
    this.dataService.changeRegisterDetails(this.RegisterForm.value);
    const postdata = this.RegisterForm.value;
    localStorage.setItem("mobile", this.RegisterForm.value.mobile);
    this.authService.register(postdata).subscribe((res: any) => {
      this.loading = false;
      if (res.success) {
        localStorage.removeItem("user");
        // this.router.navigate(["/otpverify"], { skipLocationChange: true });
        this.route.queryParamMap.subscribe((params) => {
          this.router.navigate(["otpverify"], {
            skipLocationChange: true,
            queryParams: {
              q: params.get("q"),
            },
          });
        });
      } else {
        if (res.errors) {
          if (res.errors.mobile) {
            this.mobileServerErrors = res.errors.mobile;
          } else if (res.errors.email) {
            this.emailServerErrors = res.errors.email;
          }
          return;
        }
        // this.alertService.error("Error","Something Went Wrong!")
        this.angularnotifierService.showNotification(
          "error",
          "Something Went Wrong!"
        );
      }
    });
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConversationService } from 'src/app/service/conversation/conversation.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  host: {
    "(window:resize)":"onWindowResize($event)"
  }
})
export class ChatComponent implements OnInit {
  public chat: boolean = false;
  public chatlist: boolean = true;
  isMobile: boolean = false;
  isDesktop: boolean = false;
  width:number = window.innerWidth;
  height:number = window.innerHeight;
  mobileWidth:number  = 767;
  seller_id:any;
  chatform:FormGroup;
  submitted = false;
  user_id:any;
  listing:any;
  Allmessages:any;
  sellerdetails:any;
  public newPass = () => {
    this.chat = true;
    this.chatlist = false;
  }
  
  public headername: string = 'Chat';

  constructor( private formBuilder: FormBuilder,
               private  conversationService:ConversationService) { }

  ngOnInit(): void {
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;

    var values = JSON.parse(localStorage.getItem("customer"));  
    this.user_id = values.user.id;
    this.seller_id = localStorage.getItem('sellerid');
    this.chatform = this.formBuilder.group({
      message: ['',[Validators.required]]
    });
    this.getmessages();

  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }

  onSubmit(){
    this.submitted = true;
    if (this.chatform.invalid) {
      return;
    }
    this.listing={
      user_id: this.user_id,
      seller_id:this.seller_id,
      message:this.chatform.value.message
    }
    this.conversationService.sendMessage(this.listing)
    .subscribe(res => {
          this.Allmessages= res;
          this.chatform.reset();
     });

  }

  getmessages(){
    this.listing={
      user_id: this.user_id,
      seller_id:this.seller_id,
    }
      this.conversationService.getMessage(this.listing).subscribe((res:any)=>{
          this.Allmessages= res.conversationlist;
          this.sellerdetails= res.seller;
     });
  }

}

import { Component, OnInit } from '@angular/core';
import { PurchaseHistoryService } from 'src/app/service/purchasehistoryservice/purchasehistoryservice.service';

@Component({
  selector: 'app-paymentlist',
  templateUrl: './paymentlist.component.html',
  styleUrls: ['./paymentlist.component.scss']
})
export class PaymentlistComponent implements OnInit {
  isMobile: boolean = false;
  isDesktop: boolean = false;
  allOrders:any = [];
  loading:Boolean = true;
  constructor( private purchasehistoryService: PurchaseHistoryService,
    ) {    const body = document.getElementsByTagName('body')[0];
    body.classList.add('bgcategory-light');
  }
  ngOnDestroy(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('bgcategory-light');
  }
  public headername: string = "Payments";
  ngOnInit(): void {
    this.purchasehistoryService.customerParentOrders().subscribe((res:any)=>{
      
      if(res.success){
        this.loading = false;
        this.allOrders = res.orders;
      }
    })
  }

}

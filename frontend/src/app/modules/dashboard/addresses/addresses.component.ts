import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { AddressService } from "src/app/service/address/address.service";
import { first } from "rxjs/operators";
import "sweetalert2/src/sweetalert2.scss";
import Swal from "sweetalert2";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";

@Component({
  selector: "app-addresses",
  templateUrl: "./addresses.component.html",
  styleUrls: ["./addresses.component.scss"],
  host: {
    "(window:resize)": "onWindowResize($event)",
  },
})
export class AddressesComponent implements OnInit {
  isMobile: boolean = false;
  isDesktop: boolean = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth: number = 767;
  loading: boolean = true;

  public headername: string = "Address";

  AllAddress: any = [];
  is_default: any;
  bigloading = true;
  submitted = false;
  constructor(
    private _location: Location,
    private formBuilder: FormBuilder,
    private router: Router,
    private angularnotifierService: AngularnotifierService,
    private addressService: AddressService
  ) {
    const body = document.getElementsByTagName("body")[0];
    body.classList.add("bgcategory-light");
  }

  goback() {
    this._location.back();
  }

  ngOnInit(): void {
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
    this.getAllAddress();
  }

  private getAllAddress() {
    this.bigloading = true;
    this.addressService.getAllAddress().subscribe((res) => {
      if(res.success){
        this.AllAddress = res.addresses;
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
      this.bigloading = false;
      this.loading = false;
    });
  }

  isdefault(value, id) {
    this.is_default = {
      is_default: value,
    };
    this.addressService.updatedefaultAddress(id, this.is_default).subscribe({
      next: (res) => {
        // this.alertService.success('Address updated as default', 'success');
        if(res.success){
          this.angularnotifierService.showNotification(
            "default",
            "Address updated as default"
          );
          var checkoutChangeAddress = localStorage.getItem(
            "checkoutChangeAddress"
          );
          if (checkoutChangeAddress == "1") {
            localStorage.removeItem("checkoutChangeAddress");
            this.router.navigate(["/shop/checkout"]);
            return;
          }
          this.router.navigate(["/myaccount/addresses"]);
        }else{
          this.angularnotifierService.showNotification(
            "error",
            res.msg
          );
        }
      },
      error: (error) => {
        var err = JSON.stringify(error.error.error);
        // this.alertService.error(err, 'error');
        this.angularnotifierService.showNotification("error", err);
        this.angularnotifierService.showNotification("error", err);
      },
    });
  }
  ngOnDestroy(): void {
    const body = document.getElementsByTagName("body")[0];
    body.classList.remove("bgcategory-light");
  }
  deleteAddress(id) {
    Swal.fire({
      title: "Are you sure you want to delete this Address?",
      icon: "warning",
      showClass: {
        popup: "swal-wish-wide animated fadeInDown faster",
      },
      cancelButtonColor: "#e42b5a08",
      confirmButtonColor: "#e42b5abf",
      showCloseButton: true,
      showCancelButton: true,
    }).then((check) => {
      this.addressService.deleteAddress(id).subscribe((res:any) => {
        if(res.success){
          if (check.value) {
            Swal.fire({
              title: "Yes deleted!",
              text: "Address has been deleted.",
              icon: "success",
  
              confirmButtonColor: "#e42b5abf",
              confirmButtonText: "Okay",
  
              showClass: {
                popup: "swal-wish-wide animated fadeInDown faster",
              },
            }).then(function () {
              //this.submitted = false;
              window.location.reload();
            });
          }
        }else{
          this.angularnotifierService.showNotification("error", res.msg);
        }
      });
    });
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }
}

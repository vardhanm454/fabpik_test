import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { first } from "rxjs/operators";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AddressService } from "src/app/service/address/address.service";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
declare var $: any;
declare const google: any;

@Component({
  selector: "app-manage-address",
  templateUrl: "./manage-address.component.html",
  styleUrls: ["./manage-address.component.scss"],
  host: {
    "(window:resize)": "onWindowResize($event)",
  },
})
export class ManageAddressComponent implements OnInit {
  isMobile: boolean = false;
  isDesktop: boolean = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth: number = 767;
  public headername: string = "Add Address";
  addAddressform: FormGroup;
  id: string;
  isAddMode: boolean;
  loading = false;
  submitted = false;
  states: any = null;
  default: any;
  type: any;
  addr_type: any;
  //is_defaultCtrl: FormControl;
  options = {
    componentRestrictions: {
      country: ["IN"],
    },
  };
  mapAddress: any;
  houseNumber: any;
  street: any;
  mapCity: any;
  mapState: any;
  mapcountry: any;
  locality: any;
  use_my_location_button:Boolean = false;
  google_auto_suggestions:Boolean = false;
  constructor(
    private _location: Location,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private addressService: AddressService,
    private angularnotifierService: AngularnotifierService,
    public authService: AuthenticationService
  ) {
    $("input[type='radio']").change(function () {
      if ($(this).val() == "other") {
        $("#otherAnswer").show();
      } else {
        $("#otherAnswer").hide();
      }
    });

    
  }

  goback() {
    this._location.back();
  }

  FieldsChange(event) {
    if (event.currentTarget.checked == true) {
      this.default = "1";
    } else {
      this.default = "0";
    }
  }

  ngOnInit(): void {
    this.getStates();
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
    this.id = this.route.snapshot.params["id"];
    this.isAddMode = !this.id;
    this.addressService.sendGoogleAddressSuggestion().subscribe((res:any)=>{
      this.use_my_location_button = res.use_my_location_button;
      this.google_auto_suggestions =res.google_auto_suggestions
    })
    this.addAddressform = this.formBuilder.group({
      name: [
        "",
        [Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z ]+")],
      ],
      mobile: [
        "",
        [
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(10),
          Validators.maxLength(10),
        ],
      ],
      pincode: [
        "",
        [
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(6),
          Validators.maxLength(6),
        ],
      ],
      state_id: ["", Validators.required],
      address: ["", Validators.required],
      locality: ["", Validators.required],
      city: ["", Validators.required],
      addr_type: ["", Validators.required],
      addr_other: [""],
      is_default: [this.default],
    });

    this.addAddressform.get("addr_type").valueChanges.subscribe((val) => {
      if (val == "o") {
        this.addAddressform.controls["addr_other"].setValidators([
          Validators.required,
        ]);
        this.addAddressform.controls["addr_other"].updateValueAndValidity();
      } else {
        this.addAddressform.controls["addr_other"].clearValidators();
        this.addAddressform.controls["addr_other"].updateValueAndValidity();
      }
    });

    if (!this.isAddMode) {
      this.headername = "Edit Address";
      this.addressService
        .getById(this.id)
        .subscribe((res:any) => {
          if(res.success){
            this.addAddressform.patchValue(res.data)
          }else{
            this.angularnotifierService.showNotification("error", res.msg);
          }
        });
    }
  }

  // Get all states
  private getStates() {
    this.addressService.getstates().subscribe((res) => {
      if(res.success){
        this.states = res.states;
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
    });
  }

  get f() {
    return this.addAddressform.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.addAddressform.invalid) {
      return;
    }
    this.loading = true;
    if (this.isAddMode) {
      this.addAddress();
    } else {
      this.updateAddress();
    }
  }

  private addAddress() {
    this.addressService.addAddress(this.addAddressform.value).subscribe({
      next: (res) => {
        if (res.success) {
          // this.alertService.success('Save Successfully' ,'success');
          this.angularnotifierService.showNotification(
            "default",
            "Save Successfully"
          );
          this.router.navigate(["/myaccount/addresses"]);
        } else {
          // this.alertService.error(res.error,'error');
          this.angularnotifierService.showNotification("error", res.msg);
        }
      },
      error: (error) => {
        var err = JSON.stringify(error);
        // this.alertService.error(err ,'error');
        this.angularnotifierService.showNotification("error", error);
        this.loading = false;
      },
    });
  }

  private updateAddress() {
    this.addressService.update(this.id, this.addAddressform.value).subscribe({
      next: (res) => {
        if(res.success){
          this.angularnotifierService.showNotification(
            "default",
            "Update Successfully"
          );
          this.router.navigate(["/myaccount/addresses"]);
        }else{
          this.angularnotifierService.showNotification("error", res.msg);
        }
      },
      error: (error) => {
        var err = JSON.stringify(error);
        //  this.alertService.error(err ,'error');
        this.angularnotifierService.showNotification("error", err);
        this.loading = false;
      },
    });
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }
  // handleAddressChange(address: any) {
  //   // this.addAddressform.patchValue({
  //   //   locality: address.vicinity,
  //   // });

  //   var addressGenerate = "";
  //   for (let i = 0; i < address.address_components.length; i++) {
  //     this.mapAddress = address.address_components[i];
  //     if (this.mapAddress.long_name != "") {
  //       if (this.mapAddress.types[0] == "premise") {
  //         addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
  //       }
  //       if (this.mapAddress.types[0] == "neighborhood") {
  //         addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
  //       }
  //       if (this.mapAddress.types[0] == "sublocality_level_2") {
  //         addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
  //         this.addAddressform.patchValue({
  //           locality: this.mapAddress.long_name,
  //         });
  //       } else if (
  //         this.mapAddress.types[0] == "sublocality_level_1" ||
  //         this.mapAddress.types[2] == "sublocality_level_1"
  //       ) {
  //         addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
  //         this.addAddressform.patchValue({
  //           locality: this.mapAddress.long_name,
  //         });
  //       }
  //       if (this.mapAddress.types[0] == "locality") {
  //         addressGenerate = addressGenerate + this.mapAddress.long_name;
  //         this.addAddressform.patchValue({
  //           city: this.mapAddress.long_name,
  //         });
  //       }

  //       if (this.mapAddress.types[0] == "administrative_area_level_1") {
  //         this.mapState = this.mapAddress.long_name;
  //         this.states.forEach((state) => {
  //           if (state.name == this.mapState) {
  //             this.addAddressform.patchValue({ state_id: state.id });
  //           }
  //         });
  //       }
  //       if (this.mapAddress.types[0] == "country") {
  //         this.mapcountry = this.mapAddress.long_name;
  //       } else {
  //         this.mapcountry = "";
  //       }
  //       if (this.mapAddress.types[0] == "postal_code") {
  //         this.addAddressform.patchValue({
  //           pincode: this.mapAddress.long_name,
  //         });
  //       }
  //       let streets =
  //         this.street && this.houseNumber
  //           ? this.houseNumber + ", " + this.street
  //           : "";
  //       this.mapAddress = streets ? streets : this.street;
  //     }
  //   }
  //   this.addAddressform.patchValue({
  //     address: addressGenerate,
  //   });
  // }
  handleAddressChange(address: any) {
    // this.addAddressform.patchValue({
    //   locality: address.vicinity,
    // });

    var addressGenerate = "";
    for (let i = 0; i < address.address_components.length; i++) {
      this.mapAddress = address.address_components[i];
      if (this.mapAddress.long_name != "") {
        if (this.mapAddress.types[0] == "premise") {
          addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
        }
        if (this.mapAddress.types[0] == "neighborhood") {
          addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
        }
        if (this.mapAddress.types[0] == "sublocality_level_2") {
          addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
          this.addAddressform.patchValue({
            locality: this.mapAddress.long_name,
          });
        } else if (
          this.mapAddress.types[0] == "sublocality_level_1" ||
          this.mapAddress.types[2] == "sublocality_level_1"
        ) {
          addressGenerate = addressGenerate + this.mapAddress.long_name + ", ";
          this.addAddressform.patchValue({
            locality: this.mapAddress.long_name,
          });
        }
        if (this.mapAddress.types[0] == "locality") {
          addressGenerate = addressGenerate + this.mapAddress.long_name;
          this.addAddressform.patchValue({
            city: this.mapAddress.long_name,
          });
        }

        if (this.mapAddress.types[0] == "administrative_area_level_1") {
          this.mapState = this.mapAddress.long_name;
          this.states.forEach((state) => {
            if (state.name == this.mapState) {
              this.addAddressform.patchValue({ state_id: state.id });
            }
          });
        }
        if (this.mapAddress.types[0] == "country") {
          this.mapcountry = this.mapAddress.long_name;
        } else {
          this.mapcountry = "";
        }
        if (this.mapAddress.types[0] == "postal_code") {
          this.addAddressform.patchValue({
            pincode: this.mapAddress.long_name,
          });
        }
        let streets =
          this.street && this.houseNumber
            ? this.houseNumber + ", " + this.street
            : "";
        this.mapAddress = streets ? streets : this.street;
      }
    }
    this.addAddressform.patchValue({
      address: addressGenerate,
    });
  }
  useMyLocation() {
    navigator.geolocation.getCurrentPosition((location) => {
      let geocoder = new google.maps.Geocoder();
      let latlng = new google.maps.LatLng(
        location.coords.latitude,
        location.coords.longitude
      );
      let request = {
        latLng: latlng,
      };
      geocoder.geocode(
        request,
        (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0] != null) {
              this.handleAddressChange(results[0]);
            } else {
            }
          }
        },
        (error) => {}
      );
    });
  }
}

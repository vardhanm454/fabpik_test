import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { WINDOW_PROVIDERS } from "ngx-owl-carousel-o/lib/services/window-ref.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
declare var $: any;

@Component({
  selector: "app-manage-profile",
  templateUrl: "./manage-profile.component.html",
  styleUrls: ["./manage-profile.component.scss"],
  host: {
    "(window:resize)": "onWindowResize($event)",
  },
})
export class ManageProfileComponent implements OnInit {
  isMobile: boolean = false;
  isDesktop: boolean = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth: number = 767;
  public imagePath;
  imgURL1: any;
  filedata1: any;
  public oldNumber: boolean = false;
  public newNumber: boolean = false;
  public editProfile: boolean = true;
  public filename: any;
  selectedFiles: FileList;
  fileName: string = "";
  public headername: string = "Edit Details";
  editprofileform: FormGroup;
  changemobileform: FormGroup;
  changenewmobileform: FormGroup;
  requestotpform: FormGroup;
  id: number;
  currentFileUpload: File;
  filedata: any;
  otpdata: any;
  customer: any = null;
  bigloading = false;
  loading = false;
  submitted = false;
  showNewNumberOtpInput: Boolean = false;
  loadingApi: Boolean = true;
  base64GstDoc;
  emailRegEx;
  //   selectedFiles: FileList;
  //   fileName: string = '';
  // currentFileUpload: File;

  fileEvent(event) {
    this.filedata = event.target.files[0];
    const max_size = 5242880;
    const allowed_types = ["application/pdf"];
    if (event.target.files[0].size > max_size) {
      // alert('max size error');
      this.angularnotifierService.showNotification(
        "error",
        "Upload max size 5242880"
      );
      //  this.alertService.error('Upload max size 5242880' ,'error');
      return false;
    }
    if (event.target.files[0].type != allowed_types) {
      // alert('type error');
      // this.alertService.error('Upload only pdf' ,'error');
      this.angularnotifierService.showNotification("error", "Upload only pdf");
      return false;
    }
    var reader = new FileReader();
    reader.readAsDataURL(this.filedata);
    reader.onload = () => {
      this.base64GstDoc = reader.result;
    };
  }

  selectFile(event) {
    let formData = new FormData();
    this.selectedFiles = event.target.files;
    this.fileName = this.selectedFiles[0].name;
    this.currentFileUpload = this.selectedFiles.item(0);
    formData.append("filename", this.fileName);
    formData.append("file", this.currentFileUpload);
  }

  change() {
    if ($(".riyaqas-file-input").length) {
      $(".riyaqas-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        var file_ext = fileName.substring(fileName.lastIndexOf(".") + 1);
        if (fileName.length > 20) {
          fileName = fileName.substring(0, 20) + "..." + file_ext;
        }
        $(this)
          .siblings(".custom-file-label")
          .addClass("selected")
          .html(fileName);
      });
    }
  }

  constructor(
    private _location: Location,
    private formBuilder: FormBuilder,
    private router: Router,
    private angularnotifierService: AngularnotifierService,
    private authenticationService: AuthenticationService
  ) {
    this.editprofileform = this.formBuilder.group({
      mobile: ["", Validators.required],
      name: [
        "",
        [Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z ]+")],
      ],
      email: [
        "",
        [
          Validators.required,
          Validators.email,
          Validators.pattern(this.emailRegEx),
        ],
      ],
      gender: ["", Validators.required],
      dob: ["", Validators.required],
      gst: ["", Validators.required],
      gst_no: [""],
      gst_certificate: [""],
      alternate_mobile: [
        "",
        [
          Validators.nullValidator,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(10),
          Validators.maxLength(10),
        ],
      ],
    });
  }

  changeMobile = () => {
    this.oldNumber = true;
    this.editProfile = false;
    this.changemobileform.patchValue({
      mobile: this.customer.mobile,
    });
    this.otpdata = {
      mobile: this.customer.mobile,
    };
    this.authenticationService.updateprofie(this.otpdata).subscribe({
      next: (res) => {
        //alert('otp send to your mail');
        // this.alertService.success('Otp send to your phone' ,'success');
        this.angularnotifierService.showNotification(
          "success",
          "OTP Sent to your phone."
        );
      },
      error: (error) => {
        var err = JSON.stringify(error);
        // this.alertService.error(err ,'error');
        this.angularnotifierService.showNotification("error", err);
      },
    });
  };

  public newPass = () => {
    this.oldNumber = false;
    this.editProfile = false;
    this.newNumber = true;
  };

  ngOnInit(): void {
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;

    var values = JSON.parse(localStorage.getItem("customer"));
    this.id = values.user.id;
    this.getCustomer(this.id);
    this.emailRegEx = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";

    // this.editprofileform.get('gst').valueChanges.subscribe(val => {

    //   if (val == 'y') {
    //       this.editprofileform.controls['gst_no'].setValidators([Validators.required,Validators.minLength(15)]);
    //       this.editprofileform.controls['gst_no'].updateValueAndValidity();
    //   } else if(val == 'n') {
    //     console.log(val)
    //       this.editprofileform.controls['gst_no'].clearValidators();
    //       this.editprofileform.controls['gst_no'].updateValueAndValidity();
    //   }
    // });

    this.changemobileform = this.formBuilder.group({
      mobile: [
        "",
        [
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(4),
          Validators.maxLength(4),
        ],
      ],
      // mobile: ['',  [Validators.required,Validators.minLength(10)]],
      otp: [
        "",
        [
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(4),
          Validators.maxLength(4),
        ],
      ],
    });

    this.changenewmobileform = this.formBuilder.group({
      mobile: [
        "",
        [
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(4),
          Validators.maxLength(4),
        ],
      ],
      otp: [
        "",
        [
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(4),
          Validators.maxLength(4),
        ],
      ],
    });

    this.requestotpform = this.formBuilder.group({
      mobile: [
        "",
        [
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(4),
          Validators.maxLength(4),
        ],
      ],
    });
  }

  ngAfterViewInit() {
    this.editprofileform.get("gst").valueChanges.subscribe((val) => {
      if (val == "y") {
        this.editprofileform.controls["gst_no"].setValidators([
          Validators.required,
          Validators.minLength(15),
        ]);
        this.editprofileform.controls["gst_no"].updateValueAndValidity();
      } else if (val == "n") {
        this.editprofileform.controls["gst_no"].clearValidators();
        this.editprofileform.controls["gst_no"].updateValueAndValidity();
      }
    });
  }

  private getCustomer(id) {
    this.bigloading = true;
    this.authenticationService.getById(id).subscribe((res) => {
      this.loadingApi = false;
      this.customer = res[0];
      this.editprofileform.patchValue({
        mobile: this.customer.mobile,
        name: this.customer.name,
        email: this.customer.email,
        gender: this.customer.gender,
        dob: this.customer.dob,
        gst: this.customer.gst,
        gst_no: this.customer.gst_no,
        gst_certificate: this.customer.gst_certificate,
        alternate_mobile: this.customer.alternate_mobile,
      });
      this.bigloading = false;
    });
  }

  get f() {
    return this.editprofileform.controls;
  }
  get g() {
    return this.changemobileform.controls;
  }
  get h() {
    return this.changenewmobileform.controls;
  }
  get i() {
    return this.requestotpform.controls;
  }

  onUpdate() {
    this.submitted = true;
    if (this.editprofileform.invalid) {
      return;
    }
    const postdata = this.editprofileform.value;
    postdata.gstDoc = this.base64GstDoc;

    this.authenticationService.updateprofie(postdata).subscribe({
      next: (res: any) => {
        if (res.error) {
          // this.alertService.error(res.error.email,'error');
          this.angularnotifierService.showNotification(
            "error",
            res.error.email
          );
          this.submitted = false;
        } else {
          // this.alertService.success('Update Profile' ,'success');
          this.angularnotifierService.showNotification(
            "success",
            "Update Profile"
          );
          this.router.navigate(["/myaccount"]);
        }
      },
      error: (error) => {
        var err = JSON.stringify(error);
        //  this.alertService.error(err ,'error');
        this.angularnotifierService.showNotification("error", err);
        this.loading = false;
      },
    });
  }

  onchangemobile() {
    this.submitted = true;
    // if (this.requestotpform.invalid) {
    //   return;
    // }
    this.authenticationService
      .updateprofie(this.changemobileform.value)
      .subscribe({
        next: (res) => {
          this.newPass();
        },
        error: (error) => {
          var err = JSON.stringify(error);
          //  this.alertService.error('OTP verification failed' ,'error');
          this.angularnotifierService.showNotification(
            "error",
            "OTP verification failed"
          );
        },
      });
  }

  requestotp() {
    this.submitted = true;
    // if (this.changemobileform.invalid) {
    //   return;
    // }
    const data = { mobile: this.changenewmobileform.value.mobile };
    this.showNewNumberOtpInput = true;
    this.authenticationService.updateprofie(data).subscribe({
      next: (res) => {
        // this.newPass();
        // this.alertService.success('Otp send to your phone' ,'success');
        this.angularnotifierService.showNotification(
          "default",
          "OTP Sent to your phone."
        );
      },
      error: (error) => {
        var err = JSON.stringify(error);
        //  this.alertService.error('OTP verification failed' ,'error');
        this.angularnotifierService.showNotification(
          "error",
          "OTP verification failed"
        );
      },
    });
  }

  onchangenewmobile() {
    this.submitted = true;

    // if (this.changenewmobileform.invalid) {
    //   return;
    // }
    this.loading = true;
    this.authenticationService
      .updateprofie(this.changenewmobileform.value)
      .subscribe({
        next: (res: any) => {
          if (res.error) {
            this.angularnotifierService.showNotification("error", res.error);
            return;
          }
          // this.alertService.success('Mobile No Updated Successfully' ,'success');
          this.angularnotifierService.showNotification(
            "default",
            "Mobile No Updated Successfully"
          );

          this.router.navigate(["/myaccount/manage-profile"]);
          // location.reload();
        },
        error: (error) => {
          var err = JSON.stringify(error);
          //  this.alertService.error(err ,'error');
          this.angularnotifierService.showNotification("error", err);
          // this.loading = false;
        },
      });
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }
}

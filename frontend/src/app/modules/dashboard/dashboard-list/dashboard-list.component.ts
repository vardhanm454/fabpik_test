import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/service/authentication/authentication.service";
import { ShopService } from "src/app/service/shop/shop.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ConversationService } from "src/app/service/conversation/conversation.service";
import { PurchaseHistoryService } from "src/app/service/purchasehistoryservice/purchasehistoryservice.service";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { DataService } from "src/app/service/data/data.service";
@Component({
  selector: "app-dashboard-list",
  templateUrl: "./dashboard-list.component.html",
  styleUrls: ["./dashboard-list.component.scss"],
  host: {
    "(window:resize)": "onWindowResize($event)",
  },
})
export class DashboardListComponent implements OnInit {
  cartcount: any = 0;
  customerprofile: any;
  feedbackform: FormGroup;
  height: number = window.innerHeight;
  isMobile: boolean = false;
  isDesktop: boolean = false;
  id: number;
  loading = true;
  mobileWidth: number = 767;
  nextDeliveryCount: any = 0;
  orderInproessCount: any = 0;
  submitted = false;
  width: number = window.innerWidth;
  wishlistcount: any = 0;

  public headername: string = "Dashboard";

  constructor(
    private authenticationService: AuthenticationService,
    private shopService: ShopService,
    private formBuilder: FormBuilder,
    public authService: AuthenticationService,
    private router: Router,
    private angularnotifierService: AngularnotifierService,
    private conversationService: ConversationService,
    private purchasehistoryService: PurchaseHistoryService,
    public dataService: DataService
  ) {}

  ngOnInit(): void {
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
    var values = JSON.parse(localStorage.getItem("customer"));
    this.id = values.id;
    this.myaccount(this.id);
    this.getWishlistCount();
    this.getCartCount();
    // this.OrderInprocessCount();
    // this.NextDeliveryCount();
    this.purchasehistoryService.userOrdersInfoCount().subscribe((res: any) => {
      this.orderInproessCount = res.ordersInProcess;
      this.nextDeliveryCount = res.nextDelivery;
    });
    this.feedbackform = this.formBuilder.group({
      feedback: ["", Validators.required],
    });
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }

  // myaccount (user details)
  private myaccount(id) {
    this.authenticationService.getById(id).subscribe((res) => {
      this.customerprofile = res;
      this.loading = false;
    });
  }

  //get wishlist count
  getWishlistCount() {
    this.dataService.currentWishlistCount.subscribe((res:any) => {
      if(res.success){
        this.wishlistcount = res.count;
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
    });
  }

  //get cart count
  getCartCount() {
    this.dataService.currentCartCount.subscribe((res) => {
      if(res.success){
        this.cartcount = res.count;
      }else{
        this.angularnotifierService.showNotification(
          "error",
          res.msg
        );
      }
    });
  }

  orderfilter(value) {
    localStorage.setItem("order_inprocess", value);
    this.router.navigate(["/myaccount/myorders"]);
  }

  get f() {
    return this.feedbackform.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.feedbackform.invalid) {
      return;
    }

    this.loading = true;
    this.conversationService.feedback(this.feedbackform.value).subscribe({
      next: (res:any) => {
        if(res.success){
          this.angularnotifierService.showNotification(
            "default",
            "Feedback save Successfully"
          );
          this.feedbackform.reset();
        }else{
          this.angularnotifierService.showNotification(
            "default",
            res.msg
          );
        }
        this.submitted = false;
        this.loading = false;
      },
      error: (error) => {
        var err = JSON.stringify(error);
        this.angularnotifierService.showNotification("error", err);
        this.loading = false;
      },
    });
  }
}

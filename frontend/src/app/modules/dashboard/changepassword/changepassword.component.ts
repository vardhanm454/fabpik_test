import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/_helpers/must-match.validator';
import { AuthenticationService } from 'src/app/service/authentication/authentication.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AngularnotifierService } from 'src/app/service/angularnotifier/angularnotifier.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss'],
  host: {
    "(window:resize)": "onWindowResize($event)"
  }
})
export class ChangepasswordComponent implements OnInit {

  isMobile: boolean = false;
  isDesktop: boolean = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth: number = 767;
  changePasswordForm: FormGroup;
  public headername: string = 'Change Password';
  errorMessages = {
    oldpassword: [
      { type: "required", message: "Password is required." },
      { type: "minlength", message: "Should be minimum 8 characters." },
      { type: "maxlength", message: "Should be maximum 30 characters." }
    ],
    newpassword: [
      { type: "required", message: "Password is required." },
      { type: "minlength", message: "Should be minimum 8 characters." },
      { type: "maxlength", message: "Should be maximum 30 characters." }
    ],
    confirmPassword: [
      { type: "required", message: "Confirm password is required." },
      { type: "minlength", message: "Should be minimum 8 characters." },
      { type: "maxlength", message: "Should be maximum 30 characters." },
      {
        type: "mustMatch",
        message: "Confirm password & password must be same."
      }
    ]
  };
  otpverifyform: FormGroup;
  constructor(private _location: Location,
              private formBuilder: FormBuilder,
              public authService:AuthenticationService,
              private angularnotifierService: AngularnotifierService,
              public modalService: NgbModal
    ) {
      this.otpverifyform = this.formBuilder.group({
        otp1: ['', [Validators.required, Validators.minLength(1)]],
        otp2: ['', [Validators.required, Validators.minLength(1)]],
        otp3: ['', [Validators.required, Validators.minLength(1)]],
        otp4: ['', [Validators.required, Validators.minLength(1)]]
      });
     }
  goback() {
    this._location.back();
  }

  ngOnInit() {
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
    this.changePasswordForm = this.formBuilder.group(
      {
        oldpassword: [
          "",
          [
            Validators.required,
          ]
        ],
        newpassword: [
          "",
          [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(20)
          ]
        ],
        confirmPassword: [
          "",
          [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(20)
          ]
        ]
      },
      {
        validator: MustMatch("newpassword", "confirmPassword")
      }
    );
  }
  
  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }

  submit(){

    this.authService.change_password(this.changePasswordForm.value).subscribe((res:any)=>{
      if(res.success){
        // this.alertService.success('Updated!',res.msg);
        this.angularnotifierService.showNotification('default',res.msg);
        this.changePasswordForm.reset()
      }else{
        // this.alertService.warning('Failed!',res.msg)
        this.angularnotifierService.showNotification('error',res.msg);
      }
    })
  }
  changePassword(Otpverify:any) {

  
    // this.modalService.open(Otpverify, {
    //   centered: true,
    //   size: 'md',
    //   animation: true,
    //   windowClass: 'bottom-modal   dark1-modal'
    // });
    
  }

  close() {
    this.modalService.dismissAll();
  }

}

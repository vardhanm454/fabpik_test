import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardListComponent } from "./dashboard-list/dashboard-list.component";
import { OrderDetailsComponent } from "./orders/order-details/order-details.component";
import { OrdersListComponent } from "./orders/orders-list/orders-list.component";
import { AddressesComponent } from "./addresses/addresses.component";
import { ManageAddressComponent } from "./manage-address/manage-address.component";
import { ManageProfileComponent } from "./manage-profile/manage-profile.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { ConversationsComponent } from "./conversations/conversations.component";
// import { SupportComponent } from '../pages/coupons/support/support.component';
import { ChatComponent } from "./chat/chat.component";
import { SharedModule } from "src/app/shared/shared.module";
import { OrderHelpComponent } from "./orders/order-help/order-help.component";
import { NgxSkeletonLoaderModule } from "ngx-skeleton-loader";
import { ChangepasswordComponent } from "./changepassword/changepassword.component";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NotificationsComponent } from './notifications/notifications.component';
import { PaymentlistComponent } from './paymentlist/paymentlist.component';

@NgModule({
  declarations: [
    OrderDetailsComponent,
    OrdersListComponent,
    AddressesComponent,
    ManageAddressComponent,
    ManageProfileComponent,
    ConversationsComponent,
    ChatComponent,
    OrderHelpComponent,
    ChangepasswordComponent,
    NotificationsComponent,
    PaymentlistComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    GooglePlaceModule,
    NgxSkeletonLoaderModule.forRoot(),
  ],
  exports: [DashboardListComponent],
})
export class DashboardModule {}

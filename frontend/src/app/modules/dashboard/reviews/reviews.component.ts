import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { ReviewService } from "src/app/service/review/review.service";
import { ShopService } from "src/app/service/shop/shop.service";
declare var $: any;

@Component({
  selector: "app-reviews",
  templateUrl: "./reviews.component.html",
  styleUrls: ["./reviews.component.scss"],
  host: {
    "(window:resize)": "onWindowResize($event)",
  },
})
export class ReviewsComponent implements OnInit {
  currentRate: any = 0;
  isMobile: boolean = false;
  isDesktop: boolean = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  reviewForm: FormGroup;
  reviewEditForm: FormGroup;
  mobileWidth: number = 767;
  public review: boolean = true;
  public reviewed: boolean = false;
  reviews: any = [];
  currentPid: any;
  currentPvid: any;
  editIndex: any;
  public headername: string = "Review Purchases";
  child_order_id: any;

  constructor(
    public modalService: NgbModal,
    public shopService: ShopService,
    private reviewService: ReviewService,
    private formBuilder: FormBuilder,
    public angularnotifierService:AngularnotifierService
  ) {
    this.reviewForm = this.formBuilder.group({
      review_comment: ["", [Validators.required]],
      review_title: ["", [Validators.required]],
    });
    this.reviewEditForm = this.formBuilder.group({
      review_comment: ["", [Validators.required]],
      review_title: ["", [Validators.required]],
    });
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('checkout-page');
  }

  ngOnDestroy(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('checkout-page');
  }
  public products = [
    {
      image: "assets/images/products/1.jpg",
      name: "Baby Sleepsuit",
      storename: "storename",
      saleprice: "2999",
      originalprice: "3999",
      offerpercentage: "20%",
      value: "published",
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Cute Llama printed kimono style onesie",
      storename: "storename",
      saleprice: "2999",
      originalprice: "3999",
      offerpercentage: "20%",
      value: "pending",
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Baby Sleepsuit",
      saleprice: "2999",
      storename: "storename",
      originalprice: "3999",
      offerpercentage: "20%",
      value: "published",
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Cute Llama printed kimono style onesie",
      storename: "storename",
      saleprice: "2999",
      originalprice: "3999",
      offerpercentage: "20%",
      value: "pending",
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Baby Sleepsuit",
      saleprice: "2999",
      storename: "storename",
      originalprice: "3999",
      offerpercentage: "20%",
      value: "published",
    },
    {
      image: "assets/images/products/1.jpg",
      name: "Cute Llama printed kimono style onesie",
      storename: "storename",
      saleprice: "2999",
      originalprice: "3999",
      offerpercentage: "20%",
      value: "pending",
    },
  ];

  ngOnInit(): void {
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
    this.shopService.getCustomerReviews().subscribe((res: any) => {
      this.reviews = res.reviews;
    });
  }

  writeReview(review, pid?, pv_id?, child_order_id?) {
    this.currentPid = pid;
    this.currentPvid = pv_id;
    this.child_order_id = child_order_id;
    this.modalService.open(review, {
      centered: true,
      size: "md",
      animation: true,
      windowClass: "coupons-modal dark1-modal",
      backdrop: "static",
      keyboard: false,
    });
  }

  close() {
    this.modalService.dismissAll();
  }
  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }

  getReview() {
    return this.reviews.filter((review) => review.id == null);
  }

  getReviewed() {
    return this.reviews.filter((review) => review.id != null);
  }

  submitReview() {
    var data = this.reviewForm.value;
    data.product_id = this.currentPid;
    data.product_variant_id = this.currentPvid;
    data.rating = this.currentRate;
    data.child_order_id = this.child_order_id;
    this.reviewService.addreview(data).subscribe((res: any) => {
      if(res.success){
        this.reviewForm.reset();
        this.currentRate = 0;
        this.modalService.dismissAll();
        this.shopService.getCustomerReviews().subscribe((res: any) => {
          if(res.success){
            this.reviews = res.reviews;
          }else{
            this.angularnotifierService.showNotification("error", res.msg);
          }
        });
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
    });
  }

  submitEditReview() {
    var data = this.reviewEditForm.value;
    data.review_id = this.reviews[this.editIndex].id;
    data.rating = this.currentRate;
    this.reviewService.editReview(data).subscribe((res: any) => {
      if(res.success){
        this.shopService.getCustomerReviews().subscribe((res: any) => {
          if(res.success){
            this.reviews = res.reviews;
          }else{
            this.angularnotifierService.showNotification("error", res.msg);
          }
        });
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
      this.modalService.dismissAll();
    });
  }

  editReviewModal(index) {
    this.editIndex = index;
    this.reviewEditForm.patchValue({
      review_title: this.reviews[index].review_title,
      review_comment: this.reviews[index].review_comment,
    });
    this.currentRate = this.reviews[index].rating;
  }
}

import { Component, OnInit } from "@angular/core";
import "sweetalert2/src/sweetalert2.scss";
import Swal from "sweetalert2";
import { PurchaseHistoryService } from "src/app/service/purchasehistoryservice/purchasehistoryservice.service";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
// import {environment} from '../../../'
import { environment } from "src/environments/environment";
import { ActivatedRoute } from "@angular/router";

declare var require: any;
const FileSaver = require("file-saver");

@Component({
  selector: "app-order-details",
  templateUrl: "./order-details.component.html",
  styleUrls: ["./order-details.component.scss"],
  host: {
    "(window:resize)": "onWindowResize($event)",
  },
})
export class OrderDetailsComponent implements OnInit {
  orderDetails: any;
  AllshippingAddress: any;
  AllorderSummaryStatus: any;
  AllorderSummary: any;
  appliedCoupon = null;
  canceldata: any;
  handlingCharges;
  loading: Boolean = true;
  height: number = window.innerHeight;
  isMobile: boolean = false;
  isDesktop: boolean = false;
  mobileWidth: number = 767;
  child_order_id: any;
  orderTotal = 0;
  totalMRP = 0;
  orderStatuses: any = [];
  showReturnOption: Boolean;
  shippingStatuses: any = [];
  totalSellingPrice = 0;
  discount: any = 0;
  tax = 0;
  width: number = window.innerWidth;

  public headername: string = "Order Details";

  constructor(
    private purchasehistoryService: PurchaseHistoryService,
    private angularnotifierService: AngularnotifierService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
    this.child_order_id = localStorage.getItem("child_order_id");

    this.getProductDetails(this.route.snapshot.paramMap.get("child_order_id"));
    // this.getshippingAddress(this.child_order_id);
    // this.getOrderSummary(this.child_order_id);
  }

  cancelOrder(id) {
    if (this.orderStatuses.indexOf(2) > -1) {
      return;
    }
    if (this.shippingStatuses.indexOf(4) > -1) {
      // this.alertService.warning("Failed","You cannot cancel the order!")
      this.angularnotifierService.showNotification(
        "error",
        "You cannot cancel the order!"
      );
      return;
    } else {
      Swal.fire({
        title: "Are you sure you want to cancel this order?",
        showClass: {
          popup: "swal-wish-wide animated fadeInDown faster",
        },
        cancelButtonColor: "#e42b5a08",
        confirmButtonColor: "#e42b5abf",
        showCloseButton: true,
        showCancelButton: true,
      }).then((check) => {
        if (check.value) {
          this.canceldata = {
            id: id,
          };
          this.purchasehistoryService
            .cancelorder(this.canceldata)
            .subscribe((res: any) => {
              if (res.success) {
                Swal.fire({
                  title: "Yes canceled!",
                  text: "The order has been canceled.",
                  icon: "success",

                  confirmButtonColor: "#e42b5abf",
                  confirmButtonText: "Okay",

                  showClass: {
                    popup: "swal-wish-wide animated fadeInDown faster",
                  },
                });
                this.getProductDetails(
                  this.route.snapshot.paramMap.get("child_order_id")
                );
              } else {
                Swal.fire({
                  title: "Failed!",
                  text:
                    "There is a problem with the order cancellation please contact support team!.",
                  icon: "error",

                  confirmButtonColor: "#e42b5abf",
                  confirmButtonText: "Okay",

                  showClass: {
                    popup: "swal-wish-wide animated fadeInDown faster",
                  },
                });
              }
            });
        } else if (check.dismiss === Swal.DismissReason.cancel) {
        }
      });
    }
  }

  returnOrder(id) {
    if (this.orderStatuses.indexOf(5) > -1) {
      //  this.alertService.warning("Return","Return has already been requested");
      return;
    }
    if (this.shippingStatuses.indexOf(5) > -1 && this.showReturnOption) {
      Swal.fire({
        title: "Are you sure you want to return this product?",
        showClass: {
          popup: "swal-wish-wide animated fadeInDown faster",
        },
        cancelButtonColor: "#e42b5a08",
        confirmButtonColor: "#e42b5abf",
        showCloseButton: true,
        showCancelButton: true,
      }).then((check) => {
        if (check.value) {
          const returnOrderdata = {
            id: id,
          };
          this.purchasehistoryService
            .returnOrder(returnOrderdata)
            .subscribe((res: any) => {
              if (res.success) {
                Swal.fire({
                  title: "Return!",
                  text: "Return Request Successful!.",
                  icon: "success",

                  confirmButtonColor: "#e42b5abf",
                  confirmButtonText: "Okay",

                  showClass: {
                    popup: "swal-wish-wide animated fadeInDown faster",
                  },
                });
                this.getProductDetails(
                  this.route.snapshot.paramMap.get("child_order_id")
                );
              }
            });
        } else if (check.dismiss === Swal.DismissReason.cancel) {
        }
      });
    } else {
      return;
    }
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }

  downloadInvoice(id) {
    this.purchasehistoryService.downloadInvoice(id).subscribe((res: any) => {
      if (res.success) {
        const fileName = id + ".pdf";
        FileSaver.saveAs(res.url, fileName);
      }
    });
  }

  getProductDetails(child_order_id) {
    this.purchasehistoryService
      .getproductdetailsById(child_order_id)
      .subscribe((res: any) => {
        this.loading = false;
        this.orderDetails = res.order;
        this.discount = res.discount;
        this.orderStatuses = res.order_statuses;
        this.shippingStatuses = res.shipping_statuses;
        this.showReturnOption = res.showReturnOption;
        this.totalMRP = res.totalMrp;
        this.totalSellingPrice = res.totalSellingPrice;
        if (res.coupon) {
          this.appliedCoupon = res.coupon;
        }
        this.orderTotal = res.totalOrderAmount;
        this.handlingCharges = res.handling_charges;
      });
  }

  getshippingAddress(child_order_id) {
    this.purchasehistoryService
      .getshippingAddress(child_order_id)
      .subscribe((res) => {
        this.AllshippingAddress = res;
      });
  }

  getOrderSummary(child_order_id) {
    this.purchasehistoryService
      .getOrderSummary(child_order_id)
      .subscribe((res: any) => {
        this.AllorderSummaryStatus = res.status;
        this.AllorderSummary = res.order_summary;
      });
  }
}

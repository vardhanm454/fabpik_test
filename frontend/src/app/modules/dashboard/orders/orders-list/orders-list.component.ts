import { Component, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { PurchaseHistoryService } from "src/app/service/purchasehistoryservice/purchasehistoryservice.service";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
declare var $: any;

@Component({
  selector: "app-orders-list",
  templateUrl: "./orders-list.component.html",
  styleUrls: ["./orders-list.component.scss"],
  host: {
    "(window:resize)": "onWindowResize($event)",
  },
})
export class OrdersListComponent implements OnInit {
  Allorderlist: any;
  Allorderstatuslist: any = [
    { type: "All" },
    { type: "Returned" },
    { type: "Order in Process" },
    { type: "Delivered" },
    { type: "Cancelled" },
  ];
  Allfilterorderlist: any;
  filterbystatus: any;
  filterclass: any;
  filters: any;
  height: number = window.innerHeight;
  sortBy: any = "ALL";
  isMobile: boolean = false;
  isDesktop: boolean = false;
  listing: any;
  mobileWidth: number = 767;
  searchText: any;
  searchform: FormGroup;
  value: any;
  width: number = window.innerWidth;
  loading: boolean = true;
  private _jsonURL = "assets/js/filters.json";
  public headername: string = "My Orders";
  public statustypes = [
    { color: "#ffbdbd", type: "Returned" },
    { color: "#8ca5ff", type: "Order in Process" },
    { color: "#a7ffbb", type: "Delivered" },
    { color: "#ffbd86", type: "Cancelled" },
  ];

  constructor(
    public http: HttpClient,
    public modalService: NgbModal,
    private purchaseHistoryService: PurchaseHistoryService,
    private router: Router,
    private formBuilder: FormBuilder,
    public angularnotifierService:AngularnotifierService
  ) {
    this.getJSON().subscribe((data) => {
      this.filters = data;
    });
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('bgcategory-light');
  }
  ngOnDestroy(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('bgcategory-light');
  }
  ngOnInit(): void {
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
    localStorage.removeItem("child_order_id");
    this.getOrderList();
    this.searchform = this.formBuilder.group({
      orderId: [""],
    });
    if (localStorage.getItem("order_inprocess")) {
      this.filterbyOrderStatus(localStorage.getItem("order_inprocess"));
    }
  }

  public getJSON(): Observable<any> {
    return this.http.get(this._jsonURL);
  }

  filterPopup() {
    this.filterclass = !this.filterclass;
  }

  // searchIcon(e){
  //   e.preventDefault();
  // }

  close() {
    this.modalService.dismissAll();
  }

  openSortwidget(sortwidget) {
    this.modalService.open(sortwidget, {
      centered: true,
      size: "md",
      animation: true,
      windowClass: "bottom-modal",
      keyboard: false,
    });
  }
  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }

  // list of review product
  getOrderList() {
    this.purchaseHistoryService.getOrderList(this.sortBy).subscribe((res:any) => {
      if(res.success){
        this.Allorderlist = res;
      }else{
        this.angularnotifierService.showNotification(
          "default",
          res.msg
        );
      }
      this.loading = false;
    });
  }

  onSubmit() {
    this.listing = {
      orderId: this.searchform.value.orderId,
    };
    this.purchaseHistoryService
      .searchByorderid(this.listing)
      .subscribe((res) => {
        this.Allorderlist = res;
        this.searchform.reset();
      });
  }

  getorderId(id) {
    localStorage.setItem("child_order_id", id);
  }

  filterbyOrderStatus(value) {
    this.sortBy = value;
    this.getOrderList();
  }
  //   this.filterbystatus = {
  //     id: value
  //   }
  //   this.value = value
  //   this.purchaseHistoryService.filterbyOrderStatus(this.filterbystatus)
  //   .subscribe(res => {
  //     this.Allorderlist = res;
  //     });
  // }
}

import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-order-help',
  templateUrl: './order-help.component.html',
  styleUrls: ['./order-help.component.scss']
})
export class OrderHelpComponent implements OnInit {

  constructor( private _location: Location) { }
  goback() {
    this._location.back();
  }
  ngOnInit(): void {
  }

}

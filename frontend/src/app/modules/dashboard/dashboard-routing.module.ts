import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddressesComponent } from "./addresses/addresses.component";
import { DashboardListComponent } from "./dashboard-list/dashboard-list.component";
import { DashboardComponent } from "./dashboard.component";
import { ManageAddressComponent } from "./manage-address/manage-address.component";
import { OrdersListComponent } from "./orders/orders-list/orders-list.component";
import { OrderDetailsComponent } from "./orders/order-details/order-details.component";
import { ManageProfileComponent } from "./manage-profile/manage-profile.component";
import { ReviewsComponent } from "./reviews/reviews.component";
// import { SupportComponent } from '../pages/coupons/support/support.component';
import { ConversationsComponent } from "./conversations/conversations.component";
import { ChatComponent } from "./chat/chat.component";
import { OrderHelpComponent } from "./orders/order-help/order-help.component";
import { ChangepasswordComponent } from "./changepassword/changepassword.component";
import { NotificationsComponent } from "./notifications/notifications.component";
import { PaymentlistComponent } from "./paymentlist/paymentlist.component";

const routes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    data: { breadcrumb: "Home" },
  },
  {
    path: "/dashboard",
    component: DashboardComponent,
  },
  {
    path: "dashboard-list",
    component: DashboardListComponent,
  },
  {
    path: "addresses",
    component: AddressesComponent,
  },
  {
    path: "manage-address",
    component: ManageAddressComponent,
  },
  {
    path: "manage-address/:id",
    component: ManageAddressComponent,
  },
  {
    path: "myorders",
    component: OrdersListComponent,
  },
  {
    path: "mypayments",
    component: PaymentlistComponent,
  },
  {
    path: "orders/order-details/:child_order_id",
    component: OrderDetailsComponent,
  },
  {
    path: "order-help",
    component: OrderHelpComponent,
  },
  {
    path: "manage-profile",
    component: ManageProfileComponent,
  },
  {
    path: "reviews",
    component: ReviewsComponent,
  },
  {
    path: "notifications",
    component: NotificationsComponent,
  },
  {
    path: "conversations",
    component: ConversationsComponent,
  },
  {
    path: "changepassword",
    component: ChangepasswordComponent,
  },
  {
    path: "chat",
    component: ChatComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}

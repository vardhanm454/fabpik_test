import { HttpClient } from "@angular/common/http";
import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { AngularnotifierService } from "src/app/service/angularnotifier/angularnotifier.service";
import { ShopService } from "src/app/service/shop/shop.service";

@Component({
  selector: "app-notifications",
  templateUrl: "./notifications.component.html",
  styleUrls: ["./notifications.component.scss"],
})
export class NotificationsComponent implements OnInit {
  headername: string = "Notifications";
  isMobile: boolean = false;
  isDesktop: boolean = false;
  loading: boolean = true;
  bg: boolean = true;
  Allsellerlist: any = [];
  isOpen: boolean;
  @Output() changed = new EventEmitter<boolean>();
  notificationsURL = "/assets/js/notifications.json";
  notifications;
  constructor(private http: HttpClient,
              public shopService: ShopService,
              public angularnotifierService:AngularnotifierService) {
    this.getNotifications();
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('checkout-page');
  }

  ngOnDestroy(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('checkout-page');
  }
  checked: boolean = false;
  getNotifications() {
    this.shopService.getNotifications().subscribe((res: any) => {
      if(res.success){
        this.notifications = res.notifications;
        this.loading = false;
      }else{
        this.angularnotifierService.showNotification("error", res.msg);
      }
    });
  }
  ngOnInit(): void {}
}

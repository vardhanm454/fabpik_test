import { Component, OnInit } from '@angular/core';
import { ConversationService } from 'src/app/service/conversation/conversation.service';

@Component({
  selector: 'app-conversations',
  templateUrl: './conversations.component.html',
  styleUrls: ['./conversations.component.scss'],
  host: {
    "(window:resize)": "onWindowResize($event)"
  }
})
export class ConversationsComponent implements OnInit {

  isMobile: boolean = false;
  isDesktop: boolean = false;
  width: number = window.innerWidth;
  height: number = window.innerHeight;
  mobileWidth: number = 767;
  public headername = "Converations";
  Allsellerlist:any;
  loading: boolean = true;

  constructor( private conversationService : ConversationService)
   {
      const body = document.getElementsByTagName('body')[0];
      body.classList.add('bgcategory-light'); 
   }

  ngOnDestroy(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('bgcategory-light');
  }
  public conversations = [
    {
      name: 'Chat 1',
      status: 'Open',
      Date: '3rd Oct',
      firstletter: 'R',
      avatar_bg: 'bg-soft-warning',
      status_text_color: 'text-primary'
    },
    {
      name: 'Chat 2',
      status: 'Closed',
      Date: '6th Nov',
      firstletter: 'B',
      avatar_bg: 'bg-soft-info',
      status_text_color: 'text-danger'
    },
    {
      name: 'Chat 3',
      status: 'Open',
      Date: '3rd Oct',
      firstletter: 'F',
      avatar_bg: 'bg-soft-primary',
      status_text_color: 'text-primary'
    },
    {
      name: 'Chat 4',
      status: 'Closed',
      Date: 'Wed',
      firstletter: 'S',
      avatar_bg: 'bg-soft-danger',
      status_text_color: 'text-danger'
    },
    {
      name: 'Chat 5',
      status: 'Closed',
      Date: 'Tue',
      firstletter: 'R',
      avatar_bg: 'bg-soft-success',
      status_text_color: 'text-danger'
    },


  ]

  ngOnInit(): void {
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
    this.conversationList();
  }

  conversationList(){
    this.conversationService.conversationList()
    .subscribe(res => {
      this.Allsellerlist = res;
      this.loading = false;
      });
  }

  sellerdetails(id){
    localStorage.setItem('sellerid',id);
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }
}

import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AuthenticationService } from 'src/app/service/authentication/authentication.service';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  host: {
    "(window:resize)":"onWindowResize($event)"
  }
})
export class DashboardComponent implements OnInit {

  isMobile: boolean = false;
  isDesktop: boolean = false;
  width:number = window.innerWidth;
  height:number = window.innerHeight;
  mobileWidth:number  = 767;

  public headername: string = 'Dashboard';
  customerprofile: any;
  id: number;
  loading: boolean = true;
  
  constructor( private _location: Location,
               private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    var values = JSON.parse(localStorage.getItem("customer"));  
    this.id  = values.id;
    this.myaccount(this.id);

    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }
  
  goback() {
    this._location.back();
  }

  // logout 
  logout() {
    this.authenticationService.logout();
  }

  // myaccount (user details)
  private myaccount(id) {
    this.authenticationService.getById(id)
    .subscribe(res => {
      this.customerprofile = res;
      this.loading = false;
    });
  }

  onWindowResize(event) {
    this.width = event.target.innerWidth;
    this.height = event.target.innerHeight;
    this.isMobile = this.width < this.mobileWidth;
    this.isDesktop = this.width > this.mobileWidth;
  }

}

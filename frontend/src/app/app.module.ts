import { BrowserModule } from "@angular/platform-browser";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from "@angular/platform-browser/animations";
import {
  MatFormFieldModule,
  MatInputModule,
  MatTabsModule,
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
} from "@angular/material";
import { MatDialogModule } from "@angular/material/dialog";
import { AppRoutingModule } from "./app-routing.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { WishlistComponent } from "./modules/pages/wishlist/wishlist.component";
import { HomeComponent } from "./modules/pages/home/home.component";
import { DashboardComponent } from "./modules/dashboard/dashboard.component";
import { ProductlistComponent } from "./modules/pages/productlist/productlist.component";
import { ProductdetailsComponent } from "./modules/pages/productdetails/productdetails.component";
import { CategoryComponent } from "./modules/pages/category/category.component";
import { SubcategoryComponent } from "./modules/pages/subcategory/subcategory.component";
import { SubSubcategoryComponent } from "./modules/pages/sub-subcategory/sub-subcategory.component";
import { CouponsComponent } from "./modules/pages/coupons/coupons.component";
import { SearchComponent } from "./modules/pages/search/search.component";
import { HomeMobileComponent } from "./modules/pages/home-mobile/home-mobile.component";
import { OffersComponent } from "./modules/pages/offers/offers.component";
import { ProductItemComponent } from "./modules/shop/product-item/product-item.component";
import { SupportComponent } from "./modules/pages/support/support.component";
import { PoliciesComponent } from "./modules/pages/policies/policies.component";
import { NotfoundComponent } from "./modules/pages/notfound/notfound.component";
import { SellerShopComponent } from "./modules/seller-shop/seller-shop.component";
import { LegalComponent } from "./modules/pages/legal/legal.component";
import { OrderConfirmComponent } from "./modules/pages/order-confirm/order-confirm.component";

/*plugins*/
import { NgxImgZoomModule } from "ngx-img-zoom";
import { NgxSpinnerModule } from "ngx-spinner";
import { HighlightSearchPipe } from "./highlight-search.pipe";
import { PhotoGalleryModule } from "@twogate/ngx-photo-gallery";
import { SlickCarouselModule } from "ngx-slick-carousel";
import { NotifierModule, NotifierOptions } from "angular-notifier";
import { GoogleTagManagerModule } from "angular-google-tag-manager";
import { RouterModule } from "@angular/router";
import { ShareButtonsModule } from "ngx-sharebuttons/buttons";
import { ShareIconsModule } from "ngx-sharebuttons/icons";
import { LightboxModule } from "ngx-lightbox";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CarouselModule } from "ngx-owl-carousel-o";
import { SharedModule } from "./shared/shared.module";
import { TokenInterceptor } from "./helpers/token.interceptor";
import { ErrorInterceptor } from "./helpers/error.interceptor";
import { fakeBackendProvider } from "./helpers/fake-backend.intercerptor";
import { NgxSkeletonLoaderModule } from "ngx-skeleton-loader";
import { NgxSliderModule } from "@angular-slider/ngx-slider";
import { IndexPipe } from "./index.pipe";
import { LazyLoadImageModule } from "ng-lazyload-image";
import { AngularFireModule } from "@angular/fire";
import { AngularFireMessagingModule } from "@angular/fire/messaging";
import { SizeChartComponent } from "../app/shared/size-chart/size-chart.component";
import {
  FullscreenOverlayContainer,
  OverlayContainer,
} from "@angular/cdk/overlay";
import { environment } from "src/environments/environment";
import { FeedbackformComponent } from "./modules/pages/feedbackform/feedbackform.component";

const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: "right",
      distance: 12,
    },
    vertical: {
      position: "bottom",
      distance: 12,
      gap: 10,
    },
  },
  theme: "material",
  behaviour: {
    autoHide: 3000,
    onClick: "hide",
    onMouseover: "pauseAutoHide",
    showDismissButton: true,
    stacking: 1,
  },
  animations: {
    enabled: true,
    show: {
      preset: "slide",
      speed: 300,
      easing: "ease",
    },
    hide: {
      preset: "fade",
      speed: 300,
      easing: "ease",
      offset: 50,
    },
    shift: {
      speed: 300,
      easing: "ease",
    },
    overlap: 150,
  },
};
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    ProductlistComponent,
    ProductdetailsComponent,
    CouponsComponent,
    WishlistComponent,
    SearchComponent,
    HomeMobileComponent,
    OffersComponent,
    ProductItemComponent,
    IndexPipe,
    CategoryComponent,
    SubcategoryComponent,
    SubSubcategoryComponent,
    SupportComponent,
    SellerShopComponent,
    OrderConfirmComponent,
    HighlightSearchPipe,
    PoliciesComponent,
    NotfoundComponent,
    LegalComponent,
    SizeChartComponent,
    FeedbackformComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "serverApp" }),
    AppRoutingModule,
    NgbModule,
    RouterModule,
    CarouselModule,
    BrowserAnimationsModule,
    MatTabsModule,
    HttpClientModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    SharedModule,
    ReactiveFormsModule,
    GoogleTagManagerModule.forRoot({
      id: "GTM-TQ6WMPC",
    }),
    InfiniteScrollModule,
    MatDialogModule,
    NoopAnimationsModule,
    NgxSliderModule,
    NgxSkeletonLoaderModule.forRoot(),
    LightboxModule,
    ShareButtonsModule.withConfig({
      debug: true,
    }),
    ShareIconsModule,
    SlickCarouselModule,
    NotifierModule.withConfig(customNotifierOptions),
    NgxImgZoomModule,
    NgxSpinnerModule,
    LazyLoadImageModule,
    PhotoGalleryModule.forRoot({
      defaultOptions: {
        closeOnScroll: false,
        mainClass: "product-Zoom",
      },
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireMessagingModule,
  ],
  exports: [
    CarouselModule,
    MatTabsModule,
    MatInputModule, 
    MatFormFieldModule,
    MatDialogModule,
    WishlistComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: "fill" },
    },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: OverlayContainer, useClass: FullscreenOverlayContainer },
    fakeBackendProvider,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

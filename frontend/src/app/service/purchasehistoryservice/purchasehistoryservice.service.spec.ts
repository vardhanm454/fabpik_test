import { TestBed } from '@angular/core/testing';

import { PurchasehistoryserviceService } from './purchasehistoryservice.service';

describe('PurchasehistoryserviceService', () => {
  let service: PurchasehistoryserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PurchasehistoryserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

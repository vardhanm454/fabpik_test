import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
@Injectable({
  providedIn: "root",
})
export class PurchaseHistoryService {
  constructor(private http: HttpClient, private router: Router) {}

  getOrderList(sortBy) {
    return this.http.post(
      `${environment.apiUrl}/purchasehistory/get_order_list`,
      { sortBy }
    );
  }
  filterbyOrderStatus(params) {
    return this.http.get(
      `${environment.apiUrl}/purchasehistory/get_filter_bystatus`,
      { params: params }
    );
  }
  searchByorderid(params) {
    return this.http.get(
      `${environment.apiUrl}/purchasehistory/get_search_byId`,
      { params: params }
    );
  }
  orderStatus() {
    return this.http.get(
      `${environment.apiUrl}/purchasehistory/get_order_status_list`
    );
  }
  getproductdetailsById(id) {
    return this.http.get(
      `${environment.apiUrl}/purchasehistory/get_product_details_byid/${id}`
    );
  }
  getshippingAddress(id) {
    return this.http.get(
      `${environment.apiUrl}/purchasehistory/get_shippingaddress_byid/${id}`
    );
  }
  getOrderSummary(id) {
    return this.http.get(
      `${environment.apiUrl}/purchasehistory/get_order_summary_byid/${id}`
    );
  }
  cancelorder(params) {
    return this.http.put(
      `${environment.apiUrl}/purchasehistory/cancel_order`,
      params
    );
  }
  returnOrder(params) {
    return this.http.put(
      `${environment.apiUrl}/purchasehistory/return_order`,
      params
    );
  }

  downloadInvoice(id) {
    return this.http.get(
      `${environment.apiUrl}/purchasehistory/download_invoice/${id}`
    );
  }

  customerParentOrders(){
    return this.http.get(
      `${environment.apiUrl}/purchasehistory/customerParentOrders`
    );
  }

  OrderInprocessCount() {
    return this.http.get(
      `${environment.apiUrl}/purchasehistory/get_orderinprocess_count`
    );
  }
  NextDeliveryCount() {
    return this.http.get(
      `${environment.apiUrl}/purchasehistory/get_nextdelivery_count`
    );
  }

  userOrdersInfoCount() {
    return this.http.get(
      `${environment.apiUrl}/purchasehistory/userOrdersInfoCount`
    );
  }
}

import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class DataService {
  constructor() {}

  private registerDetails = new BehaviorSubject<any>({
    email: "",
    name: "",
    mobile: "",
    password: "",
  });
  currentRegisterDetails = this.registerDetails.asObservable();

  private categories = new BehaviorSubject<any>(null);
  currentCategories = this.categories.asObservable();

  private cartCount = new BehaviorSubject<any>(0);
  currentCartCount = this.cartCount.asObservable();

  private notificationsCount = new BehaviorSubject<any>(0);
  currentNotificationsCount = this.notificationsCount.asObservable();

  private wishlistCount = new BehaviorSubject<any>(0);
  currentWishlistCount = this.wishlistCount.asObservable();

  changeRegisterDetails(userData) {
    this.registerDetails.next(userData);
  }

  changeCategories(categories) {
    this.categories.next(categories);
  }

  changeCartCount(cartCount) {
    this.cartCount.next(cartCount);
  }

  changeNotificationsCount(notificationsCount) {
    this.notificationsCount.next(notificationsCount);
  }

  changeWishlishtCount(wishlistCount) {
    this.wishlistCount.next(wishlistCount);
  }
}

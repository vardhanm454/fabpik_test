import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Customer } from "src/app/model/authentication/customer.model";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  private customerSubject: BehaviorSubject<Customer> | any;
  public customer: Observable<Customer>;

  constructor(
    private http: HttpClient,
    private router: Router // private apiUrl= environment.apiUrl
  ) {
    this.customerSubject = new BehaviorSubject<Customer>(
      JSON.parse(localStorage.getItem("customer"))
    );
    this.customer = this.customerSubject.asObservable();
  }

  public get customerValue(): Customer {
    return this.customerSubject.value;
  }

  //login or singup
  login(customer: Customer) {
    return this.http.post<Customer>(
      `${environment.apiAuthUrl}/login`,
      customer
    );
  }

  register(data) {
    return this.http.post(`${environment.apiAuthUrl}/register`, data);
  }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem("customer");
    this.customerSubject.next(null);
    this.router.navigate(["/login"]);
  }

  verifyOtpRegister(data) {
    var apiData = this.http.post(
      `${environment.apiAuthUrl}/verifyOtpRegister`,
      data,
      { withCredentials: true }
    );
    return apiData.pipe(
      map((customer: any) => {
        if (customer.success) {
          localStorage.setItem("customer", JSON.stringify(customer));
          this.customerSubject.next(customer);
          var customer = customer;
          this.router.navigate(["/"]);
          return customer;
        } else {
          return customer;
        }
      })
    );
  }

  forgotPassword(data) {
    return this.http.post(`${environment.apiAuthUrl}/forgot_password`, data);
  }

  change_password(data) {
    return this.http.post(`${environment.apiAuthUrl}/change_password`, data);
  }

  sendMailLoginOtp(data) {
    return this.http.post(`${environment.apiAuthUrl}/sendMailLoginOtp`, data);
  }

  verifyOtpLoginDetails(data) {
    return this.http.post(
      `${environment.apiAuthUrl}/verifyOtpLoginDetails`,
      data
    );
  }

  //otp verification
  verifyOtpLogin(params) {
    var data = this.http.post(
      `${environment.apiAuthUrl}/verifyOtpLogin`,
      params,
      { withCredentials: true }
    );
    return data.pipe(
      map((customer: any) => {
        if (customer.success) {
          localStorage.setItem("customer", JSON.stringify(customer));
          this.customerSubject.next(customer);
          var customer = customer;
          this.router.navigate(["/"]);
          return customer;
        } else {
          return customer;
        }
      })
    );
  }

  loginWithPassword(data) {
    var apiData = this.http.post(
      `${environment.apiAuthUrl}/loginWithPassword`,
      data,
      { withCredentials: true }
    );
    return apiData.pipe(
      map((customer: any) => {
        if (customer.success) {
          localStorage.setItem("customer", JSON.stringify(customer));
          this.customerSubject.next(customer);
          var customer = customer;
          localStorage.removeItem("user");
          localStorage.removeItem("mobile");
          this.router.navigate(["/"]);
          return customer;
        } else {
          return customer;
        }
      })
    );
  }

  resendotp(data) {
    return this.http.post(`${environment.apiAuthUrl}/resendOtp`, data);
  }

  getById(id: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/userProfile`);
  }

  updateprofie(params) {
    return this.http.put(`${environment.apiUrl}/updateProfile`, params);
  }

  tokenExpired(token: string) {
    const expiry = JSON.parse(atob(token.split(".")[1])).exp;
    return Math.floor(new Date().getTime() / 1000) >= expiry;
  }
}

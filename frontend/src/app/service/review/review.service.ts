import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
@Injectable({
  providedIn: "root",
})
export class ReviewService {
  constructor(private http: HttpClient, private router: Router) {}

  getReviewProductList() {
    return this.http.get(`${environment.apiUrl}/reviews/get_review_product`);
  }

  getReviewedProductListReadonly() {
    return this.http.get(
      `${environment.apiUrl}/reviews/get_reviewed_product_readonly`
    );
  }
  addonlyrating(params) {
    return this.http.post(
      `${environment.apiUrl}/reviews/addonlyrating`,
      params
    );
  }
  addreview(params) {
    return this.http.post(`${environment.apiUrl}/reviews/addreview`, params);
  }

  getReadReviewById(id) {
    return this.http.get(
      `${environment.apiUrl}/reviews/get_read_review_byid/${id}`
    );
  }

  editReview(data) {
    return this.http.post(`${environment.apiUrl}/reviews/editReview`, data);
  }
}

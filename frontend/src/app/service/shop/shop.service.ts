import { HttpClient } from "@angular/common/http";
import { Injectable, EventEmitter, Output } from "@angular/core";
import { Router } from "@angular/router";
import { DotsData } from "ngx-owl-carousel-o/lib/models/navigation-data.models";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { AuthenticationService } from "../authentication/authentication.service";

@Injectable({
  providedIn: "root",
})
export class ShopService {
  constructor(
    private http: HttpClient,
    private router: Router,
    public authService: AuthenticationService
  ) {}

  // on click send cat,sub,child id and slug to productlist component
  @Output() aClickedEvent = new EventEmitter<any>();

  Clicked(params) {
    localStorage.setItem("categoryid", params.categoryid);
    localStorage.setItem("categoryslug", params.categoryslug);
    localStorage.setItem("subcategoryid", params.subcategoryid);
    localStorage.setItem("subcategoryslug", params.subcategoryslug);
    localStorage.setItem("childcategoryid", params.childcategoryid);
    localStorage.setItem("childcategoryslug", params.childcategoryslug);
    this.aClickedEvent.emit(params);
  }

  // category list
  getCategory(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/category`);
  }

  // category and subcategory list
  getCategorySubCategory(id: string): Observable<any> {
    return this.http.get(
      `${environment.apiUrl}/category/categorySubCategoryList/${id}`
    );
  }

  // category and subcategory list for mobile
  getCategorySubCategorymobile(id: string): Observable<any> {
    return this.http.get(
      `${environment.apiUrl}/category/categorySubCategoryMobileList/${id}`
    );
  }

  // sub and child category list
  getSubChildCategory(id: string): Observable<any> {
    return this.http.get(
      `${environment.apiUrl}/category/subChildCategoryList/${id}`
    );
  }

  // sub and child category list for mobile
  getSubChildCategorymobile(id: string): Observable<any> {
    return this.http.get(
      `${environment.apiUrl}/category/subChildCategoryMobileList/${id}`
    );
  }

  // category and subcategory slug with chidid
  getCatSubSlug(id: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}/category/catsubSlug/${id}`);
  }

  // get only 5 category list
  getfiveCategory(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/category/fivecategory`);
  }

  // get product and category of suggestion search
  searchautosuggestion(params) {
    return this.http.get(`${environment.apiUrl}/product/autoSuggestionSearch`, {
      params: params,
    });
  }

  //get product or category based on searchcate
  search(params) {
    return this.http.get(`${environment.apiUrl}/product/search`, {
      params: params,
    });
  }

  // product list
  getProductList(params): Observable<any> {
    return this.http.post(`${environment.apiUrl}/product/productList`, params);
  }

  // product sort by
  getProductSortBy(params): Observable<any> {
    return this.http.get(
      `${environment.apiUrl}/product/productList/${params.id}`,
      { params: params }
    );
  }

  // single product details
  getProductDetails(postdata) {
    return this.http.get(
      `${environment.apiUrl}/product/productDetails/${postdata.product_variant_slug}/${postdata.unique_id}`
    );
  }

  nextVariantNavigation(data) {
    return this.http.post(
      `${environment.apiUrl}/product/nextVariantNavigation`,
      data
    );
  }

  topSellingProducts() {
    return this.http.get(`${environment.apiUrl}/product/topSellingProducts`);
  }

  similarProducts(ccid, pv_id) {
    return this.http.get(
      `${environment.apiUrl}/product/similarProducts/${ccid}/${pv_id}`
    );
  }

  deals() {
    return this.http.get(`${environment.apiUrl}/product/deals`);
  }

  // flash deal product
  getFlashDealProduct() {
    return this.http.get(`${environment.apiUrl}/product/flashDeal`);
  }

  // categorywise product list
  CategoryProductlist(id) {
    return this.http.get(`${environment.apiUrl}/product/category/${id}`);
  }

  onlyCategories() {
    return this.http.get(`${environment.apiUrl}/category/onlyCategories`);
  }

  getCartCount() {
    return this.http.get(`${environment.apiUrl}/cart/count`);
  }

  getNotificationsCount() {
    return this.http.get(
      `${environment.apiUrl}/notifications/getNotificationsCount`
    );
  }

  updateVisitedTimeAndDate() {
    return this.http.get(
      `${environment.apiUrl}/notifications/updateVisitedTimeAndDate`
    );
  }

  sliderImages(view) {
    return this.http.get(`${environment.apiUrl}/product/sliderImages/${view}`);
  }

  getCart() {
    return this.http.get(`${environment.apiUrl}/cart`);
  }

  getVirtualCart(data) {
    return this.http.post(`${environment.apiUrl}/coupon/getVirtualCart`, data);
  }

  // add to cart
  addProductToCart(params) {
    return this.http.post(`${environment.apiUrl}/cart/add`, params);
  }

  updateCart(params) {
    return this.http.put(`${environment.apiUrl}/cart/update`, params);
  }

  deleteCartProduct(id) {
    return this.http.delete(`${environment.apiUrl}/cart/remove/${id}`);
  }

  getGuestCart() {
    return this.http.get(`${environment.apiUrl}/guest/cart`, {
      withCredentials: true,
    });
  }

  guestAddToCart(data) {
    return this.http.post(`${environment.apiUrl}/guest/addToCart`, data, {
      withCredentials: true,
    });
  }

  guestCartRemoveItem(data) {
    return this.http.post(`${environment.apiUrl}/guest/removeItem`, data, {
      withCredentials: true,
    });
  }

  guestCartCount() {
    return this.http.get(`${environment.apiUrl}/guest/cartCount`, {
      withCredentials: true,
    });
  }

  guestCoupons() {
    return this.http.get(`${environment.apiUrl}/guest/coupons`, {
      withCredentials: true,
    });
  }

  guestVirtualCart(data) {
    return this.http.post(
      `${environment.apiUrl}/guest/guestVirtualCart`,
      data,
      {
        withCredentials: true,
      }
    );
  }

  getWishlistCount() {
    if (localStorage.getItem("customer") !== null) {
      return this.http.get(`${environment.apiUrl}/customer/wishlists/count`);
    }
  }

  // wishlist listing
  getWishlist(): Observable<any> {
    if (localStorage.getItem("customer") !== null) {
      return this.http.get(`${environment.apiUrl}/customer/wishlists`);
    }
  }

  // wishlist ids
  wishlistIds(): Observable<any> {
    if (localStorage.getItem("customer") !== null) {
      return this.http.get(
        `${environment.apiUrl}/customer/wishlists/variantIds`
      );
    }
  }

  checkProductExistInWishlist(data) {
    return this.http.post(
      `${environment.apiUrl}/customer/wishlists/checkProductExistInWishlist`,
      data
    );
  }

  // add to wishlist
  addToWishlist(pvid, product_id) {
    return this.http.post(`${environment.apiUrl}/customer/wishlists/add`, {
      product_variant_id: pvid,
      product_id: product_id,
    });
  }

  // delete from wishlist
  removewishlist(id) {
    return this.http.delete(
      `${environment.apiUrl}/customer/wishlists/remove/${id}`
    );
  }

  //coupons
  getCoupons() {
    return this.http.get(`${environment.apiUrl}/coupon`);
  }

  getOffers() {
    return this.http.get(`${environment.apiUrl}/coupon/get_all_offers`);
  }

  applyCoupon(coupon) {
    return this.http.post(`${environment.apiUrl}/coupon/apply_coupon`, {
      coupon,
    });
  }

  removeCoupon() {
    return this.http.get(`${environment.apiUrl}/coupon/remove_coupon`);
  }

  //get checkout details
  getCheckoutDetails() {
    return this.http.get(`${environment.apiUrl}/checkout`);
  }

  //payments
  generateOrderId(data) {
    return this.http.post(
      `${environment.apiUrl}/payments/generate_order_id`,
      data
    );
  }

  checkPincode(unique_id) {
    return this.http.get(`${environment.apiUrl}/product/checkPincode/${unique_id}`);
  }

  getcouponsForProductDetails() {
    return this.http.get(
      `${environment.apiUrl}/product/getcouponsForProductDetails`
    );
  }

  checkenterPincode(data) {
    return this.http.post(
      `${environment.apiUrl}/product/checkenterPincode`,
      data
    );
  }
  getOrderSummary() {
    return this.http.get(`${environment.apiUrl}/payments/get_order_summary`);
  }
  verifyPaymentSignature(data) {
    return this.http.post(
      `${environment.apiUrl}/payments/verify_payment_signature`,
      data
    );
  }

  reStock() {
    return this.http.get(`${environment.apiUrl}/payments/reStock`);
  }

  createOrder(data) {
    return this.http.post(`${environment.apiUrl}/orders/store_new_order`, data);
  }

  stockDeduction() {
    return this.http.get(`${environment.apiUrl}/orders/stockDeduction`);
  }

  verifyOtpForCod(data) {
    return this.http.post(`${environment.apiUrl}/orders/verifyOtpForCod`, data);
  }

  sendOtpForCod() {
    return this.http.get(`${environment.apiUrl}/orders/sendOtpForCod`);
  }

  //productFilter
  getProductsFilters(data) {
    return this.http.post(
      `${environment.apiUrl}/product/getProductsFilters`,
      data
    );
  }

  getAttributeFilters(data) {
    return this.http.post(
      `${environment.apiUrl}/product/getAttributeFilters`,
      data
    );
  }

  getGlobalSearchFilters() {
    return this.http.get(
      `${environment.apiUrl}/product/getGlobalSearchFilters`
    );
  }

  getParentCategoires() {
    return this.http.get(`${environment.apiUrl}/product/getParentCategoires`);
  }

  getSubCategories(data) {
    return this.http.post(
      `${environment.apiUrl}/product/getSubCategories`,
      data
    );
  }

  getSubCategoriesBySlugs(data) {
    return this.http.post(
      `${environment.apiUrl}/product/getSubCategoriesBySlugs`,
      data
    );
  }

  getChildCategoriesBySlug(data) {
    return this.http.post(
      `${environment.apiUrl}/product/getChildCategoriesBySlug`,
      data
    );
  }

  getChildCategories(data) {
    return this.http.post(
      `${environment.apiUrl}/product/getChildCategories`,
      data
    );
  }

  getAllBrands(data) {
    return this.http.post(`${environment.apiUrl}/product/getAllBrands`, data);
  }

  getOrderSuccess(params) {
    return this.http.post(
      `${environment.apiUrl}/orders/getOrderSuccess`,
      params
    );
  }
  getFilteredData(params) {
    return this.http.post(`${environment.apiUrl}/product/getFilteredData`, {
      params: params,
    });
  }

  getfetured_NewCollection(params) {
    return this.http.get(
      `${environment.apiUrl}/shoplanding/get_featued_newcollection_byId`,
      { params: params }
    );
  }

  getData() {
    return this.http.get("assets/js/offerimages.json");
  }

  getProductReviews(pid, reviewFilters) {
    return this.http.post(
      `${environment.apiUrl}/reviews/getReviews/${pid}`,
      reviewFilters
    );
  }

  getCustomerReviews() {
    return this.http.get(`${environment.apiUrl}/reviews/getCustomerReviews`);
  }

  getAttributesForLocalCart(data) {
    return this.http.post(`${environment.apiUrl}/cart/getAttributes`, data);
  }

  getNotifications() {
    return this.http.get(`${environment.apiUrl}/notifications`);
  }

  checkNotificationEnabled() {
    return this.http.get(
      `${environment.apiUrl}/notifications/checkNotificationEnabled`
    );
  }

  updateFCMTokenPublic(data) {
    return this.http.post(
      `${environment.apiUrl}/notifications/updateOrInsertFCMTokenPublic`,
      data
    );
  }

  updateOrInsertFCMTokenLoggedInUsers(data) {
    return this.http.post(
      `${environment.apiUrl}/notifications/updateOrInsertFCMTokenLoggedInUsers`,
      data
    );
  }

  enableNotifications() {
    return this.http.get(
      `${environment.apiUrl}/notifications/enableNotifications`
    );
  }

  stockNotify(data) {
    return this.http.post(
      `${environment.apiUrl}/notifications/stockNotify`,
      data
    );
  }

  checkUserNotifiedForTheProduct(data) {
    return this.http.post(
      `${environment.apiUrl}/notifications/checkUserNotifiedForTheProduct`,
      data
    );
  }
}

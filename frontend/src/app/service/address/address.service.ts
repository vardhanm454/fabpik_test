import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Customer } from 'src/app/model/authentication/customer.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  getstates():Observable<any> {
    return this.http.get(`${environment.apiUrl}/customer/addresses/getState`);
  }

  getAllAddress():Observable<any>{
    return this.http.get(`${environment.apiUrl}/customer/addresses`);
  }

  addAddress(params):Observable<any>{
    return this.http.post(`${environment.apiUrl}/customer/addresses/add`,params);
  }

  getById(id: string):Observable<any>{
    return this.http.get(`${environment.apiUrl}/customer/addresses/${id}`);
  }

  sendGoogleAddressSuggestion():Observable<any>{
    return this.http.get(`${environment.apiUrl}/customer/addresses/sendGoogleAddressSuggestion`);
  }

  update(id, params):Observable<any>{
    return this.http.put(`${environment.apiUrl}/customer/addresses/update/${id}`, params);
  }

  updatedefaultAddress(id, params):Observable<any>{
    return this.http.put(`${environment.apiUrl}/customer/addresses/updatedefaultAddress/${id}`, params);
  }

  deleteAddress(id):Observable<any>{
    return this.http.delete(`${environment.apiUrl}/customer/addresses/remove/${id}`);
  }

  
}

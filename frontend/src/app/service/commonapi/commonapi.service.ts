import { HttpClient } from '@angular/common/http';
import { Injectable ,EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonapiService {

 
  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }
  
  getPolicies(data):Observable<any> {
    return this.http.get(`${environment.apiUrl}/policy/${data}`);
  }
  getPoliciesdata(postdata):Observable<any> {
    return this.http.get(`${environment.apiUrl}/policy/${postdata.policyname}`);
  }
 
}

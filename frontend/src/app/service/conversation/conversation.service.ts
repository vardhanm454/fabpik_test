import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ConversationService {

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  conversationList(){
    return this.http.get(`${environment.apiUrl}/conversation/conversationList`);
  }

  sendMessage(params){
    return this.http.post(`${environment.apiUrl}/conversation/sendMessage`,params);
  }
  getMessage(params){
    return this.http.get(`${environment.apiUrl}/conversation/getMessage`,{params:params});
  }

  feedback(params){
    return this.http.post(`${environment.apiUrl}/conversation/feedback`,params);
  }
}

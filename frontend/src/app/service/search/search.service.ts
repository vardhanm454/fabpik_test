import { HttpClient } from '@angular/common/http';
import { Injectable ,EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

 // on click product_cat,name parameter send to productlist component
 @Output() searchClickedEvent = new EventEmitter<any>();
 searchClicked(params){ 
   this.searchClickedEvent.emit(params);
 }
  //get product or category based on search
  search(params){
    return this.http.get(`${environment.apiUrl}/product/search`,{params: params});
  }

  getPopularCategory(){
    return this.http.get(`${environment.apiUrl}/product/popularCategory`);
  }
}

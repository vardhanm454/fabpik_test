import { TestBed } from '@angular/core/testing';

import { AngularnotifierService } from './angularnotifier.service';

describe('AngularnotifierService', () => {
  let service: AngularnotifierService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AngularnotifierService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

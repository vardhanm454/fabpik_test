export const environment = {
  production: true,

  //Production Urls
  // apiUrl: "https://portal.fabpik.in/api",
  // apiAuthUrl: "https://portal.fabpik.in/api/auth",
  // imageUrl: "https://portal.fabpik.in/dev/uploads/product/",
  // uploads: "https://portal.fabpik.in/dev/",

  //DEV QA Urls
  apiUrl: "https://dev.fabpikindia.com/PORTAL/api",
  apiAuthUrl: "https://dev.fabpikindia.com/PORTAL/api/auth",
  imageUrl: "https://dev.fabpikindia.com/PORTAL/uploads/product/",
  uploads: "https://dev.fabpikindia.com/PORTAL/dev/",

  firebase: {
    apiKey: "AIzaSyA8gPW9L_WE9QK0pfROs-TN2cxwXyEZqBg",
    authDomain: "fabpik-ce8b5.firebaseapp.com",
    projectId: "fabpik-ce8b5",
    storageBucket: "fabpik-ce8b5.appspot.com",
    messagingSenderId: "618785523880",
    appId: "1:618785523880:web:a5abd2b77995f48b2d1a9d",
    measurementId: "G-35THTWY7RD",
  },

  //BETA Urls
  // apiUrl: "https://beta.fabpikindia.com/PORTAL/api",
  // apiAuthUrl: "https://beta.fabpikindia.com/PORTAL/api/auth",
  // imageUrl: "https://beta.fabpikindia.com/PORTAL/uploads/product/",
  // uploads: "https://beta.fabpikindia.com/PORTAL/dev",

  // apiUrl : 'http://13.233.197.247/dev/api',
  // apiAuthUrl : 'http://13.233.197.247/dev/api/auth',
  // imageUrl : 'http://13.233.197.247/dev/uploads/product/',
  // uploads : 'http://13.233.197.247/dev/',

  // razorpayKey: "rzp_live_ar7IuJMWojaAhX",
  razorpayKey: "rzp_test_Z2tC0HuHhXGnQ8",
};

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: "http://localhost/fabpik-latest/dev/api",
  apiAuthUrl: "http://localhost/fabpik-latest/dev/api/auth",
  imageUrl: "http://localhost/fabpik-latest/dev/uploads/product/",
  uploads: "http://localhost/fabpik-latest/dev/",

  // apiUrl: "http://beta.fabpikindia.com/BETA/PORTAL/api",
  // apiAuthUrl: "http://beta.fabpikindia.com/BETA/PORTAL/api/auth",
  // imageUrl: "http://beta.fabpikindia.com/BETA/PORTAL/uploads/product/",
  // uploads: "http://beta.fabpikindia.com/BETA/PORTAL/dev",
  // apiUrl: "https://portal.fabpik.in/api",
  // apiAuthUrl: "https://portal.fabpik.in/api/auth",
  // imageUrl: "https://portal.fabpik.in/dev/uploads/product/",
  // uploads: "https://portal.fabpik.in/dev/",
  razorpayKey: "rzp_live_ar7IuJMWojaAhX",
  // razorpayKey: "rzp_test_Z2tC0HuHhXGnQ8",
  // razorpayKey: "rzp_test_EQE5plaj9FQJld",
  firebase: {
    apiKey: "AIzaSyA8gPW9L_WE9QK0pfROs-TN2cxwXyEZqBg",
    authDomain: "fabpik-ce8b5.firebaseapp.com",
    projectId: "fabpik-ce8b5",
    storageBucket: "fabpik-ce8b5.appspot.com",
    messagingSenderId: "618785523880",
    appId: "1:618785523880:web:a5abd2b77995f48b2d1a9d",
    measurementId: "G-35THTWY7RD",
  },
  // apiUrl: "http://dev.fabpikindia.com/PORTAL/api",
  // apiAuthUrl: "http://dev.fabpikindia.com/PORTAL/api/auth",
  // imageUrl: "http://dev.fabpikindia.com/PORTAL/uploads/product/",
  // uploads: "http://dev.fabpikindia.com/PORTAL/dev/",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
